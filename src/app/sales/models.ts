import { Type } from '@angular/compiler';

// SALES MODEL

// OTHER MODEL
export interface Pair {
  id: number;
  name: string;
}

export enum ProgressBarState {
  Hide = -1,
  Show = 1
}

export interface ProductListColumn {
  position: number;
  productImage: string,
  productName: string;
  skuNo: string;
  price: number;
  quantity: number;
  subTotal: number;
}
import { Component, OnInit, ViewChild, Input, Inject, Injectable } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
//import { MatDialog, MatTableDataSource, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
// import { MatRippleModule } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
// import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
// import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
//import { AppComponent } from 'src/app/app.component';
import Swal from 'sweetalert2'
import { OrderDetailsComponent } from '../../../orders/order-details/order-details.component';
import { CustomerAddressDetailsComponent } from '../address-details/address-details.component';

@Component({
  selector: 'app-sales-customer-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})

//@Injectable()
export class DialogCustomerComponent implements OnInit {

  // @Input() orderStatus: string;
  // @Input() orderTabIndex: number;

  // dataSource = new MatTableDataSource<any>([]);
  // selection = new SelectionModel<any>(true, []);
  // selectionF = new SelectionModel<any>(true, []);

  @ViewChild('orderDetailsComponent', {static: true}) orderDetailsComponent: OrderDetailsComponent;
  @ViewChild('customerAddressDetailsComponent', {static: true}) customerAddressDetailsComponent: CustomerAddressDetailsComponent;

  primengTableHelper: PrimengTableHelper;
  //pageSetting: PagerSetting;
  //app: AppComponent;

  // @ViewChild('dataTable') dataTable: Table;
  // @ViewChild('paginator') paginator: Paginator;

  // filterText = '';
  // showSpinner: boolean = false;
  orderId: number = 0;
  addressId: number = 0;
  type: string = "";
  isModal: boolean = true;
  submitted: boolean = false;

  constructor(
    //private salesService: SalesService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    //private app: AppComponent,
    public dialogRef: MatDialogRef<DialogCustomerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { OrderId: number, AddressId: number, Type: string },
    private router: Router) {
    // this.primengTableHelper = new PrimengTableHelper();
    // this.pageSetting = new PagerSetting();
    //this.app = new AppComponent;
  }

  ngOnInit() {
    //this.getProductList();
    this.orderId = this.data.OrderId;
    this.addressId = this.data.AddressId;
    this.type = this.data.Type;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async onSave() {
    // TODO: save only selected tab
    //console.log('this.tabsViews ::::::::: ', QueryList.);
    //let component: QueryList<AbstractTabContainer>;
    //console.log('this.tabsViews 2 ::::::::: ', component);

    //console.log('this.employeeRenumeration.form.value ::::::::: ', this.employeeRenumeration.form.value);
    //this.employeeRenumeration.form.value;
    this.submitted = true;
    if (this.type == "address") {
      await this.customerAddressDetailsComponent.onSave();
      //console.log('returnSave :::::::::::::::: ', await  this.customerAddressDetailsComponent.returnSaveCheck);   
      if (await this.customerAddressDetailsComponent.returnSaveCheck) {
        this.dialogRef.close();
      }
    }
    else if (this.type == "purchase") {
      await this.orderDetailsComponent.onSave();
      //console.log('returnSave :::::::::::::::: ', await  this.orderDetailsComponent.returnSaveCheck);      
      if (await this.orderDetailsComponent.returnSaveCheck) {
        this.dialogRef.close();
      }
    }


    /*
    if (this.employeeInfo.form.invalid || this.employeeRenumeration.formEmployement.invalid || this.employeeRenumeration.formRenumeration.invalid
      || this.employeeRequirement.formRequirement.invalid) {
      const configSnakBar: MatSnackBarConfig = {
        duration: 4000,
        verticalPosition: 'bottom',
      };

      let errorMsg = "";

      if(this.employeeInfo.form.invalid && this.employeeRenumeration.formEmployement.invalid)
      {
        errorMsg = "Please make sure the form of Employee Info & Renumeration is complete!";
      }
      else if(this.employeeInfo.form.invalid)
      {
        errorMsg = "Please make sure the form of Employee Info is complete!";
      }
      else if(this.employeeRenumeration.formEmployement.invalid)
      {
        errorMsg = "Please make sure the form of Renumeration is complete!";
      }
      else
      {
        errorMsg = "Please make sure the form is complete!";
      }



      this.snackBar.open(errorMsg, "OK", configSnakBar);


      return; // Exit
    }

    if (this.staffId > 0) {
      const streams = this.tabsViews
        .filter(s => s.save != undefined)
        .map((component: AbstractTabContainer) => {
          //console.log('UPDATE ::::::::: ');
          component.save();
        });
    }
    else {
      this.savePhoto();
    }
    // console.log('streams ::::::::::::::: ', streams);
    // forkJoin(streams)
    //   .subscribe(() => this.dialogRef.close(true));
    */
  }


}

import { Component, OnInit, ViewChild, Inject, Input, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, FormControl, AbstractControl } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
//import { MatDialog, MatSnackBarConfig, MatPaginator, MatSort, MatTableDataSource, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';


// import { MatRippleModule, MAT_DATE_LOCALE } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
//mport { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';


// import { SelectCouponProductComponent } from '../select-product/select-product.component';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import { Router } from '@angular/router';
import { SalesService } from '../../sales.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { ActivatedRoute } from '@angular/router';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { AppComponent } from 'src/app/app.component'
import Swal from 'sweetalert2'
import * as moment from 'moment';
import { CustomValidators } from '../../../shared/validators/custom-validators';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { PurchaseListComponent } from './purchase-list/purchase-list.component';
import { AddressListComponent } from './address-list/address-list.component';

@Component({
  selector: 'app-sales-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss']
})

export class CustomerDetailsComponent implements OnInit {

  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  selectionF = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  customValidator: CustomValidators;

  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;
  @ViewChild('inputFile', {static: true}) inputFile: ElementRef;
  @ViewChild('purchaseListComponent', {static: true}) purchaseListComponent: PurchaseListComponent;
  @ViewChild('addressListComponent', {static: true}) addressListComponent: AddressListComponent;

  Attachment: string = "";
  public imagePath;
  ImageUrl: any = "";
  imageChangedEvent: any = '';
  croppedImage: any = '';
  croppedImageFile: any = '';
  submitted = false;
  CustomerId: number;
  StatusId: number = 0;
  form: FormGroup;
  statusList: any = [];
  
  // TypeId: number = 0;
  // searchKey: string;
  // typeList: any = [];
  // productList: any = [];  
  // StartDate: any;
  // EndDate: any;
  // StartTime: string = '';
  // EndTime: string = '';  

  constructor(
    private salesService: SalesService,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private snackBar: MatSnackBar,
    private app: AppComponent,
    private activatedRoute: ActivatedRoute) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
    this.customValidator = new CustomValidators();
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.CustomerId = params.id;
      }
    });
    this.getStatusList();
    //this.getTypeList();
    this.loadDetails(this.CustomerId);
    //this.getProductList();
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  onCropped() {
    this.ImageUrl = this.croppedImage;
    this.croppedImageFile = this.base64ImageToBlob(this.croppedImage);
    this.form.get('Attachment').setValue(this.croppedImageFile);
    this.imageChangedEvent = '';
  }

  base64ImageToBlob(str) {
    var pos = str.indexOf(';base64,');
    var type = str.substring(5, pos);
    var b64 = str.substr(pos + 8);
    var imageContent = atob(b64);
    var buffer = new ArrayBuffer(imageContent.length);
    var view = new Uint8Array(buffer);
    for (var n = 0; n < imageContent.length; n++) {
      view[n] = imageContent.charCodeAt(n);
    }
    var blob = new Blob([buffer], { type: type });
    return blob;
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      this.imageChangedEvent = event;
    }
  }

  cancelUpload() {
    this.Attachment = "";
    this.inputFile.nativeElement.value = "";
    this.ImageUrl = "";
    document.getElementById("displayFile").innerHTML = "";
  }

  // getTotalCostProduct() {
  //   return this.productList.map(t => t.subTotal).reduce((acc, value) => acc + value, 0);
  // }

  loadDetails(CustomerId) {
    this.app.showOverlaySpinner(true);
    this.form = this.buildFormItems();
    if (CustomerId > 0) {
      this.salesService.getCustomerDetails(CustomerId)
        .subscribe(result => {
          this.app.showOverlaySpinner(false);
          this.StatusId = result.StatusId > 0 ? result.StatusId : 0;
          this.ImageUrl = result.ImageUrl;
          // this.TypeId = result.TypeId > 0 ? result.TypeId : 0;
          // this.StartDate = result.StartDate = moment(result.StartDate) || '';
          // this.StartTime = result.StartTime;
          // this.EndDate = result.EndDate = moment(result.EndDate) || '';
          // this.EndTime = result.EndTime;
          this.form.patchValue(result);
        });
    }
    else {
      this.app.showOverlaySpinner(false);
    }
  }

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      Attachment: ['', []],
      ImageUrl: ['', []],      
      Name: ['', [Validators.required]],
      Email: ['', [Validators.required, Validators.email]],
      PhoneNo: ['', [Validators.required]], 
      StatusId: ['0', [Validators.required, Validators.min(1)]],     
    }, { validator: this.customValidation()}
    );
  }

  customValidation() {
    return (formGroup: FormGroup) => {
      const ImageUrl = formGroup.controls['ImageUrl'];
      const Attachment = formGroup.controls['Attachment'];
      if (ImageUrl && Attachment) {
        ImageUrl.setErrors(null);
        if (!ImageUrl.value && !Attachment.value) {
          ImageUrl.setErrors({ required: true });
        }
      }
    }
  }

  onSave() {

    // this.selectionF.clear();
    // this.dataSource.data.forEach(row => this.selectionF.select(row));
    // console.log('this.this.selectionF:::::::::::::::::::::: ', this.selectionF);
    // const selectedItemsF: Array<{ id: number }> = this.selectionF.selected.map(item => ({ id: item.productId, salesPrice: item.salesPrice, limitBuyer: item.limitBuyer }));
    // console.log('this.this.selectionF:::::::::::::::::::::: ', selectedItemsF);

    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Warning',
        'Please make sure the form is complete and valid!',
        'warning'
      );
    }
    else {
      let model = this.form.value;
      this.app.showOverlaySpinner(true);
      this.salesService.saveCustomer(model, this.CustomerId)
        .subscribe(r => {
          this.app.showOverlaySpinner(false);
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            //this.router.navigate(['/app/sales/promotion-list']);
          }
          else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }

  getStatusList() {
    this.salesService.getEnableList().subscribe(items => {
      this.statusList = items;
    });
  }

  // getTypeList() {
  //   this.salesService.getCouponTypeList().subscribe(items => {
  //     this.typeList = items;
  //   });
  // }


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.productId + 1}`;
  }


  deleteProduct() {
    //this.selectionF.clear();
    //this.dataSource.data.forEach(row => this.selectionF.select(row));
    console.log('this.this.selectionF:::::::::::::::::::::: ', this.selectionF);
    //const selectedItemsF: Array<{ id: number }> = this.selectionF.selected.map(item => ({ id: item.CustomerId, enable: item.Enable }));
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.productId }));
    //console.log('selectedItemsF :::::::::::::::::::::: ', selectedItemsF);

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.app.showOverlaySpinner(true);
        this.salesService.deleteCouponProduct(selectedItems, this.CustomerId)
          .subscribe(resultDelete => {
            this.app.showOverlaySpinner(false);
            if (resultDelete.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultDelete.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultDelete.ResponseMessage,
                'success'
              );
            }
            this.getProductList();
          });
      }
    })
  }


  addPorductCoupon(): void {
    // const dialogRef = this.dialog.open(SelectCouponProductComponent, { data: { CustomerId: this.CustomerId } });
    // dialogRef.afterClosed()
    //   .subscribe(ok => {
    //     this.getProductList();
    //   });
  }


  getProductList(event?: LazyLoadEvent) {
    console.log('getPromotionList ::::::::::::::::::::: ');
    if (this.CustomerId > 0) {
      this.app.showOverlaySpinner(true);
      this.salesService.getProductListByPromotion(this.pageSetting.getPagerSetting(this.paginator, event
        , this.primengTableHelper.getSorting(this.dataTable)
      ), this.CustomerId)
        .subscribe(items => {
          this.app.showOverlaySpinner(false);
          console.log('getOrderList items ::::::::::::::::::::: ', items);
          this.dataSource.data = items.list;
          this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
          this.primengTableHelper.records = items.list;
          if (event! && (event.filters || event.sortField)) {
            this.paginator.changePage(0);
          }
          this.primengTableHelper.hideLoadingIndicator();
        });
      this.primengTableHelper.showLoadingIndicator();
    }
  }


}

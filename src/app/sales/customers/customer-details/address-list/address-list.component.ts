import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
//import { MatDialog, MatTableDataSource } from '@angular/material';

// import { MatRippleModule } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
// import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import { PrimengTableHelper } from '../../../../../../src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { SalesService } from '../../../sales.service';
import { Pager, PagerSetting } from '../../../../../pagingnation';
import { AppComponent } from '../../../../../../src/app/app.component';
// import { OrderDetailsComponent } from '../../../orders/order-details/order-details.component';
import { DialogCustomerComponent } from '../dialog/dialog.component';
import { CustomerAddressDetailsComponent } from '../address-details/address-details.component';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-sales-customers-address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.scss']
})

export class AddressListComponent implements OnInit {

  @Input() CustomerId: number;
 
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  filterText = '';
  dataToPass: any = [];

  constructor(
    private salesService: SalesService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private app: AppComponent,
    private router: Router) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  ngOnInit() {
    // console.log('LIST orderStatus :::::::::::', this.orderStatus);
    // console.log('LIST orderTabIndex :::::::::::', this.orderTabIndex);
    this.getAddressList();
    //this.service.getDocumentTypeList();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.addressId + 1}`;
  }
 

  delete() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.addressId }));

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.app.showOverlaySpinner(true);
        this.salesService.deleteAddress(selectedItems)
          .subscribe(resultDelete => {
            this.app.showOverlaySpinner(false);
            if (resultDelete.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultDelete.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultDelete.ResponseMessage,
                'success'
              );
            }
            this.getAddressList();
          });
      }
    })
  }


  
  addressDetails(AddressId?: number): void {  
    console.log('orderDetails orderId ::::::::::::::: ',  AddressId); 
    // this.router.navigate(['/app/sales/order-details', orderId, this.orderTabIndex]);
    // const dialogRef = this.dialog.open(CustomerAddressDetailsComponent, { data: { AddressId: AddressId } });
    // dialogRef.afterClosed()
    // .subscribe(ok => {
    //   this.getAddressList();
    // });
    const dialogRef = this.dialog.open(DialogCustomerComponent, { data: { AddressId: AddressId, Type: "address" } });
    dialogRef.afterClosed()
    .subscribe(ok => {
      this.getAddressList();
    });
  }

  getAddressList(event?: LazyLoadEvent) {
    console.log('getAddressList ::::::::::::::::::::: ');
    this.app.showOverlaySpinner(true);
    this.salesService.getAddressList(this.CustomerId)
      .subscribe(items => {
        this.app.showOverlaySpinner(false);
        console.log('getAddressList items ::::::::::::::::::::: ', items);
        this.dataSource.data = items;
        this.primengTableHelper.totalRecordsCount = items != null ? items.length : 0;
        this.primengTableHelper.records = items;
        // if (event! && (event.filters || event.sortField)) {
        //   this.paginator.changePage(0);
        // }
        this.primengTableHelper.hideLoadingIndicator();
      });
    this.primengTableHelper.showLoadingIndicator();
  }

}

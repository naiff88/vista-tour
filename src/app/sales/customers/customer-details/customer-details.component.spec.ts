import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomorDetailsComponent } from './customer-details.component';

describe('CustomorDetailsComponent', () => {
  let component: CustomorDetailsComponent;
  let fixture: ComponentFixture<CustomorDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomorDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

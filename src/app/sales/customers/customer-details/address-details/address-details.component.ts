import { Component, OnInit, ViewChild, Input, Inject, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, FormControl, AbstractControl } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
//import { MatDialog, MatTableDataSource, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

// import { MatRippleModule } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
// import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
// import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { SalesService } from '../../../sales.service';
import { Pager, PagerSetting } from '../../../../../pagingnation';
import { AppComponent } from 'src/app/app.component';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-sales-customer-address-details',
  templateUrl: './address-details.component.html',
  styleUrls: ['./address-details.component.scss']
})

//@Injectable()
export class CustomerAddressDetailsComponent implements OnInit {

  @Input() addressId: number;
  @Input() isModal: boolean = false;
  showSpinner: boolean = false;
  form: FormGroup;
  StateId: number = 0;
  stateList: any = [];
  submitted = false;
  returnSaveCheck = false;

  constructor(
    private salesService: SalesService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    //private app: AppComponent,
    public dialogRef: MatDialogRef<CustomerAddressDetailsComponent>,
    // @Inject(MAT_DIALOG_DATA) public data: { AddressId: number },
    private router: Router) {
    
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    //this.getProductList();
    // this.addressId = this.data.AddressId;
    this.loadDetails(this.addressId);
    this.getStateList();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      Name: ['', [Validators.required]],
      PhoneNo: ['', [Validators.required]],  
      Address1: ['', [Validators.required]],  
      Address2: ['', []],  
      Poscode: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(5)]],  
      StateId: ['0', [Validators.required, Validators.min(1)]], 
      City: ['', [Validators.required]], 
    }, {}
    );
  }

  loadDetails(AddressId) {
    this.showSpinner = true;
    this.form = this.buildFormItems();
    if (AddressId > 0) {
      this.salesService.getAddressDetails(AddressId)
        .subscribe(result => {
          this.showSpinner = false;
          this.StateId = result.StateId > 0 ? result.StateId : 0;
          this.form.patchValue(result);
        });
    }
    else {
      this.showSpinner = false;
    }
  }

  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Warning',
        'Please make sure the form is complete and valid!',
        'warning'
      );
      
    }
    else {
      let model = this.form.value;
      //this.app.showOverlaySpinner(true);
      this.showSpinner = true;
      console.log('model :::::::::::::::: ', model);
      this.salesService.saveAddress(model, this.addressId)
        .subscribe(r => {
          //this.app.showOverlaySpinner(false);
          this.showSpinner = false;
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            //this.router.navigate(['/app/sales/order-list', this.orderTabIndex]);
            this.returnSaveCheck = true;
          }
          else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
           
          }
        });
    }
  }

  getStateList() {
    this.salesService.getStateList().subscribe(items => {
      this.stateList = items;
    });
  }
 

}

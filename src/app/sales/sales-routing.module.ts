import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { OrderListComponent } from './orders/list/list.component';
import { TabListComponent } from './orders/list/tab-list/tab-list.component';
import { PromotionListComponent } from './promotions/list/list.component';
import { PromotionDetailsComponent } from './promotions/promotion-details/promotion-details.component';
import { MenuComponent } from '../dashboard/menu/menu.component';
import { OrderDetailsComponent } from './orders/order-details/order-details.component';
import { CouponListComponent } from './coupon/list/list.component';
import { CouponDetailsComponent } from './coupon/coupon-details/coupon-details.component';
import { CustomerListComponent } from './customers/list/list.component';
import { CustomerDetailsComponent } from './customers/customer-details/customer-details.component';
import { RatingListComponent } from './rating/list/list.component';
import { RatingDetailsComponent } from './rating/rating-details/rating-details.component';

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MenuComponent,
                children: [
                    { path: '', pathMatch: 'full', redirectTo: 'order-list/:orderTabIndex' },
                    { path: 'order-list/:orderTabIndex', component: TabListComponent },
                    { path: 'order-details/:id/:orderTabIndex', component: OrderDetailsComponent },
                    { path: 'promotion-list', component: PromotionListComponent },
                    { path: 'promotion-details/:id', component: PromotionDetailsComponent },
                    { path: 'coupon-list', component: CouponListComponent },
                    { path: 'coupon-details/:id', component: CouponDetailsComponent },
                    { path: 'customer-list', component: CustomerListComponent },
                    { path: 'customer-details/:id', component: CustomerDetailsComponent },
                    { path: 'rating-list', component: RatingListComponent },
                    { path: 'rating-details/:id', component: RatingDetailsComponent },
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})

export class SalesRoutingModule { }

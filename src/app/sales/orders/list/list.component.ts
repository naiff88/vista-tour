import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
//import { MatDialog, MatTableDataSource } from '@angular/material';
// import { MatRippleModule } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
// import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { SalesService } from '../../sales.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { AppComponent } from 'src/app/app.component'
declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-sales-orders-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class OrderListComponent implements OnInit {

  @Input() orderStatus: string;
  @Input() orderTabIndex: number;
  
 
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  filterText = '';
  dataToPass: any = [];

  constructor(
    private salesService: SalesService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private app: AppComponent,
    private router: Router) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  ngOnInit() {
    console.log('LIST orderStatus :::::::::::', this.orderStatus);
    console.log('LIST orderTabIndex :::::::::::', this.orderTabIndex);
    this.getOrderList();
    //this.service.getDocumentTypeList();
  }
 
  

  orderDetails(orderId?: number): void {  
    console.log('orderDetails orderId ::::::::::::::: ',  orderId); 
    console.log('orderDetails this.orderTabIndex ::::::::::::::: ',  this.orderTabIndex); 
    this.router.navigate(['/app/sales/order-details', orderId, this.orderTabIndex]);
  }

  autoTablePdf(){    
    var doc = new jsPDF();
    var col = ["No", "OrderNo", "OrderDateTime", "Status", "Total"];
    var rows = [];
    let i = 1;
    console.log('this.dataSource.data ::::::::::: ', this.dataSource.data);
    let list = this.dataSource.data;
    for(var key in list){
      console.log('key[i] ::::::::::: ', this.dataSource.data[key]);
        var temp = [i, list[key].orderNo,   list[key].orderDateTime,   list[key].status, list[key].total];
        rows.push(temp);
        i++;
    }
    doc.autoTable(col, rows);
    doc.save('OrderList.pdf');
  }

  getOrderList(event?: LazyLoadEvent) {
    console.log('getOrderList ::::::::::::::::::::: ');
    this.app.showOverlaySpinner(true);
    this.salesService.getOrderList(this.pageSetting.getPagerSetting(this.paginator, event
      , this.primengTableHelper.getSorting(this.dataTable)
    ), this.orderTabIndex)
      .subscribe(items => {
        this.app.showOverlaySpinner(false);
        console.log('getOrderList items ::::::::::::::::::::: ', items);
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
        this.primengTableHelper.hideLoadingIndicator();
      });
    this.primengTableHelper.showLoadingIndicator();
  }

}

import { Component, OnInit, QueryList, ViewChildren, ViewChild, Input, Inject } from '@angular/core';
//import { MatSnackBar, MatSnackBarConfig, MatTabGroup, MatDialogRef, MatTab, MAT_DIALOG_DATA } from '@angular/material';

// import { MatRippleModule } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
// import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
//import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import { OrderListComponent } from '../list.component';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-orders-tab-list',
  templateUrl: './tab-list.component.html',
  styleUrls: ['./tab-list.component.scss']
})

export class TabListComponent implements OnInit {

  form: FormGroup;
  @ViewChildren('tab')
  @ViewChild(MatTabGroup, {static: true} ) tabs: MatTabGroup;


  orderStatus: string = 'All';
  orderTabIndex: number = 0;
  selected = new FormControl(0);
 

  @ViewChild('orderListComponent',  {static: true} ) orderListComponent: OrderListComponent;
  // @ViewChild('orderListComponent2') orderListComponent2: OrderListComponent;
  // @ViewChild('orderListComponent4') orderListComponent3: OrderListComponent;
  // @ViewChild('orderListComponent4') orderListComponent4: OrderListComponent;

  constructor(public dialogRef: MatDialogRef<TabListComponent>,
    private activatedRoute: ActivatedRoute,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    //this.tabs.selectedIndex = 1;
    this.activatedRoute.params.subscribe(params => {
      console.log('params orderTabIndex :::::::::::::: ', params.orderTabIndex);
      if (params.orderTabIndex) {
        this.orderTabIndex = params.orderTabIndex;     
      }
    });
    this.orderStatus = "All";
    this.selected.setValue(this.orderTabIndex);

    if (this.selected.value == 0) {
      this.orderStatus = 'All';
    }
    else if (this.selected.value == 1) {
      this.orderStatus = 'New';
    }
    else if (this.selected.value == 2) {
      this.orderStatus = 'Processed';
    }
    else if (this.selected.value == 3) {
      this.orderStatus = 'Completed';
    }
    this.orderTabIndex = this.selected.value;

    
   }

   ngAfterViewInit() {
    console.log('selected :::::::::::::: ', this.selected.value);
   
   }

  tabClick(val) {
    if (this.selected.value == 0) {
      this.orderStatus = 'All';
    }
    else if (this.selected.value == 1) {
      this.orderStatus = 'New';
    }
    else if (this.selected.value == 2) {
      this.orderStatus = 'Processed';
    }
    else if (this.selected.value == 3) {
      this.orderStatus = 'Completed';
    }
    console.log('this.tabs.selectedIndex ::::::::::::::::: ', this.selected.value)
    this.orderTabIndex = this.selected.value;
  }
}

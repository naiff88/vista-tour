import { Component, OnInit, ViewChild, Inject, Input, Injectable, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, FormControl } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
//import { MatDialog, MatSnackBarConfig, MatPaginator, MatSort, MatTableDataSource, MatSnackBar, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

// import { MatRippleModule } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
//import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import { Router } from '@angular/router';
import { SalesService } from '../../sales.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { ActivatedRoute } from '@angular/router';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
//import { AppComponent } from 'src/app/app.component'
import Swal from 'sweetalert2'
declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');
import html2canvas from 'html2canvas'

@Component({
  selector: 'app-sales-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})

// @Injectable({
//   providedIn: 'root'
// })
export class OrderDetailsComponent implements OnInit {
  @Input() orderId: number;
  @Input() isModal: boolean = false;
  

  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator',  {static: true}) paginator: Paginator;
  @ViewChild('content', {static: false}) content: ElementRef;
 
  filterText = '';
  dataToPass: any = [];
  searchKey: string;
  //orderId: number;
  orderTabIndex: number;
  statusList: any = [];
  paymentTypeList: any = [];
  courierTypeList: any = [];
  productList: any = [];
  CourierId: number = 0;
  StatusId: number = 0;
  PaymentTypeId: number = 0;
  ShippingCost: number = 0;
  submitted = false;
  productListDisplayedColumns: string[] = ['position', 'productImage', 'productName', 'skuNo', 'price', 'quantity', 'subTotal'];
  showSpinner: boolean = false;
  returnSaveCheck: boolean = false;



  form: FormGroup;

  constructor(
    private salesService: SalesService,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private snackBar: MatSnackBar,
    //private app: AppComponent,
    public dialogRef: MatDialogRef<OrderDetailsComponent>,
   // @Inject(MAT_DIALOG_DATA) public data: { OrderId: number },  
    private activatedRoute: ActivatedRoute) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.orderId = params.id;
        this.orderTabIndex = params.orderTabIndex;
      }
    });

    //this.orderId = this.data.OrderId ? this.data.OrderId : this.orderId;

    this.getStatusList();
    this.getPaymentTypeList();
    this.getCourierTypeList();
    this.getProductListByOrder(this.orderId);
    this.loadDetails(this.orderId);
    // console.log('OrderID : ', this.orderId);
    // console.log('orderTabIndex : ', this.orderTabIndex);
    // console.log('productList : ', this.productList);


  }

  getTotalCostProduct() {
    return this.productList.map(t => t.subTotal).reduce((acc, value) => acc + value, 0);
  }

  htmlPdf() {
    this.showSpinner = true;
    const doc = new jsPDF();

    const specialElementHandlers = {
      '#editor': function (element, renderer) {
        return true;
      }
    };

    //const content = this.content.nativeElement;
    let htmlContent = '<h1>HTML TEST</h1>';

    doc.fromHTML(htmlContent, 15, 15, {
      width: 190,
      'elementHandlers': specialElementHandlers
    });
    this.showSpinner = false;
    doc.save('html.pdf');
  }

  canvasPdf() {
    this.showSpinner = true;
    const div = document.getElementById('content');
    const options = {
      background: 'white',
      scale: 3
    };  

    html2canvas(div, options).then((canvas) => {     
      var imgData = canvas.toDataURL("image/PNG");
      var imgWidth = 210;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;
      var doc = new jsPDF('p', 'mm', "a4");
      var position = 0;
      doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight + 10);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight + 10);
        heightLeft -= pageHeight;
      }

      return doc;
    }).then((doc) => {
      this.showSpinner = false;
      doc.save('html2canvas.pdf');
    });
  }


  loadDetails(OrderId) {
    //this.app.showOverlaySpinner(true);
    this.showSpinner = true;
    this.form = this.buildFormItems();
    console.log('loadDetails OrderId::::::::::::: ', OrderId);
    if (OrderId > 0) {      
      this.salesService.getOrderDetails(OrderId)
        .subscribe(result => {
          //this.app.showOverlaySpinner(false);
          this.showSpinner = false;
          this.CourierId = result.CourierId > 0 ? result.CourierId : 0;
          this.StatusId = result.StatusId > 0 ? result.StatusId : 0;
          this.PaymentTypeId = result.PaymentTypeId > 0 ? result.PaymentTypeId : 0;
          this.ShippingCost = result.ShippingCost > 0 ? result.ShippingCost : 0;
          result.Amount = this.getTotalCostProduct() + result.ShippingCost;
          this.form.patchValue(result);
        });
    }
  }

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      Name: ['', []],
      OrderNo: ['', []],
      Address: ['', []],
      OrderDateTime: ['', []],
      Email: ['', []],
      PhoneNo: ['', []],
      CourierId: ['', []],
      TrackingNo: ['', [Validators.required]],
      StatusId: ['0', [Validators.required, Validators.min(1)]],
      PaymentTypeId: ['', []],
      TransactionNo: ['', []],
      PaymentDate: ['', []],
      Amount: ['', []],
    }, {}
    );
  }

  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Warning',
        'Please make sure the form is complete!',
        'warning'
      );
    }
    else {
      let model = this.form.value;
      //this.app.showOverlaySpinner(true);
      this.showSpinner = true;
      console.log('model :::::::::::::::: ', model);

      this.salesService.saveOrder(model, this.orderId)
        .subscribe(r => {
          //this.app.showOverlaySpinner(false);
          this.showSpinner = false;
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.returnSaveCheck = true;
            //this.router.navigate(['/app/sales/order-list', this.orderTabIndex]);
          }
          else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }

  getStatusList() {
    //console.log('getStatusList ::::::::::::::::::::: ');
    this.salesService.getStatusList().subscribe(items => {
      console.log('getStatusList items ::::::::::::::::::::: ', items);
      this.statusList = items;
    });
  }

  getPaymentTypeList() {
    //console.log('getStatusList ::::::::::::::::::::: ');
    this.salesService.getPaymentTypeList().subscribe(items => {
      console.log('getPaymentTypeList items ::::::::::::::::::::: ', items);
      this.paymentTypeList = items;
    });
  }

  getCourierTypeList() {
    //console.log('getStatusList ::::::::::::::::::::: ');
    this.salesService.getCourierTypeList().subscribe(items => {
      console.log('getCourierTypeList items ::::::::::::::::::::: ', items);
      this.courierTypeList = items;
    });
  }


  getProductListByOrder(OrderId) {
    //console.log('getStatusList ::::::::::::::::::::: ');
    this.salesService.getProductListByOrder(OrderId).subscribe(items => {
      console.log('getStatusList items ::::::::::::::::::::: ', items);
      this.productList = items;
    });
  }

  // getOrderDetails(OrderId) {
  //   console.log('getStatusList ::::::::::::::::::::: ');
  //   this.salesService.getOrderDetails(OrderId).subscribe(items => {
  //     console.log('getStatusList items ::::::::::::::::::::: ', items);
  //     this.productList = items;
  //   });
  // }


}

import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
//import { MatDialog, MatTableDataSource } from '@angular/material';

// import { MatRippleModule } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
// import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { SalesService } from '../../sales.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { AppComponent } from 'src/app/app.component';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-sales-rating-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class RatingListComponent implements OnInit {

  
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  selectionF = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  filterText = '';

  constructor(
    private salesService: SalesService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private app: AppComponent,
    private router: Router) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  ngOnInit() {
    this.getRatingList();
  }


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.RatingId + 1}`;
  }
 
  createRange(number){
    var items: number[] = [];
    for(var i = 1; i <= number; i++){
       items.push(i);
    }
    return items;
  }

  delete() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.RatingId }));

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.app.showOverlaySpinner(true);
        this.salesService.deleteRating(selectedItems)
          .subscribe(resultDelete => {
            this.app.showOverlaySpinner(false);
            if (resultDelete.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultDelete.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultDelete.ResponseMessage,
                'success'
              );
            }
            this.getRatingList();
          });
      }
    })
  }
  
  ratingDetails(RatingId?: number): void {
    console.log('orderDetails RatingId ::::::::::::::: ', RatingId);
    this.router.navigate(['/app/sales/rating-details', RatingId]);
  }

  getRatingList(event?: LazyLoadEvent) {
    this.app.showOverlaySpinner(true);
    this.salesService.getRatingList(this.pageSetting.getPagerSetting(this.paginator, event
      , this.primengTableHelper.getSorting(this.dataTable)
    ))
      .subscribe(items => {
        this.app.showOverlaySpinner(false);
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
        this.primengTableHelper.hideLoadingIndicator();
      });
    this.primengTableHelper.showLoadingIndicator();
  }

}

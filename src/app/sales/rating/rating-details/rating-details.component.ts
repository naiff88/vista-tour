import { Component, OnInit, ViewChild, Inject, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, FormControl, AbstractControl } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
//import { MatDialog, MatSnackBarConfig, MatPaginator, MatSort, MatTableDataSource, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';

// import { MatRippleModule } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
//import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import { Router } from '@angular/router';
import { SalesService } from '../../sales.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { ActivatedRoute } from '@angular/router';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { AppComponent } from 'src/app/app.component'
import Swal from 'sweetalert2'
import * as moment from 'moment';
import { CustomValidators } from '../../../shared/validators/custom-validators';

@Component({
  selector: 'app-sales-rating-details',
  templateUrl: './rating-details.component.html',
  styleUrls: ['./rating-details.component.scss']
})

export class RatingDetailsComponent implements OnInit {

  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  selectionF = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  customValidator: CustomValidators;

  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  RatingId: number;
  StatusId: number = 1;
  TypeId: number = 0;
  Rating: number = 0;
  submitted: boolean = false;
  statusList: any = [];
  typeList: any = [];
  Comments: string = '';
  Reply: string = '';
  Date: any;
 
  form: FormGroup;

  constructor(
    private salesService: SalesService,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private snackBar: MatSnackBar,
    private app: AppComponent,
    private activatedRoute: ActivatedRoute) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
    this.customValidator = new CustomValidators();
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.RatingId = params.id;
      }
    });
    this.getStatusList();
    this.getTypeList();
    this.loadDetails(this.RatingId);
    this.getProductList();
  }

  
  loadDetails(RatingId) {
    this.app.showOverlaySpinner(true);
    this.form = this.buildFormItems();
    if (RatingId > 0) {
      this.salesService.getReviewDetails(RatingId)
        .subscribe(result => {
          //console.log('result ::::::::::::::: ', result) 
          this.app.showOverlaySpinner(false);
          this.StatusId = result.StatusId > 0 ? result.StatusId : 0;
          this.TypeId = result.TypeId > 0 ? result.TypeId : 0;
          this.Rating = result.Rating > 0 ? result.Rating : 0;
          this.Date = result.Date = moment(result.Date) || '';
          this.Comments = result.Comments;
          this.Reply = result.Reply;
          this.form.patchValue(result);
        });
    }
    else {
      this.app.showOverlaySpinner(false);
    }
  }

  createRange(number){
    var items: number[] = [];
    for(var i = 1; i <= number; i++){
       items.push(i);
    }
    return items;
  }

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      CustomerName: ['', [Validators.required]],
      Date: ['', [Validators.required]],
      TypeId: ['0', [Validators.required, Validators.min(1)]],
      Comments: ['', []],
      Reply: ['', []],
      StatusId: ['1', [Validators.required, Validators.min(1)]],      
    }, { }
    );
  }


  onSave() {    

    this.submitted = true;
    
    //|| this.Reply.trim() == ''
    if (this.form.invalid ) {
      Swal.fire(
        'Warning',
        'Please make sure the form is complete and valid!',
        'warning'
      );
    }
    else {
      let model = this.form.value;
      model.Reply = this.Reply;
      model.Comments = this.Comments;

      //console.log('MODEL :::::::::::: ', model);
      this.app.showOverlaySpinner(true);
      this.salesService.saveRating(model, this.RatingId)
        .subscribe(r => {
          this.app.showOverlaySpinner(false);
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.router.navigate(['/app/sales/rating-list']);
          }
          else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }

  }

  getStatusList() {
    this.salesService.getRatingStatusList().subscribe(items => {
      this.statusList = items;
    });
  }

  getTypeList() {
    this.salesService.getReviewTypeList().subscribe(items => {
      this.typeList = items;
    });
  }



  getProductList(event?: LazyLoadEvent) {
    ///console.log('getPromotionList ::::::::::::::::::::: ');
    if (this.RatingId > 0) {
      this.app.showOverlaySpinner(true);
      this.salesService.getProductListByRating(this.pageSetting.getPagerSetting(this.paginator, event
        , this.primengTableHelper.getSorting(this.dataTable)
      ), this.RatingId)
        .subscribe(items => {
          this.app.showOverlaySpinner(false);
          //console.log('getOrderList items ::::::::::::::::::::: ', items);
          this.dataSource.data = items.list;
          this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
          this.primengTableHelper.records = items.list;
          if (event! && (event.filters || event.sortField)) {
            this.paginator.changePage(0);
          }
          this.primengTableHelper.hideLoadingIndicator();
        });
      this.primengTableHelper.showLoadingIndicator();
    }
  }


}

import { Component, OnInit, ViewChild, Inject, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, FormControl, AbstractControl } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
//import { MatDialog, MatSnackBarConfig, MatPaginator, MatSort, MatTableDataSource, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';

// import { MatRippleModule } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
//import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';


import { SelectCouponProductComponent } from '../select-product/select-product.component';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import { Router } from '@angular/router';
import { SalesService } from '../../sales.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { ActivatedRoute } from '@angular/router';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { AppComponent } from 'src/app/app.component'
import Swal from 'sweetalert2'
import * as moment from 'moment';
import { CustomValidators } from '../../../shared/validators/custom-validators';

@Component({
  selector: 'app-sales-coupon-details',
  templateUrl: './coupon-details.component.html',
  styleUrls: ['./coupon-details.component.scss']
})

export class CouponDetailsComponent implements OnInit {

  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  selectionF = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  customValidator: CustomValidators;

  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  CouponId: number;
  StatusId: number = 0;
  TypeId: number = 0;
  searchKey: string;
  statusList: any = [];
  typeList: any = [];
  productList: any = [];
  submitted = false;
  StartDate: any;
  EndDate: any;
  StartTime: string = '';
  EndTime: string = '';

  form: FormGroup;

  constructor(
    private salesService: SalesService,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private snackBar: MatSnackBar,
    private app: AppComponent,
    private activatedRoute: ActivatedRoute) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
    this.customValidator = new CustomValidators();
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.CouponId = params.id;
      }
    });
    this.getStatusList();
    this.getTypeList();
    this.loadDetails(this.CouponId);
    this.getProductList();
  }

  getTotalCostProduct() {
    return this.productList.map(t => t.subTotal).reduce((acc, value) => acc + value, 0);
  }

  loadDetails(CouponId) {
    this.app.showOverlaySpinner(true);
    this.form = this.buildFormItems();
    if (CouponId > 0) {
      this.salesService.getCouponDetails(CouponId)
        .subscribe(result => {
          this.app.showOverlaySpinner(false);
          this.StatusId = result.StatusId > 0 ? result.StatusId : 0;
          this.TypeId = result.TypeId > 0 ? result.TypeId : 0;
          this.StartDate = result.StartDate = moment(result.StartDate) || '';
          this.StartTime = result.StartTime;
          this.EndDate = result.EndDate = moment(result.EndDate) || '';
          this.EndTime = result.EndTime;
          this.form.patchValue(result);
        });
    }
    else {
      this.app.showOverlaySpinner(false);
    }
  }

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      Name: ['', [Validators.required]],
      Code: ['', [Validators.required]],
      StartDate: ['', [Validators.required]],
      StartTime: ['', [Validators.required]],
      EndDate: ['', [Validators.required]],
      EndTime: ['', [Validators.required]],      
      Discount: ['0', [Validators.required, Validators.min(0)]],
      Quantity: ['0', [Validators.required, Validators.min(1)]],
      MinPurchase: ['0', [Validators.required, Validators.min(1)]],
      LimitPerBuyer: ['', []],
      StatusId: ['0', [Validators.required, Validators.min(1)]],
      TypeId: ['0', [Validators.required, Validators.min(1)]],
    }, { validator: this.customValidation() }
    );
  }

  customValidation() {
    return (formGroup: FormGroup) => {
      const StartDate = formGroup.controls['StartDate'];
      const EndDate = formGroup.controls['EndDate'];
      const StartTime = formGroup.controls['StartTime'];
      const EndTime = formGroup.controls['EndTime'];

      if (StartDate) {
        StartDate.setErrors(null);
        if (!StartDate.value) {
          StartDate.setErrors({ required: true });
        }
        else if (StartDate.value && EndDate.value && EndDate.value < StartDate.value) {
          StartDate.setErrors({ exceed: true });
        }
      }

      if (EndDate) {
        EndDate.setErrors(null);
        if (!EndDate.value) {
          EndDate.setErrors({ required: true });
        }
        else if (StartDate.value && EndDate.value && EndDate.value < StartDate.value) {
          EndDate.setErrors({ exceed: true });
        }
      }

      if (StartTime) {
        StartTime.setErrors(null);
        if (!StartTime.value) {
          StartTime.setErrors({ required: true });
        }
        else if (StartDate.value && EndDate.value && String(StartDate.value._d) == String(EndDate.value._d) && StartTime.value && EndTime.value && EndTime.value < StartTime.value) {
          StartTime.setErrors({ exceed: true });
        }
      }

      if (EndTime) {
        EndTime.setErrors(null);
        if (!EndTime.value) {
          EndTime.setErrors({ required: true });
        }
        else if (StartDate.value && EndDate.value && String(StartDate.value._d) == String(EndDate.value._d) && StartTime.value && EndTime.value && EndTime.value < StartTime.value) {
          EndTime.setErrors({ exceed: true });
        }
      }
    }
  }

  onSave() {

    // this.selectionF.clear();
    // this.dataSource.data.forEach(row => this.selectionF.select(row));
    // console.log('this.this.selectionF:::::::::::::::::::::: ', this.selectionF);
    // const selectedItemsF: Array<{ id: number }> = this.selectionF.selected.map(item => ({ id: item.productId, salesPrice: item.salesPrice, limitBuyer: item.limitBuyer }));
    // console.log('this.this.selectionF:::::::::::::::::::::: ', selectedItemsF);

    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Warning',
        'Please make sure the form is complete and valid!',
        'warning'
      );
    }
    else {
      let model = this.form.value;
      this.app.showOverlaySpinner(true);
      this.salesService.saveCoupon(model, this.CouponId)
        .subscribe(r => {
          this.app.showOverlaySpinner(false);
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            //this.router.navigate(['/app/sales/promotion-list']);
          }
          else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }

  }

  getStatusList() {
    this.salesService.getEnableList().subscribe(items => {
      this.statusList = items;
    });
  }

  getTypeList() {
    this.salesService.getCouponTypeList().subscribe(items => {
      this.typeList = items;
    });
  }



  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.productId + 1}`;
  }


  deleteProduct() {
    //this.selectionF.clear();
    //this.dataSource.data.forEach(row => this.selectionF.select(row));
    console.log('this.this.selectionF:::::::::::::::::::::: ', this.selectionF);
    //const selectedItemsF: Array<{ id: number }> = this.selectionF.selected.map(item => ({ id: item.CouponId, enable: item.Enable }));
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.productId }));
    //console.log('selectedItemsF :::::::::::::::::::::: ', selectedItemsF);

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.app.showOverlaySpinner(true);
        this.salesService.deleteCouponProduct(selectedItems, this.CouponId)
          .subscribe(resultDelete => {
            this.app.showOverlaySpinner(false);
            if (resultDelete.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultDelete.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultDelete.ResponseMessage,
                'success'
              );
            }
            this.getProductList();
          });
      }
    })
  }


  addPorductCoupon(): void {
    const dialogRef = this.dialog.open(SelectCouponProductComponent, { data: { CouponId: this.CouponId } });
    dialogRef.afterClosed()
      .subscribe(ok => {
        this.getProductList();
      });
  }


  getProductList(event?: LazyLoadEvent) {
    console.log('getPromotionList ::::::::::::::::::::: ');
    if (this.CouponId > 0) {
      this.app.showOverlaySpinner(true);
      this.salesService.getProductListByPromotion(this.pageSetting.getPagerSetting(this.paginator, event
        , this.primengTableHelper.getSorting(this.dataTable)
      ), this.CouponId)
        .subscribe(items => {
          this.app.showOverlaySpinner(false);
          console.log('getOrderList items ::::::::::::::::::::: ', items);
          this.dataSource.data = items.list;
          this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
          this.primengTableHelper.records = items.list;
          if (event! && (event.filters || event.sortField)) {
            this.paginator.changePage(0);
          }
          this.primengTableHelper.hideLoadingIndicator();
        });
      this.primengTableHelper.showLoadingIndicator();
    }
  }


}

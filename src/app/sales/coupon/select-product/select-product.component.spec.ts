import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SelectCouponProductComponent } from './select-product.component';

describe('SelectCouponProductComponent', () => {
  let component: SelectCouponProductComponent;
  let fixture: ComponentFixture<SelectCouponProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelectCouponProductComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectCouponProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

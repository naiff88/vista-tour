import { Component, OnInit, ViewChild, Input, Inject, Injectable } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
//import { MatDialog, MatTableDataSource, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

// import { MatRippleModule } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
// import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { SalesService } from '../../sales.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { AppComponent } from 'src/app/app.component';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-sales-coupon-select-product',
  templateUrl: './select-product.component.html',
  styleUrls: ['./select-product.component.scss']
})

//@Injectable()
export class SelectCouponProductComponent implements OnInit {

  // @Input() orderStatus: string;
  // @Input() orderTabIndex: number;

  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  selectionF = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  //app: AppComponent;

  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  filterText = '';
  showSpinner: boolean = false;

  constructor(
    private salesService: SalesService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    //private app: AppComponent,
    public dialogRef: MatDialogRef<SelectCouponProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { CouponId: number },
    private router: Router) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
    //this.app = new AppComponent;
  }

  ngOnInit() {
    this.getProductList();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.productId + 1}`;
  }

  add() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.productId }));

    Swal.fire({
      title: 'Are you sure?',
      text: "The selected product will be added to the coupon product list!",
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Yes, add it!'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        this.salesService.addProductToCoupon(selectedItems, this.data.CouponId)
          .subscribe(resultAdd => {
            this.showSpinner = false;
            if (resultAdd.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultAdd.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultAdd.ResponseMessage,
                'success'
              );
              this.dialogRef.close();
            }
          });
      }
    })
  }
  
  getProductList(event?: LazyLoadEvent) {    
    this.showSpinner = true;
    this.salesService.getProductListAddCoupon(this.pageSetting.getPagerSetting(this.paginator, event
      , this.primengTableHelper.getSorting(this.dataTable)
    ),this.data.CouponId)
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
        this.primengTableHelper.hideLoadingIndicator();
      });
    this.primengTableHelper.showLoadingIndicator();
  }

}

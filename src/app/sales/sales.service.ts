import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { bufferTime, distinctUntilChanged, filter, map, scan, share, take, tap } from 'rxjs/operators';
import * as _ from 'lodash';
import { Pager, PageInfo } from '../../pagingnation';
import { API_BASE_URL } from 'src/shared/service-proxies/service-proxies';
import { ProgressBarState, Pair, ProductListColumn } from './models';

export enum SalesApiEndpoint {
  // ProductsList = '/api/finance/customer/list',
}

@Injectable({
  providedIn: 'root'
})
export class SalesService {
  private processingSubject = new Subject<ProgressBarState>();
  private baseUrl: string;
  http: HttpClient;

  //----------------------------- open API module SALES >> ORDER
  getOrderDetails(OrderId: number): Observable<any> {
    const params = new HttpParams().set('OrderId', String(OrderId));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    let record: any = {
      Name: 'Razman B. Md Zainal',
      OrderNo: '13313313',
      Address: '2477, Jalan Permata 19, Taman Permata, 53300 Kuala Lumpur. WP Kuala Lumpur',
      OrderDateTime: '02/01/2020 09:20 AM',
      Email: 'razman@gmail.com',
      PhoneNo: '60127381781',
      CourierId: 1,
      TrackingNo: '',
      ShippingCost: 3.5,
      StatusId: 2,
      PaymentTypeId: 3,
      TransactionNo: '57X78969NB701535K',
      PaymentDate: '20/01/2020 04.22 PM',
      Amount: 0,
    };
    return of(record);
  }


  getProductListByOrder(OrderId: number): Observable<any> {
    console.log('getOrderList ::::::::::::::::: ');
    const params = new HttpParams().set('OrderId', (OrderId || 0) + "")
      ;

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: ProductListColumn[] = [
      { position: 1, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Order 1', skuNo: 'XXXX', price: 12, quantity: 1, subTotal: 12 },
      { position: 2, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Order 2', skuNo: 'XXXX', price: 10, quantity: 5, subTotal: 50 },
    ];
    return of(list);
  }

  
  getOrderList(page: Pager, orderTabIndex: number): Observable<any> {
    console.log('getOrderList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      .set('orderStatusId', (orderTabIndex || 0) + "")
      ;
    console.log('params ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { orderId: 1, orderNo: '#123 Customer 12345 ', orderDateTime: '01/01/2020 12:20 AM', status: 'New', total: 1200 },
      { orderId: 2, orderNo: '#456 Customer 12345', orderDateTime: '02/01/2020 12:20 AM', status: 'Completed', total: 67 },
      { orderId: 3, orderNo: '#789 Customer 12345', orderDateTime: '03/01/2020 12:20 AM', status: 'New', total: 200 },
      { orderId: 4, orderNo: '#777 Customer 12345', orderDateTime: '04/01/2020 12:20 AM', status: 'New', total: 1900 },
    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }

  getStatusList(): Observable<any> {
    console.log('getOrderList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { id: 1, name: 'New' },
      { id: 2, name: 'Processed' },
      { id: 3, name: 'Completed' },
    ];
    return of(list);
  }

  getPaymentTypeList(): Observable<any> {
    console.log('getPaymentTypeList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { id: 1, name: 'Manual Bank Transfer' },
      { id: 2, name: 'BillPlz' },
      { id: 3, name: 'Paypal' },
    ];
    return of(list);
  }

  getCourierTypeList(): Observable<any> {
    console.log('getPaymentTypeList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { id: 1, name: 'Poslaju' },
      { id: 2, name: 'J&T Express' },
      { id: 3, name: 'DHL' },
    ];
    return of(list);
  }

  saveOrder(model, OrderId: number): Observable<any> {
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Order Details Saved!';
    const Id = OrderId;
    return of({ ReturnCode, ResponseMessage, Id });
  }
  //----------------------------- close API module SALES >> ORDER




  //----------------------------- open API module SALES >> PROMOTION
  getEnableList(): Observable<any> {
    console.log('getEnableList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { id: 1, name: 'Enabled' },
      { id: 0, name: 'Disabled' },
    ];
    return of(list);
  }

  getStateList(): Observable<any> {
    console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Kelantan' },
      { id: 2, name: 'Sabah' },
      { id: 3, name: 'Perlis' },
      { id: 4, name: 'Johor' },
      { id: 5, name: 'Kedah' },
      { id: 6, name: 'Melaka' },
      { id: 7, name: 'Negeri Sembilan' },
      { id: 8, name: 'Pahang' },
      { id: 9, name: 'Pulau Pinang' },
      { id: 10, name: 'Perak' },
      { id: 11, name: 'Sarawak' },
      { id: 12, name: 'Selangor' },
      { id: 13, name: 'Terengganu' },
      { id: 14, name: 'Kuala Lumpur' },
      { id: 15, name: 'Labuan' },
      { id: 16, name: 'Putrajaya' },
    ];
    return of(list);
  }

  getPromotionDetails(PromotionId: number): Observable<any> {
    const params = new HttpParams().set('PromotionId', String(PromotionId));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    let record: any = {
      Name: 'Hari Raya Aidilfitri',
      StartDate: '28 Jan 2020',
      StartTime: '09:00',
      EndDate: '28 Jan 2020',
      EndTime: '17:00',
      StatusId: 1,
      Description: 'Promotion Description',
    };
    return of(record);
  }

  getProductListAddPromotion(page: Pager, PromotionId: number): Observable<any> {
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      .set('PromotionId', (PromotionId || 0) + "")
      ;
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED  
    let list: any[] = [
      { productId: 1, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Product 1 Product 1 Product 1', categoryName: 'Categroy 1', skuNo: 'XXXX', variation: 'XXXXXX1, XXXXXXX2', normalPrice: 12, stock: 9 },
      { productId: 2, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Product 2', categoryName: 'Categroy 2', skuNo: '33333', variation: 'YYYYY', normalPrice: 12, stock: 34 },
      { productId: 3, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Product 3', categoryName: 'Categroy 2', skuNo: '33333', variation: 'YYYYY', normalPrice: 12, stock: 34 },
    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }

  getProductListByPromotion(page: Pager, PromotionId: number): Observable<any> {
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      .set('PromotionId', (PromotionId || 0) + "")
      ;
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED  
    let list: any[] = [
      { productId: 1, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Product 1 Product 1 Product 1', skuNo: 'XXXX', variation: 'XXXXXX1, XXXXXXX2', normalPrice: 12, salesPrice: 9.9, limitBuyer: 30 },
      { productId: 2, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Product 2', skuNo: '33333', variation: 'YYYYY', normalPrice: 12, salesPrice: 9.9, limitBuyer: 30 },
    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }

  savePromotion(model, PromotionId: number): Observable<any> {
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Promotion Details Saved!';
    const Id = 1;
    return of({ ReturnCode, ResponseMessage, Id });
  }

  enablePromotion(enable: number, PromotionId: number): Observable<any> {
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Enable Status Saved!';
    return of({ ReturnCode, ResponseMessage });
  }

  deletePromotionProduct(ids: Array<{ id: number }>, PromotionId: number) {
    console.log('deletePromotionProduct ids :::::::::::::::: ', ids);
    console.log('deletePromotionProduct PromotionId :::::::::::::::: ', PromotionId);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Promotion Product Deleted!';
    return of({ ReturnCode, ResponseMessage });
  }

  addProductToPromotion(ids: Array<{ id: number }>, PromotionId: number) {
    console.log('addProductToPromotion ids :::::::::::::::: ', ids);
    console.log('addProductToPromotion PromotionId :::::::::::::::: ', PromotionId);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Promotion Product added!';
    return of({ ReturnCode, ResponseMessage });
  }

  deletePromotion(ids: Array<{ id: number }>) {
    console.log('deletePromotion ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Promotion Campaign Deleted!';
    return of({ ReturnCode, ResponseMessage });
  }

  getPromotionList(page: Pager): Observable<any> {
    console.log('getOrderList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { PromotionId: 1, PromotionName: 'Hari Raya Aidilfitri 2020', DateStart: '01/01/2020', DateEnd: '05/01/2020', ProductCount: 2, Enable: 1 },
      { PromotionId: 2, PromotionName: 'Merdeka 2020', DateStart: '01/01/2020', DateEnd: '05/01/2020', ProductCount: 22, Enable: 0 },
    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }
  //----------------------------- close API module SALES >> PROMOTION



 //----------------------------- open API module SALES >> COUPON
 getCouponTypeList(): Observable<any> {
  console.log('getCouponTypeList ::::::::::::::::: ');
  /* API CALL */
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
    .pipe(
      map((resp: any) => {
        //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
        const list = resp.list;
        return list;
      }),
      tap({
        error: () => this.isBusy = false,
        complete: () => this.isBusy = false
      })
    );
  */
  // TEMPORARY HARDCODED
  let list: any = [
    { id: 1, name: 'Product Voucher' },
    { id: 2, name: 'Shop Voucher' },
  ];
  return of(list);
}

getCouponDetails(CouponId: number): Observable<any> {
  const params = new HttpParams().set('CouponId', String(CouponId));
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
    .pipe(
      map((resp: any) => {
        const model = resp.titleInfo as any;
        return model;
      }),
      tap({ complete: () => this.isBusy = false })
    );
    */
  let record: any = {
    Name: 'Coupon Name 1',
    Code: '11223344',
    StartDate: '28 Jan 2020',
    StartTime: '09:00',
    EndDate: '28 Jan 2020',
    EndTime: '17:00',
    StatusId: 1,
    TypeId: 1,
    Discount: 30.10,
    Quantity: 100,
    MinPurchase: 5,
    LimitPerBuyer: 1,
  };
  return of(record);
}

getProductListAddCoupon(page: Pager, CouponId: number): Observable<any> {
  const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
    .set('RowsPerPage', (page.RowsPerPage || 0) + "")
    .set('PageNumber', (page.PageNumber || 0) + "")
    .set('OrderScript', page.OrderScript || "")
    .set('ColumnFilterScript', page.ColumnFilterScript || "")
    .set('CouponId', (CouponId || 0) + "")
    ;
  /* API CALL */
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
    .pipe(
      map((resp: any) => {
        //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
        const list = resp.list;
        return list;
      }),
      tap({
        error: () => this.isBusy = false,
        complete: () => this.isBusy = false
      })
    );
  */
  // TEMPORARY HARDCODED  
  let list: any[] = [
    { productId: 1, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Product 1 Product 1 Product 1', categoryName: 'Categroy 1', skuNo: 'XXXX', variation: 'XXXXXX1, XXXXXXX2', normalPrice: 12, stock: 9 },
    { productId: 2, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Product 2', categoryName: 'Categroy 2', skuNo: '33333', variation: 'YYYYY', normalPrice: 12, stock: 34 },
    { productId: 3, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Product 3', categoryName: 'Categroy 2', skuNo: '33333', variation: 'YYYYY', normalPrice: 12, stock: 34 },
  ];
  let pageInfo: any = { RowCount: list.length }
  return of({ list, pageInfo });
}

saveCoupon(model, CouponId: number): Observable<any> {
  //CALLING API
  /*
  this.isBusy = true;
  model.Id = OrderId;
    return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
      .pipe(
        map((resp: any) => {
          //console.log('resp update ::::::::::::::: ', resp);
          const ReturnCode = resp.ReturnCode;
          const ResponseMessage = resp.ResponseMessage;
          const Id = staffId;            
          return { ReturnCode, ResponseMessage, Id };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
  */
  //HARDCODED
  const ReturnCode = 200;
  const ResponseMessage = 'Coupon Details Saved!';
  const Id = 1;
  return of({ ReturnCode, ResponseMessage, Id });
}

deleteCouponProduct(ids: Array<{ id: number }>, CouponId: number) {
  console.log('deleteCouponProduct ids :::::::::::::::: ', ids);
  console.log('deleteCouponProduct CouponId :::::::::::::::: ', CouponId);
  /*
  return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
     map((resp: any) => {
       const ReturnCode = resp.ReturnCode;
       const ResponseMessage = resp.ResponseMessage;
    return { ReturnCode, ResponseMessage };
     }),
     tap({
       error: () => this.isBusy = false,
       complete: () => this.isBusy = false
     })
   );
   */
  //HARDCODED
  const ReturnCode = 200;
  const ResponseMessage = 'Coupon Product Deleted!';
  return of({ ReturnCode, ResponseMessage });
}

addProductToCoupon(ids: Array<{ id: number }>, CouponId: number) {
  console.log('addProductToCoupon ids :::::::::::::::: ', ids);
  console.log('addProductToCoupon CouponId :::::::::::::::: ', CouponId);
  /*
  return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
     map((resp: any) => {
       const ReturnCode = resp.ReturnCode;
       const ResponseMessage = resp.ResponseMessage;
    return { ReturnCode, ResponseMessage };
     }),
     tap({
       error: () => this.isBusy = false,
       complete: () => this.isBusy = false
     })
   );
   */
  //HARDCODED
  const ReturnCode = 200;
  const ResponseMessage = 'Coupon Product added!';
  return of({ ReturnCode, ResponseMessage });
}

deleteCoupon(ids: Array<{ id: number }>) {
  console.log('deleteCoupon ids :::::::::::::::: ', ids);
  /*
  return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
     map((resp: any) => {
       const ReturnCode = resp.ReturnCode;
       const ResponseMessage = resp.ResponseMessage;
    return { ReturnCode, ResponseMessage };
     }),
     tap({
       error: () => this.isBusy = false,
       complete: () => this.isBusy = false
     })
   );
   */
  //HARDCODED
  const ReturnCode = 200;
  const ResponseMessage = 'Coupon Deleted!';

  return of({ ReturnCode, ResponseMessage });
}

getCouponList(page: Pager): Observable<any> {
  console.log('getCouponList ::::::::::::::::: ');
  const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
    .set('RowsPerPage', (page.RowsPerPage || 0) + "")
    .set('PageNumber', (page.PageNumber || 0) + "")
    .set('OrderScript', page.OrderScript || "")
    .set('ColumnFilterScript', page.ColumnFilterScript || "")
    ;
  console.log('params ::::::::::::::::: ', params);
  /* API CALL */
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
    , { params }
    )
    .pipe(
      map((resp: any) => {
        //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
        const list = resp.list;
        const pageInfo = resp.pageInfo;
        return { list, pageInfo };
      }),
      tap({
        error: () => this.isBusy = false,
        complete: () => this.isBusy = false
      })
    );
  */
  // TEMPORARY HARDCODED
  let list: any = [
    { CouponId: 1, CouponName: 'Coupon Name 1', CouponCode: 'XXXXYYYY', CouponType: 'Shop Voucher', DateStart: '01/01/2020', DateEnd: '05/01/2020', Quantity: 100, Used: 15, Discount: 15 },
    { CouponId: 2, CouponName: 'Coupon Name 2', CouponCode: 'KKKKMMMM', CouponType: 'Shop Voucher', DateStart: '01/01/2020', DateEnd: '05/01/2020', Quantity: 50, Used: 35, Discount: 5 },
    { CouponId: 3, CouponName: 'Coupon Name 3', CouponCode: '11223344', CouponType: 'Product Voucher', DateStart: '01/01/2020', DateEnd: '05/01/2020', Quantity: 100, Used: 67, Discount: 10 },

  ];
  let pageInfo: any = { RowCount: list.length }
  return of({ list, pageInfo });
}
 //----------------------------- close API module SALES >> COUPON



//----------------------------- open API module SALES >> CUSTOMERS  

 getCustomerList(page: Pager): Observable<any> {
  const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
    .set('RowsPerPage', (page.RowsPerPage || 0) + "")
    .set('PageNumber', (page.PageNumber || 0) + "")
    .set('OrderScript', page.OrderScript || "")
    .set('ColumnFilterScript', page.ColumnFilterScript || "")
    ;
  /* API CALL */
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
    .pipe(
      map((resp: any) => {
        //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
        const list = resp.list;
        return list;
      }),
      tap({
        error: () => this.isBusy = false,
        complete: () => this.isBusy = false
      })
    );
  */
  // TEMPORARY HARDCODED  
  let list: any[] = [
    { customerId: 1, customeImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', customeName: 'Razman B. Md Zainal Razman B. Md Zainal', dateJoined: '01/01/2020', customerGroup: 'Customer', phoneNo: '60129234567' },
    { customerId: 2, customeImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', customeName: 'Nik Fahmi', dateJoined: '01/01/2020', customerGroup: 'Customer', phoneNo: '60129234567' },
  ];
  let pageInfo: any = { RowCount: list.length }
  return of({ list, pageInfo });
}

deleteCustomer(ids: Array<{ id: number }>) {
  console.log('deleteCustomer ids :::::::::::::::: ', ids);
  /*
  return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
     map((resp: any) => {
       const ReturnCode = resp.ReturnCode;
       const ResponseMessage = resp.ResponseMessage;
    return { ReturnCode, ResponseMessage };
     }),
     tap({
       error: () => this.isBusy = false,
       complete: () => this.isBusy = false
     })
   );
   */
  //HARDCODED
  const ReturnCode = 200;
  const ResponseMessage = 'Customer Deleted!';

  return of({ ReturnCode, ResponseMessage });
}

saveCustomer(model, CustomerId: number): Observable<any> {
  //CALLING API
  /*
  this.isBusy = true;
  model.Id = OrderId;
    return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
      .pipe(
        map((resp: any) => {
          //console.log('resp update ::::::::::::::: ', resp);
          const ReturnCode = resp.ReturnCode;
          const ResponseMessage = resp.ResponseMessage;
          const Id = staffId;            
          return { ReturnCode, ResponseMessage, Id };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
  */
  //HARDCODED
  const ReturnCode = 200;
  const ResponseMessage = 'Customer Details Saved!';
  const Id = 1;
  return of({ ReturnCode, ResponseMessage, Id });
}

getCustomerDetails(CustomerId: number): Observable<any> {
  const params = new HttpParams().set('CustomerId', String(CustomerId));
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
    .pipe(
      map((resp: any) => {
        const model = resp.titleInfo as any;
        return model;
      }),
      tap({ complete: () => this.isBusy = false })
    );
    */
  let record: any = {
    Name: 'Razman B. Md Zainal',
    ImageUrl: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg',
    Email: 'razman@structcommerce.com.my',
    PhoneNo: '60124738743',
    StatusId: 1,
  };
  return of(record);
}

getCustomerOrderList(page: Pager, CustomerId: number): Observable<any> {
  console.log('getOrderList ::::::::::::::::: ');
  const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
    .set('RowsPerPage', (page.RowsPerPage || 0) + "")
    .set('PageNumber', (page.PageNumber || 0) + "")
    .set('OrderScript', page.OrderScript || "")
    .set('ColumnFilterScript', page.ColumnFilterScript || "")
    .set('CustomerId', (CustomerId || 0) + "")
    ;
  console.log('params ::::::::::::::::: ', params);
  /* API CALL */
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
    , { params }
    )
    .pipe(
      map((resp: any) => {
        //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
        const list = resp.list;
        const pageInfo = resp.pageInfo;
        return { list, pageInfo };
      }),
      tap({
        error: () => this.isBusy = false,
        complete: () => this.isBusy = false
      })
    );
  */
  // TEMPORARY HARDCODED
  let list: any = [
    { orderId: 1, orderNo: '#123', orderDateTime: '01/01/2020 12:20 AM', status: 'New', total: 1200 },
    { orderId: 2, orderNo: '#456', orderDateTime: '02/01/2020 12:20 AM', status: 'Completed', total: 67 },
    { orderId: 3, orderNo: '#789', orderDateTime: '03/01/2020 12:20 AM', status: 'New', total: 200 },
    { orderId: 4, orderNo: '#777', orderDateTime: '04/01/2020 12:20 AM', status: 'New', total: 1900 },
    { orderId: 5, orderNo: '#123', orderDateTime: '01/01/2020 12:20 AM', status: 'New', total: 1200 },
    { orderId: 6, orderNo: '#456', orderDateTime: '02/01/2020 12:20 AM', status: 'Completed', total: 67 },
    { orderId: 7, orderNo: '#789', orderDateTime: '03/01/2020 12:20 AM', status: 'New', total: 200 },
    { orderId: 8, orderNo: '#777', orderDateTime: '04/01/2020 12:20 AM', status: 'New', total: 1900 },
  ];
  let pageInfo: any = { RowCount: list.length }
  return of({ list, pageInfo });
}


getAddressList(CustomerId: number): Observable<any> {
  console.log('getOrderList ::::::::::::::::: ');
  const params = new HttpParams().set('CustomerId', (CustomerId || 0) + "")
    ;
  console.log('params ::::::::::::::::: ', params);
  /* API CALL */
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
    , { params }
    )
    .pipe(
      map((resp: any) => {
        //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
        const list = resp.list;
        const pageInfo = resp.pageInfo;
        return { list, pageInfo };
      }),
      tap({
        error: () => this.isBusy = false,
        complete: () => this.isBusy = false
      })
    );
  */
  // TEMPORARY HARDCODED
  let list: any = [
    { addressId: 1, name: 'Nik Ahmad Fahmi', phoneNo: '60129678782', address1: '2477, Jalan Permata 19,', address2: 'Taman Permata,', poscode: '53300', state: 'WP Kuala Lumpur', city: 'Kuala Lumpur'},
    { addressId: 2, name: 'Razman Md Zainal', phoneNo: '60129678782', address1: '2477, Jalan Permata 19,', address2: 'Taman Permata,', poscode: '53300', state: 'WP Kuala Lumpur', city: 'Kuala Lumpur'},
    { addressId: 3, name: 'Muhd Naiff', phoneNo: '60129678782', address1: '2477, Jalan Permata 19', address2: 'Taman Permata', poscode: '53300', state: 'WP Kuala Lumpur', city: 'Kuala Lumpur'},
  ];

  for (var i = 0; i < list.length; i++) {
    if(list[i].address1.trim().slice(-1) != ',' && list[i].address1.trim().slice(-1) != '.' )
    {
      list[i].address1 = list[i].address1.trim() + ","
    }

    if(list[i].address2.trim().slice(-1) != ',' && list[i].address2.trim().slice(-1) != '.' )
    {
      list[i].address2 = list[i].address2.trim() + ","
    }

    if(list[i].city.trim().slice(-1) != ',' && list[i].city.trim().slice(-1) != '.' )
    {
      list[i].city = list[i].city.trim() + ","
    }
  }
 
  return of(list);
}

getAddressDetails(AddressId: number): Observable<any> {
  const params = new HttpParams().set('AddressId', String(AddressId));
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
    .pipe(
      map((resp: any) => {
        const model = resp.titleInfo as any;
        return model;
      }),
      tap({ complete: () => this.isBusy = false })
    );
    */

  let record: any = {
    Name: 'Razman B. Md Zainal',
    PhoneNo: '60129678782',
    Address1: '2477, Jalan Permata 19,',
    Address2: 'Taman Permata,',
    Poscode: '53300',
    StateId: '14',
    City: 'Kuala Lumpur',
  };
  return of(record);
}

deleteAddress(ids: Array<{ id: number }>) {
  console.log('deleteCoupon ids :::::::::::::::: ', ids);
  /*
  return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
     map((resp: any) => {
       const ReturnCode = resp.ReturnCode;
       const ResponseMessage = resp.ResponseMessage;
    return { ReturnCode, ResponseMessage };
     }),
     tap({
       error: () => this.isBusy = false,
       complete: () => this.isBusy = false
     })
   );
   */
  //HARDCODED
  const ReturnCode = 200;
  const ResponseMessage = 'Address Deleted!';

  return of({ ReturnCode, ResponseMessage });
}

saveAddress(model, AddressId: number): Observable<any> {
  //CALLING API
  /*
  this.isBusy = true;
  model.Id = OrderId;
    return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
      .pipe(
        map((resp: any) => {
          //console.log('resp update ::::::::::::::: ', resp);
          const ReturnCode = resp.ReturnCode;
          const ResponseMessage = resp.ResponseMessage;
          const Id = staffId;            
          return { ReturnCode, ResponseMessage, Id };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
  */
  //HARDCODED
  const ReturnCode = 200;
  const ResponseMessage = 'Address Details Saved!';
  const Id = AddressId;
  return of({ ReturnCode, ResponseMessage, Id });
}

//----------------------------- close API module SALES >> CUSTOMERS

  


//----------------------------- open API module SALES >> RATINGS  

getRatingList(page: Pager): Observable<any> {
  console.log('getCouponList ::::::::::::::::: ');
  const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
    .set('RowsPerPage', (page.RowsPerPage || 0) + "")
    .set('PageNumber', (page.PageNumber || 0) + "")
    .set('OrderScript', page.OrderScript || "")
    .set('ColumnFilterScript', page.ColumnFilterScript || "")
    ;
  console.log('params ::::::::::::::::: ', params);
  /* API CALL */
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
    , { params }
    )
    .pipe(
      map((resp: any) => {
        //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
        const list = resp.list;
        const pageInfo = resp.pageInfo;
        return { list, pageInfo };
      }),
      tap({
        error: () => this.isBusy = false,
        complete: () => this.isBusy = false
      })
    );
  */
  // TEMPORARY HARDCODED
  let list: any = [
    { RatingId: 1, CustomerName: 'Razman B. Md Zainal', Type: 'Q & A', Date: '01/01/2020', ProductName: 'Laptop Acer', DateEnd: '05/01/2020', Rating: 0, Comments: 'Masih ada stok?' },
    { RatingId: 2, CustomerName: 'Nik Fahmi', Type: 'Review', Date: '01/01/2020', ProductName: 'Laptop Acer', DateEnd: '05/01/2020', Rating:3, Comments: 'Barang Terbaik' },
    { RatingId: 5, CustomerName: 'Muhd Naif', Type: 'Review', Date: '01/01/2020', ProductName: 'Laptop Acer', DateEnd: '05/01/2020', Rating:5, Comments: 'Cayalah' },

  ];
  let pageInfo: any = { RowCount: list.length }
  return of({ list, pageInfo });
}

deleteRating(ids: Array<{ id: number }>) {
  console.log('deleteCoupon ids :::::::::::::::: ', ids);
  /*
  return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
     map((resp: any) => {
       const ReturnCode = resp.ReturnCode;
       const ResponseMessage = resp.ResponseMessage;
    return { ReturnCode, ResponseMessage };
     }),
     tap({
       error: () => this.isBusy = false,
       complete: () => this.isBusy = false
     })
   );
   */
  //HARDCODED
  const ReturnCode = 200;
  const ResponseMessage = 'Rating & Review Deleted!';

  return of({ ReturnCode, ResponseMessage });
}

getReviewTypeList(): Observable<any> {
  console.log('getCouponTypeList ::::::::::::::::: ');
  /* API CALL */
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
    .pipe(
      map((resp: any) => {
        //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
        const list = resp.list;
        return list;
      }),
      tap({
        error: () => this.isBusy = false,
        complete: () => this.isBusy = false
      })
    );
  */
  // TEMPORARY HARDCODED
  let list: any = [
    { id: 1, name: 'Q & A' },
    { id: 2, name: 'Review' },
  ];
  return of(list);
}


getRatingStatusList(): Observable<any> {
  console.log('getEnableList ::::::::::::::::: ');
  /* API CALL */
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
    .pipe(
      map((resp: any) => {
        //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
        const list = resp.list;
        return list;
      }),
      tap({
        error: () => this.isBusy = false,
        complete: () => this.isBusy = false
      })
    );
  */
  // TEMPORARY HARDCODED
  let list: any = [
    { id: 1, name: 'Show Review' },
    { id: 2, name: 'Hide Review' },
  ];
  return of(list);
}

getReviewDetails(RatingId: number): Observable<any> {
  const params = new HttpParams().set('RatingId', String(RatingId));
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
    .pipe(
      map((resp: any) => {
        const model = resp.titleInfo as any;
        return model;
      }),
      tap({ complete: () => this.isBusy = false })
    );
    */

   

  let record: any = {
    CustomerName: 'Coupon Name 1',
    Date: '28 Jan 2020',
    StatusId: 1,
    TypeId: 2,
    Comments: 'Barang ini sangat baik. Terima Kasih.',
    Reply: '',
    Rating: 4,
  };
  return of(record);
}

saveRating(model, CouponId: number): Observable<any> {
  //CALLING API
  /*
  this.isBusy = true;
  model.Id = OrderId;
    return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
      .pipe(
        map((resp: any) => {
          //console.log('resp update ::::::::::::::: ', resp);
          const ReturnCode = resp.ReturnCode;
          const ResponseMessage = resp.ResponseMessage;
          const Id = staffId;            
          return { ReturnCode, ResponseMessage, Id };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
  */
  //HARDCODED
  const ReturnCode = 200;
  const ResponseMessage = 'Review Details Saved!';
  const Id = 1;
  return of({ ReturnCode, ResponseMessage, Id });
}

getProductListByRating(page: Pager, RatingId: number): Observable<any> {
  const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
    .set('RowsPerPage', (page.RowsPerPage || 0) + "")
    .set('PageNumber', (page.PageNumber || 0) + "")
    .set('OrderScript', page.OrderScript || "")
    .set('ColumnFilterScript', page.ColumnFilterScript || "")
    .set('RatingId', (RatingId || 0) + "")
    ;
  /* API CALL */
  /*
  this.isBusy = true;
  return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
    .pipe(
      map((resp: any) => {
        //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
        const list = resp.list;
        return list;
      }),
      tap({
        error: () => this.isBusy = false,
        complete: () => this.isBusy = false
      })
    );
  */
  // TEMPORARY HARDCODED  
  let list: any[] = [
    { productId: 1, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Product 1 Product 1 Product 1', skuNo: 'XXXX', variation: 'XXXXXX1, XXXXXXX2', normalPrice: 12, salesPrice: 9.9, limitBuyer: 30 },
    { productId: 2, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Product 2', skuNo: '33333', variation: 'YYYYY', normalPrice: 12, salesPrice: 9.9, limitBuyer: 30 },
  ];
  let pageInfo: any = { RowCount: list.length }
  return of({ list, pageInfo });
}
  //----------------------------- close API module SALES >> RATINGS  

  

  


  


  

  

  

  



  

  

  

  


  

  


  

  


  isBusyStream: Observable<boolean> = this.processingSubject.asObservable()
    .pipe(
      bufferTime(300),
      filter(states => states.length > 0),
      scan((total: number, states: ProgressBarState[]) => total + _.sum(states), 0),
      map((diff: number) => diff !== 0),
      distinctUntilChanged(),
      share()
    );

  set isBusy(value: boolean) {
    const state: number = value ? ProgressBarState.Show : ProgressBarState.Hide;
    this.processingSubject.next(state);
  }

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }




}

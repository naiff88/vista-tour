import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SelectPromotionProductComponent } from './select-product.component';

describe('SelectPromotionProductComponent', () => {
  let component: SelectPromotionProductComponent;
  let fixture: ComponentFixture<SelectPromotionProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelectPromotionProductComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPromotionProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

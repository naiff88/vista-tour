import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalesRoutingModule } from './sales-routing.module';
// import {
//   MatButtonModule,
//   MatCheckboxModule,
//   MatDialogModule,
//   MatIconModule,
//   MatProgressBarModule,
//   MatSelectModule,
//   MatSlideToggleModule,
//   MatSnackBarModule,
//   MatSortModule,
//   MatTableModule,
//   MatTabsModule,
//   MatDatepickerModule,
//   MatFormFieldModule,
//   MatInputModule,
//   MatRadioModule,
//   MatProgressSpinnerModule,
//   MAT_DATE_LOCALE
// } from '@angular/material';

import { MatRippleModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
import { MatIconModule} from '@angular/material/icon';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort} from '@angular/material/sort';

//import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { JoditAngularModule } from 'jodit-angular';
import { AppCommonModule } from '../shared/app-common.module';
import { QuillModule } from 'ngx-quill';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MiscModule } from '../misc/misc.module';
import { PMServiceProxy } from 'src/shared/service-proxies/service-proxies';
import { getQuillEditorSettings } from 'src/shared/helpers/quill-helper';
import { NgxPrintModule } from 'ngx-print';
import { OrderListComponent } from './orders/list/list.component';
import { PromotionListComponent } from './promotions/list/list.component';
import { PromotionDetailsComponent } from './promotions/promotion-details/promotion-details.component';
import { TabListComponent } from './orders/list/tab-list/tab-list.component';
import { OrderDetailsComponent } from './orders/order-details/order-details.component';
import { SelectPromotionProductComponent } from './promotions/select-product/select-product.component';
import { CouponListComponent } from './coupon/list/list.component';
import { CouponDetailsComponent } from './coupon/coupon-details/coupon-details.component';
import { SelectCouponProductComponent } from './coupon/select-product/select-product.component';
import { AppModule } from '../app.module';
import { DashboardModule } from '../dashboard/dashboard.module';
import { CustomerListComponent } from './customers/list/list.component';
import { CustomerDetailsComponent } from './customers/customer-details/customer-details.component';
import { PurchaseListComponent } from './customers/customer-details/purchase-list/purchase-list.component';
import { AddressListComponent } from './customers/customer-details/address-list/address-list.component';
import { DialogCustomerComponent } from './customers/customer-details/dialog/dialog.component';
import { CustomerAddressDetailsComponent } from './customers/customer-details/address-details/address-details.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { RatingListComponent } from './rating/list/list.component';
import { RatingDetailsComponent } from './rating/rating-details/rating-details.component';

//import { TwoDigitDecimaNumberDirective } from '../shared/validators/currency-input';
// const materialModules = [
//   MatButtonModule,
//   MatTableModule,
//   MatCheckboxModule,
//   MatDialogModule,
//   MatSortModule,
//   MatTabsModule,
//   MatSlideToggleModule,
//   MatSelectModule,
//   MatProgressBarModule,
//   MatIconModule,
//   MatSnackBarModule,
//   MAT_DIALOG_DATA,
//   MatDialogRef
// ];

@NgModule({
  declarations: [
    OrderListComponent,
    PromotionDetailsComponent,
    TabListComponent,
    OrderDetailsComponent,
    PromotionListComponent,
    CouponListComponent,
    CouponDetailsComponent,
    SelectPromotionProductComponent,
    SelectCouponProductComponent,
    CustomerListComponent,
    CustomerDetailsComponent,
    PurchaseListComponent,
    DialogCustomerComponent,
    AddressListComponent,
    CustomerAddressDetailsComponent,
    RatingListComponent,
    RatingDetailsComponent
    //TwoDigitDecimaNumberDirective
    //MatSpinnerOverlayComponent,
  ],
  imports: [
    DashboardModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppCommonModule,
    JoditAngularModule,
    SalesRoutingModule,
    QuillModule.forRoot(getQuillEditorSettings()),
    TableModule,
    PaginatorModule,
    ModalModule.forRoot(),
    MatDatepickerModule,
    MatMomentDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSortModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatIconModule,
    MatSnackBarModule,
    MiscModule,
    NgxPrintModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    ImageCropperModule,
    //TwoDigitDecimaNumberDirective
    //MatSpinnerOverlayComponent
  ],
  entryComponents: [
    SelectPromotionProductComponent,
    SelectCouponProductComponent,
    DialogCustomerComponent,
    CustomerAddressDetailsComponent,
    //OrderDetailsComponent, 
    // PurchaseListComponent,
    // CustomerDetailsComponent,
    // InvoiceNewItemComponent,
    // InvoicePreviewComponent,
    // InvoiceNewPaymentComponent,
    // InvoiceViewReceiptComponent
  ],
  // providers: [PMServiceProxy],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    //{ provide: AppModule }
  ]
})
export class SalesModule {
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengumumanListComponent } from './list.component';

describe('PengumumanListComponent', () => {
  let component: PengumumanListComponent;
  let fixture: ComponentFixture<PengumumanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengumumanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengumumanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

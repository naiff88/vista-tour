import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengumumanDetailsComponent } from './pengumuman-details.component';

describe('PengumumanDetailsComponent', () => {
  let component: PengumumanDetailsComponent;
  let fixture: ComponentFixture<PengumumanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengumumanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengumumanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

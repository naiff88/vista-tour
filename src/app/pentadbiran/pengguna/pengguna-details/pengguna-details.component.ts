/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import { PentadbiranService } from '../../pentadbiran.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import Swal from 'sweetalert2';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-pentadbiran-kumpulan-details',
  templateUrl: './pengguna-details.component.html',
  styleUrls: ['./pengguna-details.component.scss']
})

export class PenggunaDetailsComponent implements OnInit {

  displayedColumns: string[] = ['select', 'projectName', 'manage'];
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  showSpinner: boolean = false;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;
  filterText = '';
  dataToPass: any = [];
  searchKey: string;
  submitted = false;
  StatusId: number = 0;
  jabatanId: number = 0;
  jabatanList: any = [];
  PermissionItemList: any = [];
  Id: number;

  // PERFECT SCROLL BAR
  public type: string = 'component';
  public disabled: boolean = false;
  public config: PerfectScrollbarConfigInterface = {};
  @ViewChild(PerfectScrollbarComponent, {static: true}) componentRef?: PerfectScrollbarComponent;

  files: File;
  form: FormGroup;

  constructor(
    private service: PentadbiranService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private snackBar: MatSnackBar,
    private router: Router,
    private app: AppComponent,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });
    this.getJabatanList();
    this.loadDetails(this.Id);
  }

  loadDetails(Id) {
    this.form = this.buildFormItems();
    // console.log('this.staffID ::::::::::::: ', this.staffId);
    if (Id > 0) {
      this.showSpinner = true;
      this.service.getPenggunaDetails(Id)
          .subscribe(result => {
            this.showSpinner = false;
            this.jabatanId = result.jabatanId > 0 ? result.jabatanId : 0;
            this.form.patchValue(result);
          });
    }
  }

  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.app.showOverlaySpinner(true);

      console.log('kumpulan-->', model);

      this.service.savePengguna(model, this.Id)
        .subscribe(r => {
          this.app.showOverlaySpinner(false);
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.router.navigate(['/app/pentadbiran/pengguna-list']);
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }

  getJabatanList() {
    console.log('getJabatanList ::::::::::::::::::::: ');
    this.service.getJabatanList().subscribe(items => {
      console.log('getJabatanList items ::::::::::::::::::::: ', items);
      this.jabatanList = items;
    });
  }

  // open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const UserName = formGroup.controls['userName'];
      const FullName = formGroup.controls['fullName'];
      const Email = formGroup.controls['email'];
      const KataLaluan = formGroup.controls['password'];
      const SahKataLaluan = formGroup.controls['confirmPassword'];
      const Status = formGroup.controls['jabatanId'];

      if (UserName) {
        UserName.setErrors(null);
        if (!UserName.value) {
          UserName.setErrors({ required: true });
        }
      }

      if (FullName) {
        FullName.setErrors(null);
        if (!FullName.value) {
          FullName.setErrors({ required: true });
        }
      }

      if (Email) {
        Email.setErrors(null);
        if (!Email.value) {
          Email.setErrors({ required: true });
        }
      }

      if (KataLaluan && (this.FormType == 'add')) {
        KataLaluan.setErrors(null);
        if (!KataLaluan.value) {
          KataLaluan.setErrors({ required: true });
        }
        else if (KataLaluan.value
            && SahKataLaluan.value && SahKataLaluan.value !== KataLaluan.value
        ) {
          KataLaluan.setErrors({ notSame: true });
        }
      }

      if (SahKataLaluan && (this.FormType == 'add')) {
        SahKataLaluan.setErrors(null);
        if (!SahKataLaluan.value) {
          SahKataLaluan.setErrors({ required: true });
        }
        else if (SahKataLaluan.value
            && KataLaluan.value && KataLaluan.value !== SahKataLaluan.value
        ) {
          SahKataLaluan.setErrors({ notSame: true });
        }
      }

      if (Status) {
        Status.setErrors(null);
        if (!Status.value) {
          Status.setErrors({ required: true });
        }
      }
    }
  }
  // close custom validation

  // open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: 'Rekod semasa akan ditetapkan semula!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  // close reset function

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      jabatanId: ['0', []],
      userName: ['', []],
      password: ['', []],
      confirmPassword: ['', []],
      fullName: ['',[]],
      email: ['',[]],
      userId: ['0', []],
      }, { validator: this.customValidation() }
    );
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenggunaDetailsComponent } from './pengguna-details.component';

describe('PenggunaDetailsComponent', () => {
  let component: PenggunaDetailsComponent;
  let fixture: ComponentFixture<PenggunaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenggunaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenggunaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

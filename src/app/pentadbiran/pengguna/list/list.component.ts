/* tslint:disable:max-line-length */
import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog} from '@angular/material/dialog';
import { MatTableDataSource} from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { PentadbiranService } from '../../pentadbiran.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import {FormBuilder, FormGroup} from '@angular/forms';
import Swal from 'sweetalert2';
import {MatSnackBar} from '@angular/material/snack-bar';
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class PenggunaListComponent implements OnInit {

  // @Input() orderStatus: string;
  @Input() orderTabIndex: number;


  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  form: FormGroup;
  showSpinner: boolean = false;
  filterText = '';
  dataToPass: any = [];
  submitted = false;
  onSearch = false;
  reVisit = false;
  filterValue: any;

  // initiate list
  statusList: any = [];

  // initiate list model
  statusId: number = 0;

  // initiate date model
  dateStart: any;
  dateEnd: any;

  // any unique name for searching list
  searchID = 'getPenggunaList';

  // for excel ganerator
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'

  constructor(
    private pentadbiranService: PentadbiranService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    if (this.pageSetting.getSearchFilterValueSV(this.searchID)) {
      this.reVisit = true;
    }
    this.getUserList();
    // this.getKumpulanList();
    this.getStatusList();
    this.loadDetails();
  }

  loadDetails() {
    this.form = this.buildFormItems();
  }

  // open standard function use for multiple delete in datatable
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.CustomerId + 1}`;
  }
  // close standard function use for multiple delete in datatable

  // penggunaDetails(penggunaId?: number): void {
  //   console.log('penggunaDetails kumpulanId ::::::::::::::: ',  penggunaId);
  //   this.router.navigate(['/app/pentadbiran/pengguna-details', penggunaId]);
  // }

  // open details page
  penggunaDetails(id?: number, type?: string): void {
    this.router.navigate(['/app/pentadbiran/pengguna-details', id, type]);
  }

// open standard listing with search function
  async getUserList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    await this.reloadSearchForm();
    await this.pentadbiranService.getUserList(this.pageSetting.getPagerSettingWithSearch(this.paginator, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          this.showSpinner = false;
          this.dataSource.data = items.list;
          this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
          this.primengTableHelper.records = items.list;
          if (this.onSearch) {
            this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
              duration: 3000,
              verticalPosition: 'bottom',
              horizontalPosition: 'end'
            });
            this.onSearch = false;
          }
          this.primengTableHelper.hideLoadingIndicator();
          if (event! && (event.filters || event.sortField)) {
            this.paginator.changePage(0);
          }
        });
    await this.primengTableHelper.showLoadingIndicator();
  }
  // close standard listing with search function

  getStatusList() {
    this.pentadbiranService.getStatusList().subscribe(items => {
      this.statusList = items;
    });
  }

  // open reload search form
  reloadSearchForm() {
    this.filterValue = this.pageSetting.getSearchFilterValueSV(this.searchID);
    let SFcurrent = this.pageSetting.getSearchFilterValueSF(this.searchID);
    this.form = this.buildFormItems();
    if (SFcurrent) {
      // reasing value for list & date model
      // this.Pangkat = SFcurrent.Pangkat > 0 ? SFcurrent.Pangkat : 0;
      // this.Status = SFcurrent.Status > 0 ? SFcurrent.Status : 0;
      this.dateStart = SFcurrent.TarikhMula = moment(SFcurrent.dateStart) || '';
      this.dateEnd = SFcurrent.TarikhAkhir = moment(SFcurrent.dateEnd) || '';
      this.form.patchValue(SFcurrent);
    } else {
      // initiate list & date model
      // this.Pangkat = 0;
      // this.Status = 0;
      this.dateStart = '';
      this.dateEnd = '';
    }
  }
  // close reload search form

  // search function
  search() {
    this.submitted = true;
    const searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
          'Amaran',
          'Pastikan maklumat carian adalah sah!',
          'warning'
      );
    }
    else {
      this.submitted = false;
      // setting store procedure listing field for filtering
      this.filterValue = {
        ' userName ': { value: searchForm.userName, matchMode: 'contains' },
        ' fullName ': { value: searchForm.fullName, matchMode: 'contains' },
        ' kumpulanName ': { value: searchForm.kumpulanName, matchMode: 'contains' },
        ' statusId ': { value: searchForm.statusId, matchMode: 'equals' },
        ' dateStart ': { value: searchForm.dateStart, matchMode: 'dateGreater' },
        ' dateEnd  ': { value: searchForm.dateEnd, matchMode: 'dateLower' },
      };
      this.pageSetting.setSearchFilterLS(this.filterValue, searchForm, this.searchID);
      this.onSearch = true;
      this.getUserList();
      this.paginator.changePage(0);
    }
  }

  // reset search form
  reset() {
    this.pageSetting.removeSearchFilterLS(this.searchID);
    this.form = this.buildFormItems();

    // initiate list & date model
    this.dateStart = '';
    this.dateEnd = '';

    this.filterValue = null;
    this.getUserList();
    this.paginator.changePage(0);
  }

  // customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      const dateStart = formGroup.controls['dateStart'];
      const dateEnd = formGroup.controls['dateEnd'];

      if (dateStart) {
        dateStart.setErrors(null);
        if (dateStart.value && dateEnd.value && dateEnd.value < dateStart.value) {
          dateStart.setErrors({exceed: true});
        }
      }
      if (dateEnd) {
        dateEnd.setErrors(null);
        if (dateStart.value && dateEnd.value && dateEnd.value < dateStart.value) {
          dateEnd.setErrors({exceed: true});
        }
      }
    }
  }

  // pdf genarator
  printTablePdf() {
    var doc = new jsPDF('p', 'pt');
    // Columns to display
    var col = ['No.', 'Nama Pengguna', 'Nama Penuh', 'kumpulan', 'Emel', 'Status', 'Tarikh Dicipta'];
    var rows = [];
    let i = 1;

    this.showSpinner = true;
    // get list of data
    this.pentadbiranService.getUserList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          this.showSpinner = false;
          let list =  items.list;
          for (var key in list) {
            rows.push([i, list[key].userName, list[key].fullName, list[key].group, list[key].email, list[key].status, list[key].dateCreated])
            i++;
          }
        });

    // report title
    var header = function (data) {
      doc.setFontSize(14);
      doc.setFontStyle('normal');
      doc.text('Laporan Pengguna', data.settings.margin.left, 50);
    };

    doc.autoTable(col, rows, { margin: { top: 80 }, beforePageContent: header });
    // create pdf file
    doc.save('LaporanPengguna.pdf');
  }

  // excel genarator
  printTableExcel() {
    var workbook = new Excel.Workbook();
    // Tab Title
    var worksheet = workbook.addWorksheet('Senarai Pengguna');
    // Columns to display
    const col = ['No.', 'Nama Pengguna', 'Nama Penuh', 'kumpulan', 'Emel', 'Status', 'Tarikh Dicipta'];
    worksheet.mergeCells('A1', 'F1'); // title merge cell
    worksheet.getCell('A1').value = 'Laporan Pengguna'; // title
    worksheet.getRow(2).values = col;
    // Column key
    worksheet.columns = [
      { key: 'No', width: 5 },
      { key: 'userName', width: 25 },
      { key: 'fullName', width: 25 },
      { key: 'group', width: 10 },
      { key: 'email', width: 10 },
      { key: 'status', width: 15 },
      { key: 'dateCreated', width: 15 },
    ];

    let i = 1;
    this.showSpinner = true;
    // get list of data
    this.pentadbiranService.getUserList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          this.showSpinner = false;
          let list =  items.list;
          for (var key in list) {
            worksheet.addRow([i, list[key].userName, list[key].fullName, list[key].group, list[key].email, list[key].status, list[key].dateCreated])
            i++;
          }
        });

    worksheet.eachRow(function (row, _rowNumber) {
      row.eachCell(function (cell, _colNumber) {
        cell.border = {
          top: { style: 'thin' },
          left: { style: 'thin' },
          bottom: { style: 'thin' },
          right: { style: 'thin' }
        };
      });
    });

    workbook.xlsx.writeBuffer().then(data => {
      const blob = new Blob([data], { type: this.blobType });
      // create excell file
      FileSaver.saveAs(blob, 'LaporanKumpulanPengguna.xlsx');
    });
  }

  // multiple delete function
  delete() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.Id }));
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: 'Rekod yang telah hapus tidak akan dikembalikan!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        this.pentadbiranService.deletePengguna(selectedItems)
            .subscribe(resultDelete => {
              this.showSpinner = false;
              if (resultDelete.ReturnCode == 204) {
                Swal.fire(
                    'Error',
                    resultDelete.ResponseMessage,
                    'error'
                );
              }
              else {
                Swal.fire(
                    '',
                    resultDelete.ResponseMessage,
                    'success'
                );
              }
              this.getUserList();
            });
      }
    })
  }


  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      userName: ['', []],
      fullName: ['', []],
      kumpulanId: ['0', []],
      kumpulanName: ['', []],
      statusId: ['0', []],
      dateStart: ['', []],
      dateEnd: ['', []],
        }, { validator: this.customValidation() }
    );
  }
}

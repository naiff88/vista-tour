import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenggunaListComponent } from './list.component';

describe('PenggunaListComponent', () => {
  let component: PenggunaListComponent;
  let fixture: ComponentFixture<PenggunaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenggunaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenggunaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

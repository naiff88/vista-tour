import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KompaunDiskaunListComponent } from './list.component';

describe('KompaunDiskaunListComponent', () => {
  let component: KompaunDiskaunListComponent;
  let fixture: ComponentFixture<KompaunDiskaunListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KompaunDiskaunListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KompaunDiskaunListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

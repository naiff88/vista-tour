import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JenisKenderaanDiskaunComponent } from './jenis-kenderaan-diskaun.component';

describe('JenisKenderaanDiskaunComponent', () => {
  let component: JenisKenderaanDiskaunComponent;
  let fixture: ComponentFixture<JenisKenderaanDiskaunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JenisKenderaanDiskaunComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JenisKenderaanDiskaunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

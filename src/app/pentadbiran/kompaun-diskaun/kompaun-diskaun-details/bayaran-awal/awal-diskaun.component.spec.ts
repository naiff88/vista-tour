import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwalDiskaunComponent } from './awal-diskaun.component';

describe('AwalDiskaunComponent', () => {
  let component: AwalDiskaunComponent;
  let fixture: ComponentFixture<AwalDiskaunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwalDiskaunComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwalDiskaunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

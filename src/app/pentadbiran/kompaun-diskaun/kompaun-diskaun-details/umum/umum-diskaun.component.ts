/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
// import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import { PentadbiranService } from '../../../pentadbiran.service';
import { Pager, PagerSetting } from '../../../../../pagingnation';
import {ProjectManagementUpsert as Project} from '@service-proxies';
import Swal from 'sweetalert2';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AppComponent } from '../../../../app.component';
import { ProductImage } from '../../../models';
import { Variation1Component } from '../../../../catalogue/products/modal/variation-1/variation-1.component';
import { Variation2Component } from '../../../../catalogue/products/modal/variation-2/variation-2.component';
import { PilihAhliComponent } from '../../../kumpulan/modal/pilih-ahli/pilih-ahli.component';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import * as moment from "moment";

@Component({
  selector: 'app-pentadbiran-kumpulan-details',
  templateUrl: './umum-diskaun.component.html',
  styleUrls: ['./umum-diskaun.component.scss']
})

export class UmumDiskaunComponent implements OnInit {

  displayedColumns: string[] = ['select', 'projectName', 'manage'];
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  showSpinner: boolean = false;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;
  filterText = '';
  dataToPass: any = [];
  searchKey: string;
  submitted = false;

  files: File;
  form: FormGroup;
  Id: number;
  statusId = 0;
  statusList: any = [];
  tarikhNotisDari: any;
  tarikhNotisHingga: any;
  tarikhDiskaunDari: any;
  tarikhDiskaunHingga: any;
  diskaunOlehId: number;
  jabatanList: any = [];
  selectedJabatan = [];
  jenisNotisList: any = [];
  selectedJenisNotis = [];

  // PERFECT SCROLL BAR
  public type = 'component';
  public disabled = false;
  public config: PerfectScrollbarConfigInterface = {};
  @ViewChild(PerfectScrollbarComponent, {static: true}) componentRef?: PerfectScrollbarComponent;

  constructor(
    private service: PentadbiranService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private snackBar: MatSnackBar,
    private router: Router,
    private app: AppComponent,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  get f() { return this.form.controls; }
  dropdownSettings1: IDropdownSettings = {};
  dropdownSettings2: IDropdownSettings = {};

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });
    this.getStatusList();
    this.getJabatanList();
    this.getJenisNotisList();
    this.dropdownSettings1 = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
    this.dropdownSettings2 = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 4,
      allowSearchFilter: true
    };
    this.loadDetails(this.Id);
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.service.getDiskaunumumDetails(Id)
          .subscribe(result => {
            this.showSpinner = false;
            this.diskaunOlehId = result.diskaunOlehId > 0 ? result.diskaunOlehId : 0;
            this.statusId = result.statusId > 0 ? result.statusId : 0;
            this.tarikhNotisDari = result.tarikhNotisDari = moment(result.tarikhNotisDari) || '';
            this.tarikhNotisHingga = result.tarikhNotisHingga = moment(result.tarikhNotisHingga) || '';
            this.tarikhDiskaunDari = result.tarikhDiskaunDari = moment(result.tarikhDiskaunDari) || '';
            this.tarikhDiskaunHingga = result.tarikhDiskaunHingga = moment(result.tarikhDiskaunHingga) || '';
            this.form.patchValue(result);
          });
    }
  }

  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.app.showOverlaySpinner(true);

      console.log('modelIklan-->', model);

      this.service.saveDiskaunUmum(model, this.Id)
        .subscribe(r => {
          this.app.showOverlaySpinner(false);
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.router.navigate(['/app/pentadbiran/kompaun-diskaun-list']);
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }

  getStatusList() {
    console.log('getStatusList ::::::::::::::::::::: ');
    this.service.getStatusList().subscribe(items => {
      console.log('getStatusList items ::::::::::::::::::::: ', items);
      this.statusList = items;
    });
  }

  getJabatanList() {
    console.log('getJabatanList ::::::::::::::::::::: ');
    this.service.getJabatanList().subscribe(items => {
      console.log('getJabatanList items ::::::::::::::::::::: ', items);
      this.jabatanList = items;
    });
  }

  getJenisNotisList() {
    console.log('getJenisNotisList ::::::::::::::::::::: ');
    this.service.getJenisNotisList().subscribe(items => {
      console.log('getJenisNotisList items ::::::::::::::::::::: ', items);
      this.jenisNotisList = items;
    });
  }

  // onCancel() {
  //   this.router.navigate(['/app/pentadbiran/data-induk/kenderaan/jenama-kenderaan-list']);
  // }

  // open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const NamaDiskaun = formGroup.controls['namaDiskaun'];
      const DiskaunOleh = formGroup.controls['diskaunOlehId'];
      const Rate = formGroup.controls['rate'];
      const TarikhNotisDari = formGroup.controls['tarikhNotisDari'];
      const TarikhNotisHingga = formGroup.controls['tarikhNotisHingga'];
      const TarikhDiskaunDari = formGroup.controls['tarikhDiskaunDari'];
      const TarikhDiskaunHingga = formGroup.controls['tarikhDiskaunHingga'];
      const Status = formGroup.controls['statusId'];

      if (NamaDiskaun) {
        NamaDiskaun.setErrors(null);
        if (!NamaDiskaun.value) {
          NamaDiskaun.setErrors({ required: true });
        }
      }

      if (DiskaunOleh) {
        DiskaunOleh.setErrors(null);
        if (!DiskaunOleh.value) {
          DiskaunOleh.setErrors({ required: true });
        }
      }

      if (Rate) {
        Rate.setErrors(null);
        if (!Rate.value) {
          Rate.setErrors({ required: true });
        }
      }

      if (TarikhNotisDari) {
        TarikhNotisDari.setErrors(null);
        if (!TarikhNotisDari.value) {
          TarikhNotisDari.setErrors({ required: true });
        }
        else if (TarikhNotisDari.value && TarikhNotisHingga.value && TarikhNotisHingga.value < TarikhNotisDari.value) {
          TarikhNotisDari.setErrors({ exceed: true });
        }
      }

      if (TarikhNotisHingga) {
        TarikhNotisHingga.setErrors(null);
        if (!TarikhNotisHingga.value) {
          TarikhNotisHingga.setErrors({ required: true });
        }
        else if (TarikhNotisDari.value && TarikhNotisHingga.value && TarikhNotisHingga.value < TarikhNotisDari.value) {
          TarikhNotisHingga.setErrors({ exceed: true });
        }
      }

      if (TarikhDiskaunDari) {
        TarikhDiskaunDari.setErrors(null);
        if (!TarikhDiskaunDari.value) {
          TarikhDiskaunDari.setErrors({ required: true });
        }
        else if (TarikhDiskaunDari.value && TarikhDiskaunHingga.value && TarikhDiskaunHingga.value < TarikhDiskaunDari.value) {
          TarikhDiskaunDari.setErrors({ exceed: true });
        }
      }

      if (TarikhDiskaunHingga) {
        TarikhDiskaunHingga.setErrors(null);
        if (!TarikhDiskaunHingga.value) {
          TarikhDiskaunHingga.setErrors({ required: true });
        }
        else if (TarikhDiskaunDari.value && TarikhDiskaunHingga.value && TarikhDiskaunHingga.value < TarikhDiskaunDari.value) {
          TarikhDiskaunHingga.setErrors({ exceed: true });
        }
      }

      if (Status) {
        Status.setErrors(null);
        if (!Status.value) {
          Status.setErrors({ required: true });
        }
      }
    }
  }
  // close custom validation

  // open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: 'Rekod semasa akan ditetapkan semula!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  // close reset function

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      statusId: ['0',[]],
      namaDiskaun: ['', []],
      diskaunOlehId: ['0',[]],
      tarikhNotisDari: ['', []],
      tarikhNotisHingga: ['', []],
      tarikhDiskaunDari: ['', []],
      tarikhDiskaunHingga: ['', []],
      rate: ['', []],
      }, { validator: this.customValidation() }
    );
  }

}

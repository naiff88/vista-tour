import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumDiskaunComponent } from './umum-diskaun.component';

describe('UmumDiskaunComponent', () => {
  let component: UmumDiskaunComponent;
  let fixture: ComponentFixture<UmumDiskaunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumDiskaunComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumDiskaunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

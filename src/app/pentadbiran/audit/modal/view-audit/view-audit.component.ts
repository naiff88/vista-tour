/* tslint:disable:max-line-length */
import {Component, Inject, Input, OnInit, ViewChild} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
// import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';

// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import { PentadbiranService } from '../../../pentadbiran.service';
import { Pager, PagerSetting } from '../../../../../pagingnation';
import {ProjectManagementUpsert as Project} from '@service-proxies';
import Swal from 'sweetalert2';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AppComponent } from '../../../../app.component';

@Component({
  selector: 'app-pentadbiran-audit-view',
  templateUrl: './view-audit.component.html',
  styleUrls: ['./view-audit.component.scss']
})

// @Injectable()
export class ViewAuditComponent implements OnInit {

  @Input() orderStatus: string;
  @Input() orderTabIndex: number;

  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  selectionF = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  // app: AppComponent;

  @ViewChild('dataTable') dataTable: Table;
  @ViewChild('paginator') paginator: Paginator;

  form: FormGroup;
  showSpinner: boolean = false;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;
  filterText = '';
  dataToPass: any = [];
  searchKey: string;
  submitted = false;

  tindakanId: number = 1;
  tindakanList: string [];
  modulId: number = 1;
  modulList: string [];
  productList: any = [];
  productListDisplayedColumns: string[] = ['position', 'bidangData', 'valueBaru', 'valueLama'];


  constructor(
    private pentadbiranService: PentadbiranService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    // private app: AppComponent,
    public dialogRef: MatDialogRef<ViewAuditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { AuditId: number },
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
    // this.app = new AppComponent;
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.getUserList();
    this.getTindakanList();
    this.getModulList();
    this.getProductListByOrder(this.data.AuditId);
    this.loadDetails(this.data.AuditId);
    // this.service.getDocumentTypeList();
  }

  loadDetails(AuditId) {
    this.form = this.buildFormItems();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.productId + 1}`;
  }

  add() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.productId }));
    console.log('add selectedItems :::::::::::::::::::::: ', selectedItems);

    Swal.fire({
      title: 'Adakan Anda Pasti?',
      text: 'Pilihan Akan Ditambah Kedalam Kumpulan Pengguna!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Ya!'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        // this.app.showOverlaySpinner(true);
        this.pentadbiranService.addUserToUserGroup(selectedItems, this.data.AuditId)
          .subscribe(resultAdd => {
            this.showSpinner = false;
            // this.app.showOverlaySpinner(false);
            if (resultAdd.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultAdd.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultAdd.ResponseMessage,
                'success'
              );
              this.dialogRef.close();
            }
            // this.getProductList();
          });
      }
    })
  }

  getUserList(event?: LazyLoadEvent) {
    console.log('getUserList ::::::::::::::::::::: ');
    // this.app.showOverlaySpinner(true);
    this.showSpinner = true;
    this.pentadbiranService.getUserGroupListAddUser(this.pageSetting.getPagerSetting(this.paginator, event
      , this.primengTableHelper.getSorting(this.dataTable)
    ),this.data.AuditId)
      .subscribe(items => {
        this.showSpinner = false;
        console.log('getUserList items ::::::::::::::::::::: ', items);
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        // if (event! && (event.filters || event.sortField)) {
        //   this.paginator.changePage(0);
        // }
        this.primengTableHelper.hideLoadingIndicator();
      });
    this.primengTableHelper.showLoadingIndicator();
  }

  getTindakanList() {
    // console.log('getAktaIndukList ::::::::::::::::::::: ');
    this.pentadbiranService.getAuditList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.tindakanList = items.list as string [];
      console.log('getTindakanList---> ', this.tindakanList);
    });
  }

  getModulList() {
    // console.log('getAktaIndukList ::::::::::::::::::::: ');
    this.pentadbiranService.getAuditList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.modulList = items.list as string [];
      console.log('getModulList---> ', this.modulList);
    });
  }

  getProductListByOrder(OrderId) {
    // console.log('getStatusList ::::::::::::::::::::: ');
    this.pentadbiranService.getProductListByOrder(OrderId).subscribe(items => {
      console.log('getProductListByOrder items ::::::::::::::::::::: ', items);
      this.productList = items;
      console.log('length---', items.length);
    });
  }

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
          tindakanId: ['0', []],
          modulId: ['0', []],
          dateRecorded: ['1/1/2018', []],
          keyword: ['Laporan Kemasukan Data DDE - Bulanan', []],
          userName: ['Ali', []],
        }, {}
    );
  }

}

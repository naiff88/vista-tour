import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PangkatPegawaiListComponent } from './list.component';

describe('PangkatPegawaiListComponent', () => {
  let component: PangkatPegawaiListComponent;
  let fixture: ComponentFixture<PangkatPegawaiListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PangkatPegawaiListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PangkatPegawaiListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

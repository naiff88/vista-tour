import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PangkatPegawaiDetailsComponent } from './pangkat-pegawai-details.component';

describe('KawasanDetailsComponent', () => {
  let component: PangkatPegawaiDetailsComponent;
  let fixture: ComponentFixture<PangkatPegawaiDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PangkatPegawaiDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PangkatPegawaiDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AktaIndukDetailsComponent } from './akta-induk-details.component';

describe('PenggunaDetailsComponent', () => {
  let component: AktaIndukDetailsComponent;
  let fixture: ComponentFixture<AktaIndukDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AktaIndukDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AktaIndukDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarianIndukDetailsComponent } from './carian-induk-details.component';

describe('CarianIndukDetailsComponent', () => {
  let component: CarianIndukDetailsComponent;
  let fixture: ComponentFixture<CarianIndukDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarianIndukDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarianIndukDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

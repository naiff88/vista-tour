/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
// import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import { PentadbiranService } from '../../../pentadbiran.service';
import { Pager, PagerSetting } from '../../../../../pagingnation';
import {ProjectManagementUpsert as Project} from '@service-proxies';
import Swal from 'sweetalert2';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AppComponent } from '../../../../app.component';
import { ProductImage } from '../../../models';
import { Variation1Component } from '../../../../catalogue/products/modal/variation-1/variation-1.component';
import { Variation2Component } from '../../../../catalogue/products/modal/variation-2/variation-2.component';
import { PilihAhliComponent } from '../../../kumpulan/modal/pilih-ahli/pilih-ahli.component';

@Component({
  selector: 'app-pentadbiran-kumpulan-details',
  templateUrl: './carian-induk-details.component.html',
  styleUrls: ['./carian-induk-details.component.scss']
})

export class CarianIndukDetailsComponent implements OnInit {

  displayedColumns: string[] = ['select', 'projectName', 'manage'];
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  showSpinner: boolean = false;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;
  filterText = '';
  dataToPass: any = [];
  searchKey: string;
  submitted = false;

  StatusId: number = 0;
  jabatanId: number = 0;
  jabatanList: any = [];
  jenisId: number = 0;
  jenisList: any = [];
  statusId: number = 0;
  statusList: any = [];
  aktaIndukId: number = 0;
  aktaIndukList: string [];
  peruntukanUndangId: number = 0;
  Id: number;

  // PERFECT SCROLL BAR
  public type: string = 'component';
  public disabled: boolean = false;
  public config: PerfectScrollbarConfigInterface = {};
  @ViewChild(PerfectScrollbarComponent, {static: true}) componentRef?: PerfectScrollbarComponent;

  files: File;
  form: FormGroup;

  constructor(
    private service: PentadbiranService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private snackBar: MatSnackBar,
    private router: Router,
    private app: AppComponent,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });
    this.getJabatanList();
    this.getJenisList();
    this.loadDetails(this.Id);
  }

  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.service.getCarianIndukDetails(Id)
          .subscribe(result => {
            this.showSpinner = false;
            this.jenisId = result.jenisId > 0 ? result.jenisId : 0;
            this.form.patchValue(result);
          });
    }
  }

  getJabatanList() {
    this.service.getJabatanList().subscribe(items => {
      this.jabatanList = items;
    });
  }

  getJenisList() {
    this.service.getCarianIndukList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.jenisList = items.list as string [];;
    });
  }

  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.app.showOverlaySpinner(true);

      console.log('aktaInduk-->', model);

      this.service.saveCarianInduk(model, this.Id)
        .subscribe(r => {
          this.app.showOverlaySpinner(false);
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.router.navigate(['/app/pentadbiran/data-induk/carian-induk-list']);
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }

  // open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const Kod = formGroup.controls['kod'];
      const Jenis = formGroup.controls['jenisId'];
      const Kedudukan = formGroup.controls['kedudukan'];
      const Nama = formGroup.controls['nama'];

      if (Kod) {
        Kod.setErrors(null);
        if (!Kod.value) {
          Kod.setErrors({ required: true });
        }
      }

      if (Jenis) {
        Jenis.setErrors(null);
        if (!Jenis.value) {
          Jenis.setErrors({ required: true });
        }
      }

      if (Kedudukan) {
        Kedudukan.setErrors(null);
        if (!Kedudukan.value) {
          Kedudukan.setErrors({ required: true });
        }
      }

      if (Nama) {
        Nama.setErrors(null);
        if (!Nama.value) {
          Nama.setErrors({ required: true });
        }
      }
    }
  }
  // close custom validation

  // open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: 'Rekod semasa akan ditetapkan semula!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  // close reset function

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      kod: ['', []],
      jenisId: ['0',[]],
      kedudukan: ['', []],
      nama: ['', []],
      keterangan: ['', []],
      terkunci: ['', []],
      }, { validator: this.customValidation() }
    );
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IklanDetailsComponent } from './iklan-details.component';

describe('IklanDetailsComponent', () => {
  let component: IklanDetailsComponent;
  let fixture: ComponentFixture<IklanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IklanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IklanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IklanListComponent } from './list.component';

describe('IklanListComponent', () => {
  let component: IklanListComponent;
  let fixture: ComponentFixture<IklanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IklanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IklanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

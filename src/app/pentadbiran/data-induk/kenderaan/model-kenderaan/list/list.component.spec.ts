import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelKenderaanListComponent } from './list.component';

describe('ModelKenderaanListComponent', () => {
  let component: ModelKenderaanListComponent;
  let fixture: ComponentFixture<ModelKenderaanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelKenderaanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelKenderaanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

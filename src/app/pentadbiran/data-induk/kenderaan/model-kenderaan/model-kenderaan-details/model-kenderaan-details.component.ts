/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import { PentadbiranService } from '../../../../pentadbiran.service';
import { Pager, PagerSetting } from '../../../../../../pagingnation';
import Swal from 'sweetalert2';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AppComponent } from '../../../../../app.component';

@Component({
  selector: 'app-pentadbiran-kumpulan-details',
  templateUrl: './model-kenderaan-details.component.html',
  styleUrls: ['./model-kenderaan-details.component.scss']
})

export class ModelKenderaanDetailsComponent implements OnInit {

  displayedColumns: string[] = ['select', 'projectName', 'manage'];
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  showSpinner: boolean = false;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;
  filterText = '';
  dataToPass: any = [];
  searchKey: string;
  submitted = false;

  Id: number;
  statusId = 0;
  statusList: any = [];
  jenamaKenderaanId = 0;
  jenamaKenderaanList: string [];


  // PERFECT SCROLL BAR
  public type = 'component';
  public disabled = false;
  public config: PerfectScrollbarConfigInterface = {};
  @ViewChild(PerfectScrollbarComponent, {static: true}) componentRef?: PerfectScrollbarComponent;

  files: File;
  form: FormGroup;

  constructor(
    private service: PentadbiranService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private snackBar: MatSnackBar,
    private router: Router,
    private app: AppComponent,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });
    this.getStatusList();
    this.getJenamaKenderaanList();
    this.loadDetails(this.Id);
  }

  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.service.getModelKenderaanDetails(Id)
          .subscribe(result => {
            this.showSpinner = false;
            this.statusId = result.statusId > 0 ? result.statusId : 0;
            this.jenamaKenderaanId = result.jenamaKenderaanId > 0 ? result.jenamaKenderaanId : 0;
            this.form.patchValue(result);
          });
    }
  }

  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.app.showOverlaySpinner(true);

      console.log('modelKenderaan-->', model);

      this.service.saveModelKenderaan(model, this.Id)
        .subscribe(r => {
          this.app.showOverlaySpinner(false);
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.router.navigate(['/app/pentadbiran/data-induk/kenderaan/model-kenderaan-list']);
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }

  getJenamaKenderaanList() {
    this.service.getJenamaKenderaanList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.jenamaKenderaanList = items.list as string [];
    });
  }

  getStatusList() {
    this.service.getStatusList().subscribe(items => {
      this.statusList = items;
    });
  }

  // open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const Jenama = formGroup.controls['jenamaKenderaanId'];
      const Model = formGroup.controls['model'];
      const Status = formGroup.controls['statusId'];

      if (Jenama) {
        Jenama.setErrors(null);
        if (!Jenama.value) {
          Jenama.setErrors({ required: true });
        }
      }

      if (Model) {
        Model.setErrors(null);
        if (!Model.value) {
          Model.setErrors({ required: true });
        }
      }

      if (Status) {
        Status.setErrors(null);
        if (!Status.value) {
          Status.setErrors({ required: true });
        }
      }
    }
  }
  // close custom validation

  // open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: 'Rekod semasa akan ditetapkan semula!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  // close reset function

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      model: ['', []],
      statusId: ['0',[]],
      jenamaKenderaanId: ['0',[]],
      }, { validator: this.customValidation() }
    );
  }

}

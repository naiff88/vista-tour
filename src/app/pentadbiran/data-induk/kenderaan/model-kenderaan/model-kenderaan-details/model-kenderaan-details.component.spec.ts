import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelKenderaanDetailsComponent } from './model-kenderaan-details.component';

describe('ModelKenderaanDetailsComponent', () => {
  let component: ModelKenderaanDetailsComponent;
  let fixture: ComponentFixture<ModelKenderaanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelKenderaanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelKenderaanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JenisKenderaanListComponent } from './list.component';

describe('JenisKenderaanListComponent', () => {
  let component: JenisKenderaanListComponent;
  let fixture: ComponentFixture<JenisKenderaanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JenisKenderaanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JenisKenderaanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

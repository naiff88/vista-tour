import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JenisKenderaanDetailsComponent } from './jenis-kenderaan-details.component';

describe('JenamaKenderaanDetailsComponent', () => {
  let component: JenisKenderaanDetailsComponent;
  let fixture: ComponentFixture<JenisKenderaanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JenisKenderaanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JenisKenderaanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

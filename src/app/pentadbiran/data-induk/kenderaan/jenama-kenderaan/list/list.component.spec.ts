import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarialIndukListComponent } from './list.component';

describe('CarialIndukListComponent', () => {
  let component: CarialIndukListComponent;
  let fixture: ComponentFixture<CarialIndukListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarialIndukListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarialIndukListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

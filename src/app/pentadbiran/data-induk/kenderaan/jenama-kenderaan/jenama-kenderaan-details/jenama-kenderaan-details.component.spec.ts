import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JenamaKenderaanDetailsComponent } from './jenama-kenderaan-details.component';

describe('JenamaKenderaanDetailsComponent', () => {
  let component: JenamaKenderaanDetailsComponent;
  let fixture: ComponentFixture<JenamaKenderaanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JenamaKenderaanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JenamaKenderaanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

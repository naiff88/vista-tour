import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeksyenKesalahanDetailsComponent } from './seksyen-kesalahan-details.component';

describe('SeksyenKesalahanDetailsComponent', () => {
  let component: SeksyenKesalahanDetailsComponent;
  let fixture: ComponentFixture<SeksyenKesalahanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeksyenKesalahanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeksyenKesalahanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

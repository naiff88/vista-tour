import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeksyenKesalahanListComponent } from './list.component';

describe('SeksyenKesalahanListComponent', () => {
  let component: SeksyenKesalahanListComponent;
  let fixture: ComponentFixture<SeksyenKesalahanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeksyenKesalahanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeksyenKesalahanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

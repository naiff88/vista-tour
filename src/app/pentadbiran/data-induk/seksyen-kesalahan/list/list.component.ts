/* tslint:disable:max-line-length */
import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
// import { MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule } from '@angular/material/tabs';
// import { MatTableModule } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
// import { MatSnackBar} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
// import { MatDialogModule} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
import { MatDialog} from '@angular/material/dialog';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';
import { MatTableDataSource} from '@angular/material/table';

import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import { PentadbiranService } from '../../../pentadbiran.service';
import { Pager, PagerSetting } from '../../../../../pagingnation';
import {ProjectManagementUpsert as Project} from '@service-proxies';
import {SalesService} from '../../../../sales/sales.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
import Swal from 'sweetalert2';
import {MenuComponent} from '../../../../dashboard/menu/menu.component';
require('jspdf-autotable');

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class SeksyenKesalahanListComponent implements OnInit {

  // @Input() orderStatus: string;
  @Input() orderTabIndex: number;


  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  form: FormGroup;
  showSpinner: boolean = false;
  filterText = '';
  dataToPass: any = [];
  submitted = false;
  onSearch = false;
  reVisit: boolean = false;;
  filterValue: any;

  aktaIndukId: number = 0;
  jenisId: number = 0;
  jenisList: any = [];
  aktaIndukList: string [];
  peruntukanUndangId: number = 0;
  peruntukanUndangListRef: string [];

  // any unique name for searching list
  searchID = 'getSeksyenKesalahanList';

  // for excel ganerator
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'

  constructor(
    private pentadbiranService: PentadbiranService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private snackBar: MatSnackBar,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private menu: MenuComponent,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  ngOnInit() {
    if (this.pageSetting.getSearchFilterValueSV(this.searchID)) {
      this.reVisit = true;
    }

    this.activatedRoute.params.subscribe(params => {
      // param from other page to search pergerakan by ID Handheld
      if (params.idPeruntukan) {
        if(params.idPeruntukan)
        {
          this.menu.highlightMenu('/app/pentadbiran/data-induk/seksyen-kesalahan-list');
          this.reset();
          this.form = this.buildFormItems();
          this.form.patchValue({idPeruntukan: params.idPeruntukan});
          this.search();

        }
      }
    });

    this.getSeksyenKesalahanList();
    this.getJenisList();
    this.getAktaIndukList();
    this.getPeruntukanUndangListRef();
    this.loadDetails(this.aktaIndukId);
  }

  loadDetails(AktaIndukId) {
    this.form = this.buildFormItems();
    // console.log('this.staffID ::::::::::::: ', this.staffId);
    if (AktaIndukId > 0) {
      // this.service.getproductDetails(JabatanId)
      //   .subscribe(result => {
      //     // this.CourierId = result.CourierId > 0 ? result.CourierId : 0;
      //     this.StatusId = result.StatusId > 0 ? result.StatusId : 0;
      //     // this.PaymentTypeId = result.PaymentTypeId > 0 ? result.PaymentTypeId : 0;
      //     this.form.patchValue(result);
      //   });
    }
  }

  // open standard function use for multiple delete in datatable
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.CustomerId + 1}`;
  }
  onRowSelect(event) {
    const evt = event.originalEvent as MouseEvent;
    evt.stopPropagation();
  }
  // close standard function use for multiple delete in datatable

  // open reload search form
  reloadSearchForm() {
    this.filterValue = this.pageSetting.getSearchFilterValueSV(this.searchID);
    let SFcurrent = this.pageSetting.getSearchFilterValueSF(this.searchID);
    this.form = this.buildFormItems();
    if (SFcurrent) {
      // reasing value for list & date model
      this.jenisId = SFcurrent.jenisId > 0 ? SFcurrent.jenisId : 0;
      // this.aktaIndukId = SFcurrent.aktaIndukId > 0 ? SFcurrent.aktaIndukId : 0;
      // this.dateStart = SFcurrent.TarikhMula = moment(SFcurrent.dateStart) || '';
      // this.dateEnd = SFcurrent.TarikhAkhir = moment(SFcurrent.dateEnd) || '';
      this.form.patchValue(SFcurrent);
    } else {
      // initiate list & date model
      this.jenisId = 0;
      // this.Status = 0;
      // this.dateStart = '';
      // this.dateEnd = '';
    }
  }
  // close reload search form

  getAktaIndukList() {
    this.pentadbiranService.getAktaIndukList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.aktaIndukList = items.list as string [];
      console.log('AktaIndukGet---> ', this.aktaIndukList);
    });
  }

  getPeruntukanUndangListRef() {
    this.pentadbiranService.getPeruntukanUndangList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.peruntukanUndangListRef = items.list as string [];
      console.log('getPeruntukanUndangListRef---> ', this.peruntukanUndangListRef);
    });
  }

  getJenisList() {
    console.log('getJenisList ::::::::::::::::::::: ');
    this.pentadbiranService.getJenisList().subscribe(items => {
      console.log('getJenisList items ::::::::::::::::::::: ', items);
      this.jenisList = items;
    });
  }

  // open standard listing with search function
  async getSeksyenKesalahanList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    await this.reloadSearchForm();
    await this.pentadbiranService.getSeksyenKesalahanList(this.pageSetting.getPagerSettingWithSearch(this.paginator, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          this.showSpinner = false;
          this.dataSource.data = items.list;
          this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
          this.primengTableHelper.records = items.list;
          if (this.onSearch) {
            this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
              duration: 3000,
              verticalPosition: 'bottom',
              horizontalPosition: 'end'
            });
            this.onSearch = false;
          }
          this.primengTableHelper.hideLoadingIndicator();
          if (event! && (event.filters || event.sortField)) {
            this.paginator.changePage(0);
          }
        });
    await this.primengTableHelper.showLoadingIndicator();
  }
  // close standard listing with search function

  // search function
  search() {
    this.submitted = true;
    const searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
          'Amaran',
          'Pastikan maklumat carian adalah sah!',
          'warning'
      );
    }
    else {
      this.submitted = false;
      // setting store procedure listing field for filtering
      this.filterValue = {
        ' noSeksyen ': { value: searchForm.userName, matchMode: 'contains' },
        ' noSubseksyen ': { value: searchForm.fullName, matchMode: 'contains' },
        ' peruntukanUndangId ': { value: searchForm.fullName, matchMode: 'equals' },
      };
      this.pageSetting.setSearchFilterLS(this.filterValue, searchForm, this.searchID);
      this.onSearch = true;
      this.getSeksyenKesalahanList();
      this.paginator.changePage(0);
    }
  }

  // reset search form
  reset() {
    this.pageSetting.removeSearchFilterLS(this.searchID);
    this.form = this.buildFormItems();

    // initiate list & date model
    // this.dateStart = '';
    // this.dateEnd = '';

    this.filterValue = null;
    this.getSeksyenKesalahanList();
    this.paginator.changePage(0);
  }

  // open details page
  seksyenKesalahanDetails(id?: number, type?: string): void {
    this.router.navigate(['/app/pentadbiran/data-induk/seksyen-kesalahan-details', id, type]);
  }

  // multiple delete function
  delete() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.Id }));
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: 'Rekod yang telah hapus tidak akan dikembalikan!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        this.pentadbiranService.deleteSeksyenKesalahan(selectedItems)
            .subscribe(resultDelete => {
              this.showSpinner = false;
              if (resultDelete.ReturnCode == 204) {
                Swal.fire(
                    'Error',
                    resultDelete.ResponseMessage,
                    'error'
                );
              }
              else {
                Swal.fire(
                    '',
                    resultDelete.ResponseMessage,
                    'success'
                );
              }
              this.getSeksyenKesalahanList();
            });
      }
    })
  }

  // pdf genarator
  printTablePdf() {
    var doc = new jsPDF('p', 'pt');
    // Columns to display
    var col = ['No.', 'Peruntukan Undang-Undang', 'No. Seksyen', 'No. Subseksyen', 'Keterangan', 'Kadar Kompaun', 'Kadar', 'Caj Maksimum'];
    var rows = [];
    let i = 1;

    this.showSpinner = true;
    // get list of data
    this.pentadbiranService.getSeksyenKesalahanList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          this.showSpinner = false;
          let list =  items.list;
          for (var key in list) {
            rows.push([i, list[key].peruntukanUndang, list[key].noSeksyen, list[key].noSubSeksyen, list[key].butiranKesalahan, list[key].kadarKompaun, list[key].kadar, list[key].cajMaksimum])
            i++;
          }
        });

    // report title
    var header = function (data) {
      doc.setFontSize(14);
      doc.setFontStyle('normal');
      doc.text('Laporan Seksyen Kesalahan', data.settings.margin.left, 50);
    };

    doc.autoTable(col, rows, { margin: { top: 80 }, beforePageContent: header });
    // create pdf file
    doc.save('LaporanSeksyenKesalahan.pdf');
  }

  // excel genarator
  printTableExcel() {
    var workbook = new Excel.Workbook();
    // Tab Title
    var worksheet = workbook.addWorksheet('Senarai Seksyen Kesalahan');
    // Columns to display
    const col = ['No.', 'Peruntukan Undang-Undang', 'No. Seksyen', 'No. Subseksyen', 'Keterangan', 'Kadar Kompaun', 'Kadar', 'Caj Maksimum'];
    worksheet.mergeCells('A1', 'H1'); // title merge cell
    worksheet.getCell('A1').value = 'Laporan Seksyen Kesalahan'; // title
    worksheet.getRow(2).values = col;
    // Column key
    worksheet.columns = [
      { key: 'No', width: 5 },
      { key: 'peruntukanUndang', width: 25 },
      { key: 'noSeksyen', width: 25 },
      { key: 'noSubSeksyen', width: 10 },
      { key: 'butiranKesalahan', width: 15 },
      { key: 'singkatan', width: 15 },
      { key: 'kadarKompaun', width: 15 },
      { key: 'kadar', width: 15 },
      { key: 'cajMaksimum', width: 15 },
    ];

    let i = 1;
    this.showSpinner = true;
    // get list of data
    this.pentadbiranService.getSeksyenKesalahanList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          this.showSpinner = false;
          let list =  items.list;
          for (var key in list) {
            worksheet.addRow([i, list[key].peruntukanUndang, list[key].noSeksyen, list[key].noSubSeksyen, list[key].butiranKesalahan, list[key].kadarKompaun, list[key].kadar, list[key].cajMaksimum])
            i++;
          }
        });

    worksheet.eachRow(function (row, _rowNumber) {
      row.eachCell(function (cell, _colNumber) {
        cell.border = {
          top: { style: 'thin' },
          left: { style: 'thin' },
          bottom: { style: 'thin' },
          right: { style: 'thin' }
        };
      });
    });

    workbook.xlsx.writeBuffer().then(data => {
      const blob = new Blob([data], { type: this.blobType });
      // create excell file
      FileSaver.saveAs(blob, 'LaporanSeksyenKesalahan.xlsx');
    });
  }

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      aktaIndukId: ['0', []],
      peruntukanUndang: ['', []],
      noSeksyen: ['', []],
      noSubseksyen: ['', []],
      peruntukanUndangId: ['0',[]],
        }, {}
    );
  }
}

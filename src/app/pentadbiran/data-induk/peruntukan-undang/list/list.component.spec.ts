import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AktaIndukListComponent } from './list.component';

describe('AktaIndukListComponent', () => {
  let component: AktaIndukListComponent;
  let fixture: ComponentFixture<AktaIndukListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AktaIndukListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AktaIndukListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

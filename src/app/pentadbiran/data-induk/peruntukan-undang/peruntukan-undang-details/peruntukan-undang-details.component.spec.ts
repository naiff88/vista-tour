import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeruntukanUndangDetailsComponent } from './peruntukan-undang-details.component';

describe('PeruntukanUndangDetailsComponent', () => {
  let component: PeruntukanUndangDetailsComponent;
  let fixture: ComponentFixture<PeruntukanUndangDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeruntukanUndangDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeruntukanUndangDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

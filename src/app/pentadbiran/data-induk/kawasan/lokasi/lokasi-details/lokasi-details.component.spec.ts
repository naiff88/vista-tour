import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LokasiDetailsComponent } from './lokasi-details.component';

describe('LokasiDetailsComponent', () => {
  let component: LokasiDetailsComponent;
  let fixture: ComponentFixture<LokasiDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LokasiDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LokasiDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

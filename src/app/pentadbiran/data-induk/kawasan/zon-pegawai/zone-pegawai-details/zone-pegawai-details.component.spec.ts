import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZonePegawaiDetailsComponent } from './zone-pegawai-details.component';

describe('ModelKenderaanDetailsComponent', () => {
  let component: ZonePegawaiDetailsComponent;
  let fixture: ComponentFixture<ZonePegawaiDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZonePegawaiDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZonePegawaiDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

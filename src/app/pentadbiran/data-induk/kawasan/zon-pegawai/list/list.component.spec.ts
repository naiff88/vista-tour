import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZonPegawaiListComponent } from './list.component';

describe('ZonPegawaiListComponent', () => {
  let component: ZonPegawaiListComponent;
  let fixture: ComponentFixture<ZonPegawaiListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZonPegawaiListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZonPegawaiListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

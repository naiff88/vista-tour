import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParlimenDetailsComponent } from './parlimen-details.component';

describe('ParlimenDetailsComponent', () => {
  let component: ParlimenDetailsComponent;
  let fixture: ComponentFixture<ParlimenDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParlimenDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParlimenDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

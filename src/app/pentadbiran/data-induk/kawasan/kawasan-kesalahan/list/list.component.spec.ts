import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KawasanKesalahanListComponent } from './list.component';

describe('KawasanKesalahanListComponent', () => {
  let component: KawasanKesalahanListComponent;
  let fixture: ComponentFixture<KawasanKesalahanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KawasanKesalahanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KawasanKesalahanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

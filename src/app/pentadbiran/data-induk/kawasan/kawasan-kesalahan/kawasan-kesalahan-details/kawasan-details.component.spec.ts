import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KawasanDetailsComponent } from './kawasan-details.component';

describe('KawasanDetailsComponent', () => {
  let component: KawasanDetailsComponent;
  let fixture: ComponentFixture<KawasanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KawasanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KawasanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

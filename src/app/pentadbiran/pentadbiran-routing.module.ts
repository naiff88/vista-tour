/* tslint:disable:max-line-length */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MenuComponent } from '../dashboard/menu/menu.component';
import { KumpulanListComponent } from './kumpulan/list/list.component';
import { KumpulanDetailsComponent } from './kumpulan/kumpulan-details/kumpulan-details.component';
import { PenggunaListComponent } from './pengguna/list/list.component';
import { PenggunaDetailsComponent } from './pengguna/pengguna-details/pengguna-details.component';
import { AktaIndukListComponent } from './data-induk/akta-induk/list/list.component';
import { AktaIndukDetailsComponent } from './data-induk/akta-induk/akta-induk-details/akta-induk-details.component';
import { PeruntukanUndangListComponent } from './data-induk/peruntukan-undang/list/list.component';
import { PeruntukanUndangDetailsComponent } from './data-induk/peruntukan-undang/peruntukan-undang-details/peruntukan-undang-details.component';
import { SeksyenKesalahanListComponent } from './data-induk/seksyen-kesalahan/list/list.component';
import { SeksyenKesalahanDetailsComponent } from './data-induk/seksyen-kesalahan/seksyen-kesalahan-details/seksyen-kesalahan-details.component';
import { CarialIndukListComponent } from './data-induk/carian-data-induk/list/list.component';
import { CarianIndukDetailsComponent } from './data-induk/carian-data-induk/carian-induk-details/carian-induk-details.component';
import { JenamaKenderaanListComponent } from './data-induk/kenderaan/jenama-kenderaan/list/list.component';
import { JenamaKenderaanDetailsComponent } from './data-induk/kenderaan/jenama-kenderaan/jenama-kenderaan-details/jenama-kenderaan-details.component';
import { ModelKenderaanListComponent } from './data-induk/kenderaan/model-kenderaan/list/list.component';
import { ModelKenderaanDetailsComponent } from './data-induk/kenderaan/model-kenderaan/model-kenderaan-details/model-kenderaan-details.component';
import { JenisKenderaanListComponent } from './data-induk/kenderaan/jenis-kenderaan/list/list.component';
import { JenisKenderaanDetailsComponent } from './data-induk/kenderaan/jenis-kenderaan/jenis-kenderaan-details/jenis-kenderaan-details.component';
import { ZonPegawaiListComponent } from './data-induk/kawasan/zon-pegawai/list/list.component';
import { ZonePegawaiDetailsComponent } from './data-induk/kawasan/zon-pegawai/zone-pegawai-details/zone-pegawai-details.component';
import { ParlimenListComponent } from './data-induk/kawasan/parlimen/list/list.component';
import { ParlimenDetailsComponent } from './data-induk/kawasan/parlimen/parlimen-details/parlimen-details.component';
import { LokasiListComponent } from './data-induk/kawasan/lokasi/list/list.component';
import { LokasiDetailsComponent } from './data-induk/kawasan/lokasi/lokasi-details/lokasi-details.component';
import { KawasanKesalahanListComponent } from './data-induk/kawasan/kawasan-kesalahan/list/list.component';
import { KawasanDetailsComponent } from './data-induk/kawasan/kawasan-kesalahan/kawasan-kesalahan-details/kawasan-details.component';
import { PangkatPegawaiListComponent } from './data-induk/pangkat-pegawai/list/list.component';
import { PangkatPegawaiDetailsComponent } from './data-induk/pangkat-pegawai/pangkat-pegawai-details/pangkat-pegawai-details.component';
import { IklanListComponent } from './data-induk/iklan/list/list.component';
import { IklanDetailsComponent } from './data-induk/iklan/iklan-details/iklan-details.component';
import { PengumumanListComponent } from './pengumuman/list/list.component';
import { PengumumanDetailsComponent } from './pengumuman/pengumuman-details/pengumuman-details.component';
import { AuditListComponent } from './audit/list/list.component';
import { KompaunDiskaunListComponent } from './kompaun-diskaun/list/list.component';
import { JenisKenderaanDiskaunComponent } from './kompaun-diskaun/kompaun-diskaun-details/jenis-kenderaan/jenis-kenderaan-diskaun.component';
import { UmumDiskaunComponent } from './kompaun-diskaun/kompaun-diskaun-details/umum/umum-diskaun.component';
import { AwalDiskaunComponent } from './kompaun-diskaun/kompaun-diskaun-details/bayaran-awal/awal-diskaun.component';

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MenuComponent,

                children: [
                    { path: '', pathMatch: 'full', redirectTo: 'kumpulan-list' },
                    { path: 'kumpulan-list', component: KumpulanListComponent },
                    { path: 'kumpulan-details/:id/:type', component: KumpulanDetailsComponent },
                    { path: 'pengguna-list', component: PenggunaListComponent },
                    { path: 'pengguna-details/:id/:type', component: PenggunaDetailsComponent },
                    { path: 'data-induk/akta-induk-list', component: AktaIndukListComponent },
                    { path: 'data-induk/akta-induk-details/:id/:type', component: AktaIndukDetailsComponent },
                    { path: 'data-induk/peruntukan-undang-list', component: PeruntukanUndangListComponent },
                    { path: 'data-induk/peruntukan-undang-details/:id/:type', component: PeruntukanUndangDetailsComponent },
                    { path: 'data-induk/seksyen-kesalahan-list', component: SeksyenKesalahanListComponent },
                    { path: 'data-induk/seksyen-kesalahan-list/:idPeruntukan', component: SeksyenKesalahanListComponent },
                    { path: 'data-induk/seksyen-kesalahan-details/:id/:type', component: SeksyenKesalahanDetailsComponent },
                    { path: 'data-induk/carian-induk-list', component: CarialIndukListComponent },
                    { path: 'data-induk/carian-induk-details/:id/:type', component: CarianIndukDetailsComponent },
                    { path: 'data-induk/kenderaan/jenama-kenderaan-list', component: JenamaKenderaanListComponent },
                    { path: 'data-induk/kenderaan/jenama-kenderaan-details/:id/:type', component: JenamaKenderaanDetailsComponent },
                    { path: 'data-induk/kenderaan/model-kenderaan-list', component: ModelKenderaanListComponent },
                    { path: 'data-induk/kenderaan/model-kenderaan-details/:id/:type', component: ModelKenderaanDetailsComponent },
                    { path: 'data-induk/kenderaan/jenis-kenderaan-list', component: JenisKenderaanListComponent },
                    { path: 'data-induk/kenderaan/jenis-kenderaan-details/:id/:type', component: JenisKenderaanDetailsComponent },
                    { path: 'data-induk/kawasan/zon-pegawai-list', component: ZonPegawaiListComponent },
                    { path: 'data-induk/kawasan/zon-pegawai-details/:id/:type', component: ZonePegawaiDetailsComponent },
                    { path: 'data-induk/kawasan/parlimen-list', component: ParlimenListComponent },
                    { path: 'data-induk/kawasan/parlimen-details/:id/:type', component: ParlimenDetailsComponent },
                    { path: 'data-induk/kawasan/lokasi-list', component: LokasiListComponent },
                    { path: 'data-induk/kawasan/lokasi-details/:id/:type', component: LokasiDetailsComponent },
                    { path: 'data-induk/kawasan/kawasan-kesalahan-list', component: KawasanKesalahanListComponent },
                    { path: 'data-induk/kawasan/kawasan-kesalahan-details/:id/:type', component: KawasanDetailsComponent },
                    { path: 'data-induk/pangkat-pegawai-list', component: PangkatPegawaiListComponent },
                    { path: 'data-induk/pangkat-pegawai-details/:id/:type', component: PangkatPegawaiDetailsComponent },
                    { path: 'data-induk/iklan-list', component: IklanListComponent },
                    { path: 'data-induk/iklan-details/:id/:type', component: IklanDetailsComponent },
                    { path: 'pengumuman-list', component: PengumumanListComponent },
                    { path: 'pengumuman-details/:id/:type', component: PengumumanDetailsComponent },
                    { path: 'audit-list', component: AuditListComponent },
                    { path: 'kompaun-diskaun-list', component: KompaunDiskaunListComponent },
                    { path: 'kompaun-diskaun-details/jenis-kenderaan/:id/:type', component: JenisKenderaanDiskaunComponent },
                    { path: 'kompaun-diskaun-details/umum/:id/:type', component: UmumDiskaunComponent },
                    { path: 'kompaun-diskaun-details/awal/:id/:type', component: AwalDiskaunComponent },
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})

export class PentadbiranRoutingModule { }

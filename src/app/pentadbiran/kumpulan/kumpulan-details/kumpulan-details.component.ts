/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import { PentadbiranService } from '../../pentadbiran.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import Swal from 'sweetalert2';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AppComponent } from '../../../app.component';
import { PilihAhliComponent } from '../modal/pilih-ahli/pilih-ahli.component';

@Component({
  selector: 'app-pentadbiran-kumpulan-details',
  templateUrl: './kumpulan-details.component.html',
  styleUrls: ['./kumpulan-details.component.scss']
})

export class KumpulanDetailsComponent implements OnInit {

  displayedColumns: string[] = ['select', 'projectName', 'manage'];
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  showSpinner: boolean = false;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;
  filterText = '';
  dataToPass: any = [];
  searchKey: string;
  submitted = false;
  Id: number;
  StatusId: number = 0;
  jabatanId: number = 0;
  jabatanList: any = [];
  PermissionItemList: any = [];


  // PERFECT SCROLL BAR
  public type: string = 'component';
  public disabled: boolean = false;
  public config: PerfectScrollbarConfigInterface = {};
  @ViewChild(PerfectScrollbarComponent, {static: true}) componentRef?: PerfectScrollbarComponent;

  files: File;
  form: FormGroup;

  constructor(
    private service: PentadbiranService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private snackBar: MatSnackBar,
    private router: Router,
    private app: AppComponent,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });
    this.getJabatanList();
    this.getUserGroupList();
    this.getPermissionItem();
    this.loadDetails(this.Id);
    // this.getInvoice();
  }

  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.service.getKumpulanPenggunaDetails(Id)
          .subscribe(result => {
            this.showSpinner = false;
            this.jabatanId = result.jabatanId > 0 ? result.jabatanId : 0;
            this.form.patchValue(result);
          });
      // this.service.getproductDetails(JabatanId)
      //   .subscribe(result => {
      //     // this.CourierId = result.CourierId > 0 ? result.CourierId : 0;
      //     this.StatusId = result.StatusId > 0 ? result.StatusId : 0;
      //     // this.PaymentTypeId = result.PaymentTypeId > 0 ? result.PaymentTypeId : 0;
      //     this.form.patchValue(result);
      //   });
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }

  // open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: 'Rekod semasa akan ditetapkan semula!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  // close reset function

  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.app.showOverlaySpinner(true);

      console.log('kumpulan-->', model);

      this.service.saveKumpulan(model, this.jabatanId)
        .subscribe(r => {
          this.app.showOverlaySpinner(false);
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            // this.router.navigate(['/app/catalogue/products-list']);
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }

  // onCancel() {
  //   this.router.navigate(['/app/pentadbiran/kumpulan-list']);
  // }

  getJabatanList() {
    console.log('getJabatanList ::::::::::::::::::::: ');
    this.service.getJabatanList().subscribe(items => {
      console.log('getJabatanList items ::::::::::::::::::::: ', items);
      this.jabatanList = items;
    });
  }

  getUserGroupList(event?: LazyLoadEvent) {
    console.log('getUserGroupList ::::::::::::::::::::: ');
    this.app.showOverlaySpinner(true);
    this.service.getUserGroupList(this.pageSetting.getPagerSetting(this.paginator, event
        , this.primengTableHelper.getSorting(this.dataTable)
    )).subscribe(items => {
          this.app.showOverlaySpinner(false);
          console.log('getUserGroupList items ::::::::::::::::::::: ', items);
          this.dataSource.data = items.list;
          this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
          this.primengTableHelper.records = items.list;
          if (event! && (event.filters || event.sortField)) {
            this.paginator.changePage(0);
          }
          this.primengTableHelper.hideLoadingIndicator();
        });
    this.primengTableHelper.showLoadingIndicator();
  }

  addUserGroup(): void {
    const dialogRef = this.dialog.open(PilihAhliComponent, { data: { JabatanId: this.jabatanId } });
    dialogRef.afterClosed()
        .subscribe(ok => {
          this.getUserGroupList();
        });
  }

  getPermissionItem() {
    console.log('getPermissionItem ::::::::::::::::::::: ');
    this.service.getPermissionItem().subscribe(items => {
      console.log('getPermissionItem items ::::::::::::::::::::: ', items);
      this.PermissionItemList = items;
    });
  }

  // open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const Jabatan = formGroup.controls['jabatanId'];
      const Keterangan = formGroup.controls['keterangan'];
      const NamaKumpulan = formGroup.controls['kumpulanName'];

      if (Jabatan) {
        Jabatan.setErrors(null);
        if (!Jabatan.value) {
          Jabatan.setErrors({ required: true });
        }
        // else if (Status.value == 0) {
        //   Jabatan.setErrors({ min: true });
        // }
      }

      if (Keterangan) {
        Keterangan.setErrors(null);
        if (!Keterangan.value) {
          Keterangan.setErrors({ required: true });
        }
      }

      if (NamaKumpulan) {
        NamaKumpulan.setErrors(null);
        if (!NamaKumpulan.value) {
          NamaKumpulan.setErrors({ required: true });
        }
      }
    }
  }
  // close custom validation

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      jabatanId: ['0', []],
      keterangan: ['', []],
      kumpulanName: ['', []],
      permissionItem: ['',[]],
      }, { validator: this.customValidation() }
    );
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KumpulanDetailsComponent } from './kumpulan-details.component';

describe('KumpulanDetailsComponent', () => {
  let component: KumpulanDetailsComponent;
  let fixture: ComponentFixture<KumpulanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KumpulanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KumpulanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

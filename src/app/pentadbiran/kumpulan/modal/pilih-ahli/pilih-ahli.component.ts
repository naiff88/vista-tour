/* tslint:disable:max-line-length */
import {Component, Inject, Input, OnInit, ViewChild} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
// import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';

// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import { PentadbiranService } from '../../../pentadbiran.service';
import { Pager, PagerSetting } from '../../../../../pagingnation';
import {ProjectManagementUpsert as Project} from '@service-proxies';
import Swal from 'sweetalert2';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AppComponent } from '../../../../app.component';

@Component({
  selector: 'app-pentadbiran-kumpulan-pilih-ahlli',
  templateUrl: './pilih-ahli.component.html',
  styleUrls: ['./pilih-ahli.component.scss']
})

// @Injectable()
export class PilihAhliComponent implements OnInit {

  @Input() orderStatus: string;
  @Input() orderTabIndex: number;

  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  selectionF = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  // app: AppComponent;

  @ViewChild('dataTable') dataTable: Table;
  @ViewChild('paginator') paginator: Paginator;

  filterText = '';
  showSpinner: boolean = false;

  constructor(
    private pentadbiranService: PentadbiranService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    // private app: AppComponent,
    public dialogRef: MatDialogRef<PilihAhliComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { JabatanId: number },
    private router: Router) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
    // this.app = new AppComponent;
  }

  ngOnInit() {
    // console.log('LIST orderStatus :::::::::::', this.orderStatus);
    // console.log('LIST orderTabIndex :::::::::::', this.orderTabIndex);
    this.getUserList();
    // this.service.getDocumentTypeList();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.productId + 1}`;
  }

  add() {
    // this.selectionF.clear();
    // this.dataSource.data.forEach(row => this.selectionF.select(row));
    // console.log('this.this.selectionF:::::::::::::::::::::: ', this.selectionF);
    // const selectedItemsF: Array<{ id: number }> = this.selectionF.selected.map(item => ({ id: item.PromotionId, enable: item.Enable }));
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.productId }));
    console.log('add selectedItems :::::::::::::::::::::: ', selectedItems);

    Swal.fire({
      title: 'Adakan Anda Pasti?',
      text: 'Pilihan Akan Ditambah Kedalam Kumpulan Pengguna!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Ya!'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        // this.app.showOverlaySpinner(true);
        this.pentadbiranService.addUserToUserGroup(selectedItems, this.data.JabatanId)
          .subscribe(resultAdd => {
            this.showSpinner = false;
            // this.app.showOverlaySpinner(false);
            if (resultAdd.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultAdd.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultAdd.ResponseMessage,
                'success'
              );
              this.dialogRef.close();
            }
            // this.getProductList();
          });
      }
    })
  }

  // promotionDetails(PromotionId?: number): void {
  //   console.log('orderDetails PromotionId ::::::::::::::: ', PromotionId);
  //   this.router.navigate(['/app/sales/promotion-details', PromotionId]);
  // }

  // getPromotionList(event?: LazyLoadEvent) {
  //   console.log('getPromotionList ::::::::::::::::::::: ');
  //   //this.app.showOverlaySpinner(true);
  //   this.showSpinner = true;
  //   this.salesService.getPromotionList(this.pageSetting.getPagerSetting(this.paginator, event
  //     , this.primengTableHelper.getSorting(this.dataTable)
  //   ))
  //     .subscribe(items => {
  //       //this.app.showOverlaySpinner(false);
  //       this.showSpinner = false;
  //       console.log('getOrderList items ::::::::::::::::::::: ', items);
  //       this.dataSource.data = items.list;
  //       this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
  //       this.primengTableHelper.records = items.list;
  //       if (event! && (event.filters || event.sortField)) {
  //         this.paginator.changePage(0);
  //       }
  //       this.primengTableHelper.hideLoadingIndicator();
  //     });
  //   this.primengTableHelper.showLoadingIndicator();
  // }

  getUserList(event?: LazyLoadEvent) {
    console.log('getUserList ::::::::::::::::::::: ');
    // this.app.showOverlaySpinner(true);
    this.showSpinner = true;
    this.pentadbiranService.getUserGroupListAddUser(this.pageSetting.getPagerSetting(this.paginator, event
      , this.primengTableHelper.getSorting(this.dataTable)
    ),this.data.JabatanId)
      .subscribe(items => {
        this.showSpinner = false;
        console.log('getUserList items ::::::::::::::::::::: ', items);
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        // if (event! && (event.filters || event.sortField)) {
        //   this.paginator.changePage(0);
        // }
        this.primengTableHelper.hideLoadingIndicator();
      });
    this.primengTableHelper.showLoadingIndicator();
  }

}

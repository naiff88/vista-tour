import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PilihAhliComponent } from './pilih-ahli.component';

describe('PilihAhliComponent', () => {
  let component: PilihAhliComponent;
  let fixture: ComponentFixture<PilihAhliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PilihAhliComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PilihAhliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

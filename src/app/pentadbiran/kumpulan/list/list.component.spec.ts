import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KumpulanListComponent } from './list.component';

describe('KumpulanListComponent', () => {
  let component: KumpulanListComponent;
  let fixture: ComponentFixture<KumpulanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KumpulanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KumpulanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

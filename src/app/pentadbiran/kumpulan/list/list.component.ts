/* tslint:disable:max-line-length */
import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
// import { MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule } from '@angular/material/tabs';
// import { MatTableModule } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
// import { MatSnackBar} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
// import { MatDialogModule} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
import { MatDialog} from '@angular/material/dialog';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';
import { MatTableDataSource} from '@angular/material/table';

import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import { Router } from '@angular/router';
import { PentadbiranService } from '../../pentadbiran.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import {ProjectManagementUpsert as Project} from '@service-proxies';
import {SalesService} from '../../../sales/sales.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import {MatSnackBar} from '@angular/material/snack-bar';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class KumpulanListComponent implements OnInit {

  // @Input() orderStatus: string;
  @Input() orderTabIndex: number;


  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  showSpinner: boolean = false;
  filterText = '';
  dataToPass: any = [];
  form: FormGroup;
  onSearch = false;
  submitted = false;
  reVisit = false;
  filterValue: any;

  // any unique name for searching list
  searchID = 'getKumpulanList';

  // for excel ganerator
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  constructor(
    private pentadbiranService: PentadbiranService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  ngOnInit() {
    if (this.pageSetting.getSearchFilterValueSV(this.searchID)) {
      this.reVisit = true;
    }
    this.getKumpulanList();
    this.loadDetails();
  }

  loadDetails() {
    this.form = this.buildFormItems();
  }

  // open standard function use for multiple delete in datatable
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.CustomerId + 1}`;
  }
  // close standard function use for multiple delete in datatable

  // open details page
  kumpulanDetails(id?: number, type?: string): void {
    this.router.navigate(['/app/pentadbiran/kumpulan-details', id, type]);
  }

  // open standard listing with search function
  async getKumpulanList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    await this.reloadSearchForm();
    await this.pentadbiranService.getKumpulanList(this.pageSetting.getPagerSettingWithSearch(this.paginator, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          this.showSpinner = false;
          this.dataSource.data = items.list;
          this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
          this.primengTableHelper.records = items.list;
          if (this.onSearch) {
            this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
              duration: 3000,
              verticalPosition: 'bottom',
              horizontalPosition: 'end'
            });
            this.onSearch = false;
          }
          this.primengTableHelper.hideLoadingIndicator();
          if (event! && (event.filters || event.sortField)) {
            this.paginator.changePage(0);
          }
        });
    await this.primengTableHelper.showLoadingIndicator();
  }
  // close standard listing with search function

  // open reload search form
  reloadSearchForm() {
    this.filterValue = this.pageSetting.getSearchFilterValueSV(this.searchID);
    let SFcurrent = this.pageSetting.getSearchFilterValueSF(this.searchID);
    this.form = this.buildFormItems();
    if (SFcurrent) {
      // reasing value for list & date model
      // this.Pangkat = SFcurrent.Pangkat > 0 ? SFcurrent.Pangkat : 0;
      // this.Status = SFcurrent.Status > 0 ? SFcurrent.Status : 0;
      // this.TarikhMula = SFcurrent.TarikhMula = moment(SFcurrent.TarikhMula) || '';
      // this.TarikhAkhir = SFcurrent.TarikhAkhir = moment(SFcurrent.TarikhAkhir) || '';
      this.form.patchValue(SFcurrent);
    } else {
      // initiate list & date model
      // this.Pangkat = 0;
      // this.Status = 0;
      // this.TarikhMula = '';
      // this.TarikhAkhir = '';
    }
  }
  // close reload search form

  // search function
  search() {
    this.submitted = true;
    const searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
          'Amaran',
          'Pastikan maklumat carian adalah sah!',
          'warning'
      );
    }
    else {
      this.submitted = false;
      // setting store procedure listing field for filtering
      this.filterValue = {
        ' kataKunci ': { value: searchForm.kataKunci, matchMode: 'contains' },
        // ' Jabatan ': { value: searchForm.Jabatan, matchMode: 'equals' },
        // ' Nama ': { value: searchForm.Nama, matchMode: 'contains' },
        // ' NoBadan ': { value: searchForm.NoBadan, matchMode: 'contains' },
        // ' Status ': { value: searchForm.Status, matchMode: 'equals' },
        // ' Pangkat ': { value: searchForm.Pangkat, matchMode: 'equals' },
        // ' Tarikh ': { value: searchForm.TarikhMula, matchMode: 'dateGreater' },
        // ' Tarikh  ': { value: searchForm.TarikhAkhir, matchMode: 'dateLower' },
      }
      this.pageSetting.setSearchFilterLS(this.filterValue, searchForm, this.searchID);
      this.onSearch = true;
      this.getKumpulanList();
      this.paginator.changePage(0);
    }
  }

  // reset search form
  reset() {
    this.pageSetting.removeSearchFilterLS(this.searchID);
    this.form = this.buildFormItems();

    // initiate list & date model

    this.filterValue = null;
    this.getKumpulanList();
    this.paginator.changePage(0);
  }

  // pdf genarator
  printTablePdf() {
    var doc = new jsPDF('p', 'pt');
    // Columns to display
    var col = ['No.', 'Nama Kumpulan', 'Keterangan', 'Jabatan', 'Bil. Pengguna', 'Tarikh Kemaskini'];
    var rows = [];
    let i = 1;

    this.showSpinner = true;
    // get list of data
    this.pentadbiranService.getKumpulanList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          this.showSpinner = false;
          let list =  items.list;
          for (var key in list) {
            rows.push([i, list[key].kumpulanName, list[key].keterangan, list[key].jabatanId, list[key].bilPengguna, list[key].tarikhKemaskini])
            i++;
          }
        });

    // report title
    var header = function (data) {
      doc.setFontSize(14);
      doc.setFontStyle('normal');
      doc.text('Laporan Kumpulan Pengguna', data.settings.margin.left, 50);
    };

    doc.autoTable(col, rows, { margin: { top: 80 }, beforePageContent: header });
    // create pdf file
    doc.save('LaporanKumpulanPengguna.pdf');
  }

  // excel genarator
  printTableExcel() {
    var workbook = new Excel.Workbook();
    // Tab Title
    var worksheet = workbook.addWorksheet('Senarai Kumpulan');
    // Columns to display
    const col = ['No.', 'Nama Kumpulan', 'Keterangan', 'Jabatan', 'Bil. Pengguna', 'Tarikh Kemaskini'];
    worksheet.mergeCells('A1', 'F1'); // title merge cell
    worksheet.getCell('A1').value = 'Laporan Kumpulan Pengguna'; // title
    worksheet.getRow(2).values = col;
    // Column key
    worksheet.columns = [
      { key: 'No', width: 5 },
      { key: 'kumpulanName', width: 25 },
      { key: 'keterangan', width: 25 },
      { key: 'jabatanId', width: 10 },
      { key: 'bilPengguna', width: 10 },
      { key: 'tarikhKemaskini', width: 15 },
    ];

    let i = 1;
    this.showSpinner = true;
    // get list of data
    this.pentadbiranService.getKumpulanList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          this.showSpinner = false;
          let list =  items.list;
          for (var key in list) {
            worksheet.addRow([i, list[key].kumpulanName, list[key].keterangan, list[key].jabatanId, list[key].bilPengguna, list[key].tarikhKemaskini])
            i++;
          }
        });

    worksheet.eachRow(function (row, _rowNumber) {
      row.eachCell(function (cell, _colNumber) {
        cell.border = {
          top: { style: 'thin' },
          left: { style: 'thin' },
          bottom: { style: 'thin' },
          right: { style: 'thin' }
        };
      });
    });

    workbook.xlsx.writeBuffer().then(data => {
      const blob = new Blob([data], { type: this.blobType });
      // create excell file
      FileSaver.saveAs(blob, 'LaporanKumpulanPengguna.xlsx');
    });
  }

  // multiple delete function
  delete() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.Id }));
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: 'Rekod yang telah hapus tidak akan dikembalikan!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        this.pentadbiranService.deletekumpulanPengguna(selectedItems)
            .subscribe(resultDelete => {
              this.showSpinner = false;
              if (resultDelete.ReturnCode == 204) {
                Swal.fire(
                    'Error',
                    resultDelete.ResponseMessage,
                    'error'
                );
              }
              else {
                Swal.fire(
                    '',
                    resultDelete.ResponseMessage,
                    'success'
                );
              }
              this.getKumpulanList();
            });
      }
    })
  }

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      kataKunci: ['', []],
        }, {}
    );
  }
}

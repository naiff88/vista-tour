/* tslint:disable:max-line-length comment-format no-trailing-whitespace */
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import { bufferTime, distinctUntilChanged, filter, map, scan, share, take, tap } from 'rxjs/operators';
import * as _ from 'lodash';
import { Pager, PageInfo } from '../../pagingnation';
import { API_BASE_URL } from 'src/shared/service-proxies/service-proxies';
import { ProgressBarState, Pair } from './models';

export enum PentadbiranApiEndpoint {
  UploadImage = '/api/',
  DeleteImage = '/api/',
}

@Injectable({
  providedIn: 'root'
})
export class PentadbiranService {
  /**
   * Indicate data processing. Binded in dialog
   */
  private processingSubject = new Subject<ProgressBarState>();
  private baseUrl: string;
  http: HttpClient;


  /**
   *  Pipeline of async operation to handle loading state of product dialog
   */
  isBusyStream: Observable<boolean> = this.processingSubject.asObservable()
    .pipe(
      bufferTime(300),                  // buffers the progress Observable values for 100ms period
      filter(states => states.length > 0),   // ignore empty streams
      scan((total: number, states: ProgressBarState[]) => total + _.sum(states), 0),
      map((diff: number) => diff !== 0),       // diff between count of processing and already completed http requests
      distinctUntilChanged(),
      share()                                         // Returns a new Observable that multicasts (shares) the original Observable
    );

  set isBusy(value: boolean) {
    const state: number = value ? ProgressBarState.Show : ProgressBarState.Hide;
    this.processingSubject.next(state);
  }

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }


  ///////////////////////////////////////////////////
  //////////////// PRODUCTS SERVICE /////////////////
  ///////////////////////////////////////////////////



  getProductList(page: Pager, orderTabIndex: number): Observable<any> {
    console.log('getProductList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
      .set('RowsPerPage', (page.RowsPerPage || 0) + '')
      .set('PageNumber', (page.PageNumber || 0) + '')
      .set('OrderScript', page.OrderScript || '')
      .set('ColumnFilterScript', page.ColumnFilterScript || '')
      .set('orderStatusId', (orderTabIndex || 0) + '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { productId: 1, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Adidas Long Sleeve ', categories: 'T-Shirt', skuNo: '10515825', variation: 'XL', price: 300, stock: 14  },
      { productId: 2, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Adidas Fit Series ', categories: 'Shirt', skuNo: '181651', variation: 'M', price: 150, stock: 7  },
      { productId: 3, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Nike Air Sport ', categories: 'Shoes', skuNo: '877451', variation: '10', price: 450, stock: 5  },
      { productId: 4, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Tudung Bawal Bidang 56 ', categories: 'Tudung', skuNo: '24484', variation: '56', price: 20, stock: 34  },
    ];
    const pageInfo: any = { RowCount: 4 };
    return of({ list, pageInfo });

  }

  getproductDetails(ProducttId: number): Observable<any> {
    const params = new HttpParams().set('ProducttId', String(ProducttId));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      Name: 'Adidas Long Sleeve',
      OrderNo: '13313313',
      Address: '2477, Jalan Permata 19, Taman Permata, 53300 Kuala Lumpur. WP Kuala Lumpur',
      OrderDateTime: '02/01/2020 09:20 AM',
      Email: 'razman@gmail.com',
      PhoneNo: '60127381781',
      CourierId: 1,
      TrackingNo: '',
      ShippingCost: 3.5,
      StatusId: 2,
      PaymentTypeId: 3,
      TransactionNo: '57X78969NB701535K',
      PaymentDate: '20/01/2020 04.22 PM',
      Amount: 0,
    };
    return of(record);
  }

//........................................................................................................
//.KKKK...KKKKK.UUUU...UUUU.UMMMMM...MMMMMM.PPPPPPPPP...UUUU...UUUU..LLLL..........AAAAA.....NNNN...NNNN..
//.KKKK..KKKKK..UUUU...UUUU.UMMMMM...MMMMMM.PPPPPPPPPP..UUUU...UUUU..LLLL..........AAAAA.....NNNNN..NNNN..
//.KKKK.KKKKK...UUUU...UUUU.UMMMMM...MMMMMM.PPPPPPPPPPP.UUUU...UUUU..LLLL.........AAAAAA.....NNNNN..NNNN..
//.KKKKKKKKK....UUUU...UUUU.UMMMMMM.MMMMMMM.PPPP...PPPP.UUUU...UUUU..LLLL.........AAAAAAA....NNNNNN.NNNN..
//.KKKKKKKK.....UUUU...UUUU.UMMMMMM.MMMMMMM.PPPP...PPPP.UUUU...UUUU..LLLL........AAAAAAAA....NNNNNN.NNNN..
//.KKKKKKKK.....UUUU...UUUU.UMMMMMM.MMMMMMM.PPPPPPPPPPP.UUUU...UUUU..LLLL........AAAAAAAA....NNNNNNNNNNN..
//.KKKKKKKK.....UUUU...UUUU.UMMMMMMMMMMMMMM.PPPPPPPPPP..UUUU...UUUU..LLLL........AAAA.AAAA...NNNNNNNNNNN..
//.KKKKKKKKK....UUUU...UUUU.UMMMMMMMMMMMMMM.PPPPPPPPP...UUUU...UUUU..LLLL.......AAAAAAAAAA...NNNNNNNNNNN..
//.KKKK.KKKKK...UUUU...UUUU.UMMMMMMMMMMMMMM.PPPP........UUUU...UUUU..LLLL.......AAAAAAAAAAA..NNNNNNNNNNN..
//.KKKK..KKKK...UUUU...UUUU.UMMM.MMMMM.MMMM.PPPP........UUUU...UUUU..LLLL.......AAAAAAAAAAA..NNNN.NNNNNN..
//.KKKK..KKKKK..UUUUUUUUUUU.UMMM.MMMMM.MMMM.PPPP........UUUUUUUUUUU..LLLLLLLLLLLAAA....AAAA..NNNN..NNNNN..
//.KKKK...KKKKK..UUUUUUUUU..UMMM.MMMMM.MMMM.PPPP.........UUUUUUUUU...LLLLLLLLLLLAAA.....AAAA.NNNN..NNNNN..
//.KKKK...KKKKK...UUUUUUU...UMMM.MMMMM.MMMM.PPPP..........UUUUUUU....LLLLLLLLLLLAAA.....AAAA.NNNN...NNNN..
//........................................................................................................

  saveKumpulan(model, JabatanId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Kumpulan Telah Disimpan!';
    const Id = JabatanId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  uploadFile(data: FormData): Observable<any> {
    console.log('uploadFile:::', data);
    this.isBusy = true;
    return this.http.post<string>(this.baseUrl + PentadbiranApiEndpoint.UploadImage, data)
      .pipe(
        // map((resp: any) => resp.Result[0].ImageFileName),
        map((resp: any) => {
          console.log('resp uploadFile ::::::::::::::: ', resp);
          const ImageFileName =  resp.Result[0].ImageFileName;
          /*const ReturnCode = resp.ReturnCode;
          const ResponseMessage = resp.ResponseMessage;
          const Id = productId;
          return { ReturnCode, ResponseMessage, Id };*/
          console.log('resp ImageFileName ::::::::::::::: ', ImageFileName);
          return ( ImageFileName );
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
  }
  
  getKumpulanList(page: Pager): Observable<any> {
    console.log('getKumpulanList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { kumpulanId: 1, kumpulanName: 'Pentadbir Sistem', keterangan: 'Pentadbir Sistem', jabatanId: 1, bilPengguna: 10, tarikhKemaskini: '22/10/2020'},
      { kumpulanId: 2, kumpulanName: 'Pentadbir Stesen Handheld', keterangan: 'Pentadbir Stesen Handheld', jabatanId: 2, bilPengguna: 20, tarikhKemaskini: '22/10/2020'},
      { kumpulanId: 3, kumpulanName: 'Pegawai Penguatkuasa', keterangan: 'Pegawai Penguatkuasa', jabatanId: 3, bilPengguna: 30, tarikhKemaskini: '22/10/2020'},
      { kumpulanId: 4, kumpulanName: 'Pegawai Pembayaran', keterangan: 'Pegawai Pembayaran', jabatanId: 3, bilPengguna: 40, tarikhKemaskini: '22/10/2020'},
    ];
    const pageInfo: any = { RowCount: 4 };
    return of({ list, pageInfo });

  }

  getKumpulanPenggunaDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      jabatanId: '1',
      keterangan: 'Keterangan',
      kumpulanName: 'JPK',
    };
    return of(record);
  }


  getJabatanList(): Observable<any> {
    console.log('getJabatanList ::::::::::::::::: ');

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'JPK' },
      { id: 2, name: 'JKAS' },
      { id: 3, name: 'JPPP' },
    ];

    return of(list);

  }

  getUserGroupList(page: Pager): Observable<any> {
    console.log('getUserGroupList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { userId: 1, userGroupId: 1, userName: '000', fullName: 'MOHD NAJMUDDIN BIN  ROJALAI', email: 'user@user.com'},
      { userId: 2, userGroupId: 2, userName: '001', fullName: 'ALI BIN ABU', email: 'user@user.com'},
    ];
    const pageInfo: any = { RowCount: 4 };
    return of({ list, pageInfo });

  }

  getUserGroupListAddUser(page: Pager, PromotionId: number): Observable<any> {
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
        .set('PromotionId', (PromotionId || 0) + '')
    ;
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    const list: any[] = [
      { userId: 1, fullName: 'Ahmad Bin Abu', email: 'user@user.com'},
      { userId: 2, fullName: 'Abu Bin Bakar', email: 'user@user.com'},
      { userId: 3, fullName: 'Bakar Bin Ali', email: 'user@user.com'},
    ];
    const pageInfo: any = { RowCount: list.length };
    return of({ list, pageInfo });
  }

  addUserToUserGroup(ids: Array<{ id: number }>, JabatanId: number) {
    console.log('addUserToUserGroup ids :::::::::::::::: ', ids);
    console.log('addUserToUserGroup JabatanId :::::::::::::::: ', JabatanId);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Ahli Kumpulan Berjaya Ditambah!';
    return of({ ReturnCode, ResponseMessage });
  }

  getPermissionItem(): Observable<any> {
    console.log('getPermissionItem ::::::::::::::::: ');

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'Add' },
      { id: 2, name: 'Edit' },
      { id: 3, name: 'Delete' },
    ];

    return of(list);

  }

  deletekumpulanPengguna(ids: Array<{ id: number }>) {
    console.log('deletekumpulanPengguna ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod kumpulan pengguna telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }


//..........................................................................................................
//.PPPPPPPPP...PEEEEEEEEEE.ENNN...NNNN.....GGGGGGG.......GGGGGGG....GUUU...UUUU..UNNN...NNNN.....AAAAA......
//.PPPPPPPPPP..PEEEEEEEEEE.ENNNN..NNNN...GGGGGGGGGG....GGGGGGGGGG...GUUU...UUUU..UNNNN..NNNN.....AAAAA......
//.PPPPPPPPPPP.PEEEEEEEEEE.ENNNN..NNNN..NGGGGGGGGGGG..GGGGGGGGGGGG..GUUU...UUUU..UNNNN..NNNN....AAAAAA......
//.PPPP...PPPP.PEEE........ENNNNN.NNNN..NGGGG..GGGGG..GGGGG..GGGGG..GUUU...UUUU..UNNNNN.NNNN....AAAAAAA.....
//.PPPP...PPPP.PEEE........ENNNNN.NNNN.NNGGG....GGG..GGGGG....GGG...GUUU...UUUU..UNNNNN.NNNN...AAAAAAAA.....
//.PPPPPPPPPPP.PEEEEEEEEE..ENNNNNNNNNN.NNGG..........GGGG...........GUUU...UUUU..UNNNNNNNNNN...AAAAAAAA.....
//.PPPPPPPPPP..PEEEEEEEEE..ENNNNNNNNNN.NNGG..GGGGGGGGGGGG..GGGGGGGG.GUUU...UUUU..UNNNNNNNNNN...AAAA.AAAA....
//.PPPPPPPPP...PEEEEEEEEE..ENNNNNNNNNN.NNGG..GGGGGGGGGGGG..GGGGGGGG.GUUU...UUUU..UNNNNNNNNNN..NAAAAAAAAA....
//.PPPP........PEEE........ENNNNNNNNNN.NNGGG.GGGGGGGGGGGGG.GGGGGGGG.GUUU...UUUU..UNNNNNNNNNN..NAAAAAAAAAA...
//.PPPP........PEEE........ENNN.NNNNNN..NGGGG....GGGG.GGGGG....GGGG.GUUU...UUUU..UNNN.NNNNNN..NAAAAAAAAAA...
//.PPPP........PEEEEEEEEEE.ENNN..NNNNN..NGGGGGGGGGGG..GGGGGGGGGGGG..GUUUUUUUUUU..UNNN..NNNNN.NNAA....AAAA...
//.PPPP........PEEEEEEEEEE.ENNN..NNNNN...GGGGGGGGGG....GGGGGGGGGG....UUUUUUUUU...UNNN..NNNNN.NNAA.....AAAA..
//.PPPP........PEEEEEEEEEE.ENNN...NNNN.....GGGGGGG.......GGGGGGG......UUUUUUU....UNNN...NNNNNNNAA.....AAAA..
//..........................................................................................................

  getUserList(page: Pager): Observable<any> {
    console.log('getUserList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { userId: 1, userName: 'user1', fullName: 'NORAINI BT HAMZAH', group: 'Kakitangan Tunda', email: 'user@user.com', status: 'INACTIVE', dateCreated: '22/10/2020'},
      { userId: 2, userName: 'user2', fullName: 'KHAIRUL BIN JAMAL', group: 'Pegawai Penguatkuasa', email: 'user@user.com', status: 'ACTIVE', dateCreated: '22/10/2020'},
    ];
    const pageInfo: any = { RowCount: 4 };
    return of({ list, pageInfo });

  }

  getPenggunaDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      userName: 'noraini',
      fullName: 'NORAINI BT HAMZAH',
      group: 'JPK',
      email: 'noraini@jpk.com',
      jabatanId: '1',
    };
    return of(record);
  }

  savePengguna(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Pengguna Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deletePengguna(ids: Array<{ id: number }>) {
    console.log('deletePengguna ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod pengguna telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

//.................................................................................................................
//.....AAAAA.....KKKK...KKKKK.TTTTTTTTTTA..AAAA.......... III.INNNN...NNNN.NDDDDDDDDD...DUUU....UUUU.UKKK....KKKK..
//.....AAAAA.....KKKK..KKKKK..TTTTTTTTTTA.AAAAAA......... III.INNNN...NNNN.NDDDDDDDDDD..DUUU....UUUU.UKKK...KKKKK..
//....AAAAAA.....KKKK.KKKKK...TTTTTTTTTTA.AAAAAA......... III.INNNNN..NNNN.NDDDDDDDDDDD.DUUU....UUUU.UKKK..KKKKK...
//....AAAAAAA....KKKKKKKKK.......TTTT.....AAAAAAA........ III.INNNNNN.NNNN.NDDD....DDDD.DUUU....UUUU.UKKK.KKKKK....
//...AAAAAAAA....KKKKKKKK........TTTT....AAAAAAAA........ III.INNNNNN.NNNN.NDDD....DDDDDDUUU....UUUU.UKKKKKKKK.....
//...AAAAAAAA....KKKKKKKK........TTTT....AAAAAAAA........ III.INNNNNNNNNNN.NDDD.....DDDDDUUU....UUUU.UKKKKKKK......
//...AAAA.AAAA...KKKKKKKK........TTTT....AAAA.AAAA....... III.INNNNNNNNNNN.NDDD.....DDDDDUUU....UUUU.UKKKKKKKK.....
//..AAAAAAAAAA...KKKKKKKKK.......TTTT...AAAAAAAAAA....... III.INNN.NNNNNNN.NDDD.....DDDDDUUU....UUUU.UKKKKKKKKK....
//..AAAAAAAAAAA..KKKK.KKKKK......TTTT...AAAAAAAAAAA...... III.INNN.NNNNNNN.NDDD....DDDDDDUUUU...UUUU.UKKKK.KKKK....
//..AAAAAAAAAAA..KKKK..KKKK......TTTT..TAAAAAAAAAAA...... III.INNN..NNNNNN.NDDD....DDDD..UUUU..UUUUU.UKKK..KKKKK...
//.AAAA....AAAA..KKKK..KKKKK.....TTTT..TAAA....AAAA...... III.INNN..NNNNNN.NDDDDDDDDDDD..UUUUUUUUUUU.UKKK...KKKKK..
//.AAAA.....AAAA.KKKK...KKKKK....TTTT..TAAA....AAAAA..... III.INNN...NNNNN.NDDDDDDDDDD...UUUUUUUUUU..UKKK....KKKK..
//.AAAA.....AAAA.KKKK...KKKKK....TTTT.TTAAA.....AAAA..... III.INNN....NNNN.NDDDDDDDDD......UUUUUUU...UKKK....KKKK..
//.................................................................................................................


  getAktaIndukList(page: Pager): Observable<any> {
    // console.log('getAktaIndukList ::::::::::::::::: ');
    // console.log('page ::::::::::::::::: ', page);
    // console.log('orderTabIndex ::::::::::::::::: ', orderTabIndex);
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    // console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { aktaIndukId: 1, jabatan: 'JPK', jenis: 'TRAFIK', name: 'AKTA PENGANGKUTAN JALAN 1987 (TRAFIK)', status: 'INACTIVE'},
    ];
    const pageInfo: any = { RowCount: 1 };
    return of({ list, pageInfo });

  }

  getAktaIndukDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      statusId: '1',
      name: 'AKTA PENGANGKUTAN JALAN 1987 (TRAFIK)',
      jenisId: '1',
      jabatanId: '1',
    };
    return of(record);
  }

  getJenisList(): Observable<any> {
    console.log('getJenisList ::::::::::::::::: ');

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'TRAFIK' },
    ];

    return of(list);

  }

  getStatusList(): Observable<any> {
    console.log('getStatusList ::::::::::::::::: ');

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'Active' },
      { id: 2, name: 'Inactive' },
    ];

    return of(list);

  }

  saveAktaInduk(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Akta Induk Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deleteAktaInduk(ids: Array<{ id: number }>) {
    console.log('deleteAktaInduk ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod akta induk telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

//...............................................................................................................................
//.PPPPPPPPP...PEEEEEEEEEE.ERRRRRRRRR...RUUU...UUUU..UNNN...NNNN..NTTTTTTTTTTTUUU...UUUU..UKKK...KKKKK....AAAAA.....ANNN...NNNN..
//.PPPPPPPPPP..PEEEEEEEEEE.ERRRRRRRRRR..RUUU...UUUU..UNNNN..NNNN..NTTTTTTTTTTTUUU...UUUU..UKKK..KKKKK.....AAAAA.....ANNNN..NNNN..
//.PPPPPPPPPPP.PEEEEEEEEEE.ERRRRRRRRRR..RUUU...UUUU..UNNNN..NNNN..NTTTTTTTTTTTUUU...UUUU..UKKK.KKKKK.....AAAAAA.....ANNNN..NNNN..
//.PPPP...PPPP.PEEE........ERRR...RRRRR.RUUU...UUUU..UNNNNN.NNNN.....TTTT....TUUU...UUUU..UKKKKKKKK......AAAAAAA....ANNNNN.NNNN..
//.PPPP...PPPP.PEEE........ERRR...RRRRR.RUUU...UUUU..UNNNNN.NNNN.....TTTT....TUUU...UUUU..UKKKKKKK......AAAAAAAA....ANNNNN.NNNN..
//.PPPPPPPPPPP.PEEEEEEEEE..ERRRRRRRRRR..RUUU...UUUU..UNNNNNNNNNN.....TTTT....TUUU...UUUU..UKKKKKKK......AAAAAAAA....ANNNNNNNNNN..
//.PPPPPPPPPP..PEEEEEEEEE..ERRRRRRRRRR..RUUU...UUUU..UNNNNNNNNNN.....TTTT....TUUU...UUUU..UKKKKKKK......AAAA.AAAA...ANNNNNNNNNN..
//.PPPPPPPPP...PEEEEEEEEE..ERRRRRRR.....RUUU...UUUU..UNNNNNNNNNN.....TTTT....TUUU...UUUU..UKKKKKKKK....KAAAAAAAAA...ANNNNNNNNNN..
//.PPPP........PEEE........ERRR.RRRR....RUUU...UUUU..UNNNNNNNNNN.....TTTT....TUUU...UUUU..UKKK.KKKKK...KAAAAAAAAAA..ANNNNNNNNNN..
//.PPPP........PEEE........ERRR..RRRR...RUUU...UUUU..UNNN.NNNNNN.....TTTT....TUUU...UUUU..UKKK..KKKK...KAAAAAAAAAA..ANNN.NNNNNN..
//.PPPP........PEEEEEEEEEE.ERRR..RRRRR..RUUUUUUUUUU..UNNN..NNNNN.....TTTT....TUUUUUUUUUU..UKKK..KKKKK.KKAA....AAAA..ANNN..NNNNN..
//.PPPP........PEEEEEEEEEE.ERRR...RRRRR..UUUUUUUUU...UNNN..NNNNN.....TTTT.....UUUUUUUUU...UKKK...KKKKKKKAA.....AAAA.ANNN..NNNNN..
//.PPPP........PEEEEEEEEEE.ERRR....RRRR...UUUUUUU....UNNN...NNNN.....TTTT......UUUUUUU....UKKK...KKKKKKKAA.....AAAA.ANNN...NNNN..
//...............................................................................................................................

  getPeruntukanUndangList(page: Pager): Observable<any> {
    console.log('getPeruntukanUndangList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { peruntukanUndangId: 1, kedudukan: 0, jabatan: 'JPK', aktaInduk: 'AKTA JALAN PARIT DAN BANGUNAN 1974 (AM)', namaPeruntukanUndang: 'AKTA JALAN PARIT DAN BANGUNAN 1974', singkatan: 'AKTA JLN PARIT & BANGUNAN 1974', status: 'ACTIVE'},
    ];
    const pageInfo: any = { RowCount: 1 };
    return of({ list, pageInfo });

  }

  getPeruntukanUndangDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      kedudukan: '0',
      aktaIndukId: '1',
      namaPeruntukanUndang: 'AKTA JALAN PARIT DAN BANGUNAN 1974',
      jabatanId: '1',
      singkatan: 'AKTA JLN PARIT & BANGUNAN 1974',
      statusId: '1',
    };
    return of(record);
  }

  savePeruntukanUndang(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Peruntukan Undang-Undang Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deletePeruntukanUndang(ids: Array<{ id: number }>) {
    console.log('deletePeruntukanUndang ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Peruntukan Undang-Undang telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

//........................................................................................
//...SSSSSSS....EEEEEEEEEEE.KKKK...KKKKK..SSSSSSS...SYYY....YYYY.EEEEEEEEEEE.NNNN...NNNN..
//..SSSSSSSSS...EEEEEEEEEEE.KKKK..KKKKK..SSSSSSSSS..SYYYY..YYYYY.EEEEEEEEEEE.NNNNN..NNNN..
//..SSSSSSSSSS..EEEEEEEEEEE.KKKK.KKKKK...SSSSSSSSSS..YYYY..YYYY..EEEEEEEEEEE.NNNNN..NNNN..
//.SSSSS..SSSS..EEEE........KKKKKKKKK...KSSSS..SSSS..YYYYYYYYY...EEEE........NNNNNN.NNNN..
//.SSSSS........EEEE........KKKKKKKK....KSSSS.........YYYYYYYY...EEEE........NNNNNN.NNNN..
//..SSSSSSS.....EEEEEEEEEE..KKKKKKKK.....SSSSSSS.......YYYYYY....EEEEEEEEEE..NNNNNNNNNNN..
//...SSSSSSSSS..EEEEEEEEEE..KKKKKKKK......SSSSSSSSS....YYYYYY....EEEEEEEEEE..NNNNNNNNNNN..
//.....SSSSSSS..EEEEEEEEEE..KKKKKKKKK.......SSSSSSS.....YYYY.....EEEEEEEEEE..NNNNNNNNNNN..
//........SSSSS.EEEE........KKKK.KKKKK.........SSSSS....YYYY.....EEEE........NNNNNNNNNNN..
//.SSSS....SSSS.EEEE........KKKK..KKKK..KSSS....SSSS....YYYY.....EEEE........NNNN.NNNNNN..
//.SSSSSSSSSSSS.EEEEEEEEEEE.KKKK..KKKKK.KSSSSSSSSSSS....YYYY.....EEEEEEEEEEE.NNNN..NNNNN..
//..SSSSSSSSSS..EEEEEEEEEEE.KKKK...KKKKK.SSSSSSSSSS.....YYYY.....EEEEEEEEEEE.NNNN..NNNNN..
//...SSSSSSSS...EEEEEEEEEEE.KKKK...KKKKK..SSSSSSSS......YYYY.....EEEEEEEEEEE.NNNN...NNNN..
//........................................................................................

  getSeksyenKesalahanList(page: Pager): Observable<any> {
    console.log('getSeksyenKesalahanList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { seksyenId: 1, peruntukanUndang: 'AKTA JALAN PARIT DAN BANGUNAN 1974', noSeksyen: 'SEK46', noSubSeksyen: '(1)(g)', butiranKesalahan: 'SALAH PARKING',  kadarKompaun: '250', kadar: '100', cajMaksimum: '500', status: 'ACTIVE'},
    ];
    const pageInfo: any = { RowCount: 1 };
    return of({ list, pageInfo });

  }

  getSeksyenKesalahanDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      noSeksyen: 'SEK46',
      noSubseksyen: '(1)(g)',
      peruntukanUndangId: '1',
      butiranKesalahan: 'SALAH PARKING',
      kadarKompaun: '250',
      cajMaksimum: '500',
      statusId: '1',
    };
    return of(record);
  }

  saveSeksyenKesalahan(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Seksyen Kesalahan Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deleteSeksyenKesalahan(ids: Array<{ id: number }>) {
    console.log('deleteSeksyenKesalahan ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Seksyen Kesalahan telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

//......................................................................................................................................
//....CCCCCCC.......AAAAA.....RRRRRRRRRR..IIIII....AAAAA.....NNNN...NNNN......IIIII.NNNN...NNNN..DDDDDDDDD....UUUU...UUUU..KKKK...KKKK..
//...CCCCCCCCC......AAAAA.....RRRRRRRRRRR.IIIII....AAAAA.....NNNNN..NNNN......IIIII.NNNNN..NNNN..DDDDDDDDDD...UUUU...UUUU..KKKK..KKKKK..
//..CCCCCCCCCCC....AAAAAA.....RRRRRRRRRRR.IIIII...AAAAAA.....NNNNN..NNNN......IIIII.NNNNN..NNNN..DDDDDDDDDDD..UUUU...UUUU..KKKK.KKKKK...
//..CCCC...CCCCC...AAAAAAA....RRRR...RRRRRIIIII...AAAAAAA....NNNNNN.NNNN......IIIII.NNNNNN.NNNN..DDDD...DDDD..UUUU...UUUU..KKKKKKKKK....
//.CCCC.....CCC...AAAAAAAA....RRRR...RRRRRIIIII..AAAAAAAA....NNNNNN.NNNN......IIIII.NNNNNN.NNNN..DDDD....DDDD.UUUU...UUUU..KKKKKKKK.....
//.CCCC...........AAAAAAAA....RRRRRRRRRRR.IIIII..AAAAAAAA....NNNNNNNNNNN......IIIII.NNNNNNNNNNN..DDDD....DDDD.UUUU...UUUU..KKKKKKKK.....
//.CCCC...........AAAA.AAAA...RRRRRRRRRRR.IIIII..AAAA.AAAA...NNNNNNNNNNN......IIIII.NNNNNNNNNNN..DDDD....DDDD.UUUU...UUUU..KKKKKKKK.....
//.CCCC..........AAAAAAAAAA...RRRRRRRR....IIIII.AAAAAAAAAA...NNNNNNNNNNN......IIIII.NNNNNNNNNNN..DDDD....DDDD.UUUU...UUUU..KKKKKKKKK....
//.CCCC.....CCC..AAAAAAAAAAA..RRRR.RRRR...IIIII.AAAAAAAAAAA..NNNNNNNNNNN......IIIII.NNNNNNNNNNN..DDDD....DDDD.UUUU...UUUU..KKKK.KKKKK...
//..CCCC...CCCCC.AAAAAAAAAAA..RRRR..RRRR..IIIII.AAAAAAAAAAA..NNNN.NNNNNN......IIIII.NNNN.NNNNNN..DDDD...DDDDD.UUUU...UUUU..KKKK..KKKK...
//..CCCCCCCCCCC.AAAA....AAAA..RRRR..RRRRR.IIIIIAAAA....AAAA..NNNN..NNNNN......IIIII.NNNN..NNNNN..DDDDDDDDDDD..UUUUUUUUUUU..KKKK..KKKKK..
//...CCCCCCCCCC.AAAA.....AAAA.RRRR...RRRRRIIIIIAAAA.....AAAA.NNNN..NNNNN......IIIII.NNNN..NNNNN..DDDDDDDDDD....UUUUUUUUU...KKKK...KKKK..
//....CCCCCCC..CAAAA.....AAAA.RRRR....RRRRIIIIIAAAA.....AAAA.NNNN...NNNN......IIIII.NNNN...NNNN..DDDDDDDDD......UUUUUUU....KKKK...KKKK..
//......................................................................................................................................

  getCarianIndukList(page: Pager): Observable<any> {
    console.log('getCarianIndukList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { carianId: 1, kod: '1', jenis: 'STATE', kedudukan: '18', nama: 'W.P. PUTRAJAYA',  keterangan: 'State', terkunci: 'Locked'},
    ];
    const pageInfo: any = { RowCount: 1 };
    return of({ list, pageInfo });

  }

  getCarianIndukDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      kod: '1',
      jenisId: '1',
      kedudukan: '18',
      nama: 'W.P Putrajaya',
      keterangan: 'State',
      terkunci: 'Locked',
    };
    return of(record);
  }

  getJenisCarianInduk(): Observable<any> {
    console.log('getJenisList ::::::::::::::::: ');

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'STATE' },
    ];

    return of(list);

  }

  saveCarianInduk(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Carian Induk Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deleteCarianInduk(ids: Array<{ id: number }>) {
    console.log('deleteCarianInduk ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Carian Induk telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

//....................................................................................................................
//.KKKK...KKKKK.EEEEEEEEEEE.ENNN...NNNN..NDDDDDDDD....DEEEEEEEEEE.ERRRRRRRRR......AAAAA........AAAAA.....ANNN...NNNN..
//.KKKK..KKKKK..EEEEEEEEEEE.ENNNN..NNNN..NDDDDDDDDD...DEEEEEEEEEE.ERRRRRRRRRR.....AAAAA........AAAAA.....ANNNN..NNNN..
//.KKKK.KKKKK...EEEEEEEEEEE.ENNNN..NNNN..NDDDDDDDDDD..DEEEEEEEEEE.ERRRRRRRRRR....AAAAAA.......AAAAAA.....ANNNN..NNNN..
//.KKKKKKKKK....EEEE........ENNNNN.NNNN..NDDD...DDDD..DEEE........ERRR...RRRRR...AAAAAAA......AAAAAAA....ANNNNN.NNNN..
//.KKKKKKKK.....EEEE........ENNNNN.NNNN..NDDD....DDDD.DEEE........ERRR...RRRRR..AAAAAAAA.....AAAAAAAA....ANNNNN.NNNN..
//.KKKKKKKK.....EEEEEEEEEE..ENNNNNNNNNN..NDDD....DDDD.DEEEEEEEEE..ERRRRRRRRRR...AAAAAAAA.....AAAAAAAA....ANNNNNNNNNN..
//.KKKKKKKK.....EEEEEEEEEE..ENNNNNNNNNN..NDDD....DDDD.DEEEEEEEEE..ERRRRRRRRRR...AAAA.AAAA....AAAA.AAAA...ANNNNNNNNNN..
//.KKKKKKKKK....EEEEEEEEEE..ENNNNNNNNNN..NDDD....DDDD.DEEEEEEEEE..ERRRRRRR.....RAAAAAAAAA...AAAAAAAAAA...ANNNNNNNNNN..
//.KKKK.KKKKK...EEEE........ENNNNNNNNNN..NDDD....DDDD.DEEE........ERRR.RRRR....RAAAAAAAAAA..AAAAAAAAAAA..ANNNNNNNNNN..
//.KKKK..KKKK...EEEE........ENNN.NNNNNN..NDDD...DDDDD.DEEE........ERRR..RRRR...RAAAAAAAAAA..AAAAAAAAAAA..ANNN.NNNNNN..
//.KKKK..KKKKK..EEEEEEEEEEE.ENNN..NNNNN..NDDDDDDDDDD..DEEEEEEEEEE.ERRR..RRRRR.RRAA....AAAA.AAAA....AAAA..ANNN..NNNNN..
//.KKKK...KKKKK.EEEEEEEEEEE.ENNN..NNNNN..NDDDDDDDDD...DEEEEEEEEEE.ERRR...RRRRRRRAA.....AAAAAAAA.....AAAA.ANNN..NNNNN..
//.KKKK...KKKKK.EEEEEEEEEEE.ENNN...NNNN..NDDDDDDDD....DEEEEEEEEEE.ERRR....RRRRRRAA.....AAAAAAAA.....AAAA.ANNN...NNNN..
//....................................................................................................................

  getJenamaKenderaanList(page: Pager): Observable<any> {
    console.log('getJenamaKenderaanList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'YAMAHA', status: 'ACTIVE'},
      { id: 2, name: 'HONDA', status: 'INACTIVE'},
    ];
    const pageInfo: any = { RowCount: 1 };
    return of({ list, pageInfo });

  }

  saveJenamaKenderaan(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Jenama Kenderaan Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  getJenamaKenderaanDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      jenama: 'YAMAHA',
      statusId: '1',
    };
    return of(record);
  }

  deleteJenamaKenderaan(ids: Array<{ id: number }>) {
    console.log('deleteJenamaKenderaan ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Jenama Kenderaan telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getModelKenderaanList(page: Pager): Observable<any> {
    console.log('getModelKenderaanList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, jenama: 'YAMAHA', model: 'LAGENDA', status: 'ACTIVE'},
      { id: 2, jenama: 'YAMAHA', model: 'Y-SUKU', status: 'ACTIVE'},
      { id: 2, jenama: 'HONDA', model: 'CBR-1000R', status: 'ACTIVE'},
    ];
    const pageInfo: any = { RowCount: 1 };
    return of({ list, pageInfo });

  }

  getModelKenderaanDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      jenamaKenderaanId: '1',
      statusId: '1',
      model: 'Y-SUKU',
    };
    return of(record);
  }

  saveModelKenderaan(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Model Kenderaan Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deleteModelKenderaan(ids: Array<{ id: number }>) {
    console.log('deleteModelKenderaan ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Model Kenderaan telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getJenisKenderaanList(page: Pager): Observable<any> {
    console.log('getJenisKenderaanList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, jenisKenderaan: 'VAN',  status: 'ACTIVE'},
      { id: 2, jenisKenderaan: 'TRAKTOR',  status: 'ACTIVE'},
      { id: 3, jenisKenderaan: 'TRAILER',  status: 'ACTIVE'},
      { id: 4, jenisKenderaan: 'TEKSI',  status: 'ACTIVE'},
    ];
    const pageInfo: any = { RowCount: 1 };
    return of({ list, pageInfo });

  }

  getJenisKenderaanDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      jenisKenderaan: 'VAN',
      statusId: '1',
    };
    return of(record);
  }

  saveJenisKenderaan(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Jenis Kenderaan Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deleteJenisKenderaan(ids: Array<{ id: number }>) {
    console.log('deleteJenisKenderaan ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Jenis Kenderaan telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

//.............................................................................................
//.KKKK...KKKKK....AAAAA..AAAWW..WWWWW...WWWW..AAAAA......ASSSSSS.......AAAAA.....AANN...NNNN..
//.KKKK..KKKKK.....AAAAA...AAWW..WWWWW..WWWW...AAAAA.....AASSSSSSS......AAAAA.....AANNN..NNNN..
//.KKKK.KKKKK.....AAAAAA...AAWW..WWWWWW.WWWW..AAAAAA.....AASSSSSSSS....AAAAAA.....AANNN..NNNN..
//.KKKKKKKKK......AAAAAAA..AAWW.WWWWWWW.WWWW..AAAAAAA...AAASS..SSSS....AAAAAAA....AANNNN.NNNN..
//.KKKKKKKK......AAAAAAAA..AAWW.WWWWWWW.WWWW.WAAAAAAA...AAASS.........SAAAAAAA....AANNNN.NNNN..
//.KKKKKKKK......AAAAAAAA...AWWWWWWWWWW.WWW..WAAAAAAA....AASSSSS......SAAAAAAA....AANNNNNNNNN..
//.KKKKKKKK......AAAA.AAAA..AWWWWWW.WWWWWWW..WAAA.AAAA....ASSSSSSSS...SAAA.AAAA...AANNNNNNNNN..
//.KKKKKKKKK....AAAAAAAAAA..AWWWWWW.WWWWWWW.WWAAAAAAAA......SSSSSSS..SSAAAAAAAA...AANNNNNNNNN..
//.KKKK.KKKKK...AAAAAAAAAAA.AWWWWWW.WWWWWWW.WWAAAAAAAAA........SSSSS.SSAAAAAAAAA..AANNNNNNNNN..
//.KKKK..KKKK...AAAAAAAAAAA.AWWWWWW.WWWWWWW.WWAAAAAAAAA.AAAS....SSSS.SSAAAAAAAAA..AANN.NNNNNN..
//.KKKK..KKKKK.KAAA....AAAA..WWWWW...WWWWW.WWWA....AAAA.AAASSSSSSSSSSSSA....AAAA..AANN..NNNNN..
//.KKKK...KKKKKKAAA.....AAAA.WWWWW...WWWWW.WWWA.....AAAA.AASSSSSSSS.SSSA.....AAAA.AANN..NNNNN..
//.KKKK...KKKKKKAAA.....AAAA.WWWWW...WWWWWWWWWA.....AAAA..ASSSSSSS.SSSSA.....AAAA.AANN...NNNN..
//.............................................................................................

  getZonPegawaiList(page: Pager): Observable<any> {
    console.log('getZonPegawaiList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, zone: 'ZU-WANGSA MAJU', status: 'ACTIVE'},
      { id: 2, zone: 'ZU-SETIAWANGSA', status: 'ACTIVE'},
      { id: 3, zone: 'ZU-KEPONG', status: 'ACTIVE'},
    ];
    const pageInfo: any = { RowCount: 3 };
    return of({ list, pageInfo });

  }

  getZonPegawaiDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      zon: 'ZU-WANGSA MAJU',
      statusId: '1',
    };
    return of(record);
  }

  saveZonePegawai(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Zon Pegawai Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deleteZonPegawai(ids: Array<{ id: number }>) {
    console.log('deleteZonPegawai ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Zon Pegawai telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getParlimenList(page: Pager): Observable<any> {
    console.log('getParlimen ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'WANGSA MAJU', zone: 'ZON UTARA', status: 'ACTIVE'},
      { id: 2, name: 'SETIAWANGSA', zone: 'ZON TENGAH', status: 'ACTIVE'},
      { id: 3, name: 'SEGAMBUT', zone: 'ZON SELATAN', status: 'ACTIVE'},
    ];
    const pageInfo: any = { RowCount: 3 };
    return of({ list, pageInfo });

  }

  getParlimenDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      zonId: '1',
      namaParlimen: 'WANGSA MAJU',
      statusId: '1',
    };
    return of(record);
  }

  getZonList(): Observable<any> {
    console.log('getZonList ::::::::::::::::: ');

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'ZON TENGAH' },
      { id: 2, name: 'ZON UTARA' },
    ];

    return of(list);

  }

  saveParlimen(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Parlimen Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deleteParlimen(ids: Array<{ id: number }>) {
    console.log('deleteParlimenList ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Parlimen telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getLokasiList(page: Pager): Observable<any> {
    console.log('getLokasiList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'TAMAN TUN DR.ISMAIL', kodBB: 'SG 07/01', parlimen: 'SEGAMBUT', status: 'ACTIVE'},
      { id: 2, name: 'TAMAN DESA', kodBB: 'SP 11/03', parlimen: 'SEPUTEH', status: 'ACTIVE'},
      { id: 3, name: 'SHAMELIN', kodBB: 'CH 09/02', parlimen: 'CHERAS', status: 'ACTIVE'},
    ];
    const pageInfo: any = { RowCount: 3 };
    return of({ list, pageInfo });

  }

  getLokasiDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      parlimenId: '1',
      kawasan: 'TAMAN TUN DR.ISMAIL',
      kodBB: 'SG 07/01',
      statusId: '1',
    };
    return of(record);
  }

  saveLokasi(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Lokasi Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deleteLokasiList(ids: Array<{ id: number }>) {
    console.log('deleteParlimenList ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Lokasi telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getKawasanKesalahanList(page: Pager): Observable<any> {
    console.log('getKawasanKesalahanList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'WISMA INDAH', lokasi: 'MUTIARA', status: 'ACTIVE'},
      { id: 2, name: 'WEST ROAD 15', lokasi: 'JINJANG', status: 'ACTIVE'},
    ];
    const pageInfo: any = { RowCount: 2 };
    return of({ list, pageInfo });

  }

  getKawasanKesalahanDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      lokasiId: '1',
      kawasan: 'WISMA INDAH',
      statusId: '1',
    };
    return of(record);
  }

  deleteKawasanKesalahanList(ids: Array<{ id: number }>) {
    console.log('deleteKawasanKesalahanList ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Kawasan Kesalahan telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

//........................................................................................
//.PPPPPPPPP.....AAAA.....AAANN...NNNN....GGGGGGGG...GGGK....KKKKK....AAAA....AAAATTTTTT..
//.PPPPPPPPPP...AAAAAA....AAANN...NNNN...NGGGGGGGGG..GGGK...KKKKK....AAAAAA...AAAATTTTTT..
//.PPPPPPPPPPP..AAAAAA....AAANNN..NNNN..NNGGGGGGGGGG.GGGK..KKKKK.....AAAAAA...AAAATTTTTT..
//.PPPP...PPPP..AAAAAAA...AAANNNN.NNNN.NNNGG...GGGGG.GGGK.KKKKK......AAAAAAA......TTTT....
//.PPPP...PPPP.PAAAAAAA...AAANNNN.NNNN.NNNG.....GGG..GGGKKKKKK......KAAAAAAA......TTTT....
//.PPPPPPPPPPP.PAAAAAAA...AAANNNNNNNNN.NNNG..........GGGKKKKK.......KAAAAAAA......TTTT....
//.PPPPPPPPPP..PAAA.AAAA..AAANNNNNNNNN.NNNG..GGGGGGG.GGGKKKKKK......KAAA.AAAA.....TTTT....
//.PPPPPPPPP..PPAAAAAAAA..AAAN.NNNNNNN.NNNG..GGGGGGG.GGGKKKKKKK....KKAAAAAAAA.....TTTT....
//.PPPP.......PPAAAAAAAAA.AAAN.NNNNNNN.NNNG..GGGGGGG.GGGKK.KKKK....KKAAAAAAAAA....TTTT....
//.PPPP......PPPAAAAAAAAA.AAAN..NNNNNN..NNGGG...GGGG.GGGK..KKKKK..KKKAAAAAAAAA....TTTT....
//.PPPP......PPPA....AAAA.AAAN..NNNNNN..NNGGGGGGGGGG.GGGK...KKKKK.KKKA....AAAA....TTTT....
//.PPPP......PPPA....AAAAAAAAN...NNNNN...NGGGGGGGGG..GGGK....KKKK.KKKA....AAAAA...TTTT....
//.PPPP.....PPPPA.....AAAAAAAN....NNNN.....GGGGGG....GGGK....KKKKKKKKA.....AAAA...TTTT....
//........................................................................................

  getPangkatPegawaiList(page: Pager): Observable<any> {
    console.log('getPangkatPegawaiList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, pangkat: 'L/KPL', noBadan: 'Ya', status: 'ACTIVE'},
      { id: 2, pangkat: 'KPL', noBadan: 'Ya', status: 'ACTIVE'},
    ];
    const pageInfo: any = { RowCount: 2 };
    return of({ list, pageInfo });

  }

  getPangkatPegawaiDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      pangkat: 'L/KPL',
      noBadanId: '1',
      statusId: '1',
    };
    return of(record);
  }

  savePangkatPegawai(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Pangkat Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deletePangkatPegawaiList(ids: Array<{ id: number }>) {
    console.log('deletePangkatPegawaiList ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Pangkat Pegawai telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  //.........................................................
//.IIIII.KKKK...KKKKK.LLLL..........AAAAA.....NNNN...NNNN..
//.IIIII.KKKK..KKKKK..LLLL..........AAAAA.....NNNNN..NNNN..
//.IIIII.KKKK.KKKKK...LLLL.........AAAAAA.....NNNNN..NNNN..
//.IIIII.KKKKKKKKK....LLLL.........AAAAAAA....NNNNNN.NNNN..
//.IIIII.KKKKKKKK.....LLLL........AAAAAAAA....NNNNNN.NNNN..
//.IIIII.KKKKKKKK.....LLLL........AAAAAAAA....NNNNNNNNNNN..
//.IIIII.KKKKKKKK.....LLLL........AAAA.AAAA...NNNNNNNNNNN..
//.IIIII.KKKKKKKKK....LLLL.......AAAAAAAAAA...NNNNNNNNNNN..
//.IIIII.KKKK.KKKKK...LLLL.......AAAAAAAAAAA..NNNNNNNNNNN..
//.IIIII.KKKK..KKKK...LLLL.......AAAAAAAAAAA..NNNN.NNNNNN..
//.IIIII.KKKK..KKKKK..LLLLLLLLLLAAAA....AAAA..NNNN..NNNNN..
//.IIIII.KKKK...KKKKK.LLLLLLLLLLAAAA.....AAAA.NNNN..NNNNN..
//.IIIII.KKKK...KKKKK.LLLLLLLLLLAAAA.....AAAA.NNNN...NNNN..
//.........................................................

  getIklanList(page: Pager): Observable<any> {
    console.log('getIklanList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, jabatan: 'JPK', tarikhMula: '02/02/2020', tarikhAkhir: '02/02/2021', iklan: 'Contoh Iklan 1', status: 'ACTIVE'},
      { id: 2, jabatan: 'JPK', tarikhMula: '02/02/2020', tarikhAkhir: '02/02/2021', iklan: 'Contoh Iklan 2', status: 'ACTIVE'},
    ];
    const pageInfo: any = { RowCount: 2 };
    return of({ list, pageInfo });

  }

  getIklanDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      statusId: '1',
      jabatanId: '1',
      iklan: 'Contoh Iklan 1',
      tarikhMula: '22 November 2020',
      tarikhAkhir: '30 November 2020',
    };
    return of(record);
  }

  saveIklan(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Iklan Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deleteIklanList(ids: Array<{ id: number }>) {
    console.log('deleteIklanList ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Iklan telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

//......................................................................................................................................
//.PPPPPPPPP...PEEEEEEEEEE.ENNN...NNNN.....GGGGGGG....GUUU...UUUU.UUMMMM...MMMMMM.MUUU...UUUU.UUMMMM...MMMMMM....AAAAA.....NNNN...NNNN..
//.PPPPPPPPPP..PEEEEEEEEEE.ENNNN..NNNN...GGGGGGGGGG...GUUU...UUUU.UUMMMM...MMMMMM.MUUU...UUUU.UUMMMM...MMMMMM....AAAAA.....NNNNN..NNNN..
//.PPPPPPPPPPP.PEEEEEEEEEE.ENNNN..NNNN..NGGGGGGGGGGG..GUUU...UUUU.UUMMMM...MMMMMM.MUUU...UUUU.UUMMMM...MMMMMM...AAAAAA.....NNNNN..NNNN..
//.PPPP...PPPP.PEEE........ENNNNN.NNNN..NGGGG..GGGGG..GUUU...UUUU.UUMMMMM.MMMMMMM.MUUU...UUUU.UUMMMMM.MMMMMMM...AAAAAAA....NNNNNN.NNNN..
//.PPPP...PPPP.PEEE........ENNNNN.NNNN.NNGGG....GGG...GUUU...UUUU.UUMMMMM.MMMMMMM.MUUU...UUUU.UUMMMMM.MMMMMMM..AAAAAAAA....NNNNNN.NNNN..
//.PPPPPPPPPPP.PEEEEEEEEE..ENNNNNNNNNN.NNGG...........GUUU...UUUU.UUMMMMM.MMMMMMM.MUUU...UUUU.UUMMMMM.MMMMMMM..AAAAAAAA....NNNNNNNNNNN..
//.PPPPPPPPPP..PEEEEEEEEE..ENNNNNNNNNN.NNGG..GGGGGGGG.GUUU...UUUU.UUMMMMMMMMMMMMM.MUUU...UUUU.UUMMMMMMMMMMMMM..AAAA.AAAA...NNNNNNNNNNN..
//.PPPPPPPPP...PEEEEEEEEE..ENNNNNNNNNN.NNGG..GGGGGGGG.GUUU...UUUU.UUMMMMMMMMMMMMM.MUUU...UUUU.UUMMMMMMMMMMMMM.AAAAAAAAAA...NNNNNNNNNNN..
//.PPPP........PEEE........ENNNNNNNNNN.NNGGG.GGGGGGGG.GUUU...UUUU.UUMMMMMMMMMMMMM.MUUU...UUUU.UUMMMMMMMMMMMMM.AAAAAAAAAAA..NNNNNNNNNNN..
//.PPPP........PEEE........ENNN.NNNNNN..NGGGG....GGGG.GUUU...UUUU.UUMM.MMMMM.MMMM.MUUU...UUUU.UUMM.MMMMM.MMMM.AAAAAAAAAAA..NNNN.NNNNNN..
//.PPPP........PEEEEEEEEEE.ENNN..NNNNN..NGGGGGGGGGGG..GUUUUUUUUUU.UUMM.MMMMM.MMMM.MUUUUUUUUUU.UUMM.MMMMM.MMMMMAAA....AAAA..NNNN..NNNNN..
//.PPPP........PEEEEEEEEEE.ENNN..NNNNN...GGGGGGGGGG....UUUUUUUUU..UUMM.MMMMM.MMMM..UUUUUUUUU..UUMM.MMMMM.MMMMMAAA.....AAAA.NNNN..NNNNN..
//.PPPP........PEEEEEEEEEE.ENNN...NNNN.....GGGGGGG......UUUUUUU...UUMM.MMMMM.MMMM...UUUUUUU...UUMM.MMMMM.MMMMMAAA.....AAAA.NNNN...NNNN..
//......................................................................................................................................

  getPengumumanList(page: Pager): Observable<any> {
    console.log('getPengumumanList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, mesejRingkas: 'mesej 1', mesej: 'kandungan 1', dateStart: '22/10/2020', dateEnd: '22/10/2021', kumpulan: 'Pentadbir Sistem', status: 'INACTIVE'},
      { id: 2, mesejRingkas: 'mesej 2', mesej: 'kandungan 2', dateStart: '22/10/2020', dateEnd: '22/10/2021', kumpulan: 'Pentadbir Stesen Handheld',  status: 'ACTIVE'},
    ];
    const pageInfo: any = { RowCount: 2 };
    return of({ list, pageInfo });

  }

  getStateList(): Observable<any> {
    console.log('getStateList ::::::::::::::::: ');

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { kumpulanId: 1, kumpulanName: 'Pentadbir Sistem', keterangan: 'Pentadbir Sistem', jabatanId: 1, bilPengguna: 10, tarikhKemaskini: '22/10/2020'},
      { kumpulanId: 2, kumpulanName: 'Pentadbir Stesen Handheld', keterangan: 'Pentadbir Stesen Handheld', jabatanId: 2, bilPengguna: 20, tarikhKemaskini: '22/10/2020'},
      { kumpulanId: 3, kumpulanName: 'Pegawai Penguatkuasa', keterangan: 'Pegawai Penguatkuasa', jabatanId: 3, bilPengguna: 30, tarikhKemaskini: '22/10/2020'},
      { kumpulanId: 4, kumpulanName: 'Pegawai Pembayaran', keterangan: 'Pegawai Pembayaran', jabatanId: 3, bilPengguna: 40, tarikhKemaskini: '22/10/2020'},
    ];

    return of(list);
  }

  savePengumuman(model, JabatanId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Pengumuman Telah Disimpan!';
    const Id = JabatanId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deletePengumumanList(ids: Array<{ id: number }>) {
    console.log('deletePengumumanList ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Pengumuman telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

//....................................................................................................
//.LLLL.........OOOOOOO........GGGGGGG............AAAAA.....UUUU...UUUU..DDDDDDDDD...DIIII.TTTTTTTTT..
//.LLLL........OOOOOOOOOO....GGGGGGGGGG...........AAAAA.....UUUU...UUUU..DDDDDDDDDD..DIIII.TTTTTTTTT..
//.LLLL.......OOOOOOOOOOOO..GGGGGGGGGGGG.........AAAAAA.....UUUU...UUUU..DDDDDDDDDDD.DIIII.TTTTTTTTT..
//.LLLL.......OOOOO..OOOOO..GGGGG..GGGGG.........AAAAAAA....UUUU...UUUU..DDDD...DDDD.DIIII....TTTT....
//.LLLL......LOOOO....OOOOOOGGGG....GGG.........AAAAAAAA....UUUU...UUUU..DDDD....DDDDDIIII....TTTT....
//.LLLL......LOOO......OOOOOGGG.................AAAAAAAA....UUUU...UUUU..DDDD....DDDDDIIII....TTTT....
//.LLLL......LOOO......OOOOOGGG..GGGGGGGG.......AAAA.AAAA...UUUU...UUUU..DDDD....DDDDDIIII....TTTT....
//.LLLL......LOOO......OOOOOGGG..GGGGGGGG......AAAAAAAAAA...UUUU...UUUU..DDDD....DDDDDIIII....TTTT....
//.LLLL......LOOOO....OOOOOOGGGG.GGGGGGGG......AAAAAAAAAAA..UUUU...UUUU..DDDD....DDDDDIIII....TTTT....
//.LLLL.......OOOOO..OOOOO..GGGGG....GGGG......AAAAAAAAAAA..UUUU...UUUU..DDDD...DDDDDDIIII....TTTT....
//.LLLLLLLLLL.OOOOOOOOOOOO..GGGGGGGGGGGG...... AAA....AAAA..UUUUUUUUUUU..DDDDDDDDDDD.DIIII....TTTT....
//.LLLLLLLLLL..OOOOOOOOOO....GGGGGGGGGG....... AAA.....AAAA..UUUUUUUUU...DDDDDDDDDD..DIIII....TTTT....
//.LLLLLLLLLL....OOOOOO........GGGGGGG....... AAA.....AAAA...UUUUUUU....DDDDDDDDD...DIIII....TTTT....
//....................................................................................................

  getAuditList(page: Pager): Observable<any> {
    console.log('getAuditList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, tindakan: 'GENERATE REPORT', modul: 'M.REPORT', keyword: 'Laporan Kemasukan Data DDE - Bulanan', userName: 'ALi', dateRecorded: '22/01/2020'},
      { id: 2, tindakan: 'GENERATE REPORT', modul: 'M.REPORT', keyword: 'Laporan Kemasukan Data DDE - Bulanan', userName: 'Ahmad', dateRecorded: '22/01/2020'},
    ];
    const pageInfo: any = { RowCount: 2 };
    return of({ list, pageInfo });

  }

  getProductListByOrder(OrderId: number): Observable<any> {
    console.log('getOrderList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    const list: any = [
      { position: 1, bidang1: 'Start Date', bidang2: 'Notice Type', bidang3: 'Vehicle Type', bidang4: 'Report Type', bidang5: 'Discount Class Id' },
    ];

    // const list: any = [
    //     bidang = {
    //       position: 1, bidang1: 'Start Date', bidang2: 'Notice Type', bidang3: 'Vehicle Type', bidang4: 'Report Type', bidang5: 'Discount Class Id'
    //     },
    // ];

    // const list = {
    //   bidang: {
    //     position: 1, bidang1: 'Start Date', bidang2: 'Notice Type', bidang3: 'Vehicle Type', bidang4: 'Report Type', bidang5: 'Discount Class Id'
    //   },
    //   valueBaru: {
    //     position: 1, bidang1: '1/1/2018 12:00:00 AM', bidang2: 'JPK 55 - TRAFIK', bidang3: 'KERETA', bidang4: 'Harian', bidang5: '00000-000-00-00000'
    //   },
    //   valueLama: {
    //     position: 1, bidang1: '', bidang2: '', bidang3: '', bidang4: '', bidang5: ''
    //   },
    // };

    return of(list);
  }

//.............................................................................................
//.KKKK...KKKKK...OOOOOOO....OMMMMM...MMMMMM.PPPPPPPPP.....AAAA.....AAUU....UUUU.UUNNN...NNNN..
//.KKKK..KKKKK...OOOOOOOOOO..OMMMMM...MMMMMM.PPPPPPPPPP...AAAAAA....AAUU....UUUU.UUNNN...NNNN..
//.KKKK.KKKKK...OOOOOOOOOOOO.OMMMMM...MMMMMM.PPPPPPPPPPP..AAAAAA....AAUU....UUUU.UUNNNN..NNNN..
//.KKKKKKKKK....OOOOO..OOOOO.OMMMMMM.MMMMMMM.PPPP...PPPP..AAAAAAA...AAUU....UUUU.UUNNNNN.NNNN..
//.KKKKKKKK....KOOOO....OOOOOOMMMMMM.MMMMMMM.PPPP...PPPP.AAAAAAAA...AAUU....UUUU.UUNNNNN.NNNN..
//.KKKKKKKK....KOOO......OOOOOMMMMMM.MMMMMMM.PPPPPPPPPPP.AAAAAAAA...AAUU....UUUU.UUNNNNNNNNNN..
//.KKKKKKKK....KOOO......OOOOOMMMMMMMMMMMMMM.PPPPPPPPPP..AAAA.AAAA..AAUU....UUUU.UUNNNNNNNNNN..
//.KKKKKKKKK...KOOO......OOOOOMMMMMMMMMMMMMM.PPPPPPPPP..PAAAAAAAAA..AAUU....UUUU.UUNN.NNNNNNN..
//.KKKK.KKKKK..KOOOO....OOOOOOMMMMMMMMMMMMMM.PPPP.......PAAAAAAAAAA.AAUUU...UUUU.UUNN.NNNNNNN..
//.KKKK..KKKK...OOOOO..OOOOO.OMMM.MMMMM.MMMM.PPPP......PPAAAAAAAAAA..AUUU..UUUUU.UUNN..NNNNNN..
//.KKKK..KKKKK..OOOOOOOOOOOO.OMMM.MMMMM.MMMM.PPPP......PPAA....AAAA..AUUUUUUUUUU.UUNN..NNNNNN..
//.KKKK...KKKKK..OOOOOOOOOO..OMMM.MMMMM.MMMM.PPPP......PPAA....AAAAA.AUUUUUUUUU..UUNN...NNNNN..
//.KKKK...KKKKK....OOOOOO....OMMM.MMMMM.MMMM.PPPP.....PPPAA.....AAAA...UUUUUUU...UUNN....NNNN..
//.............................................................................................


  getKompaunDiskaunList(page: Pager): Observable<any> {
    console.log('getKompaunDiskaunList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, jenis: 'Kenderaan', nama: 'Vechicle Type', diskaunOleh: 'FLAT_RATE', peratus: 10, tarikhNotisDari: '22/11/2020', tarikhNotisHingga: '30/11/2020', tarikhDiskaunDari: '28/11/2020', tarikhDiskaunHingga: '30/11/2020', status: 'Enable'},
      { id: 2, jenis: 'General', nama: 'second', diskaunOleh: 'FLAT_RATE', peratus: 1, tarikhNotisDari: '11/10/2020', tarikhNotisHingga: '31/10/2020', tarikhDiskaunDari: '28/10/2020', tarikhDiskaunHingga: '31/10/2020', status: 'Disable'},
      { id: 3, jenis: 'EarlyPayment', nama: 'EarlyPayment', diskaunOleh: 'FLAT_RATE', peratus: 15, tarikhNotisDari: '22/11/2020', tarikhNotisHingga: '30/11/2020', tarikhDiskaunDari: '28/11/2020', tarikhDiskaunHingga: '30/11/2020', status: 'Enable'},
    ];
    const pageInfo: any = { RowCount: 3 };
    return of({ list, pageInfo });

  }

  deleteKompaunList(ids: Array<{ id: number }>) {
    console.log('deleteKompaunList ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Kompaun telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getDiskaunKenderaanDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      statusId: '1',
      diskaunOlehId: '1',
      jenisKenderaanId: '1',
      namaDiskaun: 'Vehicle Type',
      rate: '10',
      tarikhNotisDari: '22 November 2020',
      tarikhNotisHingga: '30 November 2020',
      tarikhDiskaunDari: '28 November 2020',
      tarikhDiskaunHingga: '30 November 2020',
    };
    return of(record);
  }

  saveDiskaunKenderaan(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Kompaun Diskaun Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  getJenisNotisList(): Observable<any> {
    console.log('getJenisNotisList ::::::::::::::::: ');

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'JPK55-TRAFIK' },
      { id: 2, name: 'JPK56-AM' },
      { id: 3, name: 'JPK57-Saman/Lorong Bas' },
      { id: 4, name: 'JPPP-AM' },
      { id: 5, name: 'JKAS-AM' },
    ];

    return of(list);

  }

  saveDiskaunUmum(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Kompaun Diskaun Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  getDiskaunumumDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      statusId: '1',
      diskaunOlehId: '1',
      jenisKenderaanId: '1',
      namaDiskaun: 'Vehicle Type',
      rate: '10',
      tarikhNotisDari: '22 November 2020',
      tarikhNotisHingga: '30 November 2020',
      tarikhDiskaunDari: '28 November 2020',
      tarikhDiskaunHingga: '30 November 2020',
    };
    return of(record);
  }

  saveDiskaunAwal(model, UserId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Kompaun Diskaun Telah Disimpan!';
    const Id = UserId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

}




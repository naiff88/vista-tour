import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { NgxPrintModule } from 'ngx-print';
import { ImageCropperModule } from 'ngx-image-cropper';

// import {
//   MatButtonModule,
//   MatCheckboxModule,
//   MatDialogModule,
//   MatIconModule,
//   MatProgressBarModule,
//   MatSelectModule,
//   MatSlideToggleModule,
//   MatSnackBarModule,
//   MatSortModule,
//   MatTableModule,
//   MatTabsModule,
//   MatDatepickerModule,
//   MatFormFieldModule,
//   MatInputModule,
//   MatRadioModule, MatCardModule, MatRippleModule
// } from '@angular/material';

import { MatRippleModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
import { MatIconModule} from '@angular/material/icon';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort} from '@angular/material/sort';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { JoditAngularModule } from 'jodit-angular';
import { AppCommonModule } from '../shared/app-common.module';

import { QuillModule } from 'ngx-quill';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';

import { MenuComponent } from './menu/menu.component';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MiscModule } from '../misc/misc.module';
import { PMServiceProxy } from 'src/shared/service-proxies/service-proxies';
import { getQuillEditorSettings } from 'src/shared/helpers/quill-helper';
import { MainComponent } from './main/dashboard.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { ChartModule } from '../../../src/app/chart/chart.module';


const materialModules = [
  MatButtonModule,
  MatTableModule,
  MatCheckboxModule,
  MatDialogModule,
  MatSortModule,
  MatTabsModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatProgressBarModule,
  MatIconModule,
  MatSnackBarModule,
  MatRadioModule,
  MatSlideToggleModule
];

@NgModule({
  declarations: [
    MenuComponent,
    MainComponent
  ],
  exports:[MenuComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppCommonModule,
    JoditAngularModule,
    DashboardRoutingModule,
    QuillModule.forRoot(getQuillEditorSettings()),
    TableModule,
    PaginatorModule,
    ModalModule.forRoot(),
    MatDatepickerModule,
    MatMomentDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSortModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatIconModule,
    MatSnackBarModule,
    MiscModule,
    NgxPrintModule,
    MatRadioModule,
    ImageCropperModule,
    MatCardModule,
    MatRippleModule,
    PerfectScrollbarModule,
    ChartModule
  ],
  entryComponents: [],
  providers: [PMServiceProxy],
})
export class DashboardModule {
}

import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { bufferTime, distinctUntilChanged, filter, map, scan, share, take, tap } from 'rxjs/operators';
import { ProgressBarState} from './models';
import * as _ from 'lodash';
import { API_BASE_URL } from 'src/shared/service-proxies/service-proxies';
import { Pager, PageInfo } from '../../pagingnation';

export enum HRApiEndpoint {
  // REF
  CountryList = '/api/reference/country',
}

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  /**
   * Indicate data processing. Binded in dialog
   */
  private processingSubject = new Subject<ProgressBarState>();
  private baseUrl: string;
  http: HttpClient;


  /**
   *  Pipeline of async operation to handle loading state of product dialog
   */
  isBusyStream: Observable<boolean> = this.processingSubject.asObservable()
    .pipe(
      bufferTime(300),                  // buffers the progress Observable values for 100ms period
      filter(states => states.length > 0),   // ignore empty streams
      scan((total: number, states: ProgressBarState[]) => total + _.sum(states), 0),
      map((diff: number) => diff !== 0),       // diff between count of processing and already completed http requests
      distinctUntilChanged(),
      share()                                         // Returns a new Observable that multicasts (shares) the original Observable
    );

  set isBusy(value: boolean) {
    const state: number = value ? ProgressBarState.Show : ProgressBarState.Hide;
    this.processingSubject.next(state);
  }



  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }




  // SERVICE START

  // getEmployeeInfo(id: number): Observable<any> {
  //   const params = new HttpParams().set('tenantInquiryId', String(id));
  //   this.isBusy = true;
  //   return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
  //     .pipe(
  //       map((resp: any) => {
  //         // console.log('getEmployeeInfo ::::::::::: ', resp);
  //         const model = resp.titleInfo as any;
  //         // model.historyList = resp.historyList as InquiryHistoryList[];
  //         return model;
  //       }),
  //       tap({ complete: () => this.isBusy = false })
  //     );
  // }

  // SERVICE END
}

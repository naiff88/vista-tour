import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
//import { MatDialog, MatTableDataSource } from '@angular/material';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
// import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';

import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class MainComponent implements OnInit {
  // displayedColumns = ['position', 'name', 'datejoined', 'phone', 'group'];
  // dataSource = ELEMENT_DATA;

  // displayedColumns2 = ['position', 'customer', 'orderdate', 'status', 'total'];
  // dataSource2 = ELEMENT_DATA2;

  // PERFECT SCROLL BAR
  // public type: string = 'component';
  // public disabled: boolean = false;
  // public config: PerfectScrollbarConfigInterface = {};
  // @ViewChild(PerfectScrollbarComponent) componentRef?: PerfectScrollbarComponent;

  DashboardEmploymentStatus: any;
  name = 'Set iframe source';
  url = 'https://ryurial.github.io';
  urlSafe: SafeResourceUrl;

  constructor(
    private service: DashboardService,
    public sanitizer: DomSanitizer) {
  }

  // PERFECT SCROLL BAR
  // public toggleDisabled(): void {
  //   this.disabled = !this.disabled;
  // }
  //
  // public onScrollEvent(event: any): void {
  //   console.log(event);
  // }

  ngOnInit() {
    localStorage.removeItem("menuId");
    localStorage.removeItem("listMenu");
    localStorage.removeItem("parentMenuId");
    this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    //
  }
  // getRecord() {
  //   console.log('clicked!');
  // }
}

// export interface Customers {
//   name: string;
//   position: number;
//   datejoined: string;
//   phone: string;
//   group: string;
// }

// export interface Orders {
//   customer: string;
//   position: number;
//   orderdate: string;
//   status: string;
//   total: number;
// }

// const ELEMENT_DATA: Customers[] = [
//   {position: 1, name: 'Ahmad' , datejoined: '12/10/2020', phone: '0155929', group: 'Customer'},
//   {position: 2, name: 'Siti' , datejoined: '12/10/2020', phone: '0155929', group: 'Customer'},
//   {position: 3, name: 'Ajib' , datejoined: '12/10/2020', phone: '0155929', group: 'Customer'},
//   {position: 4, name: 'Sarah' , datejoined: '12/10/2020', phone: '0155929', group: 'Customer'},
//   {position: 5, name: 'Ali' , datejoined: '12/10/2020', phone: '0155929', group: 'Customer'},
//   {position: 6, name: 'Hadi' , datejoined: '12/10/2020', phone: '0155929', group: 'Customer'},
//   {position: 7, name: 'John' , datejoined: '12/10/2020', phone: '0155929', group: 'Customer'},
//   {position: 8, name: 'Labu' , datejoined: '12/10/2020', phone: '0155929', group: 'Customer'},

// ];

// const ELEMENT_DATA2: Orders[] = [
//   {position: 1, customer: 'Ahmad' , orderdate: '12/10/2020', status: 'Paid', total: 100},
//   {position: 2, customer: 'Siti' , orderdate: '12/10/2020', status: 'Pending', total: 200},
//   {position: 3, customer: 'Ajib' , orderdate: '12/10/2020', status: 'Hold', total: 300},
//   {position: 4, customer: 'Sarah' , orderdate: '12/10/2020', status: 'Paid', total: 400},
//   {position: 5, customer: 'Ali' , orderdate: '12/10/2020', status: 'Pending', total: 500},
//   {position: 6, customer: 'Hadi' , orderdate: '12/10/2020', status: 'Hold', total: 600},
//   {position: 7, customer: 'John' , orderdate: '12/10/2020', status: 'Hold', total: 700},
//   {position: 8, customer: 'Labu' , orderdate: '12/10/2020', status: 'Paid', total: 800},

// ];

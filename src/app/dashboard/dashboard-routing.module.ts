import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { MainComponent } from './main/dashboard.component';
// import { ProductsListComponent } from '../catalogue/products/list/list.component';

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MainComponent,
                children: [
                    { path: 'main', component: MainComponent },
                    // { path: 'products-list', component: MainComponent },
                    // { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
                    // { path: 'dashboard', component: MainComponent }
                ]
            }
        ])
    ],
    // imports: [RouterModule.forRoot(routes)],
    exports: [
        RouterModule
    ]
})

export class DashboardRoutingModule { }

/* tslint:disable:max-line-length */
import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { AppSessionService } from 'src/shared/common/session/app-session.service';
import { MenuSetting } from '../../shared/common/menu-setting/menu';

@Component({
  selector: 'app-menu',
  templateUrl: './../../../../src/app/shared/common/menu-setting/menu.html',
})
export class MenuComponent implements OnInit {

  menuSetting: MenuSetting;
  currentMenuIndex = 1;
  previousParentId = 0;
  listMenus: any;
  // private wasInside = false;

  constructor(private eRef: ElementRef
  ) {
    // const getFlagMenu = this.menuSetting.getFlagMenu();
    // if(getFlagMenu == 0)
    // {
    this.menuSetting = new MenuSetting();
    // }

  }

  // @HostListener('document:click', ['$event'])
  // clickout(event) {
  //   if(this.eRef.nativeElement.contains(event.target)) {
  //     console.log('onClickInsideMenu :::::::::::::::::::: ');
  //     //this.text = "clicked inside";
  //   } else {
  //     //this.text = "clicked outside";
  //     console.log('onClickOutsideMenu :::::::::::::::::::: ');
  //   }
  // }

  // onClickedOutside(e: Event) {
  //   //this.showBox = false;
  //   console.log('clicked outside');
  // }

  // @HostListener('click')
  // clickInside() {
  //   //this.text = "clicked inside";
  //   console.log('clicked inside');
  //   this.wasInside = true;
  // }

  // @HostListener('document:click')
  // clickout() {
  //   if (!this.wasInside) {
  //     console.log('clicked outside');
  //     //this.text = "clicked outside";
  //   }
  //   this.wasInside = false;
  // }


  // onClickOutsideMenu() {
  //   console.log('onClickOutsideMenu :::::::::::::::::::: ');
  //   //var body = $("body");
  //   //console.log('onClickMenu body :::::::::::::::::::: ', body);

  //   var menu = $("#m_aside_left");
  //   //console.log('onClickMenu menu :::::::::::::::::::: ', menu);
  //   if(menu.hasClass('m-aside-left--on'))
  //   {
  //     menu.removeClass('m-aside-left--on');
  //   }
  //   else
  //   {
  //     menu.addClass('m-aside-left--on');
  //   }
  //   //this.dialog.open(ChangePasswordComponent);
  // }


  ngOnInit() {
    //console.log('ngOnInit ::::::::::::::: MENU COMPONENT NEW : ');
    // const getFlagMenu = this.menuSetting.getFlagMenu();
    // console.log('ngOnInit ::::::::::::::: storeMenu.length : ', storeMenu.length);
    // if(getFlagMenu == 0)
    // {
    //   console.log('ngOnInit ::::::::::::::: storeMenu.length : ');
    //   localStorage.setItem('flagMenu', JSON.stringify(1));

    this.previousParentId = this.menuSetting.getPreviousMenuParentId();

    this.listMenus = [
      { menuId: 1, iconClass: 'flaticon-dashboard', menuName: 'Dashboard', parentMenuId: 0, path: '/app/dashboard/main', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 2, iconClass: 'flaticon-suitcase', menuName: 'Pentadbiran', parentMenuId: 0, path: '/app/pentadbiran/kumpulan-list', active: false, hasSubmenu: true, openSub: false, extraClass: '', disabled: false },
      { menuId: 3, iconClass: '', menuName: 'Kumpulan', parentMenuId: 2, path: '/app/pentadbiran/kumpulan-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 4, iconClass: '', menuName: 'Pengguna', parentMenuId: 2, path: '/app/pentadbiran/pengguna-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 5, iconClass: '', menuName: 'Akta Induk', parentMenuId: 2, path: '/app/pentadbiran/data-induk/akta-induk-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 6, iconClass: '', menuName: 'Peruntukan Undang-Undang', parentMenuId: 2, path: '/app/pentadbiran/data-induk/peruntukan-undang-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 7, iconClass: '', menuName: 'Seksyen/Kesalahan', parentMenuId: 2, path: '/app/pentadbiran/data-induk/seksyen-kesalahan-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 8, iconClass: '', menuName: 'Carian Data Induk', parentMenuId: 2, path: '/app/pentadbiran/data-induk/carian-induk-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 9, iconClass: '', menuName: 'Jenama Kenderaan', parentMenuId: 2, path: '/app/pentadbiran/data-induk/kenderaan/jenama-kenderaan-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 10, iconClass: '', menuName: 'Model Kenderaan', parentMenuId: 2, path: '/app/pentadbiran/data-induk/kenderaan/model-kenderaan-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 11, iconClass: '', menuName: 'Jenis Kenderaan', parentMenuId: 2, path: '/app/pentadbiran/data-induk/kenderaan/jenis-kenderaan-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 12, iconClass: '', menuName: 'Zon Pegawai', parentMenuId: 2, path: '/app/pentadbiran/data-induk/kawasan/zon-pegawai-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 13, iconClass: '', menuName: 'Parlimen', parentMenuId: 2, path: '/app/pentadbiran/data-induk/kawasan/parlimen-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 14, iconClass: '', menuName: 'Lokasi', parentMenuId: 2, path: '/app/pentadbiran/data-induk/kawasan/lokasi-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 15, iconClass: '', menuName: 'Kawasan Kesalahan', parentMenuId: 2, path: '/app/pentadbiran/data-induk/kawasan/kawasan-kesalahan-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 16, iconClass: '', menuName: 'Pangkat Pegawai', parentMenuId: 2, path: '/app/pentadbiran/data-induk/pangkat-pegawai-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 22, iconClass: '', menuName: 'Iklan', parentMenuId: 2, path: '/app/pentadbiran/data-induk/iklan-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 23, iconClass: '', menuName: 'Pengumuman', parentMenuId: 2, path: '/app/pentadbiran/pengumuman-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 24, iconClass: '', menuName: 'Audit', parentMenuId: 2, path: '/app/pentadbiran/audit-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 25, iconClass: '', menuName: 'Kompaun Diskaun', parentMenuId: 2, path: '/app/pentadbiran/kompaun-diskaun-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },


      // pengurusan handheld
      { menuId: 17, iconClass: 'flaticon-laptop', menuName: 'Pengurusan Handheld', parentMenuId: 0, path: '/app/handheld/senarai-penguatkuasa-handheld', active: false, hasSubmenu: true, openSub: false, extraClass: '', disabled: false },
      { menuId: 18, iconClass: '', menuName: 'Penguatkuasa', parentMenuId: 17, path: '/app/handheld/senarai-penguatkuasa-handheld', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 19, iconClass: '', menuName: 'Serahan', parentMenuId: 17, path: '/app/handheld/senarai-serahan-handheld', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 20, iconClass: '', menuName: 'Senarai Handheld', parentMenuId: 17, path: '/app/handheld/senarai-handheld', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 21, iconClass: '', menuName: 'Pengumuman', parentMenuId: 17, path: '/app/handheld/senarai-pengumuman-handheld', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 26, iconClass: '', menuName: 'Pergerakan', parentMenuId: 17, path: '/app/handheld/senarai-pergerakan-handheld', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 27, iconClass: '', menuName: 'Patch', parentMenuId: 17, path: '/app/handheld/senarai-patch-handheld', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },

      //laporan
      { menuId: 28, iconClass: 'flaticon-analytics', menuName: 'Laporan', parentMenuId: 0, path: '/app/laporan/laporan-utama/utama', active: false, hasSubmenu: true, openSub: false, extraClass: '', disabled: false },
      { menuId: 29, iconClass: '', menuName: 'Utama', parentMenuId: 28, path: '/app/laporan/laporan-utama/utama', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 60, iconClass: '', menuName: 'Utama Mengikut Kawasan', parentMenuId: 28, path: '/app/laporan/laporan-utama/kawasan', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 61, iconClass: '', menuName: 'Bahagian / Zon / Unit', parentMenuId: 28, path: '/app/laporan/laporan-bahagian-zon-unit', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 62, iconClass: '', menuName: 'Prestasi Individu', parentMenuId: 28, path: '/app/laporan/laporan-individu-penguatkuasa', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 30, iconClass: '', menuName: 'Kemasukan Data DDE', parentMenuId: 28, path: '/app/laporan/laporan-dde', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 63, iconClass: '', menuName: 'Utama Per Undang - Undang', parentMenuId: 28, path: '/app/laporan/laporan-utama/undang-undang', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 64, iconClass: '', menuName: 'Per Undang - Undang Bagi Kawasan', parentMenuId: 28, path: '/app/laporan/laporan-bahagian-zon-unit/kawasan', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 65, iconClass: '', menuName: 'Pembatalan Notis', parentMenuId: 28, path: '/app/laporan/laporan-pembatalan-notis', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 66, iconClass: '', menuName: 'Notis Tanpa Alamat', parentMenuId: 28, path: '/app/laporan/laporan-notis-tanpa-alamat', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 67, iconClass: '', menuName: 'Saman Tanpa Alamat', parentMenuId: 28, path: '/app/laporan/laporan-saman-tanpa-alamat', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 68, iconClass: '', menuName: 'Pembayaran Notis Belum Wujud', parentMenuId: 28, path: '/app/laporan/laporan-bayar-notis-belum-wujud', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 69, iconClass: '', menuName: 'Pembayaran Notis', parentMenuId: 28, path: '/app/laporan/laporan-pembayaran-notis', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 70, iconClass: '', menuName: 'Senarai Hitam', parentMenuId: 28, path: '/app/laporan/laporan-senarai-hitam', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 71, iconClass: '', menuName: 'Uplift', parentMenuId: 28, path: '/app/laporan/laporan-uplift', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 72, iconClass: '', menuName: 'SLKE', parentMenuId: 28, path: '/app/laporan/laporan-slke', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 73, iconClass: '', menuName: 'Jenis Kenderaan', parentMenuId: 28, path: '/app/laporan/laporan-jenis-kenderaan', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 74, iconClass: '', menuName: 'Tindakan Mahkamah', parentMenuId: 28, path: '/app/laporan/laporan-tindakan-mahkamah', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 80, iconClass: '', menuName: 'Statistik Tindakan', parentMenuId: 28, path: '/app/laporan/laporan-statistik-tindakan', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      
      // pengurusan notis
      { menuId: 31, iconClass: 'flaticon-list', menuName: 'Pengurusan Notis', parentMenuId: 0, path: '/app/notis/senarai-notis', active: false, hasSubmenu: true, openSub: false, extraClass: '', disabled: false },
      { menuId: 32, iconClass: '', menuName: 'Carian Notis', parentMenuId: 31, path: '/app/notis/senarai-notis', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 33, iconClass: '', menuName: 'Kemasukan Notis Baru', parentMenuId: 31, path: '/app/notis/notis-details/0/add/kemasukan', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 34, iconClass: '', menuName: 'Cetak Peringatan Notis', parentMenuId: 31, path: '/app/notis/senarai-peringatan', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 35, iconClass: '', menuName: 'No. Fail Rujukan', parentMenuId: 31, path: '/app/notis/senarai-fail', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 36, iconClass: '', menuName: 'Cetak Notis', parentMenuId: 31, path: '/app/notis/senarai-cetakan-notis', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 37, iconClass: '', menuName: 'Cetak Aduan Ke Mahkamah', parentMenuId: 31, path: '/app/notis/senarai-aduan-mahkamah', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 38, iconClass: '', menuName: 'Carian SP2U', parentMenuId: 31, path: '/app/notis/senarai-sp2u', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 39, iconClass: '', menuName: 'Pertuduhan Baru', parentMenuId: 31, path: '/app/notis/pertuduhan-details/0/add', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },

      //rayuan
      { menuId: 40, iconClass: 'flaticon-list', menuName: 'Rayuan', parentMenuId: 0, path: '/app/rayuan/senarai-rayuan', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },

      //mahkamah
      { menuId: 41, iconClass: 'flaticon-suitcase', menuName: 'Mahkamah', parentMenuId: 0, path: '/app/mahkamah/senarai-mahkamah', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },

      // E-Sitaan
      { menuId: 75, iconClass: 'flaticon-list', menuName: 'E-Sitaan', parentMenuId: 0, path: '/app/esitaan/penerimaan-list', active: false, hasSubmenu: true, openSub: false, extraClass: '', disabled: false },
      { menuId: 76, iconClass: '', menuName: 'Penerimaan Barang', parentMenuId: 75, path: '/app/esitaan/penerimaan-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 77, iconClass: '', menuName: 'Tuntutan/Pengeluaran Barang', parentMenuId: 75, path: '/app/esitaan/tuntutan-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },
      { menuId: 78, iconClass: '', menuName: 'Pelupusan Barang Sitaan', parentMenuId: 75, path: '/app/esitaan/pelupusan-list', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },

      // { menuId: 100, iconClass: 'flaticon-bag', menuName: 'Sales', parentMenuId: 0, path: '/app/sales/order-list/0', active: false, hasSubmenu: true, openSub: false, extraClass: '', disabled: false },
      // { menuId: 101, iconClass: '', menuName: 'Orders', parentMenuId: 100, path: '/app/sales/order-list/0', active: false, hasSubmenu: false, openSub: false, extraClass: '', disabled: false },

      //this.menuSetting.clickMenu(this.menuSetting.defaultMenuIndex(2), this.listMenus);

    ];
    //default
    this.menuSetting.clickMenu(this.menuSetting.defaultMenuIndex(3), this.listMenus);
    // }

  }

  highlightMenu(path) {
    let menuId = this.listMenus.find(x => x.path === path).menuId;
    //console.log('highlightMenu menuId ::::::::::::::::: ', menuId);
    this.menuSetting.clickMenu(menuId, this.listMenus);
  }

}

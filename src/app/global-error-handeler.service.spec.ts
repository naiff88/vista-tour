import { TestBed } from '@angular/core/testing';

import { GlobalErrorHandelerService } from './global-error-handeler.service';

describe('GlobalErrorHandelerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalErrorHandelerService = TestBed.get(GlobalErrorHandelerService);
    expect(service).toBeTruthy();
  });
});

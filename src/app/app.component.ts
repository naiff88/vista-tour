import {Component, OnInit, ViewChild, Input, Injectable} from '@angular/core';

@Component({
  selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

@Injectable()
export class AppComponent implements OnInit {
  showSpinner: boolean = false;
  ngOnInit(): void {
    mLayout.init();
  }

  showOverlaySpinner(show){
    setTimeout(()=>{
    this.showSpinner = show;
    }
    , 0);
  }

}

import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { LaporanService } from '../../laporan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Moment } from 'moment';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');

@Component({
  selector: 'app-laporan-slke-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})


export class LaporanSlkeListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  showList: boolean = false;
  eventSearch: any;
  onSearch: boolean = false;
  submitted: boolean = false;
  form: FormGroup;
  //close standard var for searching page

  //initiate list
  laporanSlkeTypeList: any = [];
  ReportList: any = [];
  AktaIndukList: any = [];

  //initiate list model 
  LaporanSlkeType: number = 0;
  AktaInduk: number = 0;

  //initiate date model 
  BulanTahun: any = moment();
  Tahun: any = moment();
  // TarikhMulaLaporan: any = '';
  // TarikhAkhirLaporan: any = '';

  //dynamic report param
  TarikhMula: any = '';
  TarikhAkhir: any = '';
  //NoGaji: string = '';

  //display date trick
  DisplayMonthYear: any = moment().format("MM/YYYY");;
  DisplayYear: any = moment().format("YYYY");;;


  //report props
  // reportCol1: any = [
  //   { name: 'index', title: 'No.', width: '5', align: 'center' },
  //   { name: 'IdPengguna', title: 'ID Pengguna', width: '15', align: 'left' },
  //   { name: 'Nama', title: 'Nama', width: '20', align: 'left' },
  //   { name: 'JumlahNotis', title: 'Jumlah Notis', width: '15', align: 'left' },
  // ];
  // reportCol2: any = [
  //   { name: 'index', title: 'No.', width: '5', align: 'center' },
  //   { name: 'NoNotis', title: 'No. Notis', width: '15', align: 'left' },
  //   { name: 'Jenis', title: 'Jenis', width: '20', align: 'left' },
  //   { name: 'TarikhKesalahan', title: 'Tarikh Kesalahan', width: '15', align: 'left' },
  //   { name: 'TarikhWujud', title: 'Tarikh Wujud', width: '15', align: 'left' },
  //   { name: 'NamaPesalah', title: 'Nama Pesalah', width: '20', align: 'left' },
  //   { name: 'NoKenderaan', title: 'No. Kenderaan', width: '15', align: 'left' },
  //   { name: 'NoCukaiJalan', title: 'No. Cukai Jalan', width: '15', align: 'left' },
  //   { name: 'Peruntukan', title: 'Peruntukan Undang-Undang', width: '30', align: 'left' },
  //   { name: 'Seksyen', title: 'Seksyen Kesalahan', width: '15', align: 'left' },
  //   { name: 'Tempat', title: 'Tempat/Lokasi Kesalahan', width: '20', align: 'left' },
  //   { name: 'NoPetak', title: 'No. Petak/Tg', width: '15', align: 'left' },
  // ];

  //report title
  DisplayDate: any = '';
  ReportTitle: string = 'Laporan SLKE';
  ReportName = this.ReportTitle;

  //for excel ganerator 
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  //standard contructor
  constructor(
    private laporanService: LaporanService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  //form
  get f() { return this.form.controls; }

  //init methode to call default function
  ngOnInit() {
    //this.getSlkeList();
    this.getLaporanSlkeTypeList();
    this.getAktaIndukListAll();
    this.loadSearchDetails();
  }

  loadSearchDetails() {
    this.form = this.buildFormItems();
  }

  //open standard listing function
  getSlkeList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    this.laporanService.getLaporanSlkeList(this.TarikhMula, this.TarikhAkhir, this.LaporanSlkeType, this.AktaInduk)
      .subscribe(items => {
        this.showSpinner = false;
        this.ReportList = items;
      });
    this.primengTableHelper.showLoadingIndicator();
  }
  


  
  //close standard listing function

  //open default listing function
  getLaporanSlkeTypeList() {
    this.laporanService.getPembayaranNotisTypeList().subscribe(items => {
      this.laporanSlkeTypeList = items;
    });
  }
  getAktaIndukListAll(event?: LazyLoadEvent) {
    this.laporanService.getAktaIndukListAll()
      .subscribe(items => {
        this.AktaIndukList = items;
      });
  }
  //close default listing function

  //if diff date format in one component
  //open select month year function  
  chosenYearHandlerOnly(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const Tahun = this.form.controls['Tahun'];
    Tahun.setValue(moment());
    const ctrlValue = Tahun.value;
    ctrlValue.year(normalizedYear.year());
    Tahun.setValue(ctrlValue);
    this.DisplayYear = moment(Tahun.value).format("YYYY");
    datepicker.close();
  }
  chosenYearHandler(normalizedYear: Moment) {
    const BulanTahun = this.form.controls['BulanTahun'];
    BulanTahun.setValue(moment());
    const ctrlValue = BulanTahun.value;
    ctrlValue.year(normalizedYear.year());
    BulanTahun.setValue(ctrlValue);
    this.DisplayMonthYear = moment(BulanTahun.value).format("MM/YYYY");
  }
  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const BulanTahun = this.form.controls['BulanTahun'];
    BulanTahun.setValue(moment());
    const ctrlValue = BulanTahun.value;
    ctrlValue.month(normalizedMonth.month());
    BulanTahun.setValue(ctrlValue);
    this.DisplayMonthYear = moment(BulanTahun.value).format("MM/YYYY");
    datepicker.close();
  }
  //close select month year function  

  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      this.showList = true;
      //dynamicly display date range at report title
      if ((searchForm.LaporanSlkeType == 1 || searchForm.LaporanSlkeType == 2) && searchForm.BulanTahun) {
        this.DisplayDate = moment(searchForm.BulanTahun).format("MM/YYYY");
        this.TarikhMula = searchForm.BulanTahun.startOf('month').format('DD/MM/YYYY');
        this.TarikhAkhir = searchForm.BulanTahun.endOf('month').format('DD/MM/YYYY');
      }
      else if (searchForm.LaporanSlkeType == 3 && searchForm.Tahun) {
        this.DisplayDate = moment(searchForm.Tahun).format("YYYY");
        this.TarikhMula = searchForm.Tahun.startOf('year').format('DD/MM/YYYY');
        this.TarikhAkhir = searchForm.Tahun.endOf('year').format('DD/MM/YYYY');
      }
      // else if (searchForm.LaporanSlkeType == 3 && searchForm.TarikhMulaLaporan && searchForm.TarikhAkhirLaporan) {
      //   this.DisplayDate = moment(searchForm.TarikhMulaLaporan).format("DD/MM/YYYY") + ' - ' + moment(searchForm.TarikhAkhirLaporan).format("DD/MM/YYYY");
      // }


      //dynamicly display report title
      let laporanType = this.laporanSlkeTypeList.find(i => i.id == searchForm.LaporanSlkeType);
      this.ReportName = this.ReportName + ' ' + laporanType.name;
      if (laporanType) {
        this.ReportTitle = 'LAPORAN ' + laporanType.name + ' SLKE BAGI ' + this.DisplayDate;
      }


      //dynamicly set date range as param to call list
      // if ((searchForm.LaporanSlkeType == 1 || searchForm.LaporanSlkeType == 2) && searchForm.BulanTahun) {
      //   this.TarikhMula = searchForm.BulanTahun.startOf('month').format('DD/MM/YYYY');
      //   this.TarikhAkhir = searchForm.BulanTahun.endOf('month').format('DD/MM/YYYY');
      // }
      // else if (searchForm.LaporanSlkeType == 3 && searchForm.Tahun) {
      //   this.TarikhMula = searchForm.Tahun.startOf('year').format('DD/MM/YYYY');
      //   this.TarikhAkhir = searchForm.Tahun.endOf('year').format('DD/MM/YYYY');
      // }
      // else if (searchForm.LaporanSlkeType == 3) {
      //   this.TarikhMula = searchForm.TarikhMulaLaporan.format('DD/MM/YYYY');;
      //   this.TarikhAkhir = searchForm.TarikhAkhirLaporan.format('DD/MM/YYYY');;
      //   this.NoGaji = searchForm.NoGaji;
      // }


      this.onSearch = true;
      this.getSlkeList();
      //this.paginator.changePage(0);
    }
  }


  //on change jenis report
  onChangeType() {
    const LaporanSlkeType = this.LaporanSlkeType;
    const AktaInduk = this.AktaInduk;
    //call reset function
    this.reset();

    this.form.patchValue({ LaporanSlkeType, AktaInduk });
    this.LaporanSlkeType = LaporanSlkeType;
    this.AktaInduk = AktaInduk;
  }

  //reset search form
  // resetType() {
  //   const LaporanSlkeType = this.LaporanSlkeType;
  //   this.reset();
  //   this.form = this.buildFormItems();
  //   this.LaporanSlkeType = LaporanSlkeType;
  //   this.form.patchValue({LaporanSlkeType});
  // }
  reset() {
    this.form = this.buildFormItems();
    this.LaporanSlkeType = 0;
    this.AktaInduk = 0;
    this.BulanTahun = moment();
    this.Tahun = moment();
    // this.TarikhMulaLaporan = '';
    // this.TarikhAkhirLaporan = '';
    this.TarikhMula = '';
    this.TarikhAkhir = '';
    //reset list
   this.ReportList = [];
    this.showList = false;
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      if(formGroup.dirty)
      {
        this.showList = false;
      }  
      

      const BulanTahun = formGroup.controls['BulanTahun'];
      const Tahun = formGroup.controls['Tahun'];      
      const AktaInduk = formGroup.controls['AktaInduk'];
      const LaporanSlkeType = formGroup.controls['LaporanSlkeType'];
      const TarikhMulaLaporan = formGroup.controls['TarikhMulaLaporan'];
      const TarikhAkhirLaporan = formGroup.controls['TarikhAkhirLaporan'];
      //const NoGaji = formGroup.controls['NoGaji'];

      if (AktaInduk) {
        AktaInduk.setErrors(null);
        if (!AktaInduk.value) {
          AktaInduk.setErrors({ required: true });
        }
        else if (AktaInduk.value == 0) {
          AktaInduk.setErrors({ min: true });
        }
      }
      if (LaporanSlkeType) {
        LaporanSlkeType.setErrors(null);
        if (!LaporanSlkeType.value) {
          LaporanSlkeType.setErrors({ required: true });
        }
        else if (LaporanSlkeType.value == 0) {
          LaporanSlkeType.setErrors({ min: true });
        }
      }
      if (BulanTahun && (LaporanSlkeType.value == 1 || LaporanSlkeType.value == 2)) {
        BulanTahun.setErrors(null);
        if (!BulanTahun.value) {
          BulanTahun.setErrors({ required: true });
        }
        else if (!BulanTahun.value) {
          BulanTahun.setErrors({ required: true });
        }
      }
      if (Tahun && LaporanSlkeType.value == 3) {
        Tahun.setErrors(null);
        if (!Tahun.value) {
          Tahun.setErrors({ required: true });
        }
        else if (!Tahun.value) {
          Tahun.setErrors({ required: true });
        }
      }
      // if (TarikhMulaLaporan && LaporanSlkeType.value == 3) {
      //   TarikhMulaLaporan.setErrors(null);
      //   if (!TarikhMulaLaporan.value) {
      //     TarikhMulaLaporan.setErrors({ required: true });
      //   }
      //   else if (TarikhMulaLaporan.value && TarikhAkhirLaporan.value && TarikhAkhirLaporan.value < TarikhMulaLaporan.value) {
      //     TarikhMulaLaporan.setErrors({ exceed: true });
      //   }
      // }
      // if (TarikhAkhirLaporan && LaporanSlkeType.value == 3) {
      //   TarikhAkhirLaporan.setErrors(null);
      //   if (!TarikhAkhirLaporan.value) {
      //     TarikhAkhirLaporan.setErrors({ required: true });
      //   }
      //   else if (TarikhMulaLaporan.value && TarikhAkhirLaporan.value && TarikhAkhirLaporan.value < TarikhMulaLaporan.value) {
      //     TarikhAkhirLaporan.setErrors({ exceed: true });
      //   }
      // }
      // if (NoGaji && LaporanSlkeType.value == 3) {
      //   NoGaji.setErrors(null);
      //   if (!NoGaji.value) {
      //     NoGaji.setErrors({ required: true });
      //   }
      // }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      BulanTahun: [moment(), []],
      Tahun: [moment(), []],
      AktaInduk: ['0', []],
      LaporanSlkeType: ['0', []],
      //NoGaji: ['', []],
      TarikhMulaLaporan: ['', []],
      TarikhAkhirLaporan: ['', []],
    }, { validator: this.customValidation() }
    );
  }




  //excel genarator
  printTableExcel() {
    var workbook = new Excel.Workbook();
    // Tab Title
    var worksheet = workbook.addWorksheet(this.ReportName);
    // Columns to display
    worksheet.mergeCells('A1', 'E1'); //title merge cell
    worksheet.getCell('A1').value = this.ReportTitle.replace('<br>', ' '); //title
    worksheet.getCell('A1').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

    worksheet.getCell('A2').value = this.LaporanSlkeType == 1 ? 'Hari' :  this.LaporanSlkeType == 2 ? 'Minggu' : this.LaporanSlkeType == 3 ? 'Bulan' : '';
    worksheet.getCell('A2').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };
    worksheet.getCell('B2').value = 'Dikelurkan';
    worksheet.getCell('B2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('C2').value = 'Bilangan Dibayar';
    worksheet.getCell('C2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('D2').value = 'Kutipan (RM)';
    worksheet.getCell('D2').alignment = { horizontal: 'right', vertical: 'top', wrapText: true };
    worksheet.getCell('E2').value = 'Baki Notis Kesalahan';
    worksheet.getCell('E2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };


    // worksheet.getCell('C2').value = 'Kadar Bayaran (RM)';
    // worksheet.getCell('C2').alignment = { horizontal: 'right', vertical: 'top', wrapText: true };

    worksheet.columns = [
      { key: 'JenisLaporanCounter', width: 30, style: { alignment: { horizontal: 'left', vertical: 'top', wrapText: true } } },
      { key: 'Dikeluarkan', width: 30, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Dibayar', width: 30, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Kutipan', width: 30, style: { alignment: { horizontal: 'right', vertical: 'top', wrapText: true } } },
      { key: 'Baki', width: 30, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      //{ key: 'Bayaran', width: 30, style: { alignment: { horizontal: 'right', vertical: 'top', wrapText: true } } },
    ];


    for (let record of this.ReportList) {
      worksheet.addRow([
        record.JenisLaporanCounter,
        record.Dikeluarkan,
        record.Dibayar,
        record.Kutipan,
        record.Baki
      ])
    }

    worksheet.eachRow(function (row, _rowNumber) {
      row.eachCell(function (cell, _colNumber) {
        cell.border = {
          top: { style: 'thin' },
          left: { style: 'thin' },
          bottom: { style: 'thin' },
          right: { style: 'thin' }
        };
      });
    });

    workbook.xlsx.writeBuffer().then(data => {
      const blob = new Blob([data], { type: this.blobType });
      //create excell file
      FileSaver.saveAs(blob, this.ReportName + '.xlsx');
    });
  }
  //pdf genarator
  // printTablePdf() {
  //   // this.showSpinner = true;
  //   // let list: any;
  //   // this.laporanService.getLaporanSlkeList(this.TarikhMula, this.TarikhAkhir, this.LaporanSlkeType).subscribe(items => {
  //   //     list = items.list;
  //   //     this.pageSetting.pdfGenerator(this.LaporanSlkeType == 1 || this.LaporanSlkeType == 2 ? this.reportCol1 : this.reportCol2, list, this.ReportTitle, this.ReportTitle, 'l', 7).then(loaded => {
  //   //       if (loaded) {
  //   //         this.showSpinner = false;
  //   //       }
  //   //     });
  //   //   });
  // }

}

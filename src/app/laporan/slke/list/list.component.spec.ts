import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanSlkeListComponent } from './list.component';

describe('LaporanSlkeListComponent', () => {
  let component: LaporanSlkeListComponent;
  let fixture: ComponentFixture<LaporanSlkeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanSlkeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanSlkeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

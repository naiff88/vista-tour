import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanUpliftListComponent } from './list.component';

describe('LaporanUpliftListComponent', () => {
  let component: LaporanUpliftListComponent;
  let fixture: ComponentFixture<LaporanUpliftListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanUpliftListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanUpliftListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { LaporanService } from '../../laporan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Moment } from 'moment';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');

@Component({
  selector: 'app-laporan-uplift-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})


export class LaporanUpliftListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  showList: boolean = false;
  eventSearch: any;
  onSearch: boolean = false;
  submitted: boolean = false;
  form: FormGroup;
  //close standard var for searching page

  //initiate list

  //initiate list model 

  //initiate date model 
  TarikhMula: any = '';
  TarikhAkhir: any = '';
  JumlahBerjaya: number = 0;
  JumlahGagal: number = 0;

  reportCol: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'Tarikh', title: 'Tarikh', width: '20', align: 'left' },
    { name: 'Jumlah', title: 'Jumlah', width: '20', align: 'center' },
    { name: 'Berjaya', title: 'Berjaya', width: '20', align: 'center' },
    { name: 'Gagal', title: 'Gagal', width: '20', align: 'center' },
  ];

  //report title
  DisplayDate: any = '';
  ReportTitle: string = '';
  ReportName = 'Laporan Uplift';

  //for excel ganerator 
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  //standard contructor
  constructor(
    private laporanService: LaporanService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  //form
  get f() { return this.form.controls; }

  //init methode to call default function
  ngOnInit() {
    this.loadSearchDetails();
    this.getLaporanUpliftList();
  }

  loadSearchDetails() {
    this.form = this.buildFormItems();
  }

  //open standard listing function
  getLaporanUpliftList(event?: LazyLoadEvent) {
    
    this.showSpinner = true;
    this.laporanService.getLaporanUpliftList(this.pageSetting.getPagerSetting(this.paginator, event, this.primengTableHelper.getSorting(this.dataTable)),
    this.form.value)
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        this.JumlahBerjaya = items.pageInfo.TotalBerjaya;
        this.JumlahGagal = items.pageInfo.TotalGagal;
        if (this.onSearch) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    this.primengTableHelper.showLoadingIndicator();
  }
  //close standard listing function

  //open default listing function

  //close default listing function

  

  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      this.showList = true;
      //dynamicly display date range at report title     

      this.DisplayDate = moment(searchForm.TarikhMula).format("DD/MM/YYYY") + ' - ' + moment(searchForm.TarikhAkhir).format("DD/MM/YYYY");
      
      this.ReportTitle = 'LAPORAN UPLIFT BAGI ' + this.DisplayDate;      

      this.onSearch = true;
      this.getLaporanUpliftList();
      this.paginator.changePage(0);
    }
  }



  //reset search form  
  reset() {
    this.form = this.buildFormItems();
    this.TarikhMula = '';
    this.TarikhAkhir = '';
    this.dataSource.data = [];
    this.primengTableHelper.totalRecordsCount = 0;
    this.primengTableHelper.records = [];
    this.showList = false;
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      if(formGroup.dirty)
      {
        this.showList = false;
      }  
      const TarikhMula = formGroup.controls['TarikhMula'];
      const TarikhAkhir = formGroup.controls['TarikhAkhir'];
      // const NoNotis = formGroup.controls['NoNotis'];
      // const NoResit = formGroup.controls['NoResit'];


      if (TarikhMula) {
        TarikhMula.setErrors(null);
        if (!TarikhMula.value) {
          TarikhMula.setErrors({ required: true });
        }
        else if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhMula.setErrors({ exceed: true });
        }
      }
      if (TarikhAkhir) {
        TarikhAkhir.setErrors(null);
        if (!TarikhAkhir.value) {
          TarikhAkhir.setErrors({ required: true });
        }
        else if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhAkhir.setErrors({ exceed: true });
        }
      }
      // if (NoNotis) {
      //   NoNotis.setErrors(null);
      //   if (!NoNotis.value) {
      //     NoNotis.setErrors({ required: true });
      //   }
      // }
      // if (NoResit) {
      //   NoResit.setErrors(null);
      //   if (!NoResit.value) {
      //     NoResit.setErrors({ required: true });
      //   }
      // }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      // NoNotis: ['', []],    
      // NoResit: ['', []],    
      TarikhMula: ['', []],
      TarikhAkhir: ['', []],
    }, { validator: this.customValidation() }
    );
  }

  //excel genarator
  printTableExcel() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getLaporanUpliftList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
    this.form.value).subscribe(items => {
        list = items.list;
        let footerContent: any = [{label: "Jumlah Berjaya", value: items.pageInfo.TotalBerjaya},{label: "Jumlah Gagal", value: items.pageInfo.TotalGagal}];
        this.pageSetting.excelGenerator(this.reportCol, list, this.ReportName, this.ReportTitle.toUpperCase(),footerContent).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }
  //pdf genarator
  printTablePdf() {
    this.showSpinner = true;
    let list: any;   

    this.laporanService.getLaporanUpliftList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
    this.form.value).subscribe(items => {
        list = items.list;
        let footerContent: any = [{label: "Jumlah Berjaya", value: items.pageInfo.TotalBerjaya},{label: "Jumlah Gagal", value: items.pageInfo.TotalGagal}];

        //console.log('footerContent >>>>>>>>>>>>. ', footerContent);

        this.pageSetting.pdfGenerator(this.reportCol, list, this.ReportName, this.ReportTitle.toUpperCase(), 'p', 7, 
        null, footerContent
        ).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }


}

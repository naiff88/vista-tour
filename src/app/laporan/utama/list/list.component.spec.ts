import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanUtamaListComponent } from './list.component';

describe('LaporanUtamaListComponent', () => {
  let component: LaporanUtamaListComponent;
  let fixture: ComponentFixture<LaporanUtamaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanUtamaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanUtamaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

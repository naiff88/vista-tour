import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router, ActivatedRoute } from '@angular/router';
import { LaporanService } from '../../laporan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');
import html2canvas from 'html2canvas'



@Component({
  selector: 'app-laporan-utama-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class LaporanUtamaListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('printContent', { static: false }) printContent: ElementRef;
  showSpinner: boolean = false;
  submitted: boolean = false;
  showList: boolean = false;
  form: FormGroup;
  FromMenu: string = '';
  //close standard var for searching page

  // any unique name for searching list
  reportName = 'Laporan '

  //initiate list
  OffenceActList: any = [];
  ReportList: any = [];
  KawasanList: any = [];
  JalanList: any = [];
  AktaIndukList: any = [];
  PeruntukanList: any = [];

  //initiate list model 
  ActClassTypes: number = 1;
  OffenceAct: number = 0;
  Kawasan: number = 0;
  Jalan: number = 0;
  AktaInduk: number = 0;
  Peruntukan: number = 0;

  //initiate date model 
  StartDate: any = '';
  EndDate: any = '';

  //report title
  DisplayStartDate: any = '';
  DisplayEndDate: any = '';
  ReportTitle = '';
  ReportName = 'Laporan Utama';


  //for excel ganerator 
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  //standard contructor
  constructor(
    private laporanService: LaporanService,
    private snackBar: MatSnackBar,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
  }

  //form
  get f() { return this.form.controls; }

  //init methode to call default function
  ngOnInit() {
    this.form = this.buildFormItems();
    this.activatedRoute.params.subscribe(params => {
      this.FromMenu = params.fromMenu;
    });

    //this.getLaporanUtamaList();
    this.getOffenceActList();
    if (this.FromMenu == 'kawasan') {
      this.getKawasanList();
      this.getJalanList();
    }
    else if (this.FromMenu == 'undang-undang') {
      this.getAktaIndukList();
      this.getPeruntukanList();
    }

  }

  //open standard listing with search function
  getLaporanUtamaList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    this.laporanService.getLaporanUtamaList(this.StartDate, this.EndDate, this.ActClassTypes, this.OffenceAct, this.Kawasan, this.Jalan)
      .subscribe(items => {
        this.showSpinner = false;
        this.ReportList = items;
      });
  }

  getLaporanUtamaUndangList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    this.laporanService.getLaporanUtamaUndangList(this.StartDate, this.EndDate, this.ActClassTypes, this.OffenceAct, this.AktaInduk, this.Peruntukan)
      .subscribe(items => {
        this.showSpinner = false;
        this.ReportList = items;
      });
  }
  //close standard listing with search function

  //open default listing function
  getOffenceActList() {
    this.laporanService.getOffenceActList().subscribe(items => {
      this.OffenceActList = items;
    });
  }
  getKawasanList() {
    this.laporanService.getKawasanList().subscribe(items => {
      this.KawasanList = items;
    });
  }
  getJalanList() {
    this.laporanService.getJalanList(this.Kawasan).subscribe(items => {
      this.JalanList = items;
    });
  }
  getAktaIndukList() {
    this.laporanService.getAktaIndukList(this.OffenceAct).subscribe(items => {
      this.AktaIndukList = items;
    });
  }
  getPeruntukanList() {
    this.laporanService.getPeruntukanList(this.AktaInduk).subscribe(items => {
      this.PeruntukanList = items;
    });
  }
  //close default listing function


  onChangeKawasan() {
    this.getJalanList();
  }
  onChangeAktaInduk() {
    this.getPeruntukanList();
  }
  onChangeKelasAkta() {
    this.getAktaIndukList();
  }


  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      this.showList = true;
      this.OffenceAct = searchForm.OffenceAct;
      if (searchForm.StartDate) {
        this.DisplayStartDate = moment(searchForm.StartDate).format("DD/MM/YYYY");
      }
      if (searchForm.EndDate) {
        this.DisplayEndDate = moment(searchForm.EndDate).format("DD/MM/YYYY");
      }
      let OffenceActObject = this.OffenceActList.find(i => i.id == this.OffenceAct);
      this.ReportName = this.ReportName + ' ' + OffenceActObject.name;

      this.ReportTitle = 'LAPORAN MENGENAI PENGELUARAN NOTIS KESALAHAN ' + (OffenceActObject ? OffenceActObject.name.toUpperCase() : '') + ' / TAWARAN KOMPAUN / TINDAKAN MAHKAMAH DAN KUTIPAN BAGI ' + this.DisplayStartDate + ' - ' + this.DisplayEndDate;

      if (this.FromMenu == 'kawasan') {
        this.ReportName = this.ReportName + ' KAWASAN';
        let KawasanObject = this.KawasanList.find(i => i.id == this.Kawasan);
        let JalanObject = this.JalanList.find(i => i.id == this.Jalan);
        this.ReportTitle = this.ReportTitle + '<br>KAWASAN - ' + (KawasanObject ? KawasanObject.name.toUpperCase() + ' - ' : '') + (JalanObject ? JalanObject.name.toUpperCase() : '');
      }

      else if (this.FromMenu == 'undang-undang') {
        this.ReportName = this.ReportName + ' UNDANG-UNDANG';
        let AktaIndukObject = this.AktaIndukList.find(i => i.id == this.AktaInduk);
        let PeruntukanObject = this.PeruntukanList.find(i => i.id == this.Peruntukan);
        this.ReportTitle = this.ReportTitle + '<br>AKTA INDUK : ' + (AktaIndukObject ? AktaIndukObject.name.toUpperCase() + '<br>PERUNTUKAN UNDANG - UNDANG : ' : '') + (PeruntukanObject ? PeruntukanObject.name.toUpperCase() : '');
      }

      //retrive record          
      if (this.FromMenu == 'undang-undang') {
        this.getLaporanUtamaUndangList();
      }
      else {
        this.getLaporanUtamaList();
      }



    }
  }



  //reset search form
  resetType() {
    const OffenceAct = this.OffenceAct;
    this.reset();
    this.form = this.buildFormItems();
    this.OffenceAct = OffenceAct;
    this.form.patchValue({ OffenceAct });
    this.onChangeKelasAkta();
  }
  reset() {
    this.form = this.buildFormItems();
    //initiate list & date model
    this.OffenceAct = 0;
    this.StartDate = '';
    this.EndDate = '';
    this.ReportList = [];
    this.showList = false;
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {

      if (formGroup.dirty) {
        this.showList = false;
      }

      const StartDate = formGroup.controls['StartDate'];
      const EndDate = formGroup.controls['EndDate'];
      const OffenceAct = formGroup.controls['OffenceAct'];
      const Kawasan = formGroup.controls['Kawasan'];
      const Jalan = formGroup.controls['Jalan'];
      const AktaInduk = formGroup.controls['AktaInduk'];
      const Peruntukan = formGroup.controls['Peruntukan'];

      if (OffenceAct) {
        OffenceAct.setErrors(null);
        if (!OffenceAct.value) {
          OffenceAct.setErrors({ required: true });
        }
        else if (OffenceAct.value == 0) {
          OffenceAct.setErrors({ min: true });
        }
      }
      if (StartDate) {
        StartDate.setErrors(null);
        if (!StartDate.value) {
          StartDate.setErrors({ required: true });
        }
        else if (StartDate.value && EndDate.value && EndDate.value < StartDate.value) {
          StartDate.setErrors({ exceed: true });
        }
      }
      if (EndDate) {
        EndDate.setErrors(null);
        if (!EndDate.value) {
          EndDate.setErrors({ required: true });
        }
        else if (StartDate.value && EndDate.value && EndDate.value < StartDate.value) {
          EndDate.setErrors({ exceed: true });
        }
      }
      if (this.FromMenu == 'kawasan') {
        if (Kawasan) {
          Kawasan.setErrors(null);
          if (!Kawasan.value) {
            Kawasan.setErrors({ required: true });
          }
          else if (Kawasan.value == 0) {
            Kawasan.setErrors({ min: true });
          }
        }
        if (Jalan) {
          Jalan.setErrors(null);
          if (!Jalan.value) {
            Jalan.setErrors({ required: true });
          }
          else if (Jalan.value == 0) {
            Jalan.setErrors({ min: true });
          }
        }
      }

      if (this.FromMenu == 'undang-undang') {
        if (AktaInduk) {
          AktaInduk.setErrors(null);
          if (!AktaInduk.value) {
            AktaInduk.setErrors({ required: true });
          }
          else if (AktaInduk.value == 0) {
            AktaInduk.setErrors({ min: true });
          }
        }
        if (Peruntukan) {
          Peruntukan.setErrors(null);
          if (!Peruntukan.value) {
            Peruntukan.setErrors({ required: true });
          }
          else if (Peruntukan.value == 0) {
            Peruntukan.setErrors({ min: true });
          }
        }
      }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      OffenceAct: ['0', []],
      StartDate: ['', []],
      EndDate: ['', []],
      Kawasan: ['0', []],
      Jalan: ['0', []],
      AktaInduk: ['0', []],
      Peruntukan: ['0', []],
    }, { validator: this.customValidation() }
    );
  }


  //excel genarator
  printTableExcel() {
    var workbook = new Excel.Workbook();
    // Tab Title
    var worksheet = workbook.addWorksheet(this.ReportName);
    // Columns to display
    worksheet.mergeCells('A1', 'P1'); //title merge cell
    worksheet.getCell('A1').value = this.ReportTitle.replace('<br>', ' '); //title
    worksheet.getCell('A1').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

    worksheet.mergeCells('A2', 'A3');
    worksheet.getCell('A2').value = this.FromMenu == 'undang-undang' ? 'Jenis Kesalahan' :  this.OffenceAct == 3 ? 'Jenis' : 'Peruntukan Undang-Undang';
    worksheet.getCell('A2').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };

    worksheet.mergeCells('B2', 'D2');
    worksheet.getCell('B2').value = 'Notis Kompaun';
    worksheet.getCell('B2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('B3').value = 'Dikeluarkan';
    worksheet.getCell('B3').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('C3').value = 'Bil. Bayar';
    worksheet.getCell('C3').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('D3').value = 'Kutipan (RM)';
    worksheet.getCell('D3').alignment = { horizontal: 'right', vertical: 'top', wrapText: true };

    worksheet.mergeCells('E2', 'E3');
    worksheet.getCell('E2').value = 'Baki Notis Kesalahan';
    worksheet.getCell('E2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

    worksheet.mergeCells('F2', 'H2');
    worksheet.getCell('F2').value = 'Peringatan Tawaran Kompaun Kompaun';
    worksheet.getCell('F2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('F2').alignment = { horizontal: 'center' };
    worksheet.getCell('F3').value = 'Dikeluarkan';
    worksheet.getCell('F3').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('G3').value = 'Bil. Bayar';
    worksheet.getCell('G3').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('H3').value = 'Kutipan (RM)';
    worksheet.getCell('H3').alignment = { horizontal: 'right', vertical: 'top', wrapText: true };

    worksheet.mergeCells('I2', 'I3');
    worksheet.getCell('I2').value = 'Jumlah Kes Batal';
    worksheet.getCell('I2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

    worksheet.mergeCells('J2', 'J3');
    worksheet.getCell('J2').value = 'Jumlah Kes Untuk Tindakan Mahkamah';
    worksheet.getCell('J2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

    worksheet.mergeCells('K2', 'L2');
    worksheet.getCell('K2').value = 'Selesai Luar Mahkamah';
    worksheet.getCell('K2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('K2').alignment = { horizontal: 'center' };
    worksheet.getCell('K3').value = 'Bil. Kes';
    worksheet.getCell('K3').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('L3').value = 'Denda';
    worksheet.getCell('L3').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

    worksheet.mergeCells('M2', 'M3');
    worksheet.getCell('M2').value = 'Jumlah Kes Bayar (< 14)';
    worksheet.getCell('M2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

    worksheet.mergeCells('N2', 'N3');
    worksheet.getCell('N2').value = 'Jumlah Kes Bayar (> 14)';
    worksheet.getCell('N2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

    worksheet.mergeCells('O2', 'O3');
    worksheet.getCell('O2').value = 'Jumlah Kes Bayar';
    worksheet.getCell('O2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

    worksheet.mergeCells('P2', 'P3');
    worksheet.getCell('P2').value = 'Jumlah Kutipan Kompaun (RM)';
    worksheet.getCell('P2').alignment = { horizontal: 'right', vertical: 'top', wrapText: true };

    worksheet.columns = [
      { key: 'Col1', width: 15, style: { alignment: { horizontal: 'left', vertical: 'top', wrapText: true } } },
      { key: 'Col2', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Col3', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Col4', width: 15, style: { alignment: { horizontal: 'right', vertical: 'top', wrapText: true } } },
      { key: 'Col5', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Col6', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Col7', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Col8', width: 15, style: { alignment: { horizontal: 'right', vertical: 'top', wrapText: true } } },
      { key: 'Col9', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Col10', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Col11', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Col12', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Col13', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Col14', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Col15', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Col16', width: 15, style: { alignment: { horizontal: 'right', vertical: 'top', wrapText: true } } },
    ];


    for (let record of this.ReportList) {
      worksheet.addRow([
        record.Col1,
        record.Col2,
        record.Col3,
        record.Col4,
        record.Col5,
        record.Col6,
        record.Col7,
        record.Col8,
        record.Col9,
        record.Col10,
        record.Col11,
        record.Col12,
        record.Col13,
        record.Col14,
        record.Col15,
        record.Col16])
    }

    worksheet.eachRow(function (row, _rowNumber) {
      row.eachCell(function (cell, _colNumber) {
        cell.border = {
          top: { style: 'thin' },
          left: { style: 'thin' },
          bottom: { style: 'thin' },
          right: { style: 'thin' }
        };
      });
    });

    workbook.xlsx.writeBuffer().then(data => {
      const blob = new Blob([data], { type: this.blobType });
      //create excell file
      FileSaver.saveAs(blob, this.ReportName + '.xlsx');
    });
  }



}

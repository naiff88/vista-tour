import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { LaporanService } from '../../laporan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Moment } from 'moment';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');

@Component({
  selector: 'app-laporan-dde-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})


export class LaporanDdeListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  showList: boolean = false;
  eventSearch: any;
  onSearch: boolean = false;
  submitted: boolean = false;
  form: FormGroup;
  //close standard var for searching page

  //initiate list
  laporanDdeTypeList: any = [];

  //initiate list model 
  LaporanDdeType: number = 0;

  //initiate date model 
  BulanTahun: any = moment();
  Tahun: any = moment();
  TarikhMulaLaporan: any = '';
  TarikhAkhirLaporan: any = '';

  //dynamic report param
  TarikhMula: any = '';
  TarikhAkhir: any = '';
  NoGaji: string = '';

  //display date trick
  DisplayMonthYear: any = moment().format("MM/YYYY");;
  DisplayYear: any = moment().format("YYYY");;;


  //report props
  reportCol1: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'IdPengguna', title: 'ID Pengguna', width: '15', align: 'left' },
    { name: 'Nama', title: 'Nama', width: '20', align: 'left' },
    { name: 'JumlahNotis', title: 'Jumlah Notis', width: '15', align: 'left' },
  ];
  reportCol2: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'NoNotis', title: 'No. Notis', width: '15', align: 'left' },
    { name: 'Jenis', title: 'Jenis', width: '20', align: 'left' },
    { name: 'TarikhKesalahan', title: 'Tarikh Kesalahan', width: '15', align: 'left' },
    { name: 'TarikhWujud', title: 'Tarikh Wujud', width: '15', align: 'left' },
    { name: 'NamaPesalah', title: 'Nama Pesalah', width: '20', align: 'left' },
    { name: 'NoKenderaan', title: 'No. Kenderaan', width: '15', align: 'left' },
    { name: 'NoCukaiJalan', title: 'No. Cukai Jalan', width: '15', align: 'left' },
    { name: 'Peruntukan', title: 'Peruntukan Undang-Undang', width: '30', align: 'left' },
    { name: 'Seksyen', title: 'Seksyen Kesalahan', width: '15', align: 'left' },
    { name: 'Tempat', title: 'Tempat/Lokasi Kesalahan', width: '20', align: 'left' },
    { name: 'NoPetak', title: 'No. Petak/Tg', width: '15', align: 'left' },
  ];

  //report title
  DisplayDate: any = '';
  ReportTitle: string = 'Laporan Kemasukan Data DDE';
  ReportName = this.ReportTitle;

  //for excel ganerator 
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  //standard contructor
  constructor(
    private laporanService: LaporanService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  //form
  get f() { return this.form.controls; }

  //init methode to call default function
  ngOnInit() {
    this.getHandheldOfficerList();
    this.getLaporanDdeTypeList();
    this.loadSearchDetails();
  }

  loadSearchDetails() {
    this.form = this.buildFormItems();
  }

  //open standard listing function
  getHandheldOfficerList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    this.laporanService.getLaporanDdeList(this.pageSetting.getPagerSetting(this.paginator, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.NoGaji, this.LaporanDdeType)
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (this.onSearch) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    this.primengTableHelper.showLoadingIndicator();
  }
  //close standard listing function

  //open default listing function
  getLaporanDdeTypeList() {
    this.laporanService.getDdeTypeList().subscribe(items => {
      this.laporanDdeTypeList = items;
    });
  }
  //close default listing function

  //if diff date format in one component
  //open select month year function  
  chosenYearHandlerOnly(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const Tahun = this.form.controls['Tahun'];
    Tahun.setValue(moment());
    const ctrlValue = Tahun.value;
    ctrlValue.year(normalizedYear.year());
    Tahun.setValue(ctrlValue);
    this.DisplayYear = moment(Tahun.value).format("YYYY");
    datepicker.close();
  }
  chosenYearHandler(normalizedYear: Moment) {
    const BulanTahun = this.form.controls['BulanTahun'];
    BulanTahun.setValue(moment());
    const ctrlValue = BulanTahun.value;
    ctrlValue.year(normalizedYear.year());
    BulanTahun.setValue(ctrlValue);
    this.DisplayMonthYear = moment(BulanTahun.value).format("MM/YYYY");
  }
  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const BulanTahun = this.form.controls['BulanTahun'];
    BulanTahun.setValue(moment());
    const ctrlValue = BulanTahun.value;
    ctrlValue.month(normalizedMonth.month());
    BulanTahun.setValue(ctrlValue);
    this.DisplayMonthYear = moment(BulanTahun.value).format("MM/YYYY");
    datepicker.close();
  }
  //close select month year function  

  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      this.showList = true;
      //dynamicly display date range at report title
      if (searchForm.LaporanDdeType == 1 && searchForm.BulanTahun) {
        this.DisplayDate = moment(searchForm.BulanTahun).format("MM/YYYY");
      }
      else if (searchForm.LaporanDdeType == 2 && searchForm.Tahun) {
        this.DisplayDate = moment(searchForm.Tahun).format("YYYY");
      }
      else if (searchForm.LaporanDdeType == 3 && searchForm.TarikhMulaLaporan && searchForm.TarikhAkhirLaporan) {
        this.DisplayDate = moment(searchForm.TarikhMulaLaporan).format("DD/MM/YYYY") + ' - ' + moment(searchForm.TarikhAkhirLaporan).format("DD/MM/YYYY");
      }


      //dynamicly display report title
      let laporanType = this.laporanDdeTypeList.find(i => i.id == searchForm.LaporanDdeType);
      this.ReportName = this.ReportName + ' ' + laporanType.name;
      if (laporanType) {
        this.ReportTitle = 'LAPORAN ' + laporanType.name + ' KEMASUKAN DATA DDE ' + this.DisplayDate;
      }


      //dynamicly set date range as param to call list
      if (searchForm.LaporanDdeType == 1 && searchForm.BulanTahun) {
        this.TarikhMula = searchForm.BulanTahun.startOf('month').format('DD/MM/YYYY');
        this.TarikhAkhir = searchForm.BulanTahun.endOf('month').format('DD/MM/YYYY');
      }
      else if (searchForm.LaporanDdeType == 2 && searchForm.Tahun) {
        this.TarikhMula = searchForm.Tahun.startOf('year').format('DD/MM/YYYY');
        this.TarikhAkhir = searchForm.Tahun.endOf('year').format('DD/MM/YYYY');
      }
      else if (searchForm.LaporanDdeType == 3) {
        this.TarikhMula = searchForm.TarikhMulaLaporan.format('DD/MM/YYYY');;
        this.TarikhAkhir = searchForm.TarikhAkhirLaporan.format('DD/MM/YYYY');;
        this.NoGaji = searchForm.NoGaji;
      }


      this.onSearch = true;
      this.getHandheldOfficerList();
      this.paginator.changePage(0);
    }
  }


  //on change jenis report
  onChangeType() {
    const LaporanDdeType = this.LaporanDdeType;
    //call reset function
    this.reset();

    this.form.patchValue({ LaporanDdeType });
    this.LaporanDdeType = LaporanDdeType;
  }

  //reset search form
  resetType() {
    const LaporanDdeType = this.LaporanDdeType;
    this.reset();
    this.form = this.buildFormItems();
    this.LaporanDdeType = LaporanDdeType;
    this.form.patchValue({LaporanDdeType});
  }
  reset() {
    this.form = this.buildFormItems();
    this.LaporanDdeType = 0;
    this.BulanTahun = moment();
    this.Tahun = moment();
    this.TarikhMulaLaporan = '';
    this.TarikhAkhirLaporan = '';
    this.TarikhMula = '';
    this.TarikhAkhir = '';
    this.NoGaji = '';
    //reset list
    this.dataSource.data = [];
    this.primengTableHelper.totalRecordsCount = 0;
    this.primengTableHelper.records = [];
    this.showList = false;
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      if(formGroup.dirty)
      {
        this.showList = false;
      }  
      

      const BulanTahun = formGroup.controls['BulanTahun'];
      const Tahun = formGroup.controls['Tahun'];
      const LaporanDdeType = formGroup.controls['LaporanDdeType'];
      const TarikhMulaLaporan = formGroup.controls['TarikhMulaLaporan'];
      const TarikhAkhirLaporan = formGroup.controls['TarikhAkhirLaporan'];
      const NoGaji = formGroup.controls['NoGaji'];


      if (LaporanDdeType) {
        LaporanDdeType.setErrors(null);
        if (!LaporanDdeType.value) {
          LaporanDdeType.setErrors({ required: true });
        }
        else if (LaporanDdeType.value == 0) {
          LaporanDdeType.setErrors({ min: true });
        }
      }
      if (BulanTahun && LaporanDdeType.value == 1) {
        BulanTahun.setErrors(null);
        if (!BulanTahun.value) {
          BulanTahun.setErrors({ required: true });
        }
        else if (!BulanTahun.value) {
          BulanTahun.setErrors({ required: true });
        }
      }
      if (Tahun && LaporanDdeType.value == 2) {
        Tahun.setErrors(null);
        if (!Tahun.value) {
          Tahun.setErrors({ required: true });
        }
        else if (!Tahun.value) {
          Tahun.setErrors({ required: true });
        }
      }
      if (TarikhMulaLaporan && LaporanDdeType.value == 3) {
        TarikhMulaLaporan.setErrors(null);
        if (!TarikhMulaLaporan.value) {
          TarikhMulaLaporan.setErrors({ required: true });
        }
        else if (TarikhMulaLaporan.value && TarikhAkhirLaporan.value && TarikhAkhirLaporan.value < TarikhMulaLaporan.value) {
          TarikhMulaLaporan.setErrors({ exceed: true });
        }
      }
      if (TarikhAkhirLaporan && LaporanDdeType.value == 3) {
        TarikhAkhirLaporan.setErrors(null);
        if (!TarikhAkhirLaporan.value) {
          TarikhAkhirLaporan.setErrors({ required: true });
        }
        else if (TarikhMulaLaporan.value && TarikhAkhirLaporan.value && TarikhAkhirLaporan.value < TarikhMulaLaporan.value) {
          TarikhAkhirLaporan.setErrors({ exceed: true });
        }
      }
      if (NoGaji && LaporanDdeType.value == 3) {
        NoGaji.setErrors(null);
        if (!NoGaji.value) {
          NoGaji.setErrors({ required: true });
        }
      }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      BulanTahun: [moment(), []],
      Tahun: [moment(), []],
      LaporanDdeType: ['0', []],
      NoGaji: ['', []],
      TarikhMulaLaporan: ['', []],
      TarikhAkhirLaporan: ['', []],
    }, { validator: this.customValidation() }
    );
  }




  //excel genarator
  printTableExcel() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getLaporanDdeList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.NoGaji, this.LaporanDdeType).subscribe(items => {
        list = items.list;
        this.pageSetting.excelGenerator(this.LaporanDdeType == 1 || this.LaporanDdeType == 2 ? this.reportCol1 : this.reportCol2, list, this.ReportTitle, this.ReportTitle).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }
  //pdf genarator
  printTablePdf() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getLaporanDdeList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.NoGaji, this.LaporanDdeType).subscribe(items => {
        list = items.list;
        this.pageSetting.pdfGenerator(this.LaporanDdeType == 1 || this.LaporanDdeType == 2 ? this.reportCol1 : this.reportCol2, list, this.ReportTitle, this.ReportTitle, 'l', 7).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }



  //excel genarator
  printTableExcel2() {
    var workbook = new Excel.Workbook();
    // Tab Title
    var worksheet = workbook.addWorksheet(this.ReportName);
    // Columns to display
    let col = []
    if (this.LaporanDdeType == 1 || this.LaporanDdeType == 2) {
      col = ['No.', 'ID Pengguna', 'Nama', 'Jumlah Notis'];
    }
    else {
      col = ['No.', 'No. Notis', 'Jenis', 'Tarikh Kesalahan', 'Tarikh Kesalahan', 'Nama Pesalah', 'No. Kenderaan', 'No. Cukai Jalan', 'Peruntukan Undang-Undang', 'Seksyen Kesalahan', 'Tempat/Lokasi Kesalahan', 'No. Petak/Tg'];
    }

    worksheet.mergeCells('A1', this.LaporanDdeType == 1 || this.LaporanDdeType == 2 ? 'D1' : 'L1'); //title merge cell
    worksheet.getCell('A1').value = this.ReportTitle; //title
    worksheet.getRow(2).values = col;
    // Column key
    if (this.LaporanDdeType == 1 || this.LaporanDdeType == 2) {
      worksheet.columns = [
        { key: 'No', width: 5 },
        { key: 'IdPengguna', width: 20 },
        { key: 'Nama', width: 20 },
        { key: 'JumlahNotis', width: 20 },
      ];
    }
    else {
      worksheet.columns = [
        { key: 'No', width: 5 },
        { key: 'NoNotis', width: 15 },
        { key: 'Jenis', width: 15 },
        { key: 'TarikhKesalahan', width: 15 },
        { key: 'TarikhWujud', width: 15 },
        { key: 'NamaPesalah', width: 30 },
        { key: 'NoKenderaan', width: 15 },
        { key: 'NoCukaiJalan', width: 15 },
        { key: 'Peruntukan', width: 30 },
        { key: 'Seksyen', width: 15 },
        { key: 'Tempat', width: 15 },
        { key: 'NoPetak', width: 15 },
      ];
    }


    let i = 1;
    this.showSpinner = true;
    //get list of data
    this.laporanService.getLaporanDdeList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.NoGaji, this.LaporanDdeType)
      .subscribe(items => {
        this.showSpinner = false;
        let list = items.list;
        for (var key in list) {
          if (this.LaporanDdeType == 1 || this.LaporanDdeType == 2) {
            worksheet.addRow([i, list[key].IdPengguna, list[key].Nama, list[key].JumlahNotis])
          }
          else {
            worksheet.addRow([i, list[key].NoNotis, list[key].Jenis, list[key].TarikhKesalahan, list[key].TarikhWujud, list[key].NamaPesalah,
              list[key].NoKenderaan, list[key].NoCukaiJalan, list[key].Peruntukan, list[key].Seksyen, list[key].Tempat, list[key].NoPetak])
          }
          i++;
        }
      });

    worksheet.eachRow(function (row, _rowNumber) {
      row.eachCell(function (cell, _colNumber) {
        cell.border = {
          top: { style: 'thin' },
          left: { style: 'thin' },
          bottom: { style: 'thin' },
          right: { style: 'thin' }
        };
      });
    });

    workbook.xlsx.writeBuffer().then(data => {
      const blob = new Blob([data], { type: this.blobType });
      //create excell file
      FileSaver.saveAs(blob, this.ReportTitle + '.xlsx');
    });
  }


  //pdf genarator
  printTablePdf2() {
    console.log('this.printTablePdf title ::::::::::::::: ', this.ReportTitle);

    var doc = new jsPDF(this.LaporanDdeType == 3 ? 'l' : 'p'); //potrait setting
    // Columns to display
    let col = []
    if (this.LaporanDdeType == 1 || this.LaporanDdeType == 2) {
      col = ['No.', 'ID Pengguna', 'Nama', 'Jumlah Notis'];
    }
    else if (this.LaporanDdeType == 3) {
      col = ['No.', 'No. Notis', 'Jenis', 'Tarikh Kesalahan', 'Tarikh Kesalahan', 'Nama Pesalah', 'No. Kenderaan', 'No. Cukai Jalan', 'Peruntukan Undang-Undang', 'Seksyen Kesalahan', 'Tempat/Lokasi Kesalahan', 'No. Petak/Tg'];
    }
    var rows = [];
    let i = 1;

    this.showSpinner = true;
    //get list of data
    this.laporanService.getLaporanDdeList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.NoGaji, this.LaporanDdeType)
      .subscribe(items => {
        this.showSpinner = false;
        let list = items.list;
        for (var key in list) {

          if (this.LaporanDdeType == 1 || this.LaporanDdeType == 2) {
            rows.push([i, list[key].IdPengguna, list[key].Nama, list[key].JumlahNotis])
          }
          else {
            rows.push([i, list[key].NoNotis, list[key].Jenis, list[key].TarikhKesalahan, list[key].TarikhWujud, list[key].NamaPesalah,
              list[key].NoKenderaan, list[key].NoCukaiJalan, list[key].Peruntukan, list[key].Seksyen, list[key].Tempat, list[key].NoPetak])
          }
          i++;
        }
      });

    //report title
    var title = this.ReportTitle;
    var header = function (data) {
      doc.setFontSize(14);
      doc.setFontStyle('normal');
      doc.text(title, data.settings.margin.left, 10);

    };

    doc.autoTable(col, rows, {
      margin: { top: 15 }, theme: 'grid',
      beforePageContent: header,
      headerStyles: {
        lineWidth: 0.2,
        lineColor: [209, 209, 209],
        fillColor: [0, 0, 0],
        textColor: [255, 255, 255],
        fontSize: 8
      },
      bodyStyles: {
        fontSize: 8,
        lineWidth: 0.2,
      },
      didParseCell: this.pageSetting.enhanceWordBreak,
    },
    );

    //create pdf file
    doc.save(this.ReportTitle + '.pdf');
  }



}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanDdeListComponent } from './list.component';

describe('LaporanDdeListComponent', () => {
  let component: LaporanDdeListComponent;
  let fixture: ComponentFixture<LaporanDdeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanDdeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanDdeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

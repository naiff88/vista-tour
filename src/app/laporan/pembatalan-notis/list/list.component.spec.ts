import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanPembatalanNotisListComponent } from './list.component';

describe('LaporanPembatalanNotisListComponent', () => {
  let component: LaporanPembatalanNotisListComponent;
  let fixture: ComponentFixture<LaporanPembatalanNotisListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanPembatalanNotisListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanPembatalanNotisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

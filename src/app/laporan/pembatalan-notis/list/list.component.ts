import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { LaporanService } from '../../laporan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Moment } from 'moment';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');

@Component({
  selector: 'app-laporan-pembatalan-notis-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})


export class LaporanPembatalanNotisListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  showList: boolean = false;
  eventSearch: any;
  onSearch: boolean = false;
  submitted: boolean = false;
  form: FormGroup;
  //close standard var for searching page

  //initiate list
  laporanPembatalanNotisTypeList: any = [];
  kumpulanList: any = [];
  kumpulanPenggunaList: any = [];

  //initiate list model 
  LaporanPembatalanNotisType: number = 0;
  Kumpulan: number = 0;
  KumpulanPengguna: number = 0;

  //initiate date model 
  //BulanTahun: any = moment();
  //Tahun: any = moment();
  TarikhMula: any = '';
  TarikhAkhir: any = '';

  //dynamic report param
  // TarikhMula: any = '';
  // TarikhAkhir: any = '';
  NoGaji: string = '';

  //display date trick
  DisplayMonthYear: any = moment().format("MM/YYYY");;
  DisplayYear: any = moment().format("YYYY");;;


  //report props
  reportCol: any = [];
  reportCol1: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'KumpulanPengguna', title: 'Kumpulan Pengguna', width: '20', align: 'left' },
    { name: 'Jumlah', title: 'Jumlah', width: '15', align: 'right' },
  ];
  reportCol2: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'NoGaji', title: 'No. Gaji', width: '20', align: 'left' },
    { name: 'Nama', title: 'Nama', width: '20', align: 'left' },
    { name: 'Jumlah', title: 'Jumlah', width: '15', align: 'right' },
  ];
  reportCol3: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'Sebab', title: 'Sebab / Alasan / Justifikasi', width: '30', align: 'left' },
    { name: 'Jumlah', title: 'Jumlah', width: '15', align: 'right' },
  ];

  //report title
  DisplayDate: any = '';
  ReportTitle: string = '';
  ReportName = 'Laporan Pembatalan Notis';

  //for excel ganerator 
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  //standard contructor
  constructor(
    private laporanService: LaporanService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  //form
  get f() { return this.form.controls; }

  //init methode to call default function
  ngOnInit() {
    this.getHandheldOfficerList();
    this.getLaporanPembatalanNotisTypeList();
    this.getKumpulanPenggunaList();
    this.getKumpulanList();
    this.loadSearchDetails();
  }

  loadSearchDetails() {
    this.form = this.buildFormItems();
  }

  //open standard listing function
  getHandheldOfficerList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    this.laporanService.getLaporanPembatalanNotisList(this.pageSetting.getPagerSetting(this.paginator, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.NoGaji, this.Kumpulan, this.KumpulanPengguna, this.LaporanPembatalanNotisType)
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (this.onSearch) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    this.primengTableHelper.showLoadingIndicator();
  }
  //close standard listing function

  //open default listing function
  getLaporanPembatalanNotisTypeList() {
    this.laporanService.getPembatalanNotisTypeList().subscribe(items => {
      this.laporanPembatalanNotisTypeList = items;
    });
  }
  getKumpulanPenggunaList() {
    this.laporanService.getKumpulanPenggunaList().subscribe(items => {
      this.kumpulanPenggunaList = items;
    });
  }
  getKumpulanList() {
    this.laporanService.getKumpulanList().subscribe(items => {
      this.kumpulanList = items;
    });
  }
  //close default listing function

  

  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      this.showList = true;
      //dynamicly display date range at report title     
      this.DisplayDate = moment(searchForm.TarikhMula).format("DD/MM/YYYY") + ' - ' + moment(searchForm.TarikhAkhir).format("DD/MM/YYYY");
      //dynamicly display report title
      let laporanType = this.laporanPembatalanNotisTypeList.find(i => i.id == searchForm.LaporanPembatalanNotisType);
      //this.ReportName = this.ReportName + ' ' + laporanType.name;
      if (laporanType) {
        this.ReportTitle = 'LAPORAN PEMABATALAN NOTIS MENGIKUT ' + laporanType.name + ' BAGI ' + this.DisplayDate;
      }


      //dynamicly set date range as param to call list
      // if (searchForm.LaporanPembatalanNotisType == 1 && searchForm.BulanTahun) {
      //   this.TarikhMula = searchForm.BulanTahun.startOf('month').format('DD/MM/YYYY');
      //   this.TarikhAkhir = searchForm.BulanTahun.endOf('month').format('DD/MM/YYYY');
      // }
      // else if (searchForm.LaporanPembatalanNotisType == 2 && searchForm.Tahun) {
      //   this.TarikhMula = searchForm.Tahun.startOf('year').format('DD/MM/YYYY');
      //   this.TarikhAkhir = searchForm.Tahun.endOf('year').format('DD/MM/YYYY');
      // }
      // else if (searchForm.LaporanPembatalanNotisType == 3) {
      //   this.TarikhMula = searchForm.TarikhMula.format('DD/MM/YYYY');;
      //   this.TarikhAkhir = searchForm.TarikhAkhir.format('DD/MM/YYYY');;
      //   this.NoGaji = searchForm.NoGaji;
      // }


      this.onSearch = true;
      this.getHandheldOfficerList();
      this.paginator.changePage(0);
    }
  }


  //on change jenis report
  onChangeType() {
    const LaporanPembatalanNotisType = this.LaporanPembatalanNotisType;
    //call reset function
    this.reset();

    if(LaporanPembatalanNotisType == 1)
    {
      this.reportCol = this.reportCol1;
    } 
    else if(LaporanPembatalanNotisType == 2)
    {
      this.reportCol = this.reportCol2;
    }
    else if(LaporanPembatalanNotisType == 3)
    {
      this.reportCol = this.reportCol3;
    }

    this.form.patchValue({ LaporanPembatalanNotisType });
    this.LaporanPembatalanNotisType = LaporanPembatalanNotisType;
  }

  //reset search form
  resetType() {
    const LaporanPembatalanNotisType = this.LaporanPembatalanNotisType;
    this.reset();
    this.form = this.buildFormItems();
    this.LaporanPembatalanNotisType = LaporanPembatalanNotisType;
    this.form.patchValue({LaporanPembatalanNotisType});
  }
  reset() {
    this.form = this.buildFormItems();
    this.LaporanPembatalanNotisType = 0;
    //this.BulanTahun = moment();
    //this.Tahun = moment();
    this.TarikhMula = '';
    this.TarikhAkhir = '';
    // this.TarikhMula = '';
    // this.TarikhAkhir = '';
    this.NoGaji = '';
    //reset list
    this.dataSource.data = [];
    this.primengTableHelper.totalRecordsCount = 0;
    this.primengTableHelper.records = [];
    this.showList = false;
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      if(formGroup.dirty)
      {
        this.showList = false;
      }  
      

      // const BulanTahun = formGroup.controls['BulanTahun'];
      // const Tahun = formGroup.controls['Tahun'];
      const LaporanPembatalanNotisType = formGroup.controls['LaporanPembatalanNotisType'];
      const Kumpulan = formGroup.controls['Kumpulan'];
      const KumpulanPengguna = formGroup.controls['KumpulanPengguna'];
      const TarikhMula = formGroup.controls['TarikhMula'];
      const TarikhAkhir = formGroup.controls['TarikhAkhir'];
      const NoGaji = formGroup.controls['NoGaji'];


      if (LaporanPembatalanNotisType) {
        LaporanPembatalanNotisType.setErrors(null);
        if (!LaporanPembatalanNotisType.value) {
          LaporanPembatalanNotisType.setErrors({ required: true });
        }
        else if (LaporanPembatalanNotisType.value == 0) {
          LaporanPembatalanNotisType.setErrors({ min: true });
        }
      }    
      // if (Kumpulan) {
      //   Kumpulan.setErrors(null);
      //   if (!Kumpulan.value) {
      //     Kumpulan.setErrors({ required: true });
      //   }
      //   else if (Kumpulan.value == 0) {
      //     Kumpulan.setErrors({ min: true });
      //   }
      // }  
      // if (KumpulanPengguna) {
      //   KumpulanPengguna.setErrors(null);
      //   if (!KumpulanPengguna.value) {
      //     KumpulanPengguna.setErrors({ required: true });
      //   }
      //   else if (KumpulanPengguna.value == 0) {
      //     KumpulanPengguna.setErrors({ min: true });
      //   }
      // }     
      if (TarikhMula && LaporanPembatalanNotisType.value == 3) {
        TarikhMula.setErrors(null);
        if (!TarikhMula.value) {
          TarikhMula.setErrors({ required: true });
        }
        else if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhMula.setErrors({ exceed: true });
        }
      }
      if (TarikhAkhir && LaporanPembatalanNotisType.value == 3) {
        TarikhAkhir.setErrors(null);
        if (!TarikhAkhir.value) {
          TarikhAkhir.setErrors({ required: true });
        }
        else if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhAkhir.setErrors({ exceed: true });
        }
      }
      // if (NoGaji && LaporanPembatalanNotisType.value == 3) {
      //   NoGaji.setErrors(null);
      //   if (!NoGaji.value) {
      //     NoGaji.setErrors({ required: true });
      //   }
      // }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      // BulanTahun: [moment(), []],
      // Tahun: [moment(), []],
      LaporanPembatalanNotisType: ['0', []],
      NoGaji: ['', []],
      Kumpulan: ['0', []],
      KumpulanPengguna: ['0', []],
      TarikhMula: ['', []],
      TarikhAkhir: ['', []],
    }, { validator: this.customValidation() }
    );
  }




  //excel genarator
  printTableExcel() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getLaporanPembatalanNotisList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.NoGaji, this.Kumpulan, this.KumpulanPengguna, this.LaporanPembatalanNotisType).subscribe(items => {
        list = items.list;
        this.pageSetting.excelGenerator(this.reportCol, list, this.ReportName, this.ReportTitle.toUpperCase()).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }
  //pdf genarator
  printTablePdf() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getLaporanPembatalanNotisList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.NoGaji, this.Kumpulan, this.KumpulanPengguna, this.LaporanPembatalanNotisType).subscribe(items => {
        list = items.list;
        this.pageSetting.pdfGenerator(this.reportCol, list, this.ReportName, this.ReportTitle.toUpperCase(), 'p', 9).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }


}

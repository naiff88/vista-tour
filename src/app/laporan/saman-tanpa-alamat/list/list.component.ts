import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { LaporanService } from '../../laporan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Moment } from 'moment';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');

@Component({
  selector: 'app-laporan-saman-tanpa-alamat-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})


export class LaporanSamanTanpaAlamatListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  showList: boolean = false;
  eventSearch: any;
  onSearch: boolean = false;
  submitted: boolean = false;
  form: FormGroup;
  //close standard var for searching page

  //initiate list
  // laporanSamanTanpaAlamatTypeList: any = [];
  // kumpulanList: any = [];
  // kumpulanPenggunaList: any = [];

  // JenisDokumenList: any = [];
  // JenisNotisList: any = [];
  // StatusNotisList: any = [];

  //initiate list model 
  // LaporanSamanTanpaAlamatType: number = 0;
  // Kumpulan: number = 0;
  // KumpulanPengguna: number = 0;
  // JenisDokumen: number = 0;
  // JenisNotis: number = 0;
  // StatusNotis: number = 0;

  //initiate date model 
  TarikhMula: any = '';
  TarikhAkhir: any = '';
  reportCol: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'NoNotis', title: 'No. Notis', width: '20', align: 'left' },
    { name: 'TarikhKesalahan', title: 'Tarikh Kesalahan', width: '20', align: 'left' },   
    { name: 'NamaKesalahan', title: 'Nama Kesalahan', width: '40', align: 'left' },  
    { name: 'Seksyen', title: 'Seksyen', width: '20', align: 'left' },
    { name: 'NamaPesalah', title: 'Nama Pesalah', width: '40', align: 'left' },  
    { name: 'NoPesalah', title: 'No. KP', width: '30', align: 'left' },
    { name: 'NoKenderaan', title: 'No. Kenderaan', width: '30', align: 'left' },
    { name: 'Kompaun', title: 'Kadar Kompaun (RM)', width: '20', align: 'right' },
  ];

  //report title
  DisplayDate: any = '';
  ReportTitle: string = '';
  ReportName = 'Laporan Saman Tanpa Alamat';

  //for excel ganerator 
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  //standard contructor
  constructor(
    private laporanService: LaporanService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  //form
  get f() { return this.form.controls; }

  //init methode to call default function
  ngOnInit() {
    this.loadSearchDetails();
    this.getLaporanSamanTanpaAlamatList();
    // this.getJenisDokumenList();
    // this.getJenisNotisList();
    // this.getStatusNotisList();  
  }

  loadSearchDetails() {
    this.form = this.buildFormItems();
  }

  //open standard listing function
  getLaporanSamanTanpaAlamatList(event?: LazyLoadEvent) {
    
    this.showSpinner = true;
    this.laporanService.getLaporanSamanTanpaAlamatList(this.pageSetting.getPagerSetting(this.paginator, event, this.primengTableHelper.getSorting(this.dataTable)),
    this.form.value)
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (this.onSearch) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    this.primengTableHelper.showLoadingIndicator();
  }
  //close standard listing function

  //open default listing function
  // getStatusNotisList() {
  //   this.laporanService.getStatusNotisList().subscribe(items => {
  //     this.StatusNotisList = items;
  //   });
  // }
  // getJenisDokumenList() {
  //   this.laporanService.getJenisDokumenList().subscribe(items => {
  //     this.JenisDokumenList = items;
  //   });
  // }
  // getJenisNotisList() {
  //   this.laporanService.getJenisNotisList().subscribe(items => {
  //     this.JenisNotisList = items;
  //   });
  // }
  //close default listing function

  

  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      this.showList = true;
      //dynamicly display date range at report title     
      this.DisplayDate = moment(searchForm.TarikhMula).format("DD/MM/YYYY") + ' - ' + moment(searchForm.TarikhAkhir).format("DD/MM/YYYY");
      //dynamicly display report title
      // let JenisNotis = this.JenisNotisList.find(i => i.id == searchForm.JenisNotis);
      // let JenisDokumen = this.JenisDokumenList.find(i => i.id == searchForm.JenisDokumen);
      //this.ReportName = this.ReportName + ' ' + laporanType.name;
     
      this.ReportTitle = 'LAPORAN SAMAN YANG AKHIR TEMPOH KOMPAUN TANPA ALAMAT BAGI ' + this.DisplayDate;
      


      //dynamicly set date range as param to call list
      // if (searchForm.LaporanSamanTanpaAlamatType == 1 && searchForm.BulanTahun) {
      //   this.TarikhMula = searchForm.BulanTahun.startOf('month').format('DD/MM/YYYY');
      //   this.TarikhAkhir = searchForm.BulanTahun.endOf('month').format('DD/MM/YYYY');
      // }
      // else if (searchForm.LaporanSamanTanpaAlamatType == 2 && searchForm.Tahun) {
      //   this.TarikhMula = searchForm.Tahun.startOf('year').format('DD/MM/YYYY');
      //   this.TarikhAkhir = searchForm.Tahun.endOf('year').format('DD/MM/YYYY');
      // }
      // else if (searchForm.LaporanSamanTanpaAlamatType == 3) {
      //   this.TarikhMula = searchForm.TarikhMula.format('DD/MM/YYYY');;
      //   this.TarikhAkhir = searchForm.TarikhAkhir.format('DD/MM/YYYY');;
      //   this.NoGaji = searchForm.NoGaji;
      // }


      this.onSearch = true;
      this.getLaporanSamanTanpaAlamatList();
      this.paginator.changePage(0);
    }
  }


  //on change jenis report
  // onChangeType() {
  //   const LaporanSamanTanpaAlamatType = this.LaporanSamanTanpaAlamatType;
  //   //call reset function
  //   this.reset();

  //   if(LaporanSamanTanpaAlamatType == 1)
  //   {
  //     this.reportCol = this.reportCol1;
  //   } 
  //   else if(LaporanSamanTanpaAlamatType == 2)
  //   {
  //     this.reportCol = this.reportCol2;
  //   }
  //   else if(LaporanSamanTanpaAlamatType == 3)
  //   {
  //     this.reportCol = this.reportCol3;
  //   }

  //   this.form.patchValue({ LaporanSamanTanpaAlamatType });
  //   this.LaporanSamanTanpaAlamatType = LaporanSamanTanpaAlamatType;
  // }

  //reset search form  
  reset() {
    this.form = this.buildFormItems();
    // this.JenisDokumen = 0;
    // this.JenisNotis = 0;
    // this.StatusNotis = 0;
    this.TarikhMula = '';
    this.TarikhAkhir = '';
    //reset list
    this.dataSource.data = [];
    this.primengTableHelper.totalRecordsCount = 0;
    this.primengTableHelper.records = [];
    this.showList = false;
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      if(formGroup.dirty)
      {
        this.showList = false;
      }  
      // const JenisDokumen = formGroup.controls['JenisDokumen'];
      // const JenisNotis = formGroup.controls['JenisNotis'];
      // const StatusNotis = formGroup.controls['StatusNotis'];
      const TarikhMula = formGroup.controls['TarikhMula'];
      const TarikhAkhir = formGroup.controls['TarikhAkhir'];
      const Hari = formGroup.controls['Hari'];


      // if (JenisNotis) {
      //   JenisNotis.setErrors(null);
      //   if (!JenisNotis.value) {
      //     JenisNotis.setErrors({ required: true });
      //   }
      //   else if (JenisNotis.value == 0) {
      //     JenisNotis.setErrors({ min: true });
      //   }
      // }  
      // if (JenisDokumen) {
      //   JenisDokumen.setErrors(null);
      //   if (!JenisDokumen.value) {
      //     JenisDokumen.setErrors({ required: true });
      //   }
      //   else if (JenisDokumen.value == 0) {
      //     JenisDokumen.setErrors({ min: true });
      //   }
      // }  
      // if (StatusNotis) {
      //   StatusNotis.setErrors(null);
      //   if (!StatusNotis.value) {
      //     StatusNotis.setErrors({ required: true });
      //   }
      //   else if (StatusNotis.value == 0) {
      //     StatusNotis.setErrors({ min: true });
      //   }
      // }    
      if (TarikhMula) {
        TarikhMula.setErrors(null);
        if (!TarikhMula.value) {
          TarikhMula.setErrors({ required: true });
        }
        else if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhMula.setErrors({ exceed: true });
        }
      }
      if (TarikhAkhir) {
        TarikhAkhir.setErrors(null);
        if (!TarikhAkhir.value) {
          TarikhAkhir.setErrors({ required: true });
        }
        else if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhAkhir.setErrors({ exceed: true });
        }
      }
      if (Hari) {
        Hari.setErrors(null);
        if (!Hari.value) {
          Hari.setErrors({ required: true });
        }
      }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      // JenisDokumen: ['0', []],
      // JenisNotis: ['0', []],
      // StatusNotis: ['0', []],
      Hari: ['', []],    
      TarikhMula: ['', []],
      TarikhAkhir: ['', []],
    }, { validator: this.customValidation() }
    );
  }

  //excel genarator
  printTableExcel() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getLaporanSamanTanpaAlamatList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
    this.form.value).subscribe(items => {
        list = items.list;
        this.pageSetting.excelGenerator(this.reportCol, list, this.ReportName, this.ReportTitle.toUpperCase()).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }
  //pdf genarator
  printTablePdf() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getLaporanSamanTanpaAlamatList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
    this.form.value).subscribe(items => {
        list = items.list;
        this.pageSetting.pdfGenerator(this.reportCol, list, this.ReportName, this.ReportTitle.toUpperCase(), 'p', 7).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }


}

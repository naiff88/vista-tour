import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanSamanTanpaAlamatListComponent } from './list.component';

describe('LaporanSamanTanpaAlamatListComponent', () => {
  let component: LaporanSamanTanpaAlamatListComponent;
  let fixture: ComponentFixture<LaporanSamanTanpaAlamatListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanSamanTanpaAlamatListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanSamanTanpaAlamatListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

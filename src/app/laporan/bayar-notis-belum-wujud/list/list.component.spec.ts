import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanBayarNotisBelumWujudListComponent } from './list.component';

describe('LaporanBayarNotisBelumWujudListComponent', () => {
  let component: LaporanBayarNotisBelumWujudListComponent;
  let fixture: ComponentFixture<LaporanBayarNotisBelumWujudListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanBayarNotisBelumWujudListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanBayarNotisBelumWujudListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

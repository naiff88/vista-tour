import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanSenaraiHitamListComponent } from './list.component';

describe('LaporanSenaraiHitamListComponent', () => {
  let component: LaporanSenaraiHitamListComponent;
  let fixture: ComponentFixture<LaporanSenaraiHitamListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanSenaraiHitamListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanSenaraiHitamListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

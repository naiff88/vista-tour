import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanTindakanMahkamahListComponent } from './list.component';

describe('LaporanTindakanMahkamahListComponent', () => {
  let component: LaporanTindakanMahkamahListComponent;
  let fixture: ComponentFixture<LaporanTindakanMahkamahListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanTindakanMahkamahListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanTindakanMahkamahListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

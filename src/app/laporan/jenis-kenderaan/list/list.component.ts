import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { LaporanService } from '../../laporan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Moment } from 'moment';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');

@Component({
  selector: 'app-laporan-jenis-kenderaan-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})


export class LaporanJenisKenderaanListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  showList: boolean = false;
  eventSearch: any;
  onSearch: boolean = false;
  submitted: boolean = false;
  form: FormGroup;
  //close standard var for searching page

  //initiate list
  //JenisLaporanList: any = [];
  JenisBadanList: any = [];
  JenisList: any = [];

  //initiate list model 
  //JenisLaporan: number = 0;
  JenisBadan: number = 0;
  Jenis: number = 0;

  //initiate date model 
  //BulanTahun: any = moment();
  //Tahun: any = moment();
  TarikhMulaLaporan: any = '';
  TarikhAkhirLaporan: any = '';

  //dynamic report param
  TarikhMula: any = '';
  TarikhAkhir: any = '';
  //NoGaji: string = '';

  //display date trick
  DisplayMonthYear: any = moment().format("MM/YYYY");;
  DisplayYear: any = moment().format("YYYY");;;


  //report props
  // reportCol1: any = [
  //   { name: 'index', title: 'No.', width: '5', align: 'center' },
  //   { name: 'IdPengguna', title: 'ID Pengguna', width: '15', align: 'left' },
  //   { name: 'Nama', title: 'Nama', width: '20', align: 'left' },
  //   { name: 'JumlahNotis', title: 'Jumlah Notis', width: '15', align: 'left' },
  // ];
  reportCol: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'NoNotis', title: 'No. Notis', width: '15', align: 'left' },
    { name: 'NamaPesalah', title: 'Nama', width: '20', align: 'left' },
    { name: 'NamaPemandu', title: 'Nama Pemandu', width: '20', align: 'left' },
    { name: 'KpPemandu', title: 'No. KP Pemandu', width: '20', align: 'left' },
    { name: 'Kompaun', title: 'Jumlah Kompaun (RM)', width: '20', align: 'right' },
    { name: 'TarikhKesalahan', title: 'Tarikh Kesalahan', width: '15', align: 'left' },
    { name: 'NoKenderaan', title: 'No. Kenderaan', width: '15', align: 'left' },
    { name: 'NoResit', title: 'No. Resit', width: '15', align: 'left' },
    { name: 'TarikhBayaran', title: 'Tarikh Bayaran', width: '15', align: 'left' },
    { name: 'Lokasi', title: 'Lokasi', width: '30', align: 'left' },
    { name: 'Bayaran', title: 'Jumlah Bayaran (RM)', width: '20', align: 'right' },
    { name: 'JenisKenderaan', title: 'Jenis Kenderaan', width: '20', align: 'left' },
  ];

  //report title
  DisplayDate: any = '';
  ReportTitle: string = 'Laporan Jenis Kenderaan';
  ReportName = this.ReportTitle;

  //for excel ganerator 
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  //standard contructor
  constructor(
    private laporanService: LaporanService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  //form
  get f() { return this.form.controls; }

  //init methode to call default function
  ngOnInit() { 
    //this.getJenisLaporan();
    this.getJenisBadan();
    this.getJenis();
    this.loadSearchDetails();
    this.getLaporanJenisKenderaanList();
  }

  loadSearchDetails() {
    this.form = this.buildFormItems();
  }

  //open standard listing function
  getLaporanJenisKenderaanList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    this.laporanService.getLaporanJenisKenderaanList(this.pageSetting.getPagerSetting(this.paginator, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir,  this.form.value)
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (this.onSearch) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    this.primengTableHelper.showLoadingIndicator();
  }
  //close standard listing function

  //open default listing function
  // getJenisLaporan() {
  //   this.laporanService.getPembayaranNotisTypeList().subscribe(items => {
  //     this.JenisLaporanList = items;
  //   });
  // }
  getJenisBadan() {
    this.laporanService.getJenisBadanList().subscribe(items => {
      this.JenisBadanList = items;
    });
  }
  getJenis() {
    this.laporanService.getJenisList().subscribe(items => {
      this.JenisList = items;
    });
  }
  //close default listing function

  //if diff date format in one component
  //open select month year function  
  chosenYearHandlerOnly(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const Tahun = this.form.controls['Tahun'];
    Tahun.setValue(moment());
    const ctrlValue = Tahun.value;
    ctrlValue.year(normalizedYear.year());
    Tahun.setValue(ctrlValue);
    this.DisplayYear = moment(Tahun.value).format("YYYY");
    datepicker.close();
  }
  chosenYearHandler(normalizedYear: Moment) {
    const BulanTahun = this.form.controls['BulanTahun'];
    BulanTahun.setValue(moment());
    const ctrlValue = BulanTahun.value;
    ctrlValue.year(normalizedYear.year());
    BulanTahun.setValue(ctrlValue);
    this.DisplayMonthYear = moment(BulanTahun.value).format("MM/YYYY");
  }
  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const BulanTahun = this.form.controls['BulanTahun'];
    BulanTahun.setValue(moment());
    const ctrlValue = BulanTahun.value;
    ctrlValue.month(normalizedMonth.month());
    BulanTahun.setValue(ctrlValue);
    this.DisplayMonthYear = moment(BulanTahun.value).format("MM/YYYY");
    datepicker.close();
  }
  //close select month year function  

  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      this.showList = true;
      //dynamicly display date range at report title
      // if (searchForm.JenisLaporan == 1 && searchForm.BulanTahun) {
      //   this.DisplayDate = moment(searchForm.BulanTahun).format("MM/YYYY");
      // }
      // else if (searchForm.JenisLaporan == 2 && searchForm.Tahun) {
      //   this.DisplayDate = moment(searchForm.Tahun).format("YYYY");
      // }
      // else if (searchForm.JenisLaporan == 3 && searchForm.TarikhMulaLaporan && searchForm.TarikhAkhirLaporan) {
      //   this.DisplayDate = moment(searchForm.TarikhMulaLaporan).format("DD/MM/YYYY") + ' - ' + moment(searchForm.TarikhAkhirLaporan).format("DD/MM/YYYY");
      // }

      if (searchForm.TarikhMulaLaporan && searchForm.TarikhAkhirLaporan) {
          this.DisplayDate = moment(searchForm.TarikhMulaLaporan).format("DD/MM/YYYY") + ' - ' + moment(searchForm.TarikhAkhirLaporan).format("DD/MM/YYYY");
          this.TarikhMula = searchForm.TarikhMulaLaporan.format('DD/MM/YYYY');;
          this.TarikhAkhir = searchForm.TarikhAkhirLaporan.format('DD/MM/YYYY');;
        }

      //dynamicly display report title
      //let laporanType = this.JenisLaporanList.find(i => i.id == searchForm.JenisLaporan);
      //this.ReportName = this.ReportName + ' ' + laporanType.name;
      //if (laporanType) {
        //this.ReportTitle = 'LAPORAN ' + laporanType.name + ' KEMASUKAN DATA JENIS KENDERAAN ' + this.DisplayDate;
      //}
      this.ReportTitle = 'LAPORAN JENIS KENDERAAN ' + this.DisplayDate;

      //dynamicly set date range as param to call list
      // if (searchForm.JenisLaporan == 1 && searchForm.BulanTahun) {
      //   this.TarikhMula = searchForm.BulanTahun.startOf('month').format('DD/MM/YYYY');
      //   this.TarikhAkhir = searchForm.BulanTahun.endOf('month').format('DD/MM/YYYY');
      // }
      // else if (searchForm.JenisLaporan == 2 && searchForm.Tahun) {
      //   this.TarikhMula = searchForm.Tahun.startOf('year').format('DD/MM/YYYY');
      //   this.TarikhAkhir = searchForm.Tahun.endOf('year').format('DD/MM/YYYY');
      // }
      // else if (searchForm.JenisLaporan == 3) {
      //   this.TarikhMula = searchForm.TarikhMulaLaporan.format('DD/MM/YYYY');;
      //   this.TarikhAkhir = searchForm.TarikhAkhirLaporan.format('DD/MM/YYYY');;
      //   // this.NoGaji = searchForm.NoGaji;
      // }


      this.onSearch = true;
      this.getLaporanJenisKenderaanList();
      this.paginator.changePage(0);
    }
  }


  //on change jenis report
  // onChangeType() {
  //   const JenisLaporan = this.JenisLaporan;
  //   const Jenis = this.Jenis;
  //   const JenisBadan = this.JenisBadan;
  //   //call reset function
  //   this.reset();
  //   this.form.patchValue({ JenisLaporan, Jenis, JenisBadan });
  //   this.JenisLaporan = JenisLaporan;
  //   this.Jenis = Jenis;
  //   this.JenisBadan = JenisBadan;
  // }

  //reset search form
  // resetType() {
  //   const JenisLaporan = this.JenisLaporan;
  //   const Jenis = this.Jenis;
  //   const JenisBadan = this.JenisBadan;
  //   this.reset();
  //   this.form = this.buildFormItems();
  //   this.JenisLaporan = JenisLaporan;
  //   this.Jenis = Jenis;
  //   this.JenisBadan = JenisBadan;
  //   this.form.patchValue({JenisLaporan});
  // }
  reset() {
    this.form = this.buildFormItems();
    //this.JenisLaporan = 0;
    this.JenisBadan = 0;
    this.Jenis = 0;
    // this.BulanTahun = moment();
    // this.Tahun = moment();
    this.TarikhMulaLaporan = '';
    this.TarikhAkhirLaporan = '';
    this.TarikhMula = '';
    this.TarikhAkhir = '';
    //reset list
    this.dataSource.data = [];
    this.primengTableHelper.totalRecordsCount = 0;
    this.primengTableHelper.records = [];
    this.showList = false;
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      if(formGroup.dirty)
      {
        this.showList = false;
      }  
      

      // const BulanTahun = formGroup.controls['BulanTahun'];
      // const Tahun = formGroup.controls['Tahun'];
      // const JenisLaporan = formGroup.controls['JenisLaporan'];
      const JenisBadan = formGroup.controls['JenisBadan'];
      const Jenis = formGroup.controls['Jenis'];
      const TarikhMulaLaporan = formGroup.controls['TarikhMulaLaporan'];
      const TarikhAkhirLaporan = formGroup.controls['TarikhAkhirLaporan'];
      // const NoGaji = formGroup.controls['NoGaji'];


      // if (JenisLaporan) {
      //   JenisLaporan.setErrors(null);
      //   if (!JenisLaporan.value) {
      //     JenisLaporan.setErrors({ required: true });
      //   }
      //   else if (JenisLaporan.value == 0) {
      //     JenisLaporan.setErrors({ min: true });
      //   }
      // }
      if (JenisBadan) {
        JenisBadan.setErrors(null);
        if (!JenisBadan.value) {
          JenisBadan.setErrors({ required: true });
        }
        else if (JenisBadan.value == 0) {
          JenisBadan.setErrors({ min: true });
        }
      }
      if (Jenis) {
        Jenis.setErrors(null);
        if (!Jenis.value) {
          Jenis.setErrors({ required: true });
        }
        else if (Jenis.value == 0) {
          Jenis.setErrors({ min: true });
        }
      }
      // if (BulanTahun && JenisLaporan.value == 1) {
      //   BulanTahun.setErrors(null);
      //   if (!BulanTahun.value) {
      //     BulanTahun.setErrors({ required: true });
      //   }
      //   else if (!BulanTahun.value) {
      //     BulanTahun.setErrors({ required: true });
      //   }
      // }
      // if (Tahun && JenisLaporan.value == 2) {
      //   Tahun.setErrors(null);
      //   if (!Tahun.value) {
      //     Tahun.setErrors({ required: true });
      //   }
      //   else if (!Tahun.value) {
      //     Tahun.setErrors({ required: true });
      //   }
      // }
      //if (TarikhMulaLaporan && JenisLaporan.value == 3) {
        TarikhMulaLaporan.setErrors(null);
        if (!TarikhMulaLaporan.value) {
          TarikhMulaLaporan.setErrors({ required: true });
        }
        else if (TarikhMulaLaporan.value && TarikhAkhirLaporan.value && TarikhAkhirLaporan.value < TarikhMulaLaporan.value) {
          TarikhMulaLaporan.setErrors({ exceed: true });
        }
      //}
      //if (TarikhAkhirLaporan && JenisLaporan.value == 3) {
        TarikhAkhirLaporan.setErrors(null);
        if (!TarikhAkhirLaporan.value) {
          TarikhAkhirLaporan.setErrors({ required: true });
        }
        else if (TarikhMulaLaporan.value && TarikhAkhirLaporan.value && TarikhAkhirLaporan.value < TarikhMulaLaporan.value) {
          TarikhAkhirLaporan.setErrors({ exceed: true });
        }
      //}
      // if (NoGaji && JenisLaporan.value == 3) {
      //   NoGaji.setErrors(null);
      //   if (!NoGaji.value) {
      //     NoGaji.setErrors({ required: true });
      //   }
      // }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      // BulanTahun: [moment(), []],
      // Tahun: [moment(), []],
      //JenisLaporan: ['0', []],
      JenisBadan: ['0', []],
      Jenis: ['0', []],
      // NoGaji: ['', []],
      TarikhMulaLaporan: ['', []],
      TarikhAkhirLaporan: ['', []],
    }, { validator: this.customValidation() }
    );
  }




  //excel genarator
  printTableExcel() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getLaporanJenisKenderaanList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.form.value).subscribe(items => {
        list = items.list;
        this.pageSetting.excelGenerator(this.reportCol, list, this.ReportName, this.ReportTitle).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }
  //pdf genarator
  printTablePdf() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getLaporanJenisKenderaanList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir,  this.form.value).subscribe(items => {
        list = items.list;
        this.pageSetting.pdfGenerator(this.reportCol, list, this.ReportName, this.ReportTitle, 'l', 7).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }






}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanJenisKenderaanListComponent } from './list.component';

describe('LaporanJenisKenderaanListComponent', () => {
  let component: LaporanJenisKenderaanListComponent;
  let fixture: ComponentFixture<LaporanJenisKenderaanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanJenisKenderaanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanJenisKenderaanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

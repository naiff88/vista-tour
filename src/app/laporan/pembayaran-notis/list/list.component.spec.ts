import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanPembayaranNotisListComponent } from './list.component';

describe('LaporanPembayaranNotisListComponent', () => {
  let component: LaporanPembayaranNotisListComponent;
  let fixture: ComponentFixture<LaporanPembayaranNotisListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanPembayaranNotisListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanPembayaranNotisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

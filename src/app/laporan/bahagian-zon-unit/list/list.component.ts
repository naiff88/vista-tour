import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router, ActivatedRoute } from '@angular/router';
import { LaporanService } from '../../laporan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');
import html2canvas from 'html2canvas'



@Component({
  selector: 'app-laporan-bahagian-zon-unit-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class LaporanBahagianZonUnitListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('printContent', { static: false }) printContent: ElementRef;
  showSpinner: boolean = false;
  submitted: boolean = false;
  showList: boolean = false;
  form: FormGroup;
  FromMenu: string = '';
  //close standard var for searching page

  // any unique name for searching list
  reportName = 'Laporan '

  //initiate list
  OffenceActList: any = [];
  ReportList: any = [];
  PeruntukanList: any = [];
  BahagianList: any = [];
  // KawasanList: any = [];
  // JalanList: any = [];

  //initiate list model 
  //ActClassTypes: number = 1;
  OffenceAct: number = 0;
  // Kawasan: number = 0;
  // Jalan: number = 0;

  //initiate date model 
  StartDate: any = '';
  EndDate: any = '';

  bahagianPeruntukanCount:any = {};
  JumlahByBahagian:any = {};
  JumlahByPeruntukan:any = {};
  JumlahOverall:number = 0;

  //report title
  DisplayStartDate: any = '';
  DisplayEndDate: any = '';
  ReportTitle = '';
  ReportName = 'Laporan Bahagian Zon Unit';


  //for excel ganerator 
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  //standard contructor
  constructor(
    private laporanService: LaporanService,
    private snackBar: MatSnackBar,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.pageSetting = new PagerSetting();
    this.primengTableHelper = new PrimengTableHelper();
  }

  //form
  get f() { return this.form.controls; }

  //init methode to call default function
  ngOnInit() {
    this.form = this.buildFormItems();
    this.activatedRoute.params.subscribe(params => {
      this.FromMenu = params.fromMenu;
    });

    if (this.FromMenu == 'kawasan') {
      this.ReportName = 'Laporan Per Undang - Undang Bagi Kawasan';
    }

    //this.getLaporanBahagianZonUnitList();
    this.getOffenceActList();
   
    // this.getKawasanList();
    // this.getJalanList();
  }

  //open standard listing with search function
  getLaporanBahagianZonUnitList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    if(this.FromMenu == 'kawasan') {
      this.laporanService.getLaporanKawasanList(this.StartDate, this.EndDate, this.OffenceAct)
      .subscribe(items => {
        this.showSpinner = false;
        this.ReportList = items;
      });
    } else {    
    this.laporanService.getLaporanBahagianZonUnitList(this.StartDate, this.EndDate, this.OffenceAct)
      .subscribe(items => {
        this.showSpinner = false;
        this.ReportList = items;
      });
    }
  }
  //close standard listing with search function

  //open default listing function
  getOffenceActList() {
    this.laporanService.getOffenceActList2().subscribe(items => {
      this.OffenceActList = items;
    });
  }
  getPeruntukanByKelasAktaList() {
    this.laporanService.getPeruntukanByKelasAktaList(this.OffenceAct).subscribe(items => {
      this.PeruntukanList = items;
    });
  }
  getBahagianList() {
    if(this.FromMenu == 'kawasan') {
      this.laporanService.getKawasanList().subscribe(items => {
        this.BahagianList = items;
      });
    }
    else {
      this.laporanService.getBahagianList().subscribe(items => {
        this.BahagianList = items;
      });
    }  
  }


  
  // getKawasanList() {
  //   this.laporanService.getKawasanList().subscribe(items => {
  //     this.KawasanList = items;
  //   });
  // }
  // getJalanList() {
  //   this.laporanService.getJalanList(this.Kawasan).subscribe(items => {
  //     this.JalanList = items;
  //   });
  // }
  //close default listing function


  // onChangeKawasan() {
  //   this.getJalanList();
  // }

  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.bahagianPeruntukanCount = {};
      this.JumlahByBahagian = {};
      this.JumlahByPeruntukan = {};
      this.JumlahOverall = 0;

      this.submitted = false;
      this.showList = true;
      this.OffenceAct = searchForm.OffenceAct;
      if (searchForm.StartDate) {
        this.DisplayStartDate = moment(searchForm.StartDate).format("DD/MM/YYYY");
      }
      if (searchForm.EndDate) {
        this.DisplayEndDate = moment(searchForm.EndDate).format("DD/MM/YYYY");
      }
      let OffenceActObject = this.OffenceActList.find(i => i.id == this.OffenceAct);
      this.ReportName = this.ReportName + ' ' + OffenceActObject.name;      
     
      this.ReportTitle = 'LAPORAN MENGENAI PENGELUARAN NOTIS KESALAHAN'  ;
      if (this.FromMenu == 'kawasan') {
        this.ReportTitle = this.ReportTitle + ' MENGIKUT PERUNTUKAN UNDANG - UNDANG DAN KAWASAN KESALAHAN ' + (OffenceActObject ? OffenceActObject.name.toUpperCase() : '');
      }
      else {
        this.ReportTitle = this.ReportTitle + ' '+(OffenceActObject ? OffenceActObject.name.toUpperCase() : '') + ' MENGIKUT PERUNTUKAN UNDANG - UNDANG DAN BAHAGIAN/ZON/UNIT';
      }      
      this.ReportTitle = this.ReportTitle + ' BAGI ' + this.DisplayStartDate + ' - ' + this.DisplayEndDate;

    
      //retrive record     
      this.getBahagianList();
      this.getLaporanBahagianZonUnitList();

      this.getPeruntukanByKelasAktaList();      
      this.getCountByBahagianPeruntukan();
    }
  }



  //reset search form
  resetType() {
    const OffenceAct = this.OffenceAct;
    this.reset();
    this.form = this.buildFormItems();
    this.OffenceAct = OffenceAct;
    this.form.patchValue({ OffenceAct });   
  }
  reset() {
    this.form = this.buildFormItems();
    //initiate list & date model
    this.OffenceAct = 0;
    this.StartDate = '';
    this.EndDate = '';
    this.ReportList = [];
    this.showList = false;
  }

  //calculate function
  getCountByBahagianPeruntukan() {
    //console.log('getCountByBahagianPeruntukan bahagianId ::::: ',  bahagianId);
    //console.log('getCountByBahagianPeruntukan peruntukanId ::::: ',  peruntukanId);
    //console.log('getCountByBahagianPeruntukan ReportList ::::: ',  this.ReportList);

    for (var key in this.ReportList) {
      if(!this.JumlahByBahagian[this.ReportList[key].Bahagian])
      {
        this.JumlahByBahagian[this.ReportList[key].Bahagian] = 0;
      }
      if(!this.JumlahByPeruntukan[this.ReportList[key].Peruntukan])
      {
        this.JumlahByPeruntukan[this.ReportList[key].Peruntukan] = 0;
      }
      // console.log('getCountByBahagianPeruntukan Bahagian ::::: ',   this.ReportList[key].Bahagian);
      // console.log('getCountByBahagianPeruntukan Peruntukan ::::: ',   this.ReportList[key].Peruntukan);
      // console.log('getCountByBahagianPeruntukan Jumlah ::::: ',   this.ReportList[key].Jumlah);
      this.bahagianPeruntukanCount[this.ReportList[key].Bahagian+'_'+this.ReportList[key].Peruntukan] = this.ReportList[key].Jumlah;
      this.JumlahByBahagian[this.ReportList[key].Bahagian] = this.JumlahByBahagian[this.ReportList[key].Bahagian] + this.ReportList[key].Jumlah;
      this.JumlahByPeruntukan[this.ReportList[key].Peruntukan] = this.JumlahByPeruntukan[this.ReportList[key].Peruntukan] + this.ReportList[key].Jumlah;
      this.JumlahOverall = this.JumlahOverall + this.ReportList[key].Jumlah;
    }
    //console.log('habis ::::: ',  this.ReportList);
    //this.bahagianPeruntukanCount[bahagianId+'_'+peruntukanId] = 
    // this.laporanService.getBahagianList().subscribe(items => {
    //   this.BahagianList = items;
    // });
    //return peruntukanId;
  }


  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {

      if (formGroup.dirty) {
        this.showList = false;
      }

      const StartDate = formGroup.controls['StartDate'];
      const EndDate = formGroup.controls['EndDate'];
      const OffenceAct = formGroup.controls['OffenceAct'];

      if (OffenceAct) {
        OffenceAct.setErrors(null);
        if (!OffenceAct.value) {
          OffenceAct.setErrors({ required: true });
        }
        else if (OffenceAct.value == 0) {
          OffenceAct.setErrors({ min: true });
        }
      }
      if (StartDate) {
        StartDate.setErrors(null);
        if (!StartDate.value) {
          StartDate.setErrors({ required: true });
        }
        else if (StartDate.value && EndDate.value && EndDate.value < StartDate.value) {
          StartDate.setErrors({ exceed: true });
        }
      }
      if (EndDate) {
        EndDate.setErrors(null);
        if (!EndDate.value) {
          EndDate.setErrors({ required: true });
        }
        else if (StartDate.value && EndDate.value && EndDate.value < StartDate.value) {
          EndDate.setErrors({ exceed: true });
        }
      }      
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      OffenceAct: ['0', []],
      StartDate: ['', []],
      EndDate: ['', []],
    }, { validator: this.customValidation() }
    );
  }


  //excel genarator
  printTableExcel() {
    var workbook = new Excel.Workbook();
    // Tab Title
    var worksheet = workbook.addWorksheet(this.ReportName);
    // Columns to display
    //worksheet.mergeCells('A1', 'H1'); //title merge cell
    // console.log('this.PeruntukanList :::::::::::::::: ', this.PeruntukanList);
    // console.log('this.PeruntukanList :::::::::::::::: ', this.PeruntukanList.length);
    worksheet.mergeCells('A1', this.pageSetting.printToLetter(this.PeruntukanList.length+2)+'1'); //title merge cell
    worksheet.getCell('A1').value = this.ReportTitle; //title
    worksheet.getCell('A1').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

    worksheet.mergeCells('A2', 'A3');
    worksheet.getCell('A2').value = this.FromMenu == 'kawasan' ? 'Kawasan' : 'Bahagian / Zon / Unit';
    worksheet.getCell('A2').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };

    //worksheet.mergeCells('B2', 'H2');
    worksheet.mergeCells('B2', this.pageSetting.printToLetter(this.PeruntukanList.length+2) + '2');
    worksheet.getCell('B2').value = 'Peruntukan Undang - Undang';
    worksheet.getCell('B2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

    let columnNumber = 2;
    for (var key in this.PeruntukanList) {      
      this.pageSetting.printToLetter(columnNumber);
      worksheet.getCell(this.pageSetting.printToLetter(columnNumber)+'3').value = this.PeruntukanList[key].name;
      worksheet.getCell(this.pageSetting.printToLetter(columnNumber)+'3').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };
      columnNumber++;
    }

    worksheet.getCell(this.pageSetting.printToLetter(columnNumber)+'3').value = 'Jumlah';
    worksheet.getCell(this.pageSetting.printToLetter(columnNumber)+'3').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

/*
    worksheet.getCell('B3').value = 'Akta Pengangkutan Jalan 1987';
    worksheet.getCell('B3').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };
    worksheet.getCell('C3').value = 'Kaedah-Kaedah Lalulintas Jalan 1959';
    worksheet.getCell('C3').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };
    worksheet.getCell('D3').value = 'Kaedah-Kaedah Isyarat Lalulintas (Saiz, Warna Dan Jenis) 1959';
    worksheet.getCell('D3').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };
    worksheet.getCell('E3').value = 'Perintah Lalulintas Jalan (Peruntukan Mengenai Tempat Letak Kereta) (Ibu Kota) 1959';
    worksheet.getCell('E3').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };
    worksheet.getCell('F3').value = 'Peruntukan Lalulintas Jalan (Letak Kereta Bermeter) (Wilayah Persekutuan) 1984';
    worksheet.getCell('F3').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };
    worksheet.getCell('G3').value = 'Perintah Pengangkutan Jalan (Peruntukan Tempat Letak Kereta) Wilayah Pesekutuan Kuala Lumpur 2016';
    worksheet.getCell('G3').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };

    worksheet.getCell('H3').value = 'Jumlah';
    worksheet.getCell('H3').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    */

    /*
     worksheet.columns = [
      { key: 'Bahagian', width: 15, style: { alignment: { horizontal: 'left', vertical: 'top', wrapText: true } } },
      { key: 'Akta1', width: 15, style: { alignment: { horizontal: 'left', vertical: 'top', wrapText: true } } },
      { key: 'Akta2', width: 15, style: { alignment: { horizontal: 'left', vertical: 'top', wrapText: true } } },
      { key: 'Akta3', width: 15, style: { alignment: { horizontal: 'left', vertical: 'top', wrapText: true } } },
      { key: 'Akta4', width: 15, style: { alignment: { horizontal: 'left', vertical: 'top', wrapText: true } } },
      { key: 'Akta5', width: 15, style: { alignment: { horizontal: 'left', vertical: 'top', wrapText: true } } },
      { key: 'Akta6', width: 15, style: { alignment: { horizontal: 'left', vertical: 'top', wrapText: true } } },
      { key: 'Jumlah', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
    ];
    */

    let columsArr = []
    columsArr.push({ key: 'Bahagian', width: 30, style: { alignment: { horizontal: 'left', vertical: 'top', wrapText: true } } });   
    let keyIdx = 1;
    for (let rec in this.PeruntukanList) {
      columsArr.push({ key: 'Peruntukan'+keyIdx, width: 40, style: { alignment: { horizontal: 'left', vertical: 'top', wrapText: true } } });
      keyIdx++;
    }
    columsArr.push({ key: 'Jumlah', width: 20, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } });

    worksheet.columns = columsArr;
    
    //console.log('this.BahagianList ::::::::::::::::::: ', this.BahagianList);
    //let listFinal = [];
    for (let recBahagian in this.BahagianList) {
      //console.log('this.BahagianList[recBahagian].name ::::::::::::::::::: ', this.BahagianList[recBahagian]);
      let columsValue = [];
      //columsValue['Bahagian'] = this.BahagianList[recBahagian].name; 
      //columsValue.add({['Bahagian']: this.BahagianList[recBahagian].name});
      columsValue.push(this.BahagianList[recBahagian].name);
      //let keyIdx = 1;
      for (let recPeruntukan in this.PeruntukanList) {
        //columsValue['Peruntukan'+keyIdx] = this.bahagianPeruntukanCount[this.BahagianList[recBahagian].id + '_' + this.PeruntukanList[recPeruntukan].id]; 
        //keyIdx++
        columsValue.push(this.bahagianPeruntukanCount[this.BahagianList[recBahagian].id + '_' + this.PeruntukanList[recPeruntukan].id]);
      }
      columsValue.push(this.JumlahByBahagian[this.BahagianList[recBahagian].id]);
      //columsValue['Jumlah'] = this.JumlahByBahagian[this.BahagianList[recBahagian].id]; 

      //listFinal.push(columsValue);
      worksheet.addRow(columsValue);
    }
    let columsJumlahValue = [];
    columsJumlahValue.push('Jumlah');
    for (let recPeruntukan in this.PeruntukanList) {
      //columsValue['Peruntukan'+keyIdx] = this.bahagianPeruntukanCount[this.BahagianList[recBahagian].id + '_' + this.PeruntukanList[recPeruntukan].id]; 
      //keyIdx++
      columsJumlahValue.push(this.JumlahByPeruntukan[this.PeruntukanList[recPeruntukan].id]);
    }
    columsJumlahValue.push(this.JumlahOverall);
    worksheet.addRow(columsJumlahValue);
    
    //console.log('listFinal ::::::::::::: ', listFinal);

    // for (let recFinal of listFinal) {
    //   console.log('listFinal[record]) ::::::::::::: ', listFinal[recFinal]);
    //   worksheet.addRow([recFinal.Bahagian]);
    // }

    /*
    for (let record of this.ReportList) {
      worksheet.addRow([
        record.Bahagian,
        record.Akta1,
        record.Akta2,
        record.Akta3,
        record.Akta4,
        record.Akta5,
        record.Akta6,
        record.Jumlah,])
    }
    */

    worksheet.eachRow(function (row, _rowNumber) {
      row.eachCell(function (cell, _colNumber) {
        cell.border = {
          top: { style: 'thin' },
          left: { style: 'thin' },
          bottom: { style: 'thin' },
          right: { style: 'thin' }
        };
      });
    });

    workbook.xlsx.writeBuffer().then(data => {
      const blob = new Blob([data], { type: this.blobType });
      //create excell file
      FileSaver.saveAs(blob, this.ReportName + '.xlsx');
    });
  }



}

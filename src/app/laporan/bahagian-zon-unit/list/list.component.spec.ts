import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanBahagianZonUnitListComponent } from './list.component';

describe('LaporanBahagianZonUnitListComponent', () => {
  let component: LaporanBahagianZonUnitListComponent;
  let fixture: ComponentFixture<LaporanBahagianZonUnitListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanBahagianZonUnitListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanBahagianZonUnitListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

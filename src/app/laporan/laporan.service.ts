/* tslint:disable:max-line-length comment-format */
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { bufferTime, distinctUntilChanged, filter, map, scan, share, take, tap } from 'rxjs/operators';
import * as _ from 'lodash';
import { Pager, PageInfo } from '../../pagingnation';
import { API_BASE_URL } from 'src/shared/service-proxies/service-proxies';
import { ProgressBarState, Pair } from './models';

export enum LaporanApiEndpoint {
  //UploadImage = '/api/',
  //DeleteImage = '/api/',
}

@Injectable({
  providedIn: 'root'
})
export class LaporanService {
  /**
   * Indicate data processing. Binded in dialog
   */
  private processingSubject = new Subject<ProgressBarState>();
  private baseUrl: string;
  http: HttpClient;


  /**
   *  Pipeline of async operation to handle loading state of product dialog
   */
  isBusyStream: Observable<boolean> = this.processingSubject.asObservable()
    .pipe(
      bufferTime(300),                  // buffers the progress Observable values for 100ms period
      filter(states => states.length > 0),   // ignore empty streams
      scan((total: number, states: ProgressBarState[]) => total + _.sum(states), 0),
      map((diff: number) => diff !== 0),       // diff between count of processing and already completed http requests
      distinctUntilChanged(),
      share()                                         // Returns a new Observable that multicasts (shares) the original Observable
    );

  set isBusy(value: boolean) {
    const state: number = value ? ProgressBarState.Show : ProgressBarState.Hide;
    this.processingSubject.next(state);
  }

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }






  getLaporanUtamaUndangList(StartDate: string, EndDate: string, ActClassTypes: number, OffenceAct: number, AktaInduk: number, Peruntukan: number): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('ActClassTypes', (ActClassTypes || 0) + "")
      .set('OffenceAct', (OffenceAct || 0) + "")
      .set('AktaInduk', (AktaInduk || 0) + "")
      .set('Peruntukan', (Peruntukan || 0) + "")
      ;
    console.log('params getLaporanUtamaList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];

    list = [
      { Col1: 'Kesalahan 1', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
      { Col1: 'Kesalahan 2', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
      { Col1: 'Kesalahan 3', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
      { Col1: 'JUMLAH', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
    ];

    return of(list);
  }






  getLaporanUtamaList(StartDate: string, EndDate: string, ActClassTypes: number, OffenceAct: number, Kawasan: number, Jalan: number): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('ActClassTypes', (ActClassTypes || 0) + "")
      .set('OffenceAct', (OffenceAct || 0) + "")
      .set('Kawasan', (Kawasan || 0) + "")
      .set('Jalan', (Jalan || 0) + "")
      ;
    console.log('params getLaporanUtamaList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    if (OffenceAct == 3) {
      list = [
        { Col1: 'TRAFIK', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
        { Col1: 'AM', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
        { Col1: 'JUMLAH', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
      ];
    }
    else if (OffenceAct == 2) {
      list = [
        { Col1: 'Akta Hotel (Wilayah Persekutuan Kuala Lumpur) 2003', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
        { Col1: 'Akta (Perancangan) Wilayah Persekutuan 1982', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
        { Col1: 'Akta Hiburan (Wilayah Persekutuan Kuala Lumpur) 1992', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
        { Col1: 'JUMLAH', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
      ];
    }
    else if (OffenceAct == 1) {
      list = [
        { Col1: 'Akta Ibu Kota Persekutuan 1960', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
        { Col1: 'Akta Jalan, Parit & Bangunan 1974', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
        { Col1: 'Akta Pengangkutan Jalan 1987', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
        { Col1: 'JUMLAH', Col2: 2, Col3: 3, Col4: 1, Col5: 2, Col6: 2, Col7: 9, Col8: 9, Col9: 9, Col10: 7, Col11: 11, Col12: 1, Col13: 3, Col14: 3, Col15: 3, Col16: 3 },
      ];
    }
    return of(list);
  }





  getLaporanBahagianZonUnitList(StartDate: string, EndDate: string, ActClassTypes: number): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('KelasAkta', (ActClassTypes || 0) + "")
      ;
    console.log('params getLaporanBahagianZonUnitList ::::::::::::::::: ', params);
    //console.log('params getHandheldOfficerList ::::::::::::::::: ', params);
    // console.log('params StartDate ::::::::::::::::: ', StartDate);
    // console.log('params EndDate ::::::::::::::::: ', EndDate);
    // console.log('params NoGaji ::::::::::::::::: ', NoGaji);
    // console.log('params ReportTypes ::::::::::::::::: ', ReportTypes);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];

    if (ActClassTypes == 1) {
      list = [
        { Bahagian: 1, Peruntukan: 1, Jumlah: 12 },
        { Bahagian: 1, Peruntukan: 2, Jumlah: 34 },
        { Bahagian: 1, Peruntukan: 3, Jumlah: 56 },
        { Bahagian: 1, Peruntukan: 4, Jumlah: 2 },
        { Bahagian: 2, Peruntukan: 1, Jumlah: 34 },
        { Bahagian: 2, Peruntukan: 2, Jumlah: 12 },
        { Bahagian: 2, Peruntukan: 3, Jumlah: 33 },
        { Bahagian: 2, Peruntukan: 4, Jumlah: 71 },
        { Bahagian: 3, Peruntukan: 1, Jumlah: 1 },
        { Bahagian: 3, Peruntukan: 2, Jumlah: 8 },
        { Bahagian: 3, Peruntukan: 3, Jumlah: 9 },
        { Bahagian: 3, Peruntukan: 4, Jumlah: 23 },
        { Bahagian: 4, Peruntukan: 1, Jumlah: 27 },
        { Bahagian: 4, Peruntukan: 2, Jumlah: 50 },
        { Bahagian: 4, Peruntukan: 3, Jumlah: 41 },
        { Bahagian: 4, Peruntukan: 4, Jumlah: 7 },
      ];
    }
    else if (ActClassTypes == 2) {
      list = [
        { Bahagian: 1, Peruntukan: 5, Jumlah: 21 },
        { Bahagian: 1, Peruntukan: 6, Jumlah: 3 },
        { Bahagian: 1, Peruntukan: 7, Jumlah: 6 },
        { Bahagian: 1, Peruntukan: 8, Jumlah: 12 },
        { Bahagian: 1, Peruntukan: 9, Jumlah: 12 },
        { Bahagian: 1, Peruntukan: 10, Jumlah: 34 },
        { Bahagian: 1, Peruntukan: 11, Jumlah: 56 },
        { Bahagian: 1, Peruntukan: 12, Jumlah: 2 },
        { Bahagian: 2, Peruntukan: 5, Jumlah: 92 },
        { Bahagian: 2, Peruntukan: 6, Jumlah: 54 },
        { Bahagian: 2, Peruntukan: 7, Jumlah: 76 },
        { Bahagian: 2, Peruntukan: 8, Jumlah: 21 },
        { Bahagian: 2, Peruntukan: 9, Jumlah: 122 },
        { Bahagian: 2, Peruntukan: 10, Jumlah: 3 },
        { Bahagian: 2, Peruntukan: 11, Jumlah: 96 },
        { Bahagian: 2, Peruntukan: 12, Jumlah: 59 },
        { Bahagian: 3, Peruntukan: 5, Jumlah: 19 },
        { Bahagian: 3, Peruntukan: 6, Jumlah: 10 },
        { Bahagian: 3, Peruntukan: 7, Jumlah: 9 },
        { Bahagian: 3, Peruntukan: 8, Jumlah: 8 },
        { Bahagian: 3, Peruntukan: 9, Jumlah: 32 },
        { Bahagian: 3, Peruntukan: 10, Jumlah: 34 },
        { Bahagian: 3, Peruntukan: 11, Jumlah: 66 },
        { Bahagian: 3, Peruntukan: 12, Jumlah: 26 },
        { Bahagian: 4, Peruntukan: 5, Jumlah: 2 },
        { Bahagian: 4, Peruntukan: 6, Jumlah: 14 },
        { Bahagian: 4, Peruntukan: 7, Jumlah: 56 },
        { Bahagian: 4, Peruntukan: 8, Jumlah: 29 },
        { Bahagian: 4, Peruntukan: 9, Jumlah: 9 },
        { Bahagian: 4, Peruntukan: 10, Jumlah: 134 },
        { Bahagian: 4, Peruntukan: 11, Jumlah: 56 },
        { Bahagian: 4, Peruntukan: 12, Jumlah: 22 },
      ];

      // list = [
      //   { Bahagian: 'Zon Utara', Akta1: 20,  Akta2: 10,  Akta3: 20,  Akta4: 30,  Akta5: 10, Akta6: 10, Jumlah: 100},
      //   { Bahagian: 'Zon Tengah', Akta1: 50,  Akta2: 20,  Akta3: 10,  Akta4: 10,  Akta5: 50, Akta6: 20, Jumlah: 160},
      //   { Bahagian: 'JUMLAH', Akta1: 70,  Akta2: 30,  Akta3: 30,  Akta4: 40,  Akta5: 60, Akta6: 30, Jumlah: 260},

      // ];
    }
    return of(list);
  }


  getLaporanKawasanList(StartDate: string, EndDate: string, ActClassTypes: number): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('KelasAkta', (ActClassTypes || 0) + "")
      ;
    console.log('params getLaporanBahagianZonUnitList ::::::::::::::::: ', params);
    //console.log('params getHandheldOfficerList ::::::::::::::::: ', params);
    // console.log('params StartDate ::::::::::::::::: ', StartDate);
    // console.log('params EndDate ::::::::::::::::: ', EndDate);
    // console.log('params NoGaji ::::::::::::::::: ', NoGaji);
    // console.log('params ReportTypes ::::::::::::::::: ', ReportTypes);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];

    if (ActClassTypes == 1) {
      list = [
        { Bahagian: 1, Peruntukan: 1, Jumlah: 6 },
        { Bahagian: 1, Peruntukan: 2, Jumlah: 7 },
        { Bahagian: 1, Peruntukan: 3, Jumlah: 1 },
        { Bahagian: 1, Peruntukan: 4, Jumlah: 2 },
        { Bahagian: 2, Peruntukan: 1, Jumlah: 2 },
        { Bahagian: 2, Peruntukan: 2, Jumlah: 6 },
        { Bahagian: 2, Peruntukan: 3, Jumlah: 9 },
        { Bahagian: 2, Peruntukan: 4, Jumlah: 12 },
        { Bahagian: 3, Peruntukan: 1, Jumlah: 15 },
        { Bahagian: 3, Peruntukan: 2, Jumlah: 8 },
        { Bahagian: 3, Peruntukan: 3, Jumlah: 19 },
        { Bahagian: 3, Peruntukan: 4, Jumlah: 8 },
        { Bahagian: 4, Peruntukan: 1, Jumlah: 2 },
        { Bahagian: 4, Peruntukan: 2, Jumlah: 7 },
        { Bahagian: 4, Peruntukan: 3, Jumlah: 15 },
        { Bahagian: 4, Peruntukan: 4, Jumlah: 17 },
      ];
    }
    else if (ActClassTypes == 2) {
      list = [
        { Bahagian: 1, Peruntukan: 5, Jumlah: 21 },
        { Bahagian: 1, Peruntukan: 6, Jumlah: 3 },
        { Bahagian: 1, Peruntukan: 7, Jumlah: 6 },
        { Bahagian: 1, Peruntukan: 8, Jumlah: 16 },
        { Bahagian: 1, Peruntukan: 9, Jumlah: 12 },
        { Bahagian: 1, Peruntukan: 10, Jumlah: 2 },
        { Bahagian: 1, Peruntukan: 11, Jumlah: 3 },
        { Bahagian: 1, Peruntukan: 12, Jumlah: 2 },
        { Bahagian: 2, Peruntukan: 5, Jumlah: 70 },
        { Bahagian: 2, Peruntukan: 6, Jumlah: 6 },
        { Bahagian: 2, Peruntukan: 7, Jumlah: 11 },
        { Bahagian: 2, Peruntukan: 8, Jumlah: 21 },
        { Bahagian: 2, Peruntukan: 9, Jumlah: 56 },
        { Bahagian: 2, Peruntukan: 10, Jumlah: 3 },
        { Bahagian: 2, Peruntukan: 11, Jumlah: 1 },
        { Bahagian: 2, Peruntukan: 12, Jumlah: 7 },
        { Bahagian: 3, Peruntukan: 5, Jumlah: 2 },
        { Bahagian: 3, Peruntukan: 6, Jumlah: 12 },
        { Bahagian: 3, Peruntukan: 7, Jumlah: 9 },
        { Bahagian: 3, Peruntukan: 8, Jumlah: 8 },
        { Bahagian: 3, Peruntukan: 9, Jumlah: 4 },
        { Bahagian: 3, Peruntukan: 10, Jumlah: 27 },
        { Bahagian: 3, Peruntukan: 11, Jumlah: 12 },
        { Bahagian: 3, Peruntukan: 12, Jumlah: 31 },
        { Bahagian: 4, Peruntukan: 5, Jumlah: 2 },
        { Bahagian: 4, Peruntukan: 6, Jumlah: 1 },
        { Bahagian: 4, Peruntukan: 7, Jumlah: 21 },
        { Bahagian: 4, Peruntukan: 8, Jumlah: 12 },
        { Bahagian: 4, Peruntukan: 9, Jumlah: 19 },
        { Bahagian: 4, Peruntukan: 10, Jumlah: 8 },
        { Bahagian: 4, Peruntukan: 11, Jumlah: 2 },
        { Bahagian: 4, Peruntukan: 12, Jumlah: 1 },
      ];

      // list = [
      //   { Bahagian: 'Zon Utara', Akta1: 20,  Akta2: 10,  Akta3: 20,  Akta4: 30,  Akta5: 10, Akta6: 10, Jumlah: 100},
      //   { Bahagian: 'Zon Tengah', Akta1: 50,  Akta2: 20,  Akta3: 10,  Akta4: 10,  Akta5: 50, Akta6: 20, Jumlah: 160},
      //   { Bahagian: 'JUMLAH', Akta1: 70,  Akta2: 30,  Akta3: 30,  Akta4: 40,  Akta5: 60, Akta6: 30, Jumlah: 260},

      // ];
    }
    return of(list);
  }


  getLaporanIndividuPenguatkuasaList(page: Pager, StartDate: string, EndDate: string, Kawasan: number, Jalan: number, IdPenguatkuasa: number): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      .set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('Kawasan', String(Kawasan) || "")
      .set('Jalan', String(Jalan) || "")
      .set('IdPenguatkuasa', String(IdPenguatkuasa) || "")
      ;
    console.log('params getLaporanIndividuPenguatkuasaList ::::::::::::::::: ', params);
    // console.log('params StartDate ::::::::::::::::: ', StartDate);
    // console.log('params EndDate ::::::::::::::::: ', EndDate);
    // console.log('params NoGaji ::::::::::::::::: ', NoGaji);
    // console.log('params ReportTypes ::::::::::::::::: ', ReportTypes);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];
    // if(StartDate && EndDate && ReportTypes == 1)
    // {
    //   list = [
    //     { Id: 1, IdPengguna: '1234', Nama: 'Razman B. Md Zainal', JumlahNotis: 123},
    //     { Id: 2, IdPengguna: '6678', Nama: 'Muhd Naif', JumlahNotis: 21},
    //     { Id: 3, IdPengguna: '3313', Nama: 'Mohd Shakir Bin Sukri', JumlahNotis: 9873},
    //   ];
    // }
    // else if(StartDate && EndDate && ReportTypes == 2)
    // {
    //   list = [
    //     { Id: 1, IdPengguna: '1234', Nama: 'Razman B. Md Zainal', JumlahNotis: 123},
    //     { Id: 2, IdPengguna: '6678', Nama: 'Muhd Naif', JumlahNotis: 21},
    //     { Id: 3, IdPengguna: '3313', Nama: 'Mohd Shakir Bin Sukri', JumlahNotis: 9873},
    //     { Id: 4, IdPengguna: '9988', Nama: 'Razman B. Md Zainal 2', JumlahNotis: 123},
    //     { Id: 5, IdPengguna: '5322', Nama: 'Muhd Naif 2', JumlahNotis: 21},
    //     { Id: 6, IdPengguna: '9871', Nama: 'Mohd Shakir Bin Sukri 2', JumlahNotis: 9873},
    //   ];
    // }
    // else if(StartDate && EndDate && NoGaji && ReportTypes == 3)
    // {
    //   list = [
    //     { Id: 1, NoNotis: 'CH710000000', Jenis: 'JPK - TRAFIK', TarikhKesalahan: '01/01/2020 4:57:32 PM', TarikhWujud: '01/01/2020 4:57:32 PM', NamaPesalah: 'Ahmad Sukri Kadir', NoKenderaan: 'WWW9999', NoCukaiJalan: '38789798', Peruntukan: 'Akta Penangkutan Jalan 1987', Seksyen: 'SEK 79(2)', Tempat: 'Jalan Tengah', NoPetak: '-' },
    //     { Id: 2, NoNotis: 'CH710000001', Jenis: 'JPK - TRAFIK', TarikhKesalahan: '03/01/2020 4:57:32 PM', TarikhWujud: '01/01/2020 4:57:32 PM', NamaPesalah: 'Razman B. Md Zainal', NoKenderaan: 'WWW0001', NoCukaiJalan: '88317736', Peruntukan: 'Akta Penangkutan Jalan 1987', Seksyen: 'SEK 79(2)', Tempat: 'Jalan Tengah', NoPetak: '-' },
    //   ];
    // }

    list = [
      { Id: 1, NoNotis: 'CH710000000', Jenis: 'JPK - TRAFIK', TarikhKesalahan: '01/01/2020 4:57:32 PM', NamaPesalah: 'Ahmad Sukri Kadir', NoKenderaan: 'WWW9999', NoCukaiJalan: '38789798', Peruntukan: 'Akta Penangkutan Jalan 1987', Seksyen: 'SEK 79(2)', Tempat: 'Jalan Tengah', NoPetak: '1' },
      { Id: 2, NoNotis: 'CH710000001', Jenis: 'JPK - TRAFIK', TarikhKesalahan: '03/01/2020 4:57:32 PM', NamaPesalah: 'Razman B. Md Zainal', NoKenderaan: 'WWW0001', NoCukaiJalan: '88317736', Peruntukan: 'Akta Penangkutan Jalan 1987', Seksyen: 'SEK 79(2)', Tempat: 'Jalan Tengah', NoPetak: '-' },
    ];

    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }




  getLaporanNotisTanpaAlamatList(page: Pager,
    //  StartDate: string, EndDate: string, NoGaji: string, 
    // JenisDokumen: number, KumpulanPengguna: number, ReportTypes: number
    Carian: any
  ): Observable<any> {
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      .set('StartDate', Carian.StartDate || "")
      .set('EndDate', Carian.EndDate || "")
      .set('JenisNotis', String(Carian.JenisNotis) || "")
      .set('JenisDokumen', String(Carian.JenisDokumen) || "")
      .set('StatusNotis', String(Carian.StatusNotis) || "")
      .set('Hari', String(Carian.Hari) || "")
      ;
    console.log('params getLaporanNotisTanpaAlamatList ::::::::::::::::: ', params);
    // console.log('params StartDate ::::::::::::::::: ', StartDate);
    // console.log('params EndDate ::::::::::::::::: ', EndDate);
    // console.log('params NoGaji ::::::::::::::::: ', NoGaji);
    // console.log('params ReportTypes ::::::::::::::::: ', ReportTypes);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];

    list = [
      { Id: 1, NoNotis: 'DC0001', TarikhKesalahan: '25/01/2020 03:27 PM', Peruntukan: 'Peruntukan Undang - Undang 1', Seksyen: '12', NoKenderaan: 'WWW9999', Kompaun: '300.00', Sebab: 'Catatan Tiada Alamat 1' },
      { Id: 2, NoNotis: 'DC0002', TarikhKesalahan: '15/01/2020 03:27 PM', Peruntukan: 'Peruntukan Undang - Undang 2', Seksyen: '7', NoKenderaan: 'KKK0001', Kompaun: '100.00', Sebab: 'Catatan Tiada Alamat 2' },

    ];


    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }

  getLaporanSamanTanpaAlamatList(page: Pager,
    //  StartDate: string, EndDate: string, NoGaji: string, 
    // JenisDokumen: number, KumpulanPengguna: number, ReportTypes: number
    Carian: any
  ): Observable<any> {
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      .set('StartDate', Carian.StartDate || "")
      .set('EndDate', Carian.EndDate || "")
      // .set('JenisNotis', String(Carian.JenisNotis) || "")
      // .set('JenisDokumen', String(Carian.JenisDokumen) || "")
      // .set('StatusNotis', String(Carian.StatusNotis) || "")
      .set('Hari', String(Carian.Hari) || "")
      ;
    console.log('params getLaporanSamanTanpaAlamatList ::::::::::::::::: ', params);
    // console.log('params StartDate ::::::::::::::::: ', StartDate);
    // console.log('params EndDate ::::::::::::::::: ', EndDate);
    // console.log('params NoGaji ::::::::::::::::: ', NoGaji);
    // console.log('params ReportTypes ::::::::::::::::: ', ReportTypes);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];

    list = [
      { Id: 1, NoNotis: 'DC0001', TarikhKesalahan: '25/01/2020 03:27 PM', NamaKesalahan: 'Kesalahan 1', Seksyen: '12', NamaPesalah: 'Razman B. Md Zainal', NoPesalah: '840819075111', NoKenderaan: 'WWW9999', Kompaun: '300.00' },
      { Id: 2, NoNotis: 'DC0002', TarikhKesalahan: '15/01/2020 03:27 PM', NamaKesalahan: 'Kesalahan 2', Seksyen: '7', NamaPesalah: 'Muhd Naiff', NoPesalah: '890819075111', NoKenderaan: 'KKK0001', Kompaun: '100.00' },

    ];


    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }


  getLaporanBayarNotisBelumWujudList(page: Pager,
    //  StartDate: string, EndDate: string, NoGaji: string, 
    // JenisDokumen: number, KumpulanPengguna: number, ReportTypes: number
    Carian: any
  ): Observable<any> {
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      .set('StartDate', Carian.StartDate || "")
      .set('EndDate', Carian.EndDate || "")
      // .set('JenisNotis', String(Carian.JenisNotis) || "")
      // .set('JenisDokumen', String(Carian.JenisDokumen) || "")
      // .set('StatusNotis', String(Carian.StatusNotis) || "")
      .set('NoNotis', String(Carian.NoNotis) || "")
      .set('NoResit', String(Carian.NoResit) || "")
      ;
    console.log('params getLaporanBayarNotisBelumWujudList ::::::::::::::::: ', params);
    // console.log('params StartDate ::::::::::::::::: ', StartDate);
    // console.log('params EndDate ::::::::::::::::: ', EndDate);
    // console.log('params NoGaji ::::::::::::::::: ', NoGaji);
    // console.log('params ReportTypes ::::::::::::::::: ', ReportTypes);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];

    list = [
      { Id: 1, NoNotis: 'DC0001', TarikhBayaran: '25/01/2020', Bayaran: '300.00', Lokasi: 'Lokasi Bayar 1', NoResit: '1111' },
      { Id: 2, NoNotis: 'DC0002', TarikhBayaran: '20/01/2020', Bayaran: '100.00', Lokasi: 'Lokasi Bayar 2', NoResit: '2222' },
      { Id: 3, NoNotis: 'DC0003', TarikhBayaran: '19/01/2020', Bayaran: '100.00', Lokasi: 'Lokasi Bayar 3', NoResit: '3333' },

    ];


    let pageInfo: any = {
      RowCount: list.length,
      Total: '500'
    }
    return of({ list, pageInfo });
  }

  getLaporanUpliftList(page: Pager,
    //  StartDate: string, EndDate: string, NoGaji: string, 
    // JenisDokumen: number, KumpulanPengguna: number, ReportTypes: number
    Carian: any
  ): Observable<any> {
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      .set('StartDate', Carian.StartDate || "")
      .set('EndDate', Carian.EndDate || "")
      // .set('JenisNotis', String(Carian.JenisNotis) || "")
      // .set('JenisDokumen', String(Carian.JenisDokumen) || "")
      // .set('StatusNotis', String(Carian.StatusNotis) || "")
      // .set('NoNotis', String(Carian.NoNotis) || "")
      // .set('NoResit', String(Carian.NoResit) || "")
      ;
    console.log('params getLaporanUpliftList ::::::::::::::::: ', params);
    // console.log('params StartDate ::::::::::::::::: ', StartDate);
    // console.log('params EndDate ::::::::::::::::: ', EndDate);
    // console.log('params NoGaji ::::::::::::::::: ', NoGaji);
    // console.log('params ReportTypes ::::::::::::::::: ', ReportTypes);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];

    list = [
      { Tarikh: '01/01/2020', Jumlah: 20,  Berjaya: 10,  Gagal: 10 },
      { Tarikh: '02/01/2020', Jumlah: 20,  Berjaya: 10,  Gagal: 10 },
      { Tarikh: '03/01/2020', Jumlah: 20,  Berjaya: 10,  Gagal: 10 },

    ];


    let pageInfo: any = {
      RowCount: list.length,
      TotalBerjaya: 30,
      TotalGagal: 30
    }
    return of({ list, pageInfo });
  }

  getLaporanPembatalanNotisList(page: Pager, StartDate: string, EndDate: string, NoGaji: string, Kumpulan: number, KumpulanPengguna: number, ReportTypes: number): Observable<any> {
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      .set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('Kumpulan', String(Kumpulan) || "")
      .set('KumpulanPengguna', String(KumpulanPengguna) || "")
      .set('NoGaji', String(NoGaji) || "")
      ;
    //console.log('params getHandheldOfficerList ::::::::::::::::: ', params);
    // console.log('params StartDate ::::::::::::::::: ', StartDate);
    // console.log('params EndDate ::::::::::::::::: ', EndDate);
    // console.log('params NoGaji ::::::::::::::::: ', NoGaji);
    // console.log('params ReportTypes ::::::::::::::::: ', ReportTypes);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];
    if (StartDate && EndDate && ReportTypes == 1) {
      list = [
        { KumpulanPengguna: 'Kumpulan Pengguna 1', Jumlah: 123 },
        { KumpulanPengguna: 'Kumpulan Pengguna 2', Jumlah: 43 },
        { KumpulanPengguna: 'Kumpulan Pengguna 3', Jumlah: 87 },
        { KumpulanPengguna: 'Kumpulan Pengguna 4', Jumlah: 8 },
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 2) {
      list = [
        { NoGaji: '1234', Nama: 'Razman B. Md Zainal', Jumlah: 123 },
        { NoGaji: '6678', Nama: 'Muhd Naif', Jumlah: 21 },
        { NoGaji: '3313', Nama: 'Mohd Shakir Bin Sukri', Jumlah: 9873 },
        { NoGaji: '9988', Nama: 'Razman B. Md Zainal 2', Jumlah: 123 },
        { NoGaji: '5322', Nama: 'Muhd Naif 2', Jumlah: 21 },
        { NoGaji: '9871', Nama: 'Mohd Shakir Bin Sukri 2', Jumlah: 9873 },
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 3) {
      list = [
        { Sebab: 'Sebab 1', Jumlah: 123 },
        { Sebab: 'Sebab 2', Jumlah: 43 },
        { Sebab: 'Sebab 3', Jumlah: 87 },
        { Sebab: 'Sebab 4', Jumlah: 8 },
        { Sebab: 'Sebab 5', Jumlah: 21 },
        { Sebab: 'Sebab 6', Jumlah: 67 },
        { Sebab: 'Sebab 7', Jumlah: 89 },
        { Sebab: 'Sebab 8', Jumlah: 28 },
      ];
    }

    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }

  

  getLaporanJenisKenderaanList(page: Pager, StartDate: string, EndDate: string, Carian: any): Observable<any> {

    //JenisBadan: number, OffenceAct: number,  ReportTypes: number
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      .set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('JenisBadan', (Carian.JenisBadan || 0) + "")
      .set('Jenis', (Carian.Jenis || 0) + "")
      
      ;
    //console.log('params getHandheldOfficerList ::::::::::::::::: ', params);
    // console.log('params StartDate ::::::::::::::::: ', StartDate);
    // console.log('params EndDate ::::::::::::::::: ', EndDate);
    // console.log('params NoGaji ::::::::::::::::: ', NoGaji);
    // console.log('params ReportTypes ::::::::::::::::: ', ReportTypes);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];
    /*
    if (StartDate && EndDate && ReportTypes == 1) {
      list = [
        { Id: 1, IdPengguna: '1234', Nama: 'Razman B. Md Zainal', JumlahNotis: 123 },
        { Id: 2, IdPengguna: '6678', Nama: 'Muhd Naif', JumlahNotis: 21 },
        { Id: 3, IdPengguna: '3313', Nama: 'Mohd Shakir Bin Sukri', JumlahNotis: 9873 },
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 2) {
      list = [
        { Id: 1, IdPengguna: '1234', Nama: 'Razman B. Md Zainal', JumlahNotis: 123 },
        { Id: 2, IdPengguna: '6678', Nama: 'Muhd Naif', JumlahNotis: 21 },
        { Id: 3, IdPengguna: '3313', Nama: 'Mohd Shakir Bin Sukri', JumlahNotis: 9873 },
        { Id: 4, IdPengguna: '9988', Nama: 'Razman B. Md Zainal 2', JumlahNotis: 123 },
        { Id: 5, IdPengguna: '5322', Nama: 'Muhd Naif 2', JumlahNotis: 21 },
        { Id: 6, IdPengguna: '9871', Nama: 'Mohd Shakir Bin Sukri 2', JumlahNotis: 9873 },
      ];
    }
    else if (StartDate && EndDate && NoGaji && ReportTypes == 3) {
      list = [
        { Id: 1, NoNotis: 'CH710000000', Jenis: 'JPK - TRAFIK', TarikhKesalahan: '01/01/2020 4:57:32 PM', TarikhWujud: '01/01/2020 4:57:32 PM', NamaPesalah: 'Ahmad Sukri Kadir', NoKenderaan: 'WWW9999', NoCukaiJalan: '38789798', Peruntukan: 'Akta Penangkutan Jalan 1987', Seksyen: 'SEK 79(2)', Tempat: 'Jalan Tengah', NoPetak: '-' },
        { Id: 2, NoNotis: 'CH710000001', Jenis: 'JPK - TRAFIK', TarikhKesalahan: '03/01/2020 4:57:32 PM', TarikhWujud: '01/01/2020 4:57:32 PM', NamaPesalah: 'Razman B. Md Zainal', NoKenderaan: 'WWW0001', NoCukaiJalan: '88317736', Peruntukan: 'Akta Penangkutan Jalan 1987', Seksyen: 'SEK 79(2)', Tempat: 'Jalan Tengah', NoPetak: '-' },
      ];
    }
    */

   list = [
    { NoNotis: 'DC0001', NoKenderaan: 'WWW9999', NamaPesalah: 'Razman B. Md Zainal',  NamaPemandu: 'Razman B. Md Zainal', KpPemandu: '840819075337',  TarikhKesalahan: '25/01/2020',  Lokasi: 'Jalan Duta', Kompaun: '300.00', NoResit: '78173', TarikhBayaran: '25/01/2020', Bayaran: '300.00', JenisKenderaan: 'Jenis 1' },
    { NoNotis: 'DC0002', NoKenderaan: 'JKP9981', NamaPesalah: 'Mohd Shakir Bin Sukri',  NamaPemandu: 'Nik Ahmad Fahmi', KpPemandu: '880819075337',  TarikhKesalahan: '25/07/2020',  Lokasi: 'Jalan Tun Razak', Kompaun: '300.00', NoResit: '87317', TarikhBayaran: '25/09/2020', Bayaran: '300.00', JenisKenderaan: 'Jenis 2' },


  ];

    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }

  getLaporanDdeList(page: Pager, StartDate: string, EndDate: string, NoGaji: string, ReportTypes: number): Observable<any> {
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    //console.log('params getHandheldOfficerList ::::::::::::::::: ', params);
    // console.log('params StartDate ::::::::::::::::: ', StartDate);
    // console.log('params EndDate ::::::::::::::::: ', EndDate);
    // console.log('params NoGaji ::::::::::::::::: ', NoGaji);
    // console.log('params ReportTypes ::::::::::::::::: ', ReportTypes);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];
    if (StartDate && EndDate && ReportTypes == 1) {
      list = [
        { Id: 1, IdPengguna: '1234', Nama: 'Razman B. Md Zainal', JumlahNotis: 123 },
        { Id: 2, IdPengguna: '6678', Nama: 'Muhd Naif', JumlahNotis: 21 },
        { Id: 3, IdPengguna: '3313', Nama: 'Mohd Shakir Bin Sukri', JumlahNotis: 9873 },
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 2) {
      list = [
        { Id: 1, IdPengguna: '1234', Nama: 'Razman B. Md Zainal', JumlahNotis: 123 },
        { Id: 2, IdPengguna: '6678', Nama: 'Muhd Naif', JumlahNotis: 21 },
        { Id: 3, IdPengguna: '3313', Nama: 'Mohd Shakir Bin Sukri', JumlahNotis: 9873 },
        { Id: 4, IdPengguna: '9988', Nama: 'Razman B. Md Zainal 2', JumlahNotis: 123 },
        { Id: 5, IdPengguna: '5322', Nama: 'Muhd Naif 2', JumlahNotis: 21 },
        { Id: 6, IdPengguna: '9871', Nama: 'Mohd Shakir Bin Sukri 2', JumlahNotis: 9873 },
      ];
    }
    else if (StartDate && EndDate && NoGaji && ReportTypes == 3) {
      list = [
        { Id: 1, NoNotis: 'CH710000000', Jenis: 'JPK - TRAFIK', TarikhKesalahan: '01/01/2020 4:57:32 PM', TarikhWujud: '01/01/2020 4:57:32 PM', NamaPesalah: 'Ahmad Sukri Kadir', NoKenderaan: 'WWW9999', NoCukaiJalan: '38789798', Peruntukan: 'Akta Penangkutan Jalan 1987', Seksyen: 'SEK 79(2)', Tempat: 'Jalan Tengah', NoPetak: '-' },
        { Id: 2, NoNotis: 'CH710000001', Jenis: 'JPK - TRAFIK', TarikhKesalahan: '03/01/2020 4:57:32 PM', TarikhWujud: '01/01/2020 4:57:32 PM', NamaPesalah: 'Razman B. Md Zainal', NoKenderaan: 'WWW0001', NoCukaiJalan: '88317736', Peruntukan: 'Akta Penangkutan Jalan 1987', Seksyen: 'SEK 79(2)', Tempat: 'Jalan Tengah', NoPetak: '-' },
      ];
    }

    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }

  

  getLaporanSenaraiHitamList(StartDate: string, EndDate: string,  ReportTypes: number): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('ReportTypes', (ReportTypes || 0) + "")
      ;
    console.log('params getLaporanSenaraiHitamList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    if (StartDate && EndDate && ReportTypes == 1) {
      list = [
        { JenisLaporanCounter: '1', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '2', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '3', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '4', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '5', Jumlah: 10, Berjaya: 5, Gagal: 5 }, 
        { JenisLaporanCounter: '6', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '7', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '8', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '9', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '10', Jumlah: 10, Berjaya: 5, Gagal: 5 },  
        { JenisLaporanCounter: '11', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '12', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '13', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '14', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '15', Jumlah: 10, Berjaya: 5, Gagal: 5 }, 
        { JenisLaporanCounter: '16', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '17', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '18', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '19', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '20', Jumlah: 10, Berjaya: 5, Gagal: 5 },  
        { JenisLaporanCounter: '21', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '22', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '23', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '24', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '25', Jumlah: 10, Berjaya: 5, Gagal: 5 }, 
        { JenisLaporanCounter: '26', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '27', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '28', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '29', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '30', Jumlah: 10, Berjaya: 5, Gagal: 5 },  
        { JenisLaporanCounter: 'JUMLAH', Jumlah: 300,  Berjaya: 150, Gagal: 150 },        
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 2) {
      list = [
        { JenisLaporanCounter: '1', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '2', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '3', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: '4', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'JUMLAH', Jumlah: 40,  Berjaya: 20, Gagal: 20 },    
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 3) {
      list = [
        { JenisLaporanCounter: 'Januari', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'Februari', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'Mac', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'April', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'Mei', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'Jun', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'Julai', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'Ogos', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'September', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'Oktober', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'November', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'Disember', Jumlah: 10, Berjaya: 5, Gagal: 5 },
        { JenisLaporanCounter: 'JUMLAH', Jumlah: 120, Berjaya: 60, Gagal: 60 },   
      ];
    }
    return of(list);
  }

  getTindakanOfficerList(page: Pager, StartDate: string, EndDate: string, 
    Carian: any
    ): Observable<any> {
    //console.log('getTindakanOfficerList Carian ::::::::::::::::: ', Carian);
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      .set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('NoGaji', Carian.NoGaji || "")
      .set('ReportType', String(Carian.LaporanStatistikTindakanType) || "")
      ;
    console.log('params getHandheldOfficerList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    let jumlah = 0;
    if (Carian.LaporanStatistikTindakanType == 1) {
      list = [
        {
          Id:'1', NoGaji: '123', Nama: 'Razman B. Md Zainal', Pangkat: 'Koperal', NoBadan: '456', Bahagian: 'Bahagian 1',
          s1: 5, s2: 0, s3: 5, s4: 0, s5: 0, s6: 10, s7: 0, s8: 0, s9: 0, s10: 0,
          s11: 10, s12: 0, s13: 10, s14: 5, s15: 0, s16: 5, s17: 10, s18: 10, s19: 0, s20: 0,
          s21: 0, s22: 10, s23: 0, s24: 0, s25: 5, s26: 10, s27: 5, s28: 0, s29: 0, s30: 0,
          s31: 0, Jumlah: 100
        },
        {
          Id:'2', NoGaji: '567', Nama: 'Nik Fahmi Bin Ahmad', Pangkat: 'Koperal', NoBadan: '890', Bahagian: 'Bahagian 2',
          s1: 5, s2: 10, s3: 5, s4: 5, s5: 0, s6: 10, s7: 0, s8: 0, s9: 0, s10: 10,
          s11: 10, s12: 0, s13: 10, s14: 5, s15: 5, s16: 5, s17: 10, s18: 10, s19: 0, s20: 0,
          s21: 0, s22: 10, s23: 10, s24: 10, s25: 5, s26: 10, s27: 5, s28: 0, s29: 0, s30: 0,
          s31: 0, Jumlah: 150
        },
      ];
      jumlah = 250;
    }
    else if (Carian.LaporanStatistikTindakanType == 2) {
      list = [
        {
          Id:'1', NoGaji: '123', Nama: 'Razman B. Md Zainal', Pangkat: 'Koperal', NoBadan: '456', Bahagian: 'Bahagian 1',
          s1: 5, s2: 0, s3: 5, s4: 0, s5: 0, s6: 10, s7: 0, s8: 0, s9: 0, s10: 0,
          s11: 10, s12: 0, Jumlah: 30
        },
        {
          Id:'2', NoGaji: '567', Nama: 'Nik Fahmi Bin Ahmad', Pangkat: 'Koperal', NoBadan: '890', Bahagian: 'Bahagian 2',
          s1: 5, s2: 10, s3: 5, s4: 5, s5: 0, s6: 10, s7: 0, s8: 0, s9: 0, s10: 10,
          s11: 10, s12: 0, Jumlah: 45
        },
      ];
      jumlah = 75;
    }
    else if (Carian.LaporanStatistikTindakanType == 3) {
      list = [
        {
          Id:'1', NoGaji: '123', Nama: 'Razman B. Md Zainal', Pangkat: 'Koperal', NoBadan: '456', Bahagian: 'Bahagian 1',
          s1: 5, s2: 10, s3: 5, s4: 0, s5: 5, s6: 10, s7: 0, s8: 0, s9: 0, s10: 5,
          s11: 10, s12: 0, Jumlah: 50
        },
        {
          Id:'2', NoGaji: '567', Nama: 'Nik Fahmi Bin Ahmad', Pangkat: 'Koperal', NoBadan: '890', Bahagian: 'Bahagian 2',
          s1: 5, s2: 10, s3: 5, s4: 5, s5: 0, s6: 10, s7: 10, s8: 10, s9: 5, s10: 10,
          s11: 10, s12: 0, Jumlah: 70
        },
      ];
      jumlah = 120;
    }

  
    let pageInfo: any = { RowCount: list.length, JumlahTindakan: jumlah }
    return of({ list, pageInfo });
  }


  getStatistikTindakanMengikutTempatList(StartDate: string, EndDate: string, 
    Carian: any
    ): Observable<any> {
    //console.log('getTindakanOfficerList Carian ::::::::::::::::: ', Carian);
    const params = new HttpParams().set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('Parlimen', Carian.Parlimen || "")
      .set('Kawasan', Carian.Kawasan || "")
      .set('Lokasi', Carian.Lokasi || "")
      .set('ReportType', String(Carian.LaporanStatistikTindakanType) || "")
      ;
    console.log('params getStatistikTindakanMengikutTempatList ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    let jumlah = 0;
    if (Carian.LaporanStatistikTindakanType == 4) {
      list = [
        {
          Tempat: 'PARLIMEN 1',
          s1: 5, s2: 0, s3: 5, s4: 0, s5: 0, s6: 10, s7: 0, s8: 0, s9: 0, s10: 0,
          s11: 10, s12: 0, Jumlah: 30
        },
      ];
    }
    else if (Carian.LaporanStatistikTindakanType == 5) {
      list = [
        {
          Tempat: 'KAWASAN 1',
          s1: 5, s2: 10, s3: 5, s4: 0, s5: 0, s6: 10, s7: 0, s8: 0, s9: 0, s10: 0,
          s11: 10, s12: 0, Jumlah: 40
        },
      ];
    }
    else if (Carian.LaporanStatistikTindakanType == 6) {
      list = [
        {
          Tempat: 'LOKASI TINDAKAN 1',
          s1: 5, s2: 20, s3: 5, s4: 0, s5: 0, s6: 10, s7: 0, s8: 0, s9: 0, s10: 0,
          s11: 10, s12: 0, Jumlah: 50
        },
      ];
    }

  
    // let pageInfo: any = { RowCount: list.length, JumlahTindakan: jumlah }
    // return of({ list, pageInfo });
    return of(list);
  }


  

  getLaporanTindakanMahkamahList(StartDate: string, EndDate: string,  ReportTypes: number, StatusKesalahan: number): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('ReportTypes', (ReportTypes || 0) + "")
      .set('StatusKesalahan', (StatusKesalahan || 0) + "")
      ;
    console.log('params getLaporanTindakanMahkamahList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    if (StartDate && EndDate && ReportTypes == 1) {
      list = [
        { JenisLaporanCounter: '1', Jumlah: 10, },
        { JenisLaporanCounter: '2', Jumlah: 10, },
        { JenisLaporanCounter: '3', Jumlah: 10, },
        { JenisLaporanCounter: '4', Jumlah: 10, },
        { JenisLaporanCounter: '5', Jumlah: 10, }, 
        { JenisLaporanCounter: '6', Jumlah: 10, },
        { JenisLaporanCounter: '7', Jumlah: 10, },
        { JenisLaporanCounter: '8', Jumlah: 10, },
        { JenisLaporanCounter: '9', Jumlah: 10, },
        { JenisLaporanCounter: '10', Jumlah: 10, },  
        { JenisLaporanCounter: '11', Jumlah: 10, },
        { JenisLaporanCounter: '12', Jumlah: 10, },
        { JenisLaporanCounter: '13', Jumlah: 10, },
        { JenisLaporanCounter: '14', Jumlah: 10, },
        { JenisLaporanCounter: '15', Jumlah: 10, }, 
        { JenisLaporanCounter: '16', Jumlah: 10, },
        { JenisLaporanCounter: '17', Jumlah: 10, },
        { JenisLaporanCounter: '18', Jumlah: 10, },
        { JenisLaporanCounter: '19', Jumlah: 10, },
        { JenisLaporanCounter: '20', Jumlah: 10, },  
        { JenisLaporanCounter: '21', Jumlah: 10, },
        { JenisLaporanCounter: '22', Jumlah: 10, },
        { JenisLaporanCounter: '23', Jumlah: 10, },
        { JenisLaporanCounter: '24', Jumlah: 10, },
        { JenisLaporanCounter: '25', Jumlah: 10, }, 
        { JenisLaporanCounter: '26', Jumlah: 10, },
        { JenisLaporanCounter: '27', Jumlah: 10, },
        { JenisLaporanCounter: '28', Jumlah: 10, },
        { JenisLaporanCounter: '29', Jumlah: 10, },
        { JenisLaporanCounter: '30', Jumlah: 10, },  
        { JenisLaporanCounter: 'JUMLAH', Jumlah: 300, },        
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 2) {
      list = [
        { JenisLaporanCounter: '1', Jumlah: 10, },
        { JenisLaporanCounter: '2', Jumlah: 10, },
        { JenisLaporanCounter: '3', Jumlah: 10, },
        { JenisLaporanCounter: '4', Jumlah: 10, },
        { JenisLaporanCounter: 'JUMLAH', Jumlah: 40, },    
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 3) {
      list = [
        { JenisLaporanCounter: 'Januari', Jumlah: 10, },
        { JenisLaporanCounter: 'Februari', Jumlah: 10, },
        { JenisLaporanCounter: 'Mac', Jumlah: 10, },
        { JenisLaporanCounter: 'April', Jumlah: 10, },
        { JenisLaporanCounter: 'Mei', Jumlah: 10, },
        { JenisLaporanCounter: 'Jun', Jumlah: 10, },
        { JenisLaporanCounter: 'Julai', Jumlah: 10, },
        { JenisLaporanCounter: 'Ogos', Jumlah: 10, },
        { JenisLaporanCounter: 'September', Jumlah: 10, },
        { JenisLaporanCounter: 'Oktober', Jumlah: 10, },
        { JenisLaporanCounter: 'November', Jumlah: 10, },
        { JenisLaporanCounter: 'Disember', Jumlah: 10, },
        { JenisLaporanCounter: 'JUMLAH', Jumlah: 120, },   
      ];
    }
    return of(list);
  }

  getLaporanSlkeList(StartDate: string, EndDate: string,  ReportTypes: number, AktaInduk: number): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('ReportTypes', (ReportTypes || 0) + "")
      .set('AktaInduk', (AktaInduk || 0) + "")
      ;
    console.log('params getLaporanSlkeList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    if (StartDate && EndDate && ReportTypes == 1) {
      list = [
        { JenisLaporanCounter: '1', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '2', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '3', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '4', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '5', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 }, 
        { JenisLaporanCounter: '6', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '7', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '8', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '9', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '10', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },  
        { JenisLaporanCounter: '11', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '12', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '13', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '14', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '15', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 }, 
        { JenisLaporanCounter: '16', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '17', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '18', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '19', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '20', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },  
        { JenisLaporanCounter: '21', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '22', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '23', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '24', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '25', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 }, 
        { JenisLaporanCounter: '26', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '27', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '28', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '29', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '30', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },  
        { JenisLaporanCounter: 'JUMLAH', Dikeluarkan: 300,  Dibayar: 150, Kutipan: 15000, Baki: 150 },        
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 2) {
      list = [
        { JenisLaporanCounter: '1', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '2', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '3', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: '4', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'JUMLAH', Dikeluarkan: 40,  Dibayar: 20, Kutipan: 2000, Baki: 20 },    
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 3) {
      list = [
        { JenisLaporanCounter: 'Januari', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'Februari', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'Mac', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'April', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'Mei', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'Jun', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'Julai', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'Ogos', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'September', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'Oktober', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'November', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'Disember', Dikeluarkan: 10, Dibayar: 5, Kutipan: 500, Baki: 5 },
        { JenisLaporanCounter: 'JUMLAH', Dikeluarkan: 120, Dibayar: 60, Kutipan: 6000,  Baki: 60 },   
      ];
    }
    return of(list);
  }

  getLaporanPembayaranNotisList(StartDate: string, EndDate: string,  ReportTypes: number): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('StartDate', StartDate || "")
      .set('EndDate', EndDate || "")
      .set('ReportTypes', (ReportTypes || 0) + "")
      ;
    console.log('params getLaporanPembayaranNotisList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    if (StartDate && EndDate && ReportTypes == 1) {
      list = [
        { JenisLaporanCounter: '1', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '2', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '3', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '4', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '5', JumlahNotis: 10, Bayaran: 1000 }, 
        { JenisLaporanCounter: '6', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '7', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '8', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '9', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '10', JumlahNotis: 10, Bayaran: 1000 },  
        { JenisLaporanCounter: '11', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '12', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '13', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '14', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '15', JumlahNotis: 10, Bayaran: 1000 }, 
        { JenisLaporanCounter: '16', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '17', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '18', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '19', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '20', JumlahNotis: 10, Bayaran: 1000 },  
        { JenisLaporanCounter: '21', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '22', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '23', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '24', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '25', JumlahNotis: 10, Bayaran: 1000 }, 
        { JenisLaporanCounter: '26', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '27', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '28', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '29', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '30', JumlahNotis: 10, Bayaran: 1000 },  
        { JenisLaporanCounter: 'JUMLAH', JumlahNotis: 300, Bayaran: 30000 },        
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 2) {
      list = [
        { JenisLaporanCounter: '1', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '2', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '3', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '4', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'JUMLAH', JumlahNotis: 40, Bayaran: 4000 },    
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 3) {
      list = [
        { JenisLaporanCounter: 'Januari', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Februari', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Mac', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'April', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Mei', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Jun', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Julai', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Ogos', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'September', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Oktober', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'November', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Disember', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'JUMLAH', JumlahNotis: 120, Bayaran: 12000 },   
      ];
    }
    return of(list);
  }



  getLaporanPembayaranNotisListXX(page: Pager, StartDate: string, EndDate: string, NoGaji: string, ReportTypes: number): Observable<any> {
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    //console.log('params getHandheldOfficerList ::::::::::::::::: ', params);
    // console.log('params StartDate ::::::::::::::::: ', StartDate);
    // console.log('params EndDate ::::::::::::::::: ', EndDate);
    // console.log('params NoGaji ::::::::::::::::: ', NoGaji);
    // console.log('params ReportTypes ::::::::::::::::: ', ReportTypes);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];
    if (StartDate && EndDate && ReportTypes == 1) {
      list = [
        { JenisLaporanCounter: '1', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '2', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '3', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '4', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '5', JumlahNotis: 10, Bayaran: 1000 },        
      ];
    }
    else if (StartDate && EndDate && ReportTypes == 2) {
      list = [
        { JenisLaporanCounter: '1', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '2', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '3', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: '4', JumlahNotis: 10, Bayaran: 1000 },
      ];
    }
    else if (StartDate && EndDate && NoGaji && ReportTypes == 3) {
      list = [
        { JenisLaporanCounter: 'Januari', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Februari', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Mac', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'April', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Mei', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Jun', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Julai', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Ogos', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'September', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Oktober', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'November', JumlahNotis: 10, Bayaran: 1000 },
        { JenisLaporanCounter: 'Disember', JumlahNotis: 10, Bayaran: 1000 },
      ];
    }

    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }


  //refrence

  getOffenceActList2(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'TRAFIK' },
      { id: 2, name: 'AM' },
    ];
    return of(list);
  }

  getOffenceActList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'TRAFIK' },
      { id: 2, name: 'AM' },
      { id: 3, name: 'TRAFIK & AM' },
    ];
    return of(list);
  }


  getDdeTypeList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'BULANAN' },
      { id: 2, name: 'TAHUNAN' },
      { id: 3, name: 'PRESTASI INDIVIDU DDE' },
    ];
    return of(list);
  }


  

  getPembayaranNotisTypeList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'HARIAN' },
      { id: 2, name: 'MINGGUAN' },
      { id: 3, name: 'BULANAN' },
    ];
    return of(list);
  }


  getJenisBadanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Jenis Badan 1' },
      { id: 2, name: 'Jenis Badan 2' },
      { id: 3, name: 'Jenis Badan 3' },
    ];
    return of(list);
  }

  getJenisList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Jenis 1' },
      { id: 2, name: 'Jenis 2' },
      { id: 3, name: 'Jenis 3' },
    ];
    return of(list);
  }

  getBahagianList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Bahagian 1' },
      { id: 2, name: 'Bahagian 2' },
      { id: 3, name: 'Bahagian 3' },
      { id: 4, name: 'Bahagian 4' },
    ];
    return of(list);
  }

  getParlimenList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Parlimen 1' },
      { id: 2, name: 'Parlimen 2' },
      { id: 3, name: 'Parlimen 3' },
      { id: 4, name: 'Parlimen 4' },
    ];
    return of(list);
  }

  getKawasanByParlimenList(parlimenId): Observable<any> {
    //console.log('getJalanList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    if (parlimenId != '0') {
      list = [
        { id: 1, name: 'Kawasan 1' },
        { id: 2, name: 'Kawasan 2' },
        { id: 3, name: 'Kawasan 3' },
        { id: 4, name: 'Kawasan 4' },
      ];
    }

    return of(list);
  }

  getLokasiByKawasanList(kawasanId): Observable<any> {
    //console.log('getJalanList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    if (kawasanId != '0') {
      list = [
        { id: 1, name: 'Lokasi 1' },
        { id: 2, name: 'Lokasi 2' },
        { id: 3, name: 'Lokasi 3' },
        { id: 4, name: 'Lokasi 4' },
      ];
    }

    return of(list);
  }

  getKawasanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Kawasan 1' },
      { id: 2, name: 'Kawasan 2' },
      { id: 3, name: 'Kawasan 3' },
      { id: 4, name: 'Kawasan 4' },
    ];
    return of(list);
  }

  getJalanList(kawasanId): Observable<any> {
    //console.log('getJalanList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    if (kawasanId != '0') {
      list = [
        { id: 1, name: 'Jalan 1' },
        { id: 2, name: 'Jalan 2' },
        { id: 3, name: 'Jalan 3' },
        { id: 4, name: 'Jalan 4' },
      ];
    }

    return of(list);
  }

  getAktaIndukListAll(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    
      list = [
        { id: 1, name: 'AktaInduk 1' },
        { id: 2, name: 'AktaInduk 2' },
        { id: 3, name: 'AktaInduk 3' },
        { id: 4, name: 'AktaInduk 4' },
      ];
    
    return of(list);
  }

  getSeksyenByAktaList(Akta, Carian: any): Observable<any> {

    const params = new HttpParams().set('StartDate', Carian.StartDate || "")
    .set('EndDate', Carian.EndDate || "")
    .set('Parlimen', String(Carian.Parlimen) || "")
    .set('Kawasan', String(Carian.Kawasan) || "")
    .set('Lokasi', String(Carian.Lokasi) || "")
    ;

    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    let jumlah = 0;
    if (Akta == '1' ) {
      list = [
        { id: 1, code: 'Kod 1', count: 0 },
        { id: 2, code: 'Kod 2', count: 10 },
        { id: 3, code: 'Kod 3', count: 0 },
        { id: 4, code: 'Kod 4', count: 0 },
        { id: 5, code: 'Kod 5', count: 0 },
        { id: 6, code: 'Kod 6', count: 0 },
        { id: 7, code: 'Kod 7', count: 0 },
        { id: 8, code: 'Kod 8', count: 0 },
        { id: 9, code: 'Kod 9', count: 0 },
        { id: 10, code: 'Kod 10', count: 0 },
        { id: 11, code: 'Kod 11', count: 0 },
        { id: 12, code: 'Kod 12', count: 0 },
        { id: 13, code: 'Kod 13', count: 10 },
        { id: 14, code: 'Kod 14', count: 0 },
        { id: 15, code: 'Kod 15', count: 0 },
        { id: 16, code: 'Kod 16', count: 0 },
        { id: 17, code: 'Kod 17', count: 0 },
        { id: 18, code: 'Kod 18', count: 0 },
        { id: 19, code: 'Kod 19', count: 0 },
        { id: 20, code: 'Kod 20', count: 0 },
        { id: 21, code: 'Kod 21', count: 10 },
        { id: 22, code: 'Kod 22', count: 0 },
        { id: 23, code: 'Kod 23', count: 0 },
        { id: 24, code: 'Kod 24', count: 0 },
        { id: 25, code: 'Kod 25', count: 0 },
        { id: 26, code: 'Kod 26', count: 0 },
        { id: 27, code: 'Kod 27', count: 0 },
        { id: 28, code: 'Kod 28', count: 0 },
        { id: 29, code: 'Kod 29', count: 0 },
        { id: 30, code: 'Kod 30', count: 0 },
        { id: 31, code: 'Kod 31', count: 0 },
        { id: 32, code: 'Kod 32', count: 0 },
        { id: 33, code: 'Kod 33', count: 0 },
        { id: 34, code: 'Kod 34', count: 0 },
        { id: 35, code: 'Kod 35', count: 0 },
        { id: 36, code: 'Kod 36', count: 0 },
        { id: 37, code: 'Kod 37', count: 0 },
        { id: 38, code: 'Kod 38', count: 0 },
        { id: 39, code: 'Kod 39', count: 0 },
        { id: 40, code: 'Kod 40', count: 0 },
        { id: 41, code: 'Kod 41', count: 0 },
        { id: 42, code: 'Kod 42', count: 0 },
        { id: 43, code: 'Kod 43', count: 0 },
        { id: 44, code: 'Kod 44', count: 0 },
        { id: 45, code: 'Kod 45', count: 0 },
        { id: 46, code: 'Kod 46', count: 0 },
        { id: 47, code: 'Kod 47', count: 0 },
        { id: 48, code: 'Kod 48', count: 0 },
        { id: 49, code: 'Kod 49', count: 0 },
      ];
      jumlah = 30;
    }
    else if (Akta == '2' || Akta == '4' ) {
      list = [
        { id: 51, code: 'Kod 51', count: 5 },
        { id: 52, code: 'Kod 52', count: 0 },
        { id: 53, code: 'Kod 53', count: 0 },
        { id: 54, code: 'Kod 54', count: 0 },
        { id: 55, code: 'Kod 55', count: 5 },
        { id: 56, code: 'Kod 56', count: 0 },
        { id: 57, code: 'Kod 57', count: 5 },
        { id: 58, code: 'Kod 58', count: 0 },
        { id: 59, code: 'Kod 59', count: 0 },
        { id: 60, code: 'Kod 60', count: 5 },
        { id: 61, code: 'Kod 61', count: 0 },
        { id: 62, code: 'Kod 62', count: 0 },
      ];
      jumlah = 20;
    }
    else if (Akta == '3' ) {
      list = [
        { id: 80, code: 'Kod 80', count: 10 },
        { id: 81, code: 'Kod 81', count: 5 },
        { id: 82, code: 'Kod 82', count: 5 },
        { id: 83, code: 'Kod 83', count: 10 },
        { id: 84, code: 'Kod 84', count: 0 },
        { id: 85, code: 'Kod 85', count: 0 },
        { id: 86, code: 'Kod 86', count: 0 },
        { id: 87, code: 'Kod 87', count: 10 },
        { id: 88, code: 'Kod 88', count: 0 },
        { id: 89, code: 'Kod 89', count: 0 },
        { id: 90, code: 'Kod 90', count: 0 },
        { id: 91, code: 'Kod 91', count: 10 },
        { id: 92, code: 'Kod 92', count: 0 },
        { id: 93, code: 'Kod 93', count: 0 },
        { id: 94, code: 'Kod 94', count: 0 },
        { id: 95, code: 'Kod 95', count: 0 },
        { id: 96, code: 'Kod 96', count: 0 },
        { id: 97, code: 'Kod 97', count: 0 },
        { id: 98, code: 'Kod 98', count: 0 },
        { id: 99, code: 'Kod 99', count: 0 },       
      ];
      jumlah = 50;
    }

    
    return of({list, jumlah});
  }

  getAktaIndukList(KelasAkta): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    if (KelasAkta != '0') {
      list = [
        { id: 1, name: 'AktaInduk 1' },
        { id: 2, name: 'AktaInduk 2' },
        { id: 3, name: 'AktaInduk 3' },
        { id: 4, name: 'AktaInduk 4' },
      ];
    }
    return of(list);
  }

  getPeruntukanList(AktaIndukId): Observable<any> {
    //console.log('getJalanList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    if (AktaIndukId != '0') {
      list = [
        { id: 1, name: 'Peruntukan Undang - Undang 1' },
        { id: 2, name: 'Peruntukan Undang - Undang 2' },
        { id: 3, name: 'Peruntukan Undang - Undang 3' },
        { id: 4, name: 'Peruntukan Undang - Undang 4' },
      ];
    }

    return of(list);
  }

  getKumpulanList(): Observable<any> {
    //console.log('getJalanList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];

    list = [
      { id: 1, name: 'Kumpulan 1' },
      { id: 2, name: 'Kumpulan 2' },
      { id: 3, name: 'Kumpulan 3' },
      { id: 4, name: 'Kumpulan 4' },
    ];


    return of(list);
  }

  getKumpulanPenggunaList(): Observable<any> {
    //console.log('getJalanList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];

    list = [
      { id: 1, name: 'Kumpulan Pengguna 1' },
      { id: 2, name: 'Kumpulan Pengguna 2' },
      { id: 3, name: 'Kumpulan Pengguna 3' },
      { id: 4, name: 'Kumpulan Pengguna 4' },
    ];


    return of(list);
  }

  
  
  getStatusKesalahanList(): Observable<any> {
    //console.log('getJalanList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];

    list = [
      { id: 1, name: 'Status Kesalahan 1' },
      { id: 2, name: 'Status Kesalahan 3' },
      { id: 3, name: 'Status Kesalahan 2' },
    ];


    return of(list);
  }

  getPembatalanNotisTypeList(): Observable<any> {
    //console.log('getJalanList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];

    list = [
      { id: 1, name: 'Kumpulan Pengguna' },
      { id: 2, name: 'Pegawai' },
      { id: 3, name: 'Sebab / Alasan / Justifikasi' },
    ];


    return of(list);
  }


  getPeruntukanByKelasAktaList(KelasAkta): Observable<any> {
    //console.log('getJalanList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [];
    if (KelasAkta == '1') {
      list = [
        { id: 1, name: 'Peruntukan Undang - Undang Traffik 1' },
        { id: 2, name: 'Peruntukan Undang - Undang Traffik 2' },
        { id: 3, name: 'Peruntukan Undang - Undang Traffik 3' },
        { id: 4, name: 'Peruntukan Undang - Undang Traffik 4' },
      ];
    }
    else if (KelasAkta == '2') {
      list = [
        { id: 5, name: 'Peruntukan Undang - Undang AM 1' },
        { id: 6, name: 'Peruntukan Undang - Undang AM 2' },
        { id: 7, name: 'Peruntukan Undang - Undang AM 3' },
        { id: 8, name: 'Peruntukan Undang - Undang AM 4' },
        { id: 9, name: 'Peruntukan Undang - Undang AM 5' },
        { id: 10, name: 'Peruntukan Undang - Undang AM 6' },
        { id: 11, name: 'Peruntukan Undang - Undang AM 7' },
        { id: 12, name: 'Peruntukan Undang - Undang AM 8' },
      ];
    }

    return of(list);
  }

  

  getStatistikTindakanTypeList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'HARIAN' },
      { id: 2, name: 'BULANAN AM/TRAFIK' },
      { id: 3, name: 'TAHUNAN' },
      { id: 4, name: 'PARLIMEN' },
      { id: 5, name: 'KAWASAN' },
      { id: 6, name: 'LOKASI TINDAKAN' },
    ];
    return of(list);
  }


  getStatusNotisList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Status 1' },
      { id: 2, name: 'Status 2' },
    ];
    return of(list);
  }

  getJenisDokumenList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Dokumen 1' },
      { id: 2, name: 'Dokumen 2' },
      { id: 3, name: 'Dokumen 3' },
    ];
    return of(list);
  }


  getJenisNotisList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'TRAFIK' },
      { id: 2, name: 'AM' },
    ];
    return of(list);
  }






}




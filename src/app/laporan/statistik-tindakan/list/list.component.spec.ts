import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanStatistikTindakanListComponent } from './list.component';

describe('LaporanStatistikTindakanListComponent', () => {
  let component: LaporanStatistikTindakanListComponent;
  let fixture: ComponentFixture<LaporanStatistikTindakanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanStatistikTindakanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanStatistikTindakanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

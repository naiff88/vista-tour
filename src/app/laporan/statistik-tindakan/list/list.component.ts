import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { LaporanService } from '../../laporan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Moment } from 'moment';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');

@Component({
  selector: 'app-laporan-dde-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})


export class LaporanStatistikTindakanListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  showList: boolean = false;
  eventSearch: any;
  onSearch: boolean = false;
  submitted: boolean = false;
  form: FormGroup;
  //close standard var for searching page

  //initiate list
  laporanStatistikTindakanTypeList: any = [];
  BahagianList: any = [];
  ParlimenList: any = [];
  KawasanList: any = [];
  LokasiList: any = [];
  PeriodTypeList: any = [];
  ReportList: any = [];
  AktaList: any = [];



  //initiate list model 
  LaporanStatistikTindakanType: number = 0;
  Bahagian: number = 0;
  Parlimen: number = 0;
  Kawasan: number = 0;
  Lokasi: number = 0;

  //counter
  CounterByPeriod: any = {};
  CounterBySeksyenAkta: any = {};
  ListSeksyenByAkta: any = {};
  JumlahSeksyenByAkta: any = {};

  //initiate date model 
  BulanTahun: any = moment();
  Tahun: any = moment();
  TarikhMulaLaporan: any = '';
  TarikhAkhirLaporan: any = '';
  minDate: Date;
  maxDate: Date;

  //dynamic report param
  TarikhMula: any = '';
  TarikhAkhir: any = '';
  TarikhMulaPeriodType: any = '';
  TarikhAkhirPeriodType: any = '';
  NoGaji: string = '';

  //display date trick
  DisplayMonthYear: any = moment().format("MM/YYYY");;
  DisplayYear: any = moment().format("YYYY");;;


  //report props
  reportCol: any = [];
  // reportCol1: any = [
  //   { name: 'index', title: 'No.', width: '5', align: 'center' },
  //   { name: 'IdPengguna', title: 'ID Pengguna', width: '15', align: 'left' },
  //   { name: 'Nama', title: 'Nama', width: '20', align: 'left' },
  //   { name: 'JumlahNotis', title: 'Jumlah Notis', width: '15', align: 'left' },
  // ];
  // reportCol2: any = [
  //   { name: 'index', title: 'No.', width: '5', align: 'center' },
  //   { name: 'NoNotis', title: 'No. Notis', width: '15', align: 'left' },
  //   { name: 'Jenis', title: 'Jenis', width: '20', align: 'left' },
  //   { name: 'TarikhKesalahan', title: 'Tarikh Kesalahan', width: '15', align: 'left' },
  //   { name: 'TarikhWujud', title: 'Tarikh Wujud', width: '15', align: 'left' },
  //   { name: 'NamaPesalah', title: 'Nama Pesalah', width: '20', align: 'left' },
  //   { name: 'NoKenderaan', title: 'No. Kenderaan', width: '15', align: 'left' },
  //   { name: 'NoCukaiJalan', title: 'No. Cukai Jalan', width: '15', align: 'left' },
  //   { name: 'Peruntukan', title: 'Peruntukan Undang-Undang', width: '30', align: 'left' },
  //   { name: 'Seksyen', title: 'Seksyen Kesalahan', width: '15', align: 'left' },
  //   { name: 'Tempat', title: 'Tempat/Lokasi Kesalahan', width: '20', align: 'left' },
  //   { name: 'NoPetak', title: 'No. Petak/Tg', width: '15', align: 'left' },
  // ];

  //report title
  DisplayDate: any = '';
  ReportTitle: string = 'Laporan Statistik Tindakan';
  ReportName = this.ReportTitle;

  //for excel ganerator 
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  //standard contructor
  constructor(
    private laporanService: LaporanService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear, 0, 1);
    this.maxDate = new Date(currentYear, 11, 31);
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  //form
  get f() { return this.form.controls; }

  //init methode to call default function
  ngOnInit() {
    this.loadSearchDetails();
    this.getHandheldOfficerList();
    this.getLaporanStatistikTindakanTypeList();
    this.getBahagianList();
    this.getParlimenList();
    // this.getKawasanList();
    // this.getLokasiList();
  }

  loadSearchDetails() {
    this.form = this.buildFormItems();
  }

  //open standard listing function
  getHandheldOfficerList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    this.laporanService.getTindakanOfficerList(this.pageSetting.getPagerSetting(this.paginator, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.form.value)
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;

        if (this.onSearch &&
          (this.LaporanStatistikTindakanType == 1 || this.LaporanStatistikTindakanType == 2 || this.LaporanStatistikTindakanType == 3)) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }

        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    this.primengTableHelper.showLoadingIndicator();
  }
  getStatistikTindakanMengikutTempatList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    this.laporanService.getStatistikTindakanMengikutTempatList(this.TarikhMula, this.TarikhAkhir, this.form.value)
      .subscribe(items => {
        this.showSpinner = false;
        this.ReportList = items;
        //console.log('this.ReportList  ::::::::::: ', this.ReportList );
      });
  }
  getPeriodType() {
    this.PeriodTypeList = [];
    if (this.TarikhMulaPeriodType && this.TarikhAkhirPeriodType) {

      var start = this.TarikhMulaPeriodType.split("/");
      var end = this.TarikhAkhirPeriodType.split("/");
      let s = new Date(start[2], start[1] - 1, start[0]);
      let e = new Date(end[2], end[1] - 1, end[0]);

      if (this.LaporanStatistikTindakanType == 1) {
        while (s <= e) {
          this.PeriodTypeList.push({ id: s.getDate(), name: s.getDate() });
          let newDate = s.setDate(s.getDate() + 1);
          s = new Date(newDate);
        }
      }
      else if (this.LaporanStatistikTindakanType == 2 || this.LaporanStatistikTindakanType == 3) {
        var startDate = moment(start[2] + '-' + start[1] + '-' + start[0]);
        var endDate = moment(end[2] + '-' + end[1] + '-' + end[0]);
        while (startDate.isBefore(endDate)) {
          this.PeriodTypeList.push({ id: startDate.format("M"), name: this.getMonthFullName(startDate.format("M")) });
          startDate.add(1, 'month');
        }
      }
      //console.log('DATE PeriodTypeList ::: ', this.PeriodTypeList);
    }
  }
  getMonthFullName(m) {
    var month = new Array();
    month[1] = "Jan";
    month[2] = "Feb";
    month[3] = "Mac";
    month[4] = "Apr";
    month[5] = "Mei";
    month[6] = "Jun";
    month[7] = "Jul";
    month[8] = "Ogo";
    month[9] = "Sep";
    month[10] = "Okt";
    month[11] = "Nov";
    month[12] = "Dis";
    return month[m];
  }
  getCountByPenguatkuasaTempoh() {
    let penguatkuasaList = this.dataSource.data;
    for (var key in penguatkuasaList) {
      let idx = 1;
      this.reportCol = [{ name: 'index', title: 'No.', width: '5', align: 'center' },
      { name: 'NoGaji', title: 'No. Gaji', width: '20', align: 'left' },
      { name: 'Pangkat', title: 'Pangkat', width: '20', align: 'left' },
      { name: 'NoBadan', title: 'No. Anggota', width: '20', align: 'left' },
      { name: 'Nama', title: 'Nama', width: '30', align: 'left' },
      { name: 'Bahagian', title: 'Zon / Bahagian / unit', width: '30', align: 'left' },];
      for (var key2 in this.PeriodTypeList) {
        this.reportCol.push(
          { name: 's' + idx, title: this.LaporanStatistikTindakanType == 1 ? idx : this.getMonthFullName(idx), width: '10', align: 'center' });
        this.CounterByPeriod[penguatkuasaList[key].Id + '_' + this.PeriodTypeList[key2].id] = penguatkuasaList[key]['s' + this.PeriodTypeList[key2].id];
        idx++;
      }
      this.reportCol.push({ name: 'Jumlah', title: 'Jumlah', width: '10', align: 'center' });
    }
  }

  getListSeksyenByAkta() {

    for (var key2 in this.AktaList) {
      this.laporanService.getSeksyenByAktaList(this.AktaList[key2].id, this.form.value).subscribe(items => {
        this.ListSeksyenByAkta[this.AktaList[key2].id + '_'] = items.list;
        this.JumlahSeksyenByAkta[this.AktaList[key2].id + '_'] = items.jumlah;
      });
    }

    // let penguatkuasaList = this.dataSource.data;
    // for (var key in penguatkuasaList) {
    //   let idx = 1;
    //   this.reportCol = [ { name: 'index', title: 'No.', width: '5', align: 'center' },
    //   { name: 'NoGaji', title: 'No. Gaji', width: '20', align: 'left' },
    //   { name: 'Pangkat', title: 'Pangkat', width: '20', align: 'left' },
    //   { name: 'NoBadan', title: 'No. Anggota', width: '20', align: 'left' },
    //   { name: 'Nama', title: 'Nama', width: '30', align: 'left' },
    //   { name: 'Bahagian', title: 'Zon / Bahagian / unit', width: '30', align: 'left' },];
    //   for (var key2 in this.PeriodTypeList) {
    //     this.reportCol.push(       
    //     { name: 's'+idx, title: this.LaporanStatistikTindakanType == 1 ? idx : this.getMonthFullName(idx), width: '10', align: 'center' });
    //     this.CounterByPeriod[penguatkuasaList[key].Id + '_' + this.PeriodTypeList[key2].id] = penguatkuasaList[key]['s' + this.PeriodTypeList[key2].id];
    //     idx++;
    //   }
    //   this.reportCol.push({ name: 'Jumlah', title: 'Jumlah', width: '10', align: 'center' });
    // }
  }

  //close standard listing function

  //open default listing function
  getLaporanStatistikTindakanTypeList() {
    this.laporanService.getStatistikTindakanTypeList().subscribe(items => {
      this.laporanStatistikTindakanTypeList = items;
    });
  }
  getBahagianList() {
    this.laporanService.getBahagianList().subscribe(items => {
      this.BahagianList = items;
    });
  }
  getParlimenList() {
    this.laporanService.getParlimenList().subscribe(items => {
      this.ParlimenList = items;
    });
  }
  getKawasanList() {
    this.laporanService.getKawasanByParlimenList(this.Parlimen).subscribe(items => {
      this.KawasanList = items;
    });
  }
  getLokasiList() {
    this.laporanService.getLokasiByKawasanList(this.Kawasan).subscribe(items => {
      this.LokasiList = items;
    });
  }
  getAktaIndukListAll() {
    if (this.LaporanStatistikTindakanType == 4 || this.LaporanStatistikTindakanType == 5 || this.LaporanStatistikTindakanType == 6) {
      this.laporanService.getAktaIndukListAll().subscribe(items => {
        this.AktaList = items;
      });
    }
  }



  //close default listing function

  //if diff date format in one component
  //open select month year function  
  chosenYearHandlerOnly(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const Tahun = this.form.controls['Tahun'];
    Tahun.setValue(moment());
    const ctrlValue = Tahun.value;
    ctrlValue.year(normalizedYear.year());
    Tahun.setValue(ctrlValue);
    this.DisplayYear = moment(Tahun.value).format("YYYY");
    datepicker.close();

    this.minDate = new Date(this.DisplayYear, 0, 1);
    this.maxDate = new Date(this.DisplayYear, 11, 31);
    this.TarikhMulaLaporan = "";
    this.TarikhAkhirLaporan = "";

  }
  chosenYearHandler(normalizedYear: Moment) {
    const BulanTahun = this.form.controls['BulanTahun'];
    BulanTahun.setValue(moment());
    const ctrlValue = BulanTahun.value;
    ctrlValue.year(normalizedYear.year());
    BulanTahun.setValue(ctrlValue);
    this.DisplayMonthYear = moment(BulanTahun.value).format("MM/YYYY");
  }
  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const BulanTahun = this.form.controls['BulanTahun'];
    BulanTahun.setValue(moment());
    const ctrlValue = BulanTahun.value;
    ctrlValue.month(normalizedMonth.month());
    BulanTahun.setValue(ctrlValue);
    this.DisplayMonthYear = moment(BulanTahun.value).format("MM/YYYY");
    datepicker.close();
  }
  //close select month year function  

  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      this.showList = true;
      //dynamicly display date range at report title
      if (searchForm.LaporanStatistikTindakanType == 1 && searchForm.BulanTahun) {
        this.DisplayDate = moment(searchForm.BulanTahun).format("MM/YYYY");
      }
      else if (searchForm.LaporanStatistikTindakanType == 3 && searchForm.Tahun) {
        this.DisplayDate = moment(searchForm.Tahun).format("YYYY");
      }
      else if ((searchForm.LaporanStatistikTindakanType == 2 || searchForm.LaporanStatistikTindakanType == 4
        || searchForm.LaporanStatistikTindakanType == 5 || searchForm.LaporanStatistikTindakanType == 6) && searchForm.TarikhMulaLaporan && searchForm.TarikhAkhirLaporan) {
        this.DisplayDate = moment(searchForm.TarikhMulaLaporan).format("DD/MM/YYYY") + ' - ' + moment(searchForm.TarikhAkhirLaporan).format("DD/MM/YYYY");
      }


      //dynamicly display report title
      let laporanType = this.laporanStatistikTindakanTypeList.find(i => i.id == searchForm.LaporanStatistikTindakanType);

      this.ReportName = this.ReportName;
      if (laporanType) {
        if (searchForm.LaporanStatistikTindakanType == 4
          || searchForm.LaporanStatistikTindakanType == 5 || searchForm.LaporanStatistikTindakanType == 6) {
          let namaTempat: any;
          if (searchForm.LaporanStatistikTindakanType == 4) {
            namaTempat = this.ParlimenList.find(i => i.id == searchForm.Parlimen);
          }
          else if (searchForm.LaporanStatistikTindakanType == 5) {
            namaTempat = this.KawasanList.find(i => i.id == searchForm.Kawasan);
          }
          else if (searchForm.LaporanStatistikTindakanType == 6) {
            namaTempat = this.LokasiList.find(i => i.id == searchForm.Lokasi);
          }
          this.ReportTitle = 'STATISTIK NOTIS KOMPAUN BAGI ' + this.DisplayDate + ' UNTUK ' + laporanType.name + ' ' + namaTempat.name;
        } else {
          this.ReportTitle = 'LAPORAN ' + laporanType.name + ' STATISTIK TINDAKAN BAGI ' + this.DisplayDate;
        }
      }
      this.ReportTitle = this.ReportTitle.toUpperCase();

      //dynamicly set date range as param to call list
      if (searchForm.LaporanStatistikTindakanType == 1 && searchForm.BulanTahun) {
        this.TarikhMula = this.TarikhMulaPeriodType = searchForm.BulanTahun.startOf('month').format('DD/MM/YYYY');
        this.TarikhAkhir = this.TarikhAkhirPeriodType = searchForm.BulanTahun.endOf('month').format('DD/MM/YYYY');
      }
      else if ((searchForm.LaporanStatistikTindakanType == 2 || searchForm.LaporanStatistikTindakanType == 4
        || searchForm.LaporanStatistikTindakanType == 5 || searchForm.LaporanStatistikTindakanType == 6) && searchForm.Tahun) {
        this.TarikhMulaPeriodType = searchForm.Tahun.startOf('year').format('DD/MM/YYYY');
        this.TarikhAkhirPeriodType = searchForm.Tahun.endOf('year').format('DD/MM/YYYY');
        this.TarikhMula = searchForm.TarikhMulaLaporan.format('DD/MM/YYYY');;
        this.TarikhAkhir = searchForm.TarikhAkhirLaporan.format('DD/MM/YYYY');;
      }
      else if (searchForm.LaporanStatistikTindakanType == 3 && searchForm.Tahun) {
        this.TarikhMula = this.TarikhMulaPeriodType = searchForm.Tahun.startOf('year').format('DD/MM/YYYY');
        this.TarikhAkhir = this.TarikhAkhirPeriodType = searchForm.Tahun.endOf('year').format('DD/MM/YYYY');
      }

      this.onSearch = true;
      this.getHandheldOfficerList();
      this.getPeriodType();
      this.getCountByPenguatkuasaTempoh();
      this.getStatistikTindakanMengikutTempatList();
      this.getAktaIndukListAll();
      this.getListSeksyenByAkta();
      this.paginator.changePage(0);


    }
  }


  //on change jenis report
  onChangeType() {
    const LaporanStatistikTindakanType = this.LaporanStatistikTindakanType;
    //call reset function
    this.reset();
    this.form.patchValue({ LaporanStatistikTindakanType });
    this.LaporanStatistikTindakanType = LaporanStatistikTindakanType;
  }
  onChangeParlimen() {
    this.Kawasan = 0;
    this.Lokasi = 0;
    this.getKawasanList();
  }
  onChangeKawasan() {
    this.Lokasi = 0;
    this.getLokasiList();
  }

  reset() {
    this.form = this.buildFormItems();
    this.LaporanStatistikTindakanType = 0;
    this.Parlimen = 0;
    this.Kawasan = 0;
    this.Lokasi = 0;
    this.BulanTahun = moment();
    this.Tahun = moment();
    this.TarikhMulaLaporan = '';
    this.TarikhAkhirLaporan = '';
    this.TarikhMula = '';
    this.TarikhAkhir = '';
    this.NoGaji = '';
    //reset list
    this.ReportList = [];
    this.dataSource.data = [];
    this.primengTableHelper.totalRecordsCount = 0;
    this.primengTableHelper.records = [];
    this.showList = false;
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      if (formGroup.dirty) {
        this.showList = false;
      }
      const BulanTahun = formGroup.controls['BulanTahun'];
      const Tahun = formGroup.controls['Tahun'];
      const LaporanStatistikTindakanType = formGroup.controls['LaporanStatistikTindakanType'];
      const TarikhMulaLaporan = formGroup.controls['TarikhMulaLaporan'];
      const TarikhAkhirLaporan = formGroup.controls['TarikhAkhirLaporan'];
      const NoGaji = formGroup.controls['NoGaji'];
      const Bahagian = formGroup.controls['Bahagian'];
      const Parlimen = formGroup.controls['Parlimen'];
      const Kawasan = formGroup.controls['Kawasan'];
      const Lokasi = formGroup.controls['Lokasi'];

      if (Bahagian && (LaporanStatistikTindakanType.value == 1 || LaporanStatistikTindakanType.value == 2 || LaporanStatistikTindakanType.value == 3)) {
        Bahagian.setErrors(null);
        if (!Bahagian.value) {
          Bahagian.setErrors({ required: true });
        }
        else if (Bahagian.value == 0) {
          Bahagian.setErrors({ min: true });
        }
      }

      if (Parlimen && (LaporanStatistikTindakanType.value == 4 || LaporanStatistikTindakanType.value == 5 || LaporanStatistikTindakanType.value == 6)) {
        Parlimen.setErrors(null);
        if (!Parlimen.value) {
          Parlimen.setErrors({ required: true });
        }
        else if (Parlimen.value == 0) {
          Parlimen.setErrors({ min: true });
        }
      }
      if (Kawasan && (LaporanStatistikTindakanType.value == 5 || LaporanStatistikTindakanType.value == 6)) {
        Kawasan.setErrors(null);
        if (!Kawasan.value) {
          Kawasan.setErrors({ required: true });
        }
        else if (Kawasan.value == 0) {
          Kawasan.setErrors({ min: true });
        }
      }
      if (Lokasi && (LaporanStatistikTindakanType.value == 6)) {
        Lokasi.setErrors(null);
        if (!Lokasi.value) {
          Lokasi.setErrors({ required: true });
        }
        else if (Lokasi.value == 0) {
          Lokasi.setErrors({ min: true });
        }
      }



      if (LaporanStatistikTindakanType) {
        LaporanStatistikTindakanType.setErrors(null);
        if (!LaporanStatistikTindakanType.value) {
          LaporanStatistikTindakanType.setErrors({ required: true });
        }
        else if (LaporanStatistikTindakanType.value == 0) {
          LaporanStatistikTindakanType.setErrors({ min: true });
        }
      }
      if (BulanTahun && LaporanStatistikTindakanType.value == 1) {
        BulanTahun.setErrors(null);
        if (!BulanTahun.value) {
          BulanTahun.setErrors({ required: true });
        }
        else if (!BulanTahun.value) {
          BulanTahun.setErrors({ required: true });
        }
      }
      if (Tahun && (LaporanStatistikTindakanType.value == 2 || LaporanStatistikTindakanType.value == 3 || LaporanStatistikTindakanType.value == 4 || LaporanStatistikTindakanType.value == 5 || LaporanStatistikTindakanType.value == 6)) {
        Tahun.setErrors(null);
        if (!Tahun.value) {
          Tahun.setErrors({ required: true });
        }
        else if (!Tahun.value) {
          Tahun.setErrors({ required: true });
        }
      }
      if (TarikhMulaLaporan && (LaporanStatistikTindakanType.value == 2 || LaporanStatistikTindakanType.value == 4 || LaporanStatistikTindakanType.value == 5 || LaporanStatistikTindakanType.value == 6)) {
        TarikhMulaLaporan.setErrors(null);
        if (!TarikhMulaLaporan.value) {
          TarikhMulaLaporan.setErrors({ required: true });
        }
        else if (TarikhMulaLaporan.value && TarikhAkhirLaporan.value && TarikhAkhirLaporan.value < TarikhMulaLaporan.value) {
          TarikhMulaLaporan.setErrors({ exceed: true });
        }
      }
      if (TarikhAkhirLaporan && (LaporanStatistikTindakanType.value == 2 || LaporanStatistikTindakanType.value == 4 || LaporanStatistikTindakanType.value == 5 || LaporanStatistikTindakanType.value == 6)) {
        TarikhAkhirLaporan.setErrors(null);
        if (!TarikhAkhirLaporan.value) {
          TarikhAkhirLaporan.setErrors({ required: true });
        }
        else if (TarikhMulaLaporan.value && TarikhAkhirLaporan.value && TarikhAkhirLaporan.value < TarikhMulaLaporan.value) {
          TarikhAkhirLaporan.setErrors({ exceed: true });
        }
      }
      if (NoGaji && (LaporanStatistikTindakanType.value == 1 || LaporanStatistikTindakanType.value == 2 || LaporanStatistikTindakanType.value == 3)) {
        NoGaji.setErrors(null);
        if (!NoGaji.value) {
          NoGaji.setErrors({ required: true });
        }
      }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      BulanTahun: [moment(), []],
      Tahun: [moment(), []],
      LaporanStatistikTindakanType: ['0', []],
      Bahagian: ['0', []],
      Parlimen: ['0', []],
      Kawasan: ['0', []],
      Lokasi: ['0', []],
      NoGaji: ['', []],
      TarikhMulaLaporan: ['', []],
      TarikhAkhirLaporan: ['', []],
    }, { validator: this.customValidation() }
    );
  }




  //excel genarator
  printTableExcel() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getTindakanOfficerList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.form.value).subscribe(items => {
        list = items.list;
        this.pageSetting.excelGenerator(this.reportCol, list, this.ReportName, this.ReportTitle, [{ label: 'Jumlah Keseluruhan Tindakan : ', value: items.pageInfo.JumlahTindakan }]).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }
  //pdf genarator
  printTablePdf() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getTindakanOfficerList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.form.value).subscribe(items => {
        list = items.list;

        this.pageSetting.pdfGenerator(this.reportCol, list, this.ReportName, this.ReportTitle, this.LaporanStatistikTindakanType == 1 ? 'l' : 'p', 5,
          null, [{ label: 'Jumlah Keseluruhan Tindakan : ', value: items.pageInfo.JumlahTindakan }]).then(loaded => {
            if (loaded) {
              this.showSpinner = false;
            }
          });
      });
  }

  printTableExcelCustom() {
    var workbook = new Excel.Workbook();
    // Tab Title
    var worksheet = workbook.addWorksheet('Lokasi');
    // Columns to display
    //worksheet.mergeCells('A1', 'H1'); //title merge cell
    // console.log('this.PeruntukanList :::::::::::::::: ', this.PeruntukanList);
    // console.log('this.PeruntukanList :::::::::::::::: ', this.PeruntukanList.length);
    worksheet.mergeCells('A1', 'N1'); //title merge cell
    worksheet.getCell('A1').value = this.ReportTitle; //title
    worksheet.getCell('A1').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };

    //worksheet.mergeCells('A2', 'A3');
    worksheet.getCell('A2').value = this.LaporanStatistikTindakanType == 4 ? 'Parlimen' : this.LaporanStatistikTindakanType == 5 ? 'Kawasan' : this.LaporanStatistikTindakanType == 5 ? 'Lokasi Tindakan' : '';
    worksheet.getCell('A2').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };
    worksheet.getCell('B2').value = 'Jan';
    worksheet.getCell('B2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('C2').value = 'Feb';
    worksheet.getCell('C2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('D2').value = 'Mac';
    worksheet.getCell('D2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('E2').value = 'Apr';
    worksheet.getCell('E2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('F2').value = 'Mei';
    worksheet.getCell('F2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('G2').value = 'Jun';
    worksheet.getCell('G2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('H2').value = 'Jul';
    worksheet.getCell('H2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('I2').value = 'Ogo';
    worksheet.getCell('I2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('J2').value = 'Sep';
    worksheet.getCell('J2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('K2').value = 'Okt';
    worksheet.getCell('K2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('L2').value = 'Nov';
    worksheet.getCell('L2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('M2').value = 'Dis';
    worksheet.getCell('M2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
    worksheet.getCell('N2').value = 'Jumlah';
    worksheet.getCell('N2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

    worksheet.columns = [
      { key: 'Tempat', width: 30, style: { alignment: { horizontal: 'left', vertical: 'top', wrapText: true } } },
      { key: 's1', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 's2', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 's3', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 's4', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 's5', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 's6', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 's7', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 's8', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 's9', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 's10', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 's11', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 's12', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
      { key: 'Jumlah', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } },
    ];

    for (let record of this.ReportList) {
      worksheet.addRow([
        record.Tempat,
        record.s1,
        record.s2,
        record.s3,
        record.s4,
        record.s5,
        record.s6,
        record.s7,
        record.s8,
        record.s9,
        record.s10,
        record.s11,
        record.s12,
        record.Jumlah,])
    }

    worksheet.eachRow(function (row, _rowNumber) {
      row.eachCell(function (cell, _colNumber) {
        cell.border = {
          top: { style: 'thin' },
          left: { style: 'thin' },
          bottom: { style: 'thin' },
          right: { style: 'thin' }
        };
      });
    });



    let idx = 1;
    for (var key2 in this.AktaList) {
      worksheet = workbook.addWorksheet('Akta ' + idx);
      this.laporanService.getSeksyenByAktaList(this.AktaList[key2].id, this.form.value).subscribe(items => {
        //this.ListSeksyenByAkta[this.AktaList[key2].id+'_'] = items.list;
        //this.JumlahSeksyenByAkta[this.AktaList[key2].id+'_'] = items.jumlah;
        let SeksyenList = items.list;
        worksheet.mergeCells('A1', this.pageSetting.printToLetter(SeksyenList.length + 1) + '1'); //title merge cell
        worksheet.getCell('A1').value = this.AktaList[key2].name; //title
        worksheet.getCell('A1').alignment = { horizontal: 'left', vertical: 'top', wrapText: true };

        let columnNumber = 1;
        for (var key3 in SeksyenList) {
          this.pageSetting.printToLetter(columnNumber);
          worksheet.getCell(this.pageSetting.printToLetter(columnNumber) + '2').value = SeksyenList[key3].code;
          worksheet.getCell(this.pageSetting.printToLetter(columnNumber) + '2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };
          columnNumber++;
        }
        worksheet.getCell(this.pageSetting.printToLetter(columnNumber) + '2').value = 'Jumlah';
        worksheet.getCell(this.pageSetting.printToLetter(columnNumber) + '2').alignment = { horizontal: 'center', vertical: 'top', wrapText: true };

        let columsArr = []
        let keyIdx = 1;
        for (let rec in SeksyenList) {
          columsArr.push({ key: 'code' + keyIdx, width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } });
          keyIdx++;
        }
        columsArr.push({ key: 'Jumlah', width: 15, style: { alignment: { horizontal: 'center', vertical: 'top', wrapText: true } } });
        worksheet.columns = columsArr;

        let columsJumlahValue = [];
        for (let recPeruntukan in SeksyenList) {
          columsJumlahValue.push(SeksyenList[recPeruntukan].count);
        }
        columsJumlahValue.push(items.jumlah);
        worksheet.addRow(columsJumlahValue);

      });
      idx++;
      worksheet.eachRow(function (row, _rowNumber) {
        row.eachCell(function (cell, _colNumber) {
          cell.border = {
            top: { style: 'thin' },
            left: { style: 'thin' },
            bottom: { style: 'thin' },
            right: { style: 'thin' }
          };
        });
      });
    }



    workbook.xlsx.writeBuffer().then(data => {
      const blob = new Blob([data], { type: this.blobType });
      //create excell file
      FileSaver.saveAs(blob, this.ReportName + '.xlsx');
    });
  }




}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanIndividuPenguatkuasaListComponent } from './list.component';

describe('LaporanIndividuPenguatkuasaListComponent', () => {
  let component: LaporanIndividuPenguatkuasaListComponent;
  let fixture: ComponentFixture<LaporanIndividuPenguatkuasaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanIndividuPenguatkuasaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanIndividuPenguatkuasaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

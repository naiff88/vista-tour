import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { LaporanService } from '../../laporan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatDatepicker } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Moment } from 'moment';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');
import { PopupSearchPenguatkuasaListComponent } from '../../../popup-search/penguatkuasa/list/list.component';

@Component({
  selector: 'app-laporan-individu-penguatkuasa-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})


export class LaporanIndividuPenguatkuasaListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  showList: boolean = false;
  eventSearch: any;
  onSearch: boolean = false;
  submitted: boolean = false;
  form: FormGroup;
  //close standard var for searching page

  //initiate list
  KawasanList: any = [];
  JalanList: any = [];

  //initiate list model
  Kawasan: number = 0;
  Jalan: number = 0;
  IdPenguatkuasa: number = 0;

  //initiate date model
  TarikhMulaLaporan: any = '';
  TarikhAkhirLaporan: any = '';

  //dynamic report param
  TarikhMula: any = '';
  TarikhAkhir: any = '';
  Penguatkuasa: string = '';

  
  reportCol: any = [
    { name: 'index', title: 'No', width: '5', align: 'center' },
    { name: 'NoNotis', title: 'No. Notis', width: '15', align: 'left' },
    { name: 'Jenis', title: 'Jenis', width: '20', align: 'left' },
    { name: 'TarikhKesalahan', title: 'Tarikh Kesalahan', width: '15', align: 'left' },
    { name: 'NamaPesalah', title: 'Nama Pesalah', width: '20', align: 'left' },
    { name: 'NoKenderaan', title: 'No. Kenderaan', width: '15', align: 'left' },
    { name: 'NoCukaiJalan', title: 'No. Cukai Jalan', width: '15', align: 'left' },
    { name: 'Peruntukan', title: 'Peruntukan Undang - Undang', width: '30', align: 'left' },
    { name: 'Seksyen', title: 'Seksyen Kesalahan', width: '15', align: 'left' },
    { name: 'Tempat', title: 'Tempat / Lokasi Kesalahan', width: '20', align: 'left' },
    { name: 'NoPetak', title: 'No. Petak / Tg', width: '15', align: 'left' },
  ];

  //report title
  DisplayDate: any = '';
  ReportTitle: string = 'Laporan Prestasi Individu Penguatkuasa';
  ReportName = this.ReportTitle;

  //for excel ganerator 
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  //standard contructor
  constructor(
    private laporanService: LaporanService,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  //form
  get f() { return this.form.controls; }

  //init methode to call default function
  ngOnInit() {
    this.getLaporanIndividuPenguatkuasaList();
    //this.getLaporanIndividuPenguatkuasaTypeList();
    this.getKawasanList();
    this.getJalanList();

    this.loadSearchDetails();
  }

  loadSearchDetails() {
    this.form = this.buildFormItems();
  }

  onChangeKawasan() {
    this.getJalanList();
  }

  searchPenguatkuasa() {
    event.stopPropagation();
    const data = {};
    const dialogRef = this.dialog.open(PopupSearchPenguatkuasaListComponent, { data });
    dialogRef.afterClosed()
      .subscribe(res => {
        if(res)
        {
          this.IdPenguatkuasa = res.Id; 
          this.Penguatkuasa = res.NoBadan + ' ' + res.Pangkat + ' ' + res.Nama; 
          this.Penguatkuasa  = this.Penguatkuasa.toUpperCase(); 
        }     
      }
      );
  }
    
  //open standard listing function
  getLaporanIndividuPenguatkuasaList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    this.laporanService.getLaporanIndividuPenguatkuasaList(this.pageSetting.getPagerSetting(this.paginator, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.Kawasan, this.Jalan, this.IdPenguatkuasa)
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (this.onSearch) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    this.primengTableHelper.showLoadingIndicator();
  }
  //close standard listing function

  //open default listing function
  getKawasanList() {
    this.laporanService.getKawasanList().subscribe(items => {
      this.KawasanList = items;
    });
  }
  getJalanList() {
    this.laporanService.getJalanList(this.Kawasan).subscribe(items => {
      this.JalanList = items;
    });
  }
  //close default listing function

  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      this.showList = true;
      let KawasanObject = this.KawasanList.find(i => i.id == this.Kawasan);
      let JalanObject = this.JalanList.find(i => i.id == this.Jalan);
      this.DisplayDate = moment(searchForm.TarikhMulaLaporan).format("DD/MM/YYYY") + ' - ' + moment(searchForm.TarikhAkhirLaporan).format("DD/MM/YYYY");
      this.ReportTitle = 'LAPORAN PRESTASI INDIVIDU PENGUATKUASA ' + this.Penguatkuasa + ' BAGI ' + this.DisplayDate;
      this.ReportTitle = this.ReportTitle + ' KAWASAN - ' + (KawasanObject ? KawasanObject.name.toUpperCase() + ' - ' : '') + (JalanObject ? JalanObject.name.toUpperCase() : '');
      this.ReportTitle = this.ReportTitle.toUpperCase();
      this.onSearch = true;
      this.getLaporanIndividuPenguatkuasaList();
      this.paginator.changePage(0);
    }
  }


  reset() {
    this.form = this.buildFormItems();
    this.Kawasan = 0;
    this.Jalan = 0;
    this.IdPenguatkuasa = 0;
    this.TarikhMula = '';
    this.TarikhAkhir = '';
    this.dataSource.data = [];
    this.primengTableHelper.totalRecordsCount = 0;
    this.primengTableHelper.records = [];
    this.showList = false;
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      if(formGroup.dirty)
      {
        this.showList = false;
      }  
      
      const TarikhMulaLaporan = formGroup.controls['TarikhMulaLaporan'];
      const TarikhAkhirLaporan = formGroup.controls['TarikhAkhirLaporan'];
      const Kawasan = formGroup.controls['Kawasan'];
      const Jalan = formGroup.controls['Jalan'];
      const IdPenguatkuasa = formGroup.controls['IdPenguatkuasa'];
      const Penguatkuasa = formGroup.controls['Penguatkuasa'];
      if (TarikhMulaLaporan) {
        TarikhMulaLaporan.setErrors(null);
        if (!TarikhMulaLaporan.value) {
          TarikhMulaLaporan.setErrors({ required: true });
        }
        else if (TarikhMulaLaporan.value && TarikhAkhirLaporan.value && TarikhAkhirLaporan.value < TarikhMulaLaporan.value) {
          TarikhMulaLaporan.setErrors({ exceed: true });
        }
      }
      if (TarikhAkhirLaporan) {
        TarikhAkhirLaporan.setErrors(null);
        if (!TarikhAkhirLaporan.value) {
          TarikhAkhirLaporan.setErrors({ required: true });
        }
        else if (TarikhMulaLaporan.value && TarikhAkhirLaporan.value && TarikhAkhirLaporan.value < TarikhMulaLaporan.value) {
          TarikhAkhirLaporan.setErrors({ exceed: true });
        }
      }
      if (Kawasan) {
        Kawasan.setErrors(null);
        if (!Kawasan.value) {
          Kawasan.setErrors({ required: true });
        }
        else if (Kawasan.value == 0) {
          Kawasan.setErrors({ min: true });
        }
      }
      if (Jalan) {
        Jalan.setErrors(null);
        if (!Jalan.value) {
          Jalan.setErrors({ required: true });
        }
        else if (Jalan.value == 0) {
          Jalan.setErrors({ min: true });
        }
      }
      if (IdPenguatkuasa) {
        IdPenguatkuasa.setErrors(null);
        if (!IdPenguatkuasa.value) {
          IdPenguatkuasa.setErrors({ required: true });
        } else if (IdPenguatkuasa.value == 0) {
          IdPenguatkuasa.setErrors({ min: true });
        }
      }
      if (Penguatkuasa) {
        Penguatkuasa.setErrors(null);
        if (!Penguatkuasa.value) {
          Penguatkuasa.setErrors({ required: true });
        }
      }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      TarikhMulaLaporan: ['', []],
      TarikhAkhirLaporan: ['', []],
      Kawasan: ['0', []],
      Jalan: ['0', []],
      IdPenguatkuasa: ['0', []],
      Penguatkuasa: ['', []],
    }, { validator: this.customValidation() }
    );
  }




  //excel genarator
  printTableExcel() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getLaporanIndividuPenguatkuasaList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.Kawasan, this.Jalan, this.IdPenguatkuasa).subscribe(items => {
        list = items.list;
        this.pageSetting.excelGenerator(this.reportCol, list, this.ReportTitle, this.ReportTitle).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }
  //pdf genarator
  printTablePdf() {
    this.showSpinner = true;
    let list: any;
    this.laporanService.getLaporanIndividuPenguatkuasaList(this.pageSetting.getPagerSetting(null, event, this.primengTableHelper.getSorting(this.dataTable)),
      this.TarikhMula, this.TarikhAkhir, this.Kawasan, this.Jalan, this.IdPenguatkuasa).subscribe(items => {
        list = items.list;
        this.pageSetting.pdfGenerator(this.reportCol, list, this.ReportTitle, this.ReportTitle, 'p', 7, 20).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }

}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MenuComponent } from '../dashboard/menu/menu.component';
import { LaporanUtamaListComponent } from './utama/list/list.component';
import { LaporanDdeListComponent } from './dde/list/list.component';
import { LaporanBahagianZonUnitListComponent } from './bahagian-zon-unit/list/list.component';
import { LaporanIndividuPenguatkuasaListComponent } from './individu-penguatkuasa/list/list.component';
import { LaporanPembatalanNotisListComponent } from './pembatalan-notis/list/list.component';
import { LaporanNotisTanpaAlamatListComponent } from './notis-tanpa-alamat/list/list.component';
import { LaporanSamanTanpaAlamatListComponent } from './saman-tanpa-alamat/list/list.component';
import { LaporanBayarNotisBelumWujudListComponent } from './bayar-notis-belum-wujud/list/list.component';
import { LaporanPembayaranNotisListComponent } from './pembayaran-notis/list/list.component';
import { LaporanSenaraiHitamListComponent } from './senarai-hitam/list/list.component';
import { LaporanUpliftListComponent } from './uplift/list/list.component';
import { LaporanSlkeListComponent } from './slke/list/list.component';
import { LaporanJenisKenderaanListComponent } from './jenis-kenderaan/list/list.component';
import { LaporanTindakanMahkamahListComponent } from './tindakan-mahkamah/list/list.component';
import { LaporanStatistikTindakanListComponent } from './statistik-tindakan/list/list.component';


@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MenuComponent,

                children: [
                    { path: '', pathMatch: 'full', redirectTo: 'laporan-utama' },
                    { path: 'laporan-utama/:fromMenu', component: LaporanUtamaListComponent },
                    { path: 'laporan-dde', component: LaporanDdeListComponent },
                    { path: 'laporan-bahagian-zon-unit', component: LaporanBahagianZonUnitListComponent },
                    { path: 'laporan-individu-penguatkuasa', component: LaporanIndividuPenguatkuasaListComponent },
                    { path: 'laporan-bahagian-zon-unit/:fromMenu', component: LaporanBahagianZonUnitListComponent },
                    { path: 'laporan-pembatalan-notis', component: LaporanPembatalanNotisListComponent },
                    { path: 'laporan-notis-tanpa-alamat', component: LaporanNotisTanpaAlamatListComponent },
                    { path: 'laporan-saman-tanpa-alamat', component: LaporanSamanTanpaAlamatListComponent },
                    { path: 'laporan-bayar-notis-belum-wujud', component: LaporanBayarNotisBelumWujudListComponent },
                    { path: 'laporan-pembayaran-notis', component: LaporanPembayaranNotisListComponent },
                    { path: 'laporan-senarai-hitam', component: LaporanSenaraiHitamListComponent },
                    { path: 'laporan-uplift', component: LaporanUpliftListComponent },
                    { path: 'laporan-slke', component: LaporanSlkeListComponent },
                    { path: 'laporan-jenis-kenderaan', component: LaporanJenisKenderaanListComponent },
                    { path: 'laporan-tindakan-mahkamah', component: LaporanTindakanMahkamahListComponent },
                    { path: 'laporan-statistik-tindakan', component: LaporanStatistikTindakanListComponent },
                    
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})

export class LaporanRoutingModule { }

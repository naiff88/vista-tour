import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanNotisTanpaAlamatListComponent } from './list.component';

describe('LaporanNotisTanpaAlamatListComponent', () => {
  let component: LaporanNotisTanpaAlamatListComponent;
  let fixture: ComponentFixture<LaporanNotisTanpaAlamatListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanNotisTanpaAlamatListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanNotisTanpaAlamatListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

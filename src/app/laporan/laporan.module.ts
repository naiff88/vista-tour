import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LaporanRoutingModule } from './laporan-routing.module';
import { MatRippleModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar} from '@angular/material/snack-bar';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule} from '@angular/material/dialog';
import { MatIconModule} from '@angular/material/icon';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';


import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { JoditAngularModule } from 'jodit-angular';
import { AppCommonModule } from '../shared/app-common.module';
import { QuillModule } from 'ngx-quill';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MiscModule } from '../misc/misc.module';
import { PMServiceProxy } from 'src/shared/service-proxies/service-proxies';
import { getQuillEditorSettings } from 'src/shared/helpers/quill-helper';
import { NgxPrintModule } from 'ngx-print';
import { DashboardModule } from '../dashboard/dashboard.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { LaporanUtamaListComponent } from './utama/list/list.component';
import { LaporanDdeListComponent } from './dde/list/list.component';
import { LaporanBahagianZonUnitListComponent } from './bahagian-zon-unit/list/list.component';
import { LaporanIndividuPenguatkuasaListComponent } from './individu-penguatkuasa/list/list.component';
import { PopupSearchPenguatkuasaListComponent } from '../popup-search/penguatkuasa/list/list.component';
import { LaporanPembatalanNotisListComponent } from './pembatalan-notis/list/list.component';
import { LaporanNotisTanpaAlamatListComponent } from './notis-tanpa-alamat/list/list.component';
import { LaporanSamanTanpaAlamatListComponent } from './saman-tanpa-alamat/list/list.component';
import { LaporanBayarNotisBelumWujudListComponent } from './bayar-notis-belum-wujud/list/list.component';
import { LaporanPembayaranNotisListComponent } from './pembayaran-notis/list/list.component';
import { LaporanSenaraiHitamListComponent } from './senarai-hitam/list/list.component';
import { LaporanUpliftListComponent } from './uplift/list/list.component';
import { LaporanSlkeListComponent } from './slke/list/list.component';
import { LaporanJenisKenderaanListComponent } from './jenis-kenderaan/list/list.component';
import { LaporanTindakanMahkamahListComponent } from './tindakan-mahkamah/list/list.component';
import { LaporanStatistikTindakanListComponent } from './statistik-tindakan/list/list.component';

const materialModules = [
  MatButtonModule,
  MatTableModule,
  MatCheckboxModule,
  MatDialogModule,
  MatSortModule,
  MatTabsModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatProgressBarModule,
  MatIconModule,
  MatSnackBarModule,
  MAT_DIALOG_DATA,
  MatDialogRef
];

@NgModule({
  declarations: [
    LaporanUtamaListComponent,
    LaporanDdeListComponent,
    LaporanBahagianZonUnitListComponent,
    LaporanIndividuPenguatkuasaListComponent,
    PopupSearchPenguatkuasaListComponent,
    LaporanPembatalanNotisListComponent,
    LaporanNotisTanpaAlamatListComponent,
    LaporanSamanTanpaAlamatListComponent,
    LaporanBayarNotisBelumWujudListComponent,
    LaporanPembayaranNotisListComponent,
    LaporanSenaraiHitamListComponent,
    LaporanUpliftListComponent,
    LaporanSlkeListComponent,
    LaporanJenisKenderaanListComponent,
    LaporanTindakanMahkamahListComponent,
    LaporanStatistikTindakanListComponent
  ],
  imports: [
    DashboardModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppCommonModule,
    JoditAngularModule,
    LaporanRoutingModule,
    QuillModule.forRoot(getQuillEditorSettings()),
    TableModule,
    PaginatorModule,
    ModalModule.forRoot(),
    MatDatepickerModule,
    MatMomentDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSortModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatIconModule,
    MatSnackBarModule,
    MiscModule,
    NgxPrintModule,
    MatRadioModule,
    PerfectScrollbarModule,
    NgMultiSelectDropDownModule,
    BsDropdownModule.forRoot(),
  ],
  entryComponents: [
  ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    { provide: PMServiceProxy },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
  ]
})
export class LaporanModule {
}

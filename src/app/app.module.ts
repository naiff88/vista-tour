import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector, APP_INITIALIZER, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar} from '@angular/material/snack-bar';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule} from '@angular/material/dialog';
import { MatIconModule} from '@angular/material/icon';

import { MatTabsModule } from '@angular/material/tabs';
import { ModalModule } from 'ngx-bootstrap';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LayoutModule } from './shared/layout/layout.module';
import { ServiceProxyModule } from '../shared/service-proxies/service-proxy.module';
import { SharedService } from '../shared/shared-service';
import { AppCommonModule } from './shared/app-common.module';
import { CommonModule } from '@angular/common';
// import {TableModule} from 'primeng/table';
import { ApiInterceptor } from '../shared/interceptors/api.interceptor';
import { JoditAngularModule } from 'jodit-angular';
import { ToastrModule } from 'ngx-toastr';
import { GlobalErrorHandlerService } from './global-error-handeler.service';
import { ImageCropperModule } from 'ngx-image-cropper';
import { Router, NavigationEnd } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import { AuthInterceptor } from 'src/shared/helpers/auth.interceptor';
import { QuillModule } from 'ngx-quill';
// import { OrderListComponent } from './sales/orders/list/list.component';
// import { MatSpinnerOverlayComponent } from 'src/app/shared/common/spinner/spinner.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
//import { TwoDigitDecimaNumberDirective } from './shared/validators/currency-input';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

// import {
//   AppAsideModule,
//   AppBreadcrumbModule,
//   AppHeaderModule,
//   AppFooterModule,
//   AppSidebarModule,
// } from '@coreui/angular';



@NgModule({
  declarations: [
    AppComponent,
    ConfirmationDialogComponent,
    //TwoDigitDecimaNumberDirective,
    // MatSpinnerOverlayComponent
    // OrderListComponent,
  ],
  imports: [
    // OrderListComponent,
    // AppAsideModule,
    // AppBreadcrumbModule.forRoot(),
    // AppFooterModule,
    // AppHeaderModule,
    // AppSidebarModule,
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    JoditAngularModule,
    ImageCropperModule,
    MatButtonModule,
    HttpClientModule,
    MatCheckboxModule,
    MatSnackBarModule,
    ModalModule.forRoot(),
    LayoutModule,
    ServiceProxyModule,
    ToastrModule.forRoot(),
    AppCommonModule.forRoot(),
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    // MatSpinnerOverlayComponent
    PerfectScrollbarModule,
    NgMultiSelectDropDownModule.forRoot(),
    //TwoDigitDecimaNumberDirective

    // NgbModule.forRoot()
    // TableModule,
  ],
  exports: [
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    //TwoDigitDecimaNumberDirective,
    //TwoDigitDecimaNumberDirective,
    // MatProgressSpinnerModule,
    // MatSpinnerOverlayComponent
  ],
  bootstrap: [
    AppComponent,
  ],
  providers: [

    { provide: ErrorHandler, useClass: GlobalErrorHandlerService },
    SharedService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  entryComponents: [ConfirmationDialogComponent, ]
})
export class AppModule {
  constructor(private _router: Router) {
    _router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        mLayout.init();
        document.querySelectorAll('.m-portlet__head-icon.back').forEach(element => {
          element.addEventListener('click', e => {
            window.history.back();
          });
        });
      }
    });
  }
}

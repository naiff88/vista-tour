/* tslint:disable:max-line-length */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EsitaanRoutingModule } from './esitaan-routing.module';


import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar} from '@angular/material/snack-bar';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule} from '@angular/material/dialog';
import { MatIconModule} from '@angular/material/icon';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { JoditAngularModule } from 'jodit-angular';
import { AppCommonModule } from '../shared/app-common.module';
import { QuillModule } from 'ngx-quill';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MiscModule } from '../misc/misc.module';
import { PMServiceProxy } from 'src/shared/service-proxies/service-proxies';
import { getQuillEditorSettings } from 'src/shared/helpers/quill-helper';
import { NgxPrintModule } from 'ngx-print';
// import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PenerimaanListComponent } from './penerimaan/list/list.component';
import { PenerimaanDetailsComponent } from './penerimaan/penerimaan-details/penerimaan-details.component';
import { AddBarangComponent } from './penerimaan/modal/add-barang/add-barang.component';
import { TuntutanListComponent } from './tuntutan/list/list.component';
import { TuntutanDetailsComponent } from './tuntutan/tuntutan-details/tuntutan-details.component';
import { PelupusanListComponent } from './pelupusan/list/list.component';
import { PelupusanDetailsComponent } from './pelupusan/pelupusan-details/pelupusan-details.component';
// import { KumpulanListComponent } from './kumpulan/list/list.component';
// import { KumpulanDetailsComponent } from './kumpulan/kumpulan-details/kumpulan-details.component';
// import { PilihAhliComponent } from './kumpulan/modal/pilih-ahli/pilih-ahli.component';
// import { PenggunaListComponent } from './pengguna/list/list.component';
// import { PenggunaDetailsComponent } from './pengguna/pengguna-details/pengguna-details.component';
// import { AktaIndukListComponent } from './data-induk/akta-induk/list/list.component';
// import { AktaIndukDetailsComponent } from './data-induk/akta-induk/akta-induk-details/akta-induk-details.component';
// import { PeruntukanUndangListComponent } from './data-induk/peruntukan-undang/list/list.component';
// import { PeruntukanUndangDetailsComponent } from './data-induk/peruntukan-undang/peruntukan-undang-details/peruntukan-undang-details.component';
// import { SeksyenKesalahanListComponent } from './data-induk/seksyen-kesalahan/list/list.component';
// import { SeksyenKesalahanDetailsComponent } from './data-induk/seksyen-kesalahan/seksyen-kesalahan-details/seksyen-kesalahan-details.component';
// import { CarialIndukListComponent } from './data-induk/carian-data-induk/list/list.component';
// import { CarianIndukDetailsComponent } from './data-induk/carian-data-induk/carian-induk-details/carian-induk-details.component';
// import { JenamaKenderaanListComponent } from './data-induk/kenderaan/jenama-kenderaan/list/list.component';
// import { JenamaKenderaanDetailsComponent } from './data-induk/kenderaan/jenama-kenderaan/jenama-kenderaan-details/jenama-kenderaan-details.component';
// import { ModelKenderaanListComponent } from './data-induk/kenderaan/model-kenderaan/list/list.component';
// import { ModelKenderaanDetailsComponent } from './data-induk/kenderaan/model-kenderaan/model-kenderaan-details/model-kenderaan-details.component';
// import { JenisKenderaanListComponent } from './data-induk/kenderaan/jenis-kenderaan/list/list.component';
// import { JenisKenderaanDetailsComponent } from './data-induk/kenderaan/jenis-kenderaan/jenis-kenderaan-details/jenis-kenderaan-details.component';
// import { ZonPegawaiListComponent } from './data-induk/kawasan/zon-pegawai/list/list.component';
// import { ZonePegawaiDetailsComponent } from './data-induk/kawasan/zon-pegawai/zone-pegawai-details/zone-pegawai-details.component';
// import { ParlimenListComponent } from './data-induk/kawasan/parlimen/list/list.component';
// import { ParlimenDetailsComponent } from './data-induk/kawasan/parlimen/parlimen-details/parlimen-details.component';
// import { LokasiListComponent } from './data-induk/kawasan/lokasi/list/list.component';
// import { LokasiDetailsComponent } from './data-induk/kawasan/lokasi/lokasi-details/lokasi-details.component';
// import { KawasanKesalahanListComponent } from './data-induk/kawasan/kawasan-kesalahan/list/list.component';
// import { KawasanDetailsComponent} from './data-induk/kawasan/kawasan-kesalahan/kawasan-kesalahan-details/kawasan-details.component';
// import { PangkatPegawaiListComponent } from './data-induk/pangkat-pegawai/list/list.component';
// import { PangkatPegawaiDetailsComponent } from './data-induk/pangkat-pegawai/pangkat-pegawai-details/pangkat-pegawai-details.component';
// import { IklanListComponent } from './data-induk/iklan/list/list.component';
// import { IklanDetailsComponent } from './data-induk/iklan/iklan-details/iklan-details.component';
// import { PengumumanListComponent } from './pengumuman/list/list.component';
// import { PengumumanDetailsComponent } from './pengumuman/pengumuman-details/pengumuman-details.component';
// import { AuditListComponent } from './audit/list/list.component';
// import { ViewAuditComponent } from './audit/modal/view-audit/view-audit.component';
// import { KompaunDiskaunListComponent } from './kompaun-diskaun/list/list.component';
// import { JenisKenderaanDiskaunComponent } from './kompaun-diskaun/kompaun-diskaun-details/jenis-kenderaan/jenis-kenderaan-diskaun.component';
// import { UmumDiskaunComponent } from './kompaun-diskaun/kompaun-diskaun-details/umum/umum-diskaun.component';
// import { AwalDiskaunComponent } from './kompaun-diskaun/kompaun-diskaun-details/bayaran-awal/awal-diskaun.component';



const materialModules = [
  MatButtonModule,
  MatTableModule,
  MatCheckboxModule,
  MatDialogModule,
  MatSortModule,
  MatTabsModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatProgressBarModule,
  MatIconModule,
  MatSnackBarModule,
  MAT_DIALOG_DATA,
  MatDialogRef
];

@NgModule({
  declarations: [
    PenerimaanListComponent,
    PenerimaanDetailsComponent,
    AddBarangComponent,
    TuntutanListComponent,
    TuntutanDetailsComponent,
    PelupusanListComponent,
    PelupusanDetailsComponent
    // KumpulanListComponent,
    // KumpulanDetailsComponent,
    // PilihAhliComponent,
    // PenggunaListComponent,
    // PenggunaDetailsComponent,
    // AktaIndukListComponent,
    // AktaIndukDetailsComponent,
    // PeruntukanUndangListComponent,
    // PeruntukanUndangDetailsComponent,
    // SeksyenKesalahanListComponent,
    // SeksyenKesalahanDetailsComponent,
    // CarialIndukListComponent,
    // CarianIndukDetailsComponent,
    // JenamaKenderaanListComponent,
    // JenamaKenderaanDetailsComponent,
    // ModelKenderaanListComponent,
    // ModelKenderaanDetailsComponent,
    // JenisKenderaanListComponent,
    // JenisKenderaanDetailsComponent,
    // ZonPegawaiListComponent,
    // ZonePegawaiDetailsComponent,
    // ParlimenListComponent,
    // ParlimenDetailsComponent,
    // LokasiListComponent,
    // LokasiDetailsComponent,
    // KawasanKesalahanListComponent,
    // KawasanDetailsComponent,
    // PangkatPegawaiListComponent,
    // PangkatPegawaiDetailsComponent,
    // IklanListComponent,
    // IklanDetailsComponent,
    // PengumumanListComponent,
    // PengumumanDetailsComponent,
    // AuditListComponent,
    // ViewAuditComponent,
    // KompaunDiskaunListComponent,
    // JenisKenderaanDiskaunComponent,
    // UmumDiskaunComponent,
    // AwalDiskaunComponent
    // CategoryListComponent,
    // ProductDetailsComponent,
    // CategoryDetailsComponent,
    // Variation1Component,
    // Variation2Component
    // FinanceComponent,
    // DashboardComponent,
    // CustomerListComponent,
    // CustomerDetailsComponent,
    // CustomerInfoComponent,
    // QuotationListComponent,
    // QuotationDetailsComponent,
    // QuotationInfoComponent,
    // QuotationViewComponent,
    // QuotationPreviewComponent,
    // QuotationSendComponent,
    // QuotationItemComponent,
    // InvoiceListComponent,
    // InvoiceDetailsComponent,
    // InvoiceInfoComponent,
    // InvoiceViewComponent,
    // InvoiceNewItemComponent,
    // InvoicePreviewComponent,
    // InvoiceSendComponent,
    // InvoiceNewPaymentComponent,
    // InvoiceViewReceiptComponent
  ],
  imports: [
    DashboardModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppCommonModule,
    JoditAngularModule,
    EsitaanRoutingModule,
    QuillModule.forRoot(getQuillEditorSettings()),
    TableModule,
    PaginatorModule,
    ModalModule.forRoot(),
    MatDatepickerModule,
    MatMomentDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSortModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatIconModule,
    MatSnackBarModule,
    MiscModule,
    NgxPrintModule,
    MatRadioModule,
    PerfectScrollbarModule,
    NgMultiSelectDropDownModule,
    BsDropdownModule.forRoot(),
  ],
  entryComponents: [
    AddBarangComponent
    // PilihAhliComponent,
    // ViewAuditComponent
    // Variation1Component,
    // Variation2Component
    // CustomerDetailsComponent,
    // InvoiceNewItemComponent,
    // InvoicePreviewComponent,
    // InvoiceNewPaymentComponent,
    // InvoiceViewReceiptComponent
  ],
  // providers: [PMServiceProxy],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    { provide: PMServiceProxy }
  ]
})
export class EsitaanModule {
}

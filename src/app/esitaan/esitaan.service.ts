/* tslint:disable:max-line-length comment-format no-trailing-whitespace */
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import { bufferTime, distinctUntilChanged, filter, map, scan, share, take, tap } from 'rxjs/operators';
import * as _ from 'lodash';
import { Pager, PageInfo } from '../../pagingnation';
import { API_BASE_URL } from 'src/shared/service-proxies/service-proxies';
import { ProgressBarState, Pair } from './models';
import * as moment from 'moment';

export enum EsitaanApiEndpoint {
  UploadImage = '/api/',
  DeleteImage = '/api/',
}

@Injectable({
  providedIn: 'root'
})
export class EsitaanService {
  /**
   * Indicate data processing. Binded in dialog
   */
  private processingSubject = new Subject<ProgressBarState>();
  private baseUrl: string;
  http: HttpClient;


  /**
   *  Pipeline of async operation to handle loading state of product dialog
   */
  isBusyStream: Observable<boolean> = this.processingSubject.asObservable()
    .pipe(
      bufferTime(300),                  // buffers the progress Observable values for 100ms period
      filter(states => states.length > 0),   // ignore empty streams
      scan((total: number, states: ProgressBarState[]) => total + _.sum(states), 0),
      map((diff: number) => diff !== 0),       // diff between count of processing and already completed http requests
      distinctUntilChanged(),
      share()                                         // Returns a new Observable that multicasts (shares) the original Observable
    );

  set isBusy(value: boolean) {
    const state: number = value ? ProgressBarState.Show : ProgressBarState.Hide;
    this.processingSubject.next(state);
  }

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }

//..........................................................................................................................
//.PPPPPPPPP...PEEEEEEEEEE.ENNN...NNNN..NEEEEEEEEEE.ERRRRRRRRR..RRIIIIIMMMM...MMMMMM....AAAAA........AAAAA.....ANNN...NNNN..
//.PPPPPPPPPP..PEEEEEEEEEE.ENNNN..NNNN..NEEEEEEEEEE.ERRRRRRRRRR.RRIIIIIMMMM...MMMMMM....AAAAA........AAAAA.....ANNNN..NNNN..
//.PPPPPPPPPPP.PEEEEEEEEEE.ENNNN..NNNN..NEEEEEEEEEE.ERRRRRRRRRR.RRIIIIIMMMM...MMMMMM...AAAAAA.......AAAAAA.....ANNNN..NNNN..
//.PPPP...PPPP.PEEE........ENNNNN.NNNN..NEEE........ERRR...RRRRRRRIIIIIMMMMM.MMMMMMM...AAAAAAA......AAAAAAA....ANNNNN.NNNN..
//.PPPP...PPPP.PEEE........ENNNNN.NNNN..NEEE........ERRR...RRRRRRRIIIIIMMMMM.MMMMMMM..AAAAAAAA.....AAAAAAAA....ANNNNN.NNNN..
//.PPPPPPPPPPP.PEEEEEEEEE..ENNNNNNNNNN..NEEEEEEEEE..ERRRRRRRRRR.RRIIIIIMMMMM.MMMMMMM..AAAAAAAA.....AAAAAAAA....ANNNNNNNNNN..
//.PPPPPPPPPP..PEEEEEEEEE..ENNNNNNNNNN..NEEEEEEEEE..ERRRRRRRRRR.RRIIIIIMMMMMMMMMMMMM..AAAA.AAAA....AAAA.AAAA...ANNNNNNNNNN..
//.PPPPPPPPP...PEEEEEEEEE..ENNNNNNNNNN..NEEEEEEEEE..ERRRRRRR....RRIIIIIMMMMMMMMMMMMM.MAAAAAAAAA...AAAAAAAAAA...ANNNNNNNNNN..
//.PPPP........PEEE........ENNNNNNNNNN..NEEE........ERRR.RRRR...RRIIIIIMMMMMMMMMMMMM.MAAAAAAAAAA..AAAAAAAAAAA..ANNNNNNNNNN..
//.PPPP........PEEE........ENNN.NNNNNN..NEEE........ERRR..RRRR..RRIIIIIMM.MMMMM.MMMM.MAAAAAAAAAA..AAAAAAAAAAA..ANNN.NNNNNN..
//.PPPP........PEEEEEEEEEE.ENNN..NNNNN..NEEEEEEEEEE.ERRR..RRRRR.RRIIIIIMM.MMMMM.MMMMMMAA....AAAA.AAAA....AAAA..ANNN..NNNNN..
//.PPPP........PEEEEEEEEEE.ENNN..NNNNN..NEEEEEEEEEE.ERRR...RRRRRRRIIIIIMM.MMMMM.MMMMMMAA.....AAAAAAAA.....AAAA.ANNN..NNNNN..
//.PPPP........PEEEEEEEEEE.ENNN...NNNN..NEEEEEEEEEE.ERRR....RRRRRRIIIIIMM.MMMMM.MMMMMMAA.....AAAAAAAA.....AAAA.ANNN...NNNN..
//..........................................................................................................................

  getPenerimaanDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */

    //  getProductGeneral(id: number): Observable<ProductGeneral> {
    //   const params = new HttpParams().set('productId', String(id));
    //   this.isBusy = true;
    //   return this.http.get<ProductGeneral>(this.baseUrl + ProductApiEndpoint.General, {params})
    //     .pipe(
    //       map((resp: any) => {
    //         const model = resp.basicInfo[0] as ProductGeneral;
    //         model.imageList = resp.imageList as ProductImage[];
    //         return model;
    //       }),
    //       tap({complete: () => this.isBusy = false})
    //     );
    // }

    let record: any = {
      // MAKLUMAT SITAAN
      noSd: '772',
      noBadan: '772',
      waktuKesalahan: '15:10',
      butiran: 'butiran',
      jalanId: 1,
      zonId: '1',
      tarikhKesalahan: '02 Jan 2020',
      peruntukanUndangId: '1',
      parlimenId: '1',
      seksyenId: '1',
      kawasanId: '1',
      penguatkuasaId: '1',
      // MAKLUMAT SAKSI
      penguatkuasaIdSaksi: '1',
      tarikhSaksi: '02 Jan 2020',
      noBadanSaksi: '772',
      // MAKLUMAT PEMILIK
      tarikhPemilik: '02 Jan 2020',
      negeriPemilik: 1,
      namaPemilik: '5656',
      noKpSyarikat: '452',
      catatanPemilik: 'catatan',
      alamatPemilik1: 'alamat1',
      alamatPemilik2: 'alamat2',
      alamatPemilik3: 'alamat3',
      poskodPemilik: '54000',
      // MAKLUMAT STOR
      penguatkuasaIdStor: '1',
      waktuTerimaStor: '15:10',
      tarikhTerimaStor: '02 Jan 2020',
      tarikhStor: '02 Jan 2020',
      noBadanStor: '584',
      catatanStor: 'catatan',
    };
    return of(record);
  }

  getPenerimaanBarangList(page: Pager): Observable<any> {
    console.log('getPenerimaanBarangList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, noSd: '772', namaPegawawi: 'Rizal Hashim', noBadan: '61677', namaPemilik: 'Hashim Manaf', noKpSyarikat: '999999999999', tarikhSitaan: '30/11/2020', status: 'Enable'},
    ];
    const pageInfo: any = { RowCount: 3 };
    return of({ list, pageInfo });

  }

  getSenaraiBarangList(page: Pager): Observable<any> {
    console.log('getSenaraiBarangList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, namaBarang: 'Ikan', jumlah: '100', catatan: 'Ikan Mahal', status: 'Enable'},
    ];
    const pageInfo: any = { RowCount: 3 };
    return of({ list, pageInfo });

  }

  saveAddBarang(model, id: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Barang Disimpan!';
    const Id = id;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deleteBarangList(ids: Array<{ id: number }>) {
    console.log('deleteBarangList ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Barang telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getNegeriList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Johor' },
      { id: 2, name: 'Kedah' },
      { id: 3, name: 'Pahang' },
      { id: 4, name: 'Perak' },
      { id: 5, name: 'Selangor' },
    ];
    return of(list);
  }

//..................................................................................................
//.TTTTTTTTTTTUUUU...UUUU..NNNN...NNNN..TTTTTTTTTTTUUUU...UUUU..TTTTTTTTTTT..AAAA.....AANNN...NNNN..
//.TTTTTTTTTTTUUUU...UUUU..NNNNN..NNNN..TTTTTTTTTTTUUUU...UUUU..TTTTTTTTTTT.AAAAAA....AANNN...NNNN..
//.TTTTTTTTTTTUUUU...UUUU..NNNNN..NNNN..TTTTTTTTTTTUUUU...UUUU..TTTTTTTTTTT.AAAAAA....AANNNN..NNNN..
//....TTTT....UUUU...UUUU..NNNNNN.NNNN.....TTTT....UUUU...UUUU.....TTTT.....AAAAAAA...AANNNNN.NNNN..
//....TTTT....UUUU...UUUU..NNNNNN.NNNN.....TTTT....UUUU...UUUU.....TTTT....AAAAAAAA...AANNNNN.NNNN..
//....TTTT....UUUU...UUUU..NNNNNNNNNNN.....TTTT....UUUU...UUUU.....TTTT....AAAAAAAA...AANNNNNNNNNN..
//....TTTT....UUUU...UUUU..NNNNNNNNNNN.....TTTT....UUUU...UUUU.....TTTT....AAAA.AAAA..AANNNNNNNNNN..
//....TTTT....UUUU...UUUU..NNNNNNNNNNN.....TTTT....UUUU...UUUU.....TTTT...TAAAAAAAAA..AANN.NNNNNNN..
//....TTTT....UUUU...UUUU..NNNNNNNNNNN.....TTTT....UUUU...UUUU.....TTTT...TAAAAAAAAAA.AANN.NNNNNNN..
//....TTTT....UUUU...UUUU..NNNN.NNNNNN.....TTTT....UUUU...UUUU.....TTTT..TTAAAAAAAAAA.AANN..NNNNNN..
//....TTTT....UUUUUUUUUUU..NNNN..NNNNN.....TTTT....UUUUUUUUUUU.....TTTT..TTAA....AAAA.AANN..NNNNNN..
//....TTTT.....UUUUUUUUU...NNNN..NNNNN.....TTTT.....UUUUUUUUU......TTTT..TTAA....AAAAAAANN...NNNNN..
//....TTTT......UUUUUUU....NNNN...NNNN.....TTTT......UUUUUUU.......TTTT.TTTAA.....AAAAAANN....NNNN..
//..................................................................................................

  getTuntutanDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */

    //  getProductGeneral(id: number): Observable<ProductGeneral> {
    //   const params = new HttpParams().set('productId', String(id));
    //   this.isBusy = true;
    //   return this.http.get<ProductGeneral>(this.baseUrl + ProductApiEndpoint.General, {params})
    //     .pipe(
    //       map((resp: any) => {
    //         const model = resp.basicInfo[0] as ProductGeneral;
    //         model.imageList = resp.imageList as ProductImage[];
    //         return model;
    //       }),
    //       tap({complete: () => this.isBusy = false})
    //     );
    // }

    let record: any = {
      // MAKLUMAT SITAAN
      noSd: '772',
      noBadan: '772',
      waktuKesalahan: '15:10',
      butiran: 'butiran',
      jalanId: 1,
      zonId: '1',
      tarikhKesalahan: '02 Jan 2020',
      peruntukanUndangId: '1',
      parlimenId: '1',
      seksyenId: '1',
      kawasanId: '1',
      penguatkuasaId: '1',
      // MAKLUMAT SAKSI
      penguatkuasaIdSaksi: '1',
      tarikhSaksi: '02 Jan 2020',
      noBadanSaksi: '772',
      // MAKLUMAT PEMILIK
      tarikhPemilik: '02 Jan 2020',
      negeriPemilik: 1,
      namaPemilik: '5656',
      noKpSyarikat: '452',
      catatanPemilik: 'catatan',
      alamatPemilik1: 'alamat1',
      alamatPemilik2: 'alamat2',
      alamatPemilik3: 'alamat3',
      poskodPemilik: '54000',
      // MAKLUMAT STOR
      penguatkuasaIdStor: '1',
      waktuTerimaStor: '15:10',
      tarikhTerimaStor: '02 Jan 2020',
      tarikhStor: '02 Jan 2020',
      noBadanStor: '584',
      catatanStor: 'catatan',
      // MAKLUMAT TUNTUTAN
      penguatkuasaIdDaftarTuntutan: '1',
      waktuDaftarTuntutan: '15:10',
      tarikhDaftarTuntutan: '02 Jan 2020',
      zonIdDaftarTuntutan: '1',
      noBadanDaftarTuntutan: '486',
      catatanDaftarTuntutan: 'catatan',
      // MAKLUMAT KELULUSAN
      penguatkuasaIdKelulusanTuntutan: '1',
      statusKelulusanTuntutan: 1,
      tarikhKelulusanTuntutan: '02 Jan 2020',
      tarikhBolehDituntut: '02 Jan 2020',
      noBadanKelulusanTuntutan: '845',
      catatanKelulusanTuntutan: 'catatan',
      tempohTahananKelulusanTuntutan: '11',
      // MAKLUMAT PENGELUARAN
      penguatkuasaIdKeluaran: '1',
      tarikhPengeluaran: '02 Jan 2020',
      noBadanPengeluaran: '845',
      catatanPengeluaran: 'catatan',
    };
    return of(record);
  }

  getTuntutanBarangList(page: Pager): Observable<any> {
    console.log('getTuntutanBarangList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, noSd: '772', noSiri: '514', namaPegawawi: 'Rizal Hashim', noBadan: '61677', namaPemilik: 'Hashim Manaf', noKpSyarikat: '999999999999', tarikhSitaan: '20/03/2020', tarikhDaftarTuntutan: '29/03/2020', status: 'Penerimaan'},
      { id: 2, noSd: '772', noSiri: '514', namaPegawawi: 'Rizal Hashim', noBadan: '61677', namaPemilik: 'Hashim Manaf', noKpSyarikat: '999999999999', tarikhSitaan: '20/03/2020', tarikhDaftarTuntutan: '29/03/2020', status: 'Tuntutan'},
      { id: 3, noSd: '772', noSiri: '514', namaPegawawi: 'Rizal Hashim', noBadan: '61677', namaPemilik: 'Hashim Manaf', noKpSyarikat: '999999999999', tarikhSitaan: '20/03/2020', tarikhDaftarTuntutan: '29/03/2020', status: 'Menunggu Keputusan'},
      { id: 4, noSd: '772', noSiri: '514', namaPegawawi: 'Rizal Hashim', noBadan: '61677', namaPemilik: 'Hashim Manaf', noKpSyarikat: '999999999999', tarikhSitaan: '20/03/2020', tarikhDaftarTuntutan: '29/03/2020', status: 'Diluluskan'},
      { id: 5, noSd: '772', noSiri: '514', namaPegawawi: 'Rizal Hashim', noBadan: '61677', namaPemilik: 'Hashim Manaf', noKpSyarikat: '999999999999', tarikhSitaan: '20/03/2020', tarikhDaftarTuntutan: '29/03/2020', status: 'Selesai Pengeluaran'},
      { id: 6, noSd: '772', noSiri: '514', namaPegawawi: 'Rizal Hashim', noBadan: '61677', namaPemilik: 'Hashim Manaf', noKpSyarikat: '999999999999', tarikhSitaan: '20/03/2020', tarikhDaftarTuntutan: '29/03/2020', status: 'Ditolak'},
    ];
    const pageInfo: any = { RowCount: 6 };
    return of({ list, pageInfo });

  }

  saveTuntutanDetails(model, id: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Tuntutan Disimpan!';
    const Id = id;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deleteTuntutanBarangList(ids: Array<{ id: number }>) {
    console.log('deleteTuntutanBarangList ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Tuntutan Barang telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

//................................................................................................................
//.PPPPPPPPP...PEEEEEEEEEE.ELLL.......LUUU...UUUU..UPPPPPPPP...PUUU...UUUU...SSSSSSS.......AAAAA.....ANNN...NNNN..
//.PPPPPPPPPP..PEEEEEEEEEE.ELLL.......LUUU...UUUU..UPPPPPPPPP..PUUU...UUUU..USSSSSSSS......AAAAA.....ANNNN..NNNN..
//.PPPPPPPPPPP.PEEEEEEEEEE.ELLL.......LUUU...UUUU..UPPPPPPPPPP.PUUU...UUUU..USSSSSSSSS....AAAAAA.....ANNNN..NNNN..
//.PPPP...PPPP.PEEE........ELLL.......LUUU...UUUU..UPPP...PPPP.PUUU...UUUU.UUSSS..SSSS....AAAAAAA....ANNNNN.NNNN..
//.PPPP...PPPP.PEEE........ELLL.......LUUU...UUUU..UPPP...PPPP.PUUU...UUUU.UUSSS.........AAAAAAAA....ANNNNN.NNNN..
//.PPPPPPPPPPP.PEEEEEEEEE..ELLL.......LUUU...UUUU..UPPPPPPPPPP.PUUU...UUUU..USSSSSS......AAAAAAAA....ANNNNNNNNNN..
//.PPPPPPPPPP..PEEEEEEEEE..ELLL.......LUUU...UUUU..UPPPPPPPPP..PUUU...UUUU...SSSSSSSSS...AAAA.AAAA...ANNNNNNNNNN..
//.PPPPPPPPP...PEEEEEEEEE..ELLL.......LUUU...UUUU..UPPPPPPPP...PUUU...UUUU.....SSSSSSS..SAAAAAAAAA...ANNNNNNNNNN..
//.PPPP........PEEE........ELLL.......LUUU...UUUU..UPPP........PUUU...UUUU........SSSSS.SAAAAAAAAAA..ANNNNNNNNNN..
//.PPPP........PEEE........ELLL.......LUUU...UUUU..UPPP........PUUU...UUUU.UUSS....SSSS.SAAAAAAAAAA..ANNN.NNNNNN..
//.PPPP........PEEEEEEEEEE.ELLLLLLLLL.LUUUUUUUUUU..UPPP........PUUUUUUUUUU.UUSSSSSSSSSSSSAA....AAAA..ANNN..NNNNN..
//.PPPP........PEEEEEEEEEE.ELLLLLLLLL..UUUUUUUUU...UPPP.........UUUUUUUUU...USSSSSSSSS.SSAA.....AAAA.ANNN..NNNNN..
//.PPPP........PEEEEEEEEEE.ELLLLLLLLL...UUUUUUU....UPPP..........UUUUUUU.....SSSSSSSS.SSSAA.....AAAA.ANNN...NNNN..
//................................................................................................................

  getPelupusanDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */

    //  getProductGeneral(id: number): Observable<ProductGeneral> {
    //   const params = new HttpParams().set('productId', String(id));
    //   this.isBusy = true;
    //   return this.http.get<ProductGeneral>(this.baseUrl + ProductApiEndpoint.General, {params})
    //     .pipe(
    //       map((resp: any) => {
    //         const model = resp.basicInfo[0] as ProductGeneral;
    //         model.imageList = resp.imageList as ProductImage[];
    //         return model;
    //       }),
    //       tap({complete: () => this.isBusy = false})
    //     );
    // }

    let record: any = {
      // MAKLUMAT SITAAN
      noSd: '772',
      noBadan: '772',
      waktuKesalahan: '15:10',
      butiran: 'butiran',
      jalanId: 1,
      zonId: '1',
      tarikhKesalahan: '02 Jan 2020',
      peruntukanUndangId: '1',
      parlimenId: '1',
      seksyenId: '1',
      kawasanId: '1',
      penguatkuasaId: '1',
      // MAKLUMAT SAKSI
      penguatkuasaIdSaksi: '1',
      tarikhSaksi: '02 Jan 2020',
      noBadanSaksi: '772',
      // MAKLUMAT PEMILIK
      tarikhPemilik: '02 Jan 2020',
      negeriPemilik: 1,
      namaPemilik: '5656',
      noKpSyarikat: '452',
      catatanPemilik: 'catatan',
      alamatPemilik1: 'alamat1',
      alamatPemilik2: 'alamat2',
      alamatPemilik3: 'alamat3',
      poskodPemilik: '54000',
      // MAKLUMAT STOR
      penguatkuasaIdStor: '1',
      waktuTerimaStor: '15:10',
      tarikhTerimaStor: '02 Jan 2020',
      tarikhStor: '02 Jan 2020',
      noBadanStor: '584',
      catatanStor: 'catatan',
      // MAKLUMAT PELUPUSAN
      penguatkuasaIdDaftarPelupusan: '1',
      waktuDaftarPelupusan: '15:10',
      tarikhDaftarPelupusan: '02 Jan 2020',
      zonIdDaftarPelupusan: '1',
      noBadanDaftarPelupusan: '486',
      catatanDaftarPelupusan: 'catatan',
      // MAKLUMAT KELULUSAN
      penguatkuasaIdKelulusanPelupusan: '1',
      statusKelulusanPelupusan: 1,
      jenisPelupusan: 1,
      tarikhKelulusanPelupusan: '02 Jan 2020',
      tarikhBolehDituntut: '02 Jan 2020',
      noBadanKelulusanPelupusan: '845',
      catatanKelulusanPelupusan: 'catatan',
      tempohTahananKelulusanPelupusan: '11',
      // MAKLUMAT PENGELUARAN
      penguatkuasaIdKeluaran: '1',
      tarikhPengeluaran: '02 Jan 2020',
      noBadanPengeluaran: '845',
      catatanPengeluaran: 'catatan',
    };
    return of(record);
  }

  getPelupusanBarangList(page: Pager): Observable<any> {
    console.log('getPelupusanBarangList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
        .set('RowsPerPage', (page.RowsPerPage || 0) + '')
        .set('PageNumber', (page.PageNumber || 0) + '')
        .set('OrderScript', page.OrderScript || '')
        .set('ColumnFilterScript', page.ColumnFilterScript || '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, noSd: '772', noSiri: '514', namaPegawawi: 'Rizal Hashim', noBadan: '61677', namaPemilik: 'Hashim Manaf', noKpSyarikat: '999999999999', tarikhSitaan: '20/03/2020', tarikhDaftarPelupusan: '29/03/2020', status: 'Penerimaan'},
      { id: 2, noSd: '772', noSiri: '514', namaPegawawi: 'Rizal Hashim', noBadan: '61677', namaPemilik: 'Hashim Manaf', noKpSyarikat: '999999999999', tarikhSitaan: '20/03/2020', tarikhDaftarPelupusan: '29/03/2020', status: 'Pelupusan'},
      { id: 3, noSd: '772', noSiri: '514', namaPegawawi: 'Rizal Hashim', noBadan: '61677', namaPemilik: 'Hashim Manaf', noKpSyarikat: '999999999999', tarikhSitaan: '20/03/2020', tarikhDaftarPelupusan: '29/03/2020', status: 'Menunggu Keputusan'},
      { id: 4, noSd: '772', noSiri: '514', namaPegawawi: 'Rizal Hashim', noBadan: '61677', namaPemilik: 'Hashim Manaf', noKpSyarikat: '999999999999', tarikhSitaan: '20/03/2020', tarikhDaftarPelupusan: '29/03/2020', status: 'Diluluskan'},
      { id: 5, noSd: '772', noSiri: '514', namaPegawawi: 'Rizal Hashim', noBadan: '61677', namaPemilik: 'Hashim Manaf', noKpSyarikat: '999999999999', tarikhSitaan: '20/03/2020', tarikhDaftarPelupusan: '29/03/2020', status: 'Ditolak'},
    ];
    const pageInfo: any = { RowCount: 5 };
    return of({ list, pageInfo });

  }

  savePelupusanDetails(model, id: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Pelupusan Disimpan!';
    const Id = id;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  deletePelupusanBarangList(ids: Array<{ id: number }>) {
    console.log('deletePelupusanBarangList ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod Pelupusan Barang telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

}




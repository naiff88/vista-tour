/* tslint:disable:max-line-length */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MenuComponent } from '../dashboard/menu/menu.component';
import { PenerimaanListComponent } from './penerimaan/list/list.component';
import { PenerimaanDetailsComponent } from './penerimaan/penerimaan-details/penerimaan-details.component';
import { TuntutanListComponent } from './tuntutan/list/list.component';
import { TuntutanDetailsComponent } from './tuntutan/tuntutan-details/tuntutan-details.component';
import { PelupusanListComponent } from './pelupusan/list/list.component';
import { PelupusanDetailsComponent } from './pelupusan/pelupusan-details/pelupusan-details.component';

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MenuComponent,

                children: [
                    { path: '', pathMatch: 'full', redirectTo: 'penerimaan-list' },
                    { path: 'penerimaan-list', component: PenerimaanListComponent },
                    { path: 'penerimaan-details/:id/:type', component: PenerimaanDetailsComponent },
                    { path: 'tuntutan-list', component: TuntutanListComponent },
                    { path: 'tuntutan-details/:id/:type/:status', component: TuntutanDetailsComponent },
                    { path: 'pelupusan-list', component: PelupusanListComponent },
                    { path: 'pelupusan-details/:id/:type/:status', component: PelupusanDetailsComponent },
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})

export class EsitaanRoutingModule { }

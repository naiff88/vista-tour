import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TuntutanDetailsComponent } from './tuntutan-details.component';

describe('PenerimaanDetailsComponent', () => {
  let component: TuntutanDetailsComponent;
  let fixture: ComponentFixture<TuntutanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TuntutanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TuntutanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

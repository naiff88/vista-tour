import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TuntutanListComponent } from './list.component';

describe('TuntutanListComponent', () => {
  let component: TuntutanListComponent;
  let fixture: ComponentFixture<TuntutanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TuntutanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TuntutanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

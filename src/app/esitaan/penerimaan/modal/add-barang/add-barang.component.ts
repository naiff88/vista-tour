/* tslint:disable:max-line-length */
import {Component, Inject, Input, OnInit, ViewChild} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
// import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';

// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import { EsitaanService } from '../../../esitaan.service';
import { Pager, PagerSetting } from '../../../../../pagingnation';
import {ProjectManagementUpsert as Project} from '@service-proxies';
import Swal from 'sweetalert2';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AppComponent } from '../../../../app.component';

@Component({
  selector: 'app-pentadbiran-audit-view',
  templateUrl: './add-barang.component.html',
  styleUrls: ['./add-barang.component.scss']
})

// @Injectable()
export class AddBarangComponent implements OnInit {

  @Input() orderStatus: string;
  @Input() orderTabIndex: number;

  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  selectionF = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  // app: AppComponent;

  @ViewChild('dataTable') dataTable: Table;
  @ViewChild('paginator') paginator: Paginator;

  form: FormGroup;
  showSpinner: boolean = false;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;
  filterText = '';
  dataToPass: any = [];
  searchKey: string;
  submitted = false;

  constructor(
    private esitaanService: EsitaanService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    // private app: AppComponent,
    public dialogRef: MatDialogRef<AddBarangComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { id: number },
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
    // this.app = new AppComponent;
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.loadDetails(this.data.id);
    // this.service.getDocumentTypeList();
  }

  loadDetails(AuditId) {
    this.form = this.buildFormItems();
  }

  add() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.productId }));
    console.log('add selectedItems :::::::::::::::::::::: ', selectedItems);

    Swal.fire({
      title: 'Adakan Anda Pasti?',
      text: 'Pilihan Akan Ditambah Kedalam Senarai Barang!',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Ya!'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        // this.app.showOverlaySpinner(true);
        this.esitaanService.saveAddBarang(selectedItems, this.data.id)
          .subscribe(resultAdd => {
            this.showSpinner = false;
            // this.app.showOverlaySpinner(false);
            if (resultAdd.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultAdd.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultAdd.ResponseMessage,
                'success'
              );
              this.dialogRef.close();
            }
            // this.getProductList();
          });
      }
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const NamaBarang = formGroup.controls['namaBarang'];
      const Jumlah = formGroup.controls['jumlah'];
      const Catatan = formGroup.controls['catatan'];

      if (NamaBarang) {
        NamaBarang.setErrors(null);
        if (!NamaBarang.value) {
          NamaBarang.setErrors({ required: true });
        }
      }

      if (Jumlah) {
        Jumlah.setErrors(null);
        if (!Jumlah.value) {
          Jumlah.setErrors({ required: true });
        }
      }

      if (Catatan) {
        Catatan.setErrors(null);
        if (!Catatan.value) {
          Catatan.setErrors({ required: true });
        }
      }

    }
  }
  // close custom validation

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
          namaBarang: ['', []],
          jumlah: ['', []],
          catatan: ['', []],
        }, {}
    );
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AddBarangComponent } from './add-barang.component';

describe('PilihAhliComponent', () => {
  let component: AddBarangComponent;
  let fixture: ComponentFixture<AddBarangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddBarangComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBarangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
// import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import { EsitaanService } from '../../esitaan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import {ProjectManagementUpsert as Project} from '@service-proxies';
import Swal from 'sweetalert2';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AppComponent } from '../../../app.component';
import * as moment from 'moment';
import { PentadbiranService } from '../../../pentadbiran/pentadbiran.service';
import { AddBarangComponent } from '../modal/add-barang/add-barang.component';

@Component({
  selector: 'app-pentadbiran-kumpulan-details',
  templateUrl: './penerimaan-details.component.html',
  styleUrls: ['./penerimaan-details.component.scss']
})

export class PenerimaanDetailsComponent implements OnInit {

  displayedColumns: string[] = ['select', 'projectName', 'manage'];
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  showSpinner: boolean = false;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;
  filterText = '';
  dataToPass: any = [];
  searchKey: string;
  submitted = false;
  filterValue: any;
  onSearch = false;

  Id: number;
  statusId = 0;
  statusList: any = [];
  lokasiList: any = [];
  userList: any = [];
  tarikhKesalahan: any;
  diskaunOlehId: number;
  penguatkuasaId: number = 0;
  jalanId: number = 0;
  waktuKesalahan: any = '';
  zonId = 0;
  zonList: any = [];
  peruntukanUndangId: number = 0;
  peruntukanUndangListRef: string [];
  parlimenId = 0;
  parlimenList: string [];
  seksyenId = 0;
  kawasanId = 0;
  kawasanList: string [];
  NegeriList: any = [];
  // MAKLUMAT SAKSI
  penguatkuasaIdSaksi: number = 0;
  tarikhSaksi: any;
  // MAKLUMAT PEMILIK
  tarikhPemilik: any;
  negeriPemilik: number = 0;
  // MAKLUMAT STOR
  tarikhStor: any;
  penguatkuasaIdStor: number = 0;
  waktuTerimaStor: any = '';
  tarikhTerimaStor: any;

  // PERFECT SCROLL BAR
  public type = 'component';
  public disabled = false;
  public config: PerfectScrollbarConfigInterface = {};
  @ViewChild(PerfectScrollbarComponent, {static: true}) componentRef?: PerfectScrollbarComponent;

  files: File;
  form: FormGroup;

  // any unique name for searching list
  searchID = 'getSenaraiBarangList';

  constructor(
    private service: EsitaanService,
    private pentadbiranService: PentadbiranService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private snackBar: MatSnackBar,
    private router: Router,
    private app: AppComponent,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });
    this.getStatusList();
    this.loadDetails(this.Id);
    this.getZonList();
    this.getUserList();
    this.getPeruntukanUndangListRef();
    this.getParlimenList();
    this.getKawasanList();
    this.getLokasiList();
    this.getSenaraiBarangList();
    this.getNegeriList();
    // this.getInvoice();
  }

  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.service.getPenerimaanDetails(Id)
          .subscribe(result => {
            this.showSpinner = false;
            // MAKLUMAT SITAAN
            this.jalanId = result.jalanId > 0 ? result.jalanId : 0;
            this.zonId = result.zonId > 0 ? result.zonId : 0;
            this.tarikhKesalahan = result.tarikhKesalahan = moment(result.tarikhKesalahan) || '';
            this.parlimenId = result.parlimenId > 0 ? result.parlimenId : 0;
            this.seksyenId = result.seksyenId > 0 ? result.seksyenId : 0;
            this.kawasanId = result.kawasanId > 0 ? result.kawasanId : 0;
            this.peruntukanUndangId = result.peruntukanUndangId > 0 ? result.peruntukanUndangId : 0;
            this.waktuKesalahan = result.waktuKesalahan;
            this.penguatkuasaId = result.penguatkuasaId > 0 ? result.penguatkuasaId : 0;
            // MAKLUMAT SAKSI
            this.penguatkuasaIdSaksi = result.penguatkuasaIdSaksi > 0 ? result.penguatkuasaIdSaksi : 0;
            this.tarikhSaksi = result.tarikhSaksi = moment(result.tarikhSaksi) || '';
            // MAKLUMAT PEMILIK
            this.tarikhPemilik = result.tarikhPemilik = moment(result.tarikhPemilik) || '';
            this.negeriPemilik = result.negeriPemilik > 0 ? result.negeriPemilik : 0;
            // MAKLUMAT STOR
            this.penguatkuasaIdStor = result.penguatkuasaIdStor > 0 ? result.penguatkuasaIdStor : 0;
            this.waktuTerimaStor = result.waktuTerimaStor;
            this.tarikhTerimaStor = result.tarikhTerimaStor = moment(result.tarikhTerimaStor) || '';
            this.tarikhStor = result.tarikhStor = moment(result.tarikhStor) || '';
            // this.jenisKenderaanId = result.jenisKenderaanId > 0 ? result.jenisKenderaanId : 0;
            // this.diskaunOlehId = result.diskaunOlehId > 0 ? result.diskaunOlehId : 0;
            // this.statusId = result.statusId > 0 ? result.statusId : 0;
            // this.tarikhNotisDari = result.tarikhNotisDari = moment(result.tarikhNotisDari) || '';
            // this.tarikhNotisHingga = result.tarikhNotisHingga = moment(result.tarikhNotisHingga) || '';
            // this.tarikhDiskaunDari = result.tarikhDiskaunDari = moment(result.tarikhDiskaunDari) || '';
            // this.tarikhDiskaunHingga = result.tarikhDiskaunHingga = moment(result.tarikhDiskaunHingga) || '';
            this.form.patchValue(result);
          });
    }
  }

  getStatusList() {
    // this.service.getStatusList().subscribe(items => {
    //   this.statusList = items;
    // });
  }

  getZonList() {
    this.pentadbiranService.getZonList().subscribe(items => {
      this.zonList = items;
    });
  }

  getPeruntukanUndangListRef() {
    this.pentadbiranService.getPeruntukanUndangList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.peruntukanUndangListRef = items.list as string [];
    });
  }

  getParlimenList() {
    this.pentadbiranService.getParlimenList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.parlimenList = items.list as string [];
      console.log('getParlimenList---> ', this.parlimenList);
    });
  }

  getKawasanList() {
    this.pentadbiranService.getKawasanKesalahanList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.parlimenList = items.list as string [];
      console.log('getParlimenList---> ', this.parlimenList);
    });
  }

  getLokasiList() {
    this.pentadbiranService.getKawasanKesalahanList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.lokasiList = items.list as string [];
    });
  }

  getNegeriList() {
    this.service.getNegeriList().subscribe(items => {
      this.NegeriList = items;
    });
  }

  getUserList() {
    this.pentadbiranService.getUserList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.userList = items.list as string [];
    });
  }

  // open standard function use for multiple delete in datatable
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.CustomerId + 1}`;
  }
  onRowSelect(event) {
    const evt = event.originalEvent as MouseEvent;
    evt.stopPropagation();
  }
  // close standard function use for multiple delete in datatable

  // open standard listing with search function
  async getSenaraiBarangList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    await this.service.getSenaraiBarangList(this.pageSetting.getPagerSettingWithSearch(this.paginator, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          this.showSpinner = false;
          this.dataSource.data = items.list;
          this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
          this.primengTableHelper.records = items.list;
          if (this.onSearch) {
            this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
              duration: 3000,
              verticalPosition: 'bottom',
              horizontalPosition: 'end'
            });
            this.onSearch = false;
          }
          this.primengTableHelper.hideLoadingIndicator();
          if (event! && (event.filters || event.sortField)) {
            this.paginator.changePage(0);
          }
        });
    await this.primengTableHelper.showLoadingIndicator();
  }
  // close standard listing with search function

  addBarang(penerimaanId): void {
    const dialogRef = this.dialog.open(AddBarangComponent, { data: { id:  penerimaanId}});
    dialogRef.afterClosed()
        .subscribe(ok => {
          this.getSenaraiBarangList();
        });
  }

  onSave() {
    this.submitted = true;
    // if (this.form.invalid) {
    //   Swal.fire(
    //     'Nota',
    //     'Sila pastikan maklumat lengkap diisi!',
    //     'warning'
    //   );
    // } else {
    //   const model = this.form.value;
    //   this.app.showOverlaySpinner(true);
    //
    //   console.log('modelIklan-->', model);
    //
    //   this.service.saveDiskaunKenderaan(model, this.Id)
    //     .subscribe(r => {
    //       this.app.showOverlaySpinner(false);
    //       if (r.ReturnCode === 200) {
    //         Swal.fire(
    //           'Success',
    //           r.ResponseMessage,
    //           'success'
    //         );
    //         this.router.navigate(['/app/pentadbiran/kompaun-diskaun-list']);
    //       } else {
    //         Swal.fire(
    //           'Error',
    //           r.ResponseMessage,
    //           'error'
    //         );
    //       }
    //     });
    // }
  }

  // onCancel() {
  //   this.router.navigate(['/app/pentadbiran/data-induk/kenderaan/jenama-kenderaan-list']);
  // }

  // multiple delete function
  delete() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.Id }));
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: 'Rekod yang telah hapus tidak akan dikembalikan!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        this.service.deleteBarangList(selectedItems)
            .subscribe(resultDelete => {
              this.showSpinner = false;
              if (resultDelete.ReturnCode == 204) {
                Swal.fire(
                    'Error',
                    resultDelete.ResponseMessage,
                    'error'
                );
              }
              else {
                Swal.fire(
                    '',
                    resultDelete.ResponseMessage,
                    'success'
                );
              }
              this.getSenaraiBarangList();
            });
      }
    })
  }

  // open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const NoSD = formGroup.controls['noSd'];
      const Jalan = formGroup.controls['jalanId'];
      const Penguatkuasa = formGroup.controls['penguatkuasaId'];
      const TarikhKesalahan = formGroup.controls['tarikhKesalahan'];
      const NoBadan = formGroup.controls['noBadan'];
      const WaktuKesalahan = formGroup.controls['waktuKesalahan'];
      const Zon = formGroup.controls['zonId'];
      const Peruntukan = formGroup.controls['peruntukanUndangId'];
      const Parlimen = formGroup.controls['parlimenId'];
      const Seksyen = formGroup.controls['seksyenId'];
      const Kawasan = formGroup.controls['kawasanId'];
      const Butiran = formGroup.controls['butiran'];
      const NamaPemilik = formGroup.controls['namaPemilik'];
      const NoKpSyarikat = formGroup.controls['noKpSyarikat'];
      const Catatan = formGroup.controls['catatan'];
      // const NamaSaksi = formGroup.controls['namaSaksi'];

      const PenguatkuasaSaksi = formGroup.controls['penguatkuasaIdSaksi'];
      const TarikhSaksi = formGroup.controls['tarikhSaksi'];
      const NoBadanSaksi = formGroup.controls['noBadanSaksi'];

      const TarikhPemilik = formGroup.controls['tarikhPemilik'];
      const AlamatPemilik1 = formGroup.controls['AlamatPemilik1'];
      const AlamatPemilik2 = formGroup.controls['AlamatPemilik2'];
      const AlamatPemilik3 = formGroup.controls['AlamatPemilik3'];
      const PoskodPemilik = formGroup.controls['PoskodPemilik'];
      const NegeriPemilik = formGroup.controls['NegeriPemilik'];
      const CatatanPemilik = formGroup.controls['catatanPemilik'];

      const PenguatkuasaStor = formGroup.controls['penguatkuasaIdStor'];
      const TarikhStor = formGroup.controls['tarikhStor'];
      const WaktuTerimaStor = formGroup.controls['waktuTerimaStor'];
      const TarikhTerimaStor = formGroup.controls['tarikhTerimmaStor'];
      const NoBadanStor = formGroup.controls['noBadanStor'];

      // MAKLUMAT STOR
      if (PenguatkuasaStor) {
        PenguatkuasaStor.setErrors(null);
        if (!PenguatkuasaStor.value) {
          PenguatkuasaStor.setErrors({ required: true });
        }
      }

      if (TarikhStor) {
        TarikhStor.setErrors(null);
        if (!TarikhStor.value) {
          TarikhStor.setErrors({ required: true });
        }
      }

      if (WaktuTerimaStor) {
        WaktuTerimaStor.setErrors(null);
        if (!WaktuTerimaStor.value) {
          WaktuTerimaStor.setErrors({ required: true });
        }
      }

      if (TarikhTerimaStor) {
        TarikhTerimaStor.setErrors(null);
        if (!TarikhTerimaStor.value) {
          TarikhTerimaStor.setErrors({ required: true });
        }
      }

      if (NoBadanStor) {
        NoBadanStor.setErrors(null);
        if (!NoBadanStor.value) {
          NoBadanStor.setErrors({ required: true });
        }
      }

      // MAKLUMAT SAKSI
      if (PenguatkuasaSaksi) {
        PenguatkuasaSaksi.setErrors(null);
        if (!PenguatkuasaSaksi.value) {
          PenguatkuasaSaksi.setErrors({ required: true });
        }
      }

      if (TarikhSaksi) {
        TarikhSaksi.setErrors(null);
        if (!TarikhSaksi.value) {
          TarikhSaksi.setErrors({ required: true });
        }
      }

      if (NoBadanSaksi) {
        NoBadanSaksi.setErrors(null);
        if (!NoBadanSaksi.value) {
          NoBadanSaksi.setErrors({ required: true });
        }
      }

      // MAKLUMAT PEMILIK
      if (TarikhPemilik) {
        TarikhPemilik.setErrors(null);
        if (!TarikhPemilik.value) {
          TarikhPemilik.setErrors({ required: true });
        }
      }

      if (AlamatPemilik1) {
        AlamatPemilik1.setErrors(null);
        if (!AlamatPemilik1.value) {
          AlamatPemilik1.setErrors({ required: true });
        }
      }

      if (PoskodPemilik) {
        PoskodPemilik.setErrors(null);
        if (!PoskodPemilik.value) {
          PoskodPemilik.setErrors({ required: true });
        }
      }

      if (NegeriPemilik) {
        NegeriPemilik.setErrors(null);
        if (!NegeriPemilik.value) {
          NegeriPemilik.setErrors({ required: true });
        }
        else if (NegeriPemilik.value == 0) {
          NegeriPemilik.setErrors({ min: true });
        }
      }

      if (CatatanPemilik) {
        CatatanPemilik.setErrors(null);
        if (!CatatanPemilik.value) {
          CatatanPemilik.setErrors({ required: true });
        }
      }

      // MAKLUMAT SITAAN
      if (NoSD) {
        NoSD.setErrors(null);
        if (!NoSD.value) {
          NoSD.setErrors({ required: true });
        }
      }

      if (Jalan) {
        Jalan.setErrors(null);
        if (!Jalan.value) {
          Jalan.setErrors({ required: true });
        }
      }

      if (Penguatkuasa) {
        Penguatkuasa.setErrors(null);
        if (!Penguatkuasa.value) {
          Penguatkuasa.setErrors({ required: true });
        }
      }

      if (TarikhKesalahan) {
        TarikhKesalahan.setErrors(null);
        if (!TarikhKesalahan.value) {
          TarikhKesalahan.setErrors({ required: true });
        }
      }

      if (NoBadan) {
        NoBadan.setErrors(null);
        if (!NoBadan.value) {
          NoBadan.setErrors({ required: true });
        }
      }

      if (WaktuKesalahan) {
        WaktuKesalahan.setErrors(null);
        if (!WaktuKesalahan.value) {
          WaktuKesalahan.setErrors({ required: true });
        }
      }

      if (Zon) {
        Zon.setErrors(null);
        if (!Zon.value) {
          Zon.setErrors({ required: true });
        }
      }

      if (Peruntukan) {
        Peruntukan.setErrors(null);
        if (!Peruntukan.value) {
          Peruntukan.setErrors({ required: true });
        }
      }

      if (Parlimen) {
        Parlimen.setErrors(null);
        if (!Parlimen.value) {
          Parlimen.setErrors({ required: true });
        }
      }

      if (Seksyen) {
        Seksyen.setErrors(null);
        if (!Seksyen.value) {
          Seksyen.setErrors({ required: true });
        }
      }

      if (Kawasan) {
        Kawasan.setErrors(null);
        if (!Kawasan.value) {
          Kawasan.setErrors({ required: true });
        }
      }

      if (Butiran) {
        Butiran.setErrors(null);
        if (!Butiran.value) {
          Butiran.setErrors({ required: true });
        }
      }

      if (NamaPemilik) {
        NamaPemilik.setErrors(null);
        if (!NamaPemilik.value) {
          NamaPemilik.setErrors({ required: true });
        }
      }

      if (NoKpSyarikat) {
        NoKpSyarikat.setErrors(null);
        if (!NoKpSyarikat.value) {
          NoKpSyarikat.setErrors({ required: true });
        }
      }

      if (Catatan) {
        Catatan.setErrors(null);
        if (!Catatan.value) {
          Catatan.setErrors({ required: true });
        }
      }

      // if (NamaSaksi) {
      //   NamaSaksi.setErrors(null);
      //   if (!NamaSaksi.value) {
      //     NamaSaksi.setErrors({ required: true });
      //   }
      // }

    }
  }
  // close custom validation

  // open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: 'Rekod semasa akan ditetapkan semula!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  // close reset function


  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      noSd: ['', []],
      jalanId: ['0',[]],
      penguatkuasaId: ['0',[]],
      tarikhKesalahan: ['', []],
      noBadan: ['', []],
      waktuKesalahan: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      zonId: ['', []],
      peruntukanUndangId: ['0', []],
      parlimenId: ['', []],
      seksyenId: ['', []],
      kawasanId: ['', []],
      butiran: ['', []],
      noKpSyarikat: ['', []],
      catatan: ['', []],
      // namaSaksi: ['', []],
      // MAKLUMAT SAKSI
      penguatkuasaIdSaksi: ['0',[]],
      tarikhSaksi: ['', []],
      noBadanSaksi: ['', []],
      // MAKLUMAT PEMILIK
      namaPemilik: ['', []],
      tarikhPemilik: ['', []],
      alamatPemilik1: ['', []],
      alamatPemilik2: ['', []],
      alamatPemilik3: ['', []],
      poskodPemilik: ['', []],
      negeriPemilik: ['0', []],
      catatanPemilik: ['', []],
      // MAKLUMAT STOR
      penguatkuasaIdStor: ['0',[]],
      tarikhStor: ['', []],
      catatanStor: ['', []],
      waktuTerimaStor: ['', []],
      tarikhTerimaStor: ['', []],
      noBadanStor: ['', []],
      }, { validator: this.customValidation() }
    );
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenerimaanDetailsComponent } from './penerimaan-details.component';

describe('PenerimaanDetailsComponent', () => {
  let component: PenerimaanDetailsComponent;
  let fixture: ComponentFixture<PenerimaanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenerimaanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenerimaanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

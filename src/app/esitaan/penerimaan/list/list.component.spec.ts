import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenerimaanListComponent } from './list.component';

describe('PenerimaanListComponent', () => {
  let component: PenerimaanListComponent;
  let fixture: ComponentFixture<PenerimaanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenerimaanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenerimaanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

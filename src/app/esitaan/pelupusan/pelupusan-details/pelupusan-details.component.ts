/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
// import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import { EsitaanService } from '../../esitaan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import {ProjectManagementUpsert as Project} from '@service-proxies';
import Swal from 'sweetalert2';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AppComponent } from '../../../app.component';
import * as moment from 'moment';
import { PentadbiranService } from '../../../pentadbiran/pentadbiran.service';

@Component({
  selector: 'app-pentadbiran-kumpulan-details',
  templateUrl: './pelupusan-details.component.html',
  styleUrls: ['./pelupusan-details.component.scss']
})

export class PelupusanDetailsComponent implements OnInit {

  displayedColumns: string[] = ['select', 'projectName', 'manage'];
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  showSpinner: boolean = false;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;
  filterText = '';
  dataToPass: any = [];
  searchKey: string;
  submitted = false;
  filterValue: any;
  onSearch = false;

  Id: number;
  tStatus: string;
  statusId = 0;
  statusList: any = [];
  lokasiList: any = [];
  userList: any = [];
  tarikhKesalahan: any;
  diskaunOlehId: number;
  penguatkuasaId: number = 0;
  jalanId: number = 0;
  waktuKesalahan: any = '';
  zonId = 0;
  zonList: any = [];
  peruntukanUndangId: number = 0;
  peruntukanUndangListRef: string [];
  parlimenId = 0;
  parlimenList: string [];
  seksyenId = 0;
  kawasanId = 0;
  kawasanList: string [];
  NegeriList: any = [];
  // MAKLUMAT SAKSI
  penguatkuasaIdSaksi: number = 0;
  tarikhSaksi: any;
  // MAKLUMAT PEMILIK
  tarikhPemilik: any;
  negeriPemilik: number = 0;
  // MAKLUMAT STOR
  tarikhStor: any;
  penguatkuasaIdStor: number = 0;
  waktuTerimaStor: any = '';
  tarikhTerimaStor: any;
  // MAKLUMAT DAFTAR PELUPUSAN
  penguatkuasaIdDaftarPelupusan: number = 0;
  waktuDaftarPelupusan: any = '';
  tarikhDaftarPelupusan: any;
  zonIdDaftarPelupusan: number = 0;
  noBadanDaftarPelupusan: any;
  catatanDaftarPelupusan: any;
  // MAKLUMAT KELULUSAN PELUPUSAN
  penguatkuasaIdKelulusanPelupusan: number = 0;
  statusKelulusanPelupusan: number = 0;
  jenisPelupusan: number = 0;
  tarikhKelulusanPelupusan: any;
  tarikhBolehDituntut: any;
  noBadanKelulusanPelupusan: any;
  catatanKelulusanPelupusan: any;
  tempohTahananKelulusanPelupusan: any;
  // MAKLUMAT PENGELUARAN
  penguatkuasaIdKeluaran: number = 0;
  tarikhPengeluaran: any;
  noBadanPengeluaran: any;
  catatanPengeluaran: any;

  // PERFECT SCROLL BAR
  public type = 'component';
  public disabled = false;
  public config: PerfectScrollbarConfigInterface = {};
  @ViewChild(PerfectScrollbarComponent, {static: true}) componentRef?: PerfectScrollbarComponent;

  files: File;
  form: FormGroup;

  // any unique name for searching list
  searchID = 'getSenaraiBarangList';

  constructor(
    private service: EsitaanService,
    private pentadbiranService: PentadbiranService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private snackBar: MatSnackBar,
    private router: Router,
    private app: AppComponent,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        this.tStatus = params.status;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });
    this.getStatusList();
    this.loadDetails(this.Id);
    this.getZonList();
    this.getUserList();
    this.getPeruntukanUndangListRef();
    this.getParlimenList();
    this.getKawasanList();
    this.getLokasiList();
    this.getSenaraiBarangList();
    this.getNegeriList();
    // this.getInvoice();
  }

  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.service.getPelupusanDetails(Id)
          .subscribe(result => {
            this.showSpinner = false;
            // MAKLUMAT SITAAN
            this.jalanId = result.jalanId > 0 ? result.jalanId : 0;
            this.zonId = result.zonId > 0 ? result.zonId : 0;
            this.tarikhKesalahan = result.tarikhKesalahan = moment(result.tarikhKesalahan) || '';
            this.parlimenId = result.parlimenId > 0 ? result.parlimenId : 0;
            this.seksyenId = result.seksyenId > 0 ? result.seksyenId : 0;
            this.kawasanId = result.kawasanId > 0 ? result.kawasanId : 0;
            this.peruntukanUndangId = result.peruntukanUndangId > 0 ? result.peruntukanUndangId : 0;
            this.waktuKesalahan = result.waktuKesalahan;
            this.penguatkuasaId = result.penguatkuasaId > 0 ? result.penguatkuasaId : 0;
            // MAKLUMAT SAKSI
            this.penguatkuasaIdSaksi = result.penguatkuasaIdSaksi > 0 ? result.penguatkuasaIdSaksi : 0;
            this.tarikhSaksi = result.tarikhSaksi = moment(result.tarikhSaksi) || '';
            // MAKLUMAT PEMILIK
            this.tarikhPemilik = result.tarikhPemilik = moment(result.tarikhPemilik) || '';
            this.negeriPemilik = result.negeriPemilik > 0 ? result.negeriPemilik : 0;
            // MAKLUMAT STOR
            this.penguatkuasaIdStor = result.penguatkuasaIdStor > 0 ? result.penguatkuasaIdStor : 0;
            this.waktuTerimaStor = result.waktuTerimaStor;
            this.tarikhTerimaStor = result.tarikhTerimaStor = moment(result.tarikhTerimaStor) || '';
            this.tarikhStor = result.tarikhStor = moment(result.tarikhStor) || '';
            // MAKLUMAT PELUPUSAN
            if (this.tStatus !== 'Penerimaan') {
              this.penguatkuasaIdDaftarPelupusan = result.penguatkuasaIdDaftarPelupusan > 0 ? result.penguatkuasaIdDaftarPelupusan : 0;
              this.waktuDaftarPelupusan = result.waktuDaftarPelupusan;
              this.tarikhDaftarPelupusan = result.tarikhDaftarPelupusan = moment(result.tarikhDaftarPelupusan) || '';
              this.zonIdDaftarPelupusan = result.zonIdDaftarPelupusan > 0 ? result.zonIdDaftarPelupusan : 0;
              this.noBadanDaftarPelupusan = result.noBadanDaftarPelupusan;
              this.catatanDaftarPelupusan = result.catatanDaftarPelupusan;
            }
            // MAKLUMAT KELULUSAN
            if (this.tStatus !== 'Menunggu Keputusan') {
              this.penguatkuasaIdKelulusanPelupusan = result.penguatkuasaIdKelulusanPelupusan > 0 ? result.penguatkuasaIdKelulusanPelupusan : 0;
              this.statusKelulusanPelupusan = result.statusKelulusanPelupusan > 0 ? result.statusKelulusanPelupusan : 0;
              this.jenisPelupusan = result.jenisPelupusan > 0 ? result.jenisPelupusan : 0;
              this.tarikhBolehDituntut = result.tarikhBolehDituntut = moment(result.tarikhBolehDituntut) || '';
              this.noBadanKelulusanPelupusan = result.noBadanKelulusanPelupusan;
              this.tarikhKelulusanPelupusan = result.tarikhKelulusanPelupusan = moment(result.tarikhKelulusanPelupusan) || '';
              this.catatanKelulusanPelupusan = result.catatanKelulusanPelupusan;
              this.tempohTahananKelulusanPelupusan  = result.tempohTahananKelulusanPelupusan;
            }
            // MAKLUMAT PENGELUARAN
            if (this.tStatus !== 'Diluluskan') {
              this.penguatkuasaIdKeluaran = result.penguatkuasaIdKeluaran > 0 ? result.penguatkuasaIdKeluaran : 0;
              this.tarikhPengeluaran = result.tarikhPengeluaran = moment(result.tarikhPengeluaran) || '';
              this.noBadanPengeluaran = result.noBadanPengeluaran;
              this.catatanPengeluaran = result.catatanPengeluaran;
            }
            this.form.patchValue(result);
          });
    }
  }

  getStatusList() {
    // this.service.getStatusList().subscribe(items => {
    //   this.statusList = items;
    // });
  }

  getZonList() {
    this.pentadbiranService.getZonList().subscribe(items => {
      this.zonList = items;
    });
  }

  getPeruntukanUndangListRef() {
    this.pentadbiranService.getPeruntukanUndangList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.peruntukanUndangListRef = items.list as string [];
    });
  }

  getParlimenList() {
    this.pentadbiranService.getParlimenList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.parlimenList = items.list as string [];
      console.log('getParlimenList---> ', this.parlimenList);
    });
  }

  getKawasanList() {
    this.pentadbiranService.getKawasanKesalahanList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.parlimenList = items.list as string [];
      console.log('getParlimenList---> ', this.parlimenList);
    });
  }

  getLokasiList() {
    this.pentadbiranService.getKawasanKesalahanList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.lokasiList = items.list as string [];
    });
  }

  getNegeriList() {
    this.service.getNegeriList().subscribe(items => {
      this.NegeriList = items;
    });
  }

  getUserList() {
    this.pentadbiranService.getUserList(this.pageSetting.getPagerSetting(0)).subscribe(items => {
      this.userList = items.list as string [];
    });
  }

  // open standard function use for multiple delete in datatable
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.CustomerId + 1}`;
  }
  onRowSelect(event) {
    const evt = event.originalEvent as MouseEvent;
    evt.stopPropagation();
  }
  // close standard function use for multiple delete in datatable

  // open standard listing with search function
  async getSenaraiBarangList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    await this.service.getSenaraiBarangList(this.pageSetting.getPagerSettingWithSearch(this.paginator, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          this.showSpinner = false;
          this.dataSource.data = items.list;
          this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
          this.primengTableHelper.records = items.list;
          if (this.onSearch) {
            this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
              duration: 3000,
              verticalPosition: 'bottom',
              horizontalPosition: 'end'
            });
            this.onSearch = false;
          }
          this.primengTableHelper.hideLoadingIndicator();
          if (event! && (event.filters || event.sortField)) {
            this.paginator.changePage(0);
          }
        });
    await this.primengTableHelper.showLoadingIndicator();
  }
  // close standard listing with search function

  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.app.showOverlaySpinner(true);

      console.log('modelIklan-->', model);

      this.service.savePelupusanDetails(model, this.Id)
        .subscribe(r => {
          this.app.showOverlaySpinner(false);
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.router.navigate(['/app/esitaan/pelupusan-list']);
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }

  // onCancel() {
  //   this.router.navigate(['/app/pentadbiran/data-induk/kenderaan/jenama-kenderaan-list']);
  // }

  // multiple delete function
  delete() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.Id }));
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: 'Rekod yang telah hapus tidak akan dikembalikan!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        this.service.deleteBarangList(selectedItems)
            .subscribe(resultDelete => {
              this.showSpinner = false;
              if (resultDelete.ReturnCode == 204) {
                Swal.fire(
                    'Error',
                    resultDelete.ResponseMessage,
                    'error'
                );
              }
              else {
                Swal.fire(
                    '',
                    resultDelete.ResponseMessage,
                    'success'
                );
              }
              this.getSenaraiBarangList();
            });
      }
    })
  }

  // open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const WaktuDaftarPelupusan = formGroup.controls['waktuDaftarPelupusan'];
      const TarikhDaftarPelupusan = formGroup.controls['tarikhDaftarPelupusan'];
      const PenguatkuasaIdDaftarPelupusan = formGroup.controls['penguatkuasaIdDaftarPelupusan'];
      const NoBadanDaftarPelupusan = formGroup.controls['noBadanDaftarPelupusan'];
      const ZonIdDaftarPelupusan = formGroup.controls['zonIdDaftarPelupusan'];
      const CatatanDaftarPelupusan = formGroup.controls['catatanDaftarPelupusan'];

      const PenguatkuasaIdKelulusanPelupusan = formGroup.controls['penguatkuasaIdKelulusanPelupusan'];
      const StatusKelulusanPelupusan = formGroup.controls['statusKelulusanPelupusan'];
      const JenisPelupusan = formGroup.controls['jenisPelupusan'];
      const TarikhBolehDituntut = formGroup.controls['tarikhBolehDituntut'];
      const NoBadanKelulusanPelupusan = formGroup.controls['noBadanKelulusanPelupusan'];
      const TarikhKelulusanPelupusan = formGroup.controls['tarikhKelulusanPelupusan'];
      const CatatanKelulusanPelupusan = formGroup.controls['catatanKelulusanPelupusan'];
      const TempohTahananKelulusanPelupusan = formGroup.controls['tempohTahananKelulusanPelupusan'];

      const PenguatkuasaIdKeluaran = formGroup.controls['penguatkuasaIdKeluaran'];
      const TarikhPengeluaran = formGroup.controls['tarikhPengeluaran'];
      const NoBadanPengeluaran = formGroup.controls['noBadanPengeluaran'];
      const CatatanPengeluaran = formGroup.controls['catatanPengeluaran'];

      if (PenguatkuasaIdKeluaran) { PenguatkuasaIdKeluaran.setErrors(null); if (!PenguatkuasaIdKeluaran.value) { PenguatkuasaIdKeluaran.setErrors({ required: true }); }}
      if (TarikhPengeluaran) { TarikhPengeluaran.setErrors(null); if (!TarikhPengeluaran.value) { TarikhPengeluaran.setErrors({ required: true }); }}
      if (NoBadanPengeluaran) { NoBadanPengeluaran.setErrors(null); if (!NoBadanPengeluaran.value) { NoBadanPengeluaran.setErrors({ required: true }); }}
      if (CatatanPengeluaran) { CatatanPengeluaran.setErrors(null); if (!CatatanPengeluaran.value) { CatatanPengeluaran.setErrors({ required: true }); }}

      if (PenguatkuasaIdKelulusanPelupusan) { PenguatkuasaIdKelulusanPelupusan.setErrors(null); if (!PenguatkuasaIdKelulusanPelupusan.value) { PenguatkuasaIdKelulusanPelupusan.setErrors({ required: true }); }}
      if (StatusKelulusanPelupusan) { StatusKelulusanPelupusan.setErrors(null); if (!StatusKelulusanPelupusan.value) { StatusKelulusanPelupusan.setErrors({ required: true }); }}
      if (JenisPelupusan) { JenisPelupusan.setErrors(null); if (!JenisPelupusan.value) { JenisPelupusan.setErrors({ required: true }); }}
      if (TarikhBolehDituntut) { TarikhBolehDituntut.setErrors(null); if (!TarikhBolehDituntut.value) { TarikhBolehDituntut.setErrors({ required: true }); }}
      if (NoBadanKelulusanPelupusan) { NoBadanKelulusanPelupusan.setErrors(null); if (!NoBadanKelulusanPelupusan.value) { NoBadanKelulusanPelupusan.setErrors({ required: true }); }}
      if (TarikhKelulusanPelupusan) { TarikhKelulusanPelupusan.setErrors(null); if (!TarikhKelulusanPelupusan.value) { TarikhKelulusanPelupusan.setErrors({ required: true }); }}
      if (CatatanKelulusanPelupusan) { CatatanKelulusanPelupusan.setErrors(null); if (!CatatanKelulusanPelupusan.value) { CatatanKelulusanPelupusan.setErrors({ required: true }); }}
      if (TempohTahananKelulusanPelupusan) { TempohTahananKelulusanPelupusan.setErrors(null); if (!TempohTahananKelulusanPelupusan.value) { TempohTahananKelulusanPelupusan.setErrors({ required: true }); }}

      if (WaktuDaftarPelupusan) { WaktuDaftarPelupusan.setErrors(null); if (!WaktuDaftarPelupusan.value) { WaktuDaftarPelupusan.setErrors({ required: true }); }}
      if (PenguatkuasaIdDaftarPelupusan) { PenguatkuasaIdDaftarPelupusan.setErrors(null); if (!PenguatkuasaIdDaftarPelupusan.value) { PenguatkuasaIdDaftarPelupusan.setErrors({ required: true }); }}
      if (TarikhDaftarPelupusan) { TarikhDaftarPelupusan.setErrors(null); if (!TarikhDaftarPelupusan.value) { TarikhDaftarPelupusan.setErrors({ required: true }); }}
      if (NoBadanDaftarPelupusan) { NoBadanDaftarPelupusan.setErrors(null); if (!NoBadanDaftarPelupusan.value) { NoBadanDaftarPelupusan.setErrors({ required: true }); }}
      if (ZonIdDaftarPelupusan) { ZonIdDaftarPelupusan.setErrors(null); if (!ZonIdDaftarPelupusan.value) { ZonIdDaftarPelupusan.setErrors({ required: true }); }}
      if (CatatanDaftarPelupusan) { CatatanDaftarPelupusan.setErrors(null); if (!CatatanDaftarPelupusan.value) { CatatanDaftarPelupusan.setErrors({ required: true }); }}
    }
  }
  // close custom validation

  // open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: 'Rekod semasa akan ditetapkan semula!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  // close reset function


  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      noSd: [{ value: '', disabled: true }, []],
      jalanId: [{ value: '0', disabled: true }, []],
      penguatkuasaId: [{ value: '0', disabled: true }, []],
      tarikhKesalahan: [{ value: '', disabled: true }, []],
      noBadan: [{ value: '', disabled: true }, []],
      waktuKesalahan: [{ value: '', disabled: this.FormType == 'view' ? true : true }, []],
      zonId: [{ value: '', disabled: true }, []],
      peruntukanUndangId: [{ value: '', disabled: true }, []],
      parlimenId: [{ value: '', disabled: true }, []],
      seksyenId: [{ value: '0', disabled: true }, []],
      kawasanId: [{ value: '0', disabled: true }, []],
      butiran: [{ value: '', disabled: true }, []],
      noKpSyarikat: [{ value: '', disabled: true }, []],
      catatan: [{ value: '', disabled: true }, []],
      // namaSaksi: ['', []],
      // MAKLUMAT SAKSI
      penguatkuasaIdSaksi: [{ value: '0', disabled: true }, []],
      tarikhSaksi: [{ value: '', disabled: true }, []],
      noBadanSaksi: [{ value: '', disabled: true }, []],
      // MAKLUMAT PEMILIK
      namaPemilik: [{ value: '', disabled: true }, []],
      tarikhPemilik: [{ value: '', disabled: true }, []],
      alamatPemilik1: [{ value: '', disabled: true }, []],
      alamatPemilik2: [{ value: '', disabled: true }, []],
      alamatPemilik3: [{ value: '', disabled: true }, []],
      poskodPemilik: [{ value: '', disabled: true }, []],
      negeriPemilik: [{ value: '0', disabled: true }, []],
      catatanPemilik: [{ value: '', disabled: true }, []],
      // MAKLUMAT STOR
      penguatkuasaIdStor: [{ value: '0', disabled: true }, []],
      tarikhStor: [{ value: '', disabled: true }, []],
      catatanStor: [{ value: '', disabled: true }, []],
      waktuTerimaStor: [{ value: '', disabled: true }, []],
      tarikhTerimaStor: [{ value: '', disabled: true }, []],
      noBadanStor: [{ value: '', disabled: true }, []],
      // MAKLUMAT DAFTAR PELUPUSAN
      penguatkuasaIdDaftarPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Menunggu Keputusan' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      waktuDaftarPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Menunggu Keputusan' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      noBadanDaftarPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Menunggu Keputusan' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      tarikhDaftarPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Menunggu Keputusan' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      zonIdDaftarPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Menunggu Keputusan' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      catatanDaftarPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Menunggu Keputusan' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      // MAKLUMAT KELULUSAN PELUPUSAN
      penguatkuasaIdKelulusanPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      statusKelulusanPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      jenisPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      tarikhBolehDituntut: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      noBadanKelulusanPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      tarikhKelulusanPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      catatanKelulusanPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      tempohTahananKelulusanPelupusan: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Diluluskan' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      // MAKLUMAT PENGELUARAN
      penguatkuasaIdKeluaran: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      tarikhPengeluaran: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      noBadanPengeluaran: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      catatanPengeluaran: [{ value: '', disabled: this.FormType == 'view' ? true : this.tStatus == 'Selesai Pengeluaran' ? true : this.tStatus == 'Ditolak' ? true : null}, []],
      }, { validator: this.customValidation() }
    );
  }

}

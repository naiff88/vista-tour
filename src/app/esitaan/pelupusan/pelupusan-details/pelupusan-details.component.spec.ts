import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PelupusanDetailsComponent } from './pelupusan-details.component';

describe('PelupusanDetailsComponent', () => {
  let component: PelupusanDetailsComponent;
  let fixture: ComponentFixture<PelupusanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PelupusanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PelupusanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PelupusanListComponent } from './list.component';

describe('PelupusanListComponent', () => {
  let component: PelupusanListComponent;
  let fixture: ComponentFixture<PelupusanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PelupusanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PelupusanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

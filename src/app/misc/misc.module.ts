import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MiscRoutingModule } from './misc-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PublishingComponent } from './publishing/publishing.component';
//import { QuoteMeComponent } from './quote-me/quote-me.component';
import { BusyIfDirective } from './busy-if.directive';

@NgModule({
  declarations: [PageNotFoundComponent, PublishingComponent, BusyIfDirective], //QuoteMeComponent
  imports: [
    CommonModule,
    MiscRoutingModule
  ],
  exports:[BusyIfDirective]
})
export class MiscModule { }

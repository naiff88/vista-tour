import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PublishingComponent } from './publishing/publishing.component';

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        children: [
          { path: '404', component: PageNotFoundComponent },
          { path: 'publish', component: PublishingComponent }
        ]
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class MiscRoutingModule { }

import { Type } from '@angular/compiler';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { Injectable, Inject, Input } from '@angular/core';
import { LayoutService } from '../../layout/layout.service';

import { HeaderNavComponent } from '../../layout/nav/header-nav.component';

@Injectable({
  providedIn: 'root'
})
export class MenuSetting {
  //listMenus: any = {};
  //selectedPath: string = "";

  //storelistMenus = new HeaderNavComponent();

  // constructor(public router: Router
  //   //,
  //   // @Inject(LayoutService) public layoutService: LayoutService 
  // ) {
  //   //this.layoutService.stroreCurrentList(this.storelistMenus);
  // }

  // setPath(path) {
  //   this.selectedPath = path;
  // }

  // getFlagMenu() {
  //   return localStorage.getItem("flagMenu") !== null && localStorage.getItem("flagMenu") !== 'null' ? JSON.parse(localStorage.getItem("flagMenu")) : 0;
  // }

  storeListMenu() {
    return localStorage.getItem("listMenu") !== null && localStorage.getItem("listMenu") !== 'null' ? JSON.parse(localStorage.getItem("listMenu")) : [];
  }


  selectedPath() {
    return localStorage.getItem("selectedPath") !== null && localStorage.getItem("selectedPath") !== 'null' ? localStorage.getItem("selectedPath") : '';
  }

  defaultMenuIndex(index) {
    return localStorage.getItem("menuId") !== null && localStorage.getItem("menuId") !== 'null' ? JSON.parse(localStorage.getItem("menuId")) : index;
  }

  getPreviousMenuParentId() {
    return localStorage.getItem("parentMenuId") !== null && localStorage.getItem("parentMenuId") !== 'null' ? JSON.parse(localStorage.getItem("parentMenuId")) : 0;
  }

  // getMenuIdLocalStorage() {
  //   return localStorage.getItem("menuId") !== null && localStorage.getItem("menuId") !== 'null' ? JSON.parse(localStorage.getItem("menuId")) : 0;
  // }

  highlightPath(selectedPath) {
    // console.log('storeListMenu path :::::::::::::::::: ', selectedPath);
    // console.log('storeListMenu :::::::::::::::::: ', this.storeListMenu());
    let list = this.storeListMenu() as [];
    let id;
    let resultMenuByPath;
    let resultMenuByPath1;
    let resultMenuByPath2;
    if (selectedPath != '' && list.length > 0) {
      //console.log('ADA ::::::::::::::::: ');


      resultMenuByPath = list.find(({ path }) => path === selectedPath);
      // console.log(">>> item ::::::::::::: ", selectedPath);
      // list.map(item => {
      //   console.log(">>> item ::::::::::::: ", item);
      //   if(item['path'] == selectedPath && item['parentMenuId'] == 0)
      //   {
      //     resultMenuByPath1 = item;
      //   }
      //   else if(item['path'] == selectedPath && item['parentMenuId'] > 0)
      //   {
      //     resultMenuByPath2 = item;
      //   }
      // });

      // console.log("resultMenuByPath1 ::::::::::::: ", resultMenuByPath1);
      // console.log("resultMenuByPath2 ::::::::::::: ", resultMenuByPath2);

      // resultMenuByPath = !resultMenuByPath2 ? resultMenuByPath1 : resultMenuByPath2;

      //console.log("highlightPath resultMenuByPath ::::::::::::: ", resultMenuByPath);

      if (resultMenuByPath) {
        //localStorage.removeItem("selectedPath");
        id = resultMenuByPath['menuId'];
        // list.map(item => {
        //   var spanMenu = $("#menuLinkId" + item['menuId']);
        //   spanMenu.removeClass("active");
        // });
        // var spanMenu = $("#menuLinkId" + id);
        // spanMenu.addClass('active');
        this.clickMenu(id, list);
      }
    }
  }

  // setParentMenuOpen(previousId, currentId) {
  //   console.log('previousId ::::::::::::::::::: ', previousId);
  //   console.log('currentId ::::::::::::::::::: ', currentId);
  //   if (previousId > 0 && previousId != currentId) {
  //     var div = $("#idSubMenu" + previousId);
  //     console.log('blok div ::::::::::::::::::: 1 :', div.css('display'));
  //     div.css("display", "block");
  //     console.log('blok div ::::::::::::::::::: 2 :', div.css('display'));
  //   }
  // }

  subMenuSlider(divId, openclose) {
    //console.log('subMenuSlider divId :::::: ', divId);
    var div = $("#" + divId);
    //console.log('subMenuSlider div :::::: ', div);
    //console.log('subMenuSlider display 1 :::::: ', div.css('display'));
    //console.log('subMenuSlider openclose :::::: ', openclose);
    if (openclose == "open") {
      if (div.css('display') == undefined) {
        //console.log('div undefined :::::: ');
        //div.hide();
        //div.css("display", "none");
      }
      //console.log('subMenuSlider display 2 :::::: ', div.css('display'));
      //div.show();
      div.slideDown();
      //div.css("display", "block");
      // div.fadeIn( "slow" );
    }
    else if (openclose == "close") {
      //console.log('SLIDE UP');
      if (div.css('display') == undefined) {
        //div.css("display", "block");
      }
      //console.log('subMenuSlider display 3 :::::: ', div.css('display'));
      //div.hide();

      //div.css("display", "block");
      //div.fadeOut( "slow" );

      div.slideUp();
      //div.css("display", "none");
    }
    //  div.animate({left: '100px'}, "slow");
    //  div.animate({fontSize: '5em'}, "slow");
  }

  clickMenu(id, listMenusGet) {
  const resultMenu = listMenusGet.find(({ menuId }) => menuId === id);
  localStorage.setItem('menuId', JSON.stringify(id))

  setTimeout(() => {
    let parentIdtoClose = 0;
    //var div2 = $("#idSubMenu2");
    //console.log('>>>>>>>>>>>>>>> ::::::::::::::::::: 1 :', div2.css('display'));

    //var currentMenuID = this.getMenuIdLocalStorage();
    var selectedPath = this.selectedPath();
    var previousParentMenuId = this.getPreviousMenuParentId();
    //console.log("previousParentMenuId ::::::::::::: ", previousParentMenuId);
    // console.log("selectedPath ::::::::::::: ", this.selectedPath());
    let resultMenuByPath;
    if (selectedPath != '') {
      resultMenuByPath = listMenusGet.find(({ path }) => path === selectedPath);
      localStorage.removeItem("selectedPath");
      // console.log("resultMenuByPath ::::::::::::: ", resultMenuByPath);
      if (resultMenuByPath) {
        id = resultMenuByPath['menuId'];
      }
    }
    //console.log("resultMenuByPath ID ::::::::::::: ", id);
    //console.log("clickMenu ID", id);
    //console.log("clickMenu listMenus", listMenusGet);
    //this.storelistMenus = listMenus;
    //this.layoutService.stroreCurrentList(this.storelistMenus);
    //console.log(" this.listMenus :::::::: ",  this.storelistMenus);
    //console.log("Current URL", this.router.url);
    //localStorage.removeItem("listMenu");
    localStorage.setItem('listMenu', JSON.stringify(listMenusGet));
   
    //var currentIndex = localStorage.getItem("menuId") !== null && localStorage.getItem("menuId") !== 'null' ? JSON.parse(localStorage.getItem("menuId")) : id;
    //console.log('currentIndex :::::: ', currentIndex);
    //console.log('selectedIndex :::::: ', id);
   
    //this.setParentMenuOpen(previousParentMenuId, resultMenu.parentMenuId > 0 ? resultMenu.parentMenuId : resultMenu.menuId);

    //console.log('resultMenu :::::: ', resultMenu);
    // if (resultMenu.parentMenuId > 0) {
    //   resultMenu 
    // }

  
      if (resultMenu.parentMenuId > 0) {
        //console.log(':::::::::::::::::::::::::::: 1');
        listMenusGet.map(item => {
          if (item.menuId == resultMenu.menuId) {
            //console.log("item match ::::: ", item);
            item.active = true;
          }
          else {
            item.active = false;
          }

          if (item.menuId == resultMenu.parentMenuId) {
            item.openSub = true;
            this.subMenuSlider("idSubMenu" + item.menuId, "open");
            parentIdtoClose = item.menuId;
          }
          else {
            item.openSub = false;
            this.subMenuSlider("idSubMenu" + item.menuId, "close");
            //parentIdtoClose = item.parentMenuId;
          }
          var spanMenu = $("#menuLinkId" + item.menuId);
          spanMenu.removeClass("active");
        });
      }
      else {
        //console.log(':::::::::::::::::::::::::::: 2');

        //let menuIdClose = 0;

        listMenusGet.map(item => {
          if (item.menuId == resultMenu.menuId) {
            item.active = true;
            if (resultMenu.openSub == false) {
              item.openSub = true;
              this.subMenuSlider("idSubMenu" + item.menuId, "open");
              parentIdtoClose = item.menuId;
            }
            else {
              item.openSub = false;
              this.subMenuSlider("idSubMenu" + item.menuId, "close");
             
            }
          }
          else {
            item.active = false;
            item.openSub = false;
            // if (currentMenuID == item.menuId) {
            //   //this.subMenuSlider("idSubMenu" + item.menuId, "close");
            // }
            this.subMenuSlider("idSubMenu" + item.menuId, "close");
            //parentIdtoClose = item.menuId;
          }

          // if(menuIdClose>0 && currentMenuID == menuIdClose)
          // {
          //   this.subMenuSlider("idSubMenu" + menuIdClose, "close");
          // }

          var spanMenu = $("#menuLinkId" + item.menuId);
          spanMenu.removeClass("active");
        });
      }
      //console.log('resut listMenus :::::: ', listMenus);
      var spanMenu = $("#menuLinkId" + id);
      spanMenu.addClass('active');


      //console.log('previousParentMenuId::::::::::::::::::::', previousParentMenuId);
     
      // var div3 = $("#idSubMenu"+(resultMenu.parentMenuId > 0 ? resultMenu.parentMenuId : resultMenu.menuId));
      // console.log('MMMMMMMMMMMMMMMM blok div ::::::::::::::::::: 1 :', div3.css('display'));
      // console.log('ParentMenuId::::::::::::::::::::', (resultMenu.parentMenuId > 0 ? resultMenu.parentMenuId : resultMenu.menuId));
      //console.log('parentIdtoClose::::::::::::::::::::', parentIdtoClose);
      if(parentIdtoClose > 0)
      {
        localStorage.setItem('parentMenuId', JSON.stringify(parentIdtoClose));
      }
      else
      {
        localStorage.removeItem("parentMenuId");
      }


      //return listMenusGet;
      // this.listMenus = listMenus;
      var menu = $("#m_aside_left");
      menu.removeClass('m-aside-left--on');
    });

    

  }

}
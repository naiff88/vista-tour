import { AfterViewInit, Directive, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import * as moment from 'moment';

@Directive({
  selector: '[weekPicker]'
})
export class WeekPickerDirective implements AfterViewInit {
  hostElement: ElementRef;

  //_selectedDate: moment.Moment = moment().startOf('day');

  _selectedDate;
  _selectedDate_Check;

  @Output() selectedDateChange = new EventEmitter();

  @Input()
  get selectedDate() {
    return this._selectedDate;
  }

  set selectedDate(val) {
    this._selectedDate = val;
    this.selectedDateChange.emit(this._selectedDate);
    this.setElementText(val);
  }

  constructor(
    private _element: ElementRef
  ) {
    this.hostElement = _element;
  }

  ngAfterViewInit(): void {
    const $element = $(this.hostElement.nativeElement);
    $element.datepicker({
      todayHighlight: !0,
      format: 'dd/mm/yyyy',
      useCurrent: false,
      autoclose: true
    }).on('changeDate', e => {
      if (e.date) {
        this.selectedDate = moment(e.date);
      } else {
        this.selectedDate = null;
      } 
    });
  }

  setElementText(val: any) {
    const $element = $(this.hostElement.nativeElement);
    if (val) {
      $element.val(moment(val).format('DD/MM/YYYY'));
    } else {
      $element.val('');
    }
  }
}

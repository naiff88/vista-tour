import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { AppSessionService } from 'src/shared/common/session/app-session.service';
@Injectable()
export class AppRouteGuard implements CanActivate, CanActivateChild {

    constructor(
      private _router: Router,
      private _sessionService: AppSessionService
    ) { }

  canActivate(): boolean {
    this._router.navigate([this.selectBestRoute()]);
    return false;
  }

  canActivateChild(): boolean {
    return this.canActivate();
  }

  selectBestRoute(): string {

    if (!this._sessionService.userId) {
      return '/account/login';
    }

    return '/app/dashboard/menu';
  }

}

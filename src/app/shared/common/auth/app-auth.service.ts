import { Injectable } from '@angular/core';
import { CookieService } from 'src/shared/common/session/cookie.service';
import { Router } from '@angular/router';

@Injectable()
export class AppAuthService {

  constructor(
    private _cookieService: CookieService,
    private _router: Router
  ) {
  }

    logout(reload?: boolean, returnUrl?: string): void {
      this._cookieService.deleteCookie('spj_auth');
      localStorage.removeItem("listMenu");
      localStorage.removeItem("menuId");
      localStorage.removeItem("parentMenuId");

        if (reload) {
          if (returnUrl) {
            location.href = returnUrl;
          } else {
            location.href = '/account/login';
            }
        }

    }
}

import { ModuleWithProviders, NgModule } from '@angular/core';
import { AppAuthService } from './common/auth/app-auth.service';
import { AppRouteGuard } from './common/auth/auth-route-guard';
import { WeekPickerDirective } from './common/timing/week-picker.component';
import { DatePickerDirective } from './common/timing/date-picker.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from 'src/shared/common/common.module';
import { FormValidationErrorComponent } from './components/form-validation-error/form-validation-error.component';
import * as ngCommon from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import {
  ImageUploaderComponent,
  ImageUploaderDialogComponent
} from './components/image-uploader/image-uploader.component';
import {
  AdjustableImageUploaderComponent,
  AdjustableImageUploaderDialogComponent
} from './components/image-uploader/adjustable-image-uploader.component';
import { ImageCropperModule } from 'ngx-image-cropper';
// import {
//   MatButtonModule,
//   MatCheckboxModule,
//   MatDialogModule,
//   MatIconModule,
//   MatProgressSpinnerModule
// } from '@angular/material';


import { MatRippleModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
import { MatIconModule} from '@angular/material/icon';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort} from '@angular/material/sort';


import { ImageSliderComponent } from './components/image-slider/image-slider.component';
import { ImagePreviewDirective } from './components/image-slider/image-preview.directive';
import { OverlayModule } from '@angular/cdk/overlay';
import { ImagePreviewContentComponent } from './components/image-slider/image-preview-content.component';
import { ValidationMessagesComponent } from './components/validation-messages/validation-messages.component';
import { PrimengTableHelper } from './PrimengTableHelper';
import { ChangePasswordComponent } from './layout/change-password/change-password.component';
import { ProfileComponent } from './layout/profile/profile.component';
import { MatSpinnerOverlayComponent } from './common/spinner/spinner.component';
import { TwoDigitDecimaNumberDirective } from './validators/currency-input';
import { NumberOnly } from './validators/number-input';
import { AddDocumentComponent } from './components/add-document/add-document.component';
import { EmailOnly } from './validators/email-input';
// import {MatProgressSpinnerModule} from '@angular/material'
//import { MatButtonModule, MatCheckboxModule, MatSnackBar, MatSnackBarModule, MatDialogModule, MatIconModule, MatProgressSpinnerModule } from '@angular/material';

const materialModules = [
  MatDialogModule,
  MatButtonModule,
  MatCheckboxModule,
  MatIconModule,
  OverlayModule
];

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    ImageCropperModule,
    //...materialModules,
    ngCommon.CommonModule,
    MatDialogModule,
  MatButtonModule,
  MatCheckboxModule,
  MatIconModule,
  OverlayModule,
    //ProfileComponent,
    MatProgressSpinnerModule
    
  ],
  declarations: [
    FormValidationErrorComponent,
    ImageUploaderComponent,
    ImageUploaderDialogComponent,
    AdjustableImageUploaderComponent,
    AdjustableImageUploaderDialogComponent,
    ImageSliderComponent,
    ImagePreviewDirective,
    ImagePreviewContentComponent,
    DatePickerDirective,
    WeekPickerDirective,
    ValidationMessagesComponent,
    ChangePasswordComponent,
    ProfileComponent,
    AddDocumentComponent,
    MatSpinnerOverlayComponent,
    TwoDigitDecimaNumberDirective,
    EmailOnly,
    NumberOnly
  ],
  exports: [
    FormValidationErrorComponent,
    ValidationMessagesComponent,
    ImageUploaderComponent,
    ImageSliderComponent,
    DatePickerDirective,
    WeekPickerDirective,
    AdjustableImageUploaderComponent,
    AdjustableImageUploaderDialogComponent,
    ChangePasswordComponent,
    ProfileComponent,
    AddDocumentComponent,
    MatSpinnerOverlayComponent,
    TwoDigitDecimaNumberDirective,
    EmailOnly,
    NumberOnly
  ],
  entryComponents: [ImageUploaderDialogComponent,
    ImagePreviewContentComponent,
    AdjustableImageUploaderComponent,
    AdjustableImageUploaderDialogComponent,
    ChangePasswordComponent,
    ProfileComponent,
    AddDocumentComponent
  ]
})
export class AppCommonModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppCommonModule,
      providers: [AppAuthService, AppRouteGuard]
    };
  }
}

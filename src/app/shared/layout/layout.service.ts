import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { bufferTime, distinctUntilChanged, filter, map, scan, share, take, tap } from 'rxjs/operators';
import {
  ProgressBarState,
  Pair,
  MySettingProfile
} from './models';
import * as _ from 'lodash';
import { API_BASE_URL } from 'src/shared/service-proxies/service-proxies';

export enum SettingsApiEndpoint {
  SettingsProfile = '/api/profile/user/setting',
  UploadImage = '/api/upload/image'

}

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  /**
   * Indicate data processing. Binded in dialog
   */
  private processingSubject = new Subject<ProgressBarState>();
  private baseUrl: string;
  http: HttpClient;


  /**
   *  Pipeline of async operation to handle loading state of product dialog
   */
  isBusyStream: Observable<boolean> = this.processingSubject.asObservable()
    .pipe(
      bufferTime(300),                  // buffers the progress Observable values for 100ms period
      filter(states => states.length > 0),   // ignore empty streams
      scan((total: number, states: ProgressBarState[]) => total + _.sum(states), 0),
      map((diff: number) => diff !== 0),       // diff between count of processing and already completed http requests
      distinctUntilChanged(),
      share()                                         // Returns a new Observable that multicasts (shares) the original Observable
    );

  set isBusy(value: boolean) {
    const state: number = value ? ProgressBarState.Show : ProgressBarState.Hide;
    this.processingSubject.next(state);
  }

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }


  ///////////////////////////////////////////////////
  //////////////// SETTINGS SERVICE /////////////////
  ///////////////////////////////////////////////////

  updateProfile(model: MySettingProfile): Observable<any> {
    // const params = new HttpParams().set('SettingId', String(projectId));
    console.log('upsertSetting model ::::::::::: ', model);
    // console.log('params ::::::::::: ', params);
    this.isBusy = true;
    console.log('--update--');
      // model.ProfileId = projectId;
    console.log('--model-->', model);
    return this.http.put<void>(this.baseUrl + SettingsApiEndpoint.SettingsProfile, model)
        .pipe(
          map((resp: any) => {
            console.log('upsertSetting update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            return { ReturnCode, ResponseMessage};
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
  }

  getProfileInfo(): Observable<any> {
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SettingsApiEndpoint.SettingsProfile)
      .pipe(
        map((resp: any) => {
          console.log('getProfileInfo--->', resp);
          const model = resp as any;
          console.log('model---getSettingProfileInfo--->', model);
          // model.historyList = resp.historyList as InquiryHistoryList[];
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
  }

  uploadPhoto(data: FormData): Observable<string> {
    this.isBusy = true;
    return this.http.post<string>(this.baseUrl + SettingsApiEndpoint.UploadImage, data)
      .pipe(
        map((resp: any) => resp.Result[0].ImageFileName),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
  }
}

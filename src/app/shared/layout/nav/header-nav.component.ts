import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from './../../../helpers';
import { AppAuthService } from '../../common/auth/app-auth.service';
import { LoginServiceProxy } from 'src/shared/service-proxies/auth-service-proxies';
import * as ns from 'src/shared/service-proxies/service-proxies';
import { ServiceProxiesHelper } from 'src/shared/helpers/ServiceHelper';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { ProfileComponent } from '../profile/profile.component';
import { MatDialog } from '@angular/material/dialog';
import * as $ from 'jquery';
declare let mLayout: any;

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  // encapsulation: ViewEncapsulation.None
  styleUrls: ['./header-nav.component.css']
})

export class HeaderNavComponent implements OnInit, AfterViewInit {
  data: any = {};
  helper = new ServiceProxiesHelper();

  constructor(
    private _appAuthService: AppAuthService,
    private _login: LoginServiceProxy,
    private _loginProxy: ns.LoginServiceProxy,
    private dialog: MatDialog,
  ) {

  }
  ngOnInit() {
   
  }
  ngAfterViewInit() {
   
    this._loginProxy.userDetail().subscribe(r => {
      //console.log('userDetail header:::::::::::::', r);
      this.data = this.helper.toCamel(r);
    });
    mLayout.initHeader();
  }

  openProfileDialog() {
    const dialogRef = this.dialog.open(ProfileComponent, {
      data: {
        profilePopup: true
      },
      autoFocus: false,
      minHeight: '500px',
      maxHeight: '90vh'
      // maxWidth: '1000px',
    });
    dialogRef.afterClosed().subscribe(result => {
      // if (result) {
      //   this.service.deleteInvoiceItem(selectedItems)
      //     .subscribe(() => this.getInvoiceItem());
      //   this.toastr.info('Successfully Deleted', 'Item Deleted', {
      //     timeOut: 3000
      //   });
      // }
      this._loginProxy.userDetail().subscribe(r => {

        this.data = this.helper.toCamel(r);
      });
      mLayout.initHeader();
    });
  }

  onClickMenu() {
    //console.log('onClickMenu :::::::::::::::::::: ');
    //var body = $("body");
    //console.log('onClickMenu body :::::::::::::::::::: ', body);   
    var body = $("#body_content");
    var menu = $("#m_aside_left");
    //console.log('onClickMenu menu :::::::::::::::::::: ', menu);   
    if(menu.hasClass('m-aside-left--on') || body.hasClass('m-aside-left--on'))
    {
      menu.removeClass('m-aside-left--on');
      body.removeClass('m-aside-left--on');
    }
    else
    {
      menu.addClass('m-aside-left--on');
    }
    //this.dialog.open(ChangePasswordComponent);
  }

  onClickHeader() {
    var body = $("#body_content");
    // console.log('onClickHeader body :::::::::::::::::::: ', body);
    // console.log('onClickHeader body ADA :::::::::::::::::::: ', body.hasClass('m-topbar--on'));
    //console.log('onClickMenu :::::::::::::::::::: ');
    //var div = $("#m_aside_left_offcanvas_toggle");
    //console.log('onClickMenu div :::::::::::::::::::: ', div);
    //var menu = $("#m_header_topbar");
    //console.log('onClickMenu menu :::::::::::::::::::: ', menu);
    if(body.hasClass('m-topbar--on'))
    {
      body.removeClass('m-topbar--on');
      //$('body:not([class])').removeClass('m-topbar--on');
    }
    else
    {

      // var urlPath = window.location.pathname;
      // //convert to lowercase
      // urlPath.match(/\/(.*?)(\+|$)/)[1].toLowerCase();
      // // remove the first character
      // urlPath = urlPath.replace(/\//g, '-').slice(1);
      // //remove file extension
      // urlPath = urlPath.replace(/\.[^/.]+$/, "");
      // // add class to body
      // $(document.body).addClass(urlPath);      
      // If body has no class its propably the homepage
      body.addClass('m-topbar--on');
      //$("body").attr("class", "m-topbar--on");
      //console.log('adddd top ----- ');
      //$('body').addClass("m-topbar--on");
      // var body1 = document.body;
      // body1.classList.add("m-topbar--on");
    }
    //$(document.body).toggleClass('m-topbar--on');
    // var body1 = $("body");
    // console.log('onClickHeader body AFTER :::::::::::::::::::: ', body1);
    //this.dialog.open(ChangePasswordComponent);
  }


  openChangePasswordDialog() {
    this.dialog.open(ChangePasswordComponent);
  }

  logout(): void {
    this._appAuthService.logout(true);
  }
}

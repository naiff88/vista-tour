import {ChangeDetectorRef, Component, ElementRef, Inject, Input, OnInit, ViewChild} from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
import {FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MySettingProfile} from '../models';
import {Observable, of} from 'rxjs';
import {LayoutService} from '../layout.service';
import {ReferenceServiceProxy} from '../../../../shared/service-proxies/reference-service-proxies';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  form: FormGroup;
  ImageUrl: any = '';
  Attachment = '';
  imageChangedEvent: any = '';
  checkImageChangedEvent: boolean = false;
  croppedImage: any = '';
  croppedImageFile: any = '';
  @Input() profilePopup: boolean = false;
  @ViewChild('inputFile', {static: true}) inputFile: ElementRef;
  showSpinner: boolean = false;
 // private subscriptions: Subscription = new Subscription();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { profilePopup: boolean },
    public dialogRef: MatDialogRef<ProfileComponent>,
    private service: LayoutService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private cdRef: ChangeDetectorRef,
    private _referenceService: ReferenceServiceProxy,
    private router: Router) { }

  ngOnInit() {
    this.profilePopup = this.data.profilePopup;
    this.loadDetails();
  }

  onClose(): void {
    this.dialogRef.close();
  }

  loadDetails() {
    this.form = this.buildFormItems();
    this.service.getProfileInfo()
        .subscribe(result => {
          console.log('loadDetails result', result);
          this.ImageUrl = result.ProfileImage;
          this.Attachment = '';
          // this.ProductId = result.ProductId > 0 ? result.ProductId : 0;
          // this.ServiceId = result.ServiceId > 0 ? result.ServiceId : 0;
          // this.totalPrice = this.calcTotalPrice(result.Price, result.Quantity);
          this.form.patchValue(result as any);
        });
  }

  cancelUpload() {
    // console.log("cancelUpload ::::::::::::::: ");
    this.Attachment = '';
    this.inputFile.nativeElement.value = '';
    this.ImageUrl = '';
    document.getElementById('displayFile').innerHTML = '';
  }

  save() {
    this.showSpinner = true;
    console.log('Save profile this.form :::::::::::::::: ', this.form.value);
    //console.log('Save profileVALID :::::::::::::::: ', this.form);

    if (this.form.invalid) {
      return of(); // Exit
    }

    // const model = this.form.value as MySettingProfile;
    if (this.form.get('Attachment').value) {
      const formData = new FormData();
      formData.append('file', this.form.get('Attachment').value);
      // formData.append('file', this.croppedImageFile);
      this.service.uploadPhoto(formData)
        .subscribe(resUrl => {
          // console.log('uploadPhoto docUrl ::::::::::::::::: ', resUrl);
          this.ImageUrl = resUrl;
          // console.log('1 sendChat docUrl ::::::::::::::::: ', resUrl);
          if (resUrl !== '') {
            this.saveProfile(this.ImageUrl);
          }
        }, (err) => console.log(err));
      // this.saveStaff(imageUrl: string)
    } else {
      this.saveProfile(this.ImageUrl);
    }
    /*this.service.upsertQuotationItem(model, this.data.projectId, this.data.itemId)
      .subscribe(r => {
        // r = toCamel(r);
        console.log('ReturnCode---->', r.ReturnCode);
        const configSnakBar: MatSnackBarConfig = {
          duration: 3000,
          verticalPosition: 'bottom',
          horizontalPosition: 'left',
        };
        if (r.ReturnCode === 200) {
          this.snackBar.open(this.data.projectId > 0 ? 'Item Successfully Updated' : 'Item Successfully Added', 'OK', configSnakBar);
          this.data.projectId = r.id;
          this.data.projectId = r.itemId;
          this.dialogRef.close();
          this.loadDetails(this.data.projectId, this.data.itemId);
        } else {
          this.snackBar.open('Error', 'OK', configSnakBar);
        }
      });*/
  }

  saveProfile(imageUrl: string) {
    if (this.form.invalid) {
      return of(); // Exit
    }
    let model = this.form.value as MySettingProfile;
    model.ImageUrl = imageUrl;
    // console.log('saveStaff ::::::::: ',model);
    
    this.service.updateProfile(model)
      .subscribe(r => {
        const configSnakBar: MatSnackBarConfig = {
          duration: 2000,
          verticalPosition: 'bottom',
        };
        this.showSpinner = false;
        if (r.ReturnCode === 200) {
          //this.snackBar.open('Profile Successfully Updated', 'OK', configSnakBar);
          this.loadDetails();
          Swal.fire(
            'Profile Successfully Updated',
            r.ResponseMessage,
            'success'
          );
          
        } else {
          //this.snackBar.open('Error', 'OK', configSnakBar);
          Swal.fire(
            'Error',
            'Error',
            'error'
          );
        }
      });
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      this.imageChangedEvent = event;
      this.checkImageChangedEvent = true;
    }
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.checkImageChangedEvent = true;
  }

  imageCropped(event: ImageCroppedEvent) {
    // console.log('imageCropped ImageUrl :::::::::::::::: ', this.ImageUrl);
    this.croppedImage = event.base64;
  }

  closeCropped() {
    console.log('-------closeCropped-------');
    this.ImageUrl = this.croppedImage;
    console.log('this.ImageUrl::', this.ImageUrl);
    this.croppedImageFile = this.base64ImageToBlob(this.croppedImage);
    console.log('this.croppedImageFile::', this.croppedImageFile);
    this.form.get('Attachment').setValue(this.croppedImageFile);
    this.imageChangedEvent = '';
    this.checkImageChangedEvent = false;
  }

  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }


  base64ImageToBlob(str) {
    // extract content type and base64 payload from original string
    let pos = str.indexOf(';base64,');
    let type = str.substring(5, pos);
    let b64 = str.substr(pos + 8);
    // decode base64
    let imageContent = atob(b64);
    // create an ArrayBuffer and a view (as unsigned 8-bit)
    let buffer = new ArrayBuffer(imageContent.length);
    let view = new Uint8Array(buffer);
    // fill the view, using the decoded base64
    for (let n = 0; n < imageContent.length; n++) {
      view[n] = imageContent.charCodeAt(n);
    }
    // convert ArrayBuffer to Blob
    let blob = new Blob([buffer], { type });
    return blob;
  }

  isValid(): boolean {
    return this.form.valid;
  }

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      Fullname: ['', [Validators.required]],
      Email: ['', [Validators.required]],
      PhoneNo: ['', [Validators.required]],
      Attachment: ['', []],
    });
  }

  cancel(): void {
    this.router.navigate(['/app/dashboard/menu']);
  }

}

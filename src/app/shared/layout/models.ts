import { Type } from '@angular/compiler';

// SETTING MODEL
export interface MySettingProfile {
  Fullname: string;
  Email: string;
  PhoneNo: string;
  UserRole: string;
  ImageUrl: string;
}


// OTHER MODEL
export interface Pair {
  id: number;
  name: string;
}

export enum ProgressBarState {
  Hide = -1,
  Show = 1
}

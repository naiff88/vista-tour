import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { LoginServiceProxy, ChangePasswordRequest } from '@service-proxies';
import { toCamel } from 'src/shared/helpers/ServiceHelper';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})

export class ChangePasswordComponent implements OnInit {

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<ChangePasswordComponent>,
    private loginService: LoginServiceProxy,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.form = this.buildFormItems();
  }

  public isValid(): boolean {

    return this.form.valid;
  }

  get newPwd() {
    return this.form.get('newPassword');
  }

  get oldPwd() {
    return this.form.get('currentPassword');
  }

  get confirmPwd() {
    return this.form.get('confirmPassword');
  }

  cancel(): void {
    this.dialogRef.close();
  }

  checkPasswords(group: FormGroup) {
    return group.get('confirmPassword').value === group.get('newPassword').value ? null : { notSame: true };
  }
  change() {
    if (!this.isValid()) {
      return;
    }
    this.loginService
      .changePassword(new ChangePasswordRequest({ newPassword: this.newPwd.value, oldPassword: this.oldPwd.value }))
      .subscribe(r => {
        r = toCamel(r);
        if (r.returnCode === 200) {
          this.toastr.info('Successfully Updated Password', 'Successful', {
            timeOut: 3000
          });
          this.dialogRef.close(true);
        } else {
          this.toastr.error(r.responseMessage, 'Error', {
            timeOut: 3000
          });
        }
      })

  }
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      currentPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
    }, { validator: this.checkPasswords }
    );
  }
}

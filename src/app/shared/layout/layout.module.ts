import { NgModule } from '@angular/core';
import { HeaderNavComponent } from './nav/header-nav.component';
import { RouterModule } from '@angular/router';
import { HrefPreventDefaultDirective } from '../_directives/href-prevent-default.directive';
import { UnwrapTagDirective } from '../_directives/unwrap-tag.directive';
import { FooterComponent } from './footer/footer.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    HeaderNavComponent,
    FooterComponent,
    ScrollTopComponent,
    HrefPreventDefaultDirective,
    UnwrapTagDirective,
  ],
  exports: [
    HeaderNavComponent,
    FooterComponent,
    ScrollTopComponent,
    HrefPreventDefaultDirective,
    UnwrapTagDirective,
  ],
  imports: [
    RouterModule,
    CommonModule
  ]


})

export class LayoutModule {
}

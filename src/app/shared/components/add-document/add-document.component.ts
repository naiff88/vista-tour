import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  PMServiceProxy,
  ProjectManagementUpsert as Project,
  ProjectManagementTaskDocumentUpsert,
  ProjectManagementDocumentUpsert
} from '@service-proxies';
//import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


// import { MatRippleModule, MAT_DATE_LOCALE } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
// import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
// import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';


import { ToastrService } from 'ngx-toastr';
// import { QuoteMeService } from '../../../quote-me/quote-me.service';
import { Observable, of } from 'rxjs';
import { toCamel } from 'src/shared/helpers/ServiceHelper';
import { ReferenceServiceProxy } from 'src/shared/service-proxies/reference-service-proxies';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-document',
  templateUrl: './add-document.component.html',
  styleUrls: ['./add-document.component.scss']
})

export class AddDocumentComponent implements OnInit {
  projectId: number;
  form: FormGroup;
  uploaded = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    private projectService: PMServiceProxy,
    public dialogRef: MatDialogRef<AddDocumentComponent>,
    private toastr: ToastrService,
    // private quoteMe: QuoteMeService,
    private router: Router,
    private referenceService: ReferenceServiceProxy,
    @Inject(MAT_DIALOG_DATA) public data: { parentId: number, projectId: number, taskId: number }) { }

  ngOnInit() {
    this.form = this.buildFormItems();
  }
  public isValid(): boolean {
    return this.form.valid;
  }

  cancel(): void {
    this.dialogRef.close();
  }

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      attachmentURL: ['', [Validators.required]],
      flagShare: [false, []],
      description: ['', []],
      projectManagementTaskId: [this.data.taskId, []],
      projectManagementId: [this.data.projectId, []],
    }, {}
    );
  }

  uploadDocument(fileName) {
    const formData = new FormData();
    formData.append('doc', fileName[0].target.files[0]);
    this.referenceService.uploadFile(formData).subscribe(f => {
      this.form.patchValue({
        attachmentURL: f
      });
    });
  }

  save(): Observable<void> {
    if (this.form.invalid) {
      return of(); // Exitid
    }

    if (this.data.taskId) {
      const input = this.form.value as ProjectManagementTaskDocumentUpsert;

      if (this.form.value.flagShare === false) {
        input.flagShare = 0;
      } else {
        input.flagShare = 1;
      }

      this.projectService.insertPMInsertProjectTaskDocument(input)
        .subscribe(r => {
          r = toCamel(r);

          if (r.returnCode === 200) {
            this.toastr.success('Successfully Added', 'Document Added', {
              timeOut: 3000
            });
            this.uploaded.emit();
            this.dialogRef.close(true);
          } else {
            this.toastr.error(r.responseMessage, 'Error', {
              timeOut: 3000
            });
          }
        });

    } else {
      const input = this.form.value as ProjectManagementDocumentUpsert;

      this.projectService.insertPMInsertProjectDocument(input)
        .subscribe(r => {
          r = toCamel(r);

          if (r.returnCode === 200) {
            this.toastr.success('Successfully Added', 'Document Added', {
              timeOut: 3000
            });
            this.uploaded.emit();
            this.dialogRef.close(true);
          } else {
            this.toastr.error(r.responseMessage, 'Error', {
              timeOut: 3000
            });
          }
        });
    }

  }
}

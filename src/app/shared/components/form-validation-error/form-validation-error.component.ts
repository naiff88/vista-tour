import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'validation-error',
  template: `
    <div class="text-danger" *ngIf="control && control.errors && (control.dirty || control.touched)">
      <div *ngIf="control.errors.required"><small>This field is required</small></div>
      <div *ngIf="control.errors.maxlength"><small>More than {{ control.errors.maxlength.requiredLength }} symbols</small></div>
      <!-- TODO: all possible validation errors -->
    </div>
  `,
})
export class FormValidationErrorComponent {

  @Input()
  control: AbstractControl;
}

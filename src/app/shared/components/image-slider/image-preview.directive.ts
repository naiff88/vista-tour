import { AfterViewInit, ContentChild, Directive, ElementRef, HostListener, OnInit, } from '@angular/core';
import { Overlay, OverlayPositionBuilder, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { ImagePreviewContentComponent } from './image-preview-content.component';
import { ESCAPE } from '@angular/cdk/keycodes';
import { merge, pipe } from 'rxjs';
import { filter } from 'rxjs/operators';

@Directive({ selector: '[imagePreview]' })
export class ImagePreviewDirective implements AfterViewInit, OnInit {

  @ContentChild('previewContent')
  previewContent: ElementRef;

  private contentCopy: HTMLElement;

  private overlayRef: OverlayRef;

  constructor(
    private overlay: Overlay,
    private overlayPositionBuilder: OverlayPositionBuilder,
    private elementRef: ElementRef,
  ) {}

  ngOnInit(): void {
    const positionStrategy = this.overlay.position().global().centerHorizontally().centerVertically();
    this.overlayRef = this.overlay.create({
      positionStrategy,
      hasBackdrop: true
    });
  }

  ngAfterViewInit() {
    if (this.previewContent) {
      this.contentCopy = this.previewContent.nativeElement.cloneNode(true);
      this.elementRef.nativeElement.children[0].style.display = 'none';
    }
  }

  @HostListener('click')
  show() {
    const tooltipRef = this.overlayRef.attach(new ComponentPortal(ImagePreviewContentComponent));
    tooltipRef.instance.content = this.contentCopy;
    tooltipRef.instance.overlayRef = this.overlayRef;

    merge(
      this.overlayRef.backdropClick(),
      this.overlayRef.keydownEvents().pipe(filter((event: KeyboardEvent) => event.keyCode === ESCAPE))
    );
     // .subscribe(() => this.overlayRef.dispose());
  }
}

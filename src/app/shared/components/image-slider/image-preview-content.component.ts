import { ChangeDetectionStrategy, Component, ElementRef, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { OverlayRef } from '@angular/cdk/overlay';

@Component({
  selector: 'image-preview-content',
  template: `
    <div @tooltip #container class="image-container"></div>
  `,
  styleUrls: ['./image-preview-content.component.scss'],
  animations: [
    trigger('tooltip', [
      transition(':enter', [style({ opacity: 0 }), animate('400ms', style({ opacity: 1 }))]),
      transition(':leave', [animate('200ms', style({ opacity: 0 }))]),
    ])
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImagePreviewContentComponent implements OnInit {
  content: HTMLElement;
  overlayRef: OverlayRef;

  @ViewChild('container', { static: true })
  private containerRef: ElementRef;

  //private render: Renderer2;

  constructor(private elementRef: ElementRef,
              private renderer: Renderer2) {
                //this.containerRef = elementRef;
                //this.render = renderer;
              }

  ngOnInit() {
    if (this.content) {
      // console.log('(this.containerRef.nativeElement ::::::::::::::::: ', this.containerRef);
      // console.log('(this.containerRef.nativeElement content ::::::::::::::::: ', this.content);
      this.renderer.appendChild(this.containerRef.nativeElement, this.content);
    }
  }

  @HostListener('click')
  hide() {
    this.overlayRef.detach();
  }
}

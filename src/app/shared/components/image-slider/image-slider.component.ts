import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AbstractControl, FormArray } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as $ from 'jquery';

@Component({
  selector: 'app-image-slider',
  templateUrl: './image-slider.component.html',
  styleUrls: ['./image-slider.component.scss']
})
export class ImageSliderComponent {
  @Output()
  select: EventEmitter<File> = new EventEmitter<File>();

  @Output()
  delete: EventEmitter<{index: number, imageList: any}> = new EventEmitter<{index: number, imageList: any}>();

  //imageThreshold = 10;

  @Input()
  imageList: FormArray;

  @Input()
  editable: boolean = true;

  @Input()
  imageThreshold: number = 10;

  // constructor() {
  //   $(document).ready(() => {
  //       // console.log('Hello from app/scripts/ts/Main.ts');
  //       ($( "#sortable" ) as any).sortable();
  //       ($( "#sortable" ) as any).disableSelection();
  //   });
  // }
  

  onUploadImageDialog(file: File) {
    //console.log('file :::::::::::: ', file);
    this.select.emit(file);
  }

  // Handle single selected default image
  onSelectDefault(index: number) {
    this.imageList.controls
      .forEach((control: AbstractControl, i: number) =>
        control.patchValue({...control.value, isDefault: i === index})
      );
    this.imageList.markAsDirty();
  }

  removeImageAt(index: number) {
    this.delete.emit({index, imageList: this.imageList});
    // this.imageList.removeAt(index);
    // this.imageList.updateValueAndValidity();
    // this.imageList.markAsDirty();
  }
}

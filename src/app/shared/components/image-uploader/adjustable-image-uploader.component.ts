import {
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
  ViewChild
} from '@angular/core';
import * as _ from 'lodash';
import { ImageCroppedEvent } from 'ngx-image-cropper';
//import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';


// import { MatRippleModule, MAT_DATE_LOCALE } from '@angular/material/core';
// import { MatCardModule } from '@angular/material/card';
// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
// import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
// import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';


/**
 * Component manage upload image and crop it with aspectRatio
 */

@Component({
  selector: 'app-adjustable-image-uploader',
  template: `
    <input
      #fileInput
      type="file"
      name="file"
      [id]="uniqueId"
      accept="image/*"
      (change)="openDialog($event)"
      style="display: none"
    />
    <label (click)="fileInput.click()" [for]="uniqueId">
      <ng-content></ng-content>
    </label>
  `
})
export class AdjustableImageUploaderComponent {
  uniqueId = _.uniqueId('file_');

  @ViewChild('fileInput')
  fileInput: any;

  @Output()
  select: EventEmitter<File> = new EventEmitter<File>();

  @Input()
  aspectRatio: number = 1 / 1;

  @Input()
  resizeToWidth = 500;

  @Input()
  maintainAspectRatio: boolean;

  constructor(private dialog: MatDialog) {}

  openDialog(imageChangedEvent: any): void {
    const dialogRef = this.dialog.open(AdjustableImageUploaderDialogComponent, {
      data: {
        imageChangedEvent,
        aspectRatio: this.aspectRatio,
        resizeToWidth: this.resizeToWidth,
        maintainAspectRatio: this.maintainAspectRatio
      },
      width: '450px',
      height: '450px'
    });

    dialogRef.afterClosed().subscribe(file => this.onCloseDialog(file));
  }

  private onCloseDialog(file) {
    if (file) {
      this.select.emit(file);
    }
    this.fileInput.nativeElement.files = null; // reset selected file
  }
}

/**
 * Drill down dialog
 */
@Component({
  selector: 'app-adjustable-image-uploader-dialog',
  template: `
    <h3 mat-dialog-title>Crop product picture</h3>
    <div mat-dialog-content>
      <div style="height: 300px; width: 100%;">
        <image-cropper
          [imageChangedEvent]="data.imageChangedEvent"
          [maintainAspectRatio]="true"
          [aspectRatio]="data.aspectRatio"
          [resizeToWidth]="data.resizeToWidth"
          [onlyScaleDown]="true"
          [imageQuality]="100"
          format="png"
          (imageCropped)="imageCropped($event)"
        >
        </image-cropper>
      </div>
    </div>
    <div mat-dialog-actions style="justify-content: flex-end">
      <button
        mat-raised-button
        [disabled]="!croppedImageFile"
        [mat-dialog-close]="croppedImageFile"
        cdkFocusInitial
      >
        <mat-icon>crop</mat-icon>
        Crop
      </button>
    </div>
  `
})
export class AdjustableImageUploaderDialogComponent {
  croppedImageFile: Blob;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { imageChangedEvent: any }
  ) {}

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImageFile = event.file;
  }
}

import { Component, EventEmitter, Inject, Input, Output, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-image-uploader',
  template: `
    <input #fileInput type="file" name="file" [id]="uniqueId" accept="image/*" (change)="openDialog($event)" style="display: none">
    <label [for]="uniqueId" style="cursor: pointer;">
      <ng-content></ng-content>
    </label>
  `,
})
export class ImageUploaderComponent {

  uniqueId = _.uniqueId('file_');

  @ViewChild('fileInput')
  fileInput: any;

  @Output()
  select: EventEmitter<File> = new EventEmitter<File>();

  constructor(private dialog: MatDialog) {
  }

  openDialog(imageChangedEvent: any): void {
    const dialogRef = this.dialog.open(ImageUploaderDialogComponent, {
      data: {imageChangedEvent},
      width: '450px',
      height: '450px',
    });

    dialogRef.afterClosed()
      .subscribe(file => this.onCloseDialog(file));
  }

  private onCloseDialog(file) {
    if (file) {
      this.select.emit(file);
    }
    this.fileInput.nativeElement.files = null; // reset selected file
  }
}

/**
 * Drill down dialog
 */
@Component({
  selector: 'app-image-uploader-dialog',
  template: `
    <h3 mat-dialog-title>Crop product picture</h3>
    <div mat-dialog-content>
      <div style="height: 300px; width: 100%;">
        <image-cropper
          [imageChangedEvent]="data.imageChangedEvent"
          [maintainAspectRatio]="true"
          [aspectRatio]="1 / 1"
          [resizeToWidth]="500"
          [onlyScaleDown]="true"
          [imageQuality]="100"
          format="png"
          (imageCropped)="imageCropped($event)">
        </image-cropper>
      </div>
    </div>
    <div mat-dialog-actions style="justify-content: flex-end">
      <button mat-raised-button [disabled]="!croppedImageFile" [mat-dialog-close]="croppedImageFile" cdkFocusInitial>
        <mat-icon>crop</mat-icon>
        Crop
      </button>
    </div>
  `
})
export class ImageUploaderDialogComponent {
  croppedImageFile: Blob;

  constructor(@Inject(MAT_DIALOG_DATA) public data: { imageChangedEvent: any }) {
  }

  imageCropped(event: ImageCroppedEvent) {
    // console.log('imageCropped event ::::::::::::: ', event);
    // console.log('imageCropped event file ::::::::::::: ', event.file);
    this.croppedImageFile = event.file;
  }
}

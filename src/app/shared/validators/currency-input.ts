// import { Directive, ElementRef, HostListener } from '@angular/core';
// @Directive({
//   selector: '[appTwoDigitDecimalNumber]',
// })
// export class TwoDigitDecimaNumberDirective {
//   private regex: RegExp = new RegExp(/^\d+[.,]?\d{0,2}$/g);// user can put . or , char.
// // input also cannot start from , or .
//   private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight', 'Del', 'Delete'];

//   constructor(private el: ElementRef) {
//   }

//   @HostListener('keydown', ['$event'])
//   onKeyDown(event: KeyboardEvent) {
//     console.log("TwoDigitDecimaNumberDirective :::::::::::::: ");
//     if (this.specialKeys.includes(event.key)) {
//       return;
//     }
//     const current: string = this.el.nativeElement.value;
//     const positionEnd = this.el.nativeElement.selectionEnd;
//     const position = this.el.nativeElement.selectionStart;
//     const next: string = [current.slice(0, position), event.key == 'Decimal' ? '.' : event.key, current.slice(positionEnd)].join('');
//     //const next: string = [current.slice(0, position), event.key == 'Decimal' ? '.' : event.key, current.slice(position)].join('');
//     if (next && !String(next).match(this.regex)) {
//       event.preventDefault();
//     }
//   }
// }




import { Directive, ElementRef, HostListener, Input } from '@angular/core';
@Directive({
  selector: '[appTwoDigitDecimaNumber]'
})

export class TwoDigitDecimaNumberDirective {
  
  private regex: RegExp = new RegExp(/^\d*\.?\d{0,2}$/g);
  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home'];

  //@Input('appTwoDigitDecimaNumber') decimal:any;
  //decimal: any = true;

  constructor(private el: ElementRef) {
  }
  @HostListener('keydown', ['$event'])

  onKeyDown(event: KeyboardEvent) {
    // Allow Backspace, tab, end, and home keys
    //if(this.decimal){  
      if (this.specialKeys.indexOf(event.key) !== -1) {
        return;
      }
      let current: string = this.el.nativeElement.value;
      //console.log('current :::::::::: ', current);
      let next: string = current.concat(event.key);
      //console.log('next :::::::::: ', next);
      if (next && !String(next).match(this.regex)) {
        event.preventDefault();
      }
    // }else{
    //   const charCode = (event.which) ? event.which : event.keyCode;
    //   if (charCode > 31 && (charCode < 48 || charCode > 105)) {
    //     event.preventDefault();
    //   }
    //   return;
    // }
  }
}  


// import { Directive, ElementRef, HostListener } from '@angular/core';
// @Directive({
//   selector: '[appTwoDigitDecimaNumber]'
// })
// export class TwoDigitDecimaNumberDirective {
//   private regex: RegExp = new RegExp(/^\d*\.?\d{0,2}$/g);
//   private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight']
//   constructor(private el: ElementRef) {
//   }
//   @HostListener('keydown', ['$event'])
//   onKeyDown(event: KeyboardEvent) {
//     console.log(this.el.nativeElement.value);
//     // Allow Backspace, tab, end, and home keys
//     if (this.specialKeys.indexOf(event.key) !== -1) {
//       return;
//     }
//     let current: string = this.el.nativeElement.value;
//     const position = this.el.nativeElement.selectionStart;
//     const positionEnd = this.el.nativeElement.selectionEnd;
//     //const next: string = [current.slice(0, position), event.key == 'Decimal' ? '.' : event.key, current.slice(position)].join('');
//     const next: string = [current.slice(0, position), event.key == 'Decimal' ? '.' : event.key, current.slice(positionEnd)].join('');
//     if (next && !String(next).match(this.regex)) {
//       event.preventDefault();
//     }
//   }
// }
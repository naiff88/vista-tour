



import { Directive, ElementRef, HostListener, Input } from '@angular/core';
@Directive({
  selector: '[numberOnly]'
})

export class NumberOnly {
  
  private regex: RegExp = new RegExp(/^\d+$/);
  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', 'Dash'];

  //@Input('appTwoDigitDecimaNumber') decimal:any;
  //decimal: any = true;

  constructor(private el: ElementRef) {
  }
  @HostListener('keydown', ['$event'])

  onKeyDown(event: KeyboardEvent) {
    
      if (this.specialKeys.indexOf(event.key) !== -1) {
        return;
      }
      let current: string = this.el.nativeElement.value;
      //console.log('current :::::::::: ', current);
      let next: string = current.concat(event.key);
      //console.log('next :::::::::: ', next);
      if (next && !String(next).match(this.regex)) {
        event.preventDefault();
      }
   
  }
}  






import { Directive, ElementRef, HostListener, Input } from '@angular/core';
@Directive({
  selector: '[emailOnly]'
})

export class EmailOnly {
  
  private regex: RegExp = new RegExp(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/);
  //private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', 'Dash'];

  //@Input('appTwoDigitDecimaNumber') decimal:any;
  //decimal: any = true;

  constructor(private el: ElementRef) {
  }
  @HostListener('keydown', ['$event'])

  onKeyDown(event: KeyboardEvent) {
      console.log('event :::::::::: ', event);
      // if (this.specialKeys.indexOf(event.key) !== -1) {
      //   return;
      // }
      let current: string = this.el.nativeElement.value;
      console.log('current :::::::::: ', current);
      let next: string = current.concat(event.key);
      console.log('next :::::::::: ', next);
      if (next && !String(next).match(this.regex)) {
        event.preventDefault();
      }
   
  }
}  


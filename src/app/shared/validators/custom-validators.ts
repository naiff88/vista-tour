import { AbstractControl, FormArray } from '@angular/forms';
import * as _ from 'lodash';

export class CustomValidators {

  static hasDefaultImage = (control: AbstractControl): { [key: string]: boolean } => {
    const formArray = control.get('imageList') as FormArray;
    // const checkActive = control.get('activeStatus') as FormArray;

    // if(!checkActive){
    //   return {  }
    // }

    if (!formArray || !formArray.length) {
      return { noDefaultImage: true };
    }
    const selectedItem = _.some(formArray.value, 'isDefault');
    return selectedItem ? null : { noDefaultImage: true };
  }

  twoDecimals(e: any) {
    console.log('twoDecimals ::::::::::::::::::::::', e);
    let input = String.fromCharCode(e.charCode);
    console.log('input ::::::::::::::::::::::', input);
    const reg = /^\d*(?:[.,]\d{1,2})?$/;

    if (!reg.test(input)) {
      e.preventDefault();
    }
  }
}

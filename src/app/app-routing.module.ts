import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
//import { MenuComponent } from './menu/menu.component';
import { AppRouteGuard } from './shared/common/auth/auth-route-guard';
import { OrderListComponent } from './sales/orders/list/list.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'app',
        component: AppComponent,
        //component: MenuComponent,
        children: [
          // {
          //   path: 'menu',
          //   component: MenuComponent,
          //   // children: [
          //   //   { path: '', redirectTo: '/admin/references', pathMatch: 'full' },
          //   // ]
          // },
          /*{
            path: 'admin',
            loadChildren: './admin/admin.module#AdminModule', //Lazy load admin module
            data: { preload: false }
          },*/
          {
            path: 'dashboard',
            loadChildren: './dashboard/dashboard.module#DashboardModule', // Lazy load admin module
            data: { preload: true }
          },
          {
            path: 'catalogue',
            loadChildren: './catalogue/catalogue.module#CatalogueModule', // Lazy load catalogue module
            data: { preload: false }
          },
          {
            path: 'sales',
            loadChildren: './sales/sales.module#SalesModule', // Lazy load catalogue module
            data: { preload: false }
          },
          {
            path: 'pentadbiran',
            loadChildren: './pentadbiran/pentadbiran.module#PentadbiranModule', // Lazy load catalogue module
            data: { preload: false }
          },
          {
            path: 'handheld',
            loadChildren: './handheld/handheld.module#HandheldModule', // Lazy load catalogue module
            data: { preload: false }
          },
          {
            path: 'laporan',
            loadChildren: './laporan/laporan.module#LaporanModule',
            data: { preload: false }
          },
          {
            path: 'notis',
            loadChildren: './notis/notis.module#NotisModule', // Lazy load catalogue module
            data: { preload: false }
          },
          {
            path: 'rayuan',
            loadChildren: './rayuan/rayuan.module#RayuanModule', // Lazy load catalogue module
            data: { preload: false }
          },
          {
            path: 'mahkamah',
            loadChildren: './mahkamah/mahkamah.module#MahkamahModule', // Lazy load catalogue module
            data: { preload: false }
          },
          {
            path: 'esitaan',
            loadChildren: './esitaan/esitaan.module#EsitaanModule', // Lazy load catalogue module
            data: { preload: false }
          },
          /*{
            path: 'misc',
            loadChildren: './misc/misc.module#MiscModule', //Lazy load admin module
            data: { preload: false }
          },
          {
            path: 'my/products',
            loadChildren: './my-products/my-products.module#MyProductsModule', //Lazy load admin module
            data: { preload: false }
          },
          {
            path: 'my/services',
            loadChildren: './my-services/my-services.module#MyServicesModule', //Lazy load admin module
            data: { preload: false }
          },
          {
            path: 'sales',
            loadChildren: './my-sales/my-sales.module#MySalesModule', //Lazy load admin module
            data: { preload: false }
          },
          {
            path: 'quote',
            loadChildren: './quote-me/quote-me.module#QuoteMeModule', //Lazy load admin module
            data: { preload: false }
          },
          {
            path: 'settings',
            loadChildren: './settings/settings.module#SettingsModule', //Lazy load admin module
            data: { preload: false }
          },
          {
            path: 'tenant',
            loadChildren: './tenant/tenant.module#TenantModule', //Lazy load admin module
            data: { preload: false }
          },
          {
            path: 'user',
            loadChildren: './user/user.module#UserModule', //Lazy load admin module
            data: { preload: false }
          },
          {
            path: 'project-management',
            loadChildren: './project-management/project-management.module#ProjectManagementModule', //Lazy load project management module
            data: { preload: false }
          },
          {
            path: 'human-resource',
            loadChildren: './human-resource/human-resource.module#HumanResourceModule', //Lazy load project management module
            data: { preload: false }
          },
          {
            path: 'tenant-management',
            loadChildren: './tenant-management/tenant-management.module#TenantManagementModule', //Lazy load project management module
            data: { preload: false }
          },
          {
            path: 'finance',
            loadChildren: './finance/finance.module#FinanceModule', //Lazy load finance module
            data: { preload: false }
          },*/
          // {
          //   path: 'tendering',
          //   loadChildren: './tendering/tendering.module#TenderingModule', //Lazy load project management module
          //   data: { preload: false }
          // },
          // {
          //   path: 'outsourcing',
          //   loadChildren: './outsourcing/outsourcing.module#OutsourcingModule', //Lazy load project management module
          //   data: { preload: false }
          // },
          // {
          //   path: 'logistik',
          //   loadChildren: './logistik/logistik.module#LogistikModule', //Lazy load project management module
          //   data: { preload: false }
          // },
          // {
          //   path: 'manpower',
          //   loadChildren: './manpower/manpower.module#ManpowerModule', //Lazy load project management module
          //   data: { preload: false }
          // },
          // {
          //   path: 'event',
          //   loadChildren: './event/event.module#EventModule', //Lazy load project management module
          //   data: { preload: false }
          // },
          {
            path: '**', redirectTo: 'dashboard/main'
          }
        ],
        runGuardsAndResolvers: 'paramsChange',
      }
    ])
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RayuanRoutingModule } from './rayuan-routing.module';

import { MatRippleModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar} from '@angular/material/snack-bar';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule} from '@angular/material/dialog';
import { MatIconModule} from '@angular/material/icon';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';


import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { JoditAngularModule } from 'jodit-angular';
import { AppCommonModule } from '../shared/app-common.module';
import { QuillModule } from 'ngx-quill';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MiscModule } from '../misc/misc.module';
import { PMServiceProxy } from 'src/shared/service-proxies/service-proxies';
import { getQuillEditorSettings } from 'src/shared/helpers/quill-helper';
import { NgxPrintModule } from 'ngx-print';
import { DashboardModule } from '../dashboard/dashboard.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { RayuanListComponent } from './rayuan/list/list.component';
import { RayuanDetailsComponent } from './rayuan/rayuan-details/rayuan-details.component';
import { AddDocumentComponent } from './rayuan/rayuan-details/add-document/add-document.component';

const materialModules = [
  MatButtonModule,
  MatTableModule,
  MatCheckboxModule,
  MatDialogModule,
  MatSortModule,
  MatTabsModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatProgressBarModule,
  MatIconModule,
  MatSnackBarModule,
  MAT_DIALOG_DATA,
  MatDialogRef
];

@NgModule({
  declarations: [
    RayuanListComponent,
    RayuanDetailsComponent,
    AddDocumentComponent,
  ],
  imports: [
    DashboardModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppCommonModule,
    JoditAngularModule,
    RayuanRoutingModule,
    QuillModule.forRoot(getQuillEditorSettings()),
    TableModule,
    PaginatorModule,
    ModalModule.forRoot(),
    MatDatepickerModule,
    MatMomentDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSortModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatIconModule,
    MatSnackBarModule,
    MiscModule,
    NgxPrintModule,
    MatRadioModule,
    PerfectScrollbarModule,
    NgMultiSelectDropDownModule,
    BsDropdownModule.forRoot(),
  ],
  entryComponents: [
    //PilihAhliComponent
    // Variation1Component,
    // Variation2Component
    // CustomerDetailsComponent,
    // InvoiceNewItemComponent,
    // InvoicePreviewComponent,
    // InvoiceNewPaymentComponent,
    // InvoiceViewReceiptComponent
  ],
  // providers: [PMServiceProxy],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    { provide: PMServiceProxy },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
  ]
})
export class RayuanModule {
}

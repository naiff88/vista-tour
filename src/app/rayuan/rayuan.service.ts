/* tslint:disable:max-line-length comment-format */
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { bufferTime, distinctUntilChanged, filter, map, scan, share, take, tap } from 'rxjs/operators';
import * as _ from 'lodash';
import { Pager, PageInfo } from '../../pagingnation';
import { API_BASE_URL } from 'src/shared/service-proxies/service-proxies';
import { ProgressBarState, Pair } from './models';

export enum RayuanApiEndpoint {
  //UploadImage = '/api/',
  //DeleteImage = '/api/',
}

@Injectable({
  providedIn: 'root'
})
export class RayuanService {
  /**
   * Indicate data processing. Binded in dialog
   */
  private processingSubject = new Subject<ProgressBarState>();
  private baseUrl: string;
  http: HttpClient;


  /**
   *  Pipeline of async operation to handle loading state of product dialog
   */
  isBusyStream: Observable<boolean> = this.processingSubject.asObservable()
    .pipe(
      bufferTime(300),                  // buffers the progress Observable values for 100ms period
      filter(states => states.length > 0),   // ignore empty streams
      scan((total: number, states: ProgressBarState[]) => total + _.sum(states), 0),
      map((diff: number) => diff !== 0),       // diff between count of processing and already completed http requests
      distinctUntilChanged(),
      share()                                         // Returns a new Observable that multicasts (shares) the original Observable
    );

  set isBusy(value: boolean) {
    const state: number = value ? ProgressBarState.Show : ProgressBarState.Hide;
    this.processingSubject.next(state);
  }

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }












  // rayuan -------------------------------------------------------------------------------------


  getRayuanDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */

    //  getProductGeneral(id: number): Observable<ProductGeneral> {
    //   const params = new HttpParams().set('productId', String(id));
    //   this.isBusy = true;
    //   return this.http.get<ProductGeneral>(this.baseUrl + ProductApiEndpoint.General, {params})
    //     .pipe(
    //       map((resp: any) => {
    //         const model = resp.basicInfo[0] as ProductGeneral;
    //         model.imageList = resp.imageList as ProductImage[];
    //         return model;
    //       }),
    //       tap({complete: () => this.isBusy = false})
    //     );
    // }

    let record: any = {

      NoNotis: 'D00001',
      StatusPemohon: 1,
      NamaPemohon: 'Razman B. Md Zainal',
      NoPemohon: '840819075337',
      NoTel: '0129873718',
      Emel: 'razman@gmail.com',
      TarikhMohon: '08 Jan 2020',
      CaraMaklumbalas: 1,
      AlamatPemohon1: 'Alamat 1',
      AlamatPemohon2: 'Alamat 2',
      AlamatPemohon3: 'Alamat 3',
      PoskodPemohon: '99999',
      NegeriPemohon: 1,
      TarikhKesalahan: '02 Jan 2020',
      JenisKesalahan: 1,
      LainKesalahan: 'Catatan',

      PilihanAlasanRayuanList: [
        { id: '1' },
        { id: '2' },
      ],
      LainAlasan: 'Lain Alasan',

      dokumenSokongan: [
        { id: '1', nama: 'Dokumen 1', keterangan: 'Keterangan Dokumen 1', url: '' },
        { id: '2', nama: 'Dokumen 2', keterangan: 'Keterangan Dokumen 2', url: '' },
      ],


      IdPenguatkuasaPendaftar: 1,
      NoBadanPendaftar: '8136789',
      CatatanPendaftar: 'CatatanPendaftar',
      WaktuPendaftar: '15:10',
      TarikhPendaftar: '02 Jan 2020',

      IdPenguatkuasaPelulus: 1,
      NoBadanPelulus: '8136789',
      CatatanPelulus: 'CatatanPendaftar',
      KeputusanRayuan: 1,
      NilaiPengurangan: 100.00,
      TarikhPelulus: '02 Jan 2020',


      TarikhCipta: '02 Jan 2020',
      DiciptaOleh: 'Muhd Naiff',
      TarikhKemaskini: '03 Jan 2020',
      DikemaskiniOleh: 'Nik Fahmi',


      // imageList: [
      //   { imageId: '1', imageURL: 'https://homepages.cae.wisc.edu/~ece533/images/airplane.png' },
      //   { imageId: '2', imageURL: 'https://homepages.cae.wisc.edu/~ece533/images/baboon.png' },
      //   { imageId: '3', imageURL: 'https://homepages.cae.wisc.edu/~ece533/images/watch.png' },
      //   { imageId: '4', imageURL: 'https://homepages.cae.wisc.edu/~ece533/images/monarch.png' },
      // ]


    };
    return of(record);
  }

  saveRayuan(model, Id: number): Observable<any> {
    console.log('saveRayuan model ::::::::::::::: ', model);
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Rayuan disimpan!';
    const Id2 = 1;
    return of({ ReturnCode, ResponseMessage, Id: Id2 });
  }

  deleteImage(imageId: number) {
    // const params = new HttpParams().set('productImageId', String(imageId));

    // this.isBusy = true;
    // return this.http.delete(this.baseUrl + ProductApiEndpoint.DeleteImage, {params})
    //   .pipe(
    //     tap({complete: () => this.isBusy = false})
    //   );
    const ReturnCode = 200;
    const ResponseMessage = 'Gambar telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  deleteRayuan(ids: Array<{ id: number }>) {
    //console.log('deleteRayuan ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod rayuan telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getRayuanList(page: Pager): Observable<any> {
    //console.log('getRayuanOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    console.log('params getRayuanList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { Id: 1, NoNotis: 'DC0001', NamaPemohon: 'Razman B. Md Zainal', NoPemohon: '840819075337', TarikhMohon: '25/01/2020', TarikhNotis: '25/01/2020', Status: 'Baru', StatusId: '1' },
      { Id: 2, NoNotis: 'DC0002', NamaPemohon: 'Muhd Naiff', NoPemohon: '840819075337', TarikhMohon: '25/01/2020', TarikhNotis: '25/01/2020', Status: 'Menunggu Keputusan', StatusId: '2' },
      { Id: 3, NoNotis: 'DC0003', NamaPemohon: 'Nik Ahmad Fahmi', NoPemohon: '840819075337', TarikhMohon: '25/01/2020', TarikhNotis: '25/01/2020', Status: 'Batal', StatusId: '3' },
    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }


  
  uploadDokumenSokongan(data: FormData): Observable<string> {
    // return this.http.post<string>(baseUrl + '/api/upload/doc', data)
    //   .pipe(
    //     map((resp: any) => {
    //       //console.log('uploadFile resp :::::::::::::::::::: ', resp);
    //       return resp.Result[0].DocFileName;
    //     })
    //   );
    var linkFile = 'https://file-examples.com/wp-content/uploads/2017/02/zip_2MB.zip';
    return  of(linkFile);
  }


  getDokumenSokonganDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    let record: any = {
      Nama: 'File Name 1',
      Keterangan: 'Keterangan File Name 1',
      RayuanId: '1',
      Pakej: 'https://file-examples.com/wp-content/uploads/2017/10/file-sample_150kB.pdf',//dummy
    };
    return of(record);
  }

  saveDokumenSokongan(model, Id: number): Observable<any> {
    console.log('saveDokumenSokongan model ::::::::::::::: ', model);
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Dokumen disimpan!';
    const Id2 = 1;
    return of({ ReturnCode, ResponseMessage, Id:Id2 });
  }

  deleteDokumenSokongan(ids: Array<{ id: number }>) {
    console.log('deleteDokumenSokongan ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod patch telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getDokumenSokonganList(page: Pager, Id: number): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    console.log('params getDokumenSokonganList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];
    if (Id != 0) {
      list = [
        { Id: 1, Nama: 'Dokumen 1', Keterangan: 'Keterangan Dokumen 1', Pakej: 'https://file-examples.com/wp-content/uploads/2017/10/file-sample_150kB.pdf' },
        { Id: 2, Nama: 'Dokumen 2', Keterangan: 'Keterangan Dokumen 2', Pakej: 'https://file-examples.com/wp-content/uploads/2017/10/file-sample_150kB.pdf' },
      ];
    }
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }



  uploadFile(data: FormData): Observable<any> {
    //console.log('uploadFile:::', data);
    this.isBusy = true;
    // return this.http.post<string>(this.baseUrl + ProductApiEndpoint.UploadImage, data)
    //   .pipe(
    //     // map((resp: any) => resp.Result[0].ImageFileName),
    //     map((resp: any) => {
    //       console.log('resp uploadFile ::::::::::::::: ', resp);
    //       const ImageFileName =  resp.Result[0].ImageFileName;
    //       /*const ReturnCode = resp.ReturnCode;
    //       const ResponseMessage = resp.ResponseMessage;
    //       const Id = productId;
    //       return { ReturnCode, ResponseMessage, Id };*/
    //       console.log('resp ImageFileName ::::::::::::::: ', ImageFileName);
    //       return ( ImageFileName );
    //     }),
    //     tap({
    //       error: () => this.isBusy = false,
    //       complete: () => this.isBusy = false
    //     })
    //   );


    return of({ imageId: '1', imageURL: 'https://homepages.cae.wisc.edu/~ece533/images/airplane.png' });
  }

















  // rujukan --------------------------------------------------------------------------------
  getStatusRayuanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Baru' },
      { id: 2, name: 'Menunggu Keputusan' },
      { id: 3, name: 'Batal' },
    ];
    return of(list);
  }

  getNegeriList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Johor' },
      { id: 2, name: 'Kedah' },
      { id: 3, name: 'Pahang' },
      { id: 4, name: 'Perak' },
      { id: 5, name: 'Selangor' },
    ];
    return of(list);
  }

  getPenguatkuasaList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Penguatkuasa 1' },
      { id: 2, name: 'Penguatkuasa 2' },
      { id: 3, name: 'Penguatkuasa 3' },
      { id: 4, name: 'Penguatkuasa 4' },
    ];
    return of(list);
  }

  getAlasanRayuanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Kesilapan butir - butir notis' },
      { id: 2, name: 'Kecemasan atau kemalangan' },
      { id: 3, name: 'Tiket bayaran parking telah dibeli' },
      { id: 4, name: 'Kenderaan telah dijual' },
      { id: 5, name: 'Meter rosak' },
      { id: 6, name: 'Pemunya kereta telah meninggal dunia' },
      { id: 7, name: 'Kenderaan dicuri / klon' },
      //{ id: 8, name: 'Lain - lain' },
    ];
    return of(list);
  }

  getKeputusanRayuanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Batal' },
      { id: 2, name: 'Tiada Pembatalan / Pengurangan' },
      { id: 3, name: 'Kurang' },
    ];
    return of(list);
  }


  getJenisKesalahanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Lalulintas / Park' },
      { id: 2, name: 'Premis' },
      { id: 3, name: 'Penjaja' },
      { id: 4, name: 'Lain - Lain' },
    ];
    return of(list);
  }































}




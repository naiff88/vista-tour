import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MenuComponent } from '../dashboard/menu/menu.component';
import { RayuanListComponent } from './rayuan/list/list.component';
import { RayuanDetailsComponent } from './rayuan/rayuan-details/rayuan-details.component';

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MenuComponent,

                children: [
                    { path: '', pathMatch: 'full', redirectTo: 'senarai-rayuan' },  
                    { path: 'senarai-rayuan', component: RayuanListComponent },                   
                    { path: 'rayuan-details/:id/:type', component: RayuanDetailsComponent },
                    { path: 'rayuan-details/:id/:type/:fromMenu', component: RayuanDetailsComponent },
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})

export class RayuanRoutingModule { }

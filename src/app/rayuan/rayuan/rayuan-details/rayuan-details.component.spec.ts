import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RayuanDetailsComponent } from './rayuan-details.component';

describe('RayuanDetailsComponent', () => {
  let component: RayuanDetailsComponent;
  let fixture: ComponentFixture<RayuanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RayuanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RayuanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

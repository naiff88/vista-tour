/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RayuanService } from '../../rayuan.service';
import Swal from 'sweetalert2';
import * as _ from 'lodash';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { SelectionModel } from '@angular/cdk/collections';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { MatDialog } from '@angular/material/dialog';
import { AddDocumentComponent } from './add-document/add-document.component';
//import { AddDocumentComponent } from 'src/app/shared/components/add-document/add-document.component';


@Component({
  selector: 'app-rayuan-details',
  templateUrl: './rayuan-details.component.html',
  styleUrls: ['./rayuan-details.component.scss']
})


export class RayuanDetailsComponent implements OnInit {

  showSpinner: boolean = false;
  submitted = false;
  form: FormGroup;
  FormType: string;
  FromMenu: string = '';
  FormTitle: string = '';
  ViewMode: boolean = false;

  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  Id: number;
  files: File;


  StatusPemohon: number = 0;
  NegeriPemohon: number = 0;
  JenisKesalahan: number = 0;
  CaraMaklumbalas: number = 0;
  IdPenguatkuasaPendaftar: number = 0;
  IdPenguatkuasaPelulus: number = 0;
  KeputusanRayuan: number = 0;

  TarikhMohon: any = '';
  TarikhKesalahan: any = '';
  TarikhPendaftar: any = '';
  WaktuPendaftar: any = '';
  TarikhPelulus: any = '';

  NegeriList: any = [];
  AlasanRayuanList: any = [];
  PenguatkuasaList: any = [];
  KeputusanRayuanList: any = [];
  JenisKesalahanList: any = [];


  constructor(
    private rayuanService: RayuanService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  get f() { return this.form.controls; }

  get PilihanAlasanRayuanList(): FormArray {
    return this.form.get('PilihanAlasanRayuanList') as FormArray;
  }


  //open standard function use for multiple delete in datatable
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.CustomerId + 1}`;
  }
  onRowSelect(event) {
    const evt = event.originalEvent as MouseEvent;
    evt.stopPropagation();
  }
  //close standard function use for multiple delete in datatable

  //open init function
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.FromMenu = params.fromMenu;
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (this.FromMenu == 'keputusan') {
          this.FormTitle = 'Keputusan';
        }
        else {
          if (params.type == 'add') {
            this.FormTitle = 'Tambah';
          }
          else if (params.type == 'view') {
            this.FormTitle = 'Papar';
            this.ViewMode = true;
          }
          else if (params.type == 'edit') {
            this.FormTitle = 'Kemaskini';
          }
        }
      }
    });

    this.loadDetails(this.Id);

    //ref list call
    this.getNegeriList();
    this.getPenguatkuasaList();
    this.getKeputusanRayuanList();
    this.getAlasanRayuanList();
    this.getJenisKesalahanList();

    //dokumen list
    this.getDokumenSokonganList();

  }
  //close init function

  //upload dokumen
  uploadFile(id?: number, type?: string) {
    event.stopPropagation();
    const data = { RayuanId: this.Id,  Id: id, FormType: type};
    const dialogRef = this.dialog.open(AddDocumentComponent, { data });
    dialogRef.afterClosed()
    .subscribe(ok => this.getDokumenSokonganList()); // reload list
  }


  //open load form
  loadDetails(Id) {
    this.form = this.buildFormItems();
    this.form.setControl('PilihanAlasanRayuanList', this.formBuilder.array([]));

    if (Id > 0) {
      this.showSpinner = true;
      this.rayuanService.getRayuanDetails(Id)
        .subscribe(result => {
          this.showSpinner = false;


          this.TarikhMohon = result.TarikhMohon = moment(result.TarikhMohon) || '';
          this.TarikhKesalahan = result.TarikhKesalahan = moment(result.TarikhKesalahan) || '';
          this.TarikhPendaftar = result.TarikhPendaftar = moment(result.TarikhPendaftar) || '';
          this.WaktuPendaftar = result.WaktuPendaftar;
          this.TarikhPelulus = result.TarikhPelulus = moment(result.TarikhPelulus) || '';

          this.StatusPemohon = result.StatusPemohon > 0 ? result.StatusPemohon : 0;
          this.NegeriPemohon = result.NegeriPemohon > 0 ? result.NegeriPemohon : 0;
          this.JenisKesalahan = result.JenisKesalahan > 0 ? result.JenisKesalahan : 0;
          this.CaraMaklumbalas = result.CaraMaklumbalas > 0 ? result.CaraMaklumbalas : 0;
          this.IdPenguatkuasaPendaftar = result.IdPenguatkuasaPendaftar > 0 ? result.IdPenguatkuasaPendaftar : 0;
          this.IdPenguatkuasaPelulus = result.IdPenguatkuasaPelulus > 0 ? result.IdPenguatkuasaPelulus : 0;
          this.KeputusanRayuan = result.KeputusanRayuan > 0 ? result.KeputusanRayuan : 0;

          const PilihanAlasanRayuanList = _.get(result, 'PilihanAlasanRayuanList', []);
          const PilihanAlasanRayuanListFormGroup: FormGroup[] = _.map(PilihanAlasanRayuanList, item => this.setItemAlasan(item.id));
          this.form.setControl('PilihanAlasanRayuanList', this.formBuilder.array(PilihanAlasanRayuanListFormGroup));


          this.form.patchValue(result);
        });
    }
  }
  //close load form


  //open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod semasa akan ditetapkan semula!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  //open reset function

  //kembali
  kembali() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    if (this.FromMenu == "kemasukan") {
      this.router.navigate(['/app/rayuan/rayuan-details/0/add/kemasukan']);
    }
    else {
      this.router.navigate(['/app/rayuan/senarai-rayuan']);
    }
  }


  //open save function
  onSave(flagSave) {
    //console.log('this.form ::::::::::::::::::::::::;', this.form);
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat sah & lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.showSpinner = true;

      if (model.JenisKesalahan != '4') {
        model.LainKesalahan = '';
      }
      if (model.KeputusanRayuan != '3') {
        model.NilaiPengurangan = '';
      }

      this.rayuanService.saveRayuan(model, this.Id)
        .subscribe(r => {
          this.showSpinner = false;
          if (r.ReturnCode === 200) {

            let ResponseMessage = r.ResponseMessage;
            if (flagSave == 'toSemakan') {
              ResponseMessage = "Maklumat Rayuan Telah Dihantar Untuk Semakan";
            }
            else if (flagSave == 'toKelulusan') {
              ResponseMessage = "Maklumat Rayuan Telah Dihantar Untuk Kelulusan";
            }
            else if (flagSave == 'selesai') {
              ResponseMessage = "Maklumat Rayuan Telah Disahkan Keputusan";
            }

            Swal.fire(
              'Success',
              ResponseMessage,
              'success'
            );
            this.Id = r.Id;
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            this.router.onSameUrlNavigation = 'reload';
            if (this.FromMenu == 'keputusan' && flagSave != 'selesai') {
              this.router.navigate(['/app/rayuan/rayuan-details/', this.Id, 'view', this.FromMenu]);
            }
            else {
              if (flagSave == 'toSemakan' || flagSave == 'toKelulusan' || flagSave == 'selesai') {
                this.router.navigate(['/app/rayuan/senarai-rayuan']);
              }
              else {
                this.router.navigate(['/app/rayuan/rayuan-details/', this.Id, 'edit']);
              }

            }
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }
  //close save function

  pilihAlasan(event: any, id) {
    const formGroup = this.setItemAlasan(id);
    if (event.checked) {
      this.PilihanAlasanRayuanList.push(formGroup);
    }
    else {
      let index = (<FormArray>this.form.get('PilihanAlasanRayuanList')).controls.findIndex(x => x === formGroup);
      this.PilihanAlasanRayuanList.removeAt(index);
      this.PilihanAlasanRayuanList.updateValueAndValidity();
      this.PilihanAlasanRayuanList.markAsDirty();
    }
    this.PilihanAlasanRayuanList.updateValueAndValidity();
  }

  setItemAlasan(Id: any): FormGroup {
    return this.formBuilder.group({
      id: [Id, []],
    }
    );
  }


  //open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {


      const NoNotis = formGroup.controls['NoNotis'];
      const StatusPemohon = formGroup.controls['StatusPemohon'];
      const NamaPemohon = formGroup.controls['NamaPemohon'];
      const NoPemohon = formGroup.controls['NoPemohon'];
      const NoTel = formGroup.controls['NoTel'];
      const ButiranBatal = formGroup.controls['ButiranBatal'];
      const Emel = formGroup.controls['Emel'];
      const TarikhMohon = formGroup.controls['TarikhMohon'];
      const CaraMaklumbalas = formGroup.controls['CaraMaklumbalas'];
      const AlamatPemohon1 = formGroup.controls['AlamatPemohon1'];
      const AlamatPemohon2 = formGroup.controls['AlamatPemohon2'];
      const AlamatPemohon3 = formGroup.controls['AlamatPemohon3'];
      const PoskodPemohon = formGroup.controls['PoskodPemohon'];
      const NegeriPemohon = formGroup.controls['NegeriPemohon'];
      const TarikhKesalahan = formGroup.controls['TarikhKesalahan'];
      const JenisKesalahan = formGroup.controls['JenisKesalahan'];
      const LainKesalahan = formGroup.controls['LainKesalahan'];
      const IdPenguatkuasaPendaftar = formGroup.controls['IdPenguatkuasaPendaftar'];
      const NoBadanPendaftar = formGroup.controls['NoBadanPendaftar'];
      const CatatanPendaftar = formGroup.controls['CatatanPendaftar'];
      const WaktuPendaftar = formGroup.controls['WaktuPendaftar'];
      const TarikhPendaftar = formGroup.controls['TarikhPendaftar'];
      const IdPenguatkuasaPelulus = formGroup.controls['IdPenguatkuasaPelulus'];
      const NoBadanPelulus = formGroup.controls['NoBadanPelulus'];
      const CatatanPelulus = formGroup.controls['CatatanPelulus'];
      const KeputusanRayuan = formGroup.controls['KeputusanRayuan'];
      const NilaiPengurangan = formGroup.controls['NilaiPengurangan'];
      const TarikhPelulus = formGroup.controls['TarikhPelulus'];
      const LainAlasan = formGroup.controls['LainAlasan'];
      const PilihanAlasanRayuanList = formGroup.controls['PilihanAlasanRayuanList'];

      if (PilihanAlasanRayuanList && LainAlasan) {
        LainAlasan.setErrors(null);
        //console.log('Check Alasan ::::::::::: ', PilihanAlasanRayuanList.value);
        if (PilihanAlasanRayuanList.value.length === 0 && !LainAlasan.value) {
          LainAlasan.setErrors({ required: true });
        }
      }


      if (NoNotis) { NoNotis.setErrors(null); if (!NoNotis.value) { NoNotis.setErrors({ required: true }); } }
      if (StatusPemohon) { StatusPemohon.setErrors(null); if (!StatusPemohon.value) { StatusPemohon.setErrors({ required: true }); } else if (StatusPemohon.value == 0) { StatusPemohon.setErrors({ min: true }); } }
      if (NamaPemohon) { NamaPemohon.setErrors(null); if (!NamaPemohon.value) { NamaPemohon.setErrors({ required: true }); } }
      if (NoPemohon) { NoPemohon.setErrors(null); if (!NoPemohon.value) { NoPemohon.setErrors({ required: true }); } }
      if (NoTel) { NoTel.setErrors(null); if (!NoTel.value) { NoTel.setErrors({ required: true }); } }
      if (ButiranBatal) { ButiranBatal.setErrors(null); if (!ButiranBatal.value) { ButiranBatal.setErrors({ required: true }); } }
      if (Emel) {
        let regex: RegExp = new RegExp(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);
        Emel.setErrors(null);
        if (!Emel.value) {
          Emel.setErrors({ required: true });
        }
        else if (!String(Emel.value).match(regex)) {
          Emel.setErrors({ pattern: true });
        }
      }
      if (TarikhMohon) { TarikhMohon.setErrors(null); if (!TarikhMohon.value) { TarikhMohon.setErrors({ required: true }); } else if (TarikhMohon.value && TarikhMohon.value > moment().toDate()) { TarikhMohon.setErrors({ exceed: true }); } }
      if (CaraMaklumbalas) { CaraMaklumbalas.setErrors(null); if (!CaraMaklumbalas.value) { CaraMaklumbalas.setErrors({ required: true }); } else if (CaraMaklumbalas.value == 0) { CaraMaklumbalas.setErrors({ min: true }); } }
      if (AlamatPemohon1) { AlamatPemohon1.setErrors(null); if (!AlamatPemohon1.value) { AlamatPemohon1.setErrors({ required: true }); } }
      if (PoskodPemohon) { PoskodPemohon.setErrors(null); if (!PoskodPemohon.value) { PoskodPemohon.setErrors({ required: true }); } else if (PoskodPemohon.value.length != 5) { PoskodPemohon.setErrors({ minlength: true }); } }
      if (NegeriPemohon) { NegeriPemohon.setErrors(null); if (!NegeriPemohon.value) { NegeriPemohon.setErrors({ required: true }); } else if (NegeriPemohon.value == 0) { NegeriPemohon.setErrors({ min: true }); } }
      if (TarikhKesalahan) { TarikhKesalahan.setErrors(null); if (!TarikhKesalahan.value) { TarikhKesalahan.setErrors({ required: true }); } else if (TarikhKesalahan.value && TarikhKesalahan.value > moment().toDate()) { TarikhKesalahan.setErrors({ exceed: true }); } }
      if (JenisKesalahan) { JenisKesalahan.setErrors(null); if (!JenisKesalahan.value) { JenisKesalahan.setErrors({ required: true }); } else if (JenisKesalahan.value == 0) { JenisKesalahan.setErrors({ min: true }); } }

      if (LainKesalahan && JenisKesalahan && JenisKesalahan.value == '4') { LainKesalahan.setErrors(null); if (!LainKesalahan.value) { LainKesalahan.setErrors({ required: true }); } }

      if (IdPenguatkuasaPendaftar) { IdPenguatkuasaPendaftar.setErrors(null); if (!IdPenguatkuasaPendaftar.value) { IdPenguatkuasaPendaftar.setErrors({ required: true }); } else if (IdPenguatkuasaPendaftar.value == 0) { IdPenguatkuasaPendaftar.setErrors({ min: true }); } }
      if (NoBadanPendaftar) { NoBadanPendaftar.setErrors(null); if (!NoBadanPendaftar.value) { NoBadanPendaftar.setErrors({ required: true }); } }
      if (CatatanPendaftar) { CatatanPendaftar.setErrors(null); if (!CatatanPendaftar.value) { CatatanPendaftar.setErrors({ required: true }); } }
      if (WaktuPendaftar) { WaktuPendaftar.setErrors(null); if (!WaktuPendaftar.value) { WaktuPendaftar.setErrors({ required: true }); } }
      if (TarikhPendaftar) { TarikhPendaftar.setErrors(null); if (!TarikhPendaftar.value) { TarikhPendaftar.setErrors({ required: true }); } }

      if (this.FromMenu == 'keputusan') {
        if (IdPenguatkuasaPelulus) { IdPenguatkuasaPelulus.setErrors(null); if (!IdPenguatkuasaPelulus.value) { IdPenguatkuasaPelulus.setErrors({ required: true }); } else if (IdPenguatkuasaPelulus.value == 0) { IdPenguatkuasaPelulus.setErrors({ min: true }); } }
        if (NoBadanPelulus) { NoBadanPelulus.setErrors(null); if (!NoBadanPelulus.value) { NoBadanPelulus.setErrors({ required: true }); } }
        if (CatatanPelulus) { CatatanPelulus.setErrors(null); if (!CatatanPelulus.value) { CatatanPelulus.setErrors({ required: true }); } }
        if (KeputusanRayuan) { KeputusanRayuan.setErrors(null); if (!KeputusanRayuan.value) { KeputusanRayuan.setErrors({ required: true }); } else if (KeputusanRayuan.value == 0) { KeputusanRayuan.setErrors({ min: true }); } }
        if (NilaiPengurangan && KeputusanRayuan.value == '3') { NilaiPengurangan.setErrors(null); if (!NilaiPengurangan.value) { NilaiPengurangan.setErrors({ required: true }); } else if (NilaiPengurangan.value == 0) { NilaiPengurangan.setErrors({ min: true }); } }
        if (TarikhPelulus) { TarikhPelulus.setErrors(null); if (!TarikhPelulus.value) { TarikhPelulus.setErrors({ required: true }); } }
      }



    }
  }
  //open custom validation


  //open form setting 
  buildFormItems(): FormGroup {
    return this.formBuilder.group({

      NoNotis: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      StatusPemohon: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      NamaPemohon: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      NoPemohon: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      NoTel: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      Emel: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      TarikhMohon: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      CaraMaklumbalas: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      AlamatPemohon1: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      AlamatPemohon2: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      AlamatPemohon3: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      PoskodPemohon: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      NegeriPemohon: [{ value: '0', disabled: this.FormType == 'view' ? true : null }, []],
      TarikhKesalahan: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      JenisKesalahan: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      LainKesalahan: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      LainAlasan: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],

      IdPenguatkuasaPendaftar: [{ value: '0', disabled: this.FormType == 'view' ? true : null }, []],
      NoBadanPendaftar: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      CatatanPendaftar: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      WaktuPendaftar: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      TarikhPendaftar: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],

      IdPenguatkuasaPelulus: [{ value: '0', disabled: this.FromMenu == 'keputusan' ? null : true }, []],
      NoBadanPelulus: [{ value: '', disabled: this.FromMenu == 'keputusan' ? null : true }, []],
      CatatanPelulus: [{ value: '', disabled: this.FromMenu == 'keputusan' ? null : true }, []],
      KeputusanRayuan: [{ value: '', disabled: this.FromMenu == 'keputusan' ? null : true }, []],
      NilaiPengurangan: [{ value: '', disabled: this.FromMenu == 'keputusan' ? null : true }, []],
      TarikhPelulus: [{ value: '', disabled: this.FromMenu == 'keputusan' ? null : true }, []],

      PilihanAlasanRayuanList: this.formBuilder.array([])


    }, { validator: this.customValidation() }
    );
  }
  //open form setting 


  //open standard listing with search function
  getDokumenSokonganList(event?: LazyLoadEvent) {
    console.log('getDokumenSokonganList ::::::::::::::::: ');
    this.showSpinner = true;
    this.rayuanService.getDokumenSokonganList(this.pageSetting.getPagerSetting(this.paginator, event, this.primengTableHelper.getSorting(this.dataTable)), this.Id)
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    this.primengTableHelper.showLoadingIndicator();
  }


  // open get ref list 
  getNegeriList() {
    this.rayuanService.getNegeriList().subscribe(items => {
      this.NegeriList = items;
    });
  }
  getPenguatkuasaList() {
    this.rayuanService.getPenguatkuasaList().subscribe(items => {
      this.PenguatkuasaList = items;
    });
  }
  getKeputusanRayuanList() {
    this.rayuanService.getKeputusanRayuanList().subscribe(items => {
      this.KeputusanRayuanList = items;
    });
  }
  getAlasanRayuanList() {
    this.rayuanService.getAlasanRayuanList().subscribe(items => {
      //this.AlasanRayuanList = items;
      this.AlasanRayuanList = [];
      //console.log('FORM PilihanAlasanRayuanList >>>>>>>>>>>>>>>>> ', this.form.get('PilihanAlasanRayuanList'));
      items.forEach((element) => {
        element.checked = false;
        element.disabled = this.FormType == 'view' ? 'true' : null;
        //console.log('element >>>>>>>>>>>>>>>>> ', element);
        let obj = (<FormArray>this.form.get('PilihanAlasanRayuanList')).controls.filter(x =>
          String(x.value.id) === String(element.id)
        );
        //console.log('obj :::: ', obj);
        if (obj.length > 0) {
          element.checked = true;
        }

        //element.checked = obj.id == true;
        this.AlasanRayuanList.push(element);
        //element.product_desc = element.product_desc.substring(0,10);
      });
      //console.log('this.AlasanRayuanList >>>>>>>>>>>>>>>>> ', this.AlasanRayuanList);
      //let index = (<FormArray>this.form.get('PilihanAlasanRayuanList')).controls.findIndex(x => x=== formGroup);
    });
  }
  getJenisKesalahanList() {
    this.rayuanService.getJenisKesalahanList().subscribe(items => {
      this.JenisKesalahanList = items;
    });
  }
  // close get ref list 
}

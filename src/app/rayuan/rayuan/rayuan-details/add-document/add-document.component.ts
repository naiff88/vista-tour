import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { RayuanService } from '../../../rayuan.service';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-rayuan-add-document-details',
  templateUrl: './add-document.component.html',
  styleUrls: ['./add-document.component.scss']
})

export class AddDocumentComponent implements OnInit {
  RayuanId: number;
  @ViewChild('inputFile', { static: true }) inputFile: ElementRef;

  showSpinner: boolean = false;
  submitted = false;
  form: FormGroup;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;

  Id: number;
  PakejName: string = '';
  PakejLink: string = '';

  Status: number = 0;
  statusList: any = [];

  constructor(
    private rayuanService: RayuanService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public dialogRef: MatDialogRef<AddDocumentComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: { RayuanId: number, Id: number, FormType: string }) {
  }

  get f() { return this.form.controls; }

  //open init function
  ngOnInit() {
    this.Id = this.data.Id;
    this.RayuanId = this.data.RayuanId;
    this.FormType = this.data.FormType;

    // console.log('this.Id ::::::::::::: ', this.Id);
    // console.log('this.RayuanId ::::::::::::: ', this.RayuanId);
    // console.log('this.FormType ::::::::::::: ', this.FormType);

    // if (this.Id) {
      if (this.FormType == 'add') {
        this.FormTitle = 'Tambah';
      }
      else if (this.FormType == 'view') {
        this.FormTitle = 'Papar';
        this.ViewMode = true;
      }
      else if (this.FormType == 'edit') {
        this.FormTitle = 'Kemaskini';
      }
    // }
    // this.activatedRoute.params.subscribe(params => {
    //   if (params.id) {
    //     this.Id = params.id;
    //     this.FormType = params.type;
    //     if (params.type == 'add') {
    //       this.FormTitle = 'Tambah';
    //     }
    //     else if (params.type == 'view') {
    //       this.FormTitle = 'Papar';
    //       this.ViewMode = true;
    //     }
    //     else if (params.type == 'edit') {
    //       this.FormTitle = 'Kemaskini';
    //     }
    //   }
    // });
    //ref list call
    this.loadDetails(this.Id);
  }
  //close init function

  //open load form
  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.rayuanService.getDokumenSokonganDetails(Id)
        .subscribe(result => {
          this.showSpinner = false;
          this.PakejLink = result.Pakej;
          result.Pakej = '';
          this.form.patchValue(result);
        });
    }
  }
  //close load form


  //open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod semasa akan ditetapkan semula!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.PakejName = '';
      this.loadDetails(this.Id)
    })
  }
  //open reset function

  //open save function
  onSave() {
    //console.log('this.form.value :::::::::::::::::::: ', this.form.value);
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat sah & lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.showSpinner = true;

      if (this.form.get('Pakej').value) {
        const formData = new FormData();
        formData.append('Pakej', this.form.get('Pakej').value);
        this.rayuanService.uploadDokumenSokongan(formData)
          .subscribe(resUrl => {
            model.Pakej = resUrl;
            this.save(model)
          }, (err) => console.log(err));
      }
      else {
        model.Pakej = this.PakejLink;
        this.save(model)
      }
    }
  }

  popupClose() {
  this.dialogRef.close(true);
  }

  save(model) {
    this.rayuanService.saveDokumenSokongan(model, this.Id)
      .subscribe(r => {
        this.showSpinner = false;
        if (r.ReturnCode === 200) {
          Swal.fire(
            'Success',
            r.ResponseMessage,
            'success'
          );
          this.Id = r.Id;
          this.dialogRef.close(true);
        } else {
          Swal.fire(
            'Error',
            r.ResponseMessage,
            'error'
          );
        }
      });
  }
  //close save function


  //open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const Nama = formGroup.controls['Nama'];
      const Keterangan = formGroup.controls['Keterangan'];
      const Pakej = formGroup.controls['Pakej'];

      if (Nama) {
        Nama.setErrors(null);
        if (!Nama.value) {
          Nama.setErrors({ required: true });
        }
      }

      if (Keterangan) {
        Keterangan.setErrors(null);
        if (!Keterangan.value) {
          Keterangan.setErrors({ required: true });
        }
      }

      if (Pakej && this.PakejLink == '') {
        Pakej.setErrors(null);
        if (!Pakej.value) {
          Pakej.setErrors({ required: true });
        }
      }
    }
  }
  //open custom validation



  //open form setting 
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      Nama: ['', []],
      Keterangan: ['', []],
      Pakej: ['', []],
      RayuanId: [this.data.RayuanId, []],
    }, { validator: this.customValidation() }
    );
  }
  //open form setting 


  // open get ref list  
  // getStatusList() {
  //   this.rayuanService.getStatusList().subscribe(items => {
  //     this.statusList = items;
  //   });
  // }
  // close get ref list 

  //upload file
  uploadFile(fileName) {
    let reader = new FileReader();
    if (fileName[0].target.files.length > 0) {
      const Pakej = fileName[0].target.files[0];
      this.PakejName = fileName[0].target.value;
      reader.readAsDataURL(Pakej);
      reader.onload = () => {
        this.form.patchValue({
          Pakej: reader.result
        });
      };
    }
  }

}

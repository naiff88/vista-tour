import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { RayuanService } from '../../rayuan.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { ReportTemplete } from '../../../../report';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');
import * as $ from 'jquery';

@Component({
  selector: 'app-rayuan-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class RayuanListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  reportTemplete: ReportTemplete;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  eventSearch: any;
  filterValue: any;
  onSearch: boolean = false;
  submitted: boolean = false;
  reVisit: boolean = false;
  form: FormGroup;
  
  //close standard var for searching page

  // any unique name for searching list
  searchID = 'getRayuanList';

  //initiate list
  statusRayuanList: any = [];

  //initiate list model 
  StatusRayuan: number = 0;

  //initiate date model 
  TarikhMula: any;
  TarikhMohon: any;
  TarikhAkhir: any;

  //report props
  reportCol: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'NoNotis', title: 'No. Notis', width: '15', align: 'left' },
    { name: 'NamaPemohon', title: 'Nama Pemohon', width: '15', align: 'left' },
    { name: 'NoPemohon', title: 'No. KP / Syarikat Pemohon', width: '15', align: 'left' },
    { name: 'TarikhMohon', title: 'Tarikh Permohonan', width: '15', align: 'left' },
    { name: 'TarikhNotis', title: 'Tarikh Notis', width: '15', align: 'left' },
    { name: 'Status', title: 'Status', width: '15', align: 'left' },
  ];
  reportTitle: string = 'Senarai Rayuan';


  //standard contructor
  constructor(
    private rayuanService: RayuanService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
    this.reportTemplete = new ReportTemplete();
  }

  //form
  get f() { return this.form.controls; }


  //open standard function use for multiple delete in datatable
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.CustomerId + 1}`;
  }
  onRowSelect(event) {
    const evt = event.originalEvent as MouseEvent;
    evt.stopPropagation();
  }
  //close standard function use for multiple delete in datatable


  //init methode to call default function
  ngOnInit() {
    if (this.pageSetting.getSearchFilterValueSV(this.searchID)) {
      this.reVisit = true;
    }
    this.getRayuanList();
    this.getStatusRayuanList();
  }

  //open details page
  rayuanDetails(id?: number, type?: string): void {
    this.router.navigate(['/app/rayuan/rayuan-details/', id, type]);
  }

  keputusanDetails(id?: number, type?: string): void {
    this.router.navigate(['/app/rayuan/rayuan-details/', id, type, 'keputusan']);
  }

  //open batal page
  batalDetails(id?: number, type?: string): void {
    this.router.navigate(['/app/rayuan/rayuan-details/', id, type, 'batal']);
  }


  //open standard listing with search function
  async getRayuanList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    await this.reloadSearchForm();
    await this.rayuanService.getRayuanList(this.pageSetting.getPagerSettingWithSearch(this.paginator, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (this.onSearch) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    await this.primengTableHelper.showLoadingIndicator();
  }
  //close standard listing with search function

  //open default listing function  
  getStatusRayuanList() {
    this.rayuanService.getStatusRayuanList().subscribe(items => {
      this.statusRayuanList = items;
    });
  }
  //close default listing function


  //open reload search form
  reloadSearchForm() {
    this.filterValue = this.pageSetting.getSearchFilterValueSV(this.searchID);
    let SFcurrent = this.pageSetting.getSearchFilterValueSF(this.searchID);
    this.form = this.buildFormItems();
    if (SFcurrent) {
      //reasing value for list & date model
      this.StatusRayuan = SFcurrent.StatusRayuan > 0 ? SFcurrent.StatusRayuan : 0;
      this.TarikhMohon = SFcurrent.TarikhMohon = moment(SFcurrent.TarikhMohon) || '';
      this.TarikhMula = SFcurrent.TarikhMula = moment(SFcurrent.TarikhMula) || '';
      this.TarikhAkhir = SFcurrent.TarikhAkhir = moment(SFcurrent.TarikhAkhir) || '';
      this.form.patchValue(SFcurrent);
    } else {
      //initiate list & date model
      this.StatusRayuan = 0;
      this.TarikhMohon = '';
      this.TarikhMula = '';
      this.TarikhAkhir = '';
    }
  }
  //close reload search form


  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      //setting store procedure listing field for filtering
      this.filterValue = {
        " NoNotis ": { value: searchForm.NoNotis, matchMode: "contains" },
        " NamaPemohon ": { value: searchForm.NamaPemohon, matchMode: "contains" },
        " NoPemohon ": { value: searchForm.NoPemohon, matchMode: "contains" },
        " StatusRayuan ": { value: searchForm.StatusRayuan, matchMode: "equals" },
        " TarikhMohon ": { value: searchForm.TarikhMohon, matchMode: "dateEquals" },
        " TarikhMula ": { value: searchForm.TarikhMula, matchMode: "dateGreater" },
        " TarikhAkhir ": { value: searchForm.TarikhAkhir, matchMode: "dateLower" },
      }
      this.pageSetting.setSearchFilterLS(this.filterValue, searchForm, this.searchID);
      this.onSearch = true;
      this.getRayuanList();
      this.paginator.changePage(0);
    }
  }

  //reset search form
  reset() {
    this.pageSetting.removeSearchFilterLS(this.searchID);
    this.form = this.buildFormItems();

    //initiate list & date model
    this.StatusRayuan= 0;
    this.TarikhMohon = '';
    this.TarikhMula = '';
    this.TarikhAkhir = '';

    this.filterValue = null;
    this.getRayuanList();
    this.paginator.changePage(0);
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      
      const TarikhMohon = formGroup.controls['TarikhMohon'];
      const TarikhMula = formGroup.controls['TarikhMula'];
      const TarikhAkhir = formGroup.controls['TarikhAkhir'];

      if (TarikhMohon) {
        TarikhMohon.setErrors(null);
      }
      if (TarikhMula) {
        TarikhMula.setErrors(null);
        if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhMula.setErrors({ exceed: true });
        }
      }
      if (TarikhAkhir) {
        TarikhAkhir.setErrors(null);
        if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhAkhir.setErrors({ exceed: true });
        }
      }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      NoNotis: ['', []],
      NamaPemohon: ['', []],
      NoPemohon: ['', []],
      StatusRayuan: ['0', []],
      TarikhMohon: ['', []],
      TarikhMula: ['', []],
      TarikhAkhir: ['', []],     
    }, { validator: this.customValidation() }
    );
  }

  //excel genarator
  printTableExcel() {
    this.showSpinner = true;
    let list: any;
    this.rayuanService.getRayuanList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
      .subscribe(items => {
        list = items.list;
        this.pageSetting.excelGenerator(this.reportCol, list, this.reportTitle, this.reportTitle).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }

  //pdf genarator
  printTablePdf() {
    this.showSpinner = true;
    let list: any;
    this.rayuanService.getRayuanList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
      .subscribe(items => {
        list = items.list;
        this.pageSetting.pdfGenerator(this.reportCol, list, this.reportTitle, this.reportTitle, 'p', 7).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }

  //multiple print rayuan function
  selectionPrint(type) {
    this.showSpinner = true;
    let selectedItems: Array<{ Id: number }>;
    let listSetData: any = [];
    if (type == 'Select_A4' || type == 'Select_4') {
      selectedItems = this.selection.selected.map(item => ({ Id: item.Id }));
    }
    else if (type == 'All_A4' || type == 'All_4') {
      this.rayuanService.getRayuanList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          selectedItems =  items.list.map(item => ({ Id: item.Id })); 
        });
    }
    selectedItems.map(item => { 
      this.rayuanService.getRayuanDetails(item.Id)
      .subscribe(result => {
        listSetData.push(result);
      });
    });
     this.reportTemplete.rayuan(listSetData).then(loaded => {
      if (loaded) {
        this.showSpinner = false;
      }
    });;
  }


  //multiple delete function
  delete() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.Id }));
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod yang telah hapus tidak akan dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        this.rayuanService.deleteRayuan(selectedItems)
          .subscribe(resultDelete => {
            this.showSpinner = false;
            if (resultDelete.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultDelete.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultDelete.ResponseMessage,
                'success'
              );
            }
            this.getRayuanList();
          });
      }
    })
  }

}

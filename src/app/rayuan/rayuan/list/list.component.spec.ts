import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RayuanListComponent } from './list.component';

describe('RayuanListComponent', () => {
  let component: RayuanListComponent;
  let fixture: ComponentFixture<RayuanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RayuanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RayuanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

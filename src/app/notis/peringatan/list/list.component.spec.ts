import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeringatanListComponent } from './list.component';

describe('PeringatanListComponent', () => {
  let component: PeringatanListComponent;
  let fixture: ComponentFixture<PeringatanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeringatanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeringatanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

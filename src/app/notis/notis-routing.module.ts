import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MenuComponent } from '../dashboard/menu/menu.component';
import { NotisListComponent } from './notis/list/list.component';
import { PeringatanListComponent } from './peringatan/list/list.component';
import { NotisDetailsComponent } from './notis/notis-details/notis-details.component';
import { FailListComponent } from './fail/list/list.component';
import { CetakanNotisListComponent } from './cetakan-notis/list/list.component';
import { AduanMahkamahListComponent } from './aduan-mahkamah/list/list.component';
import { AduanMahkamahDetailsComponent } from './aduan-mahkamah/aduan-mahkamah-details/aduan-mahkamah-details.component';
import { Sp2uListComponent } from './sp2U/list/list.component';
import { PertuduhanDetailsComponent } from './pertuduhan/pertuduhan-details.component';

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MenuComponent,

                children: [
                    { path: '', pathMatch: 'full', redirectTo: 'senarai-notis' },  
                    { path: 'senarai-notis', component: NotisListComponent },
                    { path: 'senarai-peringatan', component: PeringatanListComponent },
                    { path: 'senarai-aduan-mahkamah', component: AduanMahkamahListComponent },
                    { path: 'senarai-cetakan-notis', component: CetakanNotisListComponent },
                    { path: 'senarai-fail', component: FailListComponent },
                    { path: 'notis-details/:id/:type', component: NotisDetailsComponent },
                    { path: 'aduan-mahkamah-details/:id/:type', component: AduanMahkamahDetailsComponent },
                    { path: 'notis-details/:id/:type/:fromMenu', component: NotisDetailsComponent },
                    { path: 'senarai-sp2u', component: Sp2uListComponent },
                    { path: 'pertuduhan-details/:id/:type', component: PertuduhanDetailsComponent },

                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})

export class NotisRoutingModule { }

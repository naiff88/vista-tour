/* tslint:disable:max-line-length comment-format */
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { bufferTime, distinctUntilChanged, filter, map, scan, share, take, tap } from 'rxjs/operators';
import * as _ from 'lodash';
import { Pager, PageInfo } from '../../pagingnation';
import { API_BASE_URL } from 'src/shared/service-proxies/service-proxies';
import { ProgressBarState, Pair } from './models';

export enum NotisApiEndpoint {
  //UploadImage = '/api/',
  //DeleteImage = '/api/',
}

@Injectable({
  providedIn: 'root'
})
export class NotisService {
  /**
   * Indicate data processing. Binded in dialog
   */
  private processingSubject = new Subject<ProgressBarState>();
  private baseUrl: string;
  http: HttpClient;


  /**
   *  Pipeline of async operation to handle loading state of product dialog
   */
  isBusyStream: Observable<boolean> = this.processingSubject.asObservable()
    .pipe(
      bufferTime(300),                  // buffers the progress Observable values for 100ms period
      filter(states => states.length > 0),   // ignore empty streams
      scan((total: number, states: ProgressBarState[]) => total + _.sum(states), 0),
      map((diff: number) => diff !== 0),       // diff between count of processing and already completed http requests
      distinctUntilChanged(),
      share()                                         // Returns a new Observable that multicasts (shares) the original Observable
    );

  set isBusy(value: boolean) {
    const state: number = value ? ProgressBarState.Show : ProgressBarState.Hide;
    this.processingSubject.next(state);
  }

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }



  // Pertuduhan --------------------------------------------------------------------------------

  savePertuduhan(model, Id: number): Observable<any> {
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Pertuduhan disimpan!';
    const Id2 = 1;
    return of({ ReturnCode, ResponseMessage, Id: Id2 });
  }

  getPertuduhanDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */

    //  getProductGeneral(id: number): Observable<ProductGeneral> {
    //   const params = new HttpParams().set('productId', String(id));
    //   this.isBusy = true;
    //   return this.http.get<ProductGeneral>(this.baseUrl + ProductApiEndpoint.General, {params})
    //     .pipe(
    //       map((resp: any) => {
    //         const model = resp.basicInfo[0] as ProductGeneral;
    //         model.imageList = resp.imageList as ProductImage[];
    //         return model;
    //       }),
    //       tap({complete: () => this.isBusy = false})
    //     );
    // }

    let record: any = {
      Peruntukan: 1,
      Seksyen: 1,
      Pembolehubah: 1,
      Penerangan: 'Kesalahan dibawah akta trafik',
    };
    return of(record);
  }





  // peringatan ---------------------------------------------------------------------------------------------


  getCetakanNotisAkhir(): Observable<any> {
    //const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */

    //  getProductGeneral(id: number): Observable<ProductGeneral> {
    //   const params = new HttpParams().set('productId', String(id));
    //   this.isBusy = true;
    //   return this.http.get<ProductGeneral>(this.baseUrl + ProductApiEndpoint.General, {params})
    //     .pipe(
    //       map((resp: any) => {
    //         const model = resp.basicInfo[0] as ProductGeneral;
    //         model.imageList = resp.imageList as ProductImage[];
    //         return model;
    //       }),
    //       tap({complete: () => this.isBusy = false})
    //     );
    // }

    let record: any = {
      JenisNotis: 'JPK 55 - TRAFIK',
      JenisCetak: 'Cetak Pilihan (Dot Matrix)',
      TarikhMula: '01/01/2020',
      TarikhAkhir: '15/01/2020',
      TarikhCetak: '20/01/2020',
      Status: '-',
    };
    return of(record);
  }


  getCetakanNotisList(page: Pager): Observable<any> {
    //console.log('getNotisOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    //console.log('params getNotisList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { Id: 1, NoNotis: 'DC0001', Jenis: 'JPK 55-Trafik', NoLesen: '888111', NoKenderaan: 'WWW9999', NamaPesalah: 'Razman B. Md Zainal', NoPesalah: '840819075337', TarikhKesalahan: '25/01/2020 03:27 PM', Status: 'P', Seksyen: 'KAEDAH 12-4', Peruntukan: 'Akta Pengangkutan Jalan 1974' },
      { Id: 2, NoNotis: 'DC0002', Jenis: 'JPK 55-Trafik', NoLesen: '666777', NoKenderaan: 'JKP9981', NamaPesalah: 'Razman B. Md Zainal', NoPesalah: '840819075337', TarikhKesalahan: '25/07/2020 03:27 PM', Status: 'A', Seksyen: 'KAEDAH 12-4', Peruntukan: 'Akta Pengangkutan Jalan 1974' },


    ];

    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }





  // Aduan ---------------------------------------------------------------------------------------------


  getAduanMahkamahDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */

    //  getProductGeneral(id: number): Observable<ProductGeneral> {
    //   const params = new HttpParams().set('productId', String(id));
    //   this.isBusy = true;
    //   return this.http.get<ProductGeneral>(this.baseUrl + ProductApiEndpoint.General, {params})
    //     .pipe(
    //       map((resp: any) => {
    //         const model = resp.basicInfo[0] as ProductGeneral;
    //         model.imageList = resp.imageList as ProductImage[];
    //         return model;
    //       }),
    //       tap({complete: () => this.isBusy = false})
    //     );
    // }

    let record: any = {

      NamaPesalah: 'Muhd Naiff',
      NoKPSyarikatPesalah: '880909119876',
      NoPkt: '9173891',
      NoNotis: 'N887311',
      AlamatPesalah1: 'Alamat 1',
      AlamatPesalah2: 'Alamat 2',
      AlamatPesalah3: 'Alamat 3',
      PoskodPesalah: '99999',
      NegeriPesalah: 1,
      Penerangan: 'Kesalahan dibawah akta trafik',
      NamaPenguatkuasa: 'Mohd Shahir',
    };
    return of(record);
  }

  saveAduanMahkamah(model, Id: number): Observable<any> {
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Aduan disimpan!';
    const Id2 = 1;
    return of({ ReturnCode, ResponseMessage, Id: Id2 });
  }

  getCetakanAduanMahkamahAkhir(): Observable<any> {
    //const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */

    //  getProductGeneral(id: number): Observable<ProductGeneral> {
    //   const params = new HttpParams().set('productId', String(id));
    //   this.isBusy = true;
    //   return this.http.get<ProductGeneral>(this.baseUrl + ProductApiEndpoint.General, {params})
    //     .pipe(
    //       map((resp: any) => {
    //         const model = resp.basicInfo[0] as ProductGeneral;
    //         model.imageList = resp.imageList as ProductImage[];
    //         return model;
    //       }),
    //       tap({complete: () => this.isBusy = false})
    //     );
    // }

    let record: any = {
      JenisCetak: 'Peringatan Notis',
      TarikhMula: '01/01/2020',
      TarikhAkhir: '15/01/2020',
      TarikhCetak: '20/01/2020',
    };
    return of(record);
  }


  getAduanMahkamahList(page: Pager): Observable<any> {
    //console.log('getNotisOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    //console.log('params getNotisList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { Id: 1, NoNotis: 'DC0001', Jenis: 'JPK 55-Trafik', NoLesen: '888111', NoKenderaan: 'WWW9999', NamaPesalah: 'Razman B. Md Zainal', NoPesalah: '840819075337', TarikhKesalahan: '25/01/2020 03:27 PM', Status: 'A' },
      { Id: 2, NoNotis: 'DC0002', Jenis: 'JPK 55-Trafik', NoLesen: '666777', NoKenderaan: 'JKP9981', NamaPesalah: 'Razman B. Md Zainal', NoPesalah: '840819075337', TarikhKesalahan: '25/07/2020 03:27 PM', Status: 'A' },


    ];

    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }












  // peringatan ---------------------------------------------------------------------------------------------

  getCetakanPeringatanAkhir(): Observable<any> {
    //const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */

    //  getProductGeneral(id: number): Observable<ProductGeneral> {
    //   const params = new HttpParams().set('productId', String(id));
    //   this.isBusy = true;
    //   return this.http.get<ProductGeneral>(this.baseUrl + ProductApiEndpoint.General, {params})
    //     .pipe(
    //       map((resp: any) => {
    //         const model = resp.basicInfo[0] as ProductGeneral;
    //         model.imageList = resp.imageList as ProductImage[];
    //         return model;
    //       }),
    //       tap({complete: () => this.isBusy = false})
    //     );
    // }

    let record: any = {
      JenisCetak: 'Peringatan Notis',
      TarikhMula: '01/01/2020',
      TarikhAkhir: '15/01/2020',
      TarikhCetak: '20/01/2020',
    };
    return of(record);
  }


  getPeringatanList(page: Pager): Observable<any> {
    //console.log('getNotisOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    //console.log('params getNotisList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { Id: 1, NoNotis: 'DC0001', Jenis: 'JPK 55-Trafik', NoLesen: '888111', NoKenderaan: 'WWW9999', NamaPesalah: 'Razman B. Md Zainal', NoPesalah: '840819075337', TarikhKesalahan: '25/01/2020 03:27 PM', Status: 'T' },
      { Id: 2, NoNotis: 'DC0002', Jenis: 'JPK 55-Trafik', NoLesen: '666777', NoKenderaan: 'JKP9981', NamaPesalah: 'Razman B. Md Zainal', NoPesalah: '840819075337', TarikhKesalahan: '25/07/2020 03:27 PM', Status: 'T' },


    ];

    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }







  // no rujukan fail ------------------------------------------------------------------------------

  getFailList(page: Pager, OffenceAct: number): Observable<any> {
    //console.log('getNotisOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    console.log('params getNotisList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [];

    if (OffenceAct == 1) //trafik
    {
      list = [
        { Id: 1, NoSaman: 'K99999', NoNotis: 'DC0001', TarikhKes: '25/01/2020 03:27 PM', NoKenderaan: 'WTT0891', TarikhDaftar: '25/01/2020', TarikhSaman: '25/01/2020', TarikhSebutan: '25/01/2020', Keputusan: 'Keputusan 1', NoFail: '123456' },
        { Id: 2, NoSaman: 'J98891', NoNotis: 'H78178', TarikhKes: '14/01/2020 03:27 PM', NoKenderaan: 'PKJ0831', TarikhDaftar: '14/01/2020', TarikhSaman: '14/01/2020', TarikhSebutan: '14/01/2020', Keputusan: 'Keputusan 2', NoFail: '888811' },

      ];
    }
    else if (OffenceAct == 2) //am
    {
      list = [
        { Id: 1, NoSaman: 'J98891', NoNotis: 'H78178', TarikhKesalahan: '14/01/2020 03:27 PM', NamaSyarikat: 'Pemaju Sdn. Bhd.', NoTpr: '221', NoIp: 'ASD7788', TarikhFail: '14/01/2020', NoFail: '888811' },
        { Id: 2, NoSaman: 'Y87191', NoNotis: 'N89131', TarikhKesalahan: '10/01/2020 13:27 PM', NamaSyarikat: 'Syarikat Sdn. Bhd.', NoTpr: '334', NoIp: 'KL891369', TarikhFail: '04/01/2020', NoFail: '112222' },

      ];
    }

    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }











  // notis -------------------------------------------------------------------------------------


  getNotisDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */

    //  getProductGeneral(id: number): Observable<ProductGeneral> {
    //   const params = new HttpParams().set('productId', String(id));
    //   this.isBusy = true;
    //   return this.http.get<ProductGeneral>(this.baseUrl + ProductApiEndpoint.General, {params})
    //     .pipe(
    //       map((resp: any) => {
    //         const model = resp.basicInfo[0] as ProductGeneral;
    //         model.imageList = resp.imageList as ProductImage[];
    //         return model;
    //       }),
    //       tap({complete: () => this.isBusy = false})
    //     );
    // }

    let record: any = {

      SebabBatal: 1,
      ButiranBatal: 'Catatan',
      NamaPemohonBatal: 'Mohd Fahmi',
      AlamatBatal1: 'Alamat 1',
      AlamatBatal2: 'Alamat 2',
      AlamatBatal3: 'Alamat 3',
      PoskodBatal: '99999',
      NegeriBatal: 1,


      JenisNotis: '1',
      TarikhKesalahan: '02 Jan 2020',
      TarikhWujud: '02 Jan 2020',
      TarikhTamatKompaun: '27 Jan 2020',
      NoNotis: '8892189',
      WaktuKesalahan: '15:10',
      TarikhMahkamah: '02 Jan 2020',
      NoKenderaan: 'WTS8122',
      Peruntukan: 1,
      NoCukaiJalan: '8892189',
      Seksyen: 1,
      Nota: 'Catatan',
      TarikhTamatCukai: '02 Jan 2020',
      PeruntukanKesalahan: 'Catatan',
      JenamaKenderaan: 1,
      ModelKenderaan: 1,
      ButiranKesalahan: 'Catatan',
      JenisBadan: 1,
      Penguatkuasa: 1,
      Kawasan: 1,
      Bahagian: 1,
      Tempat: 'Jalan Duta',
      IdDde: '121212',
      Petak: '2',
      Handheld: 1,
      ButiranLokasi: 'Jalan Duta',

      StatusKesalahan: 1,
      NoAduan: '12345',
      NoPeringatan: '12345',
      TarikhAduan: '02 Jan 2020',
      TarikhPeringatan: '02 Jan 2020',
      NoFailRujukan: '98762',
      TarikhFailRujukan: '02 Jan 2020',

      NamaPesalah: 'Muhd Naiff',
      NoKPSyarikatPesalah: '880909119876',
      NoLesen: '882717812',
      TarikhTamatLesen: '02 Jan 2020',
      AlamatPesalah1: 'Alamat 1',
      AlamatPesalah2: 'Alamat 2',
      AlamatPesalah3: 'Alamat 3',
      PoskodPesalah: '99999',
      NegeriPesalah: 1,
      FlagSenaraiHitam: 1,
      FlagPemerhatian: 0,
      FlagSiasatan: 0,

      KadarKompaun: 100.00,
      TarikhBayaran: '02 Jan 2020',
      KadarBayaran: 100.00,
      KadarRayuan: 50.00,
      NoResit: '900001',

      NoKes: '888111',
      StatusMahkamah: 1,
      TarikhSebutan: '02 Jan 2020',
      TarikhWaran: '15 Jan 2020',
      TarikhBicara: '30 Jan 2020',

      NamaPemandu: 'Muhd Sham',
      NoKPPemandu: '770707889990',
      AlamatPemandu1: 'Alamat 1',
      AlamatPemandu2: 'Alamat 2',
      AlamatPemandu3: 'Alamat 3',
      PoskodPemandu: '99999',
      NegeriPemandu: 1,

      TarikhCipta: '02 Jan 2020',
      DiciptaOleh: 'Muhd Naiff',
      TarikhKemaskini: '03 Jan 2020',
      DikemaskiniOleh: 'Nik Fahmi',




      imageList: [
        { imageId: '1', imageURL: 'https://homepages.cae.wisc.edu/~ece533/images/airplane.png' },
        { imageId: '2', imageURL: 'https://homepages.cae.wisc.edu/~ece533/images/baboon.png' },
        { imageId: '3', imageURL: 'https://homepages.cae.wisc.edu/~ece533/images/watch.png' },
        { imageId: '4', imageURL: 'https://homepages.cae.wisc.edu/~ece533/images/monarch.png' },
        // {imageId: '5', imageURL: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png'},
      ]
    };
    return of(record);
  }

  saveNotis(model, Id: number): Observable<any> {
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Notis disimpan!';
    const Id2 = 1;
    return of({ ReturnCode, ResponseMessage, Id: Id2 });
  }

  deleteImage(imageId: number) {
    // const params = new HttpParams().set('productImageId', String(imageId));

    // this.isBusy = true;
    // return this.http.delete(this.baseUrl + ProductApiEndpoint.DeleteImage, {params})
    //   .pipe(
    //     tap({complete: () => this.isBusy = false})
    //   );
    const ReturnCode = 200;
    const ResponseMessage = 'Gambar telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  deleteNotis(ids: Array<{ id: number }>) {
    //console.log('deleteNotis ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod notis telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getNotisList(page: Pager): Observable<any> {
    //console.log('getNotisOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    //console.log('params getNotisList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { Id: 1, NoNotis: 'DC0001', Jenis: 'JPK 55-Trafik', NoKenderaan: 'WWW9999', NamaPesalah: 'Razman B. Md Zainal', NoPesalah: '840819075337', Pemandu: 'Razman B. Md Zainal', KpPemandu: '840819075337', TarikhNotis: '25/01/2020', TarikhKesalahan: '25/01/2020 03:27 PM', Perundangan: 'Akta Pengangkutan Jalan 1974', Seksyen: 'Sek 39', Tempat: 'Jalan Duta', Kompaun: '300.00', Status: 'T', NoResit: '78173', TarikhBayaran: '25/01/2020 03:27 PM', Bayaran: '300.00' },
      { Id: 2, NoNotis: 'DC0002', Jenis: 'JPK 55-Trafik', NoKenderaan: 'JKP9981', NamaPesalah: 'Razman B. Md Zainal', NoPesalah: '840819075337', Pemandu: 'Nik Ahmad Fahmi', KpPemandu: '880819075337', TarikhNotis: '25/07/2020', TarikhKesalahan: '25/07/2020 03:27 PM', Perundangan: 'Akta Pengangkutan Jalan 1974', Seksyen: 'Sek 39', Tempat: 'Jalan Tun Razak', Kompaun: '300.00', Status: 'T', NoResit: '87317', TarikhBayaran: '25/09/2020 03:27 PM', Bayaran: '300.00' },


    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }


  uploadFile(data: FormData): Observable<any> {
    //console.log('uploadFile:::', data);
    this.isBusy = true;
    // return this.http.post<string>(this.baseUrl + ProductApiEndpoint.UploadImage, data)
    //   .pipe(
    //     // map((resp: any) => resp.Result[0].ImageFileName),
    //     map((resp: any) => {
    //       console.log('resp uploadFile ::::::::::::::: ', resp);
    //       const ImageFileName =  resp.Result[0].ImageFileName;
    //       /*const ReturnCode = resp.ReturnCode;
    //       const ResponseMessage = resp.ResponseMessage;
    //       const Id = productId;
    //       return { ReturnCode, ResponseMessage, Id };*/
    //       console.log('resp ImageFileName ::::::::::::::: ', ImageFileName);
    //       return ( ImageFileName );
    //     }),
    //     tap({
    //       error: () => this.isBusy = false,
    //       complete: () => this.isBusy = false
    //     })
    //   );


    return of({ imageId: '1', imageURL: 'https://homepages.cae.wisc.edu/~ece533/images/airplane.png' });
  }




















  // SP2U -------------------------------------------------------------------------------------


  getSp2uList(page: Pager): Observable<any> {
    //console.log('getNotisOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    //console.log('params getNotisList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { Id: 1, NoNotis: 'DC0001', NamaPesalah: 'Razman B. Md Zainal', NoPesalah: '840819075337', TarikhKesalahan: '25/01/2020 03:27 PM', Jabatan: 'Jabatan Penguatkuasa', Kompaun: '300.00', Status: 'P', TarikhBayaran: '25/01/2020 03:27 PM' },
      { Id: 2, NoNotis: 'DC0002', NamaPesalah: 'Nurwahida Binti Mat Din', NoPesalah: '850819075337', TarikhKesalahan: '05/01/2020 03:27 PM', Jabatan: 'Jabatan Penguatkuasa', Kompaun: '600.00', Status: 'P', TarikhBayaran: '30/01/2020 03:27 PM' },


    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }






  // rujukan --------------------------------------------------------------------------------


  getJalanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Jalan 1' },
      { id: 2, name: 'Jalan 2' },
      { id: 3, name: 'Jalan 3' },
    ];
    return of(list);
  }

  getJenisBadanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Badan 1' },
      { id: 2, name: 'Badan 2' },
      { id: 3, name: 'Badan 3' },
    ];
    return of(list);
  }

  getKawasanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Kawasan 1' },
      { id: 2, name: 'Kawasan 2' },
      { id: 3, name: 'Kawasan 3' },
    ];
    return of(list);
  }


  //refrence
  getBahagianList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Penguatkuasa' },
      { id: 2, name: 'Keselamatan' },
    ];
    return of(list);
  }


  getJenisNotisList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Jenis Notis 1' },
      { id: 2, name: 'Jenis Notis 2' },
      { id: 3, name: 'Jenis Notis 3' },
    ];
    return of(list);
  }


  getHandheldList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Handheld 1' },
      { id: 2, name: 'Handheld 2' },
      { id: 3, name: 'Handheld 3' },
    ];
    return of(list);
  }

  getStatusKesalahanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Status 1' },
      { id: 2, name: 'Status 2' },
      { id: 3, name: 'Status 3' },
    ];
    return of(list);
  }

  getJabatanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Jabatan 1' },
      { id: 2, name: 'Jabatan 2' },
      { id: 3, name: 'Jabatan 3' },
    ];
    return of(list);
  }

  getStatusMahkamahList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Status Mahkamah 1' },
      { id: 2, name: 'Status Mahkamah 2' },
      { id: 3, name: 'Status Mahkamah 3' },
    ];
    return of(list);
  }

  getNegeriList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Johor' },
      { id: 2, name: 'Kedah' },
      { id: 3, name: 'Pahang' },
      { id: 4, name: 'Perak' },
      { id: 5, name: 'Selangor' },
    ];
    return of(list);
  }


  getPenguatkuasaList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Penguatkuasa 1' },
      { id: 2, name: 'Penguatkuasa 2' },
      { id: 3, name: 'Penguatkuasa 3' },
      { id: 4, name: 'Penguatkuasa 4' },
    ];
    return of(list);
  }

  getSebabBatalList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Sebab Batal 1' },
      { id: 2, name: 'Sebab Batal 2' },
      { id: 3, name: 'Sebab Batal 3' },
      { id: 4, name: 'Sebab Batal 4' },
    ];
    return of(list);
  }

  getJenamaKenderaanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Jenama 1' },
      { id: 2, name: 'Jenama 2' },
      { id: 3, name: 'Jenama 3' },
      { id: 4, name: 'Jenama 4' },
    ];
    return of(list);
  }

  getModelKenderaanList(Jenama): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Model 1' },
      { id: 2, name: 'Model 2' },
      { id: 3, name: 'Model 3' },
      { id: 4, name: 'Model 4' },
    ];
    return of(list);
  }

  getSeksyenList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Seksyen 1' },
      { id: 2, name: 'Seksyen 2' },
      { id: 3, name: 'Seksyen 3' },
      { id: 4, name: 'Seksyen 4' },
      { id: 5, name: 'Seksyen 5' },
      { id: 6, name: 'Seksyen 6' },
      { id: 7, name: 'Seksyen 7' },
      { id: 8, name: 'Seksyen 8' },
    ];
    return of(list);
  }

  getPembolehubahList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Pembolehubah 1' },
      { id: 2, name: 'Pembolehubah 2' },
    ];
    return of(list);
  }


  getPeruntukanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Peruntukan Undang - Undang 1' },
      { id: 2, name: 'Peruntukan Undang - Undang 2' },
      { id: 3, name: 'Peruntukan Undang - Undang 3' },
      { id: 4, name: 'Peruntukan Undang - Undang 4' },
    ];
    return of(list);
  }

  getAktaIndukList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Akta Induk 1' },
      { id: 2, name: 'Akta Induk 2' },
      { id: 3, name: 'Akta Induk 3' },
      { id: 4, name: 'Akta Induk 4' },
    ];
    return of(list);
  }

  getOffenceActList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'TRAFIK' },
      { id: 2, name: 'AM' },
    ];
    return of(list);
  }


  getJenisKesalahanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Jenis Kesalahan 1' },
      { id: 2, name: 'Jenis Kesalahan 2' },
      { id: 3, name: 'Jenis Kesalahan 3' },
    ];
    return of(list);
  }








}




import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { NotisService } from '../../notis.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { ReportTemplete } from '../../../../report';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');
import * as $ from 'jquery';

@Component({
  selector: 'app-fail_rujukan-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class FailListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  reportTemplete: ReportTemplete;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  eventSearch: any;
  filterValue: any;
  onSearch: boolean = false;
  showList: boolean = false;
  submitted: boolean = false;
  reVisit: boolean = false;
  form: FormGroup;
  
  //close standard var for searching page

  // any unique name for searching list
  searchID = 'getFailList';

  //initiate list
  JenisKesalahanList: any = [];
  OffenceActList: any = [];

  //initiate list model 
  OffenceAct: number = 0;
  JenisKesalahan: number = 0;

  //initiate date model 
  TarikhMula: any;
  TarikhAkhir: any;
  TarikhFail: any;
  TarikhKesalahan: any;

  //report props
  reportColTrafik: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'NoSaman', title: 'No. Saman', width: '15', align: 'left' },
    { name: 'NoNotis', title: 'No. Notis', width: '15', align: 'left' },
    { name: 'TarikhKes', title: 'Tarikh Kes', width: '15', align: 'center' },
    { name: 'NoKenderaan', title: 'No. Kenderaan', width: '15', align: 'left' },
    { name: 'TarikhDaftar', title: 'Tarikh Daftar', width: '15', align: 'center' },
    { name: 'TarikhSaman', title: 'Tarikh Keluar Saman', width: '15', align: 'center' },   
    { name: 'TarikhSebutan', title: 'Tarikh Sebutan', width: '15', align: 'center' },
    { name: 'Keputusan', title: 'Keputusan', width: '15', align: 'left' },
    { name: 'NoFail', title: 'No. Fail Rujukan', width: '15', align: 'left' },
  ];
 
  reportColAm: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'NoSaman', title: 'No. Saman', width: '15', align: 'left' },
    { name: 'NoNotis', title: 'No. Notis', width: '15', align: 'left' },
    { name: 'TarikhKesalahan', title: 'Tarikh Kesalahan', width: '15', align: 'center' },
    { name: 'NamaSyarikat', title: 'Nama Syarikat', width: '15', align: 'left' },
    { name: 'NoTpr', title: 'No. TPR', width: '15', align: 'left' },
    { name: 'NoIp', title: 'No. IP', width: '15', align: 'left' },
    { name: 'TarikhFail', title: 'Tarikh Fail Rujukan', width: '15', align: 'center' },
    { name: 'NoFail', title: 'No. Fail Rujukan', width: '15', align: 'left' },
  ];
  
  reportTitle: string = '';


  //standard contructor
  constructor(
    private notisService: NotisService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
    this.reportTemplete = new ReportTemplete();
  }

  //form
  get f() { return this.form.controls; }


  //open standard function use for multiple delete in datatable
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.CustomerId + 1}`;
  }
  onRowSelect(event) {
    const evt = event.originalEvent as MouseEvent;
    evt.stopPropagation();
  }
  //close standard function use for multiple delete in datatable


  //init methode to call default function
  ngOnInit() {
    if (this.pageSetting.getSearchFilterValueSV(this.searchID)) {
      this.reVisit = true;
    }
    this.getFailList();
    this.getOffenceActList();
    this.getJenisKesalahanList();
  }

  // loadSearchDetails() {
  //   this.form = this.buildFormItems();
  // }

 

  //open standard listing with search function
  async getFailList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    await this.reloadSearchForm();
    await this.notisService.getFailList(this.pageSetting.getPagerSettingWithSearch(this.paginator, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)), this.OffenceAct)
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (this.onSearch) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    await this.primengTableHelper.showLoadingIndicator();
  }
  //close standard listing with search function

  //open default listing function
  // getCetakanFailAkhir() {
  //   this.notisService.getCetakanFailAkhir().subscribe(items => {
  //     this.CetakanTerakhir = items;
  //   });
  // }

  //open default listing function
  getOffenceActList() {
    this.notisService.getOffenceActList().subscribe(items => {
      this.OffenceActList = items;
    });
  }
  getJenisKesalahanList() {
    this.notisService.getJenisKesalahanList().subscribe(items => {
      this.JenisKesalahanList = items;
    });
  }
  //close default listing function


  //open reload search form
  reloadSearchForm() {
    this.filterValue = this.pageSetting.getSearchFilterValueSV(this.searchID);
    let SFcurrent = this.pageSetting.getSearchFilterValueSF(this.searchID);
    this.form = this.buildFormItems();
    if (SFcurrent) {     
      //reasing value for list & date model
      this.OffenceAct = SFcurrent.OffenceAct > 0 ? SFcurrent.OffenceAct : 0;
      this.JenisKesalahan = SFcurrent.JenisKesalahan > 0 ? SFcurrent.JenisKesalahan : 0;
      this.TarikhMula = SFcurrent.TarikhMula = moment(SFcurrent.TarikhMula) || '';
      this.TarikhAkhir = SFcurrent.TarikhAkhir = moment(SFcurrent.TarikhAkhir) || '';
      this.TarikhKesalahan = SFcurrent.TarikhKesalahan = moment(SFcurrent.TarikhKesalahan) || '';
      this.TarikhFail = SFcurrent.TarikhFail = moment(SFcurrent.TarikhFail) || '';

      this.form.patchValue(SFcurrent);
    } else {
      //initiate list & date model
      this.OffenceAct = 0;
      this.JenisKesalahan = 0;
      this.TarikhMula = '';
      this.TarikhAkhir = '';
      this.TarikhKesalahan = ''
      this.TarikhFail = ''
  
    }
  }
  //close reload search form


  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.showList = true;
      this.submitted = false;
      //setting store procedure listing field for filtering     
      this.filterValue = {
        " OffenceAct ": { value: searchForm.OffenceAct, matchMode: "equals" },
        " JenisKesalahan ": { value: searchForm.JenisKesalahan, matchMode: "equals" },
        " NoNotis ": { value: searchForm.NoNotis, matchMode: "contains" },
        " NoIp ": { value: searchForm.NoIp, matchMode: "contains" },
        " NoTpr ": { value: searchForm.NoTpr, matchMode: "contains" },
        " NoFail ": { value: searchForm.NoFail, matchMode: "contains" },
        " TarikhMula ": { value: searchForm.TarikhMula, matchMode: "dateGreater" },
        " TarikhAkhir ": { value: searchForm.TarikhAkhir, matchMode: "dateLower" },
        " TarikhFail ": { value: searchForm.TarikhFail, matchMode: "dateEquals" },      
        " TarikhKesalahan ": { value: searchForm.TarikhKesalahan, matchMode: "dateEquals" },   
      }
      this.pageSetting.setSearchFilterLS(this.filterValue, searchForm, this.searchID);
      this.onSearch = true;
      this.getFailList();
      this.paginator.changePage(0);

      let OffenceActObject = this.OffenceActList.find(i => i.id == this.OffenceAct);
      this.reportTitle = 'Senarai No. Fail Rujukan ' + OffenceActObject.name;
     
     
    }
  }

  //reset search form
  resetType() {
    const OffenceAct = this.OffenceAct;
    this.reset();
    this.form = this.buildFormItems();
    this.OffenceAct = OffenceAct;
    this.form.patchValue({OffenceAct});
  }

  reset() {
    this.pageSetting.removeSearchFilterLS(this.searchID);
    this.form = this.buildFormItems();

    //initiate list & date model
    this.OffenceAct = 0;
    this.JenisKesalahan = 0;
    this.TarikhMula = '';
    this.TarikhAkhir = '';
    this.TarikhKesalahan = ''
    this.TarikhFail = ''

    this.filterValue = null;
    this.getFailList();
    this.paginator.changePage(0);

    this.showList = false;
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {

      if(formGroup.dirty)
      {
        this.showList = false;
      }      

      const OffenceAct = formGroup.controls['OffenceAct'];
      const TarikhMula = formGroup.controls['TarikhMula'];
      const TarikhAkhir = formGroup.controls['TarikhAkhir'];
      const TarikhFail = formGroup.controls['TarikhFail'];
      const TarikhKesalahan = formGroup.controls['TarikhKesalahan'];

      if (OffenceAct) {
        OffenceAct.setErrors(null);
        if (!OffenceAct.value) {
          OffenceAct.setErrors({ required: true });
        }
        else if (OffenceAct.value == 0) {
          OffenceAct.setErrors({ min: true });
        }
      }
      if (TarikhFail) {
        TarikhFail.setErrors(null);
      }
      if (TarikhKesalahan) {
        TarikhKesalahan.setErrors(null);
      }
      if (TarikhMula) {
        TarikhMula.setErrors(null);
        if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhMula.setErrors({ exceed: true });
        }
      }
      if (TarikhAkhir) {
        TarikhAkhir.setErrors(null);
        if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhAkhir.setErrors({ exceed: true });
        }
      }


    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      OffenceAct: ['0', []],
      JenisKesalahan: ['0', []],      
      TarikhKesalahan: ['', []],
      NoNotis: ['', []],
      NoIp: ['', []],
      NoTpr: ['', []],    
      NoFail: ['', []],
      TarikhFail: ['', []],
      TarikhMula: ['', []],
      TarikhAkhir: ['', []],
    }, { validator: this.customValidation() }
    );
  }

  //excel genarator
  printTableExcel() {
    this.showSpinner = true;
    let list: any;
    this.notisService.getFailList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)), this.OffenceAct)
      .subscribe(items => {
        list = items.list;
        this.pageSetting.excelGenerator(this.OffenceAct == 1 ? this.reportColTrafik : this.reportColAm, list, this.reportTitle, this.reportTitle).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }

  //pdf genarator
  printTablePdf() {
    this.showSpinner = true;
    let list: any;
    this.notisService.getFailList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)), this.OffenceAct)
      .subscribe(items => {
        list = items.list;
        this.pageSetting.pdfGenerator(this.OffenceAct == 1 ? this.reportColTrafik : this.reportColAm, list, this.reportTitle, this.reportTitle, 'l', 7).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }


  //singlePrin
  // singlePrintFail(event, Id)
  // {
  //   event.stopPropagation();
    
  //   this.showSpinner = true;
  //   let listSetData: any = [];
  //   this.notisService.getNotisDetails(Id)
  //     .subscribe(result => {
  //       listSetData.push(result);
  //     });

  //     this.reportTemplete.peringatanNotis(listSetData).then(loaded => {
  //       if (loaded) {
  //         this.showSpinner = false;
  //       }
  //     });
    
  // }


  //multiple print notis function
  // selectionPrint(type) {
  //   this.showSpinner = true;
  //   let selectedItems: Array<{ Id: number }>;
  //   let listSetData: any = [];
  //   if (type == 'Select_A4' || type == 'Select_4' || type == 'Select_Matrix') {
  //     selectedItems = this.selection.selected.map(item => ({ Id: item.Id }));
  //   }
  //   else if (type == 'All_A4' || type == 'All_4' || type == 'All_Matrix') {
  //     //let list: any;
  //     this.notisService.getFailList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)), this.OffenceAct)
  //       .subscribe(items => {
  //         selectedItems =  items.list.map(item => ({ Id: item.Id })); 
  //       });
  //   }
  //   selectedItems.map(item => { 
  //     this.notisService.getNotisDetails(item.Id)
  //     .subscribe(result => {
  //       listSetData.push(result);
  //     });
  //   });
  //   this.reportTemplete.peringatanNotis(listSetData).then(loaded => {
  //     if (loaded) {
  //       this.showSpinner = false;
  //     }
  //   });
  // }


  //multiple delete function
  delete() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.Id }));
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod yang telah hapus tidak akan dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        this.notisService.deleteNotis(selectedItems)
          .subscribe(resultDelete => {
            this.showSpinner = false;
            if (resultDelete.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultDelete.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultDelete.ResponseMessage,
                'success'
              );
            }
            this.getFailList();
          });
      }
    })
  }

}

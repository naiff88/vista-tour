import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CetakanNotisListComponent } from './list.component';

describe('CetakanNotisListComponent', () => {
  let component: CetakanNotisListComponent;
  let fixture: ComponentFixture<CetakanNotisListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CetakanNotisListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CetakanNotisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AduanMahkamahListComponent } from './list.component';

describe('AduanMahkamahListComponent', () => {
  let component: AduanMahkamahListComponent;
  let fixture: ComponentFixture<AduanMahkamahListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AduanMahkamahListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AduanMahkamahListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

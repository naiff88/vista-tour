import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AduanMahkamahDetailsComponent } from './aduan-mahkamah-details.component';

describe('AduanMahkamahDetailsComponent', () => {
  let component: AduanMahkamahDetailsComponent;
  let fixture: ComponentFixture<AduanMahkamahDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AduanMahkamahDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AduanMahkamahDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

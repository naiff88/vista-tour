/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotisService } from '../../notis.service';
import { ReportTemplete } from '../../../../report';
import Swal from 'sweetalert2';
import * as _ from 'lodash';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';


@Component({
  selector: 'app-aduan-mahkamah-details',
  templateUrl: './aduan-mahkamah-details.component.html',
  styleUrls: ['./aduan-mahkamah-details.component.scss']
})


export class AduanMahkamahDetailsComponent implements OnInit {

  reportTemplete: ReportTemplete;
  showSpinner: boolean = false;
  submitted = false;
  form: FormGroup; 
  FormType: string;
  FromMenu: string = '';
  FormTitle: string = '';
  ViewMode: boolean = false;

  Id: number;  

  
  NegeriPesalah: number = 0;
  NegeriList: any = [];

  constructor(
    private notisService: NotisService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
    this.reportTemplete = new ReportTemplete();
  }

  get f() { return this.form.controls; }

  //open init function
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.FromMenu = params.fromMenu;
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;       
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });

    //ref list call   
    this.getNegeriList();

    this.loadDetails(this.Id);
  }
  //close init function

  //open load form
  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.notisService.getAduanMahkamahDetails(Id)
        .subscribe(result => {
          this.showSpinner = false;  
          this.NegeriPesalah = result.NegeriPesalah > 0 ? result.NegeriPesalah : 0;
          this.form.patchValue(result);
        });
    }
  }
  //close load form


  //open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod semasa akan ditetapkan semula!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  //open reset function

  //open save function
  onSave() {
    //console.log('this.form ::::::::::::::::::::::::;' , this.form);
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat sah & lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.showSpinner = true;
      this.notisService.saveAduanMahkamah(model, this.Id)
        .subscribe(r => {
          this.showSpinner = false;
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.Id = r.Id;
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            this.router.onSameUrlNavigation = 'reload';
            if(this.FromMenu == 'kemasukan')
            {             
              this.router.navigate(['/app/notis/aduan-mahkamah-details/',  this.Id , 'view', this.FromMenu]);
            }
            else
            {
              this.router.navigate(['/app/notis/aduan-mahkamah-details/',  this.Id , 'edit']);
            }            
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }
  //close save function

   //singlePrin
   singlePrint(type)
   {
    this.showSpinner = true;
     let listSetData: any = [];
     this.notisService.getNotisDetails(this.Id)
       .subscribe(result => {
         listSetData.push(result);
       });
 
       this.reportTemplete.aduanMahkamah(listSetData).then(loaded => {
         if (loaded) {
           this.showSpinner = false;
         }
       });    
   }


  //open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {

      const NegeriPesalah = formGroup.controls['NegeriPesalah'];
      const NoNotis = formGroup.controls['NoNotis'];
      const NamaPesalah = formGroup.controls['NamaPesalah'];
      const NoKPSyarikatPesalah = formGroup.controls['NoKPSyarikatPesalah'];
      const AlamatPesalah1 = formGroup.controls['AlamatPesalah1'];
      const PoskodPesalah = formGroup.controls['PoskodPesalah'];
      const NoPkt = formGroup.controls['NoPkt'];
      const Penerangan = formGroup.controls['Penerangan'];
      const NamaPenguatkuasa = formGroup.controls['NamaPenguatkuasa'];


      //maklumat pesalah
      if (NamaPesalah) { NamaPesalah.setErrors(null); if (!NamaPesalah.value) { NamaPesalah.setErrors({ required: true }); } }
      if (NoKPSyarikatPesalah) { NoKPSyarikatPesalah.setErrors(null); if (!NoKPSyarikatPesalah.value) { NoKPSyarikatPesalah.setErrors({ required: true }); } }
      if (NoNotis) { NoNotis.setErrors(null); if (!NoNotis.value) { NoNotis.setErrors({ required: true }); } }
      if (NoPkt) { NoPkt.setErrors(null); if (!NoPkt.value) { NoPkt.setErrors({ required: true }); } }
      if (Penerangan) { Penerangan.setErrors(null); if (!Penerangan.value) { Penerangan.setErrors({ required: true }); } }
      if (AlamatPesalah1) { AlamatPesalah1.setErrors(null); if (!AlamatPesalah1.value) { AlamatPesalah1.setErrors({ required: true }); } }
      if (PoskodPesalah) { PoskodPesalah.setErrors(null); if (!PoskodPesalah.value) { PoskodPesalah.setErrors({ required: true }); } else if (PoskodPesalah.value.length != 5) { PoskodPesalah.setErrors({ minlength: true }); } }
      if (NegeriPesalah) { NegeriPesalah.setErrors(null); if (!NegeriPesalah.value) { NegeriPesalah.setErrors({ required: true }); } else if (NegeriPesalah.value == 0) { NegeriPesalah.setErrors({ min: true }); } }
      if (NamaPenguatkuasa) { NamaPenguatkuasa.setErrors(null); if (!NamaPenguatkuasa.value) { NamaPenguatkuasa.setErrors({ required: true }); } }
     
    }
  }
  //open custom validation


  //open form setting 
  buildFormItems(): FormGroup {
    return this.formBuilder.group({

      NamaPesalah: ['', []],
      NoKPSyarikatPesalah: ['', []],
      NoNotis: ['', []],
      NoPkt: ['', []],
      NamaPenguatkuasa: ['', []],
      AlamatPesalah1: ['', []],
      AlamatPesalah2: ['', []],
      AlamatPesalah3: ['', []],
      PoskodPesalah: ['', []],
      NegeriPesalah: ['0', []],
      Penerangan: ['', []],     

      TarikhCipta: ['', []],
      DiciptaOleh: ['', []],
      TarikhKemaskini: ['', []],
      DikemaskiniOleh: ['', []],

    }, { validator: this.customValidation() }
    );
  }
  //open form setting 

  
  // open get ref list 
  getNegeriList() {
    this.notisService.getNegeriList().subscribe(items => {
      this.NegeriList = items;
    });
  } 
  // close get ref list 
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotisRoutingModule } from './notis-routing.module';

import { MatRippleModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar} from '@angular/material/snack-bar';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule} from '@angular/material/dialog';
import { MatIconModule} from '@angular/material/icon';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';


import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { JoditAngularModule } from 'jodit-angular';
import { AppCommonModule } from '../shared/app-common.module';
import { QuillModule } from 'ngx-quill';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MiscModule } from '../misc/misc.module';
import { PMServiceProxy } from 'src/shared/service-proxies/service-proxies';
import { getQuillEditorSettings } from 'src/shared/helpers/quill-helper';
import { NgxPrintModule } from 'ngx-print';
import { DashboardModule } from '../dashboard/dashboard.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NotisListComponent } from './notis/list/list.component';
import { NotisDetailsComponent } from './notis/notis-details/notis-details.component';
import { PeringatanListComponent } from './peringatan/list/list.component';
import { FailListComponent } from './fail/list/list.component';
import { CetakanNotisListComponent } from './cetakan-notis/list/list.component';
import { AduanMahkamahListComponent } from './aduan-mahkamah/list/list.component';
import { AduanMahkamahDetailsComponent } from './aduan-mahkamah/aduan-mahkamah-details/aduan-mahkamah-details.component';
import { Sp2uListComponent } from './sp2U/list/list.component';
import { PertuduhanDetailsComponent } from './pertuduhan/pertuduhan-details.component';

const materialModules = [
  MatButtonModule,
  MatTableModule,
  MatCheckboxModule,
  MatDialogModule,
  MatSortModule,
  MatTabsModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatProgressBarModule,
  MatIconModule,
  MatSnackBarModule,
  MAT_DIALOG_DATA,
  MatDialogRef
];

@NgModule({
  declarations: [
    NotisListComponent,
    NotisDetailsComponent,
    PeringatanListComponent,
    FailListComponent,
    CetakanNotisListComponent,
    AduanMahkamahListComponent,
    AduanMahkamahDetailsComponent,
    Sp2uListComponent,
    PertuduhanDetailsComponent
  ],
  imports: [
    DashboardModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppCommonModule,
    JoditAngularModule,
    NotisRoutingModule,
    QuillModule.forRoot(getQuillEditorSettings()),
    TableModule,
    PaginatorModule,
    ModalModule.forRoot(),
    MatDatepickerModule,
    MatMomentDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSortModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatIconModule,
    MatSnackBarModule,
    MiscModule,
    NgxPrintModule,
    MatRadioModule,
    PerfectScrollbarModule,
    NgMultiSelectDropDownModule,
    BsDropdownModule.forRoot(),
  ],
  entryComponents: [
    //PilihAhliComponent
    // Variation1Component,
    // Variation2Component
    // CustomerDetailsComponent,
    // InvoiceNewItemComponent,
    // InvoicePreviewComponent,
    // InvoiceNewPaymentComponent,
    // InvoiceViewReceiptComponent
  ],
  // providers: [PMServiceProxy],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    { provide: PMServiceProxy },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
  ]
})
export class NotisModule {
}

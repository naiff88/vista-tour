import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Sp2uListComponent } from './list.component';

describe('Sp2uListComponent', () => {
  let component: Sp2uListComponent;
  let fixture: ComponentFixture<Sp2uListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Sp2uListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Sp2uListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PertuduhanDetailsComponent } from './a./pertuduhan-details.component

describe('PertuduhanDetailsComponent', () => {
  let component: PertuduhanDetailsComponent;
  let fixture: ComponentFixture<PertuduhanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PertuduhanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PertuduhanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotisService } from '../notis.service';
import { ReportTemplete } from '../../../report';
import Swal from 'sweetalert2';
import * as _ from 'lodash';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';


@Component({
  selector: 'app-pertuduhan-details',
  templateUrl: './pertuduhan-details.component.html',
  styleUrls: ['./pertuduhan-details.component.scss']
})


export class PertuduhanDetailsComponent implements OnInit {

  reportTemplete: ReportTemplete;
  showSpinner: boolean = false;
  submitted = false;
  form: FormGroup; 
  FormType: string;
  FromMenu: string = '';
  FormTitle: string = '';
  ViewMode: boolean = false;

  Id: number;  

  Peruntukan: number = 0;
  Seksyen: number = 0;
  Pembolehubah: number = 0;

  PeruntukanList: any = [];
  SeksyenList: any = [];
  PembolehubahList: any = [];

  constructor(
    private notisService: NotisService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
    this.reportTemplete = new ReportTemplete();
  }

  get f() { return this.form.controls; }

  //open init function
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.FromMenu = params.fromMenu;
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;       
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });

    //ref list call   
    this.getPembolehubahList();
    this.getPeruntukanList();
    this.getSeksyenList();

    this.loadDetails(this.Id);
  }
  //close init function

  //open load form
  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.notisService.getPertuduhanDetails(Id)
        .subscribe(result => {
          this.showSpinner = false;  
          
          this.Peruntukan = result.Peruntukan > 0 ? result.Peruntukan : 0;
          this.Seksyen = result.Seksyen > 0 ? result.Seksyen : 0;
          this.Pembolehubah = result.Pembolehubah > 0 ? result.Pembolehubah : 0;
          this.form.patchValue(result);
        });
    }
  }
  //close load form


  //open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod semasa akan ditetapkan semula!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  //open reset function

  //kembali
  kembali(){
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate(['/app/notis/pertuduhan-details/0/add']);
  }

  //open save function
  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat sah & lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.showSpinner = true;
      this.notisService.savePertuduhan(model, this.Id)
        .subscribe(r => {
          this.showSpinner = false;
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.Id = r.Id;
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            this.router.onSameUrlNavigation = 'reload';
            this.router.navigate(['/app/notis/pertuduhan-details/',  this.Id , 'view']);
               
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }
  //close save function

  //open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {


      const Peruntukan = formGroup.controls['Peruntukan'];
      const Seksyen = formGroup.controls['Seksyen'];
      const Pembolehubah = formGroup.controls['Pembolehubah'];
      const Penerangan = formGroup.controls['Penerangan'];


      //maklumat pesalah
      if (Peruntukan) { Peruntukan.setErrors(null); if (!Peruntukan.value) { Peruntukan.setErrors({ required: true }); } else if (Peruntukan.value == 0) { Peruntukan.setErrors({ min: true }); } }
      if (Seksyen) { Seksyen.setErrors(null); if (!Seksyen.value) { Seksyen.setErrors({ required: true }); } else if (Seksyen.value == 0) { Seksyen.setErrors({ min: true }); } }
      if (Pembolehubah) { Pembolehubah.setErrors(null); if (!Pembolehubah.value) { Pembolehubah.setErrors({ required: true }); } else if (Pembolehubah.value == 0) { Pembolehubah.setErrors({ min: true }); } }
      if (Penerangan) { Penerangan.setErrors(null); if (!Penerangan.value) { Penerangan.setErrors({ required: true }); } }     
    }
  }
  //open custom validation


  //open form setting 
  buildFormItems(): FormGroup {
    return this.formBuilder.group({

      Peruntukan: ['0', []],
      Seksyen: ['0', []],
      Pembolehubah: ['0', []],
      Penerangan: ['', []],
      TarikhCipta: ['', []],
      DiciptaOleh: ['', []],
      TarikhKemaskini: ['', []],
      DikemaskiniOleh: ['', []],

    }, { validator: this.customValidation() }
    );
  }
  //open form setting 

  
  // open get ref list 
 getPeruntukanList() {
    this.notisService.getPeruntukanList().subscribe(items => {
      this.PeruntukanList = items;
    });
  }
  getSeksyenList() {
    this.notisService.getSeksyenList().subscribe(items => {
      this.SeksyenList = items;
    });
  }  
  getPembolehubahList() {
    this.notisService.getPembolehubahList().subscribe(items => {
      this.PembolehubahList = items;
    });
  }   
  // close get ref list 
}

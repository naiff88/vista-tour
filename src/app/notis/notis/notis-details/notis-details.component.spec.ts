import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotisDetailsComponent } from './notis-details.component';

describe('NotisDetailsComponent', () => {
  let component: NotisDetailsComponent;
  let fixture: ComponentFixture<NotisDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotisDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotisDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

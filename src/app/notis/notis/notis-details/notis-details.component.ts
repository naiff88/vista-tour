/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotisService } from '../../notis.service';
import Swal from 'sweetalert2';
import * as _ from 'lodash';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';


@Component({
  selector: 'app-notis-details',
  templateUrl: './notis-details.component.html',
  styleUrls: ['./notis-details.component.scss']
})


export class NotisDetailsComponent implements OnInit {

  showSpinner: boolean = false;
  submitted = false;
  form: FormGroup; 
  FormType: string;
  FromMenu: string = '';
  FormTitle: string = '';
  ViewMode: boolean = false;

  Id: number;  
  files: File;


  SebabBatal: number = 0;
  NegeriBatal: number = 0;
  JenisNotis: number = 0;
  Handheld: number = 0;
  JenisBadan: number = 0;
  Penguatkuasa: number = 0;
  Kawasan: number = 0;
  Bahagian: number = 0;
  JenamaKenderaan: number = 0;
  ModelKenderaan: number = 0;
  Seksyen: number = 0;
  Peruntukan: number = 0;
  StatusKesalahan: number = 0;
  NegeriPesalah: number = 0;
  NegeriPemandu: number = 0;
  FlagPemerhatian: number = 0;
  FlagSenaraiHitam: number = 0;
  FlagSiasatan: number = 0;
  StatusMahkamah: number = 0;
   
  WaktuKesalahan: string = '';
  TarikhKesalahan: any = '';
  TarikhWujud: any = moment();
  TarikhMahkamah: any = '';
  TarikhTamatCukai: any = '';
  TarikhAduan: any = '';
  TarikhPeringatan: any = '';
  TarikhFailRujukan: any = '';
  TarikhTamatLesen: any = '';
  TarikhTamatKompaun: any = '';
  TarikhBayaran: any = '';
  TarikhSebutan: any = '';
  TarikhWaran: any = '';
  TarikhBicara: any = '';
  
  SebabBatalList: any = [];
  JenisNotisList: any = [];
  HandheldList: any = [];
  JenisBadanList: any = [];
  PenguatkuasaList: any = [];
  KawasanList: any = [];
  BahagianList: any = [];
  JenamaKenderaanList: any = [];
  ModelKenderaanList: any = [];
  SeksyenList: any = [];
  PeruntukanList: any = [];
  StatusKesalahanList: any = [];
  NegeriList: any = [];
  StatusMahkamahList: any = [];

  constructor(
    private notisService: NotisService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
  }

  get f() { return this.form.controls; }

  get imageList(): FormArray {
    return this.form.get('imageList') as FormArray;
  }

  //open init function
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.FromMenu = params.fromMenu;
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;       
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });

    //ref list call
    this.getSebabBatalList();
    this.getHandheldList();
    this.getJenisBadanList();
    this.getPenguatkuasaList();
    this.getKawasanList();
    this.getBahagianList();
    this.getJenamaKenderaanList();
    this.getModelKenderaanList();
    this.getSeksyenList();
    this.getPeruntukanList();
    this.getStatusKesalahanList();
    this.getNegeriList();
    this.getJenisNotisList();
    this.getStatusMahkamahList();

    this.loadDetails(this.Id);
  }
  //close init function

  //open load form
  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.notisService.getNotisDetails(Id)
        .subscribe(result => {
          this.showSpinner = false;

          this.TarikhKesalahan = result.TarikhKesalahan = moment(result.TarikhKesalahan) || '';
          this.TarikhWujud = result.TarikhWujud = moment(result.TarikhWujud) || '';
          this.TarikhMahkamah = result.TarikhMahkamah = moment(result.TarikhMahkamah) || '';
          this.TarikhTamatCukai = result.TarikhTamatCukai = moment(result.TarikhTamatCukai) || '';
          this.TarikhAduan = result.TarikhAduan = moment(result.TarikhAduan) || '';
          this.TarikhPeringatan = result.TarikhPeringatan = moment(result.TarikhPeringatan) || '';
          this.TarikhFailRujukan = result.TarikhFailRujukan = moment(result.TarikhFailRujukan) || '';
          this.TarikhTamatLesen = result.TarikhTamatLesen = moment(result.TarikhTamatLesen) || '';
          this.TarikhTamatKompaun = result.TarikhTamatKompaun = moment(result.TarikhTamatKompaun) || '';  
          this.WaktuKesalahan = result.WaktuKesalahan;             
          this.TarikhSebutan = result.TarikhSebutan = moment(result.TarikhSebutan) || '';
          this.TarikhBicara = result.TarikhBicara = moment(result.TarikhBicara) || '';
          this.TarikhWaran = result.TarikhWaran = moment(result.TarikhWaran) || '';
          this.TarikhBayaran = result.TarikhBayaran = moment(result.TarikhBayaran) || '';

            
          this.SebabBatal = result.SebabBatal > 0 ? result.SebabBatal : 0;
          this.NegeriBatal = result.NegeriBatal > 0 ? result.NegeriBatal : 0;
          this.JenisNotis = result.JenisNotis > 0 ? result.JenisNotis : 0;
          this.Bahagian = result.Bahagian > 0 ? result.Bahagian : 0;
          this.Handheld = result.Handheld > 0 ? result.Handheld : 0;
          this.JenisBadan = result.JenisBadan > 0 ? result.JenisBadan : 0;
          this.Penguatkuasa = result.Penguatkuasa > 0 ? result.Penguatkuasa : 0;
          this.Kawasan = result.Kawasan > 0 ? result.Kawasan : 0;
          this.JenamaKenderaan = result.JenamaKenderaan > 0 ? result.JenamaKenderaan : 0;
          this.ModelKenderaan = result.ModelKenderaan > 0 ? result.ModelKenderaan: 0;
          this.Seksyen = result.Seksyen > 0 ? result.Seksyen : 0;
          this.Peruntukan = result.Peruntukan > 0 ? result.Peruntukan: 0;
          this.StatusKesalahan = result.StatusKesalahan > 0 ? result.StatusKesalahan : 0;
          this.NegeriPesalah = result.NegeriPesalah > 0 ? result.NegeriPesalah : 0;
          this.NegeriPemandu = result.NegeriPemandu > 0 ? result.NegeriPemandu : 0;
          this.StatusMahkamah = result.StatusMahkamah > 0 ? result.StatusMahkamah : 0;
          this.FlagSenaraiHitam = result.FlagSenaraiHitam > 0 ? result.FlagSenaraiHitam : 0;
          this.FlagPemerhatian = result.FlagPemerhatian > 0 ? result.FlagPemerhatian : 0;
          this.FlagSiasatan = result.FlagSiasatan > 0 ? result.FlagSiasatan : 0;

          const imageList = _.get(result, 'imageList', []);
          const imageListFormGroup: FormGroup[] = _.map(imageList, item => this.createImageFormItems(item));   
          this.form.setControl('imageList', this.formBuilder.array(imageListFormGroup));

          this.form.patchValue(result);
        });
    }
  }
  //close load form


  onChangeIsFlag(event,modelName) {
    if (event.checked == false) {
      this[modelName] = 0;
    }
    else {
      this[modelName] = 1;
    }
  }

  //open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod semasa akan ditetapkan semula!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  //open reset function

  //kembali
  kembali(){
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
    if(this.FromMenu == "kemasukan")
    {      
      this.router.navigate(['/app/notis/notis-details/0/add/kemasukan']);
    }
    else
    {
      this.router.navigate(['/app/notis/senarai-notis']);      
    }   
  }


  //open save function
  onSave() {
    //console.log('this.form ::::::::::::::::::::::::;' , this.form);
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat sah & lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.showSpinner = true;
      this.notisService.saveNotis(model, this.Id)
        .subscribe(r => {
          this.showSpinner = false;
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.Id = r.Id;
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            this.router.onSameUrlNavigation = 'reload';
            if(this.FromMenu == 'kemasukan')
            {             
              this.router.navigate(['/app/notis/notis-details/',  this.Id , 'view', this.FromMenu]);
            }
            else
            {
              this.router.navigate(['/app/notis/notis-details/',  this.Id , 'edit']);
            }            
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }
  //close save function


  // open multiple upload
  deleteGambar(obj: any) {
    const item = this.imageList.at(obj.index).value;
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod yang telah hapus tidak akan dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        this.notisService.deleteImage(item.imageId)
          .subscribe(resultDelete => {
            this.showSpinner = false;
            if (resultDelete.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultDelete.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultDelete.ResponseMessage,
                'success'
              );
              obj.imageList.removeAt(obj.index);
              obj.imageList.updateValueAndValidity();
              obj.imageList.markAsDirty();
            }
          });
      }
    })
  }

  onUploadImageDialog(file: File) {
    const fileFormData = new FormData();
    fileFormData.append('images', file);
    this.files = file;
    this.notisService.uploadFile(fileFormData)
      .subscribe(res => {
        this.onUploadImage(res);
      });
  }
  private onUploadImage(uri: any) {
     const formGroup = this.createImageFormItems(uri);
    this.imageList.push(formGroup);
    this.imageList.updateValueAndValidity();
  }
  createImageFormItems(item: any): FormGroup {
    return this.formBuilder.group({
      imageURL: [item.imageURL, []],
      imageId: [item.imageId, []]
    }
    );
  }
  // close multiple upload

  //open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {

      const JenisNotis = formGroup.controls['JenisNotis'];
      const TarikhKesalahan = formGroup.controls['TarikhKesalahan'];
      const TarikhTamatKompaun = formGroup.controls['TarikhTamatKompaun'];
      const TarikhWujud = formGroup.controls['TarikhWujud'];
      const TarikhMahkamah = formGroup.controls['TarikhMahkamah'];
      const TarikhTamatCukai = formGroup.controls['TarikhTamatCukai'];
      const TarikhAduan = formGroup.controls['TarikhAduan'];
      const TarikhPeringatan = formGroup.controls['TarikhPeringatan'];
      const TarikhFailRujukan = formGroup.controls['TarikhFailRujukan'];
      const TarikhTamatLesen = formGroup.controls['TarikhTamatLesen'];
      const Handheld = formGroup.controls['Handheld'];
      const JenisBadan = formGroup.controls['JenisBadan'];
      const Penguatkuasa = formGroup.controls['Penguatkuasa'];
      const Kawasan = formGroup.controls['Kawasan'];
      const Bahagian = formGroup.controls['Bahagian'];
      const JenamaKenderaan = formGroup.controls['JenamaKenderaan'];
      const ModelKenderaan = formGroup.controls['ModelKenderaan'];
      const Seksyen = formGroup.controls['Seksyen'];
      const Peruntukan = formGroup.controls['Peruntukan'];
      const StatusKesalahan = formGroup.controls['StatusKesalahan'];
      const NegeriPesalah = formGroup.controls['NegeriPesalah'];
      const NegeriPemandu = formGroup.controls['NegeriPemandu'];
      const NoNotis = formGroup.controls['NoNotis'];
      const WaktuKesalahan = formGroup.controls['WaktuKesalahan'];
      const NoKenderaan = formGroup.controls['NoKenderaan'];
      const NoCukaiJalan = formGroup.controls['NoCukaiJalan'];
      const Nota = formGroup.controls['Nota'];
      const ButiranKesalahan = formGroup.controls['ButiranKesalahan'];
      const PeruntukanKesalahan = formGroup.controls['PeruntukanKesalahan'];
      const Tempat = formGroup.controls['Tempat'];
      const IdDde = formGroup.controls['IdDde'];
      const Petak = formGroup.controls['Petak'];
      const ButiranLokasi = formGroup.controls['ButiranLokasi'];
      const NoAduan = formGroup.controls['NoAduan'];
      const NoPeringatan = formGroup.controls['NoPeringatan'];
      const NoFailRujukan = formGroup.controls['NoFailRujukan'];
      const NamaPesalah = formGroup.controls['NamaPesalah'];
      const NoKPSyarikatPesalah = formGroup.controls['NoKPSyarikatPesalah'];
      const NoLesen = formGroup.controls['NoLesen'];
      const AlamatPesalah1 = formGroup.controls['AlamatPesalah1'];
      const AlamatPesalah2 = formGroup.controls['AlamatPesalah2'];
      const AlamatPesalah3 = formGroup.controls['AlamatPesalah3'];
      const PoskodPesalah = formGroup.controls['PoskodPesalah'];
      const NamaPemandu = formGroup.controls['NamaPemandu'];
      const NoKPPemandu = formGroup.controls['NoKPPemandu'];
      const AlamatPemandu1 = formGroup.controls['AlamatPemandu1'];
      const AlamatPemandu2 = formGroup.controls['AlamatPemandu2'];
      const AlamatPemandu3 = formGroup.controls['AlamatPemandu3'];
      const PoskodPemandu = formGroup.controls['PoskodPemandu'];
      const FlagSenaraiHitam = formGroup.controls['FlagSenaraiHitam'];
      const FlagPemerhatian = formGroup.controls['FlagPemerhatian'];
      const FlagSiasatan = formGroup.controls['FlagSiasatan'];
      const KadarKompaun = formGroup.controls['KadarKompaun'];
      const TarikhBayaran = formGroup.controls['TarikhBayaran'];
      const KadarBayaran = formGroup.controls['KadarBayaran'];
      const KadarRayuan = formGroup.controls['KadarRayuan'];
      const NoResit = formGroup.controls['NoResit'];
      const NoKes = formGroup.controls['NoKes'];
      const StatusMahkamah = formGroup.controls['StatusMahkamah'];
      const TarikhSebutan = formGroup.controls['TarikhSebutan'];
      const TarikhWaran = formGroup.controls['TarikhWaran'];
      const TarikhBicara = formGroup.controls['TarikhBicara'];

      const SebabBatal = formGroup.controls['SebabBatal'];
      const ButiranBatal = formGroup.controls['ButiranBatal'];
      const NamaPemohonBatal = formGroup.controls['NamaPemohonBatal'];
      const AlamatBatal1 = formGroup.controls['AlamatBatal1'];
      const PoskodBatal = formGroup.controls['PoskodBatal'];
      const NegeriBatal = formGroup.controls['NegeriBatal'];

      //maklumat batal
      if (this.FromMenu == 'batal') {
        if (SebabBatal) { SebabBatal.setErrors(null); if (!SebabBatal.value) { SebabBatal.setErrors({ required: true }); } else if (SebabBatal.value == 0) { SebabBatal.setErrors({ min: true }); } }
        //if (ButiranBatal) { ButiranBatal.setErrors(null); if (!ButiranBatal.value) { ButiranBatal.setErrors({ ButiranBatal: true }); } }
        //if (NamaPemohonBatal) { NamaPemohonBatal.setErrors(null); if (!NamaPemohonBatal.value) { NamaPemohonBatal.setErrors({ NamaPemohonBatal: true }); } }
        //if (AlamatBatal1) { AlamatBatal1.setErrors(null); if (!AlamatBatal1.value) { AlamatBatal1.setErrors({ AlamatBatal1: true }); } }
        if (PoskodBatal) {
          PoskodBatal.setErrors(null);
          //if (!PoskodPemandu.value) { PoskodPemandu.setErrors({ required: true }); } else 
          if (PoskodBatal.value != '' && PoskodBatal.value.length != 5) { PoskodBatal.setErrors({ minlength: true }); }
        }
        //if (NegeriBatal) { NegeriBatal.setErrors(null); if (!NegeriBatal.value) { NegeriBatal.setErrors({ required: true }); } else if (NegeriBatal.value == 0) { NegeriBatal.setErrors({ min: true }); } }
      }
     
      //Kesalahan Notis
      if (JenisNotis) { JenisNotis.setErrors(null); if (!JenisNotis.value) { JenisNotis.setErrors({ required: true }); } else if (JenisNotis.value == 0) { JenisNotis.setErrors({ min: true }); } }
      if (Seksyen) { Seksyen.setErrors(null); if (!Seksyen.value) { Seksyen.setErrors({ required: true }); } else if (Seksyen.value == 0) { Seksyen.setErrors({ min: true }); } }
      if (NoNotis) { NoNotis.setErrors(null); if (!NoNotis.value) { NoNotis.setErrors({ required: true }); } }
      if (PeruntukanKesalahan) { PeruntukanKesalahan.setErrors(null); if (!PeruntukanKesalahan.value) { PeruntukanKesalahan.setErrors({ required: true }); } }
      if (NoKenderaan) { NoKenderaan.setErrors(null); if (!NoKenderaan.value) { NoKenderaan.setErrors({ required: true }); } }
      if (ButiranKesalahan) { ButiranKesalahan.setErrors(null); if (!ButiranKesalahan.value) { ButiranKesalahan.setErrors({ required: true }); } }
      if (NoCukaiJalan) { NoCukaiJalan.setErrors(null); if (!NoCukaiJalan.value) { NoCukaiJalan.setErrors({ required: true }); } }
      if (TarikhTamatCukai) { TarikhTamatCukai.setErrors(null); if (!TarikhTamatCukai.value) { TarikhTamatCukai.setErrors({ required: true }); } }
      if (Penguatkuasa) { Penguatkuasa.setErrors(null); if (!Penguatkuasa.value) { Penguatkuasa.setErrors({ required: true }); } else if (Penguatkuasa.value == 0) { Penguatkuasa.setErrors({ min: true }); } }
      if (JenamaKenderaan) { JenamaKenderaan.setErrors(null); if (!JenamaKenderaan.value) { JenamaKenderaan.setErrors({ required: true }); } else if (JenamaKenderaan.value == 0) { JenamaKenderaan.setErrors({ min: true }); } }
      if (Bahagian) { Bahagian.setErrors(null); if (!Bahagian.value) { Bahagian.setErrors({ required: true }); } else if (Bahagian.value == 0) { Bahagian.setErrors({ min: true }); } }
      if (ModelKenderaan) { ModelKenderaan.setErrors(null); if (!ModelKenderaan.value) { ModelKenderaan.setErrors({ required: true }); } else if (ModelKenderaan.value == 0) { ModelKenderaan.setErrors({ min: true }); } }
      if (JenisBadan) { JenisBadan.setErrors(null); if (!JenisBadan.value) { JenisBadan.setErrors({ required: true }); } else if (JenisBadan.value == 0) { JenisBadan.setErrors({ min: true }); } }
      if (IdDde) { IdDde.setErrors(null); if (!IdDde.value) { IdDde.setErrors({ required: true }); } }
      if (Handheld) { Handheld.setErrors(null); if (!Handheld.value) { Handheld.setErrors({ required: true }); } else if (Handheld.value == 0) { Handheld.setErrors({ min: true }); } }
      if (Kawasan) { Kawasan.setErrors(null); if (!Kawasan.value) { Kawasan.setErrors({ required: true }); } else if (Kawasan.value == 0) { Kawasan.setErrors({ min: true }); } }
      if (Tempat) { Tempat.setErrors(null); if (!Tempat.value) { Tempat.setErrors({ required: true }); } }
      if (Petak) { Petak.setErrors(null); if (!Petak.value) { Petak.setErrors({ required: true }); } }
      if (TarikhWujud) { TarikhWujud.setErrors(null); if (!TarikhWujud.value) { TarikhWujud.setErrors({ required: true }); } else if (TarikhWujud.value && TarikhWujud.value > moment().toDate()) { TarikhWujud.setErrors({ exceed: true }); } }
      if (ButiranLokasi) { ButiranLokasi.setErrors(null); if (!ButiranLokasi.value) { ButiranLokasi.setErrors({ required: true }); } }
      if (TarikhTamatKompaun) { TarikhTamatKompaun.setErrors(null); if (!TarikhTamatKompaun.value) { TarikhTamatKompaun.setErrors({ required: true }); } }
      if (TarikhKesalahan) { TarikhKesalahan.setErrors(null); if (!TarikhKesalahan.value) { TarikhKesalahan.setErrors({ required: true }); } else if (TarikhKesalahan.value && TarikhKesalahan.value > moment().toDate()) { TarikhKesalahan.setErrors({ exceed: true }); } }
      if (WaktuKesalahan) { WaktuKesalahan.setErrors(null); if (!WaktuKesalahan.value) { WaktuKesalahan.setErrors({ required: true }); } }
      if (TarikhMahkamah) { TarikhMahkamah.setErrors(null); if (!TarikhMahkamah.value) { TarikhMahkamah.setErrors({ required: true }); } }
      if (Nota) { Nota.setErrors(null); if (!Nota.value) { Nota.setErrors({ required: true }); } }
      if (Peruntukan) { Peruntukan.setErrors(null); if (!Peruntukan.value) { Peruntukan.setErrors({ required: true }); } else if (Peruntukan.value == 0) { Peruntukan.setErrors({ min: true }); } }

      //maklumat pembayaran
      if (KadarKompaun) { KadarKompaun.setErrors(null); if (!KadarKompaun.value) { KadarKompaun.setErrors({ required: true }); } else if (KadarKompaun.value <= 0) { KadarKompaun.setErrors({ min: true }); } }
      if (TarikhBayaran) { TarikhBayaran.setErrors(null); if (!TarikhBayaran.value) { TarikhBayaran.setErrors({ required: true }); } }
      if (KadarBayaran) { KadarBayaran.setErrors(null); if (!KadarBayaran.value) { KadarBayaran.setErrors({ required: true }); } else if (KadarBayaran.value <= 0) { KadarBayaran.setErrors({ min: true }); } }
      if (KadarRayuan) { KadarRayuan.setErrors(null); if (KadarRayuan.value <= 0 && KadarRayuan.value != '') { KadarRayuan.setErrors({ min: true }); } }
      if (NoResit) { NoResit.setErrors(null); if (!NoResit.value) { NoResit.setErrors({ required: true }); } }

      //status kesalahan
      if(this.FromMenu == 'kemasukan')
      {
        if (StatusKesalahan) { StatusKesalahan.setErrors(null); if (!StatusKesalahan.value) { StatusKesalahan.setErrors({ required: true }); } else if (StatusKesalahan.value == 0) { StatusKesalahan.setErrors({ min: true }); } }
        if (NoAduan) { NoAduan.setErrors(null); if (!NoAduan.value) { NoAduan.setErrors({ required: true }); } }
        if (TarikhAduan) { TarikhAduan.setErrors(null); if (!TarikhAduan.value) { TarikhAduan.setErrors({ required: true }); } }
        if (NoPeringatan) { NoPeringatan.setErrors(null); if (!NoPeringatan.value) { NoPeringatan.setErrors({ required: true }); } }
        if (TarikhPeringatan) { TarikhPeringatan.setErrors(null); if (!TarikhPeringatan.value) { TarikhPeringatan.setErrors({ required: true }); } }
        if (NoFailRujukan) { NoFailRujukan.setErrors(null); if (!NoFailRujukan.value) { NoFailRujukan.setErrors({ required: true }); } }
        if (TarikhFailRujukan) { TarikhFailRujukan.setErrors(null); if (!TarikhFailRujukan.value) { TarikhFailRujukan.setErrors({ required: true }); } }
      }
      
      //maklumat mahkamah
      if(this.FromMenu != 'kemasukan')
      {
        if (NoKes) { NoKes.setErrors(null); if (!NoKes.value) { NoKes.setErrors({ required: true }); } }
        if (StatusMahkamah) { StatusMahkamah.setErrors(null); if (!StatusMahkamah.value) { StatusMahkamah.setErrors({ required: true }); } else if (StatusMahkamah.value == 0) { StatusMahkamah.setErrors({ min: true }); } }
        if (TarikhSebutan) { TarikhSebutan.setErrors(null); if (!TarikhSebutan.value) { TarikhSebutan.setErrors({ required: true }); } }
        if (TarikhWaran) { TarikhWaran.setErrors(null); if (!TarikhWaran.value) { TarikhWaran.setErrors({ required: true }); } }
        if (TarikhBicara) { TarikhBicara.setErrors(null); if (!TarikhBicara.value) { TarikhBicara.setErrors({ required: true }); } }
      }

      //maklumat pesalah
      if (NamaPesalah) { NamaPesalah.setErrors(null); if (!NamaPesalah.value) { NamaPesalah.setErrors({ required: true }); } }
      if (NoKPSyarikatPesalah) { NoKPSyarikatPesalah.setErrors(null); if (!NoKPSyarikatPesalah.value) { NoKPSyarikatPesalah.setErrors({ required: true }); } }
      if (NoLesen) { NoLesen.setErrors(null); if (!NoLesen.value) { NoLesen.setErrors({ required: true }); } }
      if (TarikhTamatLesen) { TarikhTamatLesen.setErrors(null); if (!TarikhTamatLesen.value) { TarikhTamatLesen.setErrors({ required: true }); } }
      if (AlamatPesalah1) { AlamatPesalah1.setErrors(null); if (!AlamatPesalah1.value) { AlamatPesalah1.setErrors({ required: true }); } }
      if (PoskodPesalah) { PoskodPesalah.setErrors(null); if (!PoskodPesalah.value) { PoskodPesalah.setErrors({ required: true }); } else if (PoskodPesalah.value.length != 5) { PoskodPesalah.setErrors({ minlength: true }); } }
      if (NegeriPesalah) { NegeriPesalah.setErrors(null); if (!NegeriPesalah.value) { NegeriPesalah.setErrors({ required: true }); } else if (NegeriPesalah.value == 0) { NegeriPesalah.setErrors({ min: true }); } }

      //maklumat pemandu
      if (NamaPemandu) { NamaPemandu.setErrors(null); if (!NamaPemandu.value) { NamaPemandu.setErrors({ required: true }); } }
      if (NoKPPemandu) { NoKPPemandu.setErrors(null); if (!NoKPPemandu.value) { NoKPPemandu.setErrors({ required: true }); } }
      if (AlamatPemandu1) { AlamatPemandu1.setErrors(null); if (!AlamatPemandu1.value) { AlamatPemandu1.setErrors({ required: true }); } }
      if (PoskodPemandu) { PoskodPemandu.setErrors(null); if (!PoskodPemandu.value) { PoskodPemandu.setErrors({ required: true }); } else if (PoskodPemandu.value.length != 5) { PoskodPemandu.setErrors({ minlength: true }); } }
      if (NegeriPemandu) { NegeriPemandu.setErrors(null); if (!NegeriPemandu.value) { NegeriPemandu.setErrors({ required: true }); } else if (NegeriPemandu.value == 0) { NegeriPemandu.setErrors({ min: true }); } }


    }
  }
  //open custom validation


  //open form setting 
  buildFormItems(): FormGroup {
    return this.formBuilder.group({

      SebabBatal: ['0', []],
      ButiranBatal: ['', []],
      NamaPemohonBatal: ['', []],
      AlamatBatal1: ['', []],
      AlamatBatal2: ['', []],
      AlamatBatal3: ['', []],
      PoskodBatal: ['', []],
      NegeriBatal: ['0', []],

      TarikhKesalahan: ['', []],
      TarikhWujud: [moment(), []],
      TarikhTamatKompaun: ['', []],
      NoNotis: ['', []],
      WaktuKesalahan: ['', []],
      TarikhMahkamah: ['', []],
      NoKenderaan: ['', []],     
      NoCukaiJalan: ['', []],
      Nota: ['', []],
      TarikhTamatCukai: ['', []],      
      ButiranKesalahan: ['', []],
      PeruntukanKesalahan: ['', []],
      Tempat: ['', []],
      IdDde: ['', []],
      Petak: ['', []],     
      ButiranLokasi: ['', []],
      Handheld: ['0', []],
      JenisBadan: ['0', []],
      Penguatkuasa: ['0', []],
      Kawasan: ['0', []],
      Bahagian: ['0', []],
      JenamaKenderaan: ['0', []],
      ModelKenderaan: ['0', []],
      Seksyen: ['0', []],
      Peruntukan: ['0', []],
      JenisNotis: ['0', []],

     
      NoAduan: ['', []],
      NoPeringatan: ['', []],
      TarikhAduan: ['', []],
      TarikhPeringatan: ['', []],
      NoFailRujukan: ['', []],
      TarikhFailRujukan: ['', []],
      StatusKesalahan: ['0', []],

      NamaPesalah: ['', []],
      NoKPSyarikatPesalah: ['', []],
      NoLesen: ['', []],
      TarikhTamatLesen: ['', []],
      AlamatPesalah1: ['', []],
      AlamatPesalah2: ['', []],
      AlamatPesalah3: ['', []],
      PoskodPesalah: ['', []],
      NegeriPesalah: ['0', []],
      FlagSenaraiHitam: ['', []],
      FlagPemerhatian: ['', []],
      FlagSiasatan: ['', []],

      KadarKompaun: ['', []],
      TarikhBayaran: ['', []],
      KadarBayaran: ['', []],
      KadarRayuan: ['', []],
      NoResit: ['', []],

      NoKes: ['', []],
      StatusMahkamah: ['0', []],
      TarikhSebutan: ['', []],
      TarikhWaran: ['', []],
      TarikhBicara: ['', []],

      NamaPemandu: ['', []],
      NoKPPemandu: ['', []],
      AlamatPemandu1: ['', []],
      AlamatPemandu2: ['', []],
      AlamatPemandu3: ['', []],
      PoskodPemandu: ['', []],
      NegeriPemandu: ['0', []],

      TarikhCipta: ['', []],
      DiciptaOleh: ['', []],
      TarikhKemaskini: ['', []],
      DikemaskiniOleh: ['', []],

      imageList: this.formBuilder.array([])

    }, { validator: this.customValidation() }
    );
  }
  //open form setting 

  
  // open get ref list 
  getJenisNotisList() {
    this.notisService.getJenisNotisList().subscribe(items => {
      this.JenisNotisList = items;
    });
  }
  getHandheldList() {
    this.notisService.getHandheldList().subscribe(items => {
      this.HandheldList = items;
    });
  }
  getJenisBadanList() {
    this.notisService.getJenisBadanList().subscribe(items => {
      this.JenisBadanList = items;
    });
  }   
  getKawasanList() {
    this.notisService.getKawasanList().subscribe(items => {
      this.KawasanList = items;
    });
  } 
  getBahagianList() {
    this.notisService.getBahagianList().subscribe(items => {
      this.BahagianList = items;
    });
  }    
  getStatusKesalahanList() {
    this.notisService.getStatusKesalahanList().subscribe(items => {
      this.StatusKesalahanList = items;
    });
  } 
  getStatusMahkamahList() {
    this.notisService.getStatusMahkamahList().subscribe(items => {
      this.StatusMahkamahList = items;
    });
  } 
  getSebabBatalList() {
    this.notisService.getSebabBatalList().subscribe(items => {
      this.SebabBatalList = items;
    });
  } 
  getNegeriList() {
    this.notisService.getNegeriList().subscribe(items => {
      this.NegeriList = items;
    });
  } 
  getPenguatkuasaList() {
    this.notisService.getPenguatkuasaList().subscribe(items => {
      this.PenguatkuasaList = items;
    });
  } 
  getJenamaKenderaanList() {
    this.notisService.getJenamaKenderaanList().subscribe(items => {
      this.JenamaKenderaanList = items;
    });
  } 
  getModelKenderaanList() {
    this.notisService.getModelKenderaanList(this.JenamaKenderaan).subscribe(items => {
      this.ModelKenderaanList = items;
    });
  } 
  getSeksyenList() {
    this.notisService.getSeksyenList().subscribe(items => {
      this.SeksyenList = items;
    });
  } 
  getPeruntukanList() {
    this.notisService.getPeruntukanList().subscribe(items => {
      this.PeruntukanList = items;
    });
  }
  // close get ref list 
}

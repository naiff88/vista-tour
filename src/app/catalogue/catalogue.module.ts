import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogueRoutingModule } from './catalogue-routing.module';


import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar} from '@angular/material/snack-bar';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule} from '@angular/material/dialog';
import { MatIconModule} from '@angular/material/icon';


import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { JoditAngularModule } from 'jodit-angular';
import { AppCommonModule } from '../shared/app-common.module';
import { QuillModule } from 'ngx-quill';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MiscModule } from '../misc/misc.module';
import { PMServiceProxy } from 'src/shared/service-proxies/service-proxies';
import { getQuillEditorSettings } from 'src/shared/helpers/quill-helper';
import { NgxPrintModule } from 'ngx-print';
// import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import { ProductsListComponent } from './products/list/list.component';
import { CategoryListComponent } from './category/list/list.component';
import { ProductDetailsComponent } from './products/product-details/product-details.component';
import { CategoryDetailsComponent } from './category/category-details/category-details.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { Variation1Component } from './products/modal/variation-1/variation-1.component';
import { Variation2Component } from './products/modal/variation-2/variation-2.component';



const materialModules = [
  MatButtonModule,
  MatTableModule,
  MatCheckboxModule,
  MatDialogModule,
  MatSortModule,
  MatTabsModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatProgressBarModule,
  MatIconModule,
  MatSnackBarModule,
  MAT_DIALOG_DATA,
  MatDialogRef
];

@NgModule({
  declarations: [
    ProductsListComponent,
    CategoryListComponent,
    ProductDetailsComponent,
    CategoryDetailsComponent,
    Variation1Component,
    Variation2Component
    // FinanceComponent,
    // DashboardComponent,
    // CustomerListComponent,
    // CustomerDetailsComponent,
    // CustomerInfoComponent,
    // QuotationListComponent,
    // QuotationDetailsComponent,
    // QuotationInfoComponent,
    // QuotationViewComponent,
    // QuotationPreviewComponent,
    // QuotationSendComponent,
    // QuotationItemComponent,
    // InvoiceListComponent,
    // InvoiceDetailsComponent,
    // InvoiceInfoComponent,
    // InvoiceViewComponent,
    // InvoiceNewItemComponent,
    // InvoicePreviewComponent,
    // InvoiceSendComponent,
    // InvoiceNewPaymentComponent,
    // InvoiceViewReceiptComponent
  ],
  imports: [
    DashboardModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppCommonModule,
    JoditAngularModule,
    CatalogueRoutingModule,
    QuillModule.forRoot(getQuillEditorSettings()),
    TableModule,
    PaginatorModule,
    ModalModule.forRoot(),
    MatDatepickerModule,
    MatMomentDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSortModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatIconModule,
    MatSnackBarModule,
    MiscModule,
    NgxPrintModule,
    MatRadioModule,
    PerfectScrollbarModule
  ],
  entryComponents: [
    Variation1Component,
    Variation2Component
    // CustomerDetailsComponent,
    // InvoiceNewItemComponent,
    // InvoicePreviewComponent,
    // InvoiceNewPaymentComponent,
    // InvoiceViewReceiptComponent
  ],
  // providers: [PMServiceProxy],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    { provide: PMServiceProxy }
  ]
})
export class CatalogueModule {
}

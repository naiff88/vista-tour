import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProductsListComponent } from './products/list/list.component';
import { CategoryListComponent } from './category/list/list.component';
import { MenuComponent } from '../dashboard/menu/menu.component';
import { ProductDetailsComponent } from './products/product-details/product-details.component';
import { CategoryDetailsComponent } from './category/category-details/category-details.component';

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MenuComponent,

                children: [
                    { path: '', pathMatch: 'full', redirectTo: 'products-list' },
                    { path: 'products-list', component: ProductsListComponent },
                    { path: 'category-list', component: CategoryListComponent },
                    { path: 'product-details/:id', component: ProductDetailsComponent },
                    { path: 'category-details/:id', component: CategoryDetailsComponent },
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})

export class CatalogueRoutingModule { }

/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
//import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';

// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
// import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
//import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import { CatalogueService } from '../../catalogue.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import {ProjectManagementUpsert as Project} from '@service-proxies';
import Swal from 'sweetalert2';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AppComponent } from '../../../app.component';
import { ProductImage } from '../../models';
import { Variation1Component } from '../modal/variation-1/variation-1.component';
import { Variation2Component } from '../modal/variation-2/variation-2.component';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})

export class ProductDetailsComponent implements OnInit {

  displayedColumns: string[] = ['select', 'projectName', 'manage'];
  // dataSource = new MatTableDataSource<invoiceList>([]);
  // selection = new SelectionModel<invoiceList>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  filterText = '';
  dataToPass: any = [];
  searchKey: string;
  productId: number;
  CourierId: number = 0;
  StatusId: number = 0;
  PaymentTypeId: number = 0;
  statusList: any = [];
  submitted = false;
  Category: any = [];
  variationsList: any = [];
  editField: string;

  variantColumn1: string;
  variantColumn2: string;

  // PERFECT SCROLL BAR
  public type: string = 'component';
  public disabled: boolean = false;
  public config: PerfectScrollbarConfigInterface = {};
  @ViewChild(PerfectScrollbarComponent, {static: true}) componentRef?: PerfectScrollbarComponent;

  files: File;
  form: FormGroup;

  get imageList(): FormArray {
    return this.form.get('imageList') as FormArray;
  }

  constructor(
    private service: CatalogueService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private snackBar: MatSnackBar,
    private router: Router,
    private app: AppComponent,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  // PERFECT SCROLL BAR
  public toggleDisabled(): void {
    this.disabled = !this.disabled;
  }
  public onScrollEvent(event: any): void {
    console.log(event);
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.productId = params.id;
      }
    });
    this.getStatusList();
    this.getCategory();
    this.getVariationList();
    this.loadDetails(this.productId);
    // this.getInvoice();
  }

  loadDetails(ProductId) {
    this.form = this.buildFormItems();
    // console.log('this.staffID ::::::::::::: ', this.staffId);
    if (ProductId > 0) {
      this.service.getproductDetails(ProductId)
        .subscribe(result => {
          // this.CourierId = result.CourierId > 0 ? result.CourierId : 0;
          this.StatusId = result.StatusId > 0 ? result.StatusId : 0;
          // this.PaymentTypeId = result.PaymentTypeId > 0 ? result.PaymentTypeId : 0;
          this.form.patchValue(result);
        });
    }
  }

  createImageFormItems(item: ProductImage): FormGroup {
    return this.formBuilder.group({
        imageURL: [item.imageURL, []],
        isDefault: [item.isDefault, []],
        productImageId: [item.productImageId, []]
      }
    );
  }

  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Warning',
        'Please make sure the form is complete!',
        'warning'
      );
    } else {
      let model = this.form.value;
      this.app.showOverlaySpinner(true);

      console.log('productmodel-->', model);
      console.log('VariationSListS-->', this.variationsList);

      this.service.saveProduct(model, this.variationsList, this.productId)
        .subscribe(r => {
          this.app.showOverlaySpinner(false);
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.router.navigate(['/app/catalogue/products-list']);
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }

  public isValidImageDefaultImaged(): boolean {
    return this.form.hasError('noDefaultImage');
  }

  getStatusList() {
    console.log('getStatusList ::::::::::::::::::::: ');
    this.service.getStatusList().subscribe(items => {
      console.log('getStatusList items ::::::::::::::::::::: ', items);
      this.statusList = items;
    });
  }

  getCategory() {
    console.log('getCategory ::::::::::::::::::::: ');
    this.service.getCategory().subscribe(items => {
      console.log('getCategory items ::::::::::::::::::::: ', items);
      this.Category = items;
    });
  }

  addVariation1(variationId?: number): void {
    console.log('variationId-->', variationId);
    const dialogRef = this.dialog.open(Variation1Component,
      {
        data: {
          variationId,
          productId: this.productId
        },
        autoFocus: false,
        minHeight: '520px',
        maxHeight: '90vh',
        maxWidth: '1000px',
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      this.loadDetails(this.productId);
    });
  }

  addVariation2(variationId?: number): void {
    console.log('variationId-->', variationId);
    const dialogRef = this.dialog.open(Variation2Component,
      {
        data: {
          variationId,
          productId: this.productId
        },
        autoFocus: false,
        minHeight: '520px',
        maxHeight: '90vh',
        maxWidth: '1000px',
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      this.loadDetails(this.productId);
    });
  }

  onUploadImageDialog(file: File) {
    const fileFormData = new FormData();
    fileFormData.append('images', file);
    this.files = file;
    this.service.uploadFile(fileFormData)
      .subscribe(uri => this.onUploadImage(uri));
  }

  deleteProductImage(index: number) {
    const item = this.imageList.at(index).value as ProductImage;
    const message = `Image deleted`;
    if (item.productImageId) {
      this.service.deleteImage(item.productImageId)
        .subscribe(() => this.snackBar.open(message, 'OK', { duration: 2000 }));
    } else {
      this.snackBar.open(message, 'OK', { duration: 2000 });
    }
  }

  private onUploadImage(uri: string) {
    const formGroup = this.createImageFormItems({ imageURL: uri, isDefault: false });
    this.imageList.push(formGroup);
    this.imageList.updateValueAndValidity();
  }

  updateList(id: number, property: string, event: any) {
    const editField = event.target.value;
    this.variationsList[id][property] = editField;
    console.log('updateList->', this.variationsList[id][property]);
    console.log('editField->', editField);
    console.log('this.variationList[id][property]->', this.variationsList[id][property] = editField);
  }

  changeValue(id: number, property: string, event: any) {
    this.editField = event.target.value;
    console.log(this.editField);
  }


  getVariationList(event?: LazyLoadEvent) {
    this.service.getVariationList(this.productId).subscribe(items => {
      console.log('getVariationList items ::::::::::::::::::::: ', items);
      console.log('getVariationList items ::::::::::::::::::::: ', items.length);

      const newVariationList = new Array();
      let captureIndex = -1;
      let captureVariation1Value = '';


      this.variantColumn1 = items && items.length > 0 ? items[0].variation1Name : 'Column 1';
      this.variantColumn2 = items && items.length > 0 ? items[0].variation2Name : 'Column 2';


      for (let i = 0; i < items.length; i++) {
        // if(captureIndex == -1)
        // {
        //   captureIndex = i;
        //   captureVariation1Value = items[i].variation1Value;
        // }
        let newObject = new Array();
        newObject = items[i];

        // var element = {rowSpan: 0, offTd: false};
        let boolOfTd = false;
        if (captureVariation1Value === items[i].variation1Value) {
          items[captureIndex].rowSpan++;
          boolOfTd = true;
        } else {
          captureIndex = i;
          captureVariation1Value = items[i].variation1Value;
        }

        newObject["rowSpan"] = 1;
        newObject["offTd"] = boolOfTd;
        console.log('newObject ::::::::::::::::::::: ', newObject);

        newVariationList.push(newObject);

       }
      console.log('getVariationList items NEW ::::::::::::::::::::: ', newVariationList);

      this.variationsList = newVariationList;
    });
  }


  buildFormItems(): FormGroup {
    return this.formBuilder.group({
        Name: ['', []],
        Description: ['', []],
        Sku: ['', []],
        Brand: ['', []],
        Material: ['', []],
        Price: ['', []],
        Stock: ['', []],
        Weight: ['', [Validators.required]],
        Width: ['', []],
        Length: ['', []],
        Depth: ['', []],
        StatusId: ['0', [Validators.required]],
        Category: ['', []],
        imageList: this.formBuilder.array([])
      }, {}
    );
  }

}

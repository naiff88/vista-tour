import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Variation1Component } from './variation-1.component';

describe('Variation1Component', () => {
  let component: Variation1Component;
  let fixture: ComponentFixture<Variation1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Variation1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Variation1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

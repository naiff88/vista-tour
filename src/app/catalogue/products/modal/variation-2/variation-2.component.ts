/* tslint:disable:max-line-length */
import {Component, OnInit, QueryList, ViewChildren, ViewChild, Input, Inject, ChangeDetectorRef} from '@angular/core';
// import {
//   MatTabGroup,
//   MatDialogRef,
//   MatTab,
//   MAT_DIALOG_DATA,
//   MatDialog,
//   MatSnackBar,
//   MatSnackBarConfig
// } from '@angular/material';

// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
// import { MatTableModule,MatTableDataSource } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
//import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {PrimengTableHelper} from '../../../../shared/PrimengTableHelper';
import {Table} from 'primeng/table';
import {Paginator} from 'primeng/paginator';
import {SelectionModel} from '@angular/cdk/collections';
// import {invoiceItemUpsert, pmUpsert, invoiceReceiptlist} from '../../../models';
import {CreateOrEditGeneralInfoDto} from '../../../../../shared/service-proxies/user-service-proxies';
import {Observable, of, Subscription} from 'rxjs';
import {CatalogueService} from '../../../catalogue.service';
import {ReferenceServiceProxy} from '../../../../../shared/service-proxies/reference-service-proxies';
import * as moment from 'moment';
import Swal from "sweetalert2";

@Component({
  selector: 'app-main',
  templateUrl: './variation-2.component.html',
  styleUrls: ['./variation-2.component.scss']
})

export class Variation2Component implements OnInit {

  @Input() productId: number;
  form: FormGroup;
  submitted = false;
  editField: string;
  Variation1Name = '';

  // updated = new EventEmitter<any>();
  displayedColumns: string[] = ['select', 'documentName', 'description'];
  // dataSource = new MatTableDataSource<pmUpsert>([]);
  // selection = new SelectionModel<pmUpsert>(true, []);
  ReceiptDate: any;
  primengTableHelper: PrimengTableHelper;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;
  generalInfo: CreateOrEditGeneralInfoDto = new CreateOrEditGeneralInfoDto();
  showErrorcheckDateCompared = false;
  private subscriptions: Subscription = new Subscription();

  constructor(
    private service: CatalogueService,
    public dialogRef: MatDialogRef<Variation2Component>,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private cdRef: ChangeDetectorRef,
    private _referenceService: ReferenceServiceProxy,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: {productId}
  ) {
    this.primengTableHelper = new PrimengTableHelper();

  }

  // START Editable Table
  variationList: Array<any> = [
    { id: 1, variation1Value: '',  price: '', stock: '', sku: '' },
    // { id: 1, name: 'x', age: 'xx', companyName: 'xxx', country: 'xxxx', city: 'xxxxx' },
  ];

  awaitingVariationList: Array<any> = [
    { id: 2, variation1Value: '',  price: '', stock: '', sku: '' },
    { id: 3, variation1Value: '',  price: '', stock: '', sku: '' },
    { id: 4, variation1Value: '',  price: '', stock: '', sku: '' },
    { id: 5, variation1Value: '',  price: '', stock: '', sku: '' },
    // { id: 5, variation1Name: this.VariationName, variation1Value: '' },
    // { id: 6, variation1Name: this.VariationName, variation1Value: '' },
    // { id: 7, variation1Name: this.VariationName, variation1Value: '' },
    // { id: 8, variation1Name: this.VariationName, variation1Value: '' },
    // { id: 9, variation1Name: this.VariationName, variation1Value: '' },
    // { id: 10, variation1Name: this.VariationName, variation1Value: '' },
    // { id: 2, name: '', age: '', companyName: '', country: '', city: '' },
  ];

  updateList(id: number, property: string, event: any) {
    const editField = event.target.value;
    this.variationList[id][property] = editField;
    console.log('updateList->', this.variationList[id][property]);
    // console.log('editField->', editField);
    // console.log('this.variationList[id][property]->', this.variationList[id][property] = editField);
  }

  remove(id: any) {
    this.awaitingVariationList.push(this.variationList[id]);
    this.variationList.splice(id, 1);
  }

  add() {
    if (this.awaitingVariationList.length > 0) {
      const variation = this.awaitingVariationList[0];
      this.variationList.push(variation);
      this.awaitingVariationList.splice(0, 1);
    }
  }

  changeValue(id: number, property: string, event: any) {
    // this.editField = event.target.textContent;
    this.editField = event.target.value;
    console.log('changeValue->', this.editField);
  }
  // END Editable Table

  ngOnInit() {
    this.loadDetails(this.productId);
  }

  loadDetails(ProductId) {
    this.form = this.buildFormItems();
    // console.log('this.staffID ::::::::::::: ', this.staffId);
  }

  /*  cancel(): void {
      window.history.back();
    }*/

  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Warning',
        'Please make sure the form is complete!',
        'warning'
      );
    } else {
      this.Variation1Name = this.form.value.VariationName;
      let model: any[];
      model = this.variationList;
      for (const i in this.variationList) {
          this.variationList[i].variation1Name = this.Variation1Name;
      }
      // console.log('this.VariationName----->', this.VariationName);
      console.log('variationList----->', this.variationList);
      // console.log('VariationFormValue----->', model);
      // this.app.showOverlaySpinner(true);
      // this.router.navigate(['/app/catalogue/products-list']);

      this.service.saveVariation2(model, this.productId)
        .subscribe(r => {
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.dialogRef.close();
            this.loadDetails(this.data.productId);
            // this.router.navigate(['/app/catalogue/products-list']);
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });


    }
  }

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      VariationName: ['', []],
      // Variation1Value: ({ // make a nested group
      //   variationList: ['', []],
      // }),
    });
  }

}

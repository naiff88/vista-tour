import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Variation2Component } from './variation-2.component';

describe('Variation1Component', () => {
  let component: Variation2Component;
  let fixture: ComponentFixture<Variation2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Variation2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Variation2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

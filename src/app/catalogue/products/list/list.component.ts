import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
//import { MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule } from '@angular/material/tabs';
// import { MatTableModule } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
// import { MatSnackBar} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
// import { MatDialogModule} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
import { MatDialog} from '@angular/material/dialog';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';
import { MatTableDataSource} from '@angular/material/table';

import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import { Router } from '@angular/router';
import { CatalogueService } from '../../catalogue.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import {ProjectManagementUpsert as Project} from '@service-proxies';
import {SalesService} from '../../../sales/sales.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ProductsListComponent implements OnInit {

  // @Input() orderStatus: string;
  @Input() orderTabIndex: number;


  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;

  filterText = '';
  dataToPass: any = [];

  constructor(
    private cataogueService: CatalogueService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private router: Router) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  ngOnInit() {
    // console.log('LIST orderStatus :::::::::::', this.orderStatus);
    // console.log('LIST orderTabIndex :::::::::::', this.orderTabIndex);
    this.getProductList();
  }

  productDetails(productId?: number): void {
    console.log('productDetails productId ::::::::::::::: ',  productId);
    this.router.navigate(['/app/catalogue/product-details', productId]);
  }

  getProductList(event?: LazyLoadEvent) {
    console.log('getProductList ::::::::::::::::::::: ');
    // tslint:disable-next-line:max-line-length
    this.cataogueService.getProductList(this.pageSetting.getPagerSetting(this.paginator, event, this.primengTableHelper.getSorting(this.dataTable)), this.orderTabIndex)
      .subscribe(items => {
        console.log('getProductList items ::::::::::::::::::::: ', items);
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
        this.primengTableHelper.hideLoadingIndicator();
      });
    this.primengTableHelper.showLoadingIndicator();
  }
}

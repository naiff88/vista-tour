import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
// import { MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

// import { MatRadioModule } from '@angular/material/radio';
// import { MatInputModule } from '@angular/material/input';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatTabsModule } from '@angular/material/tabs';
// import { MatTableModule } from '@angular/material/table';
// import { MatSortModule } from '@angular/material/sort';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// import { MatSelectModule } from '@angular/material/select';
// import { MatProgressBarModule } from '@angular/material/progress-bar';
// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatButtonModule} from '@angular/material/button';
// import { MatCheckboxModule} from '@angular/material/checkbox';
// import { MatSnackBar} from '@angular/material/snack-bar';
// import { MatSnackBarModule} from '@angular/material/snack-bar';
// import { MatDialogModule} from '@angular/material/dialog';
// import { MatIconModule} from '@angular/material/icon';
import { MatDialog} from '@angular/material/dialog';
// import { MatPaginator} from '@angular/material/paginator';
// import { MatSort} from '@angular/material/sort';
// import { MatTableDataSource} from '@angular/material/table';


import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from 'src/shared/confirmation-dialog/confirmation-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import { CatalogueService } from '../../catalogue.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import {ProjectManagementUpsert as Project} from '@service-proxies';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.scss']
})

export class CategoryDetailsComponent implements OnInit {

  displayedColumns: string[] = ['select', 'projectName', 'manage'];
  // dataSource = new MatTableDataSource<invoiceList>([]);
  // selection = new SelectionModel<invoiceList>(true, []);

  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', {static: true}) dataTable: Table;
  @ViewChild('paginator', {static: true}) paginator: Paginator;
  // editField: string;

  filterText = '';
  dataToPass: any = [];
  searchKey: string;
  StatusId = 0;
  statusList: any = [];
  categoryId: number;
  Description: '';
  parentCategoryList: any = [];
  ParentCategoryId = 0;
  submitted = false;

  form: FormGroup;

  constructor(
    private service: CatalogueService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private router: Router,
    private app: AppComponent,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  get f() { return this.form.controls; }

  // Editable Table
  // personList: Array<any> = [
  //   { id: 1, name: 'x', age: 'xx', companyName: 'xxx', country: 'xxxx', city: 'xxxxx' },
  // ];
  //
  // awaitingPersonList: Array<any> = [
  //   { id: 2, name: '', age: '', companyName: '', country: '', city: '' },
  // ];
  //
  // updateList(id: number, property: string, event: any) {
  //   const editField = event.target.textContent;
  //   this.personList[id][property] = editField;
  // }
  //
  // remove(id: any) {
  //   this.awaitingPersonList.push(this.personList[id]);
  //   this.personList.splice(id, 1);
  // }
  //
  // add() {
  //   if (this.awaitingPersonList.length > 0) {
  //     const person = this.awaitingPersonList[0];
  //     this.personList.push(person);
  //     this.awaitingPersonList.splice(0, 1);
  //   }
  // }
  //
  // changeValue(id: number, property: string, event: any) {
  //   this.editField = event.target.textContent;
  // }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.categoryId = params.id;
      }
    });
    this.getStatusList();
    this.getParentCategoryList();
    this.loadDetails(this.categoryId);
    // this.getInvoice();
  }

  loadDetails(CategoryId) {
    this.form = this.buildFormItems();
    // console.log('this.staffID ::::::::::::: ', this.staffId);
    if (CategoryId > 0) {
      this.service.getCategoryDetails(CategoryId)
        .subscribe(result => {
          // this.CourierId = result.CourierId > 0 ? result.CourierId : 0;
          this.StatusId = result.StatusId > 0 ? result.StatusId : 0;
          // this.PaymentTypeId = result.PaymentTypeId > 0 ? result.PaymentTypeId : 0;
          this.form.patchValue(result);
        });
    }
  }

  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Warning',
        'Please make sure the form is complete!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.app.showOverlaySpinner(true);



      this.service.saveCategory(model, this.categoryId)
        .subscribe(r => {
          this.app.showOverlaySpinner(false);
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.router.navigate(['/app/catalogue/category-list']);
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }

  getStatusList() {
    console.log('getStatusList ::::::::::::::::::::: ');
    this.service.getStatusList().subscribe(items => {
      console.log('getStatusList items ::::::::::::::::::::: ', items);
      this.statusList = items;
    });
  }

  getParentCategoryList() {
    console.log('getParentCategoryList ::::::::::::::::::::: ');
    this.service.getParentCategoryList().subscribe(items => {
      console.log('getParentCategoryList items ::::::::::::::::::::: ', items);
      this.parentCategoryList = items;
    });
  }

  buildFormItems(): FormGroup {
    return this.formBuilder.group({
        Name: ['', []],
        Description: ['', []],
        ParentCategoryId: ['0', []],
        StatusId: ['0', []],
      }, {}
    );
  }


  showAlert() {
    Swal.fire({
      title: 'Error!',
      text: 'Do you want to continue',
      icon: 'info',
      confirmButtonText: 'Cool'
    });
    // Swal({ title: 'Hi ;)' });
    // Swal({
    //     title: 'Multiple inputs',
    //     html:
    //     '<input id="swal-input1" class="swal2-input">' +
    //     '<input id="swal-input2" class="swal2-input">',
    //     focusConfirm: false,
    //     preConfirm: () => Promise.resolve([$('#swal-input1').val(), $('#swal-input2').val()])
    //     }).then(result => Swal(JSON.stringify(result)));
  }
}

/* tslint:disable:max-line-length */
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import { bufferTime, distinctUntilChanged, filter, map, scan, share, take, tap } from 'rxjs/operators';
import * as _ from 'lodash';
import { Pager, PageInfo } from '../../pagingnation';
import { API_BASE_URL } from 'src/shared/service-proxies/service-proxies';
import { ProgressBarState, Pair } from './models';

export enum CatalogueApiEndpoint {
  UploadImage = '/api/',
  DeleteImage = '/api/',
}

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {
  /**
   * Indicate data processing. Binded in dialog
   */
  private processingSubject = new Subject<ProgressBarState>();
  private baseUrl: string;
  http: HttpClient;


  /**
   *  Pipeline of async operation to handle loading state of product dialog
   */
  isBusyStream: Observable<boolean> = this.processingSubject.asObservable()
    .pipe(
      bufferTime(300),                  // buffers the progress Observable values for 100ms period
      filter(states => states.length > 0),   // ignore empty streams
      scan((total: number, states: ProgressBarState[]) => total + _.sum(states), 0),
      map((diff: number) => diff !== 0),       // diff between count of processing and already completed http requests
      distinctUntilChanged(),
      share()                                         // Returns a new Observable that multicasts (shares) the original Observable
    );

  set isBusy(value: boolean) {
    const state: number = value ? ProgressBarState.Show : ProgressBarState.Hide;
    this.processingSubject.next(state);
  }

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }


  ///////////////////////////////////////////////////
  //////////////// PRODUCTS SERVICE /////////////////
  ///////////////////////////////////////////////////



  getProductList(page: Pager, orderTabIndex: number): Observable<any> {
    console.log('getProductList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
      .set('RowsPerPage', (page.RowsPerPage || 0) + '')
      .set('PageNumber', (page.PageNumber || 0) + '')
      .set('OrderScript', page.OrderScript || '')
      .set('ColumnFilterScript', page.ColumnFilterScript || '')
      .set('orderStatusId', (orderTabIndex || 0) + '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { productId: 1, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Adidas Long Sleeve ', categories: 'T-Shirt', skuNo: '10515825', variation: 'XL', price: 300, stock: 14  },
      { productId: 2, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Adidas Fit Series ', categories: 'Shirt', skuNo: '181651', variation: 'M', price: 150, stock: 7  },
      { productId: 3, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Nike Air Sport ', categories: 'Shoes', skuNo: '877451', variation: '10', price: 450, stock: 5  },
      { productId: 4, productImage: 'https://toolakufiles.blob.core.windows.net/mytenantimage/noimage.jpg', productName: 'Tudung Bawal Bidang 56 ', categories: 'Tudung', skuNo: '24484', variation: '56', price: 20, stock: 34  },
    ];
    const pageInfo: any = { RowCount: 4 };
    return of({ list, pageInfo });

  }

  getproductDetails(ProducttId: number): Observable<any> {
    const params = new HttpParams().set('ProducttId', String(ProducttId));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      Name: 'Adidas Long Sleeve',
      OrderNo: '13313313',
      Address: '2477, Jalan Permata 19, Taman Permata, 53300 Kuala Lumpur. WP Kuala Lumpur',
      OrderDateTime: '02/01/2020 09:20 AM',
      Email: 'razman@gmail.com',
      PhoneNo: '60127381781',
      CourierId: 1,
      TrackingNo: '',
      ShippingCost: 3.5,
      StatusId: 2,
      PaymentTypeId: 3,
      TransactionNo: '57X78969NB701535K',
      PaymentDate: '20/01/2020 04.22 PM',
      Amount: 0,
    };
    return of(record);
  }

  saveProduct(model, variationsList, OrderId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Product Details Saved!';
    const Id = OrderId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  getCategory(): Observable<any> {
    console.log('getCategoryList ::::::::::::::::: ');

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'New Arrival' },
      { id: 2, name: 'On Sales' },
      { id: 3, name: 'Leanen For Men > Jubah' },
      { id: 4, name: 'Leanen For Women > Tudung' },
    ];

    return of(list);

  }

  getCategoryList(page: Pager, orderTabIndex: number): Observable<any> {
    console.log('getCategoryList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || '')
      .set('RowsPerPage', (page.RowsPerPage || 0) + '')
      .set('PageNumber', (page.PageNumber || 0) + '')
      .set('OrderScript', page.OrderScript || '')
      .set('ColumnFilterScript', page.ColumnFilterScript || '')
      .set('orderStatusId', (orderTabIndex || 0) + '')
    ;
    console.log('params ::::::::::::::::: ', params);

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { productId: 1, productName: 'Adidas Long Sleeve ', categories: 'T-Shirt', skuNo: '10515825', variation: 'XL', price: 300, stock: 14  },
      { productId: 2, productName: 'Adidas Fit Series ', categories: 'Shirt', skuNo: '181651', variation: 'M', price: 150, stock: 7  },
      { productId: 3, productName: 'Nike Air Sport ', categories: 'Shoes', skuNo: '877451', variation: '10', price: 450, stock: 5  },
      { productId: 4, productName: 'Tudung Bawal Bidang 56 ', categories: 'Tudung', skuNo: '24484', variation: '56', price: 20, stock: 34  },
    ];
    const pageInfo: any = { RowCount: 4 };
    return of({ list, pageInfo });

  }

  getCategoryDetails(CategoryId: number): Observable<any> {
    const params = new HttpParams().set('CategoryId', String(CategoryId));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    const record: any = {
      Name: 'Adidas Long Sleeve',
      OrderNo: '13313313',
      Address: '2477, Jalan Permata 19, Taman Permata, 53300 Kuala Lumpur. WP Kuala Lumpur',
      OrderDateTime: '02/01/2020 09:20 AM',
      Email: 'razman@gmail.com',
      PhoneNo: '60127381781',
      CourierId: 1,
      TrackingNo: '',
      ShippingCost: 3.5,
      StatusId: 2,
      PaymentTypeId: 3,
      TransactionNo: '57X78969NB701535K',
      PaymentDate: '20/01/2020 04.22 PM',
      Amount: 0,
    };
    return of(record);
  }

  saveCategory(model, OrderId: number): Observable<any> {

    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Category Details Saved!';
    const Id = OrderId;

    return of({ ReturnCode, ResponseMessage, Id});
  }


  getStatusList(): Observable<any> {
    console.log('getStatusList ::::::::::::::::: ');

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'New' },
      { id: 2, name: 'Processed' },
      { id: 3, name: 'Completed' },
    ];

    return of(list);

  }

  getParentCategoryList(): Observable<any> {
    console.log('getParentCategoryList ::::::::::::::::: ');

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, name: 'Parent Category 1' },
      { id: 2, name: 'Parent Category 2' },
      { id: 3, name: 'Parent Category 3' },
    ];

    return of(list);

  }

  getVariationList(productId: number): Observable<any> {
    console.log('getParentCategoryList ::::::::::::::::: ');

    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */

    // TEMPORARY HARDCODED

    const list: any = [
      { id: 1, variation1Name: 'Size', variation1Value: 'S',  variation2Name: 'Color', variation2Value: 'Red',  price: '70', stock: '20', sku: '0001' },
      { id: 2, variation1Name: 'Size', variation1Value: 'S',  variation2Name: 'Color', variation2Value: 'Blue',  price: '70', stock: '20', sku: '0001' },
      { id: 3, variation1Name: 'Size', variation1Value: 'S',  variation2Name: 'Color', variation2Value: 'Green',  price: '70', stock: '20', sku: '0001' },
      { id: 4, variation1Name: 'Size', variation1Value: 'M',  variation2Name: 'Color', variation2Value: 'Red',  price: '70', stock: '20', sku: '0001' },
      // { id: 5, variation1Name: 'Size', variation1Value: 'M',  variation2Name: 'Color', variation2Value: 'Blue',  price: '70', stock: '20', sku: '0001' },
      // { id: 6, variation1Name: 'Size', variation1Value: 'M',  variation2Name: 'Color', variation2Value: 'Green',  price: '70', stock: '20', sku: '0001' },
      { id: 7, variation1Name: 'Size', variation1Value: 'L',  variation2Name: 'Color', variation2Value: 'Red',  price: '70', stock: '20', sku: '0001' },
      { id: 8, variation1Name: 'Size', variation1Value: 'L',  variation2Name: 'Color', variation2Value: 'Blue',  price: '70', stock: '20', sku: '0001' },
      // { id: 9, variation1Name: 'Size', variation1Value: 'L',  variation2Name: 'Color', variation2Value: 'Green',  price: '70', stock: '20', sku: '0001' },
      { id: 10, variation1Name: 'Size', variation1Value: 'XL',  variation2Name: 'Color', variation2Value: 'Red',  price: '70', stock: '20', sku: '0001' },
      { id: 11, variation1Name: 'Size', variation1Value: 'XL',  variation2Name: 'Color', variation2Value: 'Blue',  price: '70', stock: '20', sku: '0001' },

    ];

    return of(list);

  }

  saveVariation1(model, ProductId: number): Observable<any> {
    console.log('model saveVariation1-->', model);
    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Variation One Saved!';
    const Id = ProductId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  saveVariation2(model, ProductId: number): Observable<any> {
    console.log('model saveVariation1-->', model);
    // CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */

    // HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Variation Two Saved!';
    const Id = ProductId;

    return of({ ReturnCode, ResponseMessage, Id});
  }

  uploadFile(data: FormData): Observable<any> {
    console.log('uploadFile:::', data);
    this.isBusy = true;
    return this.http.post<string>(this.baseUrl + CatalogueApiEndpoint.UploadImage, data)
      .pipe(
        // map((resp: any) => resp.Result[0].ImageFileName),
        map((resp: any) => {
          console.log('resp uploadFile ::::::::::::::: ', resp);
          const ImageFileName =  resp.Result[0].ImageFileName;
          /*const ReturnCode = resp.ReturnCode;
          const ResponseMessage = resp.ResponseMessage;
          const Id = productId;
          return { ReturnCode, ResponseMessage, Id };*/
          console.log('resp ImageFileName ::::::::::::::: ', ImageFileName);
          return ( ImageFileName );
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
  }

  deleteImage(imageId: number) {
    const params = new HttpParams().set('productImageId', String(imageId));

    this.isBusy = true;
    return this.http.delete(this.baseUrl + CatalogueApiEndpoint.DeleteImage, {params})
      .pipe(
        tap({complete: () => this.isBusy = false})
      );
  }


}

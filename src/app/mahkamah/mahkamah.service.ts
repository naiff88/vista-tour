/* tslint:disable:max-line-length comment-format */
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { bufferTime, distinctUntilChanged, filter, map, scan, share, take, tap } from 'rxjs/operators';
import * as _ from 'lodash';
import { Pager, PageInfo } from '../../pagingnation';
import { API_BASE_URL } from 'src/shared/service-proxies/service-proxies';
import { ProgressBarState, Pair } from './models';

export enum MahkamahApiEndpoint {
  //UploadImage = '/api/',
  //DeleteImage = '/api/',
}

@Injectable({
  providedIn: 'root'
})
export class MahkamahService {
  /**
   * Indicate data processing. Binded in dialog
   */
  private processingSubject = new Subject<ProgressBarState>();
  private baseUrl: string;
  http: HttpClient;


  /**
   *  Pipeline of async operation to handle loading state of product dialog
   */
  isBusyStream: Observable<boolean> = this.processingSubject.asObservable()
    .pipe(
      bufferTime(300),                  // buffers the progress Observable values for 100ms period
      filter(states => states.length > 0),   // ignore empty streams
      scan((total: number, states: ProgressBarState[]) => total + _.sum(states), 0),
      map((diff: number) => diff !== 0),       // diff between count of processing and already completed http requests
      distinctUntilChanged(),
      share()                                         // Returns a new Observable that multicasts (shares) the original Observable
    );

  set isBusy(value: boolean) {
    const state: number = value ? ProgressBarState.Show : ProgressBarState.Hide;
    this.processingSubject.next(state);
  }

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }












  // mahkamah -------------------------------------------------------------------------------------


  getMahkamahDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */

    //  getProductGeneral(id: number): Observable<ProductGeneral> {
    //   const params = new HttpParams().set('productId', String(id));
    //   this.isBusy = true;
    //   return this.http.get<ProductGeneral>(this.baseUrl + ProductApiEndpoint.General, {params})
    //     .pipe(
    //       map((resp: any) => {
    //         const model = resp.basicInfo[0] as ProductGeneral;
    //         model.imageList = resp.imageList as ProductImage[];
    //         return model;
    //       }),
    //       tap({complete: () => this.isBusy = false})
    //     );
    // }

    let record: any = {
      Id: '1',
      IdNotis: '1',
      NoNotis: 'D00001',
      TarikhNotis: '02 Jan 2020',
      NoRujukan: 'R00001',
      StatusBayaran: 1,
      Nama: 'Razman B. Md Zainal',
      NoKP: '840819075337',
      Pertuduhan: 'Catatan Pertuduhan',

      NoKes: 'DBR0001',
      TempatBicara: 'Mahkamah Kuala Lumpur',
      Keputusan: 'Didenda RM500',
      StatusMahkamah: 1,
      TarikhDaftar: '02 Jan 2020',
      TarikhSebutan: '06 Jan 2020',
      TarikhWaran: '22 Jan 2020',

      
      TarikhCipta: '02 Jan 2020',
      DiciptaOleh: 'Muhd Naiff',
      TarikhKemaskini: '03 Jan 2020',
      DikemaskiniOleh: 'Nik Fahmi',


    };
    return of(record);
  }

  saveMahkamah(model, Id: number): Observable<any> {
    console.log('saveMahkamah model ::::::::::::::: ', model);
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Mahkamah disimpan!';
    const Id2 = 1;
    return of({ ReturnCode, ResponseMessage, Id: Id2 });
  }

  deleteImage(imageId: number) {
    // const params = new HttpParams().set('productImageId', String(imageId));

    // this.isBusy = true;
    // return this.http.delete(this.baseUrl + ProductApiEndpoint.DeleteImage, {params})
    //   .pipe(
    //     tap({complete: () => this.isBusy = false})
    //   );
    const ReturnCode = 200;
    const ResponseMessage = 'Gambar telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  deleteMahkamah(ids: Array<{ id: number }>) {
    //console.log('deleteMahkamah ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod mahkamah telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getMahkamahList(page: Pager): Observable<any> {
    //console.log('getMahkamahOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    console.log('params getMahkamahList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { Id: 1, NoNotis: 'DC0001', Nama: 'Razman B. Md Zainal', NoKP: '840819075337', NoRujukan: '123', NoKes: 'DBR987', Status: 'Baru' },
      { Id: 2, NoNotis: 'DC0002', Nama: 'Muhd Naiff', NoKP: '8501190899882', NoRujukan: '678', NoKes: 'DBR111', Status: 'Selesai' },
    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }

















  // rujukan --------------------------------------------------------------------------------
  getStatusMahkamahList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Dalam Process' },
      { id: 2, name: 'Batal' },
      { id: 3, name: 'Tangguh' },
      { id: 4, name: 'Selesai' },
    ];
    return of(list);
  }






}




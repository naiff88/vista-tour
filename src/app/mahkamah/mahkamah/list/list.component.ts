import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { MahkamahService } from '../../mahkamah.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { ReportTemplete } from '../../../../report';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');
import * as $ from 'jquery';

@Component({
  selector: 'app-mahkamah-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class MahkamahListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  reportTemplete: ReportTemplete;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  eventSearch: any;
  filterValue: any;
  onSearch: boolean = false;
  submitted: boolean = false;
  reVisit: boolean = false;
  form: FormGroup;
  
  //close standard var for searching page

  // any unique name for searching list
  searchID = 'getMahkamahList';

  //initiate list
  statusMahkamahList: any = [];

  //initiate list model 
  StatusMahkamah: number = 0;

  //initiate date model 
  // TarikhMula: any;
  // TarikhAkhir: any;

  //report props
  reportCol: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'NoNotis', title: 'No. Notis', width: '15', align: 'left' },
    { name: 'Nama', title: 'Nama', width: '15', align: 'left' },
    { name: 'NoKP', title: 'No. KP', width: '15', align: 'left' },
    { name: 'NoRujukan', title: 'No. Rujukan Fail', width: '15', align: 'left' },
    { name: 'NoKes', title: 'No. Kes DBR', width: '15', align: 'left' },
    { name: 'StatusMahkamah', title: 'Status', width: '15', align: 'left' },
  ];
  reportTitle: string = 'Senarai Mahkamah';


  //standard contructor
  constructor(
    private mahkamahService: MahkamahService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
    this.reportTemplete = new ReportTemplete();
  }

  //form
  get f() { return this.form.controls; }


  //open standard function use for multiple delete in datatable
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.CustomerId + 1}`;
  }
  onRowSelect(event) {
    const evt = event.originalEvent as MouseEvent;
    evt.stopPropagation();
  }
  //close standard function use for multiple delete in datatable


  //init methode to call default function
  ngOnInit() {
    if (this.pageSetting.getSearchFilterValueSV(this.searchID)) {
      this.reVisit = true;
    }
    this.getMahkamahList();
    this.getStatusMahkamahList();
  }

  //open details page
  mahkamahDetails(id?: number, type?: string): void {
    this.router.navigate(['/app/mahkamah/mahkamah-details/', id, type]);
  } 

  //open standard listing with search function
  async getMahkamahList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    await this.reloadSearchForm();
    await this.mahkamahService.getMahkamahList(this.pageSetting.getPagerSettingWithSearch(this.paginator, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (this.onSearch) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    await this.primengTableHelper.showLoadingIndicator();
  }
  //close standard listing with search function

  //open default listing function  
  getStatusMahkamahList() {
    this.mahkamahService.getStatusMahkamahList().subscribe(items => {
      this.statusMahkamahList = items;
    });
  }
  //close default listing function


  //open reload search form
  reloadSearchForm() {
    this.filterValue = this.pageSetting.getSearchFilterValueSV(this.searchID);
    let SFcurrent = this.pageSetting.getSearchFilterValueSF(this.searchID);
    this.form = this.buildFormItems();
    if (SFcurrent) {
      //reasing value for list & date model
      this.StatusMahkamah = SFcurrent.StatusMahkamah > 0 ? SFcurrent.StatusMahkamah : 0;    
      // this.TarikhMula = SFcurrent.TarikhMula = moment(SFcurrent.TarikhMula) || '';
      // this.TarikhAkhir = SFcurrent.TarikhAkhir = moment(SFcurrent.TarikhAkhir) || '';
      this.form.patchValue(SFcurrent);
    } else {
      //initiate list & date model
      this.StatusMahkamah = 0;     
      // this.TarikhMula = '';
      // this.TarikhAkhir = '';
    }
  }
  //close reload search form


  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      //setting store procedure listing field for filtering
      this.filterValue = {
        " NoNotis ": { value: searchForm.NoNotis, matchMode: "contains" },
        " Nama ": { value: searchForm.Nama, matchMode: "contains" },
        " NoKP ": { value: searchForm.NoKP, matchMode: "contains" },
        " NoRujukan ": { value: searchForm.NoKP, matchMode: "contains" },
        " NoKes ": { value: searchForm.NoKes, matchMode: "contains" },
        " StatusMahkamah ": { value: searchForm.StatusMahkamah, matchMode: "equals" },
        // " TarikhMula ": { value: searchForm.TarikhMula, matchMode: "dateGreater" },
        // " TarikhAkhir ": { value: searchForm.TarikhAkhir, matchMode: "dateLower" },
      }
      this.pageSetting.setSearchFilterLS(this.filterValue, searchForm, this.searchID);
      this.onSearch = true;
      this.getMahkamahList();
      this.paginator.changePage(0);
    }
  }

  //reset search form
  reset() {
    this.pageSetting.removeSearchFilterLS(this.searchID);
    this.form = this.buildFormItems();

    //initiate list & date model
    this.StatusMahkamah= 0;
    // this.TarikhMula = '';
    // this.TarikhAkhir = '';

    this.filterValue = null;
    this.getMahkamahList();
    this.paginator.changePage(0);
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {      
      // const TarikhMula = formGroup.controls['TarikhMula'];
      // const TarikhAkhir = formGroup.controls['TarikhAkhir'];

     
      // if (TarikhMula) {
      //   TarikhMula.setErrors(null);
      //   if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
      //     TarikhMula.setErrors({ exceed: true });
      //   }
      // }
      // if (TarikhAkhir) {
      //   TarikhAkhir.setErrors(null);
      //   if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
      //     TarikhAkhir.setErrors({ exceed: true });
      //   }
      // }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      NoNotis: ['', []],
      Nama: ['', []],
      NoKP: ['', []],
      NoRujukan: ['', []],
      NoKes: ['', []],
      StatusMahkamah: ['0', []],
      // TarikhMula: ['', []],
      // TarikhAkhir: ['', []],     
    }, { validator: this.customValidation() }
    );
  }

  //excel genarator
  printTableExcel() {
    this.showSpinner = true;
    let list: any;
    this.mahkamahService.getMahkamahList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
      .subscribe(items => {
        list = items.list;
        this.pageSetting.excelGenerator(this.reportCol, list, this.reportTitle, this.reportTitle).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }

  //pdf genarator
  printTablePdf() {
    this.showSpinner = true;
    let list: any;
    this.mahkamahService.getMahkamahList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
      .subscribe(items => {
        list = items.list;
        this.pageSetting.pdfGenerator(this.reportCol, list, this.reportTitle, this.reportTitle, 'p', 7).then(loaded => {
          if (loaded) {
            this.showSpinner = false;
          }
        });
      });
  }

  //multiple print mahkamah function
  selectionPrint(type) {
    this.showSpinner = true;
    let selectedItems: Array<{ Id: number }>;
    let listSetData: any = [];
    if (type == 'Select_A4' || type == 'Select_4') {
      selectedItems = this.selection.selected.map(item => ({ Id: item.Id }));
    }
    else if (type == 'All_A4' || type == 'All_4') {
      this.mahkamahService.getMahkamahList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
        .subscribe(items => {
          selectedItems =  items.list.map(item => ({ Id: item.Id })); 
        });
    }
    selectedItems.map(item => { 
      this.mahkamahService.getMahkamahDetails(item.Id)
      .subscribe(result => {
        listSetData.push(result);
      });
    });
     this.reportTemplete.mahkamah(listSetData).then(loaded => {
      if (loaded) {
        this.showSpinner = false;
      }
    });;
  }


  //multiple delete function
  delete() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.Id }));
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod yang telah hapus tidak akan dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        this.mahkamahService.deleteMahkamah(selectedItems)
          .subscribe(resultDelete => {
            this.showSpinner = false;
            if (resultDelete.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultDelete.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultDelete.ResponseMessage,
                'success'
              );
            }
            this.getMahkamahList();
          });
      }
    })
  }

}

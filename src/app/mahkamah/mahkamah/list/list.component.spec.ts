import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MahkamahListComponent } from './list.component';

describe('MahkamahListComponent', () => {
  let component: MahkamahListComponent;
  let fixture: ComponentFixture<MahkamahListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MahkamahListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MahkamahListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

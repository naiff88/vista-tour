import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MahkamahDetailsComponent } from './mahkamah-details.component';

describe('MahkamahDetailsComponent', () => {
  let component: MahkamahDetailsComponent;
  let fixture: ComponentFixture<MahkamahDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MahkamahDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MahkamahDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

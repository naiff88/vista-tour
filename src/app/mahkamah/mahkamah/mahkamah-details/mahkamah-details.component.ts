/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MahkamahService } from '../../mahkamah.service';
import Swal from 'sweetalert2';
import * as _ from 'lodash';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { SelectionModel } from '@angular/cdk/collections';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { MatDialog } from '@angular/material/dialog';
import { PopupSearchNotisComponent } from '../../../popup-search/notis/list/list.component';

@Component({
  selector: 'app-mahkamah-details',
  templateUrl: './mahkamah-details.component.html',
  styleUrls: ['./mahkamah-details.component.scss']
})


export class MahkamahDetailsComponent implements OnInit {

  showSpinner: boolean = false;
  submitted = false;
  form: FormGroup;
  FormType: string;
  FromMenu: string = '';
  FormTitle: string = '';
  ViewMode: boolean = false;

  Id: number;
  //files: File;

  StatusBayaran: number = 0;
  StatusMahkamah: number = 0;

  IdNotis: string = ''; 
  NoNotis: string = '';  
  TarikhNotis: any = '';
  TarikhDaftar: any = '';
  TarikhSebutan: any = '';
  TarikhWaran: any = '';

  StatusMahkamahList: any = [];

  constructor(
    private mahkamahService: MahkamahService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private formBuilder: FormBuilder) {
  }

  get f() { return this.form.controls; }

  searchNotis() {
    event.stopPropagation();
    const data = {};
    const dialogRef = this.dialog.open(PopupSearchNotisComponent, { data });
    dialogRef.afterClosed()
      .subscribe(res => {
        if(res)
        {
          this.IdNotis = res.Id; 
          this.NoNotis = res.NoNotis; 
          this.TarikhNotis =  moment(res.TarikhNotis,'DD/MM/YYYY'); 
        }     
      }
      );
  }


  //open init function
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.FromMenu = params.fromMenu;
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;

        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }

      }
    });

    this.loadDetails(this.Id);

    //ref list call
    this.getStatusMahkamahList();

  }
  //close init function



  //open load form
  loadDetails(Id) {
    this.form = this.buildFormItems();

    if (Id > 0) {
      this.showSpinner = true;
      this.mahkamahService.getMahkamahDetails(Id)
        .subscribe(result => {
          this.showSpinner = false;

          this.NoNotis = result.NoNotis;
          this.IdNotis = result.IdNotis;
          this.TarikhNotis = result.TarikhNotis = moment(result.TarikhNotis) || '';
          this.TarikhDaftar = result.TarikhDaftar = moment(result.TarikhDaftar) || '';
          this.TarikhSebutan = result.TarikhSebutan = moment(result.TarikhSebutan) || '';
          this.TarikhWaran = result.TarikhWaran = moment(result.TarikhWaran) || '';

          this.StatusBayaran = result.StatusBayaran > 0 ? result.StatusBayaran : 0;
          this.StatusMahkamah = result.StatusMahkamah > 0 ? result.StatusMahkamah : 0;

          this.form.patchValue(result);
        });
    }
  }
  //close load form


  //open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod semasa akan ditetapkan semula!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  //open reset function

  //kembali
  kembali() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    if (this.FromMenu == "kemasukan") {
      this.router.navigate(['/app/mahkamah/mahkamah-details/0/add/kemasukan']);
    }
    else {
      this.router.navigate(['/app/mahkamah/senarai-mahkamah']);
    }
  }


  //open save function
  onSave(flagSave) {
    //console.log('this.form ::::::::::::::::::::::::;', this.form);
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat sah & lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.showSpinner = true;

      if (model.JenisKesalahan != '4') {
        model.LainKesalahan = '';
      }
      if (model.KeputusanMahkamah != '3') {
        model.NilaiPengurangan = '';
      }

      this.mahkamahService.saveMahkamah(model, this.Id)
        .subscribe(r => {
          this.showSpinner = false;
          if (r.ReturnCode === 200) {

            let ResponseMessage = r.ResponseMessage;

            Swal.fire(
              'Success',
              ResponseMessage,
              'success'
            );
            this.Id = r.Id;
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            this.router.onSameUrlNavigation = 'reload';

          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }
  //close save function

  //open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const IdNotis = formGroup.controls['IdNotis'];
      const NoNotis = formGroup.controls['NoNotis'];
      const TarikhNotis = formGroup.controls['TarikhNotis'];
      const NoRujukan = formGroup.controls['NoRujukan'];
      const StatusBayaran = formGroup.controls['StatusBayaran'];
      const Nama = formGroup.controls['Nama'];
      const NoKP = formGroup.controls['NoKP'];
      const Pertuduhan = formGroup.controls['Pertuduhan'];
      const NoKes = formGroup.controls['NoKes'];
      const TempatBicara = formGroup.controls['TempatBicara'];
      const Keputusan = formGroup.controls['Keputusan'];
      const StatusMahkamah = formGroup.controls['StatusMahkamah'];
      const TarikhDaftar = formGroup.controls['TarikhDaftar'];
      const TarikhSebutan = formGroup.controls['TarikhSebutan'];
      const TarikhWaran = formGroup.controls['TarikhWaran'];

      if (IdNotis) { IdNotis.setErrors(null); if (!IdNotis.value) { IdNotis.setErrors({ required: true }); } else if (IdNotis.value == 0) { IdNotis.setErrors({ min: true }); } }
      if (NoNotis) { NoNotis.setErrors(null); if (!NoNotis.value) { NoNotis.setErrors({ required: true }); } }
      if (TarikhNotis) { TarikhNotis.setErrors(null); if (!TarikhNotis.value) { TarikhNotis.setErrors({ required: true }); } }
      if (NoRujukan) { NoRujukan.setErrors(null); if (!NoRujukan.value) { NoRujukan.setErrors({ required: true }); } }
      if (StatusBayaran) { StatusBayaran.setErrors(null); if (!StatusBayaran.value) { StatusBayaran.setErrors({ required: true }); } else if (StatusBayaran.value == 0) { StatusBayaran.setErrors({ min: true }); } }
      if (Nama) { Nama.setErrors(null); if (!Nama.value) { Nama.setErrors({ required: true }); } }
      if (NoKP) { NoKP.setErrors(null); if (!NoKP.value) { NoKP.setErrors({ required: true }); } }
      if (Pertuduhan) { Pertuduhan.setErrors(null); if (!Pertuduhan.value) { Pertuduhan.setErrors({ required: true }); } }
      if (NoKes) { NoKes.setErrors(null); if (!NoKes.value) { NoKes.setErrors({ required: true }); } }
      if (TempatBicara) { TempatBicara.setErrors(null); if (!TempatBicara.value) { TempatBicara.setErrors({ required: true }); } }
      if (Keputusan) { Keputusan.setErrors(null); if (!Keputusan.value) { Keputusan.setErrors({ required: true }); } }
      if (StatusMahkamah) { StatusMahkamah.setErrors(null); if (!StatusMahkamah.value) { StatusMahkamah.setErrors({ required: true }); } else if (StatusMahkamah.value == 0) { StatusMahkamah.setErrors({ min: true }); } }
      if (TarikhDaftar) { TarikhDaftar.setErrors(null); if (!TarikhDaftar.value) { TarikhDaftar.setErrors({ required: true }); } }
      if (TarikhSebutan) { TarikhSebutan.setErrors(null); if (!TarikhSebutan.value) { TarikhSebutan.setErrors({ required: true }); } }
      if (TarikhWaran) { TarikhWaran.setErrors(null); if (!TarikhWaran.value) { TarikhWaran.setErrors({ required: true }); } }

      

    }
  }
  //open custom validation


  //open form setting 
  buildFormItems(): FormGroup {
    return this.formBuilder.group({

      IdNotis: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      NoNotis: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      TarikhNotis: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      NoRujukan: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      StatusBayaran: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      Nama: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      NoKP: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      Pertuduhan: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      NoKes: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      TempatBicara: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      Keputusan: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      StatusMahkamah: [{ value: '0', disabled: this.FormType == 'view' ? true : null }, []],
      TarikhDaftar: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      TarikhSebutan: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],
      TarikhWaran: [{ value: '', disabled: this.FormType == 'view' ? true : null }, []],




    }, { validator: this.customValidation() }
    );
  }
  //open form setting 





  // open get ref list 
  getStatusMahkamahList() {
    this.mahkamahService.getStatusMahkamahList().subscribe(items => {
      this.StatusMahkamahList = items;
    });
  }
  // close get ref list 
}

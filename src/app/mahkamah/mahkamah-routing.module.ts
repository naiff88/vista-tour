import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MenuComponent } from '../dashboard/menu/menu.component';
import { MahkamahListComponent } from './mahkamah/list/list.component';
import { MahkamahDetailsComponent } from './mahkamah/mahkamah-details/mahkamah-details.component';

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MenuComponent,

                children: [
                    { path: '', pathMatch: 'full', redirectTo: 'senarai-mahkamah' },  
                    { path: 'senarai-mahkamah', component: MahkamahListComponent },                   
                    { path: 'mahkamah-details/:id/:type', component: MahkamahDetailsComponent },
                    { path: 'mahkamah-details/:id/:type/:fromMenu', component: MahkamahDetailsComponent },
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})

export class MahkamahRoutingModule { }

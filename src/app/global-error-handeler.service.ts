import { Injectable, ErrorHandler } from '@angular/core';
import { toCamel } from 'src/shared/helpers/ServiceHelper';
//import { errorHandler } from '@angular/platform-browser/src/browser';
declare var swal: any;
@Injectable({
  providedIn: 'root'
})
export class GlobalErrorHandlerService implements ErrorHandler {
  constructor() {}
  handleError(error: any) {
    //debugger;
    if (error.status != 401) {
      console.error(error);
      if (error.error && error.error.message) {
        console.log('error', error.error);
        swal('Oops...', error.error.message[0], 'warning');
      } else {
        if (error.isSwaggerException) {
          const response = toCamel(JSON.parse(error.response));
          swal('Error', response && (response.responseMessage || response.message) || error.message, 'error');
        } else {
          swal('Error', error.message, 'error');
        }
      }
    }
  }
}

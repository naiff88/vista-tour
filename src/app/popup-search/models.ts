import { Type } from '@angular/compiler';

export interface ProductImage {
  imageURL: string;
  isDefault?: boolean;
  productImageId?: number;
}

// OTHER MODEL
export interface Pair {
  id: number;
  name: string;
}

export enum ProgressBarState {
  Hide = -1,
  Show = 1
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PopupSearchPenguatkuasaListComponent } from './list.component';

describe('PopupSearchPenguatkuasaListComponent', () => {
  let component: PopupSearchPenguatkuasaListComponent;
  let fixture: ComponentFixture<PopupSearchPenguatkuasaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupSearchPenguatkuasaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupSearchPenguatkuasaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

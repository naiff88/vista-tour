import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotisListComponent } from './list.component';

describe('NotisListComponent', () => {
  let component: NotisListComponent;
  let fixture: ComponentFixture<NotisListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotisListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

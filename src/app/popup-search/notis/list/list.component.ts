import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { NotisService } from '../../../notis/notis.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { ReportTemplete } from '../../../../report';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');
import * as $ from 'jquery';

@Component({
  selector: 'app-popup-serach-notis-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class PopupSearchNotisComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  reportTemplete: ReportTemplete;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  eventSearch: any;
  filterValue: any;
  onSearch: boolean = false;
  submitted: boolean = false;
  reVisit: boolean = false;
  form: FormGroup;

  //close standard var for searching page

  // any unique name for searching list
  searchID = 'getPopupSearchNotisList';

  //initiate list
  statusKesalahanList: any = [];
  handheldList: any = [];
  jenisNotisList: any = [];
  bahagianList: any = [];
  kawasanList: any = [];
  jenisBadanList: any = [];
  jalanList: any = [];

  //initiate list model 
  StatusKesalahan: number = 0;
  Handheld: number = 0;
  JenisNotis: number = 0;
  Bahagian: number = 0;
  Kawasan: number = 0;
  JenisBadan: number = 0;
  Jalan: number = 0;

  //initiate date model 
  TarikhMula: any;
  TarikhAkhir: any;

  //report props
  reportCol: any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'NoNotis', title: 'Notis', width: '15', align: 'left' },
    { name: 'Jenis', title: 'Jenis', width: '15', align: 'left' },
    { name: 'NoKenderaan', title: 'No. Kenderaan', width: '15', align: 'left' },
    { name: 'NamaPesalah', title: 'Nama / Syarikat', width: '15', align: 'left' },
    { name: 'NoPesalah', title: 'No. KP / Syarikat', width: '15', align: 'left' },
    { name: 'Pemandu', title: 'Pemandu', width: '15', align: 'left' },
    { name: 'KpPemandu', title: 'No. KP Pemandu', width: '15', align: 'left' },
    { name: 'TarikhKesalahan', title: 'Tarikh Kesalahan', width: '15', align: 'left' },
    { name: 'Perundangan', title: 'Peruntukan Undang - Undang', width: '15', align: 'left' },
    { name: 'Seksyen', title: 'Seksyen', width: '15', align: 'left' },
    { name: 'Tempat', title: 'Tempat', width: '15', align: 'left' },
    { name: 'Kompaun', title: 'Kompaun', width: '15', align: 'right' },
    { name: 'Status', title: 'Status', width: '15', align: 'left' },
    { name: 'NoResit', title: 'No. Resit', width: '15', align: 'left' },
    { name: 'TarikhBayaran', title: 'Tarikh Bayaran', width: '15', align: 'left' },
    { name: 'Bayaran', title: 'Bayaran', width: '15', align: 'right' },
  ];
  reportTitle: string = 'Senarai Notis';


  //standard contructor
  constructor(
    private notisService: NotisService,
    private snackBar: MatSnackBar,
    private router: Router,
    public dialogRef: MatDialogRef<PopupSearchNotisComponent>,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
    this.reportTemplete = new ReportTemplete();
  }

  //form
  get f() { return this.form.controls; }


  //open standard function use for multiple delete in datatable
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onSelectionChange(newItems) {
    this.selection.clear();
    //  newItems.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.CustomerId + 1}`;
  }
  onRowSelect(event) {
    const evt = event.originalEvent as MouseEvent;
    evt.stopPropagation();
  }
  //close standard function use for multiple delete in datatable


  //init methode to call default function
  ngOnInit() {
    if (this.pageSetting.getSearchFilterValueSV(this.searchID)) {
      this.reVisit = true;
    }
    this.getNotisList();
    this.getStatusKesalahanList();
    this.getHandheldList();
    this.getJenisNotisList();
    this.getBahagianList();
    this.getKawasanList();
    this.getJenisBadanList();
    this.getJalanList();
    //this.loadSearchDetails();

  }

  popupClose() {
    this.dialogRef.close(true);
  }

  // loadSearchDetails() {
  //   this.form = this.buildFormItems();
  // }

  //open standard listing with search function
  async getNotisList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    await this.reloadSearchForm();
    await this.notisService.getNotisList(this.pageSetting.getPagerSettingWithSearch(this.paginator, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (this.onSearch) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    await this.primengTableHelper.showLoadingIndicator();
  }
  //close standard listing with search function

  //open default listing function
  getJalanList() {
    this.notisService.getJalanList().subscribe(items => {
      this.jalanList = items;
    });
  }
  getJenisBadanList() {
    this.notisService.getJenisBadanList().subscribe(items => {
      this.jenisBadanList = items;
    });
  }
  getKawasanList() {
    this.notisService.getKawasanList().subscribe(items => {
      this.kawasanList = items;
    });
  }
  getBahagianList() {
    this.notisService.getBahagianList().subscribe(items => {
      this.bahagianList = items;
    });
  }
  getJenisNotisList() {
    this.notisService.getJenisNotisList().subscribe(items => {
      this.jenisNotisList = items;
    });
  }
  getHandheldList() {
    this.notisService.getHandheldList().subscribe(items => {
      this.handheldList = items;
    });
  }
  getStatusKesalahanList() {
    this.notisService.getStatusKesalahanList().subscribe(items => {
      this.statusKesalahanList = items;
    });
  }
  //close default listing function


  //open reload search form
  reloadSearchForm() {
    this.filterValue = this.pageSetting.getSearchFilterValueSV(this.searchID);
    let SFcurrent = this.pageSetting.getSearchFilterValueSF(this.searchID);
    this.form = this.buildFormItems();
    if (SFcurrent) {
      //reasing value for list & date model
      this.StatusKesalahan = SFcurrent.StatusKesalahan > 0 ? SFcurrent.StatusKesalahan : 0;
      this.Handheld = SFcurrent.Handheld > 0 ? SFcurrent.Handheld : 0;
      this.JenisNotis = SFcurrent.JenisNotis > 0 ? SFcurrent.JenisNotis : 0;
      this.Bahagian = SFcurrent.Bahagian > 0 ? SFcurrent.Bahagian : 0;
      this.Kawasan = SFcurrent.Kawasan > 0 ? SFcurrent.Kawasan : 0;
      this.JenisBadan = SFcurrent.JenisBadan > 0 ? SFcurrent.JenisBadan : 0;
      this.Jalan = SFcurrent.Jalan > 0 ? SFcurrent.Jalan : 0;
      this.TarikhMula = SFcurrent.TarikhMula = moment(SFcurrent.TarikhMula) || '';
      this.TarikhAkhir = SFcurrent.TarikhAkhir = moment(SFcurrent.TarikhAkhir) || '';
      this.form.patchValue(SFcurrent);
    } else {
      //initiate list & date model
      this.StatusKesalahan = 0;
      this.Handheld = 0;
      this.JenisNotis = 0;
      this.Bahagian = 0;
      this.Kawasan = 0;
      this.JenisBadan = 0;
      this.Jalan = 0;
      this.TarikhMula = '';
      this.TarikhAkhir = '';
    }
  }
  //close reload search form


  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      //setting store procedure listing field for filtering
      this.filterValue = {
        " NoKenderaan ": { value: searchForm.NoKenderaan, matchMode: "contains" },
        " NoCukaiJalan ": { value: searchForm.NoCukaiJalan, matchMode: "contains" },
        " NoNotis ": { value: searchForm.NoNotis, matchMode: "contains" },
        " NoLesen ": { value: searchForm.NoLesen, matchMode: "contains" },
        " NoPesalah ": { value: searchForm.NoPesalah, matchMode: "contains" },
        " NamaPesalah ": { value: searchForm.NamaPesalah, matchMode: "contains" },
        " NoGaji ": { value: searchForm.NoGaji, matchMode: "contains" },
        " StatusKesalahan ": { value: searchForm.StatusKesalahan, matchMode: "equals" },
        " Handheld ": { value: searchForm.Handheld, matchMode: "equals" },
        " JenisNotis ": { value: searchForm.JenisNotis, matchMode: "equals" },
        " Bahagian ": { value: searchForm.Bahagian, matchMode: "equals" },
        " Kawasan ": { value: searchForm.Kawasan, matchMode: "equals" },
        " JenisBadan ": { value: searchForm.JenisBadan, matchMode: "equals" },
        " Jalan ": { value: searchForm.Jalan, matchMode: "equals" },
        " TarikhMula ": { value: searchForm.TarikhMula, matchMode: "dateGreater" },
        " TarikhAkhir ": { value: searchForm.TarikhAkhir, matchMode: "dateLower" },
      }
      this.pageSetting.setSearchFilterLS(this.filterValue, searchForm, this.searchID);
      this.onSearch = true;
      this.getNotisList();
      this.paginator.changePage(0);
    }
  }

  //reset search form
  reset() {
    this.pageSetting.removeSearchFilterLS(this.searchID);
    this.form = this.buildFormItems();

    //initiate list & date model
    this.StatusKesalahan = 0;
    this.Handheld = 0;
    this.JenisNotis = 0;
    this.Bahagian = 0;
    this.Kawasan = 0;
    this.JenisBadan = 0;
    this.Jalan = 0;
    this.TarikhMula = '';
    this.TarikhAkhir = '';

    this.filterValue = null;
    this.getNotisList();
    this.paginator.changePage(0);
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      const TarikhMula = formGroup.controls['TarikhMula'];
      const TarikhAkhir = formGroup.controls['TarikhAkhir'];

      if (TarikhMula) {
        TarikhMula.setErrors(null);
        if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhMula.setErrors({ exceed: true });
        }
      }
      if (TarikhAkhir) {
        TarikhAkhir.setErrors(null);
        if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhAkhir.setErrors({ exceed: true });
        }
      }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      NoKenderaan: ['', []],
      NoCukaiJalan: ['', []],
      StatusKesalahan: ['0', []],
      NoNotis: ['', []],
      NoLesen: ['', []],
      Handheld: ['0', []],
      NoPesalah: ['', []],
      JenisNotis: ['0', []],
      Bahagian: ['0', []],
      NamaPesalah: ['', []],
      Kawasan: ['0', []],
      NoGaji: ['', []],
      JenisBadan: ['0', []],
      Jalan: ['0', []],
      TarikhMula: ['', []],
      TarikhAkhir: ['', []],
    }, { validator: this.customValidation() }
    );
  }


}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartRoutingModule } from './chart-routing.module';
import { NgxPrintModule } from 'ngx-print';
import { ImageCropperModule } from 'ngx-image-cropper';
import { MatRippleModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule, MatTab, MatTabGroup } from '@angular/material/tabs';
import { MatTableModule,MatTableDataSource } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';
import { MatIconModule} from '@angular/material/icon';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort} from '@angular/material/sort';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { JoditAngularModule } from 'jodit-angular';
import { AppCommonModule } from '../shared/app-common.module';
import { QuillModule } from 'ngx-quill';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { PieChartComponent } from './pie/pie.component';
import { BarChartComponent } from './bar/bar.component';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MiscModule } from '../misc/misc.module';
import { PMServiceProxy } from 'src/shared/service-proxies/service-proxies';
import { getQuillEditorSettings } from 'src/shared/helpers/quill-helper';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';



const materialModules = [
  MatButtonModule,
  MatTableModule,
  MatCheckboxModule,
  MatDialogModule,
  MatSortModule,
  MatTabsModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatProgressBarModule,
  MatIconModule,
  MatSnackBarModule,
  MatRadioModule,
  MatSlideToggleModule
];

@NgModule({
  declarations: [
    PieChartComponent,
    BarChartComponent,
  ],
  exports:[PieChartComponent,BarChartComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppCommonModule,
    JoditAngularModule,
    ChartRoutingModule,
    QuillModule.forRoot(getQuillEditorSettings()),
    TableModule,
    PaginatorModule,
    ModalModule.forRoot(),
    MatDatepickerModule,
    MatMomentDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSortModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatIconModule,
    MatSnackBarModule,
    MiscModule,
    NgxPrintModule,
    MatRadioModule,
    ImageCropperModule,
    MatCardModule,
    MatRippleModule,
    PerfectScrollbarModule,
  ],
  entryComponents: [],
  providers: [PMServiceProxy],
})
export class ChartModule {
}

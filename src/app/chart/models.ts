import { Type } from '@angular/compiler';

export enum ProgressBarState {
  Hide = -1,
  Show = 1
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenguatkuasaHandheldListComponent } from './list.component';

describe('PenguatkuasaHandheldListComponent', () => {
  let component: PenguatkuasaHandheldListComponent;
  let fixture: ComponentFixture<PenguatkuasaHandheldListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenguatkuasaHandheldListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenguatkuasaHandheldListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

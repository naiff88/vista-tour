import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { HandheldService } from '../../handheld.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');

@Component({
  selector: 'app-penguatkuasa-handheld-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class PenguatkuasaHandheldListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  eventSearch: any;
  filterValue: any;
  onSearch: boolean = false;
  submitted: boolean = false;  
  reVisit: boolean = false;
  form: FormGroup;
  //close standard var for searching page

  // any unique name for searching list
  searchID = 'getHandheldOfficerList'; 

  //initiate list
  statusList: any = [];
  pangkatList: any = [];
  jabatanList: any = [];

  //initiate list model 
  Status: number = 0; 
  Pangkat: number = 0;
  Jabatan: number = 0;
  
  //initiate date model 
  TarikhMula: any;
  TarikhAkhir: any;
  

  //standard contructor
  constructor(
    private handheldService: HandheldService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  //form
  get f() { return this.form.controls; }


  //open standard function use for multiple delete in datatable
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.CustomerId + 1}`;
  }
  onRowSelect(event) {
    const evt = event.originalEvent as MouseEvent;
    evt.stopPropagation();
  }
  //close standard function use for multiple delete in datatable


  //init methode to call default function
  ngOnInit() {
    if (this.pageSetting.getSearchFilterValueSV(this.searchID)) {
      this.reVisit = true;
    }
    this.getHandheldOfficerList();
    this.getStatusList();
    this.getPangkatList();
    this.getJabatanList();
    //this.loadSearchDetails();
  }

  // loadSearchDetails() {
  //   this.form = this.buildFormItems();
  // }

  //open details page
  handheldDetails(id?: number, type?: string): void {
    this.router.navigate(['/app/handheld/penguatkuasa-details/', id, type]);
  }

  //open standard listing with search function
  async getHandheldOfficerList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    await this.reloadSearchForm();
    await this.handheldService.getHandheldOfficerList(this.pageSetting.getPagerSettingWithSearch(this.paginator, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (this.onSearch) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    await this.primengTableHelper.showLoadingIndicator();
  }
  //close standard listing with search function

  //open default listing function
  getStatusList() {
    this.handheldService.getStatusList().subscribe(items => {
      this.statusList = items;
    });
  }
  getPangkatList() {
    this.handheldService.getPangkatList().subscribe(items => {
      this.pangkatList = items;
    });
  }
  getJabatanList() {
    this.handheldService.getJabatanList().subscribe(items => {
      this.jabatanList = items;
    });
  }
  //close default listing function


  //open reload search form
  reloadSearchForm() {
    this.filterValue = this.pageSetting.getSearchFilterValueSV(this.searchID);
    let SFcurrent = this.pageSetting.getSearchFilterValueSF(this.searchID);
    this.form = this.buildFormItems();
    if (SFcurrent) {
      //reasing value for list & date model
      this.Pangkat = SFcurrent.Pangkat > 0 ? SFcurrent.Pangkat : 0;
      this.Status = SFcurrent.Status > 0 ? SFcurrent.Status : 0;
      this.TarikhMula = SFcurrent.TarikhMula = moment(SFcurrent.TarikhMula) || '';
      this.TarikhAkhir = SFcurrent.TarikhAkhir = moment(SFcurrent.TarikhAkhir) || '';
      this.form.patchValue(SFcurrent);
    } else {
      //initiate list & date model
      this.Pangkat = 0;
      this.Status = 0;
      this.TarikhMula = '';
      this.TarikhAkhir = '';
    }
  }
   //close reload search form


  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      //setting store procedure listing field for filtering
      this.filterValue = {
        " Jabatan ": { value: searchForm.Jabatan, matchMode: "equals" },
        " NoGaji ": { value: searchForm.NoGaji, matchMode: "contains" },
        " Nama ": { value: searchForm.Nama, matchMode: "contains" },
        " NoBadan ": { value: searchForm.NoBadan, matchMode: "contains" },
        " Status ": { value: searchForm.Status, matchMode: "equals" },
        " Pangkat ": { value: searchForm.Pangkat, matchMode: "equals" },
        " Tarikh ": { value: searchForm.TarikhMula, matchMode: "dateGreater" },
        " Tarikh  ": { value: searchForm.TarikhAkhir, matchMode: "dateLower" },
      }
      this.pageSetting.setSearchFilterLS(this.filterValue, searchForm, this.searchID);
      this.onSearch = true;
      this.getHandheldOfficerList();
      this.paginator.changePage(0);
    }
  }

  //reset search form
  reset() {
    this.pageSetting.removeSearchFilterLS(this.searchID);
    this.form = this.buildFormItems();

    //initiate list & date model
    this.Jabatan = 0;
    this.Pangkat = 0;
    this.Status = 0;
    this.TarikhMula = '';
    this.TarikhAkhir = '';

    this.filterValue = null;
    this.getHandheldOfficerList();
    this.paginator.changePage(0);
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      const TarikhMula = formGroup.controls['TarikhMula'];
      const TarikhAkhir = formGroup.controls['TarikhAkhir'];

      if (TarikhMula) {
        TarikhMula.setErrors(null);
        if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhMula.setErrors({ exceed: true });
        }
      }
      if (TarikhAkhir) {
        TarikhAkhir.setErrors(null);
        if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhAkhir.setErrors({ exceed: true });
        }
      }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      Jabatan: ['0', []],
      NoGaji: ['', []],
      Nama: ['', []],
      NoBadan: ['', []],
      Status: ['0', []],
      Pangkat: ['0', []],
      TarikhMula: ['', []],
      TarikhAkhir: ['', []],
    }, { validator: this.customValidation() }
    );
  }
  
  //report props
  reportCol:any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'Jabatan', title: 'Jabatan', width: '15', align: 'left'  },
    { name: 'NoGaji', title: 'No. Gaji', width: '15', align: 'left' },
    { name: 'Nama', title: 'Nama', width: '15', align: 'left' },
    { name: 'Pangkat', title: 'Pangkat', width: '15', align: 'left' },
    { name: 'NoBadan', title: 'No. Badan', width: '15', align: 'left' },
    { name: 'Tarikh', title: 'Tarikh', width: '15', align: 'left' },
    { name: 'Status', title: 'Status', width: '15', align: 'left' },
  ];
  reportTitle:string = 'Senarai Penguatkuasa Handheld';

  //excel genarator
  printTableExcel()
  { 
    this.showSpinner = true;    
    let list: any;
    this.handheldService.getHandheldOfficerList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
    .subscribe(items => {
      list = items.list;        
      this.pageSetting.excelGenerator(this.reportCol, list, this.reportTitle, this.reportTitle).then(loaded=>{
        if(loaded)
        {
          this.showSpinner = false;
        }
      });
    });   
  } 

  //pdf genarator
  printTablePdf()
  { 
    this.showSpinner = true;    
    let list: any;
    this.handheldService.getHandheldOfficerList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
    .subscribe(items => {
      list = items.list;        
      this.pageSetting.pdfGenerator(this.reportCol, list, this.reportTitle, this.reportTitle, 'p', 7).then(loaded=>{
        if(loaded)
        {
          this.showSpinner = false;
        }
      });
    });   
  }

/*
  //excel genarator
  printTableExcel() {
    var workbook = new Excel.Workbook();
    // Tab Title
    var worksheet = workbook.addWorksheet('Senarai Penguatkuasa');
    // Columns to display
    const col = ['No.', 'Jabatan', 'No. Gaji', 'Nama', 'Pangkat', 'No. Badan', 'Tarikh', 'Status'];
    worksheet.mergeCells('A1', 'H1'); //title merge cell
    worksheet.getCell('A1').value = 'Laporan Penguatkuasa Handheld'; //title
    worksheet.getRow(2).values = col;
    // Column key
    worksheet.columns = [
      { key: 'No', width: 5 },
      { key: 'Jabatan', width: 15 },
      { key: 'NoGaji', width: 15 },
      { key: 'Nama', width: 15 },
      { key: 'Pangkat', width: 15 },
      { key: 'NoBadan', width: 15 },
      { key: 'Tarikh', width: 15 },
      { key: 'Status', width: 15 },
    ];

    let i = 1;
    this.showSpinner = true;
    //get list of data
    this.handheldService.getHandheldOfficerList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
    .subscribe(items => {
      this.showSpinner = false;
      let list =  items.list;
      for (var key in list) {
        worksheet.addRow([i, list[key].Jabatan, list[key].NoGaji, list[key].Nama, list[key].Pangkat, list[key].NoBadan, list[key].Tarikh, list[key].Status])
        i++;
      }     
    });    

    worksheet.eachRow(function (row, _rowNumber) {
      row.eachCell(function (cell, _colNumber) {
        cell.border = {
          top: { style: 'thin' },
          left: { style: 'thin' },
          bottom: { style: 'thin' },
          right: { style: 'thin' }
        };
      });
    });

    workbook.xlsx.writeBuffer().then(data => {
      const blob = new Blob([data], { type: this.blobType });
      //create excell file
      FileSaver.saveAs(blob, 'LaporanPenguatkuasaHandheld.xlsx');
    });
  }


  //pdf genarator
  printTablePdf() {
    var doc = new jsPDF('p'); //potrait setting
    // Columns to display
    var col = ["No.", "Jabatan", "No. Gaji", "Nama", "Pangkat", "No. Badan", "Tarikh", "Status"];
    var rows = [];
    let i = 1;    

    this.showSpinner = true;
    //get list of data
    this.handheldService.getHandheldOfficerList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
    .subscribe(items => {
      this.showSpinner = false;
      let list =  items.list;
      for (var key in list) {
        rows.push([i, list[key].Jabatan, list[key].NoGaji, list[key].Nama, list[key].Pangkat, list[key].NoBadan, list[key].Tarikh, list[key].Status])        
        i++;
      }     
    });

    //report title
    var header = function (data) {
      doc.setFontSize(14);
      doc.setFontStyle('normal');
      doc.text("Laporan Penguatkuasa Handheld", data.settings.margin.left, 10);
    };

    doc.autoTable(col, rows, { margin: { top: 15 }, beforePageContent: header });
    //create pdf file
    doc.save('LaporanPenguatkuasaHandheld.pdf');
  }
  */


  //multiple delete function
  delete() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.Id }));
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod yang telah hapus tidak akan dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        this.handheldService.deleteHandheldOfficer(selectedItems)
          .subscribe(resultDelete => {
            this.showSpinner = false;
            if (resultDelete.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultDelete.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultDelete.ResponseMessage,
                'success'
              );
            }
            this.getHandheldOfficerList();
          });
      }
    })
  }

}

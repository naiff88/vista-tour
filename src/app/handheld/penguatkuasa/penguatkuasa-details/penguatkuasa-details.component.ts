/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HandheldService } from '../../handheld.service';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-penguatkuasa-handheld-details',
  templateUrl: './penguatkuasa-details.component.html',
  styleUrls: ['./penguatkuasa-details.component.scss']
})

export class PenguatkuasaHandheldDetailsComponent implements OnInit {

  showSpinner: boolean = false;
  submitted = false;
  form: FormGroup;
  Id: number;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;
  Status: number = 0;
  statusList: any = [];
  Pangkat: number = 0;
  pangkatList: any = [];
  Jabatan: number = 1;
  jabatanList: any = [];

  constructor(
    private handheldService: HandheldService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
  }

  get f() { return this.form.controls; }

  //open init function
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
        else if (params.type == 'password') {
          this.FormTitle = 'Tukar Katalaluan';
        }
      }
    });
    //ref list call
    this.getStatusList();
    this.getPangkatList();
    this.getJabatanList();
    this.loadDetails(this.Id);
  }
  //close init function

  //open load form
  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.handheldService.getHandheldOfficerDetails(Id)
        .subscribe(result => {
          this.showSpinner = false;
          this.Jabatan = result.Jabatan > 0 ? result.Jabatan : 0;
          this.Status = result.Status > 0 ? result.Status : 0;
          this.Pangkat = result.Pangkat > 0 ? result.Pangkat : 0;
          this.form.patchValue(result);
        });
    }
  }
  //close load form


  //open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod semasa akan ditetapkan semula!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  //open reset function

  //open save function
  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat sah & lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.showSpinner = true;
      this.handheldService.saveHandheldOfficer(model, this.Id)
        .subscribe(r => {
          this.showSpinner = false;
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.Id = r.Id;
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }
  //close save function


  //open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const Jabatan = formGroup.controls['Jabatan'];
      const NoGaji = formGroup.controls['NoGaji'];
      const Nama = formGroup.controls['Nama'];
      const Pangkat = formGroup.controls['Pangkat'];
      const NoBadan = formGroup.controls['NoBadan'];
      const Status = formGroup.controls['Status'];
      const SahKataLaluan = formGroup.controls['SahKataLaluan'];
      const KataLaluan = formGroup.controls['KataLaluan'];

      if (Jabatan && this.FormType != 'password') {
        Jabatan.setErrors(null);
        if (!Jabatan.value) {
          Jabatan.setErrors({ required: true });
        }
        else if (Status.value == 0) {
          Jabatan.setErrors({ min: true });
        }
      }

      if (NoGaji && this.FormType != 'password') {
        NoGaji.setErrors(null);
        if (!NoGaji.value) {
          NoGaji.setErrors({ required: true });
        }
      }

      if (Nama) {
        Nama.setErrors(null);
        if (!Nama.value) {
          Nama.setErrors({ required: true });
        }
      }

      if (Pangkat && this.FormType != 'password') {
        Pangkat.setErrors(null);
        if (!Pangkat.value) {
          Pangkat.setErrors({ required: true });
        }
        else if (Pangkat.value == 0) {
          Pangkat.setErrors({ min: true });
        }
      }

      if (NoBadan && this.FormType != 'password') {
        NoBadan.setErrors(null);
        if (!NoBadan.value) {
          NoBadan.setErrors({ required: true });
        }
      }

      if (Status && this.FormType != 'password') {
        Status.setErrors(null);
        if (!Status.value) {
          Status.setErrors({ required: true });
        }
        else if (Status.value == 0) {
          Status.setErrors({ min: true });
        }
      }

      if (KataLaluan && (this.FormType == 'password' || this.FormType == 'add')) {
        KataLaluan.setErrors(null);
        if (!KataLaluan.value) {
          KataLaluan.setErrors({ required: true });
        }
        else if (KataLaluan.value
          && SahKataLaluan.value && SahKataLaluan.value !== KataLaluan.value
        ) {
          KataLaluan.setErrors({ notSame: true });
        }
      }

      if (SahKataLaluan && (this.FormType == 'password' || this.FormType == 'add')) {
        SahKataLaluan.setErrors(null);
        if (!SahKataLaluan.value) {
          SahKataLaluan.setErrors({ required: true });
        }
        else if (SahKataLaluan.value
          && KataLaluan.value && KataLaluan.value !== SahKataLaluan.value
        ) {
          SahKataLaluan.setErrors({ notSame: true });
        }
      }
    }
  }
  //open custom validation



  //open form setting 
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      Jabatan: ['0', []],
      NoGaji: ['', []],
      Nama: ['', []],
      Pangkat: ['0', []],
      NoBadan: ['', []],
      Status: ['0', []],
      Nota: ['', []],
      KataLaluan: ['', []],
      SahKataLaluan: ['', []],
      TarikhKemaskiniKatalaluan: ['', []],
      TarikhCipta: ['', []],
      DiciptaOleh: ['', []],
      TarikhKemaskini: ['', []],
      DikemaskiniOleh: ['', []],
    }, { validator: this.customValidation() }
    );
  }
  //open form setting 


  // open get ref list 
  getStatusList() {
    this.handheldService.getStatusList().subscribe(items => {
      this.statusList = items;
    });
  }
  getPangkatList() {
    this.handheldService.getPangkatList().subscribe(items => {
      this.pangkatList = items;
    });
  }
  getJabatanList() {
    this.handheldService.getJabatanList().subscribe(items => {
      this.jabatanList = items;
    });
  }
  // close get ref list 

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenguatkuasaHandheldDetailsComponent } from './penguatkuasa-details.component';

describe('PenguatkuasaHandheldDetailsComponent', () => {
  let component: PenguatkuasaHandheldDetailsComponent;
  let fixture: ComponentFixture<PenguatkuasaHandheldDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenguatkuasaHandheldDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenguatkuasaHandheldDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

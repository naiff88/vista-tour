import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HandheldRoutingModule } from './handheld-routing.module';

import { MatRippleModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSnackBar} from '@angular/material/snack-bar';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule} from '@angular/material/dialog';
import { MatIconModule} from '@angular/material/icon';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';



import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { JoditAngularModule } from 'jodit-angular';
import { AppCommonModule } from '../shared/app-common.module';
import { QuillModule } from 'ngx-quill';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MiscModule } from '../misc/misc.module';
import { PMServiceProxy } from 'src/shared/service-proxies/service-proxies';
import { getQuillEditorSettings } from 'src/shared/helpers/quill-helper';
import { NgxPrintModule } from 'ngx-print';
import { DashboardModule } from '../dashboard/dashboard.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PenguatkuasaHandheldListComponent } from './penguatkuasa/list/list.component';
import { PenguatkuasaHandheldDetailsComponent } from './penguatkuasa/penguatkuasa-details/penguatkuasa-details.component';
import { SerahanHandheldListComponent } from './serahan/list/list.component';
import { SerahanHandheldDetailsComponent } from './serahan/serahan-details/serahan-details.component';
import { HandheldListComponent } from './handheld/list/list.component';
import { HandheldDetailsComponent } from './handheld/handheld-details/handheld-details.component';
import { PengumumanHandheldListComponent } from './pengumuman/list/list.component';
import { PengumumanHandheldDetailsComponent } from './pengumuman/pengumuman-details/pengumuman-details.component';
import { PergerakanHandheldListComponent } from './pergerakan/list/list.component';
import { PergerakanHandheldDetailsComponent } from './pergerakan/pergerakan-details/pergerakan-details.component';
import { PatchHandheldListComponent } from './patch/list/list.component';
import { PatchHandheldDetailsComponent } from './patch/patch-details/patch-details.component';


const materialModules = [
  MatButtonModule,
  MatTableModule,
  MatCheckboxModule,
  MatDialogModule,
  MatSortModule,
  MatTabsModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatProgressBarModule,
  MatIconModule,
  MatSnackBarModule,
  MAT_DIALOG_DATA,
  MatDialogRef
];

@NgModule({
  declarations: [
    PenguatkuasaHandheldListComponent,
    PenguatkuasaHandheldDetailsComponent,   
    SerahanHandheldListComponent,
    SerahanHandheldDetailsComponent,  
    HandheldListComponent,
    HandheldDetailsComponent,    
    PengumumanHandheldListComponent,
    PengumumanHandheldDetailsComponent,
    PergerakanHandheldListComponent,
    PergerakanHandheldDetailsComponent,
    PatchHandheldListComponent,
    PatchHandheldDetailsComponent
  ],
  imports: [
    DashboardModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppCommonModule,
    JoditAngularModule,
    HandheldRoutingModule,
    QuillModule.forRoot(getQuillEditorSettings()),
    TableModule,
    PaginatorModule,
    ModalModule.forRoot(),
    MatDatepickerModule,
    MatMomentDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSortModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatIconModule,
    MatSnackBarModule,
    MiscModule,
    NgxPrintModule,
    MatRadioModule,
    PerfectScrollbarModule,
    NgMultiSelectDropDownModule,
    BsDropdownModule.forRoot(),
  ],
  entryComponents: [
    //PilihAhliComponent
    // Variation1Component,
    // Variation2Component
    // CustomerDetailsComponent,
    // InvoiceNewItemComponent,
    // InvoicePreviewComponent,
    // InvoiceNewPaymentComponent,
    // InvoiceViewReceiptComponent
  ],
  // providers: [PMServiceProxy],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    { provide: PMServiceProxy },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
  ]
})
export class HandheldModule {
}

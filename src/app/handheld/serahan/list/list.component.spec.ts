import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SerahanHandheldListComponent } from './list.component';

describe('SerahanHandheldListComponent', () => {
  let component: SerahanHandheldListComponent;
  let fixture: ComponentFixture<SerahanHandheldListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerahanHandheldListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SerahanHandheldListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SerahanHandheldDetailsComponent } from './serahan-details.component';

describe('SerahanHandheldDetailsComponent', () => {
  let component: SerahanHandheldDetailsComponent;
  let fixture: ComponentFixture<SerahanHandheldDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerahanHandheldDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SerahanHandheldDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

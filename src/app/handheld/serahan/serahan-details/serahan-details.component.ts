/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HandheldService } from '../../handheld.service';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-serahan-handheld-details',
  templateUrl: './serahan-details.component.html',
  styleUrls: ['./serahan-details.component.scss']
})

export class SerahanHandheldDetailsComponent implements OnInit {

  showSpinner: boolean = false;
  submitted = false;
  form: FormGroup; 
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;

  Id: number;
  StatusSebelum: number = 0;
  StatusSelepas: number = 0;
  StatusSerahan: number = 0;
  statusSebelumList: any = [];
  statusSelepasList: any = [];
  statusSerahanList: any = [];
  Pangkat: number = 0;
  pangkatList: any = [];

  constructor(
    private handheldService: HandheldService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
  }

  get f() { return this.form.controls; }

  //open init function
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
        // else if (params.type == 'password') {
        //   this.FormTitle = 'Tukar Katalaluan';
        // }
      }
    });
    //ref list call
    this.getStatusList();
    this.getPangkatList();
    this.loadDetails(this.Id);
  }
  //close init function

  //open load form
  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.handheldService.getSerahanHandheldDetails(Id)
        .subscribe(result => {
          this.showSpinner = false;
          this.StatusSebelum = result.StatusSebelum > 0 ? result.StatusSebelum : 0;
          this.StatusSelepas = result.StatusSelepas > 0 ? result.StatusSelepas : 0;
          this.StatusSerahan = result.StatusSerahan > 0 ? result.StatusSerahan : 0;
          this.Pangkat = result.Pangkat > 0 ? result.Pangkat : 0;
          this.form.patchValue(result);
        });
    }
  }
  //close load form


  //open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod semasa akan ditetapkan semula!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  //open reset function

  //open save function
  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat sah & lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.showSpinner = true;
      this.handheldService.saveSerahanHandheld(model, this.Id)
        .subscribe(r => {
          this.showSpinner = false;
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.Id = r.Id;
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }
  //close save function


  //open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const IdHandheld = formGroup.controls['IdHandheld'];
      const NoGaji = formGroup.controls['NoGaji'];
      const Nama = formGroup.controls['Nama'];
      const Pangkat = formGroup.controls['Pangkat'];
      const NoBadan = formGroup.controls['NoBadan'];
      const StatusSebelum = formGroup.controls['StatusSebelum'];
      const StatusSelepas = formGroup.controls['StatusSelepas'];
      const StatusSerahan = formGroup.controls['StatusSerahan'];

      if (IdHandheld) {
        IdHandheld.setErrors(null);
        if (!IdHandheld.value) {
          IdHandheld.setErrors({ required: true });
        }
      }

      if (NoGaji) {
        NoGaji.setErrors(null);
        if (!NoGaji.value) {
          NoGaji.setErrors({ required: true });
        }
      }

      if (Nama) {
        Nama.setErrors(null);
        if (!Nama.value) {
          Nama.setErrors({ required: true });
        }
      }

      if (Pangkat) {
        Pangkat.setErrors(null);
        if (!Pangkat.value) {
          Pangkat.setErrors({ required: true });
        }
        else if (Pangkat.value == 0) {
          Pangkat.setErrors({ min: true });
        }
      }

      if (NoBadan) {
        NoBadan.setErrors(null);
        if (!NoBadan.value) {
          NoBadan.setErrors({ required: true });
        }
      }

      if (StatusSebelum) {
        StatusSebelum.setErrors(null);
        if (!StatusSebelum.value) {
          StatusSebelum.setErrors({ required: true });
        }
        else if (StatusSebelum.value == 0) {
          StatusSebelum.setErrors({ min: true });
        }
      }

      if (StatusSelepas) {
        StatusSelepas.setErrors(null);
        if (!StatusSelepas.value) {
          StatusSelepas.setErrors({ required: true });
        }
        else if (StatusSelepas.value == 0) {
          StatusSelepas.setErrors({ min: true });
        }
      }

      if (StatusSerahan) {
        StatusSerahan.setErrors(null);
        if (!StatusSerahan.value) {
          StatusSerahan.setErrors({ required: true });
        }
        else if (StatusSerahan.value == 0) {
          StatusSerahan.setErrors({ min: true });
        }
      }
    }
  }
  //open custom validation



  //open form setting 
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      IdHandheld: ['', []],
      NoGaji: ['', []],
      Nama: ['', []],
      Pangkat: ['0', []],
      NoBadan: ['', []],
      StatusSebelum: ['0', []],
      StatusSelepas: ['0', []],
      StatusSerahan: ['0', []],
      Nota: ['', []],
      TarikhCipta: ['', []],
      DiciptaOleh: ['', []],
      TarikhKemaskini: ['', []],
      DikemaskiniOleh: ['', []],
    }, { validator: this.customValidation() }
    );
  }
  //open form setting 


  // open get ref list 
  getStatusList() {
    this.handheldService.getStatusList().subscribe(items => {
      this.statusSebelumList = items;
      this.statusSelepasList = items;
      this.statusSerahanList = items;
    });
  }
  getPangkatList() {
    this.handheldService.getPangkatList().subscribe(items => {
      this.pangkatList = items;
    });
  }
  // close get ref list 

}

/* tslint:disable:max-line-length comment-format */
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import { bufferTime, distinctUntilChanged, filter, map, scan, share, take, tap } from 'rxjs/operators';
import * as _ from 'lodash';
import { Pager, PageInfo } from '../../pagingnation';
import { API_BASE_URL } from 'src/shared/service-proxies/service-proxies';
import { ProgressBarState, Pair } from './models';

export enum HandheldApiEndpoint {
  //UploadImage = '/api/',
  //DeleteImage = '/api/',
}

@Injectable({
  providedIn: 'root'
})
export class HandheldService {
  /**
   * Indicate data processing. Binded in dialog
   */
  private processingSubject = new Subject<ProgressBarState>();
  private baseUrl: string;
  http: HttpClient;


  /**
   *  Pipeline of async operation to handle loading state of product dialog
   */
  isBusyStream: Observable<boolean> = this.processingSubject.asObservable()
    .pipe(
      bufferTime(300),                  // buffers the progress Observable values for 100ms period
      filter(states => states.length > 0),   // ignore empty streams
      scan((total: number, states: ProgressBarState[]) => total + _.sum(states), 0),
      map((diff: number) => diff !== 0),       // diff between count of processing and already completed http requests
      distinctUntilChanged(),
      share()                                         // Returns a new Observable that multicasts (shares) the original Observable
    );

  set isBusy(value: boolean) {
    const state: number = value ? ProgressBarState.Show : ProgressBarState.Hide;
    this.processingSubject.next(state);
  }

  constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }



  
  


  
  getHandheldOfficerDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    let record: any = {
      Jabatan: '1',
      NoGaji: '123',
      NoBadan: '456',
      Nama: 'Razman B. Md Zainal',
      Pangkat: 1,
      Status: 1,
      Nota: 'Contoh Nota',
      TarikhKemaskiniKatalaluan: '01 Jan 2020',
      TarikhCipta: '02 Jan 2020',
      DiciptaOleh: 'Muhd Naiff',
      TarikhKemaskini: '03 Jan 2020',
      DikemaskiniOleh: 'Nik Fahmi',
    };
    return of(record);
  }

  saveHandheldOfficer(model, Id: number): Observable<any> {
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Penguatkuasa disimpan!';
    const Id2 = 1;
    return of({ ReturnCode, ResponseMessage, Id:Id2 });
  }

  deleteHandheldOfficer(ids: Array<{ id: number }>) {
    console.log('deleteHandheldOfficer ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod penguatkuasa telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getHandheldOfficerList(page: Pager): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    console.log('params getHandheldOfficerList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { Id: 1, Jabatan: 'JPK', NoGaji: '123', Nama: 'Razman B. Md Zainal', Pangkat: 'Koperal', NoBadan: '456',  Tarikh: '01/01/2020', Status: 'Status'},
      { Id: 2, Jabatan: 'JPK', NoGaji: '567', Nama: 'Nik Fahmi Bin Ahmad', Pangkat: 'Koperal', NoBadan: '890',  Tarikh: '01/01/2020', Status: 'Status'},
    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }











  getSerahanHandheldDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    let record: any = {
      IdHandheld: '8892189',
      NoGaji: '123',
      NoBadan: '456',
      Nama: 'Razman B. Md Zainal',
      Pangkat: 1,
      StatusSebelum: 1,
      StatusSelepas: 2,
      StatusSerahan: 3,
      Nota: 'Contoh Nota',
      //TarikhKemaskiniKatalaluan: '01 Jan 2020',
      TarikhCipta: '02 Jan 2020',
      DiciptaOleh: 'Muhd Naiff',
      TarikhKemaskini: '03 Jan 2020',
      DikemaskiniOleh: 'Nik Fahmi',
    };
    return of(record);
  }

  saveSerahanHandheld(model, Id: number): Observable<any> {
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Serahan disimpan!';
    const Id2 = 1;
    return of({ ReturnCode, ResponseMessage, Id:Id2 });
  }

  deleteSerahanHandheld(ids: Array<{ id: number }>) {
    console.log('deleteSerahanHandheld ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod serahan telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getSerahanHandheldList(page: Pager): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    console.log('params getSerahanHandheldList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      //{ Id: 1, Jabatan: 'JPK', NoGaji: '123', Nama: 'Razman B. Md Zainal', Pangkat: 'Koperal', NoBadan: '456',  Tarikh: '01/01/2020', Status: 'Status'},
      //{ Id: 2, Jabatan: 'JPK', NoGaji: '567', Nama: 'Nik Fahmi Bin Ahmad', Pangkat: 'Koperal', NoBadan: '890',  Tarikh: '01/01/2020', Status: 'Status'},
      { Id: 1, IdHandheld: '1112223', NoGaji: '123', Nama: 'Razman B. Md Zainal', Pangkat: 'Koperal', NoBadan: '456',  TarikhMula: '01/01/2020', TarikhAkhir: '25/01/2020', StatusSebelum: 'Status 1', StatusSelepas: 'Status 1', StatusSerahan: 'Status 1'},
      { Id: 2, IdHandheld: '8989898', NoGaji: '777', Nama: 'Nik Fahmi Bin Ahmad', Pangkat: 'Koperal', NoBadan: '888',  TarikhMula: '01/01/2020', TarikhAkhir: '25/01/2020', StatusSebelum: 'Status 1', StatusSelepas: 'Status 2', StatusSerahan: 'Status 3'},
      { Id: 3, IdHandheld: '7651522', NoGaji: '890', Nama: 'Naiff Bin Ahmad', Pangkat: 'Koperal', NoBadan: '555',  TarikhMula: '01/01/2020', TarikhAkhir: '25/01/2020', StatusSebelum: 'Status 1', StatusSelepas: 'Status 1', StatusSerahan: 'Status 1'},
    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }










  getHandheldDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    let record: any = {
      Jabatan: '1',
      IdHandheld: '1232213',
      Nama: 'Nama Handheld 1',
      NoSiri: '9992102',
      Status: 1,
      JenamaHandheld: 1,
      JenamaPrinter: 1,
      PrinterMac: '00-11-AA-11-00-00',
      Versi: '1.2',
      Nota: 'Contoh Nota',
      TarikhKemaskiniKatalaluan: '01 Jan 2020',
      TarikhCipta: '02 Jan 2020',
      DiciptaOleh: 'Muhd Naiff',
      TarikhKemaskini: '03 Jan 2020',
      DikemaskiniOleh: 'Nik Fahmi',
    };
    return of(record);
  }

  saveHandheld(model, Id: number): Observable<any> {
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Penguatkuasa disimpan!';
    const Id2 = 1;
    return of({ ReturnCode, ResponseMessage, Id:Id2 });
  }

  deleteHandheld(ids: Array<{ id: number }>) {
    console.log('deleteHandheldOfficer ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod penguatkuasa telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getHandheldList(page: Pager): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    console.log('params getHandheldList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { Id: 1, Jabatan: 'JPK', IdHandheld: '998862', Nama: 'Nama Handheld 1', NoSiri: '123882', Status: 'Status 1', JenamaHandheld: 'Samsung', JenamaPrinter: 'Fuji Xerox', PrinterMac: '00-11-AA-11-00-00', Versi: '1.0', Nota: 'Nota Catatan'},
      { Id: 2, Jabatan: 'JPK', IdHandheld: '331311', Nama: 'Nama Handheld 2', NoSiri: '866888', Status: 'Status 2', JenamaHandheld: 'Samsung', JenamaPrinter: 'Fuji Xerox', PrinterMac: '00-11-AA-11-00-00', Versi: '2.3', Nota: 'Nota Catatan'},
      { Id: 3, Jabatan: 'JPK', IdHandheld: '799999', Nama: 'Nama Handheld 3', NoSiri: '776767', Status: 'Status 1', JenamaHandheld: 'Sony', JenamaPrinter: 'Fuji Xerox', PrinterMac: '00-11-AA-11-00-00', Versi: '1', Nota: 'Nota Catatan'},
    
    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }








  //refrence

  getPergerakanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Pergerakan 1' },
      { id: 2, name: 'Pergerakan 2' },
      { id: 3, name: 'Pergerakan 3' },
    ];
    return of(list);
  }

  getStatusList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Status 1' },
      { id: 2, name: 'Status 2' },
      { id: 3, name: 'Status 3' },
    ];
    return of(list);
  }

  getPangkatList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Pangkat 1' },
      { id: 2, name: 'Pangkat 2' },
      { id: 3, name: 'Pangkat 3' },
    ];
    return of(list);
  }


  getJabatanList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'JPK' },
      { id: 2, name: 'JKAS' },
      { id: 3, name: 'JPPP' },
    ];
    return of(list);
  }

  getJenamaHandheldList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Jenama Handheld 1' },
      { id: 2, name: 'Jenama Handheld 2' },
      { id: 3, name: 'Jenama Handheld 3' },
    ];
    return of(list);
  }

  getJenamaPrinterList(): Observable<any> {
    //console.log('getStateList ::::::::::::::::: ');
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.StatusList)
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          return list;
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED

    let list: any = [
      { id: 1, name: 'Jenama Printer 1' },
      { id: 2, name: 'Jenama Printer 2' },
      { id: 3, name: 'Jenama Printer 3' },
    ];
    return of(list);
  }









  getPengumumanHandheldDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    let record: any = {
      Jabatan: 1,
      Mesej: 'Pengumuman 1',
      TarikhMula: '01 Jan 2020',
      TarikhAkhir: '02 Jan 2020',
      //TarikhKemaskiniKatalaluan: '01 Jan 2020',
      TarikhCipta: '02 Jan 2020',
      DiciptaOleh: 'Muhd Naiff',
      TarikhKemaskini: '03 Jan 2020',
      DikemaskiniOleh: 'Nik Fahmi',
    };
    return of(record);
  }

  savePengumumanHandheld(model, Id: number): Observable<any> {
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Pengumuman disimpan!';
    const Id2 = 1;
    return of({ ReturnCode, ResponseMessage, Id:Id2 });
  }

  deletePengumumanHandheld(ids: Array<{ id: number }>) {
    console.log('deletePengumumanHandheld ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod pengumuman telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getPengumumanHandheldList(page: Pager): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    console.log('params getPengumumanHandheldList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      //{ Id: 1, Jabatan: 'JPK', NoGaji: '123', Nama: 'Razman B. Md Zainal', Pangkat: 'Koperal', NoBadan: '456',  Tarikh: '01/01/2020', Status: 'Status'},
      //{ Id: 2, Jabatan: 'JPK', NoGaji: '567', Nama: 'Nik Fahmi Bin Ahmad', Pangkat: 'Koperal', NoBadan: '890',  Tarikh: '01/01/2020', Status: 'Status'},
      { Id: 1, Jabatan: 'JPK', TarikhMula: '01/01/2020', TarikhAkhir: '25/01/2020', Mesej: 'Pengumuman 1', TarikhCipta: '01/01/2020'},
      { Id: 2, Jabatan: 'JKAS', TarikhMula: '01/01/2020', TarikhAkhir: '25/01/2020', Mesej: 'Pengumuman 3', TarikhCipta: '01/01/2020'},
      { Id: 1, Jabatan: 'JPPP', TarikhMula: '01/01/2020', TarikhAkhir: '25/01/2020', Mesej: 'Pengumuman 3', TarikhCipta: '01/01/2020'},
    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }







  getPergerakanHandheldDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    let record: any = {
      Pergerakan: 1,
      Status: 1,
      Nota: 'Nota Catatan',
      Daripada: 'Daripada 1',
      Kepada: 'Kepada 1',
      TarikhSerah: '01 Jan 2020',
      TarikhCipta: '02 Jan 2020',
      DiciptaOleh: 'Muhd Naiff',
      TarikhKemaskini: '03 Jan 2020',
      DikemaskiniOleh: 'Nik Fahmi',
    };
    return of(record);
  }

  savePergerakanHandheld(model, Id: number): Observable<any> {
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Pergerakan disimpan!';
    const Id2 = 1;
    return of({ ReturnCode, ResponseMessage, Id:Id2 });
  }

  deletePergerakanHandheld(ids: Array<{ id: number }>) {
    console.log('deletePergerakanHandheld ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod pergerakan telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getPergerakanHandheldList(page: Pager): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    console.log('params getPergerakanHandheldList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { Id: 1, IdHandheld: '9731868',  Jabatan: 'JPK', Pergerakan: 'Pergerakan 1', TarikhSerah: '01/01/2020', Daripada: 'Daripada 1', Kepada: 'Kepada 1', Status: 'Status 1', Nota: 'Nota Catatan'},
      { Id: 2, IdHandheld: '9977111',  Jabatan: 'JPK', Pergerakan: 'Pergerakan 2', TarikhSerah: '01/02/2020', Daripada: 'Daripada 2', Kepada: 'Kepada 2', Status: 'Status 2', Nota: 'Nota Catatan'},
    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }






  uploadFile(data: FormData): Observable<string> {
    // return this.http.post<string>(baseUrl + '/api/upload/doc', data)
    //   .pipe(
    //     map((resp: any) => {
    //       //console.log('uploadFile resp :::::::::::::::::::: ', resp);
    //       return resp.Result[0].DocFileName;
    //     })
    //   );
    var linkFile = 'https://file-examples.com/wp-content/uploads/2017/02/zip_2MB.zip';
    return  of(linkFile);
  }


  getPatchHandheldDetails(Id: number): Observable<any> {
    const params = new HttpParams().set('Id', String(Id));
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + HRApiEndpoint.EmployeeInfo, { params })
      .pipe(
        map((resp: any) => {
          const model = resp.titleInfo as any;
          return model;
        }),
        tap({ complete: () => this.isBusy = false })
      );
      */
    let record: any = {
      NamaFail: 'File Name 1',
      Status: 1,
      Versi: '1.1',
      Jenama: 'Jenama Patch 1',    
      Pakej: 'https://file-examples.com/wp-content/uploads/2017/02/zip_2MB.zip',//dummy
      TarikhCipta: '02 Jan 2020',
      DiciptaOleh: 'Muhd Naiff',
      TarikhKemaskini: '03 Jan 2020',
      DikemaskiniOleh: 'Nik Fahmi',
    };
    return of(record);
  }

  savePatchHandheld(model, Id: number): Observable<any> {
    console.log('savePatchHandheld model ::::::::::::::: ', model);
    //CALLING API
    /*
    this.isBusy = true;
    model.Id = OrderId;
      return this.http.put<void>(this.baseUrl + HRApiEndpoint.Employee, model)    
        .pipe(
          map((resp: any) => {
            //console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            const Id = staffId;            
            return { ReturnCode, ResponseMessage, Id };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
    */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Maklumat Patch disimpan!';
    const Id2 = 1;
    return of({ ReturnCode, ResponseMessage, Id:Id2 });
  }

  deletePatchHandheld(ids: Array<{ id: number }>) {
    console.log('deletePatchHandheld ids :::::::::::::::: ', ids);
    /*
    return this.http.put(this.baseUrl + HRApiEndpoint.EmployeeDelete, ids).pipe(
       map((resp: any) => {
         const ReturnCode = resp.ReturnCode;
         const ResponseMessage = resp.ResponseMessage;
      return { ReturnCode, ResponseMessage };
       }),
       tap({
         error: () => this.isBusy = false,
         complete: () => this.isBusy = false
       })
     );
     */
    //HARDCODED
    const ReturnCode = 200;
    const ResponseMessage = 'Rekod patch telah dihapuskan!';
    return of({ ReturnCode, ResponseMessage });
  }

  getPatchHandheldList(page: Pager): Observable<any> {
    //console.log('getHandheldOfficerList ::::::::::::::::: ');
    const params = new HttpParams().set('searchKey', page.GlobalFilter || "")
      .set('RowsPerPage', (page.RowsPerPage || 0) + "")
      .set('PageNumber', (page.PageNumber || 0) + "")
      .set('OrderScript', page.OrderScript || "")
      .set('ColumnFilterScript', page.ColumnFilterScript || "")
      ;
    console.log('params getPatchHandheldList ::::::::::::::::: ', params);
    /* API CALL */
    /*
    this.isBusy = true;
    return this.http.get<any>(this.baseUrl + SalesApiEndpoint.OrderList
      , { params }
      )
      .pipe(
        map((resp: any) => {
          //console.log('getEmployeeList resp :::::::::::::::::::: ',resp);
          const list = resp.list;
          const pageInfo = resp.pageInfo;
          return { list, pageInfo };
        }),
        tap({
          error: () => this.isBusy = false,
          complete: () => this.isBusy = false
        })
      );
    */
    // TEMPORARY HARDCODED
    let list: any = [
      { Id: 1, Versi: '3.2',  Jenama: 'Jenama Patch 1', NamaFail: 'Patch_1', Status: 'Status 1', TarikhCipta: '01/01/2020', Pakej: 'https://file-examples.com/wp-content/uploads/2017/02/zip_2MB.zip'},
      { Id: 2, Versi: '1.2',  Jenama: 'Jenama Patch 2', NamaFail: 'Patch_2', Status: 'Status 2', TarikhCipta: '02/01/2020', Pakej: 'https://file-examples.com/wp-content/uploads/2017/02/zip_2MB.zip'},
      { Id: 3, Versi: '1.0',  Jenama: 'Jenama Patch 3', NamaFail: 'Patch_3', Status: 'Status 3', TarikhCipta: '03/01/2020', Pakej: 'https://file-examples.com/wp-content/uploads/2017/02/zip_2MB.zip'},
      { Id: 4, Versi: '2.3',  Jenama: 'Jenama Patch 4', NamaFail: 'Patch_4', Status: 'Status 4', TarikhCipta: '04/01/2020', Pakej: 'https://file-examples.com/wp-content/uploads/2017/02/zip_2MB.zip'},
     
    ];
    let pageInfo: any = { RowCount: list.length }
    return of({ list, pageInfo });
  }



}




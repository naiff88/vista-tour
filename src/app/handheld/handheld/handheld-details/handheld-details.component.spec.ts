import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandheldDetailsComponent } from './handheld-details.component';

describe('HandheldDetailsComponent', () => {
  let component: HandheldDetailsComponent;
  let fixture: ComponentFixture<HandheldDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandheldDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandheldDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HandheldService } from '../../handheld.service';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-handheld-details',
  templateUrl: './handheld-details.component.html',
  styleUrls: ['./handheld-details.component.scss']
})

export class HandheldDetailsComponent implements OnInit {

  showSpinner: boolean = false;
  submitted = false;
  form: FormGroup;
  Id: number;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;
  
  
  Status: number = 0;
  statusList: any = [];

  Jabatan: number = 1;
  jabatanList: any = [];

  JenamaHandheld: number = 0;
  jenamaHandheldList: any = [];

  JenamaPrinter: number = 0;
  jenamaPrinterList: any = [];

  constructor(
    private handheldService: HandheldService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
  }

  get f() { return this.form.controls; }

  //open init function
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
        else if (params.type == 'gerak') {
          this.FormTitle = 'Gerak Alat';
        }
      }
    });
    //ref list call
    this.getStatusList();
    this.getJabatanList();
    this.getJenamaHandheldList();
    this.getJenamaPrinterList();
    this.loadDetails(this.Id);
  }
  //close init function

  //open load form
  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.handheldService.getHandheldDetails(Id)
        .subscribe(result => {
          this.showSpinner = false;
          this.Status = result.Status > 0 ? result.Status : 0;
          this.Jabatan = result.Jabatan > 0 ? result.Jabatan : 0;
          this.JenamaHandheld = result.JenamaHandheld > 0 ? result.JenamaHandheld : 0;
          this.JenamaPrinter = result.JenamaPrinter > 0 ? result.JenamaPrinter : 0;
          this.form.patchValue(result);
        });
    }
  }
  //close load form


  //open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod semasa akan ditetapkan semula!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  //open reset function

  //open save function
  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat sah & lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.showSpinner = true;
      this.handheldService.saveHandheld(model, this.Id)
        .subscribe(r => {
          this.showSpinner = false;
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.Id = r.Id;
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }
  //close save function


  //open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const Jabatan = formGroup.controls['Jabatan'];
      const IdHandheld = formGroup.controls['IdHandheld'];
      const Nama = formGroup.controls['Nama'];
      const NoSiri = formGroup.controls['NoSiri'];
      const Status = formGroup.controls['Status'];
      const JenamaHandheld = formGroup.controls['JenamaHandheld'];
      const JenamaPrinter = formGroup.controls['JenamaPrinter'];
      const PrinterMac = formGroup.controls['PrinterMac'];
      const Versi = formGroup.controls['Versi'];

      if (Jabatan) {
        Jabatan.setErrors(null);
        if (!Jabatan.value) {
          Jabatan.setErrors({ required: true });
        }
        else if (Status.value == 0) {
          Status.setErrors({ min: true });
        }
      }

      if (IdHandheld) {
        IdHandheld.setErrors(null);
        if (!IdHandheld.value) {
          IdHandheld.setErrors({ required: true });
        }
      }

      if (Nama) {
        Nama.setErrors(null);
        if (!Nama.value) {
          Nama.setErrors({ required: true });
        }
      }

      if (NoSiri) {
        NoSiri.setErrors(null);
        if (!NoSiri.value) {
          NoSiri.setErrors({ required: true });
        }
      }

      if (Status && this.FormType != 'gerak') {
        Status.setErrors(null);
        if (!Status.value) {
          Status.setErrors({ required: true });
        }
        else if (Status.value == 0) {
          Status.setErrors({ min: true });
        }
      } 

      if (JenamaHandheld && this.FormType != 'gerak') {
        JenamaHandheld.setErrors(null);
        if (!JenamaHandheld.value) {
          JenamaHandheld.setErrors({ required: true });
        }
        else if (JenamaHandheld.value == 0) {
          JenamaHandheld.setErrors({ min: true });
        }
      } 

      // if (JenamaPrinter && this.FormType != 'gerak') {
      //   JenamaPrinter.setErrors(null);
      //   if (!JenamaPrinter.value) {
      //     JenamaPrinter.setErrors({ required: true });
      //   }
      //   else if (JenamaPrinter.value == 0) {
      //     JenamaPrinter.setErrors({ min: true });
      //   }
      // }       

      // if (PrinterMac && this.FormType != 'gerak') {
      //   PrinterMac.setErrors(null);
      //   if (!PrinterMac.value) {
      //     PrinterMac.setErrors({ required: true });
      //   }
      // }

      // if (Versi && this.FormType != 'gerak') {
      //   Versi.setErrors(null);
      //   if (!Versi.value) {
      //     Versi.setErrors({ required: true });
      //   }
      // }          
    }
  }
  //open custom validation



  //open form setting 
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      Jabatan: ['0', []],
      IdHandheld: ['', []],
      NoSiri: ['', []],
      Nama: ['', []],
      Status: ['0', []],
      Nota: ['', []],
      JenamaHandheld: ['0', []],
      JenamaPrinter: ['0', []],
      PrinterMac: ['', []],
      Versi: ['', []],     
      TarikhCipta: ['', []],
      DiciptaOleh: ['', []],
      TarikhKemaskini: ['', []],
      DikemaskiniOleh: ['', []],
    }, { validator: this.customValidation() }
    );
  }
  //open form setting 


  // open get ref list 
  getStatusList() {
    this.handheldService.getStatusList().subscribe(items => {
      this.statusList = items;
    });
  }
  getJabatanList() {
    this.handheldService.getJabatanList().subscribe(items => {
      this.jabatanList = items;
    });
  }
  getJenamaPrinterList() {
    this.handheldService.getJenamaPrinterList().subscribe(items => {
      this.jenamaPrinterList = items;
    });
  }
  getJenamaHandheldList() {
    this.handheldService.getJenamaHandheldList().subscribe(items => {
      this.jenamaHandheldList = items;
    });
  }
  // close get ref list 

}

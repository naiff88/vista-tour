import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandheldListComponent } from './list.component';

describe('HandheldListComponent', () => {
  let component: HandheldListComponent;
  let fixture: ComponentFixture<HandheldListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandheldListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandheldListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

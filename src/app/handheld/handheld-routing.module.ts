import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MenuComponent } from '../dashboard/menu/menu.component';
import { PenguatkuasaHandheldListComponent } from './penguatkuasa/list/list.component';
import { PenguatkuasaHandheldDetailsComponent } from './penguatkuasa/penguatkuasa-details/penguatkuasa-details.component';
import { SerahanHandheldListComponent } from './serahan/list/list.component';
import { SerahanHandheldDetailsComponent } from './serahan/serahan-details/serahan-details.component';
import { HandheldListComponent } from './handheld/list/list.component';
import { HandheldDetailsComponent } from './handheld/handheld-details/handheld-details.component';
import { PengumumanHandheldListComponent } from './pengumuman/list/list.component';
import { PengumumanHandheldDetailsComponent } from './pengumuman/pengumuman-details/pengumuman-details.component';
import { PergerakanHandheldListComponent } from './pergerakan/list/list.component';
import { PergerakanHandheldDetailsComponent } from './pergerakan/pergerakan-details/pergerakan-details.component';
import { PatchHandheldListComponent } from './patch/list/list.component';
import { PatchHandheldDetailsComponent } from './patch/patch-details/patch-details.component';

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MenuComponent,

                children: [
                    { path: '', pathMatch: 'full', redirectTo: 'penguatkuasa-handheld' },                    
                    { path: 'senarai-penguatkuasa-handheld', component: PenguatkuasaHandheldListComponent },
                    { path: 'penguatkuasa-details/:id/:type', component: PenguatkuasaHandheldDetailsComponent },
                    { path: 'senarai-serahan-handheld', component: SerahanHandheldListComponent },
                    { path: 'serahan-details/:id/:type', component: SerahanHandheldDetailsComponent },
                    { path: 'senarai-handheld', component: HandheldListComponent },
                    { path: 'handheld-details/:id/:type', component: HandheldDetailsComponent },
                    { path: 'senarai-pengumuman-handheld', component: PengumumanHandheldListComponent },
                    { path: 'pengumuman-details/:id/:type', component: PengumumanHandheldDetailsComponent },
                    { path: 'senarai-pergerakan-handheld', component: PergerakanHandheldListComponent },
                    { path: 'pergerakan-details/:id/:type', component: PergerakanHandheldDetailsComponent },
                    { path: 'senarai-pergerakan-handheld/:searchHandheldId', component: PergerakanHandheldListComponent },
                    { path: 'senarai-patch-handheld', component: PatchHandheldListComponent },
                    { path: 'patch-details/:id/:type', component: PatchHandheldDetailsComponent },
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})

export class HandheldRoutingModule { }

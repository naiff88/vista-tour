import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatchHandheldListComponent } from './list.component';

describe('PatchHandheldListComponent', () => {
  let component: PatchHandheldListComponent;
  let fixture: ComponentFixture<PatchHandheldListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatchHandheldListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatchHandheldListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

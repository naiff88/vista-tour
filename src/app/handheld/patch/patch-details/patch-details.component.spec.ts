import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatchHandheldDetailsComponent } from './patch-details.component';

describe('PatchHandheldDetailsComponent', () => {
  let component: PatchHandheldDetailsComponent;
  let fixture: ComponentFixture<PatchHandheldDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatchHandheldDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatchHandheldDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HandheldService } from '../../handheld.service';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-patch-handheld-details',
  templateUrl: './patch-details.component.html',
  styleUrls: ['./patch-details.component.scss']
})

export class PatchHandheldDetailsComponent implements OnInit {

  @ViewChild('inputFile', { static: true }) inputFile: ElementRef;

  showSpinner: boolean = false;
  submitted = false;
  form: FormGroup;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;

  Id: number;
  PakejName: string = '';
  PakejLink: string = '';

  Status: number = 0;
  statusList: any = [];

  constructor(
    private handheldService: HandheldService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
  }

  get f() { return this.form.controls; }

  //open init function
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });
    //ref list call
    this.getStatusList();
    this.loadDetails(this.Id);
  }
  //close init function

  //open load form
  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.handheldService.getPatchHandheldDetails(Id)
        .subscribe(result => {
          this.showSpinner = false;
          this.PakejLink = result.Pakej;
          result.Pakej = '';
          this.Status = result.Status > 0 ? result.Status : 0;
          this.form.patchValue(result);
        });
    }
  }
  //close load form


  //open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod semasa akan ditetapkan semula!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.PakejName = '';
      this.loadDetails(this.Id)
    })
  }
  //open reset function

  //open save function
  onSave() {
    console.log('this.form.value :::::::::::::::::::: ', this.form.value);
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat sah & lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.showSpinner = true; 

      if (this.form.get('Pakej').value) {
        const formData = new FormData();
        formData.append('Pakej', this.form.get('Pakej').value);
        this.handheldService.uploadFile(formData)
          .subscribe(resUrl => {
            model.Pakej = resUrl;
            this.save(model)
          }, (err) => console.log(err));
      }
      else {
        model.Pakej = this.PakejLink;
        this.save(model)
      }
    }
  }

  save(model) {
    this.handheldService.savePatchHandheld(model, this.Id)
      .subscribe(r => {
        this.showSpinner = false;
        if (r.ReturnCode === 200) {
          Swal.fire(
            'Success',
            r.ResponseMessage,
            'success'
          );
          this.Id = r.Id;
        } else {
          Swal.fire(
            'Error',
            r.ResponseMessage,
            'error'
          );
        }
      });
  }
  //close save function


  //open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const NamaFail = formGroup.controls['NamaFail'];
      const Status = formGroup.controls['Status'];
      const Versi = formGroup.controls['Versi'];
      const Jenama = formGroup.controls['Jenama'];
      const Pakej = formGroup.controls['Pakej'];

      if (NamaFail) {
        NamaFail.setErrors(null);
        if (!NamaFail.value) {
          NamaFail.setErrors({ required: true });
        }
      }

      if (Status) {
        Status.setErrors(null);
        if (!Status.value) {
          Status.setErrors({ required: true });
        }
        else if (Status.value == 0) {
          Status.setErrors({ min: true });
        }
      }

      if (Versi) {
        Versi.setErrors(null);
        if (!Versi.value) {
          Versi.setErrors({ required: true });
        }
      }

      if (Jenama) {
        Jenama.setErrors(null);
        if (!Jenama.value) {
          Jenama.setErrors({ required: true });
        }
      }

      if (Pakej && this.PakejLink == '') {
        Pakej.setErrors(null);
        if (!Pakej.value) {
          Pakej.setErrors({ required: true });
        }
      }
    }
  }
  //open custom validation



  //open form setting 
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      NamaFail: ['', []],
      Status: ['1', []],
      Versi: ['', []],
      Jenama: ['', []],
      Pakej: ['', []],
      TarikhCipta: ['', []],
      DiciptaOleh: ['', []],
      TarikhKemaskini: ['', []],
      DikemaskiniOleh: ['', []],
    }, { validator: this.customValidation() }
    );
  }
  //open form setting 


  // open get ref list  
  getStatusList() {
    this.handheldService.getStatusList().subscribe(items => {
      this.statusList = items;
    });
  }
  // close get ref list 

  //upload file
  uploadFile(fileName) {
    let reader = new FileReader();
    if (fileName[0].target.files.length > 0) {
      const Pakej = fileName[0].target.files[0];
      this.PakejName = fileName[0].target.value;
      reader.readAsDataURL(Pakej);
      reader.onload = () => {
        this.form.patchValue({
          Pakej: reader.result
        });
      };
    }
  }

}

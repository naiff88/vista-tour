import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PergerakanHandheldListComponent } from './list.component';

describe('PergerakanHandheldListComponent', () => {
  let component: PergerakanHandheldListComponent;
  let fixture: ComponentFixture<PergerakanHandheldListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PergerakanHandheldListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PergerakanHandheldListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PergerakanHandheldDetailsComponent } from './pergerakan-details.component';

describe('PergerakanHandheldDetailsComponent', () => {
  let component: PergerakanHandheldDetailsComponent;
  let fixture: ComponentFixture<PergerakanHandheldDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PergerakanHandheldDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PergerakanHandheldDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

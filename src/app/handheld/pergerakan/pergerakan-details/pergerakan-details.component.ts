/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HandheldService } from '../../handheld.service';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-pergerakan-handheld-details',
  templateUrl: './pergerakan-details.component.html',
  styleUrls: ['./pergerakan-details.component.scss']
})

export class PergerakanHandheldDetailsComponent implements OnInit {

  showSpinner: boolean = false;
  submitted = false;
  form: FormGroup;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;

  Id: number;

  TarikhSerah: any;

  Pergerakan: number = 0;
  pergerakanList: any = [];
  Status: number = 0;
  statusList: any = [];

  constructor(
    private handheldService: HandheldService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
  }

  get f() { return this.form.controls; }

  //open init function
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });
    //ref list call
    this.getStatusList();
    this.getPergerakanList();
    this.loadDetails(this.Id);
  }
  //close init function

  //open load form
  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.handheldService.getPergerakanHandheldDetails(Id)
        .subscribe(result => {
          this.showSpinner = false;
          this.TarikhSerah = result.TarikhSerah = moment(result.TarikhSerah) || '';
          this.Pergerakan = result.Pergerakan > 0 ? result.Pergerakan : 0;
          this.Status = result.Status > 0 ? result.Status : 0;
          this.form.patchValue(result);
        });
    }
  }
  //close load form


  //open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod semasa akan ditetapkan semula!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  //open reset function

  //open save function
  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat sah & lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.showSpinner = true;
      this.handheldService.savePergerakanHandheld(model, this.Id)
        .subscribe(r => {
          this.showSpinner = false;
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.Id = r.Id;
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }
  //close save function


  //open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const Pergerakan = formGroup.controls['Pergerakan'];
      const Status = formGroup.controls['Status'];
      const Daripada = formGroup.controls['Daripada'];
      const Kepada = formGroup.controls['Kepada'];
      const IdHandheld = formGroup.controls['IdHandheld'];
      const TarikhSerah = formGroup.controls['TarikhSerah'];

      if (Pergerakan) {
        Pergerakan.setErrors(null);
        if (!Pergerakan.value) {
          Pergerakan.setErrors({ required: true });
        }
        else if (Pergerakan.value == 0) {
          Pergerakan.setErrors({ min: true });
        }
      }

      if (Status) {
        Status.setErrors(null);
        if (!Status.value) {
          Status.setErrors({ required: true });
        }
        else if (Status.value == 0) {
          Status.setErrors({ min: true });
        }
      }

      if (IdHandheld) {
        IdHandheld.setErrors(null);
        if (!IdHandheld.value) {
          IdHandheld.setErrors({ required: true });
        }
      }

      if (Daripada) {
        Daripada.setErrors(null);
        if (!Daripada.value) {
          Daripada.setErrors({ required: true });
        }
      }

      if (Kepada) {
        Kepada.setErrors(null);
        if (!Kepada.value) {
          Kepada.setErrors({ required: true });
        }
      }

      if (TarikhSerah) {
        TarikhSerah.setErrors(null);
        if (!TarikhSerah.value) {
          TarikhSerah.setErrors({ required: true });
        }
        else if (TarikhSerah.value && TarikhSerah.value > moment().toDate()) {
          TarikhSerah.setErrors({ exceed: true });
        }
      }

    }
  }
  //open custom validation



  //open form setting 
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      Pergerakan: ['1', []],
      Status: ['1', []],
      TarikhSerah: ['', []],
      IdHandheld: ['', []],
      Daripada: ['', []],
      Kepada: ['', []],
      Nota: ['', []],
      TarikhCipta: ['', []],
      DiciptaOleh: ['', []],
      TarikhKemaskini: ['', []],
      DikemaskiniOleh: ['', []],
    }, { validator: this.customValidation() }
    );
  }
  //open form setting 


  // open get ref list 
  getPergerakanList() {
    this.handheldService.getPergerakanList().subscribe(items => {
      this.pergerakanList = items;
    });
  }
  getStatusList() {
    this.handheldService.getStatusList().subscribe(items => {
      this.statusList = items;
    });
  }
  // close get ref list 

}

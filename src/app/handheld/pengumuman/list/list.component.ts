import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { PrimengTableHelper } from 'src/app/shared/PrimengTableHelper';
import { LazyLoadEvent } from 'primeng';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';
import { HandheldService } from '../../handheld.service';
import { Pager, PagerSetting } from '../../../../pagingnation';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import Swal from 'sweetalert2'
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';
require('jspdf-autotable');

@Component({
  selector: 'app-pengumuman-handheld-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class PengumumanHandheldListComponent implements OnInit {

  //open standard var for searching page
  dataSource = new MatTableDataSource<any>([]);
  selection = new SelectionModel<any>(true, []);
  primengTableHelper: PrimengTableHelper;
  pageSetting: PagerSetting;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  showSpinner: boolean = false;
  eventSearch: any;
  filterValue: any;
  onSearch: boolean = false;
  submitted: boolean = false;  
  reVisit: boolean = false;
  form: FormGroup;
  //close standard var for searching page

  // any unique name for searching list
  searchID = 'getPengumumanHandheldList'; 

  //initiate list
  jabatanList: any = [];

  //initiate list model 
  Jabatan: number = 0;
  
  //initiate date model 
  TarikhMula: any;
  TarikhAkhir: any;

  //for excel ganerator 
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  //standard contructor
  constructor(
    private handheldService: HandheldService,
    private snackBar: MatSnackBar,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.primengTableHelper = new PrimengTableHelper();
    this.pageSetting = new PagerSetting();
  }

  //form
  get f() { return this.form.controls; }


  //open standard function use for multiple delete in datatable
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  onSelectionChange(newItems) {
    this.selection.clear();
    newItems.forEach(row => this.selection.select(row));
  }
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.CustomerId + 1}`;
  }
  onRowSelect(event) {
    const evt = event.originalEvent as MouseEvent;
    evt.stopPropagation();
  }
  //close standard function use for multiple delete in datatable


  //init methode to call default function
  ngOnInit() {
    if (this.pageSetting.getSearchFilterValueSV(this.searchID)) {
      this.reVisit = true;
    }
    this.getPengumumanHandheldList();
    this.getJabatanList();
  }

  
  //open details page
  pengumumanDetails(id?: number, type?: string): void {
    this.router.navigate(['/app/handheld/pengumuman-details/', id, type]);
  }

  //open standard listing with search function
  async getPengumumanHandheldList(event?: LazyLoadEvent) {
    this.showSpinner = true;
    await this.reloadSearchForm();
    await this.handheldService.getPengumumanHandheldList(this.pageSetting.getPagerSettingWithSearch(this.paginator, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
      .subscribe(items => {
        this.showSpinner = false;
        this.dataSource.data = items.list;
        this.primengTableHelper.totalRecordsCount = items.pageInfo ? items.pageInfo.RowCount : items.list != null ? items.list.length : 0;
        this.primengTableHelper.records = items.list;
        if (this.onSearch) {
          this.snackBar.open(this.primengTableHelper.totalRecordsCount + ' rekod dijumpai', 'OK', {
            duration: 3000,
            verticalPosition: 'bottom',
            horizontalPosition: 'end'
          });
          this.onSearch = false;
        }
        this.primengTableHelper.hideLoadingIndicator();
        if (event! && (event.filters || event.sortField)) {
          this.paginator.changePage(0);
        }
      });
    await this.primengTableHelper.showLoadingIndicator();
  }
  //close standard listing with search function

  //open default listing function  
  getJabatanList() {
    this.handheldService.getJabatanList().subscribe(items => {
      this.jabatanList = items;
    });
  }
  //close default listing function


  //open reload search form
  reloadSearchForm() {
    this.filterValue = this.pageSetting.getSearchFilterValueSV(this.searchID);
    let SFcurrent = this.pageSetting.getSearchFilterValueSF(this.searchID);
    this.form = this.buildFormItems();
    if (SFcurrent) {
      //reasing value for list & date model
      this.TarikhMula = SFcurrent.TarikhMula = moment(SFcurrent.TarikhMula) || '';
      this.TarikhAkhir = SFcurrent.TarikhAkhir = moment(SFcurrent.TarikhAkhir) || '';
      this.form.patchValue(SFcurrent);
    } else {
      //initiate list & date model
      this.TarikhMula = '';
      this.TarikhAkhir = '';
    }
  }
   //close reload search form


  //search function
  search() {
    this.submitted = true;
    let searchForm = this.form.value;
    if (this.form.invalid) {
      Swal.fire(
        'Amaran',
        'Pastikan maklumat carian adalah sah!',
        'warning'
      );
    }
    else {
      this.submitted = false;
      //setting store procedure listing field for filtering
      this.filterValue = {
        " Jabatan ": { value: searchForm.IdHandheld, matchMode: "equals" },
        " Mesej ": { value: searchForm.NoGaji, matchMode: "contains" },
        " TarikhMula ": { value: searchForm.TarikhMula, matchMode: "dateEquals" },
        " TarikhAkhir ": { value: searchForm.TarikhAkhir, matchMode: "dateEquals" },
      }
      this.pageSetting.setSearchFilterLS(this.filterValue, searchForm, this.searchID);
      this.onSearch = true;
      this.getPengumumanHandheldList();
      this.paginator.changePage(0);
    }
  }

  //reset search form
  reset() {
    this.pageSetting.removeSearchFilterLS(this.searchID);
    this.form = this.buildFormItems();

    //initiate list & date model
    this.Jabatan = 0;
    this.TarikhMula = '';
    this.TarikhAkhir = '';

    this.filterValue = null;
    this.getPengumumanHandheldList();
    this.paginator.changePage(0);
  }

  //customValidation function
  customValidation() {
    return (formGroup: FormGroup) => {
      const TarikhMula = formGroup.controls['TarikhMula'];
      const TarikhAkhir = formGroup.controls['TarikhAkhir'];

      if (TarikhMula) {
        TarikhMula.setErrors(null);
        if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhMula.setErrors({ exceed: true });
        }
      }
      if (TarikhAkhir) {
        TarikhAkhir.setErrors(null);
        if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhAkhir.setErrors({ exceed: true });
        }
      }
    }
  }

  //default form builder to initate default value for search form field
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      Jabatan: ['0', []],
      Mesej: ['', []],
      TarikhMula: ['', []],
      TarikhAkhir: ['', []],
    }, { validator: this.customValidation() }
    );
  }



   //report props
   reportCol:any = [
    { name: 'index', title: 'No.', width: '5', align: 'center' },
    { name: 'Jabatan', title: 'Jabatan', width: '15', align: 'left'  },
    { name: 'TarikhMula', title: 'Tarikh Mula', width: '15', align: 'left' },
    { name: 'TarikhAkhir', title: 'Tarikh Akhir', width: '15', align: 'left' },
    { name: 'Mesej', title: 'Mesej', width: '15', align: 'left' },
    { name: 'TarikhCipta', title: 'Tarikh Dicipta', width: '15', align: 'left' },
  ];
  reportTitle:string = 'Senarai Pengumuman Handheld';

  //excel genarator
  printTableExcel()
  { 
    this.showSpinner = true;    
    let list: any;
    this.handheldService.getPengumumanHandheldList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
    .subscribe(items => {
      list = items.list;        
      this.pageSetting.excelGenerator(this.reportCol, list, this.reportTitle, this.reportTitle).then(loaded=>{
        if(loaded)
        {
          this.showSpinner = false;
        }
      });
    });   
  } 

  //pdf genarator
  printTablePdf()
  { 
    this.showSpinner = true;    
    let list: any;
    this.handheldService.getPengumumanHandheldList(this.pageSetting.getPagerSettingWithSearch(null, event, this.filterValue, this.primengTableHelper.getSorting(this.dataTable)))
    .subscribe(items => {
      list = items.list;        
      this.pageSetting.pdfGenerator(this.reportCol, list, this.reportTitle, this.reportTitle, 'p', 7).then(loaded=>{
        if(loaded)
        {
          this.showSpinner = false;
        }
      });
    });   
  }




  //multiple delete function
  delete() {
    const selectedItems: Array<{ id: number }> = this.selection.selected.map(item => ({ id: item.Id }));
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod yang telah hapus tidak akan dikembalikan!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, hapus!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.showSpinner = true;
        this.handheldService.deletePengumumanHandheld(selectedItems)
          .subscribe(resultDelete => {
            this.showSpinner = false;
            if (resultDelete.ReturnCode == 204) {
              Swal.fire(
                'Error',
                resultDelete.ResponseMessage,
                'error'
              );
            }
            else {
              Swal.fire(
                '',
                resultDelete.ResponseMessage,
                'success'
              );
            }
            this.getPengumumanHandheldList();
          });
      }
    })
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengumumanHandheldListComponent } from './list.component';

describe('PengumumanHandheldListComponent', () => {
  let component: PengumumanHandheldListComponent;
  let fixture: ComponentFixture<PengumumanHandheldListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengumumanHandheldListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengumumanHandheldListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

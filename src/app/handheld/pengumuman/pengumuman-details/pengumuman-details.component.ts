/* tslint:disable:max-line-length */
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HandheldService } from '../../handheld.service';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-pengumuman-handheld-details',
  templateUrl: './pengumuman-details.component.html',
  styleUrls: ['./pengumuman-details.component.scss']
})

export class PengumumanHandheldDetailsComponent implements OnInit {

  showSpinner: boolean = false;
  submitted = false;
  form: FormGroup;
  FormType: string;
  FormTitle: string = '';
  ViewMode: boolean = false;

  Id: number;

  TarikhMula: any;
  TarikhAkhir: any;

  Jabatan: number = 1;
  jabatanList: any = [];

  constructor(
    private handheldService: HandheldService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder) {
  }

  get f() { return this.form.controls; }

  //open init function
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.Id = params.id;
        this.FormType = params.type;
        if (params.type == 'add') {
          this.FormTitle = 'Tambah';
        }
        else if (params.type == 'view') {
          this.FormTitle = 'Papar';
          this.ViewMode = true;
        }
        else if (params.type == 'edit') {
          this.FormTitle = 'Kemaskini';
        }
      }
    });
    //ref list call
    this.getJabatanList();
    this.loadDetails(this.Id);
  }
  //close init function

  //open load form
  loadDetails(Id) {
    this.form = this.buildFormItems();
    if (Id > 0) {
      this.showSpinner = true;
      this.handheldService.getPengumumanHandheldDetails(Id)
        .subscribe(result => {
          this.showSpinner = false;
          this.TarikhMula = result.TarikhMula = moment(result.TarikhMula) || '';
          this.TarikhAkhir = result.TarikhAkhir = moment(result.TarikhAkhir) || '';
          this.Jabatan = result.Jabatan > 0 ? result.Jabatan : 0;
          this.form.patchValue(result);
        });
    }
  }
  //close load form


  //open reset function
  resetForm() {
    Swal.fire({
      title: 'Adakah anda pasti?',
      text: "Rekod semasa akan ditetapkan semula!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya, reset!',
      cancelButtonText: 'Batal'
    }).then((result) => {
      this.loadDetails(this.Id)
    })
  }
  //open reset function

  //open save function
  onSave() {
    this.submitted = true;
    if (this.form.invalid) {
      Swal.fire(
        'Nota',
        'Sila pastikan maklumat sah & lengkap diisi!',
        'warning'
      );
    } else {
      const model = this.form.value;
      this.showSpinner = true;
      this.handheldService.savePengumumanHandheld(model, this.Id)
        .subscribe(r => {
          this.showSpinner = false;
          if (r.ReturnCode === 200) {
            Swal.fire(
              'Success',
              r.ResponseMessage,
              'success'
            );
            this.Id = r.Id;
          } else {
            Swal.fire(
              'Error',
              r.ResponseMessage,
              'error'
            );
          }
        });
    }
  }
  //close save function


  //open custom validation
  customValidation() {
    return (formGroup: FormGroup) => {
      const Jabatan = formGroup.controls['Jabatan'];
      const Mesej = formGroup.controls['Mesej'];
      const TarikhMula = formGroup.controls['TarikhMula'];
      const TarikhAkhir = formGroup.controls['TarikhAkhir'];

      if (Jabatan) {
        Jabatan.setErrors(null);
        if (!Jabatan.value) {
          Jabatan.setErrors({ required: true });
        }
        else if (Jabatan.value == 0) {
          Jabatan.setErrors({ min: true });
        }
      }

      if (Mesej) {
        Mesej.setErrors(null);
        if (!Mesej.value) {
          Mesej.setErrors({ required: true });
        }
      }

      if (TarikhMula) {
        TarikhMula.setErrors(null);
        if (!TarikhMula.value) {
          TarikhMula.setErrors({ required: true });
        }
        else if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhMula.setErrors({ exceed: true });
        }
      }

      if (TarikhAkhir) {
        TarikhAkhir.setErrors(null);
        if (!TarikhAkhir.value) {
          TarikhAkhir.setErrors({ required: true });
        }
        else if (TarikhMula.value && TarikhAkhir.value && TarikhAkhir.value < TarikhMula.value) {
          TarikhAkhir.setErrors({ exceed: true });
        }
      }

    }
  }
  //open custom validation



  //open form setting 
  buildFormItems(): FormGroup {
    return this.formBuilder.group({
      Jabatan: ['1', []],
      TarikhMula: ['', []],
      TarikhAkhir: ['', []],
      Mesej: ['', []],
      TarikhCipta: ['', []],
      DiciptaOleh: ['', []],
      TarikhKemaskini: ['', []],
      DikemaskiniOleh: ['', []],
    }, { validator: this.customValidation() }
    );
  }
  //open form setting 


  // open get ref list 
  getJabatanList() {
    this.handheldService.getJabatanList().subscribe(items => {
      this.jabatanList = items;
    });
  }
  // close get ref list 

}

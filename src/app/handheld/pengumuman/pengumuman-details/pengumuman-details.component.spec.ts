import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengumumanHandheldDetailsComponent } from './pengumuman-details.component';

describe('PengumumanHandheldDetailsComponent', () => {
  let component: PengumumanHandheldDetailsComponent;
  let fixture: ComponentFixture<PengumumanHandheldDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengumumanHandheldDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengumumanHandheldDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

//== Class Definition

console.log('SnippetLogin ::::::::::::::::::::::::: ');


var SnippetLogin = (function() {

 
  var login = $('#m_login');
  var signupCall = function(e, s) {};
  var showErrorMsg = function(form, type, msg) {
    var alert = $(
      '<div class="m-alert m-alert--outline alert alert-' +
        type +
        ' alert-dismissible" role="alert">\
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
    <span></span>\
  </div>'
    );

    form.find('.alert').remove();
    alert.prependTo(form);
    //alert.animateClass('fadeIn animated');
    mUtil.animateClass(alert[0], 'fadeIn animated');
    alert.find('span').html(msg);
  };

  //== Private Functions

  var displaySignUpForm = function() {
    login.removeClass('m-login--forget-password');
    login.removeClass('m-login--signin');

    login.addClass('m-login--signup');
    mUtil.animateClass(login.find('.m-login__signup')[0], 'flipInX animated');
  };

  var displaySignInForm = function() {
    login.removeClass('m-login--forget-password');
    login.removeClass('m-login--signup');

    login.addClass('m-login--signin');
    mUtil.animateClass(login.find('.m-login__signin')[0], 'flipInX animated');
    //login.find('.m-login__signin').animateClass('flipInX animated');
  };

  var displayForgetPasswordForm = function() {

    console.log('displayForgetPasswordForm 1 :::::::::::::::::::: ');

    login.removeClass('m-login--signin');
    login.removeClass('m-login--signup');
    console.log('displayForgetPasswordForm 2 :::::::::::::::::::: ');
    login.addClass('m-login--forget-password');
    //login.find('.m-login__forget-password').animateClass('flipInX animated');

    //$(".fa-arrow-right").animate(

    //console.log('displayForgetPasswordForm 3 :::::::::::::::::::: ');
    mUtil.animateClass(
      login.find('.m-login__forget-password')[0],
      'flipInX animated'
    );
    console.log('displayForgetPasswordForm 4 :::::::::::::::::::: ');
  };

  var handleFormSwitch = function() {
    $('#m_login_forget_password').click(function(e) {
      e.preventDefault();
      displayForgetPasswordForm();
    });

    $('#m_login_forget_password_cancel').click(function(e) {
      e.preventDefault();
      displaySignInForm();
    });

    $('#m_login_signup').click(function(e) {
      e.preventDefault();
      displaySignUpForm();
    });

    $('#m_login_signup_cancel').click(function(e) {
      e.preventDefault();
      displaySignInForm();
    });
  };

  var handleSignInFormSubmit = function() {
    $('#m_login_signin_submit').click(function(e) {
      e.preventDefault();
      var btn = $(this);
      var form = $(this).closest('form');

      form.validate({
        rules: {
          email: {
            required: true,
            email: true
          },
          password: {
            required: true
          }
        }
      });

      if (!form.valid()) {
        return;
      }

      btn
        .addClass('m-loader m-loader--right m-loader--light')
        .attr('disabled', true);

      form.ajaxSubmit({
        url: '',
        success: function(response, status, xhr, $form) {
          // similate 2s delay
          setTimeout(function() {
            btn
              .removeClass('m-loader m-loader--right m-loader--light')
              .attr('disabled', false);
            showErrorMsg(
              form,
              'danger',
              'Incorrect username or password. Please try again.'
            );
          }, 2000);
        }
      });
    });
  };

  var handleSignUpFormSubmit = function(s) {
    $('#m_login_signup_submit').click(function(e) {
      e.preventDefault();

      var btn = $(this);
      var form = $(this).closest('form');

      form.validate({
        rules: {
          fullname: {
            required: true
          },
          email: {
            required: true,
            email: true
          },
          password: {
            required: true
          },
          rpassword: {
            required: true
          },
          agree: {
            required: true
          }
        }
      });

      if (!form.valid()) {
        return;
      }

      btn
        .addClass('m-loader m-loader--right m-loader--light')
        .attr('disabled', true);

      if (s) {
        s().then(
          r => {

            btn
              .removeClass('m-loader m-loader--right m-loader--light')
              .attr('disabled', false);
              if (r.returnCode == 200){
                form.clearForm();
                form.validate().resetForm();

                // display signup form
                displaySignInForm();
                var signInForm = login.find('.m-login__signin form');
                signInForm.clearForm();
                signInForm.validate().resetForm();

                showErrorMsg(
                  signInForm,
                  'success',
                  r.responseMessage
                );
              } else{

                showErrorMsg(
                  form,
                  'danger',
                  r.responseMessage
                );
              }
          },
          e => {
            //debugger;
            showErrorMsg(
              signInForm,
              'danger',
              e.responseMessage ||
                'Thank you. To complete your registration please check your email.'
            );
          }
        );
      } else {
        form.ajaxSubmit({
          url: '',
          success: function(response, status, xhr, $form) {
            // similate 2s delay
            setTimeout(function() {
              btn
                .removeClass('m-loader m-loader--right m-loader--light')
                .attr('disabled', false);
              form.clearForm();
              form.validate().resetForm();

              // display signup form
              displaySignInForm();
              var signInForm = login.find('.m-login__signin form');
              signInForm.clearForm();
              signInForm.validate().resetForm();

              showErrorMsg(
                signInForm,
                'success',
                'Thank you. To complete your registration please check your email.'
              );
            }, 2000);
          }
        });
      }
    });
  };

  var handleForgetPasswordFormSubmit = function(f) {
    $('#m_login_forget_password_submit').click(function(e) {
      e.preventDefault();

      var btn = $(this);
      var form = $(this).closest('form');

      form.validate({
        rules: {
          email: {
            required: true,
            email: true
          }
        }
      });

      if (!form.valid()) {
        return;
      }

      btn
        .addClass('m-loader m-loader--right m-loader--light')
        .attr('disabled', true);

      form.ajaxSubmit({
        url: '',
        success: function(response, status, xhr, $form) {
          // similate 2s delay
          setTimeout(function() {
            btn
              .removeClass('m-loader m-loader--right m-loader--light')
              .attr('disabled', false); // remove
            form.clearForm(); // clear form
            form.validate().resetForm(); // reset validation states

            // display signup form
            displaySignInForm();
            var signInForm = login.find('.m-login__signin form');
            signInForm.clearForm();
            signInForm.validate().resetForm();

            // showErrorMsg(
            //   signInForm,
            //   'success',
            //   'Cool! Password recovery instruction has been sent to your email.'
            // );
          }, 2000);
        }
      });
    });
  };

  //== Public Functions
  return {
    // public functions
    init: function(s, r, f) {
      //debugger;
      handleFormSwitch();
      handleSignInFormSubmit(s);
      handleSignUpFormSubmit(r);
      handleForgetPasswordFormSubmit(f);
    },
    displaySigninForm: displaySignInForm,
    displaySignUpForm: displaySignUpForm,
    displayForgetPasswordForm: displayForgetPasswordForm
  };
})();

// //== Class Initialization
// jQuery(document).ready(function() {
//   SnippetLogin.init();
// });

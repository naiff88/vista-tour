import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from './app/shared/loader/script-loader.service';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Helpers } from './app/helpers';

declare let mApp: any;
declare let mUtil: any;
declare let mLayout: any;


@Component({
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class RootComponent implements OnInit {

  globalBodyClass = 'm-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default m-scroll-top--shown';

  constructor(private _script: ScriptLoaderService, private _router: Router) {

  }
  ngOnInit() {
    this._script.loadScripts('body', ['assets/vendors/base/vendors.bundle.js', 'assets/demo/default/base/scripts.bundle.js'], true)
      .then(result => {
        Helpers.setLoading(false);
        // optional js to be loaded once
        this._script.loadScripts('head', ['assets/vendors/custom/fullcalendar/fullcalendar.bundle.js'], true);
      });
    this._router.events.subscribe((route) => {
      if (route instanceof NavigationStart) {
        // (<any>mLayout).closeMobileAsideMenuOffcanvas();
        // (<any>mLayout).closeMobileHorMenuOffcanvas();
        Helpers.setLoading(true);
        Helpers.bodyClass(this.globalBodyClass);
        // hide visible popover
        (<any>$('[data-toggle="m-popover"]')).popover('hide');
      }
      if (route instanceof NavigationEnd) {
        // init required js
        (<any>mApp).init();
        (<any>mUtil).init();
        Helpers.setLoading(false);
        // content m-wrapper animation
        let animation = 'm-animate-fade-in-up';
        $('.m-wrapper').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e) {
          $('.m-wrapper').removeClass(animation);
        }).removeClass(animation).addClass(animation);
      }
    });
  }
}


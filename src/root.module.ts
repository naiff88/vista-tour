import { NgModule, Injector, APP_INITIALIZER } from '@angular/core';
import * as _ from 'lodash';
import { RootRoutingModule } from './root-routing.module';
import { RootComponent } from './root.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppModule } from './app';
import { ScriptLoaderService } from './app/shared/loader/script-loader.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceProxyModule } from './shared/service-proxies/service-proxy.module';
import { CookieService } from 'src/shared/common/session/cookie.service';
import { AuthInterceptor } from './shared/helpers/auth.interceptor';
//import { ErrorInterceptor } from './shared/helpers/error.interceptor';
import { AuthService } from './shared/helpers/auth.service';
import { AppSessionService } from './shared/common/session/app-session.service';
import { CommonModule } from 'src/shared/common/common.module';
import { environment } from './environments/environment.prod';
import { API_BASE_URL } from './shared/service-proxies/service-proxies';


export function appInitializerFactory(injector: Injector) {
  let appSessionService: AppSessionService = injector.get(AppSessionService);
  let cookieService: CookieService = injector.get(CookieService);
  if (cookieService.check('spj_auth')) {
    return () => appSessionService.init();
  }
  return () => { };
}


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RootRoutingModule,
    ServiceProxyModule,
    AppModule,
    CommonModule.forRoot()
  ],
  declarations: [
    RootComponent
  ],
  providers: [
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: AuthInterceptor,
    //   multi: true
    // },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: ErrorInterceptor,
    //   multi: true
    // },
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFactory,
      deps: [Injector],
      multi: true,
    },
    { provide: API_BASE_URL, useFactory: getRemoteServiceBaseUrl },
    ScriptLoaderService,
    CookieService,
    AuthService
  ],
  bootstrap: [RootComponent]
})
export class RootModule {

}

export function getRemoteServiceBaseUrl(): string {
  return environment.API_BASE_URL;
}

import { Type } from '@angular/compiler';
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');

export class ReportTemplete {

  notis(setData) {
    return new Promise(resolve => {

      let htmlContent = ``;
      htmlContent += `<html>
     
      <style>
        @page {
          size: A4;
          margin: 0 auto;
          margin-bottom: 1cm;
          margin-top: 1cm;
        td{            
          color: grey;
          font-size: 12px;
          text-align: left;
        }

        @media print {
          html, body {
            width: 210mm;
            height: 297mm;
          }

          .tableMain {page-break-after: always;}
          tr {page-break-after: always;}
         }
      </style>
      <body>`;

      setData.map(function (item, index, array) {
             
         htmlContent +=  ` <h1 id="heading" > NOTIS : `+item.NoNotis+`</h1>
        <table width="100%"  class="tableMain" >
        <tbody>
        `;

        item = JSON.stringify(item);
        JSON.parse(item, function (key, value) {
          htmlContent += ` <tr>
        <td class="td" >`+ key + `</td>
        <td>`+ value + `</td>
        </tr>`;
        });
        htmlContent +=  `</tbody></table>`;
      });
      htmlContent += `</body
      </html>`;

      
      setTimeout(() => {
        this.reportPopupProps(htmlContent);
        resolve(true);
      });
    });
  }

  peringatanNotis(setData) {
    return new Promise(resolve => {

      let htmlContent = ``;
      htmlContent += `<html>
     
      <style>
        @page {
          size: A4;
          margin: 0 auto;
          margin-bottom: 1cm;
          margin-top: 1cm;
        td{            
          color: grey;
          font-size: 12px;
          text-align: left;
        }

        @media print {
          html, body {
            width: 210mm;
            height: 297mm;
          }

          .tableMain {page-break-after: always;}
          tr {page-break-after: always;}
         }
      </style>
      <body>`;

      setData.map(function (item, index, array) {
             
         htmlContent +=  ` <h1 id="heading" > PERINGATAN : `+item.NoNotis+`</h1>
        <table width="100%"  class="tableMain" >
        <tbody>
        `;

        item = JSON.stringify(item);
        JSON.parse(item, function (key, value) {
          htmlContent += ` <tr>
        <td class="td" >`+ key + `</td>
        <td>`+ value + `</td>
        </tr>`;
        });
        htmlContent +=  `</tbody></table>`;
      });
      htmlContent += `</body
      </html>`;

     

      setTimeout(() => {
        this.reportPopupProps(htmlContent);
        resolve(true);
      });
    });
  }
  

  aduanMahkamah(setData) {
    return new Promise(resolve => {

      let htmlContent = ``;
      htmlContent += `<html>
     
      <style>
        @page {
          size: A4;
          margin: 0 auto;
          margin-bottom: 1cm;
          margin-top: 1cm;
        td{            
          color: grey;
          font-size: 12px;
          text-align: left;
        }

        @media print {
          html, body {
            width: 210mm;
            height: 297mm;
          }

          .tableMain {page-break-after: always;}
          tr {page-break-after: always;}
         }
      </style>
      <body>`;

      setData.map(function (item, index, array) {
             
         htmlContent +=  ` <h1 id="heading" > ADUAN MAHKAMAH : `+item.NoNotis+`</h1>
        <table width="100%"  class="tableMain" >
        <tbody>
        `;

        item = JSON.stringify(item);
        JSON.parse(item, function (key, value) {
          htmlContent += ` <tr>
        <td class="td" >`+ key + `</td>
        <td>`+ value + `</td>
        </tr>`;
        });
        htmlContent +=  `</tbody></table>`;
      });
      htmlContent += `</body
      </html>`;

     

      setTimeout(() => {
        this.reportPopupProps(htmlContent);
        resolve(true);
      });
    });
  }


  sp2u(setData) {
    return new Promise(resolve => {

      let htmlContent = ``;
      htmlContent += `<html>
     
      <style>
        @page {
          size: A4;
          margin: 0 auto;
          margin-bottom: 1cm;
          margin-top: 1cm;
        td{            
          color: grey;
          font-size: 12px;
          text-align: left;
        }

        @media print {
          html, body {
            width: 210mm;
            height: 297mm;
          }

          .tableMain {page-break-after: always;}
          tr {page-break-after: always;}
         }
      </style>
      <body>`;

      setData.map(function (item, index, array) {
             
         htmlContent +=  ` <h1 id="heading" > SP2U : `+item.NoNotis+`</h1>
        <table width="100%"  class="tableMain" >
        <tbody>
        `;

        item = JSON.stringify(item);
        JSON.parse(item, function (key, value) {
          htmlContent += ` <tr>
        <td class="td" >`+ key + `</td>
        <td>`+ value + `</td>
        </tr>`;
        });
        htmlContent +=  `</tbody></table>`;
      });
      htmlContent += `</body
      </html>`;

     

      setTimeout(() => {
        this.reportPopupProps(htmlContent);
        resolve(true);
      });
    });
  }


  cetakanNotis(setData) {
    return new Promise(resolve => {

      let htmlContent = ``;
      htmlContent += `<html>
     
      <style>
        @page {
          size: A4;
          margin: 0 auto;
          margin-bottom: 1cm;
          margin-top: 1cm;
        td{            
          color: grey;
          font-size: 12px;
          text-align: left;
        }

        @media print {
          html, body {
            width: 210mm;
            height: 297mm;
          }

          .tableMain {page-break-after: always;}
          tr {page-break-after: always;}
         }
      </style>
      <body>`;

      setData.map(function (item, index, array) {
             
         htmlContent +=  ` <h1 id="heading" > CETAKAN NOTIS : `+item.NoNotis+`</h1>
        <table width="100%"  class="tableMain" >
        <tbody>
        `;

        item = JSON.stringify(item);
        JSON.parse(item, function (key, value) {
          htmlContent += ` <tr>
        <td class="td" >`+ key + `</td>
        <td>`+ value + `</td>
        </tr>`;
        });
        htmlContent +=  `</tbody></table>`;
      });
      htmlContent += `</body
      </html>`;

     

      setTimeout(() => {
        this.reportPopupProps(htmlContent);
        resolve(true);
      });
    });
  }


  rayuan(setData) {
    return new Promise(resolve => {

      let htmlContent = ``;
      htmlContent += `<html>
     
      <style>
        @page {
          size: A4;
          margin: 0 auto;
          margin-bottom: 1cm;
          margin-top: 1cm;
        td{            
          color: grey;
          font-size: 12px;
          text-align: left;
        }

        @media print {
          html, body {
            width: 210mm;
            height: 297mm;
          }

          .tableMain {page-break-after: always;}
          tr {page-break-after: always;}
         }
      </style>
      <body>`;

      setData.map(function (item, index, array) {
             
         htmlContent +=  ` <h1 id="heading" > RAYUAN : `+item.NoNotis+`</h1>
        <table width="100%"  class="tableMain" >
        <tbody>
        `;

        item = JSON.stringify(item);
        JSON.parse(item, function (key, value) {
          htmlContent += ` <tr>
        <td class="td" >`+ key + `</td>
        <td>`+ value + `</td>
        </tr>`;
        });
        htmlContent +=  `</tbody></table>`;
      });
      htmlContent += `</body
      </html>`;

      
      setTimeout(() => {
        this.reportPopupProps(htmlContent);
        resolve(true);
      });
    });
  }

  mahkamah(setData) {
    return new Promise(resolve => {

      let htmlContent = ``;
      htmlContent += `<html>
     
      <style>
        @page {
          size: A4;
          margin: 0 auto;
          margin-bottom: 1cm;
          margin-top: 1cm;
        td{            
          color: grey;
          font-size: 12px;
          text-align: left;
        }

        @media print {
          html, body {
            width: 210mm;
            height: 297mm;
          }

          .tableMain {page-break-after: always;}
          tr {page-break-after: always;}
         }
      </style>
      <body>`;

      setData.map(function (item, index, array) {
             
         htmlContent +=  ` <h1 id="heading" > Mahkamah : `+item.NoNotis+`</h1>
        <table width="100%"  class="tableMain" >
        <tbody>
        `;

        item = JSON.stringify(item);
        JSON.parse(item, function (key, value) {
          htmlContent += ` <tr>
        <td class="td" >`+ key + `</td>
        <td>`+ value + `</td>
        </tr>`;
        });
        htmlContent +=  `</tbody></table>`;
      });
      htmlContent += `</body
      </html>`;

      
      setTimeout(() => {
        this.reportPopupProps(htmlContent);
        resolve(true);
      });
    });
  }

  
  reportPopupProps(htmlContent){
    var myWindow = window.open("", "", "width=600,height=600");
    myWindow.document.write(htmlContent);
    myWindow.document.close(); //missing code
    myWindow.focus();
    myWindow.print();
    myWindow.close();
  }

}
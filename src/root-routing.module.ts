import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/account/login', pathMatch: 'full' },
  {
    path: 'account',
    loadChildren: './account/account.module#AccountModule', //Lazy load account module
    data: { preload: true }
  }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { onSameUrlNavigation:'reload' })],
    exports: [RouterModule],
    providers: []
})
export class RootRoutingModule { }

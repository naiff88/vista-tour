import { Injectable } from '@angular/core';
import { LoginServiceProxy,
   ResultDtoOfLoginModule,
   UserDetailsDto
} from '../../service-proxies/auth-service-proxies';
import { CookieService } from 'src/shared/common/session/cookie.service';
import * as ns from '@service-proxies';

@Injectable()
export class AppSessionService {

  private _modules: ResultDtoOfLoginModule;
  private _userDetails: UserDetailsDto;
  private _loginProxy: ns.LoginServiceProxy;

  constructor(private _loginService: LoginServiceProxy, private _cookieService: CookieService) {

  }

  get modules(): ResultDtoOfLoginModule {
    return this._modules;
  }

  get userDetails(): UserDetailsDto {
    return this._userDetails;
  }

  get userId(): number {
    return this._userDetails ? this.userDetails.userId : null;
  }

  init(): Promise<boolean> {

    return new Promise<boolean>((resolve, reject) => {
      /*this._loginProxy.userDetail().toPromise().then((result: UserDetailsDto) => {
        console.log('userDetail appSession:::::::::::::', result);
        this._userDetails = result;
        resolve(true);
      }, (err) => {
        reject(err);
      });*/
      this._loginService.getUserDetails().toPromise().then((result: UserDetailsDto) => {
        console.log('getUserDetails appSession:::::::::::::', result);
        this._userDetails = result;
        resolve(true);
      }, (err) => {
        reject(err);
      });
    });
  }

}

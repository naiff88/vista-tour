import { Injectable } from '@angular/core';

@Injectable()
export class CookieService {

  constructor() { }

  public getCookie(name: string) {

    

    let ca: Array<string> = document.cookie.split(';');
    let caLen: number = ca.length;
    let cookieName = `${name}=`;
    let c: string;

    for (let i: number = 0; i < caLen; i += 1) {
      c = ca[i].replace(/^\s+/g, '');
      if (c.indexOf(cookieName) == 0) {
        return c.substring(cookieName.length, c.length);
      }
    }
    return '';
  }

  public check(name: string): boolean {
    let check: boolean = false;
    if (this.getCookie(name) != '') {
      check = true;
    }
    return check;
  }

  public deleteCookie(name) {
    this.setCookie(name, "", -1, "/");
  }

  public setCookie(name: string, value: string, expireInSeconds: number, path: string = "/") {
    let tokenExpireDate: string = "expires=" + (new Date(new Date().getTime() + 1000 * expireInSeconds));
    document.cookie = name + "=" + value + "; " + tokenExpireDate + (path.length > 0 ? "; path=" + path : "");
  }

}

import * as ngCommon from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { AppSessionService } from './session/app-session.service';
import { CookieService } from './session/cookie.service';

@NgModule({
    imports: [
        ngCommon.CommonModule
    ]
})
export class CommonModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CommonModule,
            providers: [
              AppSessionService,
              CookieService
            ]
        };
    }
}

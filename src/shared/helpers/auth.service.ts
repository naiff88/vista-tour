import { Injectable } from '@angular/core';
import { CookieService } from 'src/shared/common/session/cookie.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  constructor(private _cookieService: CookieService, private router: Router) {
  }

  public getToken(): string {
    // console.log('getToken :::::::::: ', this._cookieService.getCookie('spj_auth'));
    // console.log('Router :::::::::: ', this.router.url);
    //alert('xxxxx');
    return this._cookieService.getCookie('spj_auth');
  }


}

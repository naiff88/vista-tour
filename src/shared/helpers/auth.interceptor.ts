import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CookieService } from '../common/session/cookie.service';
import { Router,ActivatedRoute  } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  paramToken: string = '';
  expireInSeconds: number;

  constructor(private _cookieService: CookieService, private router: Router,private activatedRoute: ActivatedRoute) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //console.log('intercept :::: ');
    //console.log('getToken :::::::::: ', this._cookieService.getCookie('spj_auth'));
    //console.log('Router :::::::::: ', this.router.url);
    this.activatedRoute.queryParams.subscribe(params => {
      this.paramToken = params['token'];
      //console.log('paramToken :::::: ', this.paramToken); // Print the parameter to the console.
  });


    if(this.paramToken)
    {
      req = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${this.paramToken}`)
      });

      this._cookieService.setCookie('spj_auth', this.paramToken, this.expireInSeconds);
      //this.clear();
      location.href = '/app/dashboard/menu';

    }
    else
    {
      let token = this._cookieService.getCookie('spj_auth');
      if (token!= '') {
        req = req.clone({
          headers: req.headers.set('Authorization', `Bearer ${token}`)
        });
      }
    }

    return next.handle(req);
  }
}

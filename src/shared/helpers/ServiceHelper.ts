import { Observable } from 'rxjs';

export class ServiceProxiesHelper {
  blobToText(blob: any): Observable<string> {
    return new Observable<string>((observer: any) => {
      if (!blob) {
        observer.next('');
        observer.complete();
      } else {
        const reader = new FileReader();
        reader.onload = function() {
          observer.next(this.result);
          observer.complete();
        };
        reader.readAsText(blob);
      }
    });
  }

  toCamel(o: any): any {
    let newO, origKey, newKey, value;
    if (o instanceof Array) {
      return o.map(function(value) {
        if (typeof value === 'object') {
          value = this.toCamel(value);
        }
        return value;
      });
    } else {
      newO = {};
      for (origKey in o) {
        if (o.hasOwnProperty(origKey)) {
          newKey = (
            origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey
          ).toString();
          value = o[origKey];
          if (
            value instanceof Array ||
            (value !== null && value.constructor === Object)
          ) {
            value = this.toCamel(value);
          }
          newO[newKey] = value;
        }
      }
    }
    return newO;
  }

}

export const localDateString = function(o) {
  let timezoneOffsetInHours = -(o.getTimezoneOffset() / 60); // UTC minus local time
  let sign = timezoneOffsetInHours >= 0 ? '+' : '-';
  let leadingZero = (Math.abs(timezoneOffsetInHours) < 10) ? '0' : '';

  let correctedDate = new Date(o.getFullYear(), o.getMonth(),
      o.getDate(), o.getHours(), o.getMinutes(), o.getSeconds(),
      o.getMilliseconds());
  correctedDate.setHours(o.getHours() + timezoneOffsetInHours);
  let iso = correctedDate.toISOString().replace('Z', '');

  return iso; //+ sign + leadingZero + Math.abs(timezoneOffsetInHours).toString() + ':00';
};

export function toCamel(o: any): any {
  let newO, origKey, newKey, value;
  if (o instanceof Array) {
    return o.map(function(value) {
      if (typeof value === 'object') {
        value = toCamel(value);
      }
      return value;
    });
  } else {
    newO = {};
    for (origKey in o) {
      if (o.hasOwnProperty(origKey)) {
        newKey = (
          origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey
        ).toString();
        value = o[origKey];
        if (
          value instanceof Array ||
          (value !== null && value.constructor === Object)
        ) {
          value = toCamel(value);
        }
        newO[newKey] = value;
      }
    }
  }
  return newO;
}

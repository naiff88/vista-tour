import { mergeMap as _observableMergeMap, catchError as _observableCatch, map, tap } from 'rxjs/operators';
import { Observable, from as _observableFrom, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { Injectable, Inject, Optional, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';

import * as moment from 'moment';
import { environment } from '../../environments/environment';

let baseUrl = environment.API_BASE_URL;

@Injectable()
export class ReferenceServiceProxy {

  private http: HttpClient;
  protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

  constructor(@Inject(HttpClient) http: HttpClient) {
    this.http = http;
  }

  uploadImage(file: File): Observable<UploadImageResult> {
    let url_ = baseUrl + "/api/upload/image";

    let formData = new FormData();
    formData.append('image', file[0]);

    let options_: any = {
      body: formData,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processUploadImage(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processUploadImage(<any>response_);
        } catch (e) {
          return <Observable<UploadImageResult>><any>_observableThrow(e);
        }
      } else
        return <Observable<UploadImageResult>><any>_observableThrow(response_);
    }));

  }

  protected processUploadImage(response: HttpResponseBase): Observable<UploadImageResult> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? UploadImageResult.fromJS(resultData200) : new UploadImageResult();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<UploadImageResult>(<any>null);
  }

  uploadDoc(file: File): Observable<any> {
    let url_ = baseUrl + "/api/upload/doc";

    let formData = new FormData();
    formData.append('doc', file[0]);

    let options_: any = {
      body: formData,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processUploadDoc(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processUploadDoc(<any>response_);
        } catch (e) {
          return <Observable<any>><any>_observableThrow(e);
        }
      } else
        return <Observable<any>><any>_observableThrow(response_);
    }));

  }

  protected processUploadDoc(response: HttpResponseBase): Observable<any> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
//        result200 = resultData200 ? any.fromJS(resultData200) : new any();
        return _observableOf(resultData200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<any>(<any>null);
  }

  uploadFile(data: FormData): Observable<string> {
    return this.http.post<string>(baseUrl + '/api/upload/doc', data)
      .pipe(
        map((resp: any) => {
          //console.log('uploadFile resp :::::::::::::::::::: ', resp);
          return resp.Result[0].DocFileName;
        })
      );
  }

  getAllCategoryType(): Observable<PagedResultDtoOfGetCategoryTypeForView> {
    let url_ = baseUrl + "/api/reference/category/type?";

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetAllCategoryType(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetAllCategoryType(<any>response_);
        } catch (e) {
          return <Observable<PagedResultDtoOfGetCategoryTypeForView>><any>_observableThrow(e);
        }
      } else
        return <Observable<PagedResultDtoOfGetCategoryTypeForView>><any>_observableThrow(response_);
    }));
  }

  protected processGetAllCategoryType(response: HttpResponseBase): Observable<PagedResultDtoOfGetCategoryTypeForView> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? PagedResultDtoOfGetCategoryTypeForView.fromJS(resultData200) : new PagedResultDtoOfGetCategoryTypeForView();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<PagedResultDtoOfGetCategoryTypeForView>(<any>null);
  }

  getCountries(): Observable<ListResultDtoOfCountryListDto> {
    let url_ = baseUrl + "/api/reference/country";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetCountries(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetCountries(<any>response_);
        } catch (e) {
          return <Observable<ListResultDtoOfCountryListDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<ListResultDtoOfCountryListDto>><any>_observableThrow(response_);
    }));
  }

  protected processGetCountries(response: HttpResponseBase): Observable<ListResultDtoOfCountryListDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? ListResultDtoOfCountryListDto.fromJS(resultData200) : new ListResultDtoOfCountryListDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<ListResultDtoOfCountryListDto>(<any>null);
  }

  getStates(countryId: number): Observable<ListResultDtoOfStateListDto> {
    let url_ = baseUrl + "/api/reference/state?";
    if (countryId !== undefined)
      url_ += "countryId=" + encodeURIComponent("" + countryId) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetStates(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetStates(<any>response_);
        } catch (e) {
          return <Observable<ListResultDtoOfStateListDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<ListResultDtoOfStateListDto>><any>_observableThrow(response_);
    }));
  }

  protected processGetStates(response: HttpResponseBase): Observable<ListResultDtoOfStateListDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? ListResultDtoOfStateListDto.fromJS(resultData200) : new ListResultDtoOfStateListDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<ListResultDtoOfStateListDto>(<any>null);
  }

  getCities(stateId: number): Observable<ListResultDtoOfCityListDto> {
    let url_ = baseUrl + "/api/reference/city?";
    if (stateId !== undefined)
      url_ += "stateId=" + encodeURIComponent("" + stateId) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetCities(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetCities(<any>response_);
        } catch (e) {
          return <Observable<ListResultDtoOfCityListDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<ListResultDtoOfCityListDto>><any>_observableThrow(response_);
    }));
  }

  protected processGetCities(response: HttpResponseBase): Observable<ListResultDtoOfCityListDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? ListResultDtoOfCityListDto.fromJS(resultData200) : new ListResultDtoOfCityListDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<ListResultDtoOfCityListDto>(<any>null);
  }

}

export class PagedResultDtoOfGetCategoryTypeForView implements IPagedResultDtoOfGetCategoryTypeForView {
  totalCount!: number | undefined;
  items!: CategoryTypeDto[] | undefined;

  constructor(data?: IPagedResultDtoOfGetCategoryTypeForView) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.totalCount = data["totalCount"];
      if (data["Result"] && data["Result"].constructor === Array) {
        this.items = [];
        for (let item of data["Result"])
          this.items.push(CategoryTypeDto.fromJS(item));
      }
    }
  }

  static fromJS(data: any): PagedResultDtoOfGetCategoryTypeForView {
    data = typeof data === 'object' ? data : {};
    let result = new PagedResultDtoOfGetCategoryTypeForView();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["totalCount"] = this.totalCount;
    if (this.items && this.items.constructor === Array) {
      data["Result"] = [];
      for (let item of this.items)
        data["Result"].push(item.toJSON());
    }
    return data;
  }
}

export interface IPagedResultDtoOfGetCategoryTypeForView {
  totalCount: number | undefined;
  items: CategoryTypeDto[] | undefined;
}


export class CategoryTypeDto implements ICategoryTypeDto {
  typeName!: string | undefined;
  id!: number | undefined;

  constructor(data?: ICategoryTypeDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.typeName = data["TypeName"];
      this.id = data["Id"];
    }
  }

  static fromJS(data: any): CategoryTypeDto {
    data = typeof data === 'object' ? data : {};
    let result = new CategoryTypeDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["TypeName"] = this.typeName;
    data["Id"] = this.id;
    return data;
  }
}

export interface ICategoryTypeDto {
  typeName: string | undefined;
  id: number | undefined;
}
function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
  if (result !== null && result !== undefined)
    return _observableThrow(result);
  else
    return _observableThrow(new SwaggerException(message, status, response, headers, null));
}

export class ListResultDtoOfCountryListDto implements IListResultDtoOfCountryListDto {
  items!: CountryListDto[] | undefined;

  constructor(data?: IListResultDtoOfCountryListDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data["Result"] && data["Result"].constructor === Array) {
        this.items = [];
        for (let item of data["Result"])
          this.items.push(CountryListDto.fromJS(item));
      }
    }
  }

  static fromJS(data: any): ListResultDtoOfCountryListDto {
    data = typeof data === 'object' ? data : {};
    let result = new ListResultDtoOfCountryListDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.items && this.items.constructor === Array) {
      data["Result"] = [];
      for (let item of this.items)
        data["Result"].push(item.toJSON());
    }
    return data;
  }
}

export interface IListResultDtoOfCountryListDto {
  items: CountryListDto[] | undefined;
}

export class CountryListDto implements ICountryListDto {
  countryName!: string | undefined;
  countryCode!: string | undefined;
  countryId!: number | undefined;

  constructor(data?: ICountryListDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.countryName = data["CountryName"];
      this.countryCode = data["CountryCode"];
      this.countryId = data["CountryId"];
    }
  }

  static fromJS(data: any): CountryListDto {
    data = typeof data === 'object' ? data : {};
    let result = new CountryListDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["CountryName"] = this.countryName;
    data["CountryCode"] = this.countryCode;
    data["CountryId"] = this.countryId;
    return data;
  }
}

export interface ICountryListDto {
  countryName: string | undefined;
  countryCode: string | undefined;
  countryId: number | undefined;
}

export class ListResultDtoOfStateListDto implements IListResultDtoOfStateListDto {
  items!: StateListDto[] | undefined;

  constructor(data?: IListResultDtoOfStateListDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data["Result"] && data["Result"].constructor === Array) {
        this.items = [];
        for (let item of data["Result"])
          this.items.push(StateListDto.fromJS(item));
      }
    }
  }

  static fromJS(data: any): ListResultDtoOfStateListDto {
    data = typeof data === 'object' ? data : {};
    let result = new ListResultDtoOfStateListDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.items && this.items.constructor === Array) {
      data["Result"] = [];
      for (let item of this.items)
        data["Result"].push(item.toJSON());
    }
    return data;
  }
}

export interface IListResultDtoOfStateListDto {
  items: StateListDto[] | undefined;
}

export class StateListDto implements IStateListDto {
  stateName!: string | undefined;
  stateCode!: string | undefined;
  stateId!: number | undefined;

  constructor(data?: IStateListDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.stateName = data["StateName"];
      this.stateCode = data["StateCode"];
      this.stateId = data["StateId"];
    }
  }

  static fromJS(data: any): StateListDto {
    data = typeof data === 'object' ? data : {};
    let result = new StateListDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["StateName"] = this.stateName;
    data["StateCode"] = this.stateCode;
    data["StateId"] = this.stateId;
    return data;
  }
}

export interface IStateListDto {
  stateName: string | undefined;
  stateCode: string | undefined;
  stateId: number | undefined;
}

export class ListResultDtoOfCityListDto implements IListResultDtoOfCityListDto {
  items!: CityListDto[] | undefined;

  constructor(data?: IListResultDtoOfCityListDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data["Result"] && data["Result"].constructor === Array) {
        this.items = [];
        for (let item of data["Result"])
          this.items.push(CityListDto.fromJS(item));
      }
    }
  }

  static fromJS(data: any): ListResultDtoOfCityListDto {
    data = typeof data === 'object' ? data : {};
    let result = new ListResultDtoOfCityListDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.items && this.items.constructor === Array) {
      data["Result"] = [];
      for (let item of this.items)
        data["Result"].push(item.toJSON());
    }
    return data;
  }
}

export interface IListResultDtoOfCityListDto {
  items: CityListDto[] | undefined;
}

export class CityListDto implements ICityListDto {
  cityName!: string | undefined;
  cityId!: number | undefined;

  constructor(data?: ICityListDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.cityName = data["CityName"];
      this.cityId = data["CityId"];
    }
  }

  static fromJS(data: any): CityListDto {
    data = typeof data === 'object' ? data : {};
    let result = new CityListDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["CityName"] = this.cityName;
    data["CityId"] = this.cityId;
    return data;
  }
}

export interface ICityListDto {
  cityName: string | undefined;
  cityId: number | undefined;
}

export class UploadImageResult implements IUploadImageResult {
  image!: ImageResult | undefined;
  responseMessage!: string | null | undefined;

  constructor(data?: IUploadImageResult) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      //result returned as array **hardcoded to first array to get result
      this.image = data["Result"][0] ? ImageResult.fromJS(data["Result"][0]) : <any>undefined;
      this.responseMessage = data["ResponseMessage"];
    }
  }

  static fromJS(data: any): UploadImageResult {
    data = typeof data === 'object' ? data : {};
    let result = new UploadImageResult();
    result.init(data);
    console.log(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["Result"][0] = this.image ? this.image.toJSON() : <any>undefined;
    data["ResponseMessage"] = this.responseMessage;
    return data;
  }
}

export interface IUploadImageResult {
  image: ImageResult | undefined;
  responseMessage: string | null | undefined;
}

export class ImageResult implements IImageResult {
  imageFileName!: string | null | undefined;

  constructor(data?: IImageResult) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.imageFileName = data["ImageFileName"];
    }
  }

  static fromJS(data: any): ImageResult {
    data = typeof data === 'object' ? data : {};
    let result = new ImageResult();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["ImageFileName"] = this.imageFileName;
    return data;
  }
}

export interface IImageResult {
  imageFileName: string | null | undefined;
}

function blobToText(blob: any): Observable<string> {
  return new Observable<string>((observer: any) => {
    if (!blob) {
      observer.next("");
      observer.complete();
    } else {
      let reader = new FileReader();
      reader.onload = function () {
        observer.next(this.result);
        observer.complete();
      }
      reader.readAsText(blob);
    }
  });
}

export class SwaggerException extends Error {
  message: string;
  status: number;
  response: string;
  headers: { [key: string]: any; };
  result: any;

  constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
    super();

    this.message = message;
    this.status = status;
    this.response = response;
    this.headers = headers;
    this.result = result;
  }

  protected isSwaggerException = true;

  static isSwaggerException(obj: any): obj is SwaggerException {
    return obj.isSwaggerException === true;
  }
}

import { NgModule } from '@angular/core';
import * as ReferenceServiceProxies from './reference-service-proxies';
import * as AuthServiceProxies from './auth-service-proxies';
import * as UserServiceProxies from './user-service-proxies';
import * as Ns from './service-proxies';
import { AuthInterceptor } from '../helpers/auth.interceptor';
//import { ErrorInterceptor } from '../helpers/error.interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { getRemoteServiceBaseUrl } from 'src/root.module';
import { ApiInterceptor } from '../interceptors/api.interceptor';


@NgModule({
  imports: [
    HttpClientModule,
  ],
  providers: [
    ReferenceServiceProxies.ReferenceServiceProxy,
    AuthServiceProxies.AuthServiceProxy,
    AuthServiceProxies.TenantRegistrationServiceProxy,
    AuthServiceProxies.LoginServiceProxy,
    UserServiceProxies.ProfileServiceProxy,
    Ns.ProfileServiceProxy,
    Ns.LoginServiceProxy,
    Ns.ReferenceServiceProxy,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    },
    { provide: Ns.API_BASE_URL, useFactory: getRemoteServiceBaseUrl },
    ]
})
export class ServiceProxyModule { }

import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, from as _observableFrom, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { Injectable, Inject, Optional, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { CookieService } from '../common/session/cookie.service';

import * as moment from 'moment';
import { environment } from '../../environments/environment';

const baseUrl = environment.API_BASE_URL;

@Injectable()
export class AuthServiceProxy {

  private http: HttpClient;
  protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

  constructor(@Inject(HttpClient) http: HttpClient, private _cookieService: CookieService) {
    this.http = http;
  }




  // authenticateRelogin(model: AuthenticateModel | null | undefined): Observable<AuthenticateResultModel> {
  //
  //   //console.log('authenticat model ::::: ', model);
  //
  //   let url_ = baseUrl + "/api/tenant/auth";
  //   url_ = url_.replace(/[?&]$/, "");
  //   this._cookieService;
  //   var str = [];
  //   for (var key in model) {
  //     if (model.hasOwnProperty(key)) {
  //       str.push(encodeURIComponent(key) + "=" + encodeURIComponent(model[key]))
  //     }
  //   }
  //   //str.push("username=azman@toolaku.com");
  //
  //   const content_ = str.join("&");
  //
  //   let options_: any = {
  //     body: content_,
  //     observe: "response",
  //     responseType: "blob",
  //     headers: new HttpHeaders({
  //       "Content-Type": "application/x-www-form-urlencoded",
  //       "Accept": "application/json"
  //     })
  //   };
  //
  //   return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
  //     return this.processAuthenticate(response_);
  //   })).pipe(_observableCatch((response_: any) => {
  //     if (response_ instanceof HttpResponseBase) {
  //       try {
  //         return this.processAuthenticate(<any>response_);
  //       } catch (e) {
  //         return <Observable<AuthenticateResultModel>><any>_observableThrow(e);
  //       }
  //     } else
  //       return <Observable<AuthenticateResultModel>><any>_observableThrow(response_);
  //   }));
  // }


  authenticate(model: AuthenticateModel | null | undefined): Observable<AuthenticateResultModel> {

    // console.log('authenticat ::::: ');

    let url_ = baseUrl + '/api/tenant/auth';
    url_ = url_.replace(/[?&]$/, '');
    this._cookieService;
    const str = [];
    for (const key in model) {
      if (model.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key) + '=' + encodeURIComponent(model[key]));
      }
    }

    const content_ = str.join('&');

    const options_: any = {
      body: content_,
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json'
      })
    };

    return this.http.request('post', url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processAuthenticate(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processAuthenticate(response_ as any);
        } catch (e) {
          return _observableThrow(e) as any as Observable<AuthenticateResultModel>;
        }
      } else {
        return _observableThrow(response_) as any as Observable<AuthenticateResultModel>;
      }
    }));
  }

  protected processAuthenticate(response: HttpResponseBase): Observable<AuthenticateResultModel> {

    // console.log('processAuthenticate response ::::: ', response);

    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (response as any).error instanceof Blob ? (response as any).error : undefined;

    const _headers: any = {}; if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); } }
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        const resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? AuthenticateResultModel.fromJS(resultData200) : new AuthenticateResultModel();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      }));
    }
    return _observableOf<AuthenticateResultModel>(null as any);
  }

}

@Injectable()
export class TenantRegistrationServiceProxy {
  private http: HttpClient;
  protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

  constructor(@Inject(HttpClient) http: HttpClient, private _cookieService: CookieService) {
    this.http = http;
  }

  /**
   * @input (optional)
   * @return Success
   */
  registerTenant(input: RegisterTenantInput | null | undefined): Observable<RegisterTenantOutput> {
    let url_ = baseUrl + "/api/login/tenant/register";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processRegisterTenant(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processRegisterTenant(<any>response_);
        } catch (e) {
          return <Observable<RegisterTenantOutput>><any>_observableThrow(e);
        }
      } else
        return <Observable<RegisterTenantOutput>><any>_observableThrow(response_);
    }));
  }



  changePassword(input: ChangePasswordInput | null | undefined): Observable<any> {
    let url_ = baseUrl + "/api/login/password/change";
    url_ = url_.replace(/[?&]$/, "");

    const content_ = JSON.stringify(input);

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("put", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processRegisterTenant(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processRegisterTenant(<any>response_);
        } catch (e) {
          return <Observable<any>><any>_observableThrow(e);
        }
      } else
        return <Observable<any>><any>_observableThrow(response_);
    }));
  }

  protected processRegisterTenant(response: HttpResponseBase): Observable<RegisterTenantOutput> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? RegisterTenantOutput.fromJS(resultData200) : new RegisterTenantOutput();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<RegisterTenantOutput>(<any>null);
  }
}




@Injectable()
export class LoginServiceProxy {
  private http: HttpClient;
  protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

  constructor(@Inject(HttpClient) http: HttpClient, private _cookieService: CookieService) {
    this.http = http;
  }

  /**
   * @input (optional)
   * @return Success
   */
  // getModules(): Observable<ResultDtoOfLoginModule> {
  //   let url_ = baseUrl + '/api/login/module';
  //   url_ = url_.replace(/[?&]$/, '');
  //
  //   const options_: any = {
  //     observe: 'response',
  //     responseType: 'blob',
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json',
  //       Accept: 'application/json'
  //     })
  //   };
  //
  //   return this.http.request('get', url_, options_).pipe(_observableMergeMap((response_: any) => {
  //     return this.processGetModules(response_);
  //   })).pipe(_observableCatch((response_: any) => {
  //     if (response_ instanceof HttpResponseBase) {
  //       try {
  //         return this.processGetModules(response_ as any);
  //       } catch (e) {
  //         return _observableThrow(e) as any as Observable<ResultDtoOfLoginModule>;
  //       }
  //     } else {
  //       return _observableThrow(response_) as any as Observable<ResultDtoOfLoginModule>;
  //     }
  //   }));
  // }

  // protected processGetModules(response: HttpResponseBase): Observable<ResultDtoOfLoginModule> {
  //   const status = response.status;
  //   const responseBlob =
  //     response instanceof HttpResponse ? response.body :
  //       (response as any).error instanceof Blob ? (response as any).error : undefined;
  //
  //   const _headers: any = {}; if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); } }
  //   if (status === 200) {
  //     return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
  //       let result200: any = null;
  //       const resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
  //       result200 = resultData200 ? ResultDtoOfLoginModule.fromJS(resultData200) : new ResultDtoOfLoginModule();
  //       return _observableOf(result200);
  //     }));
  //   } else if (status !== 200 && status !== 204) {
  //     return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
  //       return throwException('An unexpected server error occurred.', status, _responseText, _headers);
  //     }));
  //   }
  //   return _observableOf<ResultDtoOfLoginModule>(null as any);
  // }

  getUserDetails(): Observable<UserDetailsDto> {
    // console.log('getUserDetails ::::::::::::::: ');
    let url_ = baseUrl + '/api/login/userdetail';
    url_ = url_.replace(/[?&]$/, '');

    const options_: any = {
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json'
      })
    };

    return this.http.request('get', url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetUserDetails(response_);
    })).pipe(_observableCatch((response_: any) => {


      if (response_ instanceof HttpResponseBase) {

        // console.log('processGetUserDetails response_ 0 ::::::::::::::: ', response_.status);
        // if (response_.status == '401') {
        //   //console.log('LOGIN SKREEN ');
        //   location.href = '/account/login';

        // }

        try {
          // console.log('processGetUserDetails TRY ERRORRRR ::::::::::::::: ', response_);
          if (response_.status == 401) {
            // return this.processGetUserDetails(<any>response_);
            location.href = '/account/login';
            this._cookieService.deleteCookie('spj_auth');
            // return <Observable<UserDetailsDto>><any>_observableThrow(response_);
          } else {
            return this.processGetUserDetails(response_ as any);
          }

        } catch (e) {
          // console.log('processGetUserDetails CATACH ERRORRRR ::::::::::::::: ');
          return _observableThrow(e) as any as Observable<UserDetailsDto>;
        }
      } else {
        return _observableThrow(response_) as any as Observable<UserDetailsDto>;
      }
    }




    ));
  }

  protected processGetUserDetails(response: HttpResponseBase): Observable<UserDetailsDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (response as any).error instanceof Blob ? (response as any).error : undefined;

    const _headers: any = {}; if (response.headers) { for (const key of response.headers.keys()) { _headers[key] = response.headers.get(key); } }
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        const resultData200 = _responseText === '' ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? UserDetailsDto.fromJS(resultData200) : new UserDetailsDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException('An unexpected server error occurred.', status, _responseText, _headers);
      }));
    }
    return _observableOf<UserDetailsDto>(null as any);
  }
}

export enum ProgressBarState {
  Hide = -1,
  Show = 1
}

export class AuthenticateModel implements IAuthenticateModel {

  constructor(data?: IAuthenticateModel) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (this as any)[property] = (data as any)[property];
        }
      }
    }
  }
  grant_type!: string;
  username!: string;
  password!: string;

  static fromJS(data: any): AuthenticateModel {
    data = typeof data === 'object' ? data : {};
    const result = new AuthenticateModel();
    result.init(data);
    return result;
  }

  init(data?: any) {
    if (data) {
      this.grant_type = data.grant_type;
      this.username = data.username;
      this.password = data.password;
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data.grant_type = this.grant_type;
    data.username = this.username;
    data.password = this.password;
    return data;
  }
}

export interface IAuthenticateModel {
  grant_type: string;
  username: string;
  password: string;
}

export class AuthenticateResultModel implements IAuthenticateResultModel {

  constructor(data?: IAuthenticateResultModel) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (this as any)[property] = (data as any)[property];
        }
      }
    }
  }
  accessToken!: string | undefined;
  tokenType!: string | undefined;
  expireInSeconds!: number | undefined;

  static fromJS(data: any): AuthenticateResultModel {
    data = typeof data === 'object' ? data : {};
    const result = new AuthenticateResultModel();
    result.init(data);
    return result;
  }

  init(data?: any) {
    if (data) {
      this.accessToken = data.access_token;
      this.tokenType = data.token_type;
      this.expireInSeconds = data.expires_in;
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data.access_token = this.accessToken;
    data.token_type = this.tokenType;
    data.expires_in = this.expireInSeconds;
    return data;
  }
}

export interface IAuthenticateResultModel {
 accessToken: string | undefined;
  tokenType: string | undefined;
  expireInSeconds: number | undefined;
}


export class RegisterTenantInput implements IRegisterTenantInput {

  constructor(data?: IRegisterTenantInput) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (this as any)[property] = (data as any)[property];
        }
      }
    }
  }
  email!: string;
  name!: string;
  mobileNo!: number;
  password!: string | undefined;
  confirmPassword!: string | undefined;

  static fromJS(data: any): RegisterTenantInput {
    data = typeof data === 'object' ? data : {};
    const result = new RegisterTenantInput();
    result.init(data);
    return result;
  }

  init(data?: any) {
    if (data) {
      this.email = data.Email;
      this.name = data.Name;
      this.mobileNo = data.MobileNo;
      this.password = data.Password;
      //this.confirmPassword = data.ConfirmPassword;      
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data.Email = this.email;
    data.Name = this.name;
    data.MobileNo = this.mobileNo;
    data.Password = this.password;
    //data.ConfirmPassword = this.password;
    return data;
  }
}



export class ChangePasswordInput implements IChangePasswordInput {

  constructor(data?: IChangePasswordInput) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (this as any)[property] = (data as any)[property];
        }
      }
    }
  }
  // email!: string;
  // name!: string;
  // mobileNo!: number;
  OldPassword!: string | undefined;
  NewPassword!: string | undefined;

  static fromJS(data: any): ChangePasswordInput {
    data = typeof data === 'object' ? data : {};
    const result = new ChangePasswordInput();
    result.init(data);
    return result;
  }

  init(data?: any) {
    if (data) {
      // this.email = data["Email"];
      // this.name = data["Name"];
      // this.mobileNo = data["MobileNo"];
      this.OldPassword = data.OldPassword;
      this.NewPassword = data.NewPassword;
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    // data["Email"] = this.email;
    // data["Name"] = this.name;
    data.OldPassword = this.OldPassword;
    data.NewPassword = this.NewPassword;
    return data;
  }
}

export interface IRegisterTenantInput {
  email: string;
  name: string;
  mobileNo: number;
  password: string | undefined;
}

export interface IChangePasswordInput {
  NewPassword: string | undefined;
  OldPassword: string | undefined;
}

export class RegisterTenantOutput implements IRegisterTenantOutput {

  constructor(data?: IRegisterTenantOutput) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (this as any)[property] = (data as any)[property];
        }
      }
    }
  }
  isSuccessful!: boolean | undefined;
  userId!: number | undefined;
  returnCode!: string | undefined;
  responseMessage!: string | undefined;

  static fromJS(data: any): RegisterTenantOutput {
    data = typeof data === 'object' ? data : {};
    const result = new RegisterTenantOutput();
    result.init(data);
    return result;
  }

  init(data?: any) {
    if (data) {
      this.isSuccessful = data.IsSuccessful;
      this.userId = data.UserId;
      this.returnCode = data.ReturnCode;
      this.responseMessage = data.ResponseMessage;
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data.IsSuccessful = this.isSuccessful;
    data.UserId = this.userId;
    data.ReturnCode = this.returnCode;
    data.ResponseMessage = this.responseMessage;
    return data;
  }
}

export interface IRegisterTenantOutput {
  isSuccessful: boolean | undefined;
  userId: number | undefined;
  returnCode: string | undefined;
  responseMessage: string | undefined;
}

export class ResultDtoOfLoginModule implements IResultDtoOfLoginModule {

  constructor(data?: IResultDtoOfLoginModule) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (this as any)[property] = (data as any)[property];
        }
      }
    }
  }
  items!: LoginModule[] | undefined;

  static fromJS(data: any): ResultDtoOfLoginModule {
    data = typeof data === 'object' ? data : {};
    const result = new ResultDtoOfLoginModule();
    result.init(data);
    return result;
  }

  init(data?: any) {
    if (data) {
      if (data.Result && data.Result.constructor === Array) {
        this.items = [];
        for (const item of data.Result) {
          this.items.push(LoginModule.fromJS(item));
        }
      }
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.items && this.items.constructor === Array) {
      data.Result = [];
      for (const item of this.items) {
        data.Result.push(item.toJSON());
      }
    }
    return data;
  }
}

export interface IResultDtoOfLoginModule {
  items: LoginModule[] | undefined;
}


export class LoginModule implements ILoginModule {

  constructor(data?: ILoginModule) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (this as any)[property] = (data as any)[property];
        }
      }
    }
  }
  moduleName!: string | undefined;
  id!: number | undefined;

  static fromJS(data: any): LoginModule {
    data = typeof data === 'object' ? data : {};
    const result = new LoginModule();
    result.init(data);
    return result;
  }

  init(data?: any) {
    if (data) {
      this.moduleName = data.ModuleName;
      this.id = data.Id;
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data.ModuleName = this.moduleName;
    data.Id = this.id;
    return data;
  }
}

export interface ILoginModule {
  moduleName: string | undefined;
  id: number | undefined;
}

export class UserDetailsDto implements IUserDetailsDto {

  constructor(data?: IUserDetailsDto) {
    if (data) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          (this as any)[property] = (data as any)[property];
        }
      }
    }
  }
  userId!: number | undefined;
  name!: string | undefined;
  imageUrl!: string | undefined;

  static fromJS(data: any): UserDetailsDto {
    data = typeof data === 'object' ? data : {};
    const result = new UserDetailsDto();
    result.init(data);
    return result;
  }

  init(data?: any) {
    if (data) {
      this.userId = data.UserId;
      this.name = data.Name;
      this.imageUrl = data.ImageURL;
    }
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data.UserId = this.userId;
    data.Name = this.name;
    data.ImageURL = this.imageUrl;
    return data;
  }
}

export interface IUserDetailsDto {
  userId: number | undefined;
  name: string | undefined;
  imageUrl: string | undefined;
}


function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
  if (result !== null && result !== undefined) {
    return _observableThrow(result);
  } else {
    return _observableThrow(new SwaggerException(message, status, response, headers, null));
  }
}

function blobToText(blob: any): Observable<string> {
  return new Observable<string>((observer: any) => {
    if (!blob) {
      observer.next('');
      observer.complete();
    } else {
      const reader = new FileReader();
      reader.onload = function() {
        observer.next(this.result);
        observer.complete();
      };
      reader.readAsText(blob);
    }
  });
}

export class SwaggerException extends Error {
  message: string;
  status: number;
  response: string;
  headers: { [key: string]: any; };
  result: any;

  constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
    super();

    this.message = message;
    this.status = status;
    this.response = response;
    this.headers = headers;
    this.result = result;
  }

  protected isSwaggerException = true;

  static isSwaggerException(obj: any): obj is SwaggerException {
    return obj.isSwaggerException === true;
  }
}

import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, from as _observableFrom, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { Injectable, Inject, Optional, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';

import * as moment from 'moment';
import { environment } from '../../environments/environment';

let baseUrl = environment.API_BASE_URL;

@Injectable()
export class ProfileServiceProxy {
  private http: HttpClient;
  protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

  constructor(@Inject(HttpClient) http: HttpClient) {
    this.http = http;
  }

  getGeneralInfo(): Observable<CreateOrEditGeneralInfoDto> {
    let url_ = baseUrl + "/api/profile/general";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processCreateOrEditGeneralInfo(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processCreateOrEditGeneralInfo(<any>response_);
        } catch (e) {
          return <Observable<CreateOrEditGeneralInfoDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<CreateOrEditGeneralInfoDto>><any>_observableThrow(response_);
    }));
  }

  protected processCreateOrEditGeneralInfo(response: HttpResponseBase): Observable<CreateOrEditGeneralInfoDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? CreateOrEditGeneralInfoDto.fromJS(resultData200) : new CreateOrEditGeneralInfoDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<CreateOrEditGeneralInfoDto>(<any>null);
  }

  updateGeneralInfo(input: CreateOrEditGeneralInfoDto | null | undefined): Observable<void> {
    let url_ = baseUrl + "/api/profile/general";
    url_ = url_.replace(/[?&]$/, "");

    var str = [];
    for (var key in input) {
      if (input.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key) + "=" + encodeURIComponent(input[key]))
      }
    }

    const content_ = str.join("&");

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      })
    };

    return this.http.request("put", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processUpdate(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processUpdate(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }

  protected processUpdate(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return _observableOf<void>(<any>null);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<void>(<any>null);
  }

  //API Not Working - Under Review
  getContactInfo(): Observable<CreateOrEditContactInfoDto> {
    let url_ = baseUrl + "/api/profile/contact";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processCreateOrEditContactInfo(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processCreateOrEditContactInfo(<any>response_);
        } catch (e) {
          return <Observable<CreateOrEditContactInfoDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<CreateOrEditContactInfoDto>><any>_observableThrow(response_);
    }));
  }

  protected processCreateOrEditContactInfo(response: HttpResponseBase): Observable<CreateOrEditContactInfoDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? CreateOrEditContactInfoDto.fromJS(resultData200) : new CreateOrEditContactInfoDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<CreateOrEditContactInfoDto>(<any>null);
  }


  updateContactInfo(input: CreateOrEditContactInfoDto | null | undefined): Observable<void> {
    let url_ = baseUrl + "/api/profile/contact?";
    url_ = url_.replace(/[?&]$/, "");

    var str = [];
    for (var key in input) {
      if (input.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key) + "=" + encodeURIComponent(input[key]))
      }
    }

    const content_ = str.join("&");

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      })
    };

    return this.http.request("put", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processUpdateContactInfo(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processUpdateContactInfo(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }

  protected processUpdateContactInfo(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return _observableOf<void>(<any>null);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<void>(<any>null);
  }

  //under review
  getShareholderSummary(): Observable<CreateOrEditShareholderInfoDto> {
    let url_ = baseUrl + "/api/profile/shareholder";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processCreateOrEditShareholderSummary(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processCreateOrEditShareholderSummary(<any>response_);
        } catch (e) {
          return <Observable<CreateOrEditShareholderInfoDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<CreateOrEditShareholderInfoDto>><any>_observableThrow(response_);
    }));
  }

  protected processCreateOrEditShareholderSummary(response: HttpResponseBase): Observable<CreateOrEditShareholderInfoDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? CreateOrEditShareholderInfoDto.fromJS(resultData200) : new CreateOrEditShareholderInfoDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<CreateOrEditShareholderInfoDto>(<any>null);
  }


  updateShareholderSummary(input: CreateOrEditShareholderInfoDto | null | undefined): Observable<void> {
    let url_ = baseUrl + "/api/profile/shareholder?";
    url_ = url_.replace(/[?&]$/, "");

    var str = [];
    for (var key in input) {
      if (input.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key) + "=" + encodeURIComponent(input[key]))
      }
    }

    const content_ = str.join("&");

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      })
    };

    return this.http.request("put", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processUpdateShareholderSummary(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processUpdateShareholderSummary(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }

  protected processUpdateShareholderSummary(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return _observableOf<void>(<any>null);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<void>(<any>null);
  }

  createAward(input: CreateOrEditAwardDto | null | undefined): Observable<void> {
    let url_ = baseUrl + "/api/profile/award/detail";
    url_ = url_.replace(/[?&]$/, "");

    var str = [];
    for (var key in input) {
      if (input.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key) + "=" + encodeURIComponent(input[key]))
      }
    }

    const content_ = str.join("&");

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processCreateOrEditAward(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processCreateOrEditAward(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }

  updateAward(input: CreateOrEditAwardDto | null | undefined): Observable<void> {
    let url_ = baseUrl + "/api/profile/award/detail";
    url_ = url_.replace(/[?&]$/, "");

    var str = [];
    for (var key in input) {
      if (input.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key) + "=" + encodeURIComponent(input[key]))
      }
    }

    const content_ = str.join("&");

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      })
    };

    return this.http.request("put", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processCreateOrEditAward(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processCreateOrEditAward(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }


  protected processCreateOrEditAward(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return _observableOf<void>(<any>null);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<void>(<any>null);
  }

  getAwardList(): Observable<ListResultOfAwardListDto> {
    let url_ = baseUrl + "/api/profile/award/list";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetAwardList(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetAwardList(<any>response_);
        } catch (e) {
          return <Observable<ListResultOfAwardListDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<ListResultOfAwardListDto>><any>_observableThrow(response_);
    }));
  }

  protected processGetAwardList(response: HttpResponseBase): Observable<ListResultOfAwardListDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? ListResultOfAwardListDto.fromJS(resultData200) : new ListResultOfAwardListDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<ListResultOfAwardListDto>(<any>null);
  }

  deleteAward(tenantAwardId: number | null | undefined): Observable<void> {
    let url_ = baseUrl + "/api/profile/award/list?";
    if (tenantAwardId !== undefined)
      url_ += "TenantAwardId=" + encodeURIComponent("" + tenantAwardId) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      })
    };

    return this.http.request("delete", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processDeleteAward(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processDeleteAward(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }

  protected processDeleteAward(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return _observableOf<void>(<any>null);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<void>(<any>null);
  }

  createCertificate(input: CreateOrEditCertificateDto | null | undefined): Observable<void> {
    let url_ = baseUrl + "/api/profile/certificate/detail";
    url_ = url_.replace(/[?&]$/, "");

    var str = [];
    for (var key in input) {
      if (input.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key) + "=" + encodeURIComponent(input[key]))
      }
    }

    const content_ = str.join("&");

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      })
    };

    return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processCreateOrEditAward(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processCreateOrEditAward(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }

  updateCertificate(input: CreateOrEditCertificateDto | null | undefined): Observable<void> {
    let url_ = baseUrl + "/api/profile/certificate/detail";
    url_ = url_.replace(/[?&]$/, "");

    var str = [];
    for (var key in input) {
      if (input.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key) + "=" + encodeURIComponent(input[key]))
      }
    }

    const content_ = str.join("&");

    let options_: any = {
      body: content_,
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      })
    };

    return this.http.request("put", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processCreateOrEditCertificate(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processCreateOrEditCertificate(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }


  protected processCreateOrEditCertificate(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return _observableOf<void>(<any>null);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<void>(<any>null);
  }

  deleteCertificate(tenantCertificationId: number | null | undefined): Observable<void> {
    let url_ = baseUrl + "/api/profile/certification/list?";
    if (tenantCertificationId !== undefined)
      url_ += "TenantCertificationId=" + encodeURIComponent("" + tenantCertificationId) + "&";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      })
    };

    return this.http.request("delete", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processDeleteCertificate(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processDeleteCertificate(<any>response_);
        } catch (e) {
          return <Observable<void>><any>_observableThrow(e);
        }
      } else
        return <Observable<void>><any>_observableThrow(response_);
    }));
  }

  protected processDeleteCertificate(response: HttpResponseBase): Observable<void> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return _observableOf<void>(<any>null);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<void>(<any>null);
  }

  getCertificateList(): Observable<ListResultOfCertificateListDto> {
    let url_ = baseUrl + "/api/profile/certification/list";
    url_ = url_.replace(/[?&]$/, "");

    let options_: any = {
      observe: "response",
      responseType: "blob",
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      })
    };

    return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
      return this.processGetCertificateList(response_);
    })).pipe(_observableCatch((response_: any) => {
      if (response_ instanceof HttpResponseBase) {
        try {
          return this.processGetCertificateList(<any>response_);
        } catch (e) {
          return <Observable<ListResultOfCertificateListDto>><any>_observableThrow(e);
        }
      } else
        return <Observable<ListResultOfCertificateListDto>><any>_observableThrow(response_);
    }));
  }

  protected processGetCertificateList(response: HttpResponseBase): Observable<ListResultOfCertificateListDto> {
    const status = response.status;
    const responseBlob =
      response instanceof HttpResponse ? response.body :
        (<any>response).error instanceof Blob ? (<any>response).error : undefined;

    let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
    if (status === 200) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        let result200: any = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 ? ListResultOfCertificateListDto.fromJS(resultData200) : new ListResultOfCertificateListDto();
        return _observableOf(result200);
      }));
    } else if (status !== 200 && status !== 204) {
      return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      }));
    }
    return _observableOf<ListResultOfCertificateListDto>(<any>null);
  }

}

export class CreateOrEditGeneralInfoDto implements ICreateOrEditGeneralInfoDto {
  coverImgUrl!: string | null | undefined;
  logoImgUrl!: string | null | undefined;
  companyName!: string | null | undefined;
  brNo!: string | undefined;
  form9URL!: string | null | undefined;
  form13URL!: string | null | undefined;
  countryId!: number;
  address1!: string | null | undefined;
  address2!: string | null | undefined;
  postcode!: string | null | undefined;
  cityId!: number;
  stateId!: number;
  dateIncorporated!: any | null;
  Username!: string | null | undefined;
  returnCode!: number ;
  responseMessage!: string | null;

  constructor(data?: ICreateOrEditGeneralInfoDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.coverImgUrl = data["CoverImgUrl"];
      this.logoImgUrl = data["LogoImgUrl"];
      this.companyName = data["CompanyName"];
      this.brNo = data["BRNo"];
      this.form9URL = data["Form9URL"];
      this.form13URL = data["Form13URL"];
      this.countryId = data["CountryId"];
      this.address1 = data["Address1"];
      this.address2 = data["Address2"];
      this.postcode = data["Postcode"];
      this.cityId = data["CityId"];
      this.stateId = data["StateId"];
      this.dateIncorporated = data["DateIncorporated"];
      this.Username = data["Username"];      
      this.returnCode = data["ReturnCode"];
      this.responseMessage = data["ResponseMessage"];
    }
  }

  static fromJS(data: any): CreateOrEditGeneralInfoDto {
    data = typeof data === 'object' ? data : {};
    let result = new CreateOrEditGeneralInfoDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["CoverImgUrl"] = this.coverImgUrl;
    data["LogoImgUrl"] = this.logoImgUrl;
    data["CompanyName"] = this.companyName;
    data["BRNo"] = this.brNo;
    data["Form9URL"] = this.form9URL;
    data["Form13URL"] = this.form13URL;
    data["CountryId"] = this.countryId;
    data["Address1"] = this.address1;
    data["Address2"] = this.address2;
    data["Postcode"] = this.postcode;
    data["CityId"] = this.cityId;
    data["StateId"] = this.stateId;
    data["DateIncorporated"] = this.dateIncorporated;
  }
}

export interface ICreateOrEditGeneralInfoDto {
  coverImgUrl: string | null | undefined;
  logoImgUrl: string | null | undefined;
  companyName: string | null | undefined;
  brNo: string | undefined;
  form9URL: string | null | undefined;
  form13URL: string | null | undefined;
  countryId: number;
  address1: string | null | undefined;
  address2: string | null | undefined;
  postcode: string | null | undefined;
  cityId: number;
  stateId: number;
  dateIncorporated: string | null;
  returnCode: number;
  responseMessage: string | null;

}

export class CreateOrEditContactInfoDto implements ICreateOrEditContactInfoDto {
  officialEmail!: string | null | undefined;
  officePhone!: string | null | undefined;
  officePhone2!: string | null | undefined;
  officeFax!: string | undefined;
  picName!: string | null | undefined;
  picDesignation!: string | null | undefined;
  mobileNo!: string | null | undefined;

  constructor(data?: ICreateOrEditContactInfoDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.officialEmail = data["OfficialEmail"];
      this.officePhone = data["OfficePhone"];
      this.officePhone2 = data["OfficePhone2"];
      this.officeFax = data["OfficeFax"];
      this.picName = data["PICName"];
      this.picDesignation = data["PICDesignation"];
      this.mobileNo = data["MobileNo"];
    }
  }

  static fromJS(data: any): CreateOrEditContactInfoDto {
    data = typeof data === 'object' ? data : {};
    let result = new CreateOrEditContactInfoDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["OfficialEmail"] = this.officialEmail;
    data["OfficePhone"] = this.officePhone;
    data["OfficePhone2"] = this.officePhone2;
    data["OfficeFax"] = this.officeFax;
    data["PICName"] = this.picName;
    data["PICDesignation"] = this.picDesignation;
    data["MobileNo"] = this.mobileNo;
    return data;
  }
}

export interface ICreateOrEditContactInfoDto {
  officialEmail: string | null | undefined;
  officePhone: string | null | undefined;
  officePhone2: string | null | undefined;
  officeFax: string | null | undefined;
  picName: string | null | undefined;
  picDesignation: string | null | undefined;
  mobileNo: string | null | undefined;
}

export class CreateOrEditShareholderInfoDto implements ICreateOrEditShareholderInfoDto {
  authorizeCapital!: number | undefined;
  preferenceShares!: number | undefined;
  valuePreferenceShares!: number | undefined;
  ordinaryShares!: number | undefined;
  valueOrdinaryShares!: number | undefined;

  constructor(data?: ICreateOrEditShareholderInfoDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.authorizeCapital = data["AuthorizeCapital"];
      this.preferenceShares = data["PreferenceShares"];
      this.valuePreferenceShares = data["ValuePreferenceShares"];
      this.ordinaryShares = data["OrdinaryShares"];
      this.valueOrdinaryShares = data["ValueOrdinaryShares"];
    }
  }

  static fromJS(data: any): CreateOrEditShareholderInfoDto {
    data = typeof data === 'object' ? data : {};
    let result = new CreateOrEditShareholderInfoDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["AuthorizeCapital"] = this.authorizeCapital;
    data["PreferenceShares"] = this.preferenceShares;
    data["ValuePreferenceShares"] = this.valuePreferenceShares;
    data["OrdinaryShares"] = this.ordinaryShares;
    data["ValueOrdinaryShares"] = this.valueOrdinaryShares;
    return data;
  }
}

export interface ICreateOrEditShareholderInfoDto {
  authorizeCapital: number | undefined;
  preferenceShares: number | undefined;
  valuePreferenceShares: number | undefined;
  ordinaryShares: number | undefined;
  valueOrdinaryShares: number | undefined;
}

export class CreateOrEditEmployeeInfoDto implements ICreateOrEditEmployeeInfoDto {
  tenantEmployeeId!: number | undefined;
  departmentName!: string | null | undefined;
  noPermanentStaff!: number | undefined;
  noContractStaff!: number | undefined;

  constructor(data?: ICreateOrEditEmployeeInfoDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.tenantEmployeeId = data["TenantEmployeeId"];
      this.departmentName = data["DepartmentName"];
      this.noPermanentStaff = data["NoPermanentStaff"];
      this.noContractStaff = data["NoContractStaff"];
    }
  }

  static fromJS(data: any): CreateOrEditEmployeeInfoDto {
    data = typeof data === 'object' ? data : {};
    let result = new CreateOrEditEmployeeInfoDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["TenantEmployeeId"] = this.tenantEmployeeId;
    data["DepartmentName"] = this.departmentName;
    data["NoPermanentStaff"] = this.noPermanentStaff;
    data["NoContractStaff"] = this.noContractStaff;
    return data;
  }
}

export interface ICreateOrEditEmployeeInfoDto {
  tenantEmployeeId: number | undefined;
  departmentName: string | null | undefined;
  noPermanentStaff: number | undefined;
  noContractStaff: number | undefined;
}

export class CreateOrEditAwardDto implements ICreateOrEditAwardDto {
  logoUrl!: string | null | undefined;
  awardName!: string | null | undefined;
  description!: string | null | undefined;
  awardedBy!: string | null | undefined;
  dateAwarded!: string | undefined;
  tenantAwardId!: number | null | undefined;

  constructor(data?: ICreateOrEditAwardDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.logoUrl = data["LogoURL"];
      this.awardName = data["AwardName"];
      this.description = data["Description"];
      this.awardedBy = data["AwardedBy"];
      this.dateAwarded = data["DateAwarded"];
      this.tenantAwardId = data["TenantAwardId"];
    }
  }

  static fromJS(data: any): CreateOrEditAwardDto {
    data = typeof data === 'object' ? data : {};
    let result = new CreateOrEditAwardDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["LogoURL"] = this.logoUrl;
    data["AwardName"] = this.awardName;
    data["Description"] = this.description;
    data["AwardedBy"] = this.awardedBy;
    data["DateAwarded"] = this.dateAwarded;
    data["TenantAwardId"] = this.tenantAwardId;
  }
}

export interface ICreateOrEditAwardDto {
  logoUrl: string | null | undefined;
  awardName: string | null | undefined;
  description: string | null | undefined;
  awardedBy: string | null | undefined;
  dateAwarded: string | undefined;
  tenantAwardId: number | null | undefined;
}

export class ListResultOfAwardListDto implements IListResultOfAwardListDto {
  items!: CreateOrEditAwardDto[] | undefined;

  constructor(data?: IListResultOfAwardListDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data["Result"] && data["Result"].constructor === Array) {
        this.items = [];
        for (let item of data["Result"])
          this.items.push(CreateOrEditAwardDto.fromJS(item));
      }
    }
  }

  static fromJS(data: any): ListResultOfAwardListDto {
    data = typeof data === 'object' ? data : {};
    let result = new ListResultOfAwardListDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.items && this.items.constructor === Array) {
      data["Result"] = [];
      for (let item of this.items)
        data["Result"].push(item.toJSON());
    }
    return data;
  }
}

export interface IListResultOfAwardListDto {
  items: CreateOrEditAwardDto[] | undefined;
}

export class CreateOrEditCertificateDto implements ICreateOrEditCertificateDto {
  logoUrl!: string | null | undefined;
  certificateName!: string | null | undefined;
  certNo!: string | null | undefined;
  issuedBy!: string | null | undefined;
  dateIssued!: string | undefined;
  tenantCertificateId!: number | null | undefined;

  constructor(data?: ICreateOrEditCertificateDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.logoUrl = data["LogoURL"];
      this.certificateName = data["CertificateName"];
      this.certNo = data["CertNo"];
      this.issuedBy = data["IssuedBy"];
      this.dateIssued = data["DateIssued"];
      this.tenantCertificateId = data["TenantCertificateId"];
    }
  }

  static fromJS(data: any): CreateOrEditCertificateDto {
    data = typeof data === 'object' ? data : {};
    let result = new CreateOrEditCertificateDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["LogoURL"] = this.logoUrl;
    data["CertificateName"] = this.certificateName;
    data["CertNo"] = this.certNo;
    data["IssuedBy"] = this.issuedBy;
    data["DateIssued"] = this.dateIssued;
    data["TenantCertificateId"] = this.tenantCertificateId;
  }
}

export interface ICreateOrEditCertificateDto {
  logoUrl: string | null | undefined;
  certificateName: string | null | undefined;
  certNo: string | null | undefined;
  issuedBy: string | null | undefined;
  dateIssued: string | undefined;
  tenantCertificateId: number | null | undefined;
}

export class ListResultOfCertificateListDto implements IListResultOfCertificateListDto {
  items!: CreateOrEditCertificateDto[] | undefined;

  constructor(data?: IListResultOfCertificateListDto) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      if (data["Result"] && data["Result"].constructor === Array) {
        this.items = [];
        for (let item of data["Result"])
          this.items.push(CreateOrEditCertificateDto.fromJS(item));
      }
    }
  }

  static fromJS(data: any): ListResultOfCertificateListDto {
    data = typeof data === 'object' ? data : {};
    let result = new ListResultOfCertificateListDto();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    if (this.items && this.items.constructor === Array) {
      data["Result"] = [];
      for (let item of this.items)
        data["Result"].push(item.toJSON());
    }
    return data;
  }
}

export interface IListResultOfCertificateListDto {
  items: CreateOrEditCertificateDto[] | undefined;
}


function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
  if (result !== null && result !== undefined)
    return _observableThrow(result);
  else
    return _observableThrow(new SwaggerException(message, status, response, headers, null));
}

function blobToText(blob: any): Observable<string> {
  return new Observable<string>((observer: any) => {
    if (!blob) {
      observer.next("");
      observer.complete();
    } else {
      let reader = new FileReader();
      reader.onload = function () {
        observer.next(this.result);
        observer.complete();
      }
      reader.readAsText(blob);
    }
  });
}

export class SwaggerException extends Error {
  message: string;
  status: number;
  response: string;
  headers: { [key: string]: any; };
  result: any;

  constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
    super();

    this.message = message;
    this.status = status;
    this.response = response;
    this.headers = headers;
    this.result = result;
  }

  protected isSwaggerException = true;

  static isSwaggerException(obj: any): obj is SwaggerException {
    return obj.isSwaggerException === true;
  }
}

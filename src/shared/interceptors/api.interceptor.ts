import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import * as _ from 'lodash';
import { map, tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class ApiInterceptor implements ApiInterceptor {
  constructor(private snackBar: MatSnackBar) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
   
    const token = localStorage.getItem('AccessToken');
    //console.log('intercept:::::::::::::::::::::: ', token);
    if (token !== null && !req.headers.has('Authorization') && !req.url.includes('auth')) {
      req = req.clone({
        url: this.getApiURL(req.url),
        headers: req.headers.set('Authorization', token)
      });
      if (req.method === 'POST') {
        req = req.clone({
          headers: req.headers.set('Access-Control-Allow-Origin', '*')
                              .set('Authorization', token)
        });
      }
    }

    return next.handle(req)
      .pipe(
        tap({
          next: (event: HttpEvent<any>) => this.globalHttpErrorParser(event),
          error: (e: HttpErrorResponse) => this.commonHttpErrorHandler(e)
        })
      );
  }


  private globalHttpErrorParser(event: HttpEvent<any>) {
    if (event instanceof HttpResponse) {
      const httpResponse = event as HttpResponse<any>;
      const code = httpResponse.body.ReturnCode;
      if (parseFloat(code) >= 400) {
        const responseMessage = httpResponse.body.ResponseMessage;
        this.snackBar.open(responseMessage, 'OK', {duration: 5000});
      }
    }
  }

  private commonHttpErrorHandler(e: HttpErrorResponse) {
    const message: string = _.get(e, 'error.Message', 'Something went wrong.');
    this.snackBar.open(message, 'OK', {duration: 5000});
  }

  private getApiURL(url: string): string {
    if (url.includes(environment.API_BASE_URL)) {
      return url;
    }
    return environment.API_BASE_URL + url;
  }
}

import { Type } from '@angular/compiler';
import * as moment from 'moment';
declare const require: any;
const jsPDF = require('jspdf');
var Excel = require('exceljs');
import * as FileSaver from 'file-saver';




export interface Pager {
  RowsPerPage: number;
  PageNumber: number;
  OrderScript: string;
  ColumnFilterScript: string;
  GlobalFilter: string;
}

export interface PageInfo {
  rowCount: number;
}



export class PagerSetting {

  GlobalFilter: string = "";
  ColumnFilterScript: string = "";
  OrderScript: string = "";

  isEllipsisTitle(e, str) {
    //console.log('e.toElement.offsetWidth ::::: ' + (e.toElement.offsetWidth+20) + ' e.fromElement.offsetWidth :::::::::: ' + e.fromElement.offsetWidth);
    if (e && e.toElement && e.fromElement) {
      // console.log('e :::: ', e);
      // console.log('e toElement:::: ', e.toElement.innerHTML);
      // console.log('e fromElement:::: ', e.fromElement);
      if ((e.toElement.offsetWidth + 25) > e.fromElement.offsetWidth) {
        e.fromElement.title = str;
        //e.target.className = "marquee";
        //e.toElement.innerHTML = "<marquee behavior=\"scroll\" direction=\"left\" id=\"mymarquee\">"+str+"</marquee>";
      }
      else {
        //e.target.classList.remove("marquee");
        e.fromElement.title = '';
        //e.toElement.innerHTML = str;
      }
    }
  }



  printToLetter(num) {
    //console.log('printToLetter ::::::::::: ', num);
    var s = '', t;
    while (num > 0) {
      t = (num - 1) % 26;
      s = String.fromCharCode(65 + t) + s;
      num = (num - t) / 26 | 0;
    }
    return s || undefined;
  }


  excelGenerator(col, list, excelName, reportTitle, footerContent: any = []) {
    return new Promise(resolve => {
      var workbook = new Excel.Workbook();
      var lastLetter = this.printToLetter(col.length);

      // Tab Title
      var worksheet = workbook.addWorksheet(excelName);

      let colTitle = [];
      let colName = [];
      let k = 1;
      for (var key in col) {
        var column = this.printToLetter(k) + '2';
        worksheet.getCell(column).value = col[key].title;
        worksheet.getCell(column).alignment = { horizontal: col[key].align, vertical: 'top', wrapText: true };

        colName.push({
          key: col[key].name, width: col[key].width,
          style: { alignment: { horizontal: col[key].align, vertical: 'top', wrapText: true } }
        });

        k++;
      }
      worksheet.mergeCells('A1', lastLetter + '1'); //title merge cell
      worksheet.getCell('A1').value = reportTitle; //title

      // Column key
      worksheet.columns = colName;

      //console.log('list :::::::::: ', list);

      let i = 1;
      for (var obj in list) {
        let data = [];
        for (var keycolName in col) {
          if (col[keycolName].name == 'index') {
            data.push(i);
          }
          else {
            data.push(list[obj][col[keycolName].name]);
          }
        }
        worksheet.addRow(data);
        i++;
      }


      worksheet.eachRow(function (row, _rowNumber) {
        row.eachCell(function (cell, _colNumber) {
          cell.border = {
            top: { style: 'thin' },
            left: { style: 'thin' },
            bottom: { style: 'thin' },
            right: { style: 'thin' }
          };
        });
      });

      
      if(footerContent)
      {
        
        let i = 0;
        for (var key in footerContent) {
          worksheet.getCell('B'+(list.length+5+i)).value = footerContent[key].label;
          worksheet.getCell('C'+(list.length+5+i)).value = footerContent[key].value;
          i++;
        }
      }

      workbook.xlsx.writeBuffer().then(data => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' });
        //create excell file
        FileSaver.saveAs(blob, excelName + '.xlsx');
        setTimeout(() => {
          resolve(true);
        });
      });
    });
  }
  

  pdfGenerator(col, list, pdfName, reportTitle, reportType, fontSize, headerMargin: number = 15, footerContent: any = []) {
    return new Promise(resolve => {
      let colTitle = [];
      let colStyle = {};
      let headerStyle = [];
      let cols = [];
      for (var key in col) {
        colTitle.push(col[key].title);
        cols.push({ header: col[key].title, dataKey: col[key].name });
        colStyle[col[key].name] = { halign: col[key].align };
        //headerStyle[col[key].name] = {halign: col[key].align};
        headerStyle.push({ content: col[key].title,  styles: {halign: col[key].align}});
      }
      // console.log('colStyle :::::::::::::::::: ', colStyle);
      //console.log('headerStyle :::::::::::::::::: ', headerStyle);
      var doc = new jsPDF(reportType);
      var rows = [];
      let i = 1;
      for (var key in list) {
        let data = {};
        for (var keycolName in col) {
          if (col[keycolName].name == 'index') {
            data['index'] = i;
          }
          else {
            data[col[keycolName].name] = list[key][col[keycolName].name];
          }
        }
        rows.push(data);
        i++;
      }
      //report title    
      let title = reportTitle;

      //var text = "Hi How are you",

      // var footer = function (data) {
      //  // doc.text("My footer ---------- " ,data.settings.margin.left, doc.autoTable.previous.finalY+30)
      //   doc.text("My footer ---------- " , data.settings.margin.left, 10, {maxWidth: reportType == 'l' ? 260 : 180, align : "justify"});
      // }
   
      var header = function (data) {
        // console.log('data.settings ::::::::::::::: ', data.settings.tableWidth);
        doc.setFontSize(10);
        doc.text(title, data.settings.margin.left, 10, {maxWidth: reportType == 'l' ? 260 : 180, align : "justify"});

        var pageSize = doc.internal.pageSize;
        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();

        //doc.text("My footer ---------- " , data.settings.margin.left,  pageHeight - 10, {maxWidth: reportType == 'l' ? 260 : 180, align : "justify"});
        //let xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(title) * doc.internal.getFontSize() / 2); 
        //doc.text(title, xOffset, 10);
        //doc.text(title, data.settings.margin.left, 10, 'center');
        


        //doc.text('Hi How are you', data.settings.margin.left, 10, 'center');
        // doc.setFontSize(14);
        // //doc.columnStyles('wrap');
        // doc.setFontStyle('normal');
        // doc.text(title, data.settings.margin.left, 10);
        //doc.text('The text', doc.internal.pageSize.width, 50, null, null, 'center');
        //doc.text(title, 40, 30);
      };

      //console.log('header :::::::::::::: ', header);

      doc.autoTable(
        // colTitle, rows, 
        {
          columnStyles: colStyle,
          body: rows,
          columns: cols,
          head: [headerStyle],
          rowPageBreak: 'avoid',
          margin: { top: headerMargin }, theme: 'grid',
          didDrawPage  : header,
         // afterPageContent: footer,
          headStyles : {
            lineWidth: 0.2,
            //columnStyles: 'wrap',
            lineColor: [209, 209, 209],
            fillColor: [0, 0, 0],
            textColor: [255, 255, 255],
            fontSize,
            //minCellWidth: 5
            // overflow : 'linebreak',
            // columnWidth : 'wrap'
            //minCellWidth: 'auto'
          },
        //   styles : {   
        //     overflow : 'linebreak',
        //     columnWidth : 'wrap'
        //  },
          // headerStyles: headerStyle,
          bodyStyles: {
            fontSize,
            lineWidth: 0.2,
            //minCellWidth: 5
            // overflow : 'linebreak',
            // columnWidth : 'wrap'
          },
          //drawHeaderCell: this.enhanceWordBreak,
          didParseCell: this.enhanceWordBreak,
          // drawHeaderCell: function (cell, data) {
          //   console.log('cell ::::::::::::::::::::::: ', cell);
          //   if (cell.raw === 'index') {//paint.Name header red
          //     cell.styles.fontSize = 15;
          //     cell.styles.halign = 'right';
          //     cell.styles.textColor = [255, 0, 0];
          //   } else {
          //     cell.styles.textColor = 255;
          //     cell.styles.fontSize = 10;
          //   }
          // },
        },
      );
      //create pdf file
      //doc.save(pdfName + '.pdf');
      
      //doc.addPage();
      //doc.fromHTML('FOOTER', 15, doc.autoTable.previous.finalY+10, {maxWidth: reportType == 'l' ? 260 : 180, align : "justify"});

      //console.log('footerContent :::: ', footerContent);

      if(footerContent)
      {
        let gapMargin = 10;
        doc.setFontSize(10);
        for (var key in footerContent) {
          let displayText = footerContent[key].label + ' : ' + footerContent[key].value;
          doc.text(displayText, 15, doc.autoTable.previous.finalY+gapMargin, {maxWidth: reportType == 'l' ? 260 : 180, align : "justify"});
          gapMargin = gapMargin + 5;
        }
      }
     
     
      // doc.text('F2', 15, doc.autoTable.previous.finalY+15, {maxWidth: reportType == 'l' ? 260 : 180, align : "justify"});
      // doc.text('F3', 15, doc.autoTable.previous.finalY+20, {maxWidth: reportType == 'l' ? 260 : 180, align : "justify"});

      doc.autoPrint();
      window.open(doc.output('bloburl'), '_blank');
      setTimeout(() => {
        resolve(true);
      });
    });
  }

  

  enhanceWordBreak({ doc, cell, column }) {
    if (cell === undefined) {
      return;
    }

    const hasCustomWidth = (typeof cell.styles.cellWidth === 'number');

    if (hasCustomWidth || cell.raw == null || cell.colSpan > 1) {
      return
    }

    let text;

    if (cell.raw instanceof Node) {
      text = cell.raw.innerText;
    } else {
      if (typeof cell.raw == 'object') {
        // not implemented yet
        // when a cell contains other cells (colSpan)
        return;
      } else {
        text = '' + cell.raw;
      }
    }

    // split cell string by spaces
    const words = text.split(/\s+/);

    // calculate longest word width
    const maxWordUnitWidth = words.map(s => Math.floor(doc.getStringUnitWidth(s) * 100) / 100).reduce((a, b) => Math.max(a, b), 0);
    const maxWordWidth = maxWordUnitWidth * (cell.styles.fontSize / doc.internal.scaleFactor)

    const minWidth = cell.padding('horizontal') + maxWordWidth;

    // update minWidth for cell & column
    if (minWidth > cell.minWidth) {
      cell.minWidth = minWidth;
    }
    if (cell.minWidth > cell.wrappedWidth) {
      cell.wrappedWidth = cell.minWidth;
    }
    if (cell.minWidth > column.minWidth) {
      column.minWidth = cell.minWidth;
    }
    if (column.minWidth > column.wrappedWidth) {
      column.wrappedWidth = column.minWidth;
    }
  }


  getPagerSetting(paginator: any = null, event: any = null, getSorting: any = null) {
    let page: Pager = {} as any;
    if (paginator) {
      let pageNumber = paginator.getPage();

      // console.log('RowsPerPage 1 >>>>>>>>>>>>>>>>>> ', paginator.rows);
      // console.log('RowsPerPage 2 >>>>>>>>>>>>>>>>>> ', paginator._rows);

      let RowsPerPage = paginator.rows;
      page.PageNumber = !pageNumber ? 1 : pageNumber + 1;
      page.RowsPerPage = !RowsPerPage || RowsPerPage == 0 ? 10 : RowsPerPage;

      if (event) {
        //console.log('event.filters ::::::::::::::::::: ', event);
        //console.log('event.filters ::::::::::::::::::: ', event.filters);
        if (event.filters) {
          this.ColumnFilterScript = "";
          this.GlobalFilter = "";
          let columnCount = 0;
          for (var key in event.filters) {
            if (key != 'global') {
              columnCount++;
              if (event.filters[key].matchMode == 'contains' || event.filters[key].matchMode == 'equals') {
                if (columnCount > 1) {
                  this.ColumnFilterScript += " AND ";
                }
                if (event.filters[key].matchMode == 'contains') {
                  this.ColumnFilterScript += " " + key + " like '%" + event.filters[key].value + "%' ";
                }
                else if (event.filters[key].matchMode == 'equals') {
                  this.ColumnFilterScript += " " + key + " = '" + event.filters[key].value + "' ";
                }
              }
            }
            else {
              this.GlobalFilter = event.filters[key].value;
            }
          }
        }
      }
    }
    page.ColumnFilterScript = this.ColumnFilterScript;
    if (getSorting != undefined) {
      this.OrderScript = getSorting;
    }
    page.OrderScript = this.OrderScript;
    page.GlobalFilter = this.GlobalFilter;
    return page;
  }

  setSearchFilterLS(filter, searchForm, id) {
    localStorage.setItem('SF' + id, JSON.stringify(searchForm));
    localStorage.setItem('SV' + id, JSON.stringify(filter));
  }

  getSearchFilterValueSV(id) {
    return localStorage.getItem('SV' + id) !== null && localStorage.getItem('SV' + id) !== 'null' ? JSON.parse(localStorage.getItem('SV' + id)) : null;
  }

  getSearchFilterValueSF(id) {
    //console.log('getSearchFilterValueSF :::::::::::::: ', localStorage.getItem('SF' + id));
    //console.log('getSearchFilterValueSF [] :::::::::::::: ', localStorage.getItem('SF' + id) !== null && localStorage.getItem('SF' + id) !== 'null' ? JSON.parse(localStorage.getItem('SF' + id)) : []);
    return localStorage.getItem('SF' + id) !== null && localStorage.getItem('SF' + id) !== 'null' ? JSON.parse(localStorage.getItem('SF' + id)) : null;
  }

  removeSearchFilterLS(id) {
    localStorage.removeItem('SF' + id);
    localStorage.removeItem('SV' + id);
  }


  getPagerSettingWithSearch(paginator: any = null, event: any = null, searchObject: any = null, getSorting: any = null) {
    //console.log('getPagerSettingWithSearch paginator :::::::::::::::: ', paginator)
    let page: Pager = {} as any;
    if (paginator) {
      //console.log('getPagerSettingWithSearch paginator paginator.getPage() :::::::::::::::: ', paginator.getPage())
      let pageNumber = paginator.getPage();
      let RowsPerPage = paginator._rows;
      page.PageNumber = !pageNumber ? 1 : pageNumber + 1;
      page.RowsPerPage = !RowsPerPage || RowsPerPage == 0 ? 10 : RowsPerPage;

      this.ColumnFilterScript = "";
      this.GlobalFilter = "";
      let columnCount = 0;

      /*
      if (event && event.filters) {
        for (var key in event.filters) {
          if (key != 'global') {
            columnCount++;
            if (event.filters[key].matchMode == 'contains' || event.filters[key].matchMode == 'equals') {
              if (columnCount > 1) {
                this.ColumnFilterScript += " AND ";
              }
              if (event.filters[key].matchMode == 'contains') {
                this.ColumnFilterScript += " " + key + " like '%" + event.filters[key].value + "%' ";
              }
              else if (event.filters[key].matchMode == 'equals') {
                this.ColumnFilterScript += " " + key + " = '" + event.filters[key].value + "' ";
              }
            }
          }
          else {
            this.GlobalFilter = event.filters[key].value;
          }
        }
      }
      */
      if (searchObject) {
        //console.log('searchObject :::::::::::::::::::::::::::: ', searchObject);
        for (var key in searchObject) {
          if (searchObject[key].value != '' && searchObject[key].value != null && searchObject[key].value != '0') {
            if (key != 'global') {
              columnCount++;
              if (searchObject[key].matchMode == 'contains' || searchObject[key].matchMode == 'equals'
                || searchObject[key].matchMode == 'dateGreater' || searchObject[key].matchMode == 'dateLower' || searchObject[key].matchMode == 'dateEquals') {
                if (columnCount > 1) {
                  this.ColumnFilterScript += " AND ";
                }
                if (searchObject[key].matchMode == 'contains') {
                  this.ColumnFilterScript += " " + key + " like '%" + searchObject[key].value + "%' ";
                }
                else if (searchObject[key].matchMode == 'equals') {
                  this.ColumnFilterScript += " " + key + " = '" + searchObject[key].value + "' ";
                }
                else if (searchObject[key].matchMode == 'dateGreater') {
                  //console.log('dateGreater :::::::::::::::::::::::::::: ', searchObject[key].value);
                  if (moment.isMoment(searchObject[key].value)) {
                    var momentString = searchObject[key].value.format('DD/MM/YYYY');
                    //.format('DD/MM/YYYY');
                    //console.log('dateGreater :::::::::::::::::::::::::::: ', momentString);
                    this.ColumnFilterScript += " " + key + " >= convert(datetime, '" + momentString + "', 103)  ";
                  }
                  else {
                    //console.log('dateObj 1 :::::::::::::::::::::::::::: ', searchObject[key].value);
                    var dateObj = moment(searchObject[key].value, "YYYY-MM-DDTHH:mm:ssZ").toDate();
                    //console.log('dateObj 2 :::::::::::::::::::::::::::: ', this.convertDate(dateObj));
                    let momentString = this.convertDate(dateObj);
                    this.ColumnFilterScript += " " + key + " >= convert(datetime, '" + momentString + "', 103)  ";
                  }
                }
                else if (searchObject[key].matchMode == 'dateLower') {
                  if (moment.isMoment(searchObject[key].value)) {
                    var momentString = searchObject[key].value.format('DD/MM/YYYY');
                    //.format('DD/MM/YYYY');
                    //console.log('dateGreater :::::::::::::::::::::::::::: ', momentString);
                    this.ColumnFilterScript += " " + key + " <= convert(datetime, '" + momentString + "', 103)  ";
                  }
                  else {
                    //console.log('dateObj 1 :::::::::::::::::::::::::::: ', searchObject[key].value);
                    var dateObj = moment(searchObject[key].value, "YYYY-MM-DDTHH:mm:ssZ").toDate();
                    //console.log('dateObj 2 :::::::::::::::::::::::::::: ', this.convertDate(dateObj));
                    let momentString = this.convertDate(dateObj);
                    this.ColumnFilterScript += " " + key + " <= convert(datetime, '" + momentString + "', 103)  ";
                  }
                }
                else if (searchObject[key].matchMode == 'dateEquals') {
                  if (moment.isMoment(searchObject[key].value)) {
                    var momentString = searchObject[key].value.format('DD/MM/YYYY');
                    //.format('DD/MM/YYYY');
                    //console.log('dateGreater :::::::::::::::::::::::::::: ', momentString);
                    this.ColumnFilterScript += " " + key + " = convert(datetime, '" + momentString + "', 103)  ";
                  }
                  else {
                    //console.log('dateObj 1 :::::::::::::::::::::::::::: ', searchObject[key].value);
                    var dateObj = moment(searchObject[key].value, "YYYY-MM-DDTHH:mm:ssZ").toDate();
                    //console.log('dateObj 2 :::::::::::::::::::::::::::: ', this.convertDate(dateObj));
                    let momentString = this.convertDate(dateObj);
                    this.ColumnFilterScript += " " + key + " = convert(datetime, '" + momentString + "', 103)  ";
                  }
                }
              }
            }
            else {
              this.GlobalFilter = searchObject[key].value;
            }
          }
        }
      }
    }
    page.ColumnFilterScript = this.ColumnFilterScript;
    if (getSorting != undefined) {
      this.OrderScript = getSorting;
    }
    page.OrderScript = this.OrderScript;
    page.GlobalFilter = this.GlobalFilter;
    return page;
  }

  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat)
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/')
  }

}
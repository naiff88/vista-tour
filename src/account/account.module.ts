import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountRoutingModule } from './account-routing.module';
import { LoginComponent } from './login/login.component';
import { AccountComponent } from './account.component';
import { FormsModule } from '@angular/forms';
import { ServiceProxyModule } from '../shared/service-proxies/service-proxy.module';
import { LoginService } from './login/login.service';
import { AccountRouteGuard } from './auth/account-route-guard';
//import { NumberOnly } from '../app/shared/validators/number-input';

@NgModule({
  declarations: [
    LoginComponent,
    AccountComponent,
    //NumberOnly
  ],
  exports: [
   //NumberOnly
  ],
  imports: [
    CommonModule,
    FormsModule,
    AccountRoutingModule,
    ServiceProxyModule,
    //NumberOnly
  ],
  providers: [
    LoginService,
    AccountRouteGuard
  ]
})
export class AccountModule { }

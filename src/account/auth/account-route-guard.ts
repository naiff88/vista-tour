import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { CookieService } from 'src/shared/common/session/cookie.service';
import { AppSessionService } from 'src/shared/common/session/app-session.service';

@Injectable()
export class AccountRouteGuard implements CanActivate {

    constructor(
      private _router: Router,
      private _cookieService: CookieService,
      private _sessionService: AppSessionService
    ) { }

    canActivate(): boolean {

      if (this._sessionService.userId) {
            this._router.navigate(['/app/dashboard/menu']);
            return false;
        }
        return true;
    }
}

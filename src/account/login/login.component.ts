import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../app/shared/loader/script-loader.service';
import { Helpers } from '../../app/helpers';
import {
  TenantRegistrationServiceProxy,
  RegisterTenantInput,
  ChangePasswordInput,
  AuthServiceProxy
} from '../../shared/service-proxies/auth-service-proxies';
import { LoginService } from './login.service';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';

//declare let $: any;
declare let mUtil: any;
declare let SnippetLogin: any;

@Component({
  selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {
  registerModel: RegisterTenantInput = new RegisterTenantInput();
  changePasswordModel: ChangePasswordInput = new ChangePasswordInput();

  loading = false;
  returnUrl: string;
  submitting: boolean;
  error: string;
  forgetpasswordModel = { email: '' };

  constructor(
    private loginService: LoginService,
    private _authService: AuthServiceProxy,
    private _script: ScriptLoaderService,
    private toastr: ToastrService,
    private _tenantRegistrationService: TenantRegistrationServiceProxy
  ) { }


  displaySignUpForm() {
    // let login = document.getElementById('m_login');
    // mUtil.removeClass(login, 'm-login--signin');
    // try {
    //   $('form')
    //     .data('validator')
    //     .resetForm();
    // } catch (e) { }
    // //mUtil.removeClass(login, 'm-login--signup');
    // mUtil.addClass(login, 'm-login--signup');
    // mUtil.animateClass(
    //   login.getElementsByClassName('m-login--signup')[0],
    //   'flipInX animated'
    // );
    var login = $('#m_login');
    login.removeClass('m-login--forget-password');
    login.removeClass('m-login--signin');
    login.addClass('m-login--signup');
    mUtil.animateClass(login.find('.m-login__signup')[0], 'flipInX animated');
    this.registerModel = new RegisterTenantInput();
    this.error = undefined;
  }

  displayForgetPasswordForm() {
    //let login = document.getElementById('m_login');
    var login = $('#m_login');
    //mUtil.removeClass(login, 'm-login--signin');  
    login.removeClass('m-login--signin');
    login.removeClass('m-login--signup'); 
    login.addClass('m-login--forget-password');
    this.changePasswordModel = new ChangePasswordInput();
    mUtil.animateClass(
      login.find('.m-login__forget-password')[0],
      'flipInX animated'
    );
    this.error = undefined;
  }

  closeForm() {
    // $('#m_login_signup_cancel').click(function (e) {
    //   e.preventDefault();
      let login = document.getElementById('m_login');
      mUtil.removeClass(login, 'm-login--forget-password');
      mUtil.removeClass(login, 'm-login--signup');
      try {
        $('form')
          .data('validator')
          .resetForm();
      } catch (e) { }

      mUtil.addClass(login, 'm-login--signin');
      mUtil.animateClass(
        login.getElementsByClassName('m-login__signin')[0],
        'flipInX animated'
      );
    //});
    this.error = undefined;
  }

 


  signup() {
    this.loading = true;
    // console.log('signup ::::::::::::::: ',this.registerModel.email);
    // console.log('name ::::::::::::::: ',this.registerModel.name);


    //if (this.registerModel.email != "" || this.registerModel.name != "" || this.registerModel.mobileNo < 0 || this.registerModel.password != "" )
    if (!this.registerModel.email || !this.registerModel.name || !this.registerModel.mobileNo || !this.registerModel.password )
    {
      //console.log('error 1');
      this.error = 'All fields are required!';
    }
    else if (this.registerModel.confirmPassword != this.registerModel.password )
    {
      //console.log('error 2');
      this.error = 'Password confirmation and Password must match!';
    }
    else {
    this._tenantRegistrationService
      .registerTenant(this.registerModel)
      .subscribe(
        r => {
          console.log('registerTenant data ::::::::::: ', r);
          this.loading = false;
          // this.displaySignInForm();
          // this.registerModel = new RegisterTenantInput();
          // this.error = undefined;

          
          if (r.returnCode != '400') {
            this.displaySignInForm();
            this.error = undefined;
            this.toastr.success(r.responseMessage, '', {
              timeOut: 5000
            });
          }
          else {
            this.error = r.responseMessage;
          }

        },
        error => {
          this.loading = false;
          this.error = error;
        }
      );
    }
  }


  changePassword() {
    this.loading = true;
    this._tenantRegistrationService
      .changePassword(this.changePasswordModel)
      .subscribe(
        data => {
          this.loading = false;
          this.displaySignInForm();
          this.changePasswordModel = new ChangePasswordInput();
        },
        error => {
          this.loading = false;
          this.error = error;
        }
      );
  }

  displaySignInForm() {
    let login = document.getElementById('m_login');
    mUtil.removeClass(login, 'm-login--forget-password');
    mUtil.removeClass(login, 'm-login--signup');
    try {
      $('form')
        .data('validator')
        .resetForm();
    } catch (e) { }

    mUtil.addClass(login, 'm-login--signin');
    mUtil.animateClass(
      login.getElementsByClassName('m-login__signin')[0],
      'flipInX animated'
    );
  }

  login(): void {
    //console.log('login  ');
    this.submitting = true;
    this.error = undefined;
    this.loginService.authenticate(
      () => (this.submitting = false),
      e => (this.error = e)
    );
    localStorage.removeItem("listMenu");
    localStorage.removeItem("menuId");
    localStorage.removeItem("parentMenuId");
  }

  isChrome() {
    //return !!window['chrome'] && (!!window['chrome']['webstore'] || !!window['chrome']['runtime']);
    //return navigator.userAgent.indexOf("Chrome") != -1 ;
    var browserName = "";
    if (
      navigator.userAgent.indexOf("Edge") > -1 &&
      navigator.appVersion.indexOf("Edge") > -1
    ) {
      browserName = "Edge";
    } else if (
      navigator.userAgent.indexOf("Opera") != -1 ||
      navigator.userAgent.indexOf("OPR") != -1
    ) {
      browserName = "Opera";
    } else if (navigator.userAgent.indexOf("Chrome") != -1) {
      browserName = "Chrome";
    } else if (navigator.userAgent.indexOf("Safari") != -1) {
      browserName = "Safari";
    } else if (navigator.userAgent.indexOf("Firefox") != -1) {
      browserName = "Firefox";
    } else if (
      navigator.userAgent.indexOf("MSIE") != -1 ||
      !!document['documentMode'] == true
    ) {
      //IF IE > 10
      browserName = "IE";
    } else {
      browserName = "unknown";
    }

    return (
      // !!window["chrome"] &&
      // (!!window["chrome"]["webstore"] || !!window["chrome"]["runtime"])
      //navigator.userAgent.indexOf("Chrome") != -1
      browserName == "Chrome"
    );
  }

  forgetpassword() {
    //console.log('forgetpassword ::::::::::::::::::::: ', this.forgetpasswordModel.email);
    //debugger;
    //this._authService.
    // todo: need forget password api endpoint

    if (this.forgetpasswordModel.email) {
      this.loginService.forgotPassword(this.forgetpasswordModel.email)
        .subscribe(r => {
          // console.log('R ::::::::::::::::::::: ', r);
          // if(r.ReturnCode == 401)
          // {
          //   this.toastr.warning(r.ResponseMessage, 'Recovery Failed', {
          //       timeOut: 3000
          //     });
          // }
          // else
          // {
          //   this.toastr.success(r.ResponseMessage, '', {
          //        timeOut: 3000
          //      });
          // }

          if (r.ReturnCode != 401) {
            this.displaySignInForm();
            this.error = undefined;
            this.toastr.success(r.ResponseMessage, '', {
              timeOut: 5000
            });
          }
          else {
            this.error = r.ResponseMessage;
          }
        });
    }
    else {
      this.error = 'Username/Email is required!';
    }


  }

  ngOnInit() {
    //console.log('ischrome ::::::::::::: ', this.isChrome());
    // if (!this.isChrome()) {
    //   setTimeout(() => this.toastr.warning('For the best view & experince, please use Google Chrome as your browser', 'Alert', {
    //     timeOut: 10000
    //   }));
    // }
  }
  ngAfterViewInit() {
    // this._script
    //   .loadScripts('.m-grid.m-grid--hor.m-grid--root.m-page', [
    //     'assets/snippets/custom/pages/user/login.js'
    //   ])
    //   .then(() => {
    //     SnippetLogin.init(
    //       () => {},
    //       () => {
    //         return this._tenantRegistrationService
    //           .registerTenant(this.registerModel)
    //           .toPromise();
    //       },
    //       () => {}
    //     );
    //   });

    Helpers.bodyClass(
      'm--skin- m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default login-bg'
    );
  }
}

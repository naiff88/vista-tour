import { Injectable, Inject, Optional } from '@angular/core';
import {
  AuthenticateModel,
  AuthenticateResultModel,
  AuthServiceProxy,
  ProgressBarState
} from 'src/shared/service-proxies/auth-service-proxies';
import { Router } from '@angular/router';
import { CookieService } from 'src/shared/common/session/cookie.service';
import { finalize, catchError } from 'rxjs/operators';
import { throwError, ObservableInput, Observable, Subject } from 'rxjs';
import { JsonPipe } from '@angular/common';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API_BASE_URL } from 'src/shared/service-proxies/service-proxies';
import { bufferTime, distinctUntilChanged, filter, map, scan, share, take, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

const baseUrl = environment.API_BASE_URL;

@Injectable()
export class LoginService {
  authenticateModel: AuthenticateModel;
  authenticateResult: AuthenticateResultModel;
  http: HttpClient;
  // private baseUrl: string;
  private processingSubject = new Subject<ProgressBarState>();

  constructor(
    private _authService: AuthServiceProxy,
    private _router: Router,
    private _cookieService: CookieService,
    @Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string
  ) {
    this.http = http;
    // this.baseUrl = baseUrl;
    this.clear();
  }

  set isBusy(value: boolean) {
    const state: number = value ? ProgressBarState.Show : ProgressBarState.Hide;
    this.processingSubject.next(state);
  }




  // constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
  //   this.http = http;
  //   this.baseUrl = baseUrl;
  // }

  authenticate(finallyCallback?: () => void, errorCallback?: (e: string) => void, redirectUrl?: string) {
    finallyCallback = finallyCallback || (() => {});
    errorCallback = errorCallback || ((e) => {});
    this.authenticateModel.grant_type = 'password';
    this._authService
      .authenticate(this.authenticateModel)
      .pipe(finalize(finallyCallback))
      .pipe(
        catchError((e, r) => {
          const error =  JSON.parse(e.response);
          errorCallback(error.error_description);
          return null;
        })
      )
      .subscribe((result: AuthenticateResultModel) => {
        this.processAuthenticateResult(result);
      });
  }


  // authenticateRelogin(Username: string, finallyCallback?: () => void, errorCallback?: (e: string) => void, redirectUrl?: string) {
  //   finallyCallback = finallyCallback || (() => {});
  //   errorCallback = errorCallback || ((e) => {});
  //   this.authenticateModel.grant_type = 'password';
  //   this.authenticateModel.username = Username;
  //   this.authenticateModel.password = 'M@5tEr$t00L@kU';
  //
  //   this._authService
  //     .authenticateRelogin(this.authenticateModel)
  //     .pipe(finalize(finallyCallback))
  //     .pipe(
  //       catchError((e, r) => {
  //         const error =  JSON.parse(e.response);
  //         errorCallback(error.error_description);
  //         return null;
  //       })
  //     )
  //     .subscribe((result: AuthenticateResultModel) => {
  //       this.processAuthenticateResultrefreshToken(result);
  //     });
  // }




  private processAuthenticateResult(
    authenticateResult: AuthenticateResultModel
  ) {
    this.authenticateResult = authenticateResult;
    // console.log('authenticateResult ::::::::::::::::: ', authenticateResult);
    if (authenticateResult.accessToken) {
      this.login(
        authenticateResult.accessToken,
        authenticateResult.tokenType,
        authenticateResult.expireInSeconds
      );
    }
  }

  forgotPassword(emailTo: string): Observable<any> {
    // console.log("URL :::::::::::: "+baseUrl + "/api/Admin/password/recovery");
    this.isBusy = true;
    return this.http.put<any>(baseUrl + '/api/Admin/password/recovery', {emailTo})
        .pipe(
          map((resp: any) => {
            // console.log('resp update ::::::::::::::: ', resp);
            const ReturnCode = resp.ReturnCode;
            const ResponseMessage = resp.ResponseMessage;
            return { ReturnCode, ResponseMessage };
          }),
          tap({
            error: () => this.isBusy = false,
            complete: () => this.isBusy = false
          })
        );
  }

  private processAuthenticateResultrefreshToken(
    authenticateResult: AuthenticateResultModel
  ) {
    this.authenticateResult = authenticateResult;
    // console.log('authenticateResult ::::::::::::::::: ', authenticateResult);
    if (authenticateResult.accessToken) {
      this.refreshToken(
        authenticateResult.accessToken,
        authenticateResult.tokenType,
        authenticateResult.expireInSeconds
      );
    }
  }

  private login(
    accessToken: string,
    tokenType: string,
    expireInSeconds: number
  ) {
    this._cookieService.setCookie('spj_auth', accessToken, expireInSeconds);
    this.clear();
    location.href = '/app/dashboard/menu';
  }

  private refreshToken(
    accessToken: string,
    tokenType: string,
    expireInSeconds: number
  ) {
    this._cookieService.setCookie('spj_auth', accessToken, expireInSeconds);
    this.clear();
    // location.href = '/app/dashboard/menu';
  }


  private clear(): void {
    this.authenticateModel = new AuthenticateModel();
    this.authenticateResult = null;
  }
}
